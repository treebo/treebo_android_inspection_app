package com.treebo.prowlapp.flowstaffincentives.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class IncentiveGivenUseCase extends UseCase<BaseResponse> {

    private RestClient mRestClient;
    private int mHotelID;
    private int mDepartmentID;
    private int mStaffID;
    private int mMonth;
    private int mYear;
    private int mIncentiveGiven;
    private String mReason;

    public IncentiveGivenUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setField(int hotelID, int deptID, int staffID, int month, int year, String reason, int incentive) {
        this.mHotelID = hotelID;
        this.mDepartmentID = deptID;
        this.mStaffID = staffID;
        this.mMonth = month;
        this.mYear = year;
        this.mReason = reason;
        this.mIncentiveGiven = incentive;
    }

    @Override
    protected Observable<BaseResponse> getObservable() {
        return mRestClient.incentiveGiven(mHotelID, mDepartmentID, mStaffID, mReason, mMonth, mYear, mIncentiveGiven);
    }
}
