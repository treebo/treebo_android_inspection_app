package com.treebo.prowlapp.flowstaffincentives.presenter;

import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveContract;
import com.treebo.prowlapp.flowstaffincentives.observer.DepartmentListObserver;
import com.treebo.prowlapp.flowstaffincentives.usecase.DepartmentListUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.staffincentives.DepartmentObject;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 01/03/17.
 */

public class DepartmentListPresenter implements StaffIncentiveContract.IDepartmentListPresenter{

    private StaffIncentiveContract.IDepartmentListView mView;

    private DepartmentListUseCase mUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (StaffIncentiveContract.IDepartmentListView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        mView.hideLoading();
    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void onCreate() {
        mView.setUpToolbar();
        mView.setUpRecyclerView();
    }

    @Override
    public void getDepartmentIncentives(int hotelID, String status, int month, int year) {
        mView.showLoading();
        mUseCase.setFields(hotelID, status, month, year);
        mUseCase.execute(providesStaffListObserver());
    }

    @Override
    public void setAdapterData(ArrayList<DepartmentObject> data) {
        mView.onLoadListSuccess(data);
    }

    @Override
    public void onGetListFailure(String msg) {
        mView.onLoadListFailure(msg);
    }

    public void setUseCase(DepartmentListUseCase mRoomSingleUse) {
        this.mUseCase = mRoomSingleUse;
    }

    public DepartmentListObserver providesStaffListObserver() {
        return new DepartmentListObserver(this, mUseCase);
    }
}
