package com.treebo.prowlapp.flowstaffincentives.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class MonthlyIncentiveListUseCase extends UseCase<MonthlyIncentiveListResponse> {

    private RestClient mRestClient;
    private int mHotelID;

    public MonthlyIncentiveListUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setFields(int hotelID) {
        this.mHotelID = hotelID;
    }

    @Override
    protected Observable<MonthlyIncentiveListResponse> getObservable() {
        return mRestClient.getMonthlyIncentiveList(mHotelID);
    }
}
