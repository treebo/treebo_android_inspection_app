package com.treebo.prowlapp.flowstaffincentives.observer;

import com.treebo.prowlapp.flowstaffincentives.presenter.MonthlyListPresenter;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class MonthlyIncentiveListObserver extends BaseObserver<MonthlyIncentiveListResponse> {

    private MonthlyListPresenter mPresenter;

    public MonthlyIncentiveListObserver(MonthlyListPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        mPresenter = presenter;
    }

    @Override
    public void onCompleted() {
        mPresenter.resume();
    }

    @Override
    public void onNext(MonthlyIncentiveListResponse response) {
        mPresenter.resume();
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.setAdapterData(response.getMonthlyIncentiveList());
        } else {
            mPresenter.onGetListFailure(response.msg);
        }
    }
}
