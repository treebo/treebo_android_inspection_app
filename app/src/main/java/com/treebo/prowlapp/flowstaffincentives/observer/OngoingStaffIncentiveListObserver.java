package com.treebo.prowlapp.flowstaffincentives.observer;

import com.treebo.prowlapp.flowstaffincentives.presenter.StaffIncentivesDonePresenter;
import com.treebo.prowlapp.flowstaffincentives.presenter.ToDoStaffIncentivesPresenter;
import com.treebo.prowlapp.response.staffincentives.StaffListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class OngoingStaffIncentiveListObserver extends BaseObserver<StaffListResponse> {

    private ToDoStaffIncentivesPresenter mToDoPresenter;
    private StaffIncentivesDonePresenter mDonePresenter;
    private boolean isViewOnly;

    public OngoingStaffIncentiveListObserver(ToDoStaffIncentivesPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        mToDoPresenter = presenter;
    }

    public OngoingStaffIncentiveListObserver(StaffIncentivesDonePresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        mDonePresenter = presenter;
        isViewOnly = true;
    }

    @Override
    public void onCompleted() {
        if (isViewOnly)
            mDonePresenter.resume();
        else
            mToDoPresenter.resume();
    }

    @Override
    public void onNext(StaffListResponse response) {
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            if (isViewOnly)
                mDonePresenter.onStaffListSuccess(response);
            else
                mToDoPresenter.onStaffListSuccess(response);
        } else {
            if (isViewOnly)
                mDonePresenter.onCallFailure(response.msg);
            else
                mToDoPresenter.onCallFailure(response.msg);
        }
    }
}
