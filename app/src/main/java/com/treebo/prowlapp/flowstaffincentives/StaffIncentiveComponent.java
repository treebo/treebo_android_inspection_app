package com.treebo.prowlapp.flowstaffincentives;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.DepartmentListActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.EditIncentivesActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.MonthlyListActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.StaffDetailsActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.StaffIncentivesDoneActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.ToDoStaffIncentivesActivity;

import dagger.Component;

/**
 * Created by abhisheknair on 28/02/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, StaffIncentiveModule.class})
public interface StaffIncentiveComponent {

    void injectMonthlyListActivity(MonthlyListActivity monthlyListActivity);

    void injectDepartmentListActivity(DepartmentListActivity departmentListActivity);

    void injectEditIncentiveActivity(EditIncentivesActivity editIncentivesActivity);

    void injectToDoStaffIncentivesActivity(ToDoStaffIncentivesActivity toDoStaffIncentivesActivity);

    void injectStaffIncentivesDoneActivity(StaffIncentivesDoneActivity staffIncentivesDoneActivity);

    void injectStaffDetailsActivity(StaffDetailsActivity staffDetailsActivity);

}
