package com.treebo.prowlapp.flowstaffincentives.presenter;

import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveContract;
import com.treebo.prowlapp.flowstaffincentives.observer.OngoingStaffIncentiveListObserver;
import com.treebo.prowlapp.flowstaffincentives.usecase.OngoingStaffIncentiveListUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.staffincentives.StaffListResponse;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class StaffIncentivesDonePresenter implements StaffIncentiveContract.IStaffIncentivesDonePresenter {

    private StaffIncentiveContract.IStaffIncentivesDoneView mView;
    private OngoingStaffIncentiveListUseCase mUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (StaffIncentiveContract.IStaffIncentivesDoneView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        mView.hideLoading();
    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void onCreate() {
        mView.setUpToolBar();
        mView.setUpRecyclerView();
    }

    @Override
    public void getStaffList(int hotelID, int deptID, String status, int month, int year) {
        mView.showLoading();
        mUseCase.setFields(hotelID, deptID, status, month, year);
        mUseCase.execute(providesStaffListObserver());
    }

    @Override
    public void onStaffListSuccess(StaffListResponse response) {
        mView.hideLoading();
        mView.setDataInAdapter(response.getStaffList());
    }

    @Override
    public void onCallFailure(String msg) {
        mView.hideLoading();
        mView.onStaffListFailure(msg);
    }

    public void setUseCase(OngoingStaffIncentiveListUseCase mRoomSingleUse) {
        this.mUseCase = mRoomSingleUse;
    }

    public OngoingStaffIncentiveListObserver providesStaffListObserver() {
        return new OngoingStaffIncentiveListObserver(this, mUseCase);
    }
}
