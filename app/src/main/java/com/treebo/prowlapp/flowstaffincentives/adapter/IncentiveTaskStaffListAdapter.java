package com.treebo.prowlapp.flowstaffincentives.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.staffincentivemodel.SectionedStaffListModel;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.response.staffincentives.StaffObject;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;
import com.tubb.smrv.SwipeHorizontalMenuLayout;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 02/03/17.
 */

public class IncentiveTaskStaffListAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private ArrayList<SectionedStaffListModel> mList;
    private staffClickListener mListener;
    private Context mContext;
    private String month;
    private RolesAndUserPermissions.PermissionList mUserPermissions;

    public IncentiveTaskStaffListAdapter(Context context, ArrayList<SectionedStaffListModel> list,
                                         staffClickListener listener, String month,
                                         RolesAndUserPermissions.PermissionList permissions) {
        this.mContext = context;
        this.mList = list;
        this.mListener = listener;
        this.month = month;
        this.mUserPermissions = permissions;
    }

    public void setData(ArrayList<SectionedStaffListModel> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public void clearData() {
        mList.clear();
    }

    @Override
    public int getSectionCount() {
        if (mList == null)
            return 0;
        else
            return mList.size();
    }

    @Override
    public int getItemCount(int i) {
        if (mList == null)
            return 0;
        else
            return mList.get(i).getStaffList().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        SectionedStaffListModel section = mList.get(i);
        ((IncentiveSectionViewHolder) viewHolder).sectionTitle.setText(section.getSectionName());
        ((IncentiveSectionViewHolder) viewHolder).sectionCount.setText(String.valueOf(section.getStaffList().size()));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int section, int i1, int i2) {
        StaffObject staffObject = mList.get(section).getStaffList().get(i1);
        ((StaffHolder) viewHolder).object = staffObject;
        ((StaffHolder) viewHolder).staffName.setText(staffObject.getStaffName());
        if (!TextUtils.isEmpty(staffObject.getStaffImageUrl())) {
            Utils.loadRoundedImageUsingGlide(mContext, staffObject.getStaffImageUrl(),
                    ((StaffHolder) viewHolder).staffImage, R.drawable.ic_person_placeholder);
        }
        if (staffObject.isEligible()) {
            ((StaffHolder) viewHolder).incentiveText.setText(mContext.getString(R.string.rupee_string,
                    staffObject.getIncentive()));
            ((StaffHolder) viewHolder).incentiveText.setTextColor(ContextCompat.getColor(mContext, staffObject.getColor()));
            ((StaffHolder) viewHolder).siaTv.setText(mContext.getString(R.string.sia_string,
                    staffObject.getStartingIncentive()));
        } else {
            ((StaffHolder) viewHolder).incentiveText.setText(mContext.getString(R.string.na_string));
            ((StaffHolder) viewHolder).siaTv.setText(mContext.getString(R.string.not_applicable, month));
        }
        if (staffObject.isIncentiveModified()) {
            ((StaffHolder) viewHolder).editedStringTv.setVisibility(View.VISIBLE);
            ((StaffHolder) viewHolder).editedIncentiveTv.setText(mContext.getString(R.string.rupee_string,
                    staffObject.getIncentiveGiven()));
            ((StaffHolder) viewHolder).editedIncentiveTv
                    .setPaintFlags(((StaffHolder) viewHolder).editedIncentiveTv.getPaintFlags() |
                            Paint.STRIKE_THRU_TEXT_FLAG);
            ((StaffHolder) viewHolder).editedIncentiveTv.setVisibility(View.VISIBLE);
        } else {
            ((StaffHolder) viewHolder).editedStringTv.setVisibility(View.GONE);
            ((StaffHolder) viewHolder).editedIncentiveTv.setVisibility(View.GONE);
        }
        if (staffObject.isPendingGiven()) {
            ((StaffHolder) viewHolder).resolvedCardHeading.setText(staffObject.getStaffName());
            ((StaffHolder) viewHolder).resolvedCardSubHeading.setText(mContext.getString(R.string.sia_string,
                    staffObject.getStartingIncentive()));
            ((StaffHolder) viewHolder).resolvedLayout.setVisibility(View.VISIBLE);
        } else {
            ((StaffHolder) viewHolder).resolvedLayout.setVisibility(View.GONE);
        }
        ((StaffHolder) viewHolder).editAction.setOnClickListener(view -> {
            mListener.editGivenClicked(staffObject, true);
            ((SwipeHorizontalMenuLayout) viewHolder.itemView).smoothCloseMenu();
        });
        ((StaffHolder) viewHolder).givenAction.setOnClickListener(view -> {
            mListener.editGivenClicked(staffObject, false);
            ((SwipeHorizontalMenuLayout) viewHolder.itemView).smoothCloseMenu();
        });
        if (staffObject.getStatus().toLowerCase().equals(mContext.getString(R.string.status_complete))) {
            ((StaffHolder) viewHolder).disabledLayout.setVisibility(View.VISIBLE);
            ((StaffHolder) viewHolder).incentiveText.setText(mContext.getString(R.string.rupee_string,
                    staffObject.getIncentive()));
            ((SwipeHorizontalMenuLayout) viewHolder.itemView).setSwipeEnable(false);
        } else {
            ((StaffHolder) viewHolder).disabledLayout.setVisibility(View.GONE);
            ((SwipeHorizontalMenuLayout) viewHolder.itemView).setSwipeEnable(true);
        }
        ((StaffHolder) viewHolder).editAction.setVisibility(mUserPermissions.isStaffIncentivesEditEnabled()
                ? View.VISIBLE : View.GONE);
        ((StaffHolder) viewHolder).givenAction.setVisibility(mUserPermissions.isStaffIncentivesDistributeEnabled()
                ? View.VISIBLE : View.GONE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_swipable_staff_incentives,
                    parent, false);
            return new StaffHolder(view, mListener);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_issue_section_header,
                    parent, false);
            return new IncentiveSectionViewHolder(view);
        }
    }

    private class IncentiveSectionViewHolder extends RecyclerView.ViewHolder {

        TreeboTextView sectionTitle;
        TreeboTextView sectionCount;

        IncentiveSectionViewHolder(View itemView) {
            super(itemView);
            sectionTitle = (TreeboTextView) itemView.findViewById(R.id.issue_section_header_title);
            sectionCount = (TreeboTextView) itemView.findViewById(R.id.issue_section_item_count);
        }
    }

    private class StaffHolder extends RecyclerView.ViewHolder {

        StaffObject object;
        ImageView staffImage;
        TreeboTextView staffName;
        TreeboTextView siaTv;
        TreeboTextView incentiveText;
        TreeboTextView editedStringTv;
        TreeboTextView editedIncentiveTv;
        View resolvedLayout;
        TreeboTextView resolvedCardHeading;
        TreeboTextView resolvedCardSubHeading;
        TreeboTextView undoBtn;
        View disabledLayout;
        TreeboButton editAction;
        TreeboButton givenAction;

        public StaffHolder(View itemView, staffClickListener listener) {
            super(itemView);
            itemView.setOnClickListener(view -> {
                listener.cardClicked(object);
            });
            staffImage = (ImageView) itemView.findViewById(R.id.staff_image);
            staffName = (TreeboTextView) itemView.findViewById(R.id.staff_name_tv);
            siaTv = (TreeboTextView) itemView.findViewById(R.id.incentive_actual_tv);
            incentiveText = (TreeboTextView) itemView.findViewById(R.id.incentive_paid_tv);
            editedStringTv = (TreeboTextView) itemView.findViewById(R.id.edited_tv);
            editedIncentiveTv = (TreeboTextView) itemView.findViewById(R.id.edited_paid_incentive_tv);
            editAction = (TreeboButton) itemView.findViewById(R.id.swipe_edit);
            givenAction = (TreeboButton) itemView.findViewById(R.id.swipe_given);
            resolvedLayout = itemView.findViewById(R.id.resolved_layout);
            resolvedCardHeading = (TreeboTextView) itemView.findViewById(R.id.resolved_card_heading);
            resolvedCardSubHeading = (TreeboTextView) itemView.findViewById(R.id.resolved_card_subheading);
            undoBtn = (TreeboTextView) itemView.findViewById(R.id.undo_btn);
            undoBtn.setOnClickListener(view -> {
                listener.undoClicked(object);
            });
            disabledLayout = itemView.findViewById(R.id.card_disabled);
        }
    }

    public interface staffClickListener {
        void undoClicked(StaffObject object);

        void cardClicked(StaffObject object);

        void editGivenClicked(StaffObject object, boolean isEdit);
    }
}
