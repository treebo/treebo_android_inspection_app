package com.treebo.prowlapp.flowstaffincentives.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.WindowManager;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowstaffincentives.DaggerStaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveContract;
import com.treebo.prowlapp.flowstaffincentives.adapter.IncentiveTaskStaffListAdapter;
import com.treebo.prowlapp.flowstaffincentives.presenter.ToDoStaffIncentivesPresenter;
import com.treebo.prowlapp.flowstaffincentives.usecase.IncentiveGivenUseCase;
import com.treebo.prowlapp.flowstaffincentives.usecase.OngoingStaffIncentiveListUseCase;
import com.treebo.prowlapp.Models.staffincentivemodel.SectionedStaffListModel;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.staffincentives.StaffObject;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;
import com.tubb.smrv.SwipeMenuRecyclerView;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class ToDoStaffIncentivesActivity extends AppCompatActivity
        implements StaffIncentiveContract.IToDoStaffIncentivesView, IncentiveTaskStaffListAdapter.staffClickListener {

    private static final int REQUEST_EDIT = 160;
    private static final int REQUEST_DETAILS = 165;

    @Inject
    public ToDoStaffIncentivesPresenter mPresenter;

    @Inject
    public OngoingStaffIncentiveListUseCase mUseCase;

    @Inject
    public IncentiveGivenUseCase mIncentiveGivenUseCase;

    @Inject
    public Navigator mNavigator;

    @Inject
    public LoginSharedPrefManager mSharedPrefs;

    private int mHotelID;
    private int mDepartmentID;
    private int mMonth;
    private int mYear;
    private String mStatus;

    private View mLoadingView;
    private SwipeMenuRecyclerView mRecyclerView;
    private IncentiveTaskStaffListAdapter mAdapter;
    private ArrayList<SectionedStaffListModel> mAdapterList;
    private ArrayList<StaffObject> mStaffList;
    private String mDepartmentTitle;
    private boolean isUndoClicked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_incentives_task);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }

        StaffIncentiveComponent staffIncentiveComponent = DaggerStaffIncentiveComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        staffIncentiveComponent.injectToDoStaffIncentivesActivity(this);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mHotelID = intent.getIntExtra(Utils.HOTEL_ID, -1);
            mDepartmentID = intent.getIntExtra(Utils.DEPARTMENT_ID, -1);
            mDepartmentTitle = intent.getStringExtra(Utils.DEPARTMENT_TITLE);
            mMonth = intent.getIntExtra(Utils.REQUIRED_MONTH, -1);
            mYear = intent.getIntExtra(Utils.REQUIRED_YEAR, -1);
            mStatus = intent.getStringExtra(Utils.DEPARTMENT_INCENTIVE_STATUS);
        } else {
            mHotelID = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            mDepartmentID = savedInstanceState.getInt(Utils.DEPARTMENT_ID, -1);
            mDepartmentTitle = savedInstanceState.getString(Utils.DEPARTMENT_TITLE);
            mMonth = savedInstanceState.getInt(Utils.REQUIRED_MONTH, -1);
            mYear = savedInstanceState.getInt(Utils.REQUIRED_YEAR, -1);
            mStatus = savedInstanceState.getString(Utils.DEPARTMENT_INCENTIVE_STATUS);
        }

        View toolbar = findViewById(R.id.toolbar);
        TreeboTextView title = (TreeboTextView) toolbar.findViewById(R.id.toolbar_title_tv);
        String monthText = new DateFormatSymbols().getMonths()[mMonth - 1];
        title.setText(getString(R.string.todo_incentives_title, mDepartmentTitle, monthText));
        View crossBtn = toolbar.findViewById(R.id.toolbar_audit_back_btn);
        crossBtn.setOnClickListener(view -> {
            onBackPressed();
        });

        mLoadingView = findViewById(R.id.loader_layout);
        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));
        mRecyclerView = (SwipeMenuRecyclerView) findViewById(R.id.staff_incentives_rv);

        mPresenter.onCreate();
        mPresenter.getStaffList(mHotelID, mDepartmentID, mMonth, mYear);

    }

    @Override
    public void onBackPressed() {
        if (mAdapterList.size() > 1) {
            if (mAdapterList.get(0).getStaffList().size() == getNonEligibleStaffCount()) {
                setResultAndFinish();
            } else {
                showPauseAuditDialog();
            }
        } else if (mAdapterList.get(0).getSectionName().equals(getString(R.string.incentives_given))) {
            setResultAndFinish();
        } else {
            super.onBackPressed();
        }
    }

    private void showPauseAuditDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_pause_audit, null);
        TreeboTextView dialogText = (TreeboTextView) dialogView.findViewById(R.id.pause_dialog_text);
        dialogText.setText(getString(R.string.pause_incentives_text, mDepartmentTitle));
        dialogBuilder.setView(dialogView);
        final AlertDialog pauseAuditDialog = dialogBuilder.create();
        pauseAuditDialog.show();

        TreeboTextView notNowBtn = (TreeboTextView) dialogView.findViewById(R.id.not_now_btn);
        notNowBtn.setOnClickListener(view -> pauseAuditDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            pauseAuditDialog.dismiss();
            setResultAndFinish();
        });

    }

    private void setResultAndFinish() {
        Intent intent = new Intent();
        intent.putExtra(Utils.DEPARTMENT_ID, mDepartmentID);
        int tasksDone = 0;
        if (mAdapterList.size() > 1) {
            tasksDone = mAdapterList.get(1).getStaffList().size() + getNonEligibleStaffCount();
        } else if (mAdapterList.get(0).getSectionName().equals(getString(R.string.incentives_given))) {
            tasksDone = mAdapterList.get(0).getStaffList().size();
        }
        intent.putExtra(Utils.TASKS_DONE_COUNT, tasksDone);
        setResult(RESULT_OK, intent);
        finish();
    }

    private int getNonEligibleStaffCount() {
        int count = 0;
        for (StaffObject object : mStaffList) {
            if (!object.isEligible())
                count++;
        }
        return count;
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setListUseCase(mUseCase);
        mPresenter.setmIncentiveGivenUseCase(mIncentiveGivenUseCase);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void setDataInAdapter(ArrayList<StaffObject> list) {
        mStaffList = list;
        mAdapterList = formSectionedList();
        mAdapter.setData(mAdapterList);
    }

    @Override
    public void setUpRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new IncentiveTaskStaffListAdapter(this, mAdapterList, this,
                new DateFormatSymbols().getMonths()[mMonth - 1], mSharedPrefs.getUserPermissionList(mHotelID));
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private ArrayList<SectionedStaffListModel> formSectionedList() {
        ArrayList<SectionedStaffListModel> list = new ArrayList<>();
        ArrayList<StaffObject> toBeDone = new ArrayList<>();
        ArrayList<StaffObject> doneList = new ArrayList<>();
        for (StaffObject object : mStaffList) {
            if (object.getStatus().equals(getString(R.string.status_complete)) && object.isEligible()) {
                doneList.add(object);
            } else {
                toBeDone.add(object);
            }
        }
        if (toBeDone.size() > 0) {
            SectionedStaffListModel toDoModel = new SectionedStaffListModel();
            toDoModel.setSectionName(getString(R.string.incentives_to_give));
            toDoModel.setStaffList(toBeDone);
            list.add(toDoModel);
        }

        if (doneList.size() > 0) {
            SectionedStaffListModel doneModel = new SectionedStaffListModel();
            doneModel.setSectionName(getString(R.string.incentives_given));
            doneModel.setStaffList(doneList);
            list.add(doneModel);
        }

        return list;
    }

    @Override
    public void onStaffListFailure(String msg) {
        SnackbarUtils.show(findViewById(android.R.id.content), msg);
    }

    @Override
    public void onIncentiveGivenPostFailure(StaffObject object, String msg) {
        SnackbarUtils.show(findViewById(android.R.id.content), msg);
        int index = findStaffInList(object);
        if (index != -1) {
            object.setPendingGiven(false);
            object.setStatus(getString(R.string.status_pending));
            mStaffList.set(index, object);
            mAdapter.setData(mAdapterList);
        }
    }

    @Override
    public void undoClicked(StaffObject object) {
        isUndoClicked = true;
    }

    @Override
    public void cardClicked(StaffObject object) {
        mNavigator.navigateToStaffDetailsActivity(this, mHotelID, object, mMonth, REQUEST_DETAILS);
    }

    @Override
    public void editGivenClicked(StaffObject object, boolean isEdit) {
        if (!isEdit) {
            int index = findStaffInList(object);
            if (index != -1) {
                object.setPendingGiven(true);
                object.setStatus(getString(R.string.status_complete));
                mStaffList.set(index, object);
                mAdapter.setData(mAdapterList);
                new Handler().postDelayed(() -> {
                    if (!isUndoClicked) {
                        object.setPendingGiven(false);
                        object.setStatus(getString(R.string.status_complete));
                        mPresenter.postIncentiveGiven(mHotelID, mDepartmentID, mMonth, mYear, object);
                    } else {
                        object.setPendingGiven(false);
                        object.setStatus(getString(R.string.status_pending));
                    }
                    mStaffList.set(index, object);
                    mAdapterList = formSectionedList();
                    mAdapter.setData(mAdapterList);
                    isUndoClicked = false;
                }, TimeUnit.SECONDS.toMillis(2));
            }
        } else {
            mNavigator.navigateToEditIncentiveActivity(this, object, REQUEST_EDIT);
        }
    }

    private int findStaffInList(StaffObject object) {
        int index = -1;
        for (int i = 0; i < mStaffList.size(); i++) {
            StaffObject staff = mStaffList.get(i);
            if (staff.getStaffID() == object.getStaffID()) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(Utils.HOTEL_ID, mHotelID);
        savedInstanceState.putInt(Utils.DEPARTMENT_ID, mDepartmentID);
        savedInstanceState.putString(Utils.DEPARTMENT_TITLE, mDepartmentTitle);
        savedInstanceState.putInt(Utils.REQUIRED_MONTH, mMonth);
        savedInstanceState.putInt(Utils.REQUIRED_YEAR, mYear);
        savedInstanceState.putString(Utils.DEPARTMENT_INCENTIVE_STATUS, mStatus);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_EDIT:
                if (resultCode == RESULT_OK) {
                    StaffObject object = data.getParcelableExtra(Utils.STAFF_OBJECT);
                    int index = findStaffInList(object);
                    if (index != -1) {
                        mStaffList.set(index, object);
                        mAdapterList = formSectionedList();
                        mAdapter.setData(mAdapterList);
                    }
                }
                break;

            case REQUEST_DETAILS:
                if (resultCode == Utils.RESULT_EDIT_INCENTIVE) {
                    StaffObject object = data.getParcelableExtra(Utils.STAFF_OBJECT);
                    int index = findStaffInList(object);
                    if (index != -1) {
                        mStaffList.set(index, object);
                        mAdapterList = formSectionedList();
                        mAdapter.setData(mAdapterList);
                    }
                } else if (resultCode == Utils.RESULT_INCENTIVE_GIVEN) {
                    StaffObject object = data.getParcelableExtra(Utils.STAFF_OBJECT);
                    int index = findStaffInList(object);
                    if (index != -1) {
                        object.setPendingGiven(false);
                        object.setStatus(getString(R.string.status_complete));
                        mPresenter.postIncentiveGiven(mHotelID, mDepartmentID, mMonth, mYear, object);
                        mStaffList.set(index, object);
                        mAdapterList = formSectionedList();
                        mAdapter.setData(mAdapterList);
                    }
                }
                break;
        }
    }
}
