package com.treebo.prowlapp.flowstaffincentives.fragment;

import android.app.Dialog;
import android.os.Bundle;

import android.view.View;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowstaffincentives.adapter.MonthBottomSheetAdapter;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 01/03/17.
 */

public class PastMonthBottomSheetFragment extends BottomSheetDialogFragment
        implements MonthBottomSheetAdapter.onMonthClicked {

    private onMonthClickedActionListener mListener;
    private ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> mList;

    public static PastMonthBottomSheetFragment newInstance(ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> list) {
        PastMonthBottomSheetFragment fragment = new PastMonthBottomSheetFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(Utils.PAST_MONTH_LIST, list);
        fragment.setArguments(args);
        return fragment;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View rootView = View.inflate(getContext(), R.layout.layout_month_bottomsheet, null);
        dialog.setContentView(rootView);
        dialog.setOnShowListener(view -> {
            BottomSheetBehavior.from((View) rootView.getParent())
                    .setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) rootView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        mList = getArguments().getParcelableArrayList(Utils.PAST_MONTH_LIST);
        View closeBtn = rootView.findViewById(R.id.bottom_sheet_close_icon);
        closeBtn.setOnClickListener(view -> dismiss());

        RecyclerView monthRv = (RecyclerView) rootView.findViewById(R.id.month_rv);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext());
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        monthRv.setLayoutManager(layoutManager1);
        monthRv.setItemAnimator(new DefaultItemAnimator());

        MonthBottomSheetAdapter adapter = new MonthBottomSheetAdapter(getContext(), mList, this);
        monthRv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public interface onMonthClickedActionListener {
        void openMonthSpecificIncentivesFromBottomSheet(MonthlyIncentiveListResponse.MonthlyIncentiveListObject month);
    }

    public void setListener(onMonthClickedActionListener listener) {
        this.mListener = listener;
    }

    @Override
    public void goToSelectedMonth(MonthlyIncentiveListResponse.MonthlyIncentiveListObject month) {
        dismiss();
        mListener.openMonthSpecificIncentivesFromBottomSheet(month);
    }
}
