package com.treebo.prowlapp.flowstaffincentives.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.WindowManager;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.DepartmentIncentivesGivenEvent;
import com.treebo.prowlapp.flowstaffincentives.DaggerStaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveContract;
import com.treebo.prowlapp.flowstaffincentives.adapter.DepartmentListAdapter;
import com.treebo.prowlapp.flowstaffincentives.presenter.DepartmentListPresenter;
import com.treebo.prowlapp.flowstaffincentives.usecase.DepartmentListUseCase;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.response.staffincentives.DepartmentObject;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 01/03/17.
 */

public class DepartmentListActivity extends AppCompatActivity
        implements StaffIncentiveContract.IDepartmentListView, DepartmentListAdapter.onDepartmentClickedListener {

    private static final int REQUEST_TO_DO_STAFF_INCENTIVES = 170;

    @Inject
    public DepartmentListPresenter mPresenter;

    @Inject
    public Navigator mNavigator;

    @Inject
    public DepartmentListUseCase mUseCase;

    @Inject
    public RxBus mRxBus;

    @Inject
    public LoginSharedPrefManager mSharedPrefs;

    private int mHotelID;

    private RecyclerView mMonthlyIncentivesRecyclerView;
    private View mLoadingView;
    private TreeboTextView mTitle;

    private DepartmentListAdapter mAdapter;
    private DepartmentListAdapter mDoneAdapter;
    private ArrayList<DepartmentObject> mDepartmentList = new ArrayList<>();
    private ArrayList<DepartmentObject> mDoneDepartmentList = new ArrayList<>();

    private MonthlyIncentiveListResponse.MonthlyIncentiveListObject mMonthlyObject;
    private int mDepartmentID;

    private View mNoIncentivesView;
    private TreeboTextView mNoIncentivesViewTitle;
    private TreeboTextView mNoIncentivesViewSubTitle;
    private View mCompletedText;
    private TreeboTextView mCompletedCountTv;
    private RecyclerView mDoneMonthsRecyclerView;
    private RolesAndUserPermissions.PermissionList mUserPermissions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_incentives);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.activity_gray_background));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        StaffIncentiveComponent staffIncentiveComponent = DaggerStaffIncentiveComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        staffIncentiveComponent.injectDepartmentListActivity(this);

        if (savedInstanceState != null) {
            mHotelID = savedInstanceState.getInt(Utils.HOTEL_ID);
            mDepartmentID = savedInstanceState.getInt(Utils.DEPARTMENT_ID);
            mMonthlyObject = savedInstanceState.getParcelable(Utils.MONTHLY_RESPONSE_OBJECT);
        } else {
            Intent intent = getIntent();
            mHotelID = intent.getIntExtra(Utils.HOTEL_ID, -1);
            mDepartmentID = intent.getIntExtra(Utils.DEPARTMENT_ID, -1);
            mMonthlyObject = intent.getParcelableExtra(Utils.MONTHLY_RESPONSE_OBJECT);
        }
        mUserPermissions = mSharedPrefs.getUserPermissionList(mHotelID);

        View mPastMonths = findViewById(R.id.past_months_tv);
        mPastMonths.setVisibility(View.GONE);
        mMonthlyIncentivesRecyclerView = (RecyclerView) findViewById(R.id.past_months_rv);
        View backBtn = findViewById(R.id.ic_toolbar_audit_back);
        backBtn.setOnClickListener(view -> {
            onBackPressed();
        });
        mNoIncentivesView = findViewById(R.id.layout_no_incentive);
        View crossBtn = mNoIncentivesView.findViewById(R.id.ic_close);
        crossBtn.setOnClickListener(view -> {
            mNoIncentivesView.setVisibility(View.GONE);
        });
        mNoIncentivesViewTitle = (TreeboTextView) mNoIncentivesView.findViewById(R.id.title_tv);
        mNoIncentivesViewSubTitle = (TreeboTextView) mNoIncentivesView.findViewById(R.id.subtext_tv);
        View textView = mNoIncentivesView.findViewById(R.id.month_history_tv);
        textView.setVisibility(View.GONE);
        mCompletedText = findViewById(R.id.completed_audit_tv);
        mCompletedCountTv = (TreeboTextView) findViewById(R.id.completed_count_tv);

        mDoneMonthsRecyclerView = (RecyclerView) findViewById(R.id.done_months_rv);
        mLoadingView = findViewById(R.id.loader_layout);
        mTitle = (TreeboTextView) findViewById(R.id.toolbar_audit_title);

        mPresenter.onCreate();
        mPresenter.getDepartmentIncentives(mHotelID, mMonthlyObject.getStatus(), mMonthlyObject.getMonth(), mMonthlyObject.getYear());
    }

    @Override
    public void onBackPressed() {
        if (mNoIncentivesView.getVisibility() == View.VISIBLE) {
            mNoIncentivesView.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setUseCase(mUseCase);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void onLoadListSuccess(ArrayList<DepartmentObject> data) {
        for (DepartmentObject object : data) {
            if (object.getStatus().equals(getString(R.string.status_complete))) {
                mDoneDepartmentList.add(object);
            } else {
                mDepartmentList.add(object);
            }
        }
        mAdapter.setData(mDepartmentList);
        mDoneAdapter.setData(mDoneDepartmentList);
        if (mDepartmentList.size() == 0) {
            mRxBus.postEvent(new DepartmentIncentivesGivenEvent(mMonthlyObject.getMonth(), mMonthlyObject.getYear()));
        }
        if (mDoneDepartmentList.size() > 0) {
            mCompletedText.setVisibility(View.VISIBLE);
            mCompletedCountTv.setText(String.valueOf(mDoneDepartmentList.size()));
            mCompletedCountTv.setVisibility(View.VISIBLE);
            mDoneMonthsRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoadListFailure(String msg) {
        SnackbarUtils.show(findViewById(android.R.id.content), msg);
    }

    @Override
    public void setUpRecyclerView() {
        mAdapter = new DepartmentListAdapter(this, mDepartmentList, this, mMonthlyObject.getStatus());
        mMonthlyIncentivesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMonthlyIncentivesRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        mDoneAdapter = new DepartmentListAdapter(this, mDoneDepartmentList, this, mMonthlyObject.getStatus());
        mDoneMonthsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDoneMonthsRecyclerView.setAdapter(mDoneAdapter);
        mDoneAdapter.notifyDataSetChanged();
    }

    @Override
    public void setUpToolbar() {
        if (mMonthlyObject != null)
            mTitle.setText(getString(R.string.monthly_department_title, mMonthlyObject.getMonthText()));
    }

    @Override
    public void onDepartmentClicked(DepartmentObject dept) {
        if (dept.isDistribute()) {
            if (mMonthlyObject.getStatus().equals(getString(R.string.status_pending)) && mUserPermissions.isStaffIncentivesWriteEnabled())
                mNavigator.navigateToToDoStaffIncentiveActivity(this, mHotelID, dept.getDepartmentID(), dept.getDepartmentName(), mMonthlyObject.getStatus(),
                        mMonthlyObject.getMonth(), mMonthlyObject.getYear(), REQUEST_TO_DO_STAFF_INCENTIVES);
            else
                mNavigator.navigateToStaffIncentiveDoneActivity(this, mHotelID, dept, mMonthlyObject.getStatus(),
                        mMonthlyObject.getMonth(), mMonthlyObject.getYear());
        } else {
            if (mMonthlyObject.getStatus().equals(getString(R.string.status_ongoing))) {
                mNavigator.navigateToStaffIncentiveDoneActivity(this, mHotelID, dept, mMonthlyObject.getStatus(),
                        mMonthlyObject.getMonth(), mMonthlyObject.getYear());
            } else {
                mNoIncentivesViewTitle.setText(getString(R.string.no_incentive_title, dept.getDepartmentName()));
                mNoIncentivesViewSubTitle.setText(getString(R.string.no_incentive_subtext, dept.getDepartmentName(), mMonthlyObject.getMonthText(), mMonthlyObject.getYear()));
                mNoIncentivesView.setVisibility(View.VISIBLE);
                TreeboButton noIncentivesBtn = (TreeboButton) mNoIncentivesView.findViewById(R.id.okay_btn);
                noIncentivesBtn.setOnClickListener(view -> {
                    int index = findDepartmentByID(dept.getDepartmentID());
                    if (index != -1 && mMonthlyObject.getStatus().equals(getString(R.string.status_pending))) {
                        DepartmentObject object = mDepartmentList.get(index);
                        object.setStatus(getString(R.string.status_complete));
                        updateLists(index, object);
                    }
                    mNoIncentivesView.setVisibility(View.GONE);
                });
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(Utils.HOTEL_ID, mHotelID);
        savedInstanceState.putInt(Utils.DEPARTMENT_ID, mDepartmentID);
        savedInstanceState.putParcelable(Utils.MONTHLY_RESPONSE_OBJECT, mMonthlyObject);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_TO_DO_STAFF_INCENTIVES:
                if (resultCode == RESULT_OK) {
                    int deptID = data.getIntExtra(Utils.DEPARTMENT_ID, -1);
                    int doneCount = data.getIntExtra(Utils.TASKS_DONE_COUNT, 0);
                    int index = findDepartmentByID(deptID);
                    if (index != -1) {
                        DepartmentObject object = mDepartmentList.get(index);
                        int pendingCount = object.getTotalStaff() - doneCount;
                        object.setPendingCount(pendingCount);
                        if (pendingCount > 0) {
                            object.setStatus(getString(R.string.paused).toLowerCase());
                        } else if (pendingCount == 0) {
                            object.setStatus(getString(R.string.status_complete));
                        }
                        updateLists(index, object);
                    }
                }
                break;
        }
    }

    private void updateLists(int index, DepartmentObject object) {
        if (object.getStatus().equals(getString(R.string.status_complete))) {
            mDoneDepartmentList.add(object);
            mDepartmentList.remove(index);
            mCompletedCountTv.setText(String.valueOf(mDoneDepartmentList.size()));
            mCompletedText.setVisibility(View.VISIBLE);
            mCompletedCountTv.setVisibility(View.VISIBLE);
            mDoneMonthsRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mDepartmentList.set(index, object);
        }
        if (mDepartmentList.size() == 0) {
            mRxBus.postEvent(new DepartmentIncentivesGivenEvent(mMonthlyObject.getMonth(), mMonthlyObject.getYear()));
        }
        mAdapter.setData(mDepartmentList);
        mDoneAdapter.setData(mDoneDepartmentList);
    }

    private int findDepartmentByID(int depID) {
        int index = -1;
        for (int i = 0; i < mDepartmentList.size(); i++) {
            DepartmentObject object = mDepartmentList.get(i);
            if (object.getDepartmentID() == depID) {
                index = i;
                break;
            }
        }
        return index;
    }
}
