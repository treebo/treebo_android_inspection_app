package com.treebo.prowlapp.flowstaffincentives.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.staffincentives.DepartmentListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class DepartmentListUseCase extends UseCase<DepartmentListResponse> {

    private RestClient mRestClient;
    private int mHotelID;
    private int month;
    private int year;
    private String status;

    public DepartmentListUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setFields(int hotelID, String status, int month, int year) {
        this.mHotelID = hotelID;
        this.status = status;
        this.month = month;
        this.year = year;
    }

    @Override
    protected Observable<DepartmentListResponse> getObservable() {
        return mRestClient.getDepartmentList(mHotelID, status, month, year);
    }
}
