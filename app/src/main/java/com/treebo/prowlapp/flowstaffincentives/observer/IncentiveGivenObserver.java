package com.treebo.prowlapp.flowstaffincentives.observer;

import com.treebo.prowlapp.flowstaffincentives.presenter.ToDoStaffIncentivesPresenter;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.response.staffincentives.StaffObject;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class IncentiveGivenObserver extends BaseObserver<BaseResponse> {

    private ToDoStaffIncentivesPresenter mPresenter;
    private StaffObject mObject;

    public IncentiveGivenObserver(ToDoStaffIncentivesPresenter presenter, UseCase useCase, StaffObject object) {
        super(presenter, useCase);
        mPresenter = presenter;
    }

    @Override
    public void onCompleted() {
        mPresenter.resume();
    }

    @Override
    public void onNext(BaseResponse baseResponse) {
        mPresenter.resume();
        if (!baseResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onIncentiveGivenPostFailure(baseResponse.msg, mObject);
        }
    }
}
