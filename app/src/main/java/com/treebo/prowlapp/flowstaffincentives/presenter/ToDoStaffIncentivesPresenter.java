package com.treebo.prowlapp.flowstaffincentives.presenter;

import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveContract;
import com.treebo.prowlapp.flowstaffincentives.observer.IncentiveGivenObserver;
import com.treebo.prowlapp.flowstaffincentives.observer.OngoingStaffIncentiveListObserver;
import com.treebo.prowlapp.flowstaffincentives.usecase.IncentiveGivenUseCase;
import com.treebo.prowlapp.flowstaffincentives.usecase.OngoingStaffIncentiveListUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.staffincentives.StaffListResponse;
import com.treebo.prowlapp.response.staffincentives.StaffObject;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class ToDoStaffIncentivesPresenter implements StaffIncentiveContract.IToDoStaffIncentivesPresenter {

    private StaffIncentiveContract.IToDoStaffIncentivesView mView;

    private OngoingStaffIncentiveListUseCase mListUseCase;

    private IncentiveGivenUseCase mIncentiveGivenUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (StaffIncentiveContract.IToDoStaffIncentivesView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        mView.hideLoading();
    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void onCreate() {
        mView.setUpRecyclerView();
    }

    @Override
    public void getStaffList(int hotelID, int deptID, int month, int year) {
        mView.showLoading();
        mListUseCase.setFields(hotelID, deptID, "pending", month, year);
        mListUseCase.execute(providesStaffListObserver());
    }

    @Override
    public void onStaffListSuccess(StaffListResponse response) {
        mView.setDataInAdapter(response.getStaffList());
    }

    @Override
    public void onCallFailure(String msg) {
        mView.onStaffListFailure(msg);
    }

    @Override
    public void postIncentiveGiven(int hotelID, int deptID, int month, int year, StaffObject object) {
        mIncentiveGivenUseCase.setField(hotelID, deptID, object.getStaffID(), month, year, object.getReason()
                , object.isIncentiveModified() ? object.getIncentiveGiven() : object.getIncentive());
        mIncentiveGivenUseCase.execute(providesIncentiveGivenObserver(object));
    }

    @Override
    public void onIncentiveGivenPostSuccess() {

    }

    @Override
    public void onIncentiveGivenPostFailure(String msg, StaffObject staffObject) {
        mView.onIncentiveGivenPostFailure(staffObject, msg);
    }

    public void setmIncentiveGivenUseCase(IncentiveGivenUseCase useCase) {
        this.mIncentiveGivenUseCase = useCase;
    }

    public void setListUseCase(OngoingStaffIncentiveListUseCase useCase) {
        this.mListUseCase = useCase;
    }

    public OngoingStaffIncentiveListObserver providesStaffListObserver() {
        return new OngoingStaffIncentiveListObserver(this, mListUseCase);
    }

    public IncentiveGivenObserver providesIncentiveGivenObserver(StaffObject staffObject) {
        return new IncentiveGivenObserver(this, mIncentiveGivenUseCase, staffObject);
    }
}
