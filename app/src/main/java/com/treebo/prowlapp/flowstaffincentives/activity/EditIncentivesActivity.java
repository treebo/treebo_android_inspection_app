package com.treebo.prowlapp.flowstaffincentives.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowstaffincentives.DaggerStaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveComponent;
import com.treebo.prowlapp.response.staffincentives.StaffObject;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class EditIncentivesActivity extends AppCompatActivity {

    private static final int REASON_EMPTY_ERROR = 1;
    private static final int AMOUNT_ERROR = 2;

    @Inject
    public LoginSharedPrefManager mSharedPref;


    private TreeboEditText mAmountEditText;
    private TreeboEditText mReasonEditText;
    private TreeboTextView mAmountTagTv;
    private TreeboTextView mReasonTagTv;
    private SwitchCompat mSwitch;
    private View mAmountErrorView;
    private View mReasonErrorView;
    private TreeboTextView mMaxTv;
    private TreeboTextView mMinTv;

    private StaffObject mStaffObject;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_incentives);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        StaffIncentiveComponent staffIncentiveComponent = DaggerStaffIncentiveComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        staffIncentiveComponent.injectEditIncentiveActivity(this);

        if (savedInstanceState == null) {
            mStaffObject = getIntent().getParcelableExtra(Utils.STAFF_OBJECT);
        } else {
            mStaffObject = savedInstanceState.getParcelable(Utils.STAFF_OBJECT);
        }

        View toolbar = findViewById(R.id.toolbar_update_issue);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        TreeboTextView title = (TreeboTextView) toolbar.findViewById(R.id.toolbar_title_text);
        title.setText(getString(R.string.edit_incentives));
        View crossBtn = toolbar.findViewById(R.id.room_picker_back);
        crossBtn.setOnClickListener(view -> onBackPressed());

        mAmountEditText = (TreeboEditText) findViewById(R.id.amount_edittext);
        mAmountEditText.setText(String.valueOf(mStaffObject.getIncentive()));
        mAmountEditText.addTextChangedListener(new AmountTextWatcher());
        mAmountTagTv = (TreeboTextView) findViewById(R.id.amount_title);
        mReasonEditText = (TreeboEditText) findViewById(R.id.reason_edittext);
        mReasonEditText.setClickable(false);
        mReasonEditText.setEnabled(false);
        mReasonEditText.addTextChangedListener(new ReasonTextWatcher());
        mReasonTagTv = (TreeboTextView) findViewById(R.id.reason_title_text);
        mSwitch = (SwitchCompat) findViewById(R.id.incentive_not_given_switch);
        mSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mReasonEditText.setClickable(true);
                mReasonEditText.setEnabled(true);
                mReasonEditText.setText(" ");
                mAmountEditText.setText("0");
                mAmountEditText.setEnabled(false);
                mAmountEditText.setClickable(false);
            } else {
                mReasonEditText.setClickable(false);
                mReasonEditText.setEnabled(false);
                mReasonEditText.setText("");
                mAmountEditText.setText("");
                mAmountEditText.setEnabled(true);
                mAmountEditText.setClickable(true);
                if (mReasonErrorView.getVisibility() == View.VISIBLE)
                    mReasonErrorView.setVisibility(View.GONE);
            }
        });
        mAmountErrorView = findViewById(R.id.error_tv);
        mReasonErrorView = findViewById(R.id.reason_error_tv);

        float percentMinMax = mSharedPref.getPercentMinMax();
        int incentiveAmount = mStaffObject.getIncentive();
        int maxAmount = incentiveAmount + Math.round(incentiveAmount * percentMinMax / 100);
        int minAmount = incentiveAmount - Math.round(incentiveAmount * percentMinMax / 100);
        mMaxTv = (TreeboTextView) findViewById(R.id.max_rupee_tv);
        mMaxTv.setText(getString(R.string.max_rupee, (maxAmount >= 0 ? maxAmount : 0)));
        mMinTv = (TreeboTextView) findViewById(R.id.min_rupee_tv);
        mMinTv.setText(getString(R.string.min_rupee, (minAmount >= 0 ? minAmount : 0)));

        TreeboButton doneBtn = (TreeboButton) findViewById(R.id.done_btn);
        doneBtn.setOnClickListener(view -> {
            if (mSwitch.isChecked()) {
                if (TextUtils.isEmpty(mReasonEditText.getText())) {
                    setError(REASON_EMPTY_ERROR, true);
                } else {
                    setResultAndFinish();
                }
            } else {
                if (TextUtils.isEmpty(mAmountEditText.getText()) || Integer.valueOf(mAmountEditText.getText().toString()) > maxAmount
                        || Integer.valueOf(mAmountEditText.getText().toString()) < minAmount) {
                    setError(AMOUNT_ERROR, true);
                } else {
                    setResultAndFinish();
                }
            }
        });
    }

    private void setResultAndFinish() {
        mStaffObject.setReason(mReasonEditText.getText().toString());
        mStaffObject.setIncentiveModified(true);
        mStaffObject.setIncentiveGiven(Integer.valueOf(mAmountEditText.getText().toString()));
        Intent intent = new Intent();
        intent.putExtra(Utils.STAFF_OBJECT, mStaffObject);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private void setError(int errorType, boolean isError) {
        int resID = isError ? R.drawable.bg_rectangle_border_red : R.drawable.bg_edittext_selector;
        switch (errorType) {
            case REASON_EMPTY_ERROR:
                mReasonErrorView.setVisibility(isError ? View.VISIBLE : View.GONE);
                mReasonEditText.setBackground(ContextCompat.getDrawable(this, resID));
                break;

            case AMOUNT_ERROR:
                mAmountErrorView.setVisibility(isError ? View.VISIBLE : View.GONE);
                mAmountEditText.setBackground(ContextCompat.getDrawable(this, resID));
                break;

            default:
                break;
        }
    }

    private void setEditTextBackGround(boolean enabled, TreeboEditText textView, TreeboTextView title) {
        title.setVisibility(!enabled ? View.GONE : View.VISIBLE);
        int resID = !enabled ? R.drawable.bg_rectangle_border_gray : R.drawable.bg_rectangle_border_green;
        textView.setBackground(ContextCompat.getDrawable(this, resID));
    }


    private class AmountTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() == 0) {
                mAmountEditText.setHint(getString(R.string.amount));
                setEditTextBackGround(false, mAmountEditText, mAmountTagTv);
            } else if (editable.length() > 0 && editable.length() < 2) {
                setError(AMOUNT_ERROR, false);
                setEditTextBackGround(true, mAmountEditText, mAmountTagTv);
            } else {
                setError(AMOUNT_ERROR, false);
                setEditTextBackGround(true, mAmountEditText, mAmountTagTv);
            }
        }
    }

    private class ReasonTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() == 0) {
                mReasonEditText.setHint(getString(R.string.reason));
                setEditTextBackGround(false, mReasonEditText, mReasonTagTv);
            } else if (editable.length() > 0 && editable.length() < 2) {
                setError(REASON_EMPTY_ERROR, false);
                setEditTextBackGround(true, mReasonEditText, mReasonTagTv);
            } else {
                setError(REASON_EMPTY_ERROR, false);
                setEditTextBackGround(true, mReasonEditText, mReasonTagTv);
            }
        }
    }
}
