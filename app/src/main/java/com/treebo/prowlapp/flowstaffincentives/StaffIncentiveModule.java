package com.treebo.prowlapp.flowstaffincentives;

import com.treebo.prowlapp.flowstaffincentives.presenter.DepartmentListPresenter;
import com.treebo.prowlapp.flowstaffincentives.presenter.MonthlyListPresenter;
import com.treebo.prowlapp.flowstaffincentives.presenter.StaffIncentivesDonePresenter;
import com.treebo.prowlapp.flowstaffincentives.presenter.ToDoStaffIncentivesPresenter;
import com.treebo.prowlapp.flowstaffincentives.usecase.DepartmentListUseCase;
import com.treebo.prowlapp.flowstaffincentives.usecase.IncentiveGivenUseCase;
import com.treebo.prowlapp.flowstaffincentives.usecase.MonthlyIncentiveListUseCase;
import com.treebo.prowlapp.flowstaffincentives.usecase.OngoingStaffIncentiveListUseCase;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by abhisheknair on 28/02/17.
 */
@Module
public class StaffIncentiveModule {

    @Provides
    RestClient providesRestClient() {
        return new RestClient();
    }

    @Provides
    MonthlyListPresenter providesDashboardPresenter() {
        return new MonthlyListPresenter();
    }

    @Provides
    DepartmentListPresenter providesDepartmentListPresenter() {
        return new DepartmentListPresenter();
    }

    @Provides
    StaffIncentivesDonePresenter providesStaffIncentivesDonePresenter() {
        return new StaffIncentivesDonePresenter();
    }

    @Provides
    ToDoStaffIncentivesPresenter providesToDoStaffIncentivesPresenter() {
        return new ToDoStaffIncentivesPresenter();
    }

    @Provides
    OngoingStaffIncentiveListUseCase providesOngoingStaffIncentivesUseCase(RestClient restClient) {
        return new OngoingStaffIncentiveListUseCase(restClient);
    }

    @Provides
    DepartmentListUseCase providesDepartmentListUseCase(RestClient restClient) {
        return new DepartmentListUseCase(restClient);
    }

    @Provides
    MonthlyIncentiveListUseCase providesMonthlyIncentiveListUseCase(RestClient restClient) {
        return new MonthlyIncentiveListUseCase(restClient);
    }

    @Provides
    IncentiveGivenUseCase providesIncentiveGivenUseCase(RestClient restClient) {
        return new IncentiveGivenUseCase(restClient);
    }
}
