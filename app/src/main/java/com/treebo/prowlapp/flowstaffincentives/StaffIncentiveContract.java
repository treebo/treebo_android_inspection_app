package com.treebo.prowlapp.flowstaffincentives;

import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.staffincentives.DepartmentObject;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.response.staffincentives.StaffListResponse;
import com.treebo.prowlapp.response.staffincentives.StaffObject;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 28/02/17.
 */

public interface StaffIncentiveContract {

    interface IMonthlyListView extends BaseView {
        void onLoadListSuccess(ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> data);

        void onLoadListFailure(String msg);

        void setUpRecyclerView();

        void setUpToolbar();

        void showBottomSheet();
    }

    interface IMonthlyListPresenter extends BasePresenter {
        void onCreate();

        void getIncentives(int hotelID);

        void setAdapterData(ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> data);

        void openBottomSheet();

        void onGetListFailure(String msg);
    }

    interface IDepartmentListView extends BaseView {
        void onLoadListSuccess(ArrayList<DepartmentObject> data);

        void onLoadListFailure(String msg);

        void setUpRecyclerView();

        void setUpToolbar();
    }

    interface IDepartmentListPresenter extends BasePresenter {
        void onCreate();

        void getDepartmentIncentives(int hotelID, String status, int month, int year);

        void setAdapterData(ArrayList<DepartmentObject> data);

        void onGetListFailure(String msg);
    }

    interface IToDoStaffIncentivesView extends BaseView {

        void setDataInAdapter(ArrayList<StaffObject> list);

        void setUpRecyclerView();

        void onStaffListFailure(String msg);

        void onIncentiveGivenPostFailure(StaffObject object, String msg);

    }

    interface IToDoStaffIncentivesPresenter extends BasePresenter {
        void onCreate();

        void getStaffList(int hotelID, int deptID, int month, int year);

        void onStaffListSuccess(StaffListResponse response);

        void onCallFailure(String msg);

        void postIncentiveGiven(int hotelID, int deptID, int month, int year, StaffObject object);

        void onIncentiveGivenPostSuccess();

        void onIncentiveGivenPostFailure(String msg, StaffObject staffObject);
    }

    interface IStaffIncentivesDoneView extends BaseView {

        void setUpToolBar();

        void setDataInAdapter(ArrayList<StaffObject> list);

        void setUpRecyclerView();

        void onStaffListFailure(String msg);

    }

    interface IStaffIncentivesDonePresenter extends BasePresenter {
        void onCreate();

        void getStaffList(int hotelID, int deptID, String status, int month, int year);

        void onStaffListSuccess(StaffListResponse response);

        void onCallFailure(String msg);
    }
}
