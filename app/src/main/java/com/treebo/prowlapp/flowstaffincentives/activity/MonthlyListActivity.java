package com.treebo.prowlapp.flowstaffincentives.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.DepartmentIncentivesGivenEvent;
import com.treebo.prowlapp.flowstaffincentives.DaggerStaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveContract;
import com.treebo.prowlapp.flowstaffincentives.adapter.MonthlyIncentiveListAdapter;
import com.treebo.prowlapp.flowstaffincentives.fragment.PastMonthBottomSheetFragment;
import com.treebo.prowlapp.flowstaffincentives.presenter.MonthlyListPresenter;
import com.treebo.prowlapp.flowstaffincentives.usecase.MonthlyIncentiveListUseCase;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 28/02/17.
 */

public class MonthlyListActivity extends AppCompatActivity
        implements StaffIncentiveContract.IMonthlyListView,
        PastMonthBottomSheetFragment.onMonthClickedActionListener,
        MonthlyIncentiveListAdapter.monthClickListener {

    @Inject
    public Navigator mNavigator;

    @Inject
    public MonthlyListPresenter mPresenter;

    @Inject
    public MonthlyIncentiveListUseCase mUseCase;

    @Inject
    public RxBus mRxBus;

    private TreeboTextView mPastMonths;
    private RecyclerView mMonthlyIncentivesRecyclerView;
    private View mLoadingView;
    private TreeboTextView mTitle;

    private View mCalculationsPendingView;
    private TreeboTextView mPendingViewTitle;
    private TreeboTextView mPendingViewText;
    private TreeboButton mPendingViewOkay;

    private MonthlyIncentiveListAdapter mAdapter;
    private ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> mMonthList;
    private PastMonthBottomSheetFragment mPastMonthBottomSheetFragment;
    private int mHotelID;

    private CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_incentives);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.activity_gray_background));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        StaffIncentiveComponent staffIncentiveComponent = DaggerStaffIncentiveComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        staffIncentiveComponent.injectMonthlyListActivity(this);

        if (savedInstanceState == null) {
            mHotelID = getIntent().getIntExtra(Utils.HOTEL_ID, -1);
        } else {
            mHotelID = savedInstanceState.getInt(Utils.HOTEL_ID);
        }

        mPastMonths = (TreeboTextView) findViewById(R.id.past_months_tv);
        mPastMonths.setVisibility(View.VISIBLE);
        mPastMonths.setOnClickListener(view -> {
            mPresenter.openBottomSheet();
        });
        mMonthlyIncentivesRecyclerView = (RecyclerView) findViewById(R.id.past_months_rv);
        View backBtn = findViewById(R.id.ic_toolbar_audit_back);
        backBtn.setOnClickListener(view -> {
            onBackPressed();
        });
        mLoadingView = findViewById(R.id.loader_layout);
        mCalculationsPendingView = findViewById(R.id.pending_calculations_layout);
        mPendingViewTitle = (TreeboTextView) mCalculationsPendingView.findViewById(R.id.empty_title);
        mPendingViewText = (TreeboTextView) mCalculationsPendingView.findViewById(R.id.empty_text);
        mPendingViewOkay = (TreeboButton) mCalculationsPendingView.findViewById(R.id.okay_btn);
        mPendingViewOkay.setOnClickListener(view -> {
            mCalculationsPendingView.setVisibility(View.GONE);
        });
        mTitle = (TreeboTextView) findViewById(R.id.toolbar_audit_title);

        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof DepartmentIncentivesGivenEvent) {
                int month = ((DepartmentIncentivesGivenEvent) event).getMonth();
                int year = ((DepartmentIncentivesGivenEvent) event).getYear();
                for (int i = 0; i < mMonthList.size(); i++) {
                    MonthlyIncentiveListResponse.MonthlyIncentiveListObject object =
                            mMonthList.get(i);
                    if (object.getMonth() == month && object.getYear() == year) {
                        mMonthList.remove(i);
                        break;
                    }
                }
                mAdapter.setData(mMonthList);

            }
        }));
        mPresenter.onCreate();
        mPresenter.getIncentives(mHotelID);
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setUseCase(mUseCase);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void onLoadListSuccess(ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> data) {
        mMonthList = data;
        mAdapter.setData(data);
    }

    @Override
    public void onLoadListFailure(String msg) {
        SnackbarUtils.show(findViewById(android.R.id.content), msg);
    }

    @Override
    public void setUpRecyclerView() {
        mAdapter = new MonthlyIncentiveListAdapter(this, mMonthList, this);
        mMonthlyIncentivesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mMonthlyIncentivesRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setUpToolbar() {
        mTitle.setText(getString(R.string.title_monthly_incentives));
    }

    @Override
    public void showBottomSheet() {
        int earliestMonthIndex = 0;
        int earliestYearInList = mMonthList.get(earliestMonthIndex).getYear();
        int earliestMonthInList = mMonthList.get(earliestMonthIndex).getMonth();
        for (int i = 0; i < mMonthList.size(); i++) {
            MonthlyIncentiveListResponse.MonthlyIncentiveListObject object = mMonthList.get(i);
            if (object.getYear() < earliestYearInList) {
                earliestYearInList = object.getYear();
            }
            if (object.getYear() == earliestYearInList && object.getMonth() < earliestMonthInList)
                earliestMonthIndex = i;
        }
        ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> last5monthsList
                = Utils.getPastMonths(mMonthList.get(earliestMonthIndex).getMonth() - 1, mMonthList.get(earliestMonthIndex).getYear());
        if (last5monthsList.size() > 0) {
            mPastMonthBottomSheetFragment = PastMonthBottomSheetFragment.newInstance(last5monthsList);
            mPastMonthBottomSheetFragment.setListener(this);
            mPastMonthBottomSheetFragment.show(getSupportFragmentManager(), null);
        } else {
            SnackbarUtils.show(findViewById(android.R.id.content), getString(R.string.last_5_months_error));
        }
    }

    @Override
    public void openMonthSpecificIncentivesFromBottomSheet(MonthlyIncentiveListResponse.MonthlyIncentiveListObject month) {
        mNavigator.navigateToMonthlyDepartmentListActivity(this, month, mHotelID);
    }

    @Override
    public void openMonthIncentives(MonthlyIncentiveListResponse.MonthlyIncentiveListObject object) {
        if (object.getStatus().toLowerCase().equals(getString(R.string.status_locked))) {
            mPendingViewText.setText(getString(R.string.pending_text, object.getMonthText()));
            mPendingViewTitle.setText(getString(R.string.pending_title, object.getMonthText()));
            mCalculationsPendingView.setVisibility(View.VISIBLE);
        } else {
            mNavigator.navigateToMonthlyDepartmentListActivity(this, object, mHotelID);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(Utils.HOTEL_ID, mHotelID);
        super.onSaveInstanceState(savedInstanceState);
    }
}
