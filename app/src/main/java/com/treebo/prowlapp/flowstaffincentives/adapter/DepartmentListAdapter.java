package com.treebo.prowlapp.flowstaffincentives.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.staffincentives.DepartmentObject;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 01/03/17.
 */

public class DepartmentListAdapter extends RecyclerView.Adapter<DepartmentListAdapter.DepartmentViewHolder> {

    private Context mContext;
    private onDepartmentClickedListener mListener;
    private String mStatus;
    private ArrayList<DepartmentObject> mList = new ArrayList<>();

    public DepartmentListAdapter(Context context, ArrayList<DepartmentObject> list,
                                 onDepartmentClickedListener listener, String status) {
        this.mContext = context;
        this.mList = list;
        this.mListener = listener;
        this.mStatus = status;
    }

    public void setData(ArrayList<DepartmentObject> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    @Override
    public DepartmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_department,
                parent, false);
        return new DepartmentViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(DepartmentViewHolder holder, int position) {
        DepartmentObject object = mList.get(position);
        holder.departmentObject = object;
        holder.departmentName.setText(object.getDepartmentName());
        holder.departmentIcon.setImageResource(getDepartmentIcon(object.getDepartmentName()));
        String tailText = mContext.getResources().getQuantityString(R.plurals.staff_member, object.getTotalStaff());
        holder.descriptionText.setText(mContext.getString(R.string.total_staff_for_incentives, object.getTotalStaff(), tailText));
        if (object.getStatus().toLowerCase().equals(mContext.getString(R.string.paused).toLowerCase())) {
            holder.pauseIcon.setVisibility(View.VISIBLE);
            holder.pauseText.setVisibility(View.VISIBLE);
            holder.descriptionText.setVisibility(View.GONE);
        } else {
            holder.pauseIcon.setVisibility(View.GONE);
            holder.pauseText.setVisibility(View.GONE);
            holder.descriptionText.setVisibility(View.VISIBLE);
        }
        if (mStatus.equals(mContext.getString(R.string.status_ongoing))) {
            holder.deltaTv.setText(mContext.getString(R.string.rupee_string,
                    (object.getDailyDelta() < 0) ? (object.getDailyDelta() * -1) : object.getDailyDelta()));
            holder.deltaTv.setTextColor(Utils.getDeltaColor(mContext, object.getDailyDelta()));
            if (object.getDailyDelta() == 0) {
                holder.deltaIv.setVisibility(View.GONE);
            } else {
                holder.deltaIv.setColorFilter(Utils.getDeltaColor(mContext, object.getDailyDelta()));
                holder.deltaIv.setImageResource(object.getDailyDelta() < 0 ? R.drawable.ic_arrow_downward
                        : R.drawable.ic_arrow_upward);
            }
            holder.deltaView.setVisibility(View.VISIBLE);
        } else {
            holder.deltaView.setVisibility(View.GONE);
        }
    }

    private int getDepartmentIcon(String departmentName) {
        switch (departmentName) {
            case "Front Office":
                return R.drawable.ic_front_office;

            case "Food & Beverages":
                return R.drawable.ic_fnb_department;

            case "Hotel Manager":
                return R.drawable.ic_hotel_manager;

            case "House Keeping":
            default:
                return R.drawable.ic_house_keeping;

        }
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        else
            return mList.size();
    }

    public class DepartmentViewHolder extends RecyclerView.ViewHolder {

        DepartmentObject departmentObject;
        TreeboTextView departmentName;
        TreeboTextView descriptionText;
        ImageView pauseIcon;
        View pauseText;
        ImageView departmentIcon;
        TreeboTextView deltaTv;
        ImageView deltaIv;
        View deltaView;

        public DepartmentViewHolder(View itemView, onDepartmentClickedListener listener) {
            super(itemView);
            departmentIcon = (ImageView) itemView.findViewById(R.id.department_card_iv);
            departmentName = (TreeboTextView) itemView.findViewById(R.id.department_card_header);
            descriptionText = (TreeboTextView) itemView.findViewById(R.id.description_tv);
            pauseIcon = (ImageView) itemView.findViewById(R.id.ic_pause);
            pauseText = itemView.findViewById(R.id.pause_text);
            deltaView = itemView.findViewById(R.id.delta_rl);
            deltaIv = (ImageView) itemView.findViewById(R.id.delta_iv);
            deltaTv = (TreeboTextView) itemView.findViewById(R.id.delta_tv);
            itemView.setOnClickListener(view -> {
                listener.onDepartmentClicked(departmentObject);
            });
        }
    }

    public interface onDepartmentClickedListener {
        void onDepartmentClicked(DepartmentObject dept);
    }
}
