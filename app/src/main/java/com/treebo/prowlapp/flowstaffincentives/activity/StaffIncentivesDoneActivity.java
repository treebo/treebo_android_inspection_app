package com.treebo.prowlapp.flowstaffincentives.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowstaffincentives.DaggerStaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveContract;
import com.treebo.prowlapp.flowstaffincentives.adapter.ViewOnlyIncentiveTaskStaffListAdapter;
import com.treebo.prowlapp.flowstaffincentives.presenter.StaffIncentivesDonePresenter;
import com.treebo.prowlapp.flowstaffincentives.usecase.OngoingStaffIncentiveListUseCase;
import com.treebo.prowlapp.response.staffincentives.DepartmentObject;
import com.treebo.prowlapp.response.staffincentives.StaffObject;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.text.DateFormatSymbols;
import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class StaffIncentivesDoneActivity extends AppCompatActivity implements StaffIncentiveContract.IStaffIncentivesDoneView {

    @Inject
    public StaffIncentivesDonePresenter mPresenter;

    @Inject
    public OngoingStaffIncentiveListUseCase mUseCase;

    private int mHotelID;
    private int mMonth;
    private int mYear;
    private String mStatus;
    private DepartmentObject mDepartmentObject;

    private View mLoadingView;
    private RecyclerView mRecyclerView;
    private ViewOnlyIncentiveTaskStaffListAdapter mAdapter;
    private ArrayList<StaffObject> mList;

    private Toolbar mToolbar;
    private TreeboTextView mDepartmentNameTv;
    private TreeboTextView mCurrentMonth;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_month_incentives);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }

        StaffIncentiveComponent staffIncentiveComponent = DaggerStaffIncentiveComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        staffIncentiveComponent.injectStaffIncentivesDoneActivity(this);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mHotelID = intent.getIntExtra(Utils.HOTEL_ID, -1);
            mDepartmentObject = intent.getParcelableExtra(Utils.DEPARTMENT_OBJECT);
            mMonth = intent.getIntExtra(Utils.REQUIRED_MONTH, 1);
            mYear = intent.getIntExtra(Utils.REQUIRED_YEAR, -1);
            mStatus = intent.getStringExtra(Utils.DEPARTMENT_INCENTIVE_STATUS);
        } else {
            mHotelID = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            mDepartmentObject = savedInstanceState.getParcelable(Utils.DEPARTMENT_OBJECT);
            mMonth = savedInstanceState.getInt(Utils.REQUIRED_MONTH, -1);
            mYear = savedInstanceState.getInt(Utils.REQUIRED_YEAR, -1);
            mStatus = savedInstanceState.getString(Utils.DEPARTMENT_INCENTIVE_STATUS);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        mCurrentMonth = (TreeboTextView) findViewById(R.id.month_name_title);
        mDepartmentNameTv = (TreeboTextView) findViewById(R.id.dept_name_title);
        if (mStatus.equals(getString(R.string.status_ongoing))) {
            TreeboTextView dailyDeltaTv = (TreeboTextView) findViewById(R.id.daily_delta_tv);
            ImageView dailyDeltaIv = (ImageView) findViewById(R.id.daily_delta_iv);
            TreeboTextView dailyText = (TreeboTextView) findViewById(R.id.daily_delta_text);
            TreeboTextView monthlyDeltaTv = (TreeboTextView) findViewById(R.id.monthly_delta_tv);
            ImageView monthlyDeltaIv = (ImageView) findViewById(R.id.monthly_delta_iv);
            TreeboTextView monthlyText = (TreeboTextView) findViewById(R.id.monthly_delta_text);
            monthlyText.setText(DateUtils.getCurrentMonthString());
            setUpToolbarDeltaFields(dailyDeltaTv, dailyDeltaIv, dailyText, mDepartmentObject.getDailyDelta());
            setUpToolbarDeltaFields(monthlyDeltaTv, monthlyDeltaIv, monthlyText, mDepartmentObject.getMonthlyDelta());

        }
        mLoadingView = findViewById(R.id.loader_layout);
        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));
        mRecyclerView = (RecyclerView) findViewById(R.id.previous_month_incentives_rv);

        mPresenter.onCreate();
        mPresenter.getStaffList(mHotelID, mDepartmentObject.getDepartmentID(), mStatus, mMonth, mYear);

    }

    private void setUpToolbarDeltaFields(TreeboTextView deltaTv, ImageView deltaIv,
                                         TreeboTextView deltaText, int deltaValue) {
        deltaTv.setText(getString(R.string.rupee_string,
                (deltaValue < 0) ? (deltaValue * -1) : deltaValue));
        deltaTv.setTextColor(Utils.getDeltaColor(this, deltaValue));
        if (deltaValue == 0) {
            deltaIv.setVisibility(View.GONE);
        } else {
            deltaIv.setColorFilter(Utils.getDeltaColor(this, deltaValue));
            deltaIv.setImageDrawable(ContextCompat.getDrawable(this,
                    deltaValue < 0 ? R.drawable.ic_arrow_downward : R.drawable.ic_arrow_upward));
        }
        deltaIv.setVisibility(View.VISIBLE);
        deltaTv.setVisibility(View.VISIBLE);
        deltaText.setVisibility(View.VISIBLE);
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setUseCase(mUseCase);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void setUpToolBar() {
        mDepartmentNameTv.setText(mDepartmentObject.getDepartmentName());
        if (mStatus.equals(getString(R.string.status_ongoing))) {
            mCurrentMonth.setText(DateUtils.getTodaysDisplayDate());
        } else {
            mCurrentMonth.setText(new DateFormatSymbols().getMonths()[mMonth - 1]);
        }
    }

    @Override
    public void setDataInAdapter(ArrayList<StaffObject> list) {
        mList = list;
        mAdapter.setData(mList);
    }

    @Override
    public void setUpRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new ViewOnlyIncentiveTaskStaffListAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStaffListFailure(String msg) {
        SnackbarUtils.show(findViewById(android.R.id.content), msg);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(Utils.HOTEL_ID, mHotelID);
        savedInstanceState.putParcelable(Utils.DEPARTMENT_OBJECT, mDepartmentObject);
        savedInstanceState.putInt(Utils.REQUIRED_MONTH, mMonth);
        savedInstanceState.putInt(Utils.REQUIRED_YEAR, mYear);
        savedInstanceState.putString(Utils.DEPARTMENT_INCENTIVE_STATUS, mStatus);
        super.onSaveInstanceState(savedInstanceState);
    }
}
