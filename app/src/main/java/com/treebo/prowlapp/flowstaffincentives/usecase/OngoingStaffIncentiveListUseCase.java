package com.treebo.prowlapp.flowstaffincentives.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.staffincentives.StaffListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class OngoingStaffIncentiveListUseCase extends UseCase<StaffListResponse> {

    private RestClient mRestClient;
    private int mHotelID;
    private int mDeptID;
    private String mType;
    private int mMonth;
    private int mYear;

    public OngoingStaffIncentiveListUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setFields(int hotelId, int departmentID, String type, int month, int year) {
        mHotelID = hotelId;
        mDeptID = departmentID;
        mType = type;
        mMonth = month;
        mYear = year;
    }

    @Override
    protected Observable<StaffListResponse> getObservable() {
        return mRestClient.getOngoingStaffList(mHotelID, mDeptID, mType, mMonth, mYear);
    }
}
