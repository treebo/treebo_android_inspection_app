package com.treebo.prowlapp.flowstaffincentives.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 28/02/17.
 */

public class MonthlyIncentiveListAdapter extends RecyclerView.Adapter<MonthlyIncentiveListAdapter.MonthlyIncentiveViewHolder> {

    private ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> mList = new ArrayList<>();
    private Context mContext;
    private monthClickListener mListener;

    public MonthlyIncentiveListAdapter(Context context,
                                       ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> list,
                                       monthClickListener listener) {
        this.mContext = context;
        this.mList = list;
        this.mListener = listener;
    }

    public void setData(ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    @Override
    public MonthlyIncentiveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_monthly_incentives, parent, false);
        return new MonthlyIncentiveViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(MonthlyIncentiveViewHolder holder, int position) {
        MonthlyIncentiveListResponse.MonthlyIncentiveListObject object = mList.get(position);
        holder.monthObject = object;
        holder.month.setText(object.getMonthText().length() > 3 ? object.getMonthText().substring(0, 3) : object.getMonthText());
        holder.year.setText(String.valueOf(object.getYear()));
        holder.description.setText(object.getText());
        holder.description.setTextColor(ContextCompat.getColor(mContext, object.getColor()));
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        else
            return mList.size();
    }

    protected class MonthlyIncentiveViewHolder extends RecyclerView.ViewHolder {

        MonthlyIncentiveListResponse.MonthlyIncentiveListObject monthObject;
        TreeboTextView month;
        TreeboTextView year;
        TreeboTextView description;

        public MonthlyIncentiveViewHolder(View itemView, monthClickListener listener) {
            super(itemView);
            month = (TreeboTextView) itemView.findViewById(R.id.month_name_tv);
            year = (TreeboTextView) itemView.findViewById(R.id.year_name_tv);
            description = (TreeboTextView) itemView.findViewById(R.id.description_tv);
            itemView.setOnClickListener(view -> {
                listener.openMonthIncentives(monthObject);
            });
        }
    }

    public interface monthClickListener {
        void openMonthIncentives(MonthlyIncentiveListResponse.MonthlyIncentiveListObject object);
    }
}
