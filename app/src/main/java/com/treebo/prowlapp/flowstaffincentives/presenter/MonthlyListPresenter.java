package com.treebo.prowlapp.flowstaffincentives.presenter;

import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveContract;
import com.treebo.prowlapp.flowstaffincentives.observer.MonthlyIncentiveListObserver;
import com.treebo.prowlapp.flowstaffincentives.usecase.MonthlyIncentiveListUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 28/02/17.
 */

public class MonthlyListPresenter implements StaffIncentiveContract.IMonthlyListPresenter {

    private StaffIncentiveContract.IMonthlyListView mView;
    private MonthlyIncentiveListUseCase mUseCase;

    @Override
    public void onCreate() {
        mView.setUpToolbar();
        mView.setUpRecyclerView();
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (StaffIncentiveContract.IMonthlyListView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void openBottomSheet() {
        mView.showBottomSheet();
    }

    @Override
    public void onGetListFailure(String msg) {
        mView.onLoadListFailure(msg);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        mView.hideLoading();
    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void getIncentives(int hotelID) {
        mView.showLoading();
        mUseCase.setFields(hotelID);
        mUseCase.execute(providesStaffListObserver());
    }

    @Override
    public void setAdapterData(ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> data) {
        mView.onLoadListSuccess(data);
    }

    public void setUseCase(MonthlyIncentiveListUseCase mRoomSingleUse) {
        this.mUseCase = mRoomSingleUse;
    }

    public MonthlyIncentiveListObserver providesStaffListObserver() {
        return new MonthlyIncentiveListObserver(this, mUseCase);
    }
}
