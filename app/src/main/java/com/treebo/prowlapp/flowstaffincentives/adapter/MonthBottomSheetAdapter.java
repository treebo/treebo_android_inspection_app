package com.treebo.prowlapp.flowstaffincentives.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 01/03/17.
 */

public class MonthBottomSheetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> mSortByList;
    private Context mContext;
    private onMonthClicked mListener;

    public MonthBottomSheetAdapter(Context context, ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> list, onMonthClicked listener) {
        this.mContext = context;
        this.mSortByList = list;
        this.mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sort_by,
                parent, false);
        return new MonthViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MonthlyIncentiveListResponse.MonthlyIncentiveListObject month = mSortByList.get(position);
        ((MonthViewHolder) holder).month = month;
        ((MonthViewHolder) holder).text.setText(TextUtils.concat(month.getMonthText(), " ", String.valueOf(month.getYear())));

    }

    @Override
    public int getItemCount() {
        return mSortByList.size();
    }


    private class MonthViewHolder extends RecyclerView.ViewHolder {

        private MonthlyIncentiveListResponse.MonthlyIncentiveListObject month;
        private TreeboTextView text;

        public MonthViewHolder(View itemView, onMonthClicked listener) {
            super(itemView);
            text = (TreeboTextView) itemView.findViewById(R.id.sort_by_title_tv);
            text.setGravity(Gravity.START);
            itemView.setOnClickListener(view -> {
                listener.goToSelectedMonth(month);

            });
        }
    }

    public interface onMonthClicked {
        void goToSelectedMonth(MonthlyIncentiveListResponse.MonthlyIncentiveListObject month);
    }
}
