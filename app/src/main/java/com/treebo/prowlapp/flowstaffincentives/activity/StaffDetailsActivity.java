package com.treebo.prowlapp.flowstaffincentives.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowstaffincentives.DaggerStaffIncentiveComponent;
import com.treebo.prowlapp.flowstaffincentives.StaffIncentiveComponent;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.response.staffincentives.StaffObject;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.text.DateFormatSymbols;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 06/03/17.
 */

public class StaffDetailsActivity extends AppCompatActivity {

    private static final int REQUEST_EDIT = 170;

    @Inject
    public Navigator mNavigator;

    @Inject
    public LoginSharedPrefManager mSharedPrefs;

    private StaffObject mStaffDetailsObject;
    private int mCurrentMonth;
    private View mSuccessView;
    private int mHotelID;

    private boolean isUndoClicked;
    private boolean isGivenClicked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_details);
        setToolbarColor(R.color.white);

        StaffIncentiveComponent staffIncentiveComponent = DaggerStaffIncentiveComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        staffIncentiveComponent.injectStaffDetailsActivity(this);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mHotelID = intent.getIntExtra(Utils.HOTEL_ID, 0);
            mStaffDetailsObject = intent.getParcelableExtra(Utils.STAFF_OBJECT);
            mCurrentMonth = intent.getIntExtra(Utils.REQUIRED_MONTH, 1);
        } else {
            mHotelID = savedInstanceState.getInt(Utils.HOTEL_ID);
            mStaffDetailsObject = savedInstanceState.getParcelable(Utils.STAFF_OBJECT);
            mCurrentMonth = savedInstanceState.getInt(Utils.REQUIRED_MONTH, 1);
        }
        RolesAndUserPermissions.PermissionList userPermissions = mSharedPrefs.getUserPermissionList(mHotelID);

        ImageView profilePic = (ImageView) findViewById(R.id.ic_personal_image);
        if (!TextUtils.isEmpty(mStaffDetailsObject.getStaffImageUrl())) {
            Utils.loadRoundedImageUsingGlide(this, mStaffDetailsObject.getStaffImageUrl(),
                    profilePic, R.drawable.ic_person_placeholder);
        }

        View crossBtn = findViewById(R.id.ic_close);
        crossBtn.setOnClickListener(view -> {
            onBackPressed();
        });

        TreeboTextView name = (TreeboTextView) findViewById(R.id.name_tv);
        name.setText(mStaffDetailsObject.getStaffName());

        TreeboTextView siaText = (TreeboTextView) findViewById(R.id.sia_text);
        siaText.setText(getString(R.string.sia_string, mStaffDetailsObject.getStartingIncentive()));

        TreeboTextView monthText = (TreeboTextView) findViewById(R.id.total_month_tv);
        String month = new DateFormatSymbols().getMonths()[mCurrentMonth - 1];
        monthText.setText(getString(R.string.month_string, month.substring(0, 3)));

        TreeboTextView monthTotalTv = (TreeboTextView) findViewById(R.id.total_amount_tv);
        monthTotalTv.setText(getString(R.string.rupee_string, mStaffDetailsObject.getIncentive()));
        monthTotalTv.setTextColor(ContextCompat.getColor(this, mStaffDetailsObject.getColor()));

        TreeboTextView historyTv = (TreeboTextView) findViewById(R.id.month_history_tv);
        String historyListString = "";
        for (StaffObject.StaffHistory history : mStaffDetailsObject.getHistoryArrayList()) {
            historyListString = history.getMonth() + " " + getString(R.string.rupee_string, history.getIncentiveGiven()) + "  ";
        }
        historyTv.setText(historyListString);

        mSuccessView = findViewById(R.id.audit_success_layout);
        TreeboTextView successText = (TreeboTextView) mSuccessView.findViewById(R.id.audit_successfully_submitted);
        successText.setText(getString(R.string.incentive_given_successfully, mStaffDetailsObject.getStaffName()));
        View undoBtn = mSuccessView.findViewById(R.id.undo_btn);
        undoBtn.setVisibility(View.VISIBLE);
        undoBtn.setOnClickListener(view -> {
            isUndoClicked = true;
        });

        TreeboButton editBtn = (TreeboButton) findViewById(R.id.edit_btn);
        editBtn.setOnClickListener(view -> {
            mNavigator.navigateToEditIncentiveActivity(this, mStaffDetailsObject, REQUEST_EDIT);
        });

        TreeboButton givenBtn = (TreeboButton) findViewById(R.id.given_btn);
        givenBtn.setOnClickListener(view -> {
            isGivenClicked = true;
            mSuccessView.setVisibility(View.VISIBLE);
            setToolbarColor(R.color.green_color_primary);
            givenBtn.setVisibility(View.GONE);
            editBtn.setVisibility(View.GONE);
            new Handler().postDelayed(() -> {
                if (!isUndoClicked) {
                    Intent intent = new Intent();
                    intent.putExtra(Utils.STAFF_OBJECT, mStaffDetailsObject);
                    setResult(Utils.RESULT_INCENTIVE_GIVEN, intent);
                    finish();
                    overridePendingTransition(R.anim.no_change, R.anim.slide_down);
                } else {
                    mSuccessView.setVisibility(View.GONE);
                    givenBtn.setVisibility(View.VISIBLE);
                    editBtn.setVisibility(View.VISIBLE);
                    setToolbarColor(R.color.white);
                    isGivenClicked = false;
                }
                isUndoClicked = false;
            }, TimeUnit.SECONDS.toMillis(2));
        });

        editBtn.setVisibility(userPermissions.isStaffIncentivesEditEnabled() ? View.VISIBLE : View.GONE);
        givenBtn.setVisibility(userPermissions.isStaffIncentivesDistributeEnabled() ? View.VISIBLE : View.GONE);

    }

    private void setToolbarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    public void onBackPressed() {
        if (!isGivenClicked) {
            super.onBackPressed();
            overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Utils.STAFF_OBJECT, mStaffDetailsObject);
        outState.putInt(Utils.REQUIRED_MONTH, mCurrentMonth);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_EDIT:
                if (resultCode == RESULT_OK) {
                    setResult(Utils.RESULT_EDIT_INCENTIVE, data);
                    finish();
                }
                break;
        }
    }
}
