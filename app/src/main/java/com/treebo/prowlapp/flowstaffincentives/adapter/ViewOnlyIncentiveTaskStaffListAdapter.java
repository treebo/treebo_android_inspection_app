package com.treebo.prowlapp.flowstaffincentives.adapter;

import android.content.Context;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.staffincentives.StaffObject;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class ViewOnlyIncentiveTaskStaffListAdapter extends RecyclerView.Adapter<ViewOnlyIncentiveTaskStaffListAdapter.ViewOnlyStaffHolder> {

    private ArrayList<StaffObject> mList = new ArrayList<>();
    private Context mContext;

    public ViewOnlyIncentiveTaskStaffListAdapter(Context context, ArrayList<StaffObject> list) {
        mContext = context;
        mList = list;
    }

    public void setData(ArrayList<StaffObject> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewOnlyStaffHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_staff_incentives_previous,
                parent, false);
        return new ViewOnlyStaffHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewOnlyStaffHolder viewHolder, int position) {
        StaffObject staffObject = mList.get(position);
        viewHolder.object = staffObject;
        viewHolder.staffName.setText(staffObject.getStaffName());
        if (!TextUtils.isEmpty(staffObject.getStaffImageUrl())) {
            Utils.loadRoundedImageUsingGlide(mContext, staffObject.getStaffImageUrl(),
                    viewHolder.staffImage, R.drawable.ic_person_placeholder);
        }
        viewHolder.siaTv.setText(mContext.getString(R.string.sia_string,
                staffObject.getStartingIncentive()));
        viewHolder.incentiveText.setText(mContext.getString(R.string.rupee_string,
                staffObject.getIncentiveGiven()));
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        else
            return mList.size();
    }

    public class ViewOnlyStaffHolder extends RecyclerView.ViewHolder {

        StaffObject object;
        ImageView staffImage;
        TreeboTextView staffName;
        TreeboTextView siaTv;
        TreeboTextView incentiveText;

        public ViewOnlyStaffHolder(View itemView) {
            super(itemView);
            CardView cardView = (CardView) itemView.findViewById(R.id.incentive_card);
            cardView.setCardElevation(0);
            staffImage = (ImageView) itemView.findViewById(R.id.staff_image);
            staffName = (TreeboTextView) itemView.findViewById(R.id.staff_name_tv);
            siaTv = (TreeboTextView) itemView.findViewById(R.id.incentive_actual_tv);
            incentiveText = (TreeboTextView) itemView.findViewById(R.id.incentive_paid_tv);
        }
    }
}
