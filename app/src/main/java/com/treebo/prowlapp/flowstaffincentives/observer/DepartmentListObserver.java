package com.treebo.prowlapp.flowstaffincentives.observer;

import com.treebo.prowlapp.flowstaffincentives.presenter.DepartmentListPresenter;
import com.treebo.prowlapp.response.staffincentives.DepartmentListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 03/03/17.
 */

public class DepartmentListObserver extends BaseObserver<DepartmentListResponse> {

    private DepartmentListPresenter mPresenter;

    public DepartmentListObserver(DepartmentListPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        mPresenter = presenter;
    }

    @Override
    public void onCompleted() {
        mPresenter.resume();
    }

    @Override
    public void onNext(DepartmentListResponse departmentListResponse) {
        mPresenter.resume();
        if (departmentListResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.setAdapterData(departmentListResponse.getDepartmentList());
        } else {
            mPresenter.onGetListFailure(departmentListResponse.msg);
        }
    }
}
