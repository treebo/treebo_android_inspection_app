/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.treebo.prowlapp.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowOnboarding.activity.BlockerActivity;
import com.treebo.prowlapp.flowOnboarding.activity.OnBoardingActivity;
import com.treebo.prowlapp.flowaudit.activity.HotelAuditActivity;
import com.treebo.prowlapp.flowauditanyhotel.activity.AuditAnyHotelActivity;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.activity.AuditCardActivity;
import com.treebo.prowlapp.flowauditnew.activity.AuditCardType2Activity;
import com.treebo.prowlapp.flowauditnew.activity.AuditPeriodicActivity;
import com.treebo.prowlapp.flowauditnew.activity.DailyAuditActivity;
import com.treebo.prowlapp.flowauditnew.activity.FraudAuditActivity;
import com.treebo.prowlapp.flowauditnew.activity.RoomPickerActivity;
import com.treebo.prowlapp.flowdashboard.activity.ContactInfoActivity;
import com.treebo.prowlapp.flowdashboard.activity.CreateIssueActivity;
import com.treebo.prowlapp.flowdashboard.activity.ExploreActivity;
import com.treebo.prowlapp.flowdashboard.activity.HistoryActivity;
import com.treebo.prowlapp.flowdashboard.activity.SubmitFeedbackActivity;
import com.treebo.prowlapp.flowhotelportfolio.PortfolioActivity;
import com.treebo.prowlapp.flowhotelview.HotelsOverViewActivity;
import com.treebo.prowlapp.flowincentives.IncentivesActivity;
import com.treebo.prowlapp.flowissuenew.activity.EscalateIssueActivity;
import com.treebo.prowlapp.flowissuenew.activity.IssueDetailsActivity;
import com.treebo.prowlapp.flowissuenew.activity.IssueListActivity;
import com.treebo.prowlapp.flowissuenew.activity.UpdateIssueActivity;
import com.treebo.prowlapp.flowissues.activity.IssuesActivity;
import com.treebo.prowlapp.flowlocation.activity.EnableLocationActivity;
import com.treebo.prowlapp.flowlocation.activity.LocationSplashActivity;
import com.treebo.prowlapp.flowportfolionew.activity.CreatedTaskListActivity;
import com.treebo.prowlapp.flowportfolionew.activity.PendingTasksListActivity;
import com.treebo.prowlapp.flowportfolionew.activity.PortfolioActivityNew;
import com.treebo.prowlapp.flowportfolionew.activity.SettingsActivity;
import com.treebo.prowlapp.flowportfolionew.activity.TaskCreationActivity;
import com.treebo.prowlapp.flowportfolionew.activity.TaskDetailsActivity;
import com.treebo.prowlapp.flowscores.HotelScoresActivity;
import com.treebo.prowlapp.flowstaff.HotelStaffActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.DepartmentListActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.EditIncentivesActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.MonthlyListActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.StaffDetailsActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.StaffIncentivesDoneActivity;
import com.treebo.prowlapp.flowstaffincentives.activity.ToDoStaffIncentivesActivity;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioHotelV3;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.response.staffincentives.DepartmentObject;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.response.staffincentives.StaffObject;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.LocationUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;


/**
 * Class used to navigate through the application.
 */

public class Navigator {


    public Navigator() {
        //empty
    }

    /**
     * Goes to the user list screen.
     *
     * @param context A Context needed to open the destiny activity.
     */

    public void navigateToLoginActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch;
            intentToLaunch = OnBoardingActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToHotelsOverviewActivity(Activity context) {
        if (context != null) {
            Intent intentToLaunch = HotelsOverViewActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToSplashActivity(Activity context) {
        Intent intent;
        if (LocationUtils.isLocationEnabled(context)) {
            intent = LocationSplashActivity.getCallingIntent(context);
        } else {
            intent = EnableLocationActivity.getCallingIntent(context);
        }
        context.startActivity(intent);
    }

    public void navigateToIncentivesActivity(Context context, HotelDataResponse hotelData) {
        if (context != null) {
            Intent intentToLaunch = IncentivesActivity.getCallingIntent(context, hotelData);
            Bundle bundle = new Bundle();
            bundle.putParcelable(IncentivesActivity.HOTEL_DATA, hotelData);
            intentToLaunch.putExtras(bundle);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToScoresActivity(Context context, HotelDataResponse hotelDataResponse) {
        if (context != null) {
            Intent intentToLaunch = HotelScoresActivity.getCallingIntent(context, hotelDataResponse);
            Bundle bundle = new Bundle();
            bundle.putParcelable(HotelScoresActivity.SCORES_HOTEL_DATA, hotelDataResponse);
            intentToLaunch.putExtras(bundle);
            context.startActivity(intentToLaunch);

        }
    }

    public void navigateToHotelAuditActivity(Context context, HotelDataResponse mSelectedHotel) {
        if (context != null) {
            Intent intentToLaunch = HotelAuditActivity.getCallingIntent(context);
            intentToLaunch.putExtra(Utils.HOTEL_ID, mSelectedHotel.getHotelId());
            intentToLaunch.putExtra(Utils.HOTEL_NAME, mSelectedHotel.getHotelName());
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToHotelIssuesActivity(Context context, int hotelId) {
        if (context != null) {
            Intent intentToLaunch = IssuesActivity.getCallingIntent(context);
            intentToLaunch.putExtra(Utils.HOTEL_ID, hotelId);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToPortfolioActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch = PortfolioActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }

    public void navigateToStaffActivity(Context context, HotelDataResponse mSelectedHotel) {
        if (context != null) {
            Intent intentToLaunch = HotelStaffActivity.getCallingIntent(context);
            Bundle bundle = new Bundle();
            bundle.putParcelable(HotelStaffActivity.HOTEL_DATA, mSelectedHotel);
            intentToLaunch.putExtras(bundle);
            context.startActivity(intentToLaunch);

        }
    }

    public void navigateToRoomActivity(Activity activity, RoomListResponse roomTypeResponse, TaskModel taskModel) {
        if (activity != null) {
            Intent intent = RoomPickerActivity.getCallingIntent(activity);
            intent.putExtra(Constants.TASK_MODEL, taskModel);
            Bundle bundle = new Bundle();
            bundle.putParcelable(RoomSingleResponse.TAG, roomTypeResponse.getRoomDataObject());
            intent.putExtras(bundle);
            activity.startActivity(intent);
        }
    }

    public void navigateToCommonAudit(Activity activity, ArrayList<Category> categories, ArrayList<IssueListModelV3> issues,
                                      int auditType, int hotelID, String hotelName, boolean isInHotel, String roomName,
                                      int roomId, boolean isRestart, TaskModel taskModel) {
        if (activity != null) {
            Intent intent = AuditCardActivity.getCallingIntent(activity);
            intent.putParcelableArrayListExtra(Category.INCLUDED_CATEGORY_LIST, categories);
            intent.putParcelableArrayListExtra(Utils.PRE_EXISTING_ISSUES, issues);
            intent.putExtra(AuditUtilsV3.AUDIT_TYPE, auditType);
            intent.putExtra(Utils.HOTEL_ID, hotelID);
            intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
            intent.putExtra(Utils.HOTEL_NAME, hotelName);
            if (roomName != null) {
                intent.putExtra(Utils.ROOM_NAME, roomName);
            }
            intent.putExtra(Constants.TASK_MODEL, taskModel);
            intent.putExtra(Constants.ROOM_ID, roomId);
            intent.putExtra(Constants.IS_RESTART_AUDIT, isRestart);
            activity.startActivityForResult(intent, Constants.AUDIT_TASK_START);
        }
    }

    public void navigateToCommonAudit(Activity activity, ArrayList<Category> categories, ArrayList<IssueListModelV3> issues,
                                      int auditType, int hotelID, String hotelName, boolean isInHotel, String roomName,
                                      int roomId, boolean isRestart, TaskModel taskModel, Boolean isFromAuditAnyHotel) {
        if (activity != null) {
            Intent intent = AuditCardActivity.getCallingIntent(activity);
            intent.putParcelableArrayListExtra(Category.INCLUDED_CATEGORY_LIST, categories);
            intent.putParcelableArrayListExtra(Utils.PRE_EXISTING_ISSUES, issues);
            intent.putExtra(AuditUtilsV3.AUDIT_TYPE, auditType);
            intent.putExtra(Utils.HOTEL_ID, hotelID);
            intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
            intent.putExtra(Utils.HOTEL_NAME, hotelName);
            if (roomName != null) {
                intent.putExtra(Utils.ROOM_NAME, roomName);
            }
            intent.putExtra(Constants.TASK_MODEL, taskModel);
            intent.putExtra(Constants.ROOM_ID, roomId);
            intent.putExtra(Constants.IS_RESTART_AUDIT, isRestart);
            intent.putExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL, isFromAuditAnyHotel);
            activity.startActivityForResult(intent, Constants.AUDIT_TASK_START);
        }
    }


    public void navigateToAuditActivity(Activity activity, ArrayList<Category> categories, int auditType, int hotelID,
                                        String hotelName, boolean isInHotel, String roomName, int roomId, boolean isRestart, TaskModel taskModel) {
        if (activity != null) {
            Intent intent = AuditCardActivity.getCallingIntent(activity);
            intent.putParcelableArrayListExtra(Category.INCLUDED_CATEGORY_LIST, categories);
            intent.putExtra(AuditUtilsV3.AUDIT_TYPE, auditType);
            intent.putExtra(Utils.HOTEL_ID, hotelID);
            intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
            intent.putExtra(Utils.HOTEL_NAME, hotelName);
            if (roomName != null) {
                intent.putExtra(Utils.ROOM_NAME, roomName);
            }
            intent.putExtra(Constants.TASK_MODEL, taskModel);
            intent.putExtra(Constants.ROOM_ID, roomId);
            intent.putExtra(Constants.IS_RESTART_AUDIT, isRestart);
            activity.startActivityForResult(intent, Constants.AUDIT_TASK_START);
        }
    }

    public void navigateToAuditActivity(Activity activity, ArrayList<Category> categories, int auditType, int hotelID,
                                        String hotelName, boolean isInHotel, String roomName, int roomId, boolean isRestart, TaskModel taskModel, boolean isFromAuditAnyHotel) {
        if (activity != null) {
            Intent intent = AuditCardActivity.getCallingIntent(activity);
            intent.putParcelableArrayListExtra(Category.INCLUDED_CATEGORY_LIST, categories);
            intent.putExtra(AuditUtilsV3.AUDIT_TYPE, auditType);
            intent.putExtra(Utils.HOTEL_ID, hotelID);
            intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
            intent.putExtra(Utils.HOTEL_NAME, hotelName);
            if (roomName != null) {
                intent.putExtra(Utils.ROOM_NAME, roomName);
            }
            intent.putExtra(Constants.TASK_MODEL, taskModel);
            intent.putExtra(Constants.ROOM_ID, roomId);
            intent.putExtra(Constants.IS_RESTART_AUDIT, isRestart);
            intent.putExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL, isFromAuditAnyHotel);
            activity.startActivityForResult(intent, Constants.AUDIT_TASK_START);
        }
    }

    public void navigateToAuditActivity(Activity activity, int hotelID, String hotelName,
                                        boolean isInHotel, String roomName, int roomId, TaskModel taskModel,
                                        boolean isExtraRoomAudit, boolean isFromAuditAnyHotel) {
        if (activity != null) {
            Intent intent = AuditCardActivity.getCallingIntent(activity);
            intent.putParcelableArrayListExtra(Category.INCLUDED_CATEGORY_LIST, new ArrayList<>());
            intent.putExtra(AuditUtilsV3.AUDIT_TYPE, 2);
            intent.putExtra(Utils.HOTEL_ID, hotelID);
            intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
            intent.putExtra(Utils.HOTEL_NAME, hotelName);
            if (roomName != null) {
                intent.putExtra(Utils.ROOM_NAME, roomName);
            }
            intent.putExtra(Constants.TASK_MODEL, taskModel);
            intent.putExtra(Constants.ROOM_ID, roomId);
            intent.putExtra(Utils.IS_EXTRA_ROOM_AUDIT, isExtraRoomAudit);
            intent.putExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL, isFromAuditAnyHotel);
            activity.startActivity(intent);
        }
    }

    public void navigateToAuditActivity(Activity activity, int hotelID, String hotelName,
                                        boolean isInHotel, String roomName, int roomId, TaskModel taskModel,
                                        boolean isExtraRoomAudit) {
        if (activity != null) {
            Intent intent = AuditCardActivity.getCallingIntent(activity);
            intent.putParcelableArrayListExtra(Category.INCLUDED_CATEGORY_LIST, new ArrayList<>());
            intent.putExtra(AuditUtilsV3.AUDIT_TYPE, 2);
            intent.putExtra(Utils.HOTEL_ID, hotelID);
            intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
            intent.putExtra(Utils.HOTEL_NAME, hotelName);
            if (roomName != null) {
                intent.putExtra(Utils.ROOM_NAME, roomName);
            }
            intent.putExtra(Constants.TASK_MODEL, taskModel);
            intent.putExtra(Constants.ROOM_ID, roomId);
            intent.putExtra(Utils.IS_EXTRA_ROOM_AUDIT, isExtraRoomAudit);
            activity.startActivity(intent);
        }
    }

    public void navigateToIssueListActivity(Activity activity, int hotelId, String hotelName,
                                            int sortCriteria, int taskLogID, boolean isIncList) {
        if (activity != null) {
            Intent intent = IssueListActivity.getCallingIntent(activity);
            intent.putExtra(Utils.ISSUE_SORT_ORDER, sortCriteria);
            intent.putExtra(Utils.HOTEL_ID, hotelId);
            intent.putExtra(Utils.HOTEL_NAME, hotelName);
            intent.putExtra(Utils.TASK_LOG_ID, taskLogID);
            intent.putExtra(Utils.IS_INC_LIST, isIncList);
            activity.startActivity(intent);
        }
    }

    public void navigateToIssueDetailsActivity(Activity activity, IssueListModelV3 issue, int hotelID,
                                               int requestCode, int taskLogID, boolean isFromPreMarkedIssue) {
        if (activity != null) {
            Intent intent = IssueDetailsActivity.getCallingIntent(activity);
            intent.putExtra(Utils.ISSUES_OBJECT, issue);
            intent.putExtra(Utils.HOTEL_ID, hotelID);
            intent.putExtra(Utils.TASK_LOG_ID, taskLogID);
            intent.putExtra(Utils.IS_FROM_PRE_EXISTING_ISSUE, isFromPreMarkedIssue);
            activity.startActivityForResult(intent, requestCode);
        }
    }

    public void navigateToPortfolioActivity(Activity activity, PortfolioHotelV3 hotelV3, boolean isInHotelLocation) {
        if (activity != null) {
            SegmentAnalyticsManager.trackScreenOnSegment(SegmentAnalyticsManager.PORTFOLIO_VIEW,
                    new Properties().putValue(SegmentAnalyticsManager.SOURCE_SCREEN, SegmentAnalyticsManager.DASHBOARD_SCREEN));
            Intent intent = PortfolioActivityNew.getCallingIntent(activity);
            intent.putExtra(Utils.HOTEL_OBJECT, hotelV3);
            intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
            intent.setFlags(FLAG_ACTIVITY_SINGLE_TOP | FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
        }
    }

    public void navigateToIssueResolveActivity(Activity activity, IssueListModelV3 issue,
                                               boolean isResolveIssue, int hotelId, int userId, int taskLogID) {
        Intent intent = new Intent(activity, UpdateIssueActivity.class);
        intent.putExtra(Utils.HOTEL_ID, hotelId);
        intent.putExtra(Utils.ISSUE_ID, issue.getIssueID());
        intent.putExtra(Utils.ISSUE_RESOLUTION_DATE, issue.getResolutionDate());
        intent.putExtra(Utils.USER_ID, userId);
        intent.putExtra(Utils.TASK_LOG_ID, taskLogID);
        intent.putExtra(Utils.ISSUE_RESOLVE, isResolveIssue);
        activity.startActivityForResult(intent, isResolveIssue ? Constants.REQUEST_RESOLVE_ISSUE : Constants.REQUEST_UPDATE_ISSUE);
        activity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    public void navigateToFeedbackActivity(Activity activity) {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.FEEDBACK_CLICK, new Properties());
        Intent intent = new Intent(activity, SubmitFeedbackActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    public void navigateToExploreActivity(Activity activity, String hotelName, int hotelID) {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.EXPLORE_CLICK, new Properties());
        Intent intent = new Intent(activity, ExploreActivity.class);
        intent.putExtra(Utils.HOTEL_NAME, hotelName);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    public void navigateToFraudAuditActivity(Activity activity, int hotelID,
                                             TaskModel taskModel, String hotelName, boolean isInHotelLocation) {
        Intent intent = new Intent(activity, FraudAuditActivity.class);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.HOTEL_NAME, hotelName);
        intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
        intent.putExtra(Constants.TASK_MODEL, taskModel);
        activity.startActivity(intent);
    }

    public void navigateToSettingsActivity(Activity activity) {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SETTINGS_CLICK, new Properties());
        Intent intent = new Intent(activity, SettingsActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    public void navigateToHistoryActivity(Activity activity) {
        Intent intent = new Intent(activity, HistoryActivity.class);
        activity.startActivity(intent);
    }

    public void navigateToContactInfoActivity(Activity activity) {
        Intent intent = new Intent(activity, ContactInfoActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    public void navigateToPendingTasksListActivity(Activity activity) {
        Intent intent = new Intent(activity, PendingTasksListActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    public void navigateToTaskCreationActivity(Activity activity) {
        Intent intent = new Intent(activity, TaskCreationActivity.class);
        activity.startActivity(intent);
    }

    public void navigateToCreatedTaskListActivity(Activity activity) {
        Intent intent = new Intent(activity, CreatedTaskListActivity.class);
        activity.startActivity(intent);
    }

    public void navigateToTaskDetailsActivity(Activity activity, CreatedTaskModel task) {
        Intent intent = new Intent(activity, TaskDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Utils.CREATED_TASK, task);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public void navigateToDailyAuditActivity(Activity activity, String hotelName, boolean isInHotel, TaskModel taskModel, int hotelId) {
        Intent intent = new Intent(activity, DailyAuditActivity.class);
        intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
        intent.putExtra(Constants.TASK_MODEL, taskModel);
        intent.putExtra(Utils.HOTEL_NAME, hotelName);
        intent.putExtra(Utils.HOTEL_ID, hotelId);

        activity.startActivity(intent);
    }

    public void navigateToCreateIssueActivity(Activity activity, int hotelId, String hotelName) {
        if (activity != null) {
            Intent intent = new Intent(activity, CreateIssueActivity.class);
            intent.putExtra(Utils.HOTEL_ID, hotelId);
            intent.putExtra(Utils.HOTEL_NAME, hotelName);
            activity.startActivity(intent);
        }
    }

    public void navigateToAuditType2Activity(Activity activity, TaskModel taskModel, int auditCategoryId,
                                             ArrayList<Category> categories, int hotelID,
                                             String hotelName, boolean isInHotel,
                                             boolean isFromIntermediateScreen) {
        Intent intent = AuditCardType2Activity.getCallingIntent(activity);
        intent.putExtra(Constants.TASK_MODEL, taskModel);
        intent.putExtra(Constants.AUDIT_CATEGORY_ID, auditCategoryId);
        intent.putExtra(Category.INCLUDED_CATEGORY_LIST, categories);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
        intent.putExtra(Utils.HOTEL_NAME, hotelName);
        intent.putExtra(Utils.IS_FROM_INTERMEDIATE_SCREEN, isFromIntermediateScreen);
        activity.startActivity(intent);
    }

    public void navigateToAuditType2Activity(Activity activity, TaskModel taskModel, int auditCategoryId,
                                             ArrayList<Category> categories, int hotelID,
                                             String hotelName, boolean isInHotel,
                                             boolean isFromIntermediateScreen, boolean isFromAuditAnyHotel) {
        Intent intent = AuditCardType2Activity.getCallingIntent(activity);
        intent.putExtra(Constants.TASK_MODEL, taskModel);
        intent.putExtra(Constants.AUDIT_CATEGORY_ID, auditCategoryId);
        intent.putExtra(Category.INCLUDED_CATEGORY_LIST, categories);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
        intent.putExtra(Utils.HOTEL_NAME, hotelName);
        intent.putExtra(Utils.IS_FROM_INTERMEDIATE_SCREEN, isFromIntermediateScreen);
        intent.putExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL, isFromAuditAnyHotel);
        activity.startActivity(intent);
    }

    public void navigateToAuditType2Activity(Activity activity, TaskModel taskModel, int auditCategoryId,
                                             ArrayList<Category> categories, int hotelID,
                                             String hotelName, boolean isInHotel,
                                             boolean isFromIntermediateScreen, boolean isFromCreateIssue,
                                             int roomID) {
        Intent intent = AuditCardType2Activity.getCallingIntent(activity);
        intent.putExtra(Constants.TASK_MODEL, taskModel);
        intent.putExtra(Constants.AUDIT_CATEGORY_ID, auditCategoryId);
        intent.putExtra(Category.INCLUDED_CATEGORY_LIST, categories);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
        intent.putExtra(Utils.HOTEL_NAME, hotelName);
        intent.putExtra(Utils.IS_FROM_INTERMEDIATE_SCREEN, isFromIntermediateScreen);
        intent.putExtra(Utils.IS_FROM_CREATE_ISSUE, isFromCreateIssue);
        intent.putExtra(Constants.ROOM_ID, roomID);
        activity.startActivity(intent);
    }

    public void navigateToPeriodicActivity(Activity activity, TaskModel taskModel,
                                           int hotelID, String hotelName, boolean isInHotel) {
        Intent intent = AuditPeriodicActivity.getCallingIntent(activity);
        intent.putExtra(Constants.TASK_MODEL, taskModel);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
        intent.putExtra(Utils.HOTEL_NAME, hotelName);
        activity.startActivity(intent);
    }
    public void navigateToPeriodicActivity(Activity activity, TaskModel taskModel,
                                           int hotelID, String hotelName, boolean isInHotel, boolean isFromAuditAnyHotel) {
        Intent intent = AuditPeriodicActivity.getCallingIntent(activity);
        intent.putExtra(Constants.TASK_MODEL, taskModel);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.IS_IN_HOTEL, isInHotel);
        intent.putExtra(Utils.HOTEL_NAME, hotelName);
        intent.putExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL, isFromAuditAnyHotel);
        activity.startActivity(intent);
    }

    public void navigateToMonthlyIncentiveListActivity(Activity activity, int hotelID) {
        Intent intent = new Intent(activity, MonthlyListActivity.class);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        activity.startActivity(intent);
    }

    public void navigateToMonthlyDepartmentListActivity(Activity activity,
                                                        MonthlyIncentiveListResponse.MonthlyIncentiveListObject object,
                                                        int hotelID) {
        Intent intent = new Intent(activity, DepartmentListActivity.class);
        intent.putExtra(Utils.MONTHLY_RESPONSE_OBJECT, object);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        activity.startActivity(intent);
    }

    public void navigateToToDoStaffIncentiveActivity(Activity activity, int hotelID, int departmentID,
                                                     String departmentTitle,
                                                     String status, int month, int year,
                                                     int requestCode) {
        Intent intent = new Intent(activity, ToDoStaffIncentivesActivity.class);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.DEPARTMENT_ID, departmentID);
        intent.putExtra(Utils.DEPARTMENT_TITLE, departmentTitle);
        intent.putExtra(Utils.DEPARTMENT_INCENTIVE_STATUS, status);
        intent.putExtra(Utils.REQUIRED_MONTH, month);
        intent.putExtra(Utils.REQUIRED_YEAR, year);
        activity.startActivityForResult(intent, requestCode);
    }

    public void navigateToStaffIncentiveDoneActivity(Activity activity, int hotelID,
                                                     DepartmentObject departmentObject,
                                                     String status, int month, int year) {
        Intent intent = new Intent(activity, StaffIncentivesDoneActivity.class);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.DEPARTMENT_INCENTIVE_STATUS, status);
        intent.putExtra(Utils.DEPARTMENT_OBJECT, departmentObject);
        intent.putExtra(Utils.REQUIRED_MONTH, month);
        intent.putExtra(Utils.REQUIRED_YEAR, year);
        activity.startActivity(intent);
    }

    public void navigateToEditIncentiveActivity(Activity activity, StaffObject staffObject, int requestCode) {
        Intent intent = new Intent(activity, EditIncentivesActivity.class);
        intent.putExtra(Utils.STAFF_OBJECT, staffObject);
        activity.startActivityForResult(intent, requestCode);
    }

    public void navigateToStaffDetailsActivity(Activity activity, int hotelID, StaffObject staffObject, int month, int requestCode) {
        Intent intent = new Intent(activity, StaffDetailsActivity.class);
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.STAFF_OBJECT, staffObject);
        intent.putExtra(Utils.REQUIRED_MONTH, month);
        activity.startActivityForResult(intent, requestCode);
        activity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    public void navigateToBlockerScreen(Activity activity) {
        Intent intent = new Intent(activity, BlockerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    public void navigateToEscalateIssueScreen(Activity activity, IssueListModelV3 issue, int hotelID, String hotelName) {
        Intent intent = new Intent(activity, EscalateIssueActivity.class);
        intent.putExtra(Utils.ISSUE_ID, issue.getIssueID());
        intent.putExtra(Utils.HOTEL_ID, hotelID);
        intent.putExtra(Utils.HOTEL_NAME, hotelName);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
    }

    public void navigateToAuditAnyHotelActivity(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, AuditAnyHotelActivity.class);
            activity.startActivity(intent);
        }
    }
}
