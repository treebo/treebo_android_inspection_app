package com.treebo.prowlapp.events;

/**
 * Created by sumandas on 23/06/2016.
 */
public class AuditIssueSubmitEvent {

    public String mEventType;

    public AuditIssueSubmitEvent(String eventType){
        mEventType=eventType;
    }
}
