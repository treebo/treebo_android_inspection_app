package com.treebo.prowlapp.events;

import java.util.ArrayList;

/**
 * Created by devesh on 28/05/16.
 */
public class LanguageChangeEvent {
    ArrayList<String> langs;
    String type;

    public LanguageChangeEvent(ArrayList<String> langs, String type) {
        this.langs = langs;
        this.type = type;
    }
}
