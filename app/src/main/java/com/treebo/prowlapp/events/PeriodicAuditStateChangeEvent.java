package com.treebo.prowlapp.events;

/**
 * Created by sumandas on 25/01/2017.
 */

public class PeriodicAuditStateChangeEvent extends AuditStateChangeEvent {

    public int mTaskLogID;

    public PeriodicAuditStateChangeEvent(String desc, int taskId) {
        super(desc);
        mTaskLogID = taskId;
    }
}
