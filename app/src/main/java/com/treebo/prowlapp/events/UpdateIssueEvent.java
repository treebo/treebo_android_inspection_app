package com.treebo.prowlapp.events;

import com.treebo.prowlapp.Models.UpdateIssueModel;

/**
 * Created by sumandas on 07/05/2016.
 */
public class UpdateIssueEvent {
    public UpdateIssueModel mUpdateIssueModel;

    public UpdateIssueEvent(UpdateIssueModel updateIssueModel){
        mUpdateIssueModel=updateIssueModel;
    }
}
