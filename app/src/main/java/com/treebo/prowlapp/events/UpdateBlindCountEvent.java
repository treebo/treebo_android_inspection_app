package com.treebo.prowlapp.events;

import android.util.Pair;

import com.treebo.prowlapp.Models.RoomTypeList;

import java.util.ArrayList;

/**
 * Created by sumandas on 09/05/2016.
 */
public class UpdateBlindCountEvent {
    public ArrayList<Pair<String, String>> mScorelist;
    public ArrayList<RoomTypeList> mRooms;

    public UpdateBlindCountEvent(ArrayList<Pair<String, String>> scorelist,ArrayList<RoomTypeList> rooms){
        mScorelist=scorelist;
        mRooms=rooms;
    }
}
