package com.treebo.prowlapp.events;

import com.treebo.prowlapp.Models.RoomTypeList;

import java.util.ArrayList;

/**
 * Created by sumandas on 09/05/2016.
 */
public class RoomSubmittedEvent {

    public ArrayList<RoomTypeList> mRoomList;

    public RoomSubmittedEvent(ArrayList<RoomTypeList>roomList){
        mRoomList=roomList;
    }

}
