package com.treebo.prowlapp.events;

/**
 * Created by abhisheknair on 14/03/17.
 */

public class DepartmentIncentivesGivenEvent {

    private int month;
    private int year;

    public DepartmentIncentivesGivenEvent(int month, int year) {
        this.month = month;
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }
}
