package com.treebo.prowlapp.events;

/**
 * Created by abhisheknair on 09/12/16.
 */

public class IssueResolvedEventV3 {

    public int overdueIssueCount;
    public int todaysIssueCount;

    public IssueResolvedEventV3(int overdueIssueCount, int todaysIssueCount) {
        this.overdueIssueCount = overdueIssueCount;
        this.todaysIssueCount = todaysIssueCount;
    }
}
