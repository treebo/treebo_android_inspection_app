package com.treebo.prowlapp.events;

import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;

/**
 * Created by abhisheknair on 28/06/17.
 */

public class CreatedTaskUpdated {

    private boolean isTaskDeleted;
    private CreatedTaskModel taskObject;

    public CreatedTaskUpdated(boolean isDelete, CreatedTaskModel task) {
        this.isTaskDeleted = isDelete;
        this.taskObject = task;
    }

    public boolean isTaskDeleted() {
        return isTaskDeleted;
    }

    public CreatedTaskModel getTaskObject() {
        return taskObject;
    }
}
