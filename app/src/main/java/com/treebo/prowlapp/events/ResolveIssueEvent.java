package com.treebo.prowlapp.events;

import com.treebo.prowlapp.Models.UpdateIssueModel;

/**
 * Created by sumandas on 07/05/2016.
 */
public class ResolveIssueEvent {

    public UpdateIssueModel mUpdateIssueModel;

    public ResolveIssueEvent(UpdateIssueModel updateIssueModel){
        mUpdateIssueModel=updateIssueModel;
    }

}
