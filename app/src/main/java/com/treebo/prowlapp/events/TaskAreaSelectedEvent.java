package com.treebo.prowlapp.events;

import com.treebo.prowlapp.Models.taskcreationmodel.TextSubtextModel;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 28/06/17.
 */

public class TaskAreaSelectedEvent {

    private String type;

    private ArrayList<TextSubtextModel> list;

    private boolean areAllSelected;

    public String getType() {
        return type;
    }

    public ArrayList<TextSubtextModel> getList() {
        return list == null ? new ArrayList<>() : list;
    }

    public boolean areAllSelected() {
        return areAllSelected;
    }

    public String getDisplayText() {
        String displayText = "";
        if (areAllSelected) {
            switch (type) {
                case "region":
                    displayText = "All regions";
                    break;

                case "cluster":
                    displayText = "All regions";
                    break;
            }
        } else {
            for (int i = 0; i < getList().size(); i++) {
                TextSubtextModel model = getList().get(i);
                displayText += (model.getText() + ",");
                if (i >= 3) {
                    displayText = displayText.substring(0, displayText.length() - 1);
                    displayText += "....";
                    break;
                }
                displayText = displayText.length() > 1
                        ? displayText.substring(0, displayText.length() - 1) : displayText;
            }
        }
        return displayText;
    }

    public TaskAreaSelectedEvent(String type, ArrayList<TextSubtextModel> models,
                                 boolean allSelected) {
        this.type = type;
        this.list = models;
        this.areAllSelected = allSelected;
    }
}
