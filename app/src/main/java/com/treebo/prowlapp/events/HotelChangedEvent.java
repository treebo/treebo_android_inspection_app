package com.treebo.prowlapp.events;

import com.treebo.prowlapp.Models.HotelLocation;

import java.util.ArrayList;

/**
 * Created by sumandas on 22/11/2016.
 */

public class HotelChangedEvent {

    public ArrayList<HotelLocation> mHotelList;

    public HotelChangedEvent(ArrayList<HotelLocation> hotelList) {
        mHotelList = hotelList;
    }
}
