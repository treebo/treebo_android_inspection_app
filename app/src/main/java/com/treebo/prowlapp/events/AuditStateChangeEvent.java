package com.treebo.prowlapp.events;

/**
 * Created by sumandas on 02/12/2016.
 */

public class AuditStateChangeEvent {

    public static final String ROOM_AUDIT_SUBMIT = "daily_audit_submit";
    public static final String COMMON_AREA_AUDIT_SUBMIT = "common_area_audit_submit";
    public static final String FRAUD_AUDIT_SUBMIT = "fraud_audit_submit";

    public static final String UPDATE_AVAILABLE_ROOM_COUNT = "update_available_room_count";
    public static final String CATEGORY_DONE = "category_done";
    public static final String AUDIT_COMPLETE = "audit_complete";

    public static final String ISSUE_CREATED = "issue_created";
    public static final String CLEAR_AUDIT = "clear_audit";

    public static final String PERIODIC_AUDIT_SUBMIT = "periodic_audit_submit";
    public static final String FnB_PERIODIC_AUDIT_SUBMIT = "fnb_audit_submit";


    public String auditStateDescription;

    public AuditStateChangeEvent(String desc) {
        auditStateDescription = desc;
    }
}
