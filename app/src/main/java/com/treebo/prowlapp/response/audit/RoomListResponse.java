package com.treebo.prowlapp.response.audit;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by sumandas on 10/11/2016.
 */

public class RoomListResponse extends BaseResponse {


    public static final String TAG = "room_list_response";

    @SerializedName("data")
    private RoomDataObject roomDataObject;

    public static class RoomDataObject implements Parcelable {

        @SerializedName("display_room_count")
        private int displayRoomCount;

        @SerializedName("rooms")
        private ArrayList<RoomV3Object> roomList;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.displayRoomCount);
            dest.writeTypedList(this.roomList);
        }

        public RoomDataObject() {
        }

        protected RoomDataObject(Parcel in) {
            this.displayRoomCount = in.readInt();
            this.roomList = in.createTypedArrayList(RoomV3Object.CREATOR);
        }

        public static final Creator<RoomDataObject> CREATOR = new Creator<RoomDataObject>() {
            @Override
            public RoomDataObject createFromParcel(Parcel source) {
                return new RoomDataObject(source);
            }

            @Override
            public RoomDataObject[] newArray(int size) {
                return new RoomDataObject[size];
            }
        };

        public int getDisplayRoomCount() {
            return displayRoomCount;
        }

        public void setDisplayRoomCount(int displayRoomCount) {
            this.displayRoomCount = displayRoomCount;
        }

        public ArrayList<RoomV3Object> getRoomList() {
            return roomList;
        }

        public void setRoomList(ArrayList<RoomV3Object> roomList) {
            this.roomList = roomList;
        }
    }

    public RoomDataObject getRoomDataObject() {
        return roomDataObject;
    }

    public void setRoomDataObject(RoomDataObject roomDataObject) {
        this.roomDataObject = roomDataObject;
    }


}
