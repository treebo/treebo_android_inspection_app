package com.treebo.prowlapp.response.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by abhisheknair on 25/05/17.
 */

public class UserPermissionsResponse extends BaseResponse implements Parcelable {

    @SerializedName("data")
    private RolesAndUserPermissions rolesAndUserPermissions;


    protected UserPermissionsResponse(Parcel in) {
        rolesAndUserPermissions = in.readParcelable(RolesAndUserPermissions.class.getClassLoader());
    }

    public static final Creator<UserPermissionsResponse> CREATOR = new Creator<UserPermissionsResponse>() {
        @Override
        public UserPermissionsResponse createFromParcel(Parcel in) {
            return new UserPermissionsResponse(in);
        }

        @Override
        public UserPermissionsResponse[] newArray(int size) {
            return new UserPermissionsResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public RolesAndUserPermissions getRolesAndUserPermissions() {
        return rolesAndUserPermissions;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(rolesAndUserPermissions, flags);
    }
}
