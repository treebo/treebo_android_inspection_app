package com.treebo.prowlapp.response.common;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by abhisheknair on 07/02/17.
 */

public class SystemPropertyResponse extends BaseResponse {

    @SerializedName("data")
    private Properties data;

    public static class Properties {

        @SerializedName("system_properties")
        private SystemProperty systemProperty;

        public SystemProperty getSystemProperty() {
            return systemProperty;
        }
    }

    public static class SystemProperty {

        @SerializedName("accuracy")
        private String accuracy;

        @SerializedName("house_count_tooltip_text")
        private String houseCountTooltipText;

        @SerializedName("price_mismatch_tooltip_text")
        private String priceMismatchTooltipText;

        @SerializedName("checklist_version")
        private String masterChecklistVersion;

        @SerializedName("percentage_change")
        private String percentMinMax;

        @SerializedName("app_version")
        private String appVersion;

        @SerializedName("prowl.issue.creation.geofence")
        private String isIssueCreationEnabled;

        public float getAccuracy() {
            return Float.valueOf(accuracy);
        }

        public String getPriceMismatchTooltipText() {
            return this.priceMismatchTooltipText;
        }

        public String getHouseCountTooltipText() {
            return this.houseCountTooltipText;
        }

        public int getMasterCheckListVersion() {
            return Integer.valueOf(this.masterChecklistVersion);
        }

        public float getPercentMinMax() {
            return Float.valueOf(this.percentMinMax);
        }

        public int getAppVersion() {
            return Integer.valueOf(this.appVersion);
        }

        public boolean getIsIssueCreationEnabled() {
            return isIssueCreationEnabled != null
                    && isIssueCreationEnabled.toLowerCase().equals("true");
        }
    }

    public Properties getData() {
        return data;
    }

}
