package com.treebo.prowlapp.response.issue;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 15/11/16.
 */

public class IssueListResponse extends BaseResponse {

    @SerializedName("data")
    public ArrayList<IssueListModelV3> data;


}
