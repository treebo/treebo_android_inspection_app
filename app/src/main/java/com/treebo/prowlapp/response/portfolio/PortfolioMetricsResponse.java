package com.treebo.prowlapp.response.portfolio;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioMetrics;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by abhisheknair on 24/11/16.
 */

public class PortfolioMetricsResponse extends BaseResponse {

    @SerializedName("data")
    public PortfolioMetrics metrics;
}
