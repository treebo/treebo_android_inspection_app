package com.treebo.prowlapp.response.dashboard;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.dashboardmodel.TaskHistoryModel;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class TaskHistoryResponse extends BaseResponse {

    @SerializedName("data")
    public TaskHistory data;

    public static class TaskHistory {

        @SerializedName("user_id")
        public int userID;

        @SerializedName("user_name")
        public String userName;

        @SerializedName("user_profile")
        public String userProfilePic;

        @SerializedName("history")
        public ArrayList<TaskHistoryModel> taskList;
    }
}
