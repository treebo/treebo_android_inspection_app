package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.RootCauseData;

/**
 * Created by sumandas on 04/05/2016.
 */
@Deprecated
public class RootCauseResponse extends BaseResponse {

    @SerializedName("data")
    public RootCauseData data;

}
