package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.DepartmentModel;

import java.util.List;

/**
 * Created by devesh on 02/05/16.
 */
public class DepartmentResponse extends BaseResponse {

    @SerializedName("data")
    public DepartmentResponseData data;

    public static class DepartmentResponseData{
        @SerializedName("departments")
        public List<DepartmentModel> departments;
    }


}
