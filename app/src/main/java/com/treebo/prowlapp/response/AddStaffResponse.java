package com.treebo.prowlapp.response;

/**
 * Created by devesh on 27/04/16.
 */
public class AddStaffResponse {

    AddStaffResponseData data;

    public static class AddStaffResponseData{
        String message;
    }

    public String getMessage() {
        return data.message;
    }

}
