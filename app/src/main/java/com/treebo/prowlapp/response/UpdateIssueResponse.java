package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sumandas on 01/05/2016.
 */
public class UpdateIssueResponse extends BaseResponse {
    @SerializedName("data")
    public String data;
}
