package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sumandas on 04/07/2016.
 */
@Deprecated
public class MarkIncentiveDone extends BaseResponse {

    @SerializedName("data")
    public IncentiveStatus data;

    public static class IncentiveStatus {
        @SerializedName("message")
        public String message;
    }
}

