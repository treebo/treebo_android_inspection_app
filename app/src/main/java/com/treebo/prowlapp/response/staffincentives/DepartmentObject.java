package com.treebo.prowlapp.response.staffincentives;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abhisheknair on 01/03/17.
 */

public class DepartmentObject implements Parcelable {

    @SerializedName("department_name")
    private String departmentName;

    @SerializedName("department_id")
    private int departmentID;

    @SerializedName("staff_count")
    private int totalStaff;

    @SerializedName("pending_count")
    private int pendingCount;

    @SerializedName("monthly_delta")
    private float monthlyDelta;

    @SerializedName("daily_delta")
    private float dailyDelta;

    @SerializedName("distribute")
    private boolean distribute;

    @SerializedName("status")
    private String status;

    public DepartmentObject() {

    }

    protected DepartmentObject(Parcel in) {
        departmentName = in.readString();
        departmentID = in.readInt();
        totalStaff = in.readInt();
        pendingCount = in.readInt();
        monthlyDelta = in.readFloat();
        dailyDelta = in.readFloat();
        distribute = in.readByte() != 0;
        status = in.readString();
    }

    public static final Creator<DepartmentObject> CREATOR = new Creator<DepartmentObject>() {
        @Override
        public DepartmentObject createFromParcel(Parcel in) {
            return new DepartmentObject(in);
        }

        @Override
        public DepartmentObject[] newArray(int size) {
            return new DepartmentObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(departmentName);
        dest.writeInt(departmentID);
        dest.writeInt(totalStaff);
        dest.writeInt(pendingCount);
        dest.writeFloat(monthlyDelta);
        dest.writeFloat(dailyDelta);
        dest.writeByte((byte) (distribute ? 1 : 0));
        dest.writeString(status);
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(int departmentID) {
        this.departmentID = departmentID;
    }

    public int getTotalStaff() {
        return totalStaff;
    }

    public void setTotalStaff(int totalStaff) {
        this.totalStaff = totalStaff;
    }

    public int getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(int pendingCount) {
        this.pendingCount = pendingCount;
    }

    public int getMonthlyDelta() {
        return Math.round(monthlyDelta);
    }

    public void setMonthlyDelta(float monthlyDelta) {
        this.monthlyDelta = monthlyDelta;
    }

    public int getDailyDelta() {
        return Math.round(dailyDelta);
    }

    public void setDailyDelta(float dailyDelta) {
        this.dailyDelta = dailyDelta;
    }

    public boolean isDistribute() {
        return distribute;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
