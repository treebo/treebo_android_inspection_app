package com.treebo.prowlapp.response.common;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by sumandas on 10/11/2016.
 */

public class HotelLocationResponse extends BaseResponse {

    @SerializedName("data")
    public HotelData data;

    public static class HotelData {

        @SerializedName("hotels")
        public ArrayList<HotelLocation> hotels;
    }


}
