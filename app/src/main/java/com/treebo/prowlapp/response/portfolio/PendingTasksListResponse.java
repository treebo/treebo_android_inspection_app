package com.treebo.prowlapp.response.portfolio;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 29/04/17.
 */

public class PendingTasksListResponse extends BaseResponse implements Parcelable {

    @SerializedName("data")
    private ArrayList<PendingTaskObject> pendingTaskList;

    protected PendingTasksListResponse(Parcel in) {
        pendingTaskList = in.createTypedArrayList(PendingTaskObject.CREATOR);
    }

    public static final Creator<PendingTasksListResponse> CREATOR = new Creator<PendingTasksListResponse>() {
        @Override
        public PendingTasksListResponse createFromParcel(Parcel in) {
            return new PendingTasksListResponse(in);
        }

        @Override
        public PendingTasksListResponse[] newArray(int size) {
            return new PendingTasksListResponse[size];
        }
    };

    public ArrayList<PendingTaskObject> getPendingTaskList() {
        return pendingTaskList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(pendingTaskList);
    }


    public static class PendingTaskObject implements Parcelable {

        @SerializedName("hotel_id")
        private int hotelID;

        @SerializedName("hotel")
        private String hotelName;

        @SerializedName("task")
        private String taskName;

        @SerializedName("completed")
        private boolean isCompleted;

        @SerializedName("completion_date")
        private String completionDate;

        protected PendingTaskObject(Parcel in) {
            hotelID = in.readInt();
            hotelName = in.readString();
            taskName = in.readString();
            isCompleted = in.readByte() != 0;
            completionDate = in.readString();
        }

        public static final Creator<PendingTaskObject> CREATOR = new Creator<PendingTaskObject>() {
            @Override
            public PendingTaskObject createFromParcel(Parcel in) {
                return new PendingTaskObject(in);
            }

            @Override
            public PendingTaskObject[] newArray(int size) {
                return new PendingTaskObject[size];
            }
        };

        public int getHotelID() {
            return hotelID;
        }

        public String getHotelName() {
            return hotelName;
        }

        public String getTaskName() {
            return taskName;
        }

        public boolean isCompleted() {
            return isCompleted;
        }

        public String getCompletionDate() {
            return completionDate;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(hotelID);
            dest.writeString(hotelName);
            dest.writeString(taskName);
            dest.writeByte((byte) (isCompleted ? 1 : 0));
            dest.writeString(completionDate);
        }
    }
}
