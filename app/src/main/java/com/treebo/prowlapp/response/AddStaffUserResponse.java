package com.treebo.prowlapp.response;


import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.StaffModel;

/**
 * Created by devesh on 27/04/16.
 */
public class AddStaffUserResponse extends BaseResponse {

    @SerializedName("data")
    public AddStaffUserResponseData data;

    public int getStaffID() {
        return this.data.getStaffID();
    }

    public static class AddStaffUserResponseData {
        @SerializedName("staff")
        private StaffModel staff;

        public int getStaffID() {
            return staff.getId();
        }

    }
}
