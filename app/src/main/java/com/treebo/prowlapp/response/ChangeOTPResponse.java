package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by devesh on 27/04/16.
 */
@Deprecated
public class ChangeOTPResponse extends BaseResponse {

    @Override
    public String toString() {
        return "ChangeOTPResponse{" +
                "data=" + data +
                '}';
    }

    @SerializedName("data")
    public ChangeOTPData data;

    public static class ChangeOTPData {

        @SerializedName("message")
        private String message;

        @Override
        public String toString() {
            return "ChangeOTPData{" +
                    "message='" + message + '\'' +
                    '}';
        }
    }

    public String getMessage() {
        return data.message;
    }

    public void setMessage(String message) {
        data.message = message;
    }
}
