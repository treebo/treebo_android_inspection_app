package com.treebo.prowlapp.response.staffincentives;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 02/03/17.
 */

public class StaffListResponse extends BaseResponse {

    @SerializedName("data")
    private ArrayList<StaffObject> staffList;

    public ArrayList<StaffObject> getStaffList() {
        return this.staffList;
    }

    public void setStaffList(ArrayList<StaffObject> list) {
        this.staffList = list;
    }
}
