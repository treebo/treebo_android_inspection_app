package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.data.IndividualEntry;

import java.util.List;

/**
 * Created by devesh on 28/04/16.
 */
@Deprecated
public class HotelIncentivesResponse extends BaseResponse {

    @SerializedName("data")
    public HotelIncentivesResponseData data;


    public static class HotelIncentivesResponseData {

        @SerializedName("individuals")
        public List<DepartmentIndividuals> departmentIndividualsList;

        @SerializedName("departments")
        public List<Department> departments;

    }

    public static class DepartmentIndividuals {
        @SerializedName("department")
        public String individualDepartment;

        @SerializedName("information")
        public List<IndividualEntry> individualEntries;

    }

    public static class Department {
        @SerializedName("department")
        public String department;

        @SerializedName("information")
        public Information information;
    }

    public static class Information {
        @SerializedName("todays_changes")
        public int todaysChanges;
        @SerializedName("score")
        public int score;
        @SerializedName("till_date")
        public int tillDate;
    }


}
