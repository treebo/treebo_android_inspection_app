package com.treebo.prowlapp.response.taskcreation;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by abhisheknair on 28/06/17.
 */

public class TaskCreationResponse extends BaseResponse {

    @SerializedName("data")
    private CreatedTaskModel task;

    public CreatedTaskModel getTask() {
        return task;
    }
}
