package com.treebo.prowlapp.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 16/05/17.
 */

public class BankListResponse extends BaseResponse {

    @SerializedName("data")
    private ArrayList<Bank> bankList = new ArrayList<>();

    public ArrayList<Bank> getBankList() {
        return bankList;
    }

    public static class Bank implements Parcelable {

        @SerializedName("id")
        private int id;

        @SerializedName("bank_name")
        private String bankName;

        @SerializedName("validator")
        private String validator;

        protected Bank(Parcel in) {
            id = in.readInt();
            bankName = in.readString();
            validator = in.readString();
        }

        public Bank() {

        }

        public static final Creator<Bank> CREATOR = new Creator<Bank>() {
            @Override
            public Bank createFromParcel(Parcel in) {
                return new Bank(in);
            }

            @Override
            public Bank[] newArray(int size) {
                return new Bank[size];
            }
        };

        public String getBankName() {
            return this.bankName == null ? "" : this.bankName;
        }

        public String getValidator() {
            return this.validator == null ? "" : this.validator;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(bankName);
            dest.writeString(validator);
        }
    }
}
