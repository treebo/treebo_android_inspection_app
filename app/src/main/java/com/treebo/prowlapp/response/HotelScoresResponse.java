package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by devesh on 30/04/16.
 */
@Deprecated
public class HotelScoresResponse extends BaseResponse {

    @SerializedName("data")
    public HotelScoresResponseData data;

    public static class HotelScoresResponseData {
        @SerializedName("overall")
        public int overall;




    }


    /*
    "QA": {
      "overall": 80,
      "HK": {
        "current": 80,
        "last_seven_days": 80,
        "last_thirty_days": 80
      },
      "Owner / Manager": {
        "current": 89,
        "last_seven_days": 89,
        "last_thirty_days": 89
      },
      "F&B": {
        "current": 80,
        "last_seven_days": 80,
        "last_thirty_days": 80
      },
      "FO": {
        "current": 70,
        "last_seven_days": 70,
        "last_thirty_days": 70
      }
    },
    "Treebo": {
      "Treebo": {}
    }
     */

}
