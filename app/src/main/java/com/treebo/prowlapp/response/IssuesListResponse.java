package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.IssuesModel;

import java.util.List;

/**
 * Created by devesh on 29/04/16.
 */
@Deprecated
public class IssuesListResponse extends BaseResponse {

    @SerializedName("data")
    public IssuesListData data;

    public static class IssuesListData{
        @SerializedName("issues")
        public List<IssuesModel> issues;
    }



}

