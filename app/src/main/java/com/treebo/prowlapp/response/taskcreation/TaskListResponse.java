package com.treebo.prowlapp.response.taskcreation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class TaskListResponse extends BaseResponse {

    @SerializedName("data")
    private Tasks taskList;

    public Tasks getTaskList() {
        return taskList;
    }


    public static class Tasks implements Parcelable {

        @SerializedName("started")
        private ArrayList<CreatedTaskModel> startedTasks;

        @SerializedName("completed")
        private ArrayList<CreatedTaskModel> completedTasks;

        @SerializedName("yet_to_start")
        private ArrayList<CreatedTaskModel> yetToStartTasks;

        public ArrayList<CreatedTaskModel> getStartedTasks() {
            return startedTasks != null ? startedTasks : new ArrayList<>();
        }

        public ArrayList<CreatedTaskModel> getCompletedTasks() {
            return completedTasks != null ? completedTasks : new ArrayList<>();
        }

        public ArrayList<CreatedTaskModel> getYetToStartTasks() {
            return yetToStartTasks != null ? yetToStartTasks : new ArrayList<>();
        }

        protected Tasks(Parcel in) {
            startedTasks = in.createTypedArrayList(CreatedTaskModel.CREATOR);
            completedTasks = in.createTypedArrayList(CreatedTaskModel.CREATOR);
            yetToStartTasks = in.createTypedArrayList(CreatedTaskModel.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(startedTasks);
            dest.writeTypedList(completedTasks);
            dest.writeTypedList(yetToStartTasks);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Tasks> CREATOR = new Creator<Tasks>() {
            @Override
            public Tasks createFromParcel(Parcel in) {
                return new Tasks(in);
            }

            @Override
            public Tasks[] newArray(int size) {
                return new Tasks[size];
            }
        };
    }
}
