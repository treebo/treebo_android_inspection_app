package com.treebo.prowlapp.response.issue;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 03/05/17.
 */

public class EscalationUserListResponse extends BaseResponse {

    @SerializedName("data")
    private Users userList;

    public static class Users implements Parcelable {

        @SerializedName("users")
        private ArrayList<EscalationUser> userList = new ArrayList<>();

        @SerializedName("hotel_name")
        private String hotelName = "";

        protected Users(Parcel in) {
            userList = in.createTypedArrayList(EscalationUser.CREATOR);
            hotelName = in.readString();
        }

        public static final Creator<Users> CREATOR = new Creator<Users>() {
            @Override
            public Users createFromParcel(Parcel in) {
                return new Users(in);
            }

            @Override
            public Users[] newArray(int size) {
                return new Users[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(userList);
            dest.writeString(hotelName);
        }

        public ArrayList<EscalationUser> getUserList() {
            return userList;
        }

        public String getHotelName() {
            return hotelName;
        }
    }

    public static class EscalationUser implements Parcelable {

        public EscalationUser(String email) {
            this.email = email;
        }

        @SerializedName("id")
        private int id;

        @SerializedName("first_name")
        private String firstName;

        @SerializedName("last_name")
        private String lastName;

        @SerializedName("email")
        private String email;

        protected EscalationUser(Parcel in) {
            id = in.readInt();
            firstName = in.readString();
            lastName = in.readString();
            email = in.readString();
        }

        public static final Creator<EscalationUser> CREATOR = new Creator<EscalationUser>() {
            @Override
            public EscalationUser createFromParcel(Parcel in) {
                return new EscalationUser(in);
            }

            @Override
            public EscalationUser[] newArray(int size) {
                return new EscalationUser[size];
            }
        };

        public int getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getEmail() {
            return email;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(firstName);
            dest.writeString(lastName);
            dest.writeString(email);
        }

        @Override
        public boolean equals(Object object1) {
            if (object1 == null)
                return false;
            if (!EscalationUser.class.isAssignableFrom(object1.getClass()))
                return false;
            EscalationUser user = (EscalationUser) object1;
            return user.getEmail().equals(this.getEmail());
        }
    }


    public ArrayList<EscalationUser> getUserList() {
        return userList != null ? userList.getUserList() : new ArrayList<>();
    }
}
