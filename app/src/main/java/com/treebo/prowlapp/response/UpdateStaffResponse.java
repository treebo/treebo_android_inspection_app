package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by devesh on 29/04/16.
 */
public class UpdateStaffResponse extends BaseResponse {

    @SerializedName("data")
    UpdateStaffResponseData data;

    public static class UpdateStaffResponseData{
        @SerializedName("message")
        public String message;
    }

    public String getMessage() {
        return data.message;
    }



}
