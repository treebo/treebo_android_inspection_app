package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by devesh on 26/04/16.
 */
public class BaseResponse {

    @SerializedName("status")
    public String status;

    @SerializedName("msg")
    public String msg;

    @SerializedName("code")
    public int code;

    @Override
    public String toString() {
        return "BaseResponse{" +
                "status='" + status + '\'' +
                ", message='" + msg + '\'' +
                ", code=" + code +
                '}';
    }

}
