package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.Audit;

/**
 * Created by sumandas on 29/04/2016.
 */
@Deprecated
public class HotelRoomResponse extends BaseResponse {
    @SerializedName("data")
    public Audit data;

}
