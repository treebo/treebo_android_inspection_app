package com.treebo.prowlapp.response.audit;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sumandas on 20/01/2017.
 */

public class AuditAreaResponse extends BaseResponse implements Parcelable {

    @SerializedName("data")
    public AuditAreaObject mAuditAreaObject;

    public static class AuditAreaObject implements Parcelable {

        @SerializedName("key")
        public ArrayList<AuditArea> mAuditAreas;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.mAuditAreas);
        }

        public AuditAreaObject() {
        }

        protected AuditAreaObject(Parcel in) {
            this.mAuditAreas = in.createTypedArrayList(AuditArea.CREATOR);
        }

        public static final Creator<AuditAreaObject> CREATOR = new Creator<AuditAreaObject>() {
            @Override
            public AuditAreaObject createFromParcel(Parcel source) {
                return new AuditAreaObject(source);
            }

            @Override
            public AuditAreaObject[] newArray(int size) {
                return new AuditAreaObject[size];
            }
        };
    }

    public static final class AuditArea implements Parcelable {

        @SerializedName("details")
        public ArrayList<AuditAreaDetails> mDetails;

        @SerializedName("title")
        public String mName;

        @SerializedName("audit_category_id")
        public int mAuditCategoryId;

        public AuditArea() {

        }

        protected AuditArea(Parcel in) {
            mDetails = in.createTypedArrayList(AuditAreaDetails.CREATOR);
            mName = in.readString();
            mAuditCategoryId = in.readInt();
        }

        public static final Creator<AuditArea> CREATOR = new Creator<AuditArea>() {
            @Override
            public AuditArea createFromParcel(Parcel in) {
                return new AuditArea(in);
            }

            @Override
            public AuditArea[] newArray(int size) {
                return new AuditArea[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(mDetails);
            dest.writeString(mName);
            dest.writeInt(mAuditCategoryId);
        }
    }

    public static class AuditAreaDetails implements Parcelable {

        @SerializedName("included_categories")
        public HashMap<Integer, ArrayList<Integer>> mCheckpointList;

        @SerializedName("is_audited")
        public boolean isAuditDone;

        @SerializedName("room_id")
        private int roomID;

        @SerializedName("room_number")
        private String roomNumber;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeSerializable(this.mCheckpointList);
            dest.writeByte(this.isAuditDone ? (byte) 1 : (byte) 0);
            dest.writeInt(roomID);
            dest.writeString(roomNumber);
        }

        public AuditAreaDetails() {
        }

        protected AuditAreaDetails(Parcel in) {
            this.mCheckpointList = (HashMap<Integer, ArrayList<Integer>>) in.readSerializable();
            this.isAuditDone = in.readByte() != 0;
            this.roomID = in.readInt();
            this.roomNumber = in.readString();
        }

        public static final Creator<AuditAreaDetails> CREATOR = new Creator<AuditAreaDetails>() {
            @Override
            public AuditAreaDetails createFromParcel(Parcel source) {
                return new AuditAreaDetails(source);
            }

            @Override
            public AuditAreaDetails[] newArray(int size) {
                return new AuditAreaDetails[size];
            }
        };

        public int getRoomID() {
            return roomID;
        }

        public String getRoomNumber() {
            return roomNumber;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mAuditAreaObject, flags);
    }

    public AuditAreaResponse() {
    }

    protected AuditAreaResponse(Parcel in) {
        this.mAuditAreaObject = in.readParcelable(AuditAreaObject.class.getClassLoader());
    }

    public static final Creator<AuditAreaResponse> CREATOR = new Creator<AuditAreaResponse>() {
        @Override
        public AuditAreaResponse createFromParcel(Parcel source) {
            return new AuditAreaResponse(source);
        }

        @Override
        public AuditAreaResponse[] newArray(int size) {
            return new AuditAreaResponse[size];
        }
    };
}
