package com.treebo.prowlapp.response.issue;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.issuemodel.IssueDetailsV3;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by sumandas on 17/11/2016.
 */

public class IssueDetailResponse  extends BaseResponse {

    @SerializedName("default_sort_key")
    public String defaultSortOrder;

    @SerializedName("data")
    public IssueDetailsV3 data;

}
