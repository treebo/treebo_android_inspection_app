package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by devesh on 05/05/16.
 */
@Deprecated
public class PortfolioResponse extends BaseResponse {

    @SerializedName("data")
    public List<PortfolioResponseData> data;

    public static class PortfolioResponseData {
        @SerializedName("department")
        public String department;

        @SerializedName("score")
        public double score;
    }
}
