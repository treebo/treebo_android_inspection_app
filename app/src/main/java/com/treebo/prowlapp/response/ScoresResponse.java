package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by devesh on 27/04/16.
 */
public class ScoresResponse extends BaseResponse {

    @SerializedName("data")
    public List<ScoresSectionedData> data;

    public static class ScoresSectionedData {

        @SerializedName("heading")
        public String heading;

        @SerializedName("scores")
        public Scores scores;

    }


    public static class Scores {

        @SerializedName("overall")
        public double overall;

        @SerializedName("tql_month")
        public String tqlMonth;

        @SerializedName("subheading")
        public List<SubHeadingScores> subHeading;


    }

    public static class SubHeadingScores {

        @SerializedName("current")
        public double current;

        @SerializedName("name")
        public String name;

        @SerializedName("last_seven_days")
        public double last_seven_days;

        @SerializedName("last_thirty_days")
        public double last_thirty_days;


    }
}
