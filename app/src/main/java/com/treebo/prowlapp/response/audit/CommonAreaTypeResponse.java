package com.treebo.prowlapp.response.audit;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.auditmodel.CommonArea;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by sumandas on 08/11/2016.
 */

public class CommonAreaTypeResponse extends BaseResponse {

    public static final String TAG = "common_area_response";

    @SerializedName("data")
    private CommonAreaObject data;


    public static class CommonAreaObject {
        @SerializedName("common_area")
        private CommonArea commonArea;

        public CommonArea getCommonArea() {
            return this.commonArea;
        }
    }

    public CommonAreaObject getData() {
        return this.data;
    }

    public void setData(CommonAreaObject object) {
        this.data = object;
    }

}
