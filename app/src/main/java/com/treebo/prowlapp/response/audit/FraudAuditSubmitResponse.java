package com.treebo.prowlapp.response.audit;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by abhisheknair on 20/01/17.
 */

public class FraudAuditSubmitResponse extends BaseResponse {
    @SerializedName("data")
    public FraudAuditStatus data;

    public static class FraudAuditStatus {
        @SerializedName("status")
        public String status;
    }
}
