package com.treebo.prowlapp.response.portfolio;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioHotelV3;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class PortfolioHotelListResponseV3 extends BaseResponse {

    @SerializedName("data")
    public ArrayList<PortfolioHotelV3> hotelList;
}
