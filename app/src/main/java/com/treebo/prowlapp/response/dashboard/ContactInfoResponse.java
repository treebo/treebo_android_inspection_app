package com.treebo.prowlapp.response.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 21/04/17.
 */

public class ContactInfoResponse extends BaseResponse {

    @SerializedName("data")
    private ContactList contactsList;

    public static class ContactList implements Parcelable {

        @SerializedName("users")
        private ArrayList<ContactInfoObject> mUserList = new ArrayList<>();

        @SerializedName("channels")
        private ArrayList<ContactInfoObject> mChannelList = new ArrayList<>();

        protected ContactList(Parcel in) {
            mUserList = in.createTypedArrayList(ContactInfoObject.CREATOR);
            mChannelList = in.createTypedArrayList(ContactInfoObject.CREATOR);
        }

        public static final Creator<ContactList> CREATOR = new Creator<ContactList>() {
            @Override
            public ContactList createFromParcel(Parcel in) {
                return new ContactList(in);
            }

            @Override
            public ContactList[] newArray(int size) {
                return new ContactList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(mUserList);
            dest.writeTypedList(mChannelList);
        }

        public ArrayList<ContactInfoObject> getUserList() {
            return mUserList;
        }

        public ArrayList<ContactInfoObject> getChannelList() {
            return mChannelList;
        }

    }

    public static class ContactInfoObject extends BaseResponse implements Parcelable {

        @SerializedName("description")
        private String dependency;

        @SerializedName("name")
        private String name;

        @SerializedName("phone_number")
        private String phoneNumber;

        @SerializedName("email_id")
        private String emailID;

        @SerializedName("slack_user_id")
        private String slackUserID;

        @SerializedName("is_slack_channel")
        private boolean isSlackChannel;

        @SerializedName("slack_channel_id")
        private String channelID;

        public ContactInfoObject() {

        }

        protected ContactInfoObject(Parcel in) {
            dependency = in.readString();
            name = in.readString();
            phoneNumber = in.readString();
            emailID = in.readString();
            slackUserID = in.readString();
            isSlackChannel = in.readByte() != 0;
            channelID = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(dependency);
            dest.writeString(name);
            dest.writeString(phoneNumber);
            dest.writeString(emailID);
            dest.writeString(slackUserID);
            dest.writeByte((byte) (isSlackChannel ? 1 : 0));
            dest.writeString(channelID);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<ContactInfoObject> CREATOR = new Creator<ContactInfoObject>() {
            @Override
            public ContactInfoObject createFromParcel(Parcel in) {
                return new ContactInfoObject(in);
            }

            @Override
            public ContactInfoObject[] newArray(int size) {
                return new ContactInfoObject[size];
            }
        };

        public String getDependency() {
            return dependency;
        }

        public String getName() {
            return name;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public String getEmailID() {
            return emailID;
        }

        public String getSlackUserID() {
            return slackUserID;
        }

        public boolean isSlackChannel() {
            return isSlackChannel;
        }

        public String getChannelID() {
            return channelID;
        }

        public void setDependency(String dependency) {
            this.dependency = dependency;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }

        public void setSlackUserID(String slackUserID) {
            this.slackUserID = slackUserID;
        }

        public void setSlackChannel(boolean slackChannel) {
            isSlackChannel = slackChannel;
        }

        public void setChannelID(String channelID) {
            this.channelID = channelID;
        }
    }

    public ArrayList<ContactInfoObject> getContactInfoList() {
        ArrayList<ContactInfoObject> list = new ArrayList<>();
        for (ContactInfoObject object : this.contactsList.getChannelList()) {
            String name = object.getName();
            if (!name.contains("Slack channel")) {
                object.setName("Slack channel: " + name);
                object.setSlackChannel(true);
            }
        }
        list.addAll(this.contactsList.getChannelList());
        list.addAll(this.contactsList.getUserList());
        return list;
    }
}
