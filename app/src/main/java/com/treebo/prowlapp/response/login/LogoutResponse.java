package com.treebo.prowlapp.response.login;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by sumandas on 21/08/2016.
 */
public class LogoutResponse extends BaseResponse {

    @SerializedName("data")
    public LogoutData data;

    public static class LogoutData {
        @SerializedName("token")
        public String token;

    }
}
