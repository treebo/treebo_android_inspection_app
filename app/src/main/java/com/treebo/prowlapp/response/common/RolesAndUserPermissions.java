package com.treebo.prowlapp.response.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 22/05/17.
 */

public class RolesAndUserPermissions implements Parcelable {

    @SerializedName("hotel_roles")
    private ArrayList<HotelUserRole> otherRoles;

    @SerializedName("basic_roles")
    private ArrayList<UserRole> basicRole;

    protected RolesAndUserPermissions(Parcel in) {
        otherRoles = in.createTypedArrayList(HotelUserRole.CREATOR);
        basicRole = in.createTypedArrayList(UserRole.CREATOR);
    }

    public static final Creator<RolesAndUserPermissions> CREATOR = new Creator<RolesAndUserPermissions>() {
        @Override
        public RolesAndUserPermissions createFromParcel(Parcel in) {
            return new RolesAndUserPermissions(in);
        }

        @Override
        public RolesAndUserPermissions[] newArray(int size) {
            return new RolesAndUserPermissions[size];
        }
    };

    public String getAssignedRoles(int hotelID, boolean isTruncated) {
        String roles = "";
        if (basicRole != null) {
            for (int i = 0; i < this.basicRole.size(); i++) {
                roles += i == 0 ? this.basicRole.get(i).getName()
                        : (isTruncated ? "," : "/") + this.basicRole.get(i).getName();
            }
        }
        if (otherRoles != null) {
            for (int i = 0; i < this.otherRoles.size(); i++) {
                if (hotelID == otherRoles.get(i).getHotelID() || hotelID == 0) {
                    for (UserRole userRole : otherRoles.get(i).getRoles()) {
                        roles += (isTruncated ? "," : "/") + userRole.getName();
                    }
                }
                if (i >= 3 && isTruncated) {
                    roles += "...";
                    break;
                }
            }
        }
        return roles;
    }

    public PermissionList getUserPermissionsList(int hotelID) {
        PermissionList unionList = new PermissionList();
        if (basicRole != null) {
            for (int i = 0; i < this.basicRole.size(); i++) {
                unionList.setProwlEnabled(unionList.isProwlEnabled()
                        || basicRole.get(i).getRolePermissions().isProwlEnabled());
                unionList.setMasterLoginEnabled(unionList.isMasterLoginEnabled()
                        || basicRole.get(i).getRolePermissions().isMasterLoginEnabled());
                unionList.setGoogleLoginEnabled(unionList.isGoogleLoginEnabled()
                        || basicRole.get(i).getRolePermissions().isGoogleLoginEnabled());
                unionList.setDashboardMastheadEnabled(unionList.isDashboardMastheadEnabled()
                        || basicRole.get(i).getRolePermissions().isDashboardMastheadEnabled());
                unionList.setDashboardMastheadHeaderEnabled(unionList.isDashboardMastheadHeaderEnabled()
                        || basicRole.get(i).getRolePermissions().isDashboardMastheadHeaderEnabled());
                unionList.setDashboardComplianceMetricEnabled(unionList.isDashboardComplianceMetricEnabled()
                        || basicRole.get(i).getRolePermissions().isDashboardComplianceMetricEnabled());
                unionList.setDashboardDelightMetricEnabled(unionList.isDashboardDelightMetricEnabled()
                        || basicRole.get(i).getRolePermissions().isDashboardDelightMetricEnabled());
                unionList.setDashboardDisasterMetricEnabled(unionList.isDashboardDisasterMetricEnabled()
                        || basicRole.get(i).getRolePermissions().isDashboardDisasterMetricEnabled());
                unionList.setDashboardEmptyScreenEnabled(unionList.isDashboardEmptyScreenEnabled()
                        || basicRole.get(i).getRolePermissions().isDashboardEmptyScreenEnabled());
                unionList.setDashboardPortfolioEnabled(unionList.isDashboardPortfolioEnabled()
                        || basicRole.get(i).getRolePermissions().isDashboardPortfolioEnabled());
                unionList.setDashboardExploreEnabled(unionList.isDashboardExploreEnabled()
                        || basicRole.get(i).getRolePermissions().isDashboardExploreEnabled());
                unionList.setExploreCreateIssueEnabled(unionList.isExploreCreateIssueEnabled()
                        || basicRole.get(i).getRolePermissions().isExploreCreateIssueEnabled());
                unionList.setExploreIssueLogEnabled(unionList.isExploreIssueLogEnabled()
                        || basicRole.get(i).getRolePermissions().isExploreIssueLogEnabled());
                unionList.setExploreStaffDetailsEnabled(unionList.isExploreStaffDetailsEnabled()
                        || basicRole.get(i).getRolePermissions().isExploreStaffDetailsEnabled());
                unionList.setExploreStaffIncentivesEnabled(unionList.isExploreStaffIncentivesEnabled()
                        || basicRole.get(i).getRolePermissions().isExploreStaffIncentivesEnabled());
                unionList.setExploreHistoryEnabled(unionList.isExploreHistoryEnabled()
                        || basicRole.get(i).getRolePermissions().isExploreHistoryEnabled());
                unionList.setSettingsEnabled(unionList.isSettingsEnabled()
                        || basicRole.get(i).getRolePermissions().isSettingsEnabled());
                unionList.setSettingsTaskManagementEnabled(unionList.isSettingsTaskManagementEnabled()
                        || basicRole.get(i).getRolePermissions().isSettingsTaskManagementEnabled());
                unionList.setSettingsFeedbackEnabled(unionList.isSettingsFeedbackEnabled()
                        || basicRole.get(i).getRolePermissions().isSettingsFeedbackEnabled());
                unionList.setSettingsContactInfoEnabled(unionList.isSettingsContactInfoEnabled()
                        || basicRole.get(i).getRolePermissions().isSettingsContactInfoEnabled());
                unionList.setSettingsTaskCompletionEnabled(unionList.isSettingsTaskCompletionEnabled()
                        || basicRole.get(i).getRolePermissions().isSettingsTaskCompletionEnabled());
                unionList.setSettingsSignoutEnabled(unionList.isSettingsSignoutEnabled()
                        || basicRole.get(i).getRolePermissions().isSettingsSignoutEnabled());
                unionList.setSettingsVersionDetailsEnabled(unionList.isSettingsVersionDetailsEnabled()
                        || basicRole.get(i).getRolePermissions().isSettingsVersionDetailsEnabled());
                unionList.setDashboardMastheadEnabled(unionList.isDashboardMastheadEnabled()
                        || basicRole.get(i).getRolePermissions().isDashboardMastheadEnabled());
                unionList.setPortfolioMastheadEnabled(unionList.isPortfolioMastheadEnabled()
                        || basicRole.get(i).getRolePermissions().isPortfolioMastheadEnabled());
                unionList.setPortfolioMastheadHeaderEnabled(unionList.isPortfolioMastheadHeaderEnabled()
                        || basicRole.get(i).getRolePermissions().isPortfolioMastheadHeaderEnabled());
                unionList.setPortfolioComplianceMetricEnabled(unionList.isPortfolioComplianceMetricEnabled()
                        || basicRole.get(i).getRolePermissions().isPortfolioComplianceMetricEnabled());
                unionList.setPortfolioDelightMetricEnabled(unionList.isPortfolioDelightMetricEnabled()
                        || basicRole.get(i).getRolePermissions().isPortfolioDelightMetricEnabled());
                unionList.setPortfolioDisasterMetricEnabled(unionList.isPortfolioDisasterMetricEnabled()
                        || basicRole.get(i).getRolePermissions().isPortfolioDisasterMetricEnabled());
                unionList.setPortfolioIncMetricEnabled(unionList.isPortfolioIncMetricEnabled()
                        || basicRole.get(i).getRolePermissions().isPortfolioIncMetricEnabled());
                unionList.setPortfolioTaskCompletionMetricEnabled(unionList.isPortfolioTaskCompletionMetricEnabled()
                        || basicRole.get(i).getRolePermissions().isPortfolioTaskCompletionMetricEnabled());
                unionList.setEscalateIssueEnabled(unionList.isEscalateIssueEnabled()
                        || basicRole.get(i).getRolePermissions().isEscalateIssueEnabled());
                unionList.setSwipeIssueEnabled(unionList.isSwipeIssueEnabled()
                        || basicRole.get(i).getRolePermissions().isSwipeIssueEnabled());
                unionList.setSwipeUpdateIssueEnabled(unionList.isSwipeUpdateIssueEnabled()
                        || basicRole.get(i).getRolePermissions().isSwipeUpdateIssueEnabled());
                unionList.setSwipeResolveIssueEnabled(unionList.isSwipeResolveIssueEnabled()
                        || basicRole.get(i).getRolePermissions().isSwipeResolveIssueEnabled());
                unionList.setUpdateIssueEnabled(unionList.isUpdateIssueEnabled()
                        || basicRole.get(i).getRolePermissions().isUpdateIssueEnabled());
                unionList.setResolveIssueEnabled(unionList.isResolveIssueEnabled()
                        || basicRole.get(i).getRolePermissions().isResolveIssueEnabled());
                unionList.setStaffDetailsAddNewEnabled(unionList.isStaffDetailsAddNewEnabled()
                        || basicRole.get(i).getRolePermissions().isStaffDetailsAddNewEnabled());
                unionList.setStaffDetailsEditEnabled(unionList.isStaffDetailsEditEnabled()
                        || basicRole.get(i).getRolePermissions().isStaffDetailsEditEnabled());
                unionList.setStaffDetailsDeleteEnabled(unionList.isStaffDetailsDeleteEnabled()
                        || basicRole.get(i).getRolePermissions().isStaffDetailsDeleteEnabled());
                unionList.setStaffIncentivesEditEnabled(unionList.isStaffIncentivesEditEnabled()
                        || basicRole.get(i).getRolePermissions().isStaffIncentivesEditEnabled());
                unionList.setStaffIncentivesDistributeEnabled(unionList.isStaffIncentivesDistributeEnabled()
                        || basicRole.get(i).getRolePermissions().isStaffIncentivesDistributeEnabled());
                unionList.setStaffIncentivesWriteEnabled(unionList.isStaffIncentivesWriteEnabled()
                        || basicRole.get(i).getRolePermissions().isStaffIncentivesWriteEnabled());
            }
        }
        if (otherRoles != null) {
            for (int i = 0; i < this.otherRoles.size(); i++) {
                HotelUserRole currentHotelRole = otherRoles.get(i);
                if (currentHotelRole.getHotelID() == hotelID) {
                    for (int j = 0; j < currentHotelRole.getRoles().size(); j++) {
                        unionList.setProwlEnabled(unionList.isProwlEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isProwlEnabled());
                        unionList.setMasterLoginEnabled(unionList.isMasterLoginEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isMasterLoginEnabled());
                        unionList.setGoogleLoginEnabled(unionList.isGoogleLoginEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isGoogleLoginEnabled());
                        unionList.setDashboardMastheadEnabled(unionList.isDashboardMastheadEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isDashboardMastheadEnabled());
                        unionList.setDashboardMastheadHeaderEnabled(unionList.isDashboardMastheadHeaderEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isDashboardMastheadHeaderEnabled());
                        unionList.setDashboardComplianceMetricEnabled(unionList.isDashboardComplianceMetricEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isDashboardComplianceMetricEnabled());
                        unionList.setDashboardDelightMetricEnabled(unionList.isDashboardDelightMetricEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isDashboardDelightMetricEnabled());
                        unionList.setDashboardDisasterMetricEnabled(unionList.isDashboardDisasterMetricEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isDashboardDisasterMetricEnabled());
                        unionList.setDashboardEmptyScreenEnabled(unionList.isDashboardEmptyScreenEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isDashboardEmptyScreenEnabled());
                        unionList.setDashboardPortfolioEnabled(unionList.isDashboardPortfolioEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isDashboardPortfolioEnabled());
                        unionList.setDashboardExploreEnabled(unionList.isDashboardExploreEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isDashboardExploreEnabled());
                        unionList.setExploreIssueLogEnabled(unionList.isExploreIssueLogEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isExploreIssueLogEnabled());
                        unionList.setExploreCreateIssueEnabled(unionList.isExploreCreateIssueEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isExploreCreateIssueEnabled());
                        unionList.setExploreStaffDetailsEnabled(unionList.isExploreStaffDetailsEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isExploreStaffDetailsEnabled());
                        unionList.setExploreStaffIncentivesEnabled(unionList.isExploreStaffIncentivesEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isExploreStaffIncentivesEnabled());
                        unionList.setExploreHistoryEnabled(unionList.isExploreHistoryEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isExploreHistoryEnabled());
                        unionList.setSettingsEnabled(unionList.isSettingsEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSettingsEnabled());
                        unionList.setSettingsTaskManagementEnabled(unionList.isSettingsTaskManagementEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSettingsTaskManagementEnabled());
                        unionList.setSettingsFeedbackEnabled(unionList.isSettingsFeedbackEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSettingsFeedbackEnabled());
                        unionList.setSettingsContactInfoEnabled(unionList.isSettingsContactInfoEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSettingsContactInfoEnabled());
                        unionList.setSettingsTaskCompletionEnabled(unionList.isSettingsTaskCompletionEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSettingsTaskCompletionEnabled());
                        unionList.setSettingsSignoutEnabled(unionList.isSettingsSignoutEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSettingsSignoutEnabled());
                        unionList.setSettingsVersionDetailsEnabled(unionList.isSettingsVersionDetailsEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSettingsVersionDetailsEnabled());
                        unionList.setDashboardMastheadEnabled(unionList.isDashboardMastheadEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isDashboardMastheadEnabled());
                        unionList.setPortfolioMastheadEnabled(unionList.isPortfolioMastheadEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isPortfolioMastheadEnabled());
                        unionList.setPortfolioMastheadHeaderEnabled(unionList.isPortfolioMastheadHeaderEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isPortfolioMastheadHeaderEnabled());
                        unionList.setPortfolioComplianceMetricEnabled(unionList.isPortfolioComplianceMetricEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isPortfolioComplianceMetricEnabled());
                        unionList.setPortfolioDelightMetricEnabled(unionList.isPortfolioDelightMetricEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isPortfolioDelightMetricEnabled());
                        unionList.setPortfolioDisasterMetricEnabled(unionList.isPortfolioDisasterMetricEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isPortfolioDisasterMetricEnabled());
                        unionList.setPortfolioIncMetricEnabled(unionList.isPortfolioIncMetricEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isPortfolioIncMetricEnabled());
                        unionList.setPortfolioTaskCompletionMetricEnabled(unionList.isPortfolioTaskCompletionMetricEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isPortfolioTaskCompletionMetricEnabled());
                        unionList.setEscalateIssueEnabled(unionList.isEscalateIssueEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isEscalateIssueEnabled());
                        unionList.setSwipeIssueEnabled(unionList.isSwipeIssueEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSwipeIssueEnabled());
                        unionList.setSwipeUpdateIssueEnabled(unionList.isSwipeUpdateIssueEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSwipeUpdateIssueEnabled());
                        unionList.setSwipeResolveIssueEnabled(unionList.isSwipeResolveIssueEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isSwipeResolveIssueEnabled());
                        unionList.setUpdateIssueEnabled(unionList.isUpdateIssueEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isUpdateIssueEnabled());
                        unionList.setResolveIssueEnabled(unionList.isResolveIssueEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isResolveIssueEnabled());
                        unionList.setStaffDetailsAddNewEnabled(unionList.isStaffDetailsAddNewEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isStaffDetailsAddNewEnabled());
                        unionList.setStaffDetailsEditEnabled(unionList.isStaffDetailsEditEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isStaffDetailsEditEnabled());
                        unionList.setStaffDetailsDeleteEnabled(unionList.isStaffDetailsDeleteEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isStaffDetailsDeleteEnabled());
                        unionList.setStaffIncentivesEditEnabled(unionList.isStaffIncentivesEditEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isStaffIncentivesEditEnabled());
                        unionList.setStaffIncentivesDistributeEnabled(unionList.isStaffIncentivesDistributeEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isStaffIncentivesDistributeEnabled());
                        unionList.setStaffIncentivesWriteEnabled(unionList.isStaffIncentivesWriteEnabled()
                                || currentHotelRole.getRoles().get(j).getRolePermissions().isStaffIncentivesWriteEnabled());
                    }
                }
            }
        }
        return unionList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(otherRoles);
        dest.writeTypedList(basicRole);
    }

    public static class PermissionList implements Parcelable {

        public PermissionList() {

        }

        @SerializedName("prowl")
        private boolean isProwlEnabled;

        @SerializedName("prowl.login")
        private boolean isMasterLoginEnabled;

        @SerializedName("prowl.login.google-login")
        private boolean isGoogleLoginEnabled;

        @SerializedName("prowl.dashboard.masthead")
        private boolean isDashboardMastheadEnabled;

        @SerializedName("prowl.dashboard.masthead.header")
        private boolean isDashboardMastheadHeaderEnabled;

        @SerializedName("prowl.dashboard.masthead.compliance")
        private boolean isDashboardComplianceMetricEnabled;

        @SerializedName("prowl.dashboard.masthead.delight")
        private boolean isDashboardDelightMetricEnabled;

        @SerializedName("prowl.dashboard.masthead.disaster")
        private boolean isDashboardDisasterMetricEnabled;

        @SerializedName("prowl.dashboard.empty_screen")
        private boolean isDashboardEmptyScreenEnabled;

        @SerializedName("prowl.dashboard.portfolio")
        private boolean isDashboardPortfolioEnabled;

        @SerializedName("prowl.dashboard.explore")
        private boolean isDashboardExploreEnabled;

        @SerializedName("prowl.dashboard.explore.create_issue")
        private boolean isExploreCreateIssueEnabled;

        @SerializedName("prowl.dashboard.explore.issuelog")
        private boolean isExploreIssueLogEnabled;

        @SerializedName("prowl.dashboard.explore.staffdetails")
        private boolean isExploreStaffDetailsEnabled;

        @SerializedName("prowl.dashboard.explore.staffincentives")
        private boolean isExploreStaffIncentivesEnabled;

        @SerializedName("prowl.dashboard.explore.history")
        private boolean isExploreHistoryEnabled;

        @SerializedName("prowl.portfolio.settings")
        private boolean isSettingsEnabled;

        @SerializedName("prowl.portfolio.settings.task_management")
        private boolean isSettingsTaskManagementEnabled;

        @SerializedName("prowl.portfolio.settings.feedback")
        private boolean isSettingsFeedbackEnabled;

        @SerializedName("prowl.portfolio.settings.contact_information")
        private boolean isSettingsContactInfoEnabled;

        @SerializedName("prowl.portfolio.settings.task_completion")
        private boolean isSettingsTaskCompletionEnabled;

        @SerializedName("prowl.portfolio.settings.signout")
        private boolean isSettingsSignoutEnabled;

        @SerializedName("prowl.portfolio.settings.version_details")
        private boolean isSettingsVersionDetailsEnabled;

        @SerializedName("prowl.portfolio.masthead")
        private boolean isPortfolioMastheadEnabled;

        @SerializedName("prowl.portfolio.masthead.header")
        private boolean isPortfolioMastheadHeaderEnabled;

        @SerializedName("prowl.portfolio.masthead.compliance")
        private boolean isPortfolioComplianceMetricEnabled;

        @SerializedName("prowl.portfolio.masthead.delight")
        private boolean isPortfolioDelightMetricEnabled;

        @SerializedName("prowl.portfolio.masthead.disaster")
        private boolean isPortfolioDisasterMetricEnabled;

        @SerializedName("prowl.portfolio.masthead.inc")
        private boolean isPortfolioIncMetricEnabled;

        @SerializedName("prowl.portfolio.masthead.task_completion")
        private boolean isPortfolioTaskCompletionMetricEnabled;

        @SerializedName("prowl.dashboard.explore.issuelog.escalate")
        private boolean isEscalateIssueEnabled;

        @SerializedName("prowl.dashboard.explore.issuelog.swipe")
        private boolean isSwipeIssueEnabled;

        @SerializedName("prowl.dashboard.explore.issuelog.swipe.update")
        private boolean isSwipeUpdateIssueEnabled;

        @SerializedName("prowl.dashboard.explore.issuelog.swipe.resolve")
        private boolean isSwipeResolveIssueEnabled;

        @SerializedName("prowl.dashboard.explore.issuelog.issue.update")
        private boolean isUpdateIssueEnabled;

        @SerializedName("prowl.dashboard.explore.issuelog.issue.resolve")
        private boolean isResolveIssueEnabled;

        @SerializedName("prowl.dashboard.explore.staffdetails.add")
        private boolean isStaffDetailsAddNewEnabled;

        @SerializedName("prowl.dashboard.explore.staffdetails.edit")
        private boolean isStaffDetailsEditEnabled;

        @SerializedName("prowl.dashboard.explore.staffdetails.delete")
        private boolean isStaffDetailsDeleteEnabled;

        @SerializedName("prowl.dashboard.explore.staffincentives.write")
        private boolean isStaffIncentivesWriteEnabled;

        @SerializedName("prowl.dashboard.explore.staffincentives.distribute")
        private boolean isStaffIncentivesDistributeEnabled;

        @SerializedName("prowl.dashboard.explore.staffincentives.edit")
        private boolean isStaffIncentivesEditEnabled;

        protected PermissionList(Parcel in) {
            isProwlEnabled = in.readByte() != 0;
            isMasterLoginEnabled = in.readByte() != 0;
            isGoogleLoginEnabled = in.readByte() != 0;
            isDashboardMastheadEnabled = in.readByte() != 0;
            isDashboardMastheadHeaderEnabled = in.readByte() != 0;
            isDashboardComplianceMetricEnabled = in.readByte() != 0;
            isDashboardDelightMetricEnabled = in.readByte() != 0;
            isDashboardDisasterMetricEnabled = in.readByte() != 0;
            isDashboardEmptyScreenEnabled = in.readByte() != 0;
            isDashboardPortfolioEnabled = in.readByte() != 0;
            isDashboardExploreEnabled = in.readByte() != 0;
            isExploreCreateIssueEnabled = in.readByte() != 0;
            isExploreIssueLogEnabled = in.readByte() != 0;
            isExploreStaffDetailsEnabled = in.readByte() != 0;
            isExploreStaffIncentivesEnabled = in.readByte() != 0;
            isExploreHistoryEnabled = in.readByte() != 0;
            isSettingsEnabled = in.readByte() != 0;
            isSettingsTaskManagementEnabled = in.readByte() != 0;
            isSettingsFeedbackEnabled = in.readByte() != 0;
            isSettingsContactInfoEnabled = in.readByte() != 0;
            isSettingsTaskCompletionEnabled = in.readByte() != 0;
            isSettingsSignoutEnabled = in.readByte() != 0;
            isSettingsVersionDetailsEnabled = in.readByte() != 0;
            isSettingsVersionDetailsEnabled = in.readByte() != 0;
            isPortfolioMastheadEnabled = in.readByte() != 0;
            isPortfolioMastheadHeaderEnabled = in.readByte() != 0;
            isPortfolioComplianceMetricEnabled = in.readByte() != 0;
            isPortfolioDelightMetricEnabled = in.readByte() != 0;
            isPortfolioDisasterMetricEnabled = in.readByte() != 0;
            isPortfolioIncMetricEnabled = in.readByte() != 0;
            isPortfolioTaskCompletionMetricEnabled = in.readByte() != 0;
            isEscalateIssueEnabled = in.readByte() != 0;
            isSwipeIssueEnabled = in.readByte() != 0;
            isSwipeUpdateIssueEnabled = in.readByte() != 0;
            isSwipeResolveIssueEnabled = in.readByte() != 0;
            isUpdateIssueEnabled = in.readByte() != 0;
            isResolveIssueEnabled = in.readByte() != 0;
            isStaffDetailsAddNewEnabled = in.readByte() != 0;
            isStaffDetailsEditEnabled = in.readByte() != 0;
            isStaffDetailsDeleteEnabled = in.readByte() != 0;
            isStaffIncentivesDistributeEnabled = in.readByte() != 0;
            isStaffIncentivesEditEnabled = in.readByte() != 0;
            isStaffIncentivesWriteEnabled = in.readByte() != 0;
        }

        public static final Creator<PermissionList> CREATOR = new Creator<PermissionList>() {
            @Override
            public PermissionList createFromParcel(Parcel in) {
                return new PermissionList(in);
            }

            @Override
            public PermissionList[] newArray(int size) {
                return new PermissionList[size];
            }
        };

        public boolean isProwlEnabled() {
            return isProwlEnabled;
        }

        public boolean isMasterLoginEnabled() {
            return isMasterLoginEnabled;
        }

        public boolean isGoogleLoginEnabled() {
            return isGoogleLoginEnabled;
        }

        public boolean isDashboardMastheadEnabled() {
            return isDashboardMastheadEnabled;
        }

        public boolean isDashboardComplianceMetricEnabled() {
            return isDashboardComplianceMetricEnabled;
        }

        public boolean isDashboardDelightMetricEnabled() {
            return isDashboardDelightMetricEnabled;
        }

        public boolean isDashboardDisasterMetricEnabled() {
            return isDashboardDisasterMetricEnabled;
        }

        public boolean isDashboardPortfolioEnabled() {
            return isDashboardPortfolioEnabled;
        }

        public boolean isDashboardExploreEnabled() {
            return isDashboardExploreEnabled;
        }

        public boolean isExploreIssueLogEnabled() {
            return isExploreIssueLogEnabled;
        }

        public boolean isExploreCreateIssueEnabled() {
            return isExploreCreateIssueEnabled;
        }

        public boolean isExploreStaffDetailsEnabled() {
            return isExploreStaffDetailsEnabled;
        }

        public boolean isExploreStaffIncentivesEnabled() {
            return isExploreStaffIncentivesEnabled;
        }

        public boolean isExploreHistoryEnabled() {
            return isExploreHistoryEnabled;
        }

        public void setProwlEnabled(boolean prowlEnabled) {
            isProwlEnabled = prowlEnabled;
        }

        public void setMasterLoginEnabled(boolean masterLoginEnabled) {
            isMasterLoginEnabled = masterLoginEnabled;
        }

        public void setGoogleLoginEnabled(boolean googleLoginEnabled) {
            isGoogleLoginEnabled = googleLoginEnabled;
        }

        public void setDashboardMastheadEnabled(boolean dashboardMastheadEnabled) {
            isDashboardMastheadEnabled = dashboardMastheadEnabled;
        }

        public void setDashboardComplianceMetricEnabled(boolean dashboardComplianceMetricEnabled) {
            isDashboardComplianceMetricEnabled = dashboardComplianceMetricEnabled;
        }

        public void setDashboardDelightMetricEnabled(boolean dashboardDelightMetricEnabled) {
            isDashboardDelightMetricEnabled = dashboardDelightMetricEnabled;
        }

        public void setDashboardDisasterMetricEnabled(boolean dashboardDisasterMetricEnabled) {
            isDashboardDisasterMetricEnabled = dashboardDisasterMetricEnabled;
        }

        public void setDashboardPortfolioEnabled(boolean dashboardPortfolioEnabled) {
            isDashboardPortfolioEnabled = dashboardPortfolioEnabled;
        }

        public void setDashboardExploreEnabled(boolean dashboardExploreEnabled) {
            isDashboardExploreEnabled = dashboardExploreEnabled;
        }

        public void setExploreCreateIssueEnabled(boolean exploreCreateIssueEnabled) {
            isExploreCreateIssueEnabled = exploreCreateIssueEnabled;
        }

        public void setExploreIssueLogEnabled(boolean exploreIssueLogEnabled) {
            isExploreIssueLogEnabled = exploreIssueLogEnabled;
        }

        public void setExploreStaffDetailsEnabled(boolean exploreStaffDetailsEnabled) {
            isExploreStaffDetailsEnabled = exploreStaffDetailsEnabled;
        }

        public void setExploreStaffIncentivesEnabled(boolean exploreStaffIncentivesEnabled) {
            isExploreStaffIncentivesEnabled = exploreStaffIncentivesEnabled;
        }

        public void setExploreHistoryEnabled(boolean exploreHistoryEnabled) {
            isExploreHistoryEnabled = exploreHistoryEnabled;
        }

        public boolean isSettingsEnabled() {
            return isSettingsEnabled;
        }

        public void setSettingsEnabled(boolean settingsEnabled) {
            isSettingsEnabled = settingsEnabled;
        }

        public boolean isSettingsTaskManagementEnabled() {
            return isSettingsTaskManagementEnabled;
        }

        public void setSettingsTaskManagementEnabled(boolean settingsTaskManagementEnabled) {
            isSettingsTaskManagementEnabled = settingsTaskManagementEnabled;
        }

        public boolean isSettingsFeedbackEnabled() {
            return isSettingsFeedbackEnabled;
        }

        public void setSettingsFeedbackEnabled(boolean settingsFeedbackEnabled) {
            isSettingsFeedbackEnabled = settingsFeedbackEnabled;
        }

        public boolean isSettingsContactInfoEnabled() {
            return isSettingsContactInfoEnabled;
        }

        public void setSettingsContactInfoEnabled(boolean settingsContactInfoEnabled) {
            isSettingsContactInfoEnabled = settingsContactInfoEnabled;
        }

        public boolean isSettingsTaskCompletionEnabled() {
            return isSettingsTaskCompletionEnabled;
        }

        public void setSettingsTaskCompletionEnabled(boolean settingsTaskCompletionEnabled) {
            isSettingsTaskCompletionEnabled = settingsTaskCompletionEnabled;
        }

        public boolean isSettingsSignoutEnabled() {
            return isSettingsSignoutEnabled;
        }

        public void setSettingsSignoutEnabled(boolean settingsSignoutEnabled) {
            isSettingsSignoutEnabled = settingsSignoutEnabled;
        }

        public boolean isSettingsVersionDetailsEnabled() {
            return isSettingsVersionDetailsEnabled;
        }

        public void setSettingsVersionDetailsEnabled(boolean settingsVersionDetailsEnabled) {
            isSettingsVersionDetailsEnabled = settingsVersionDetailsEnabled;
        }

        public boolean isPortfolioMastheadEnabled() {
            return isPortfolioMastheadEnabled;
        }

        public void setPortfolioMastheadEnabled(boolean portfolioMastheadEnabled) {
            isPortfolioMastheadEnabled = portfolioMastheadEnabled;
        }

        public boolean isPortfolioComplianceMetricEnabled() {
            return isPortfolioComplianceMetricEnabled;
        }

        public void setPortfolioComplianceMetricEnabled(boolean portfolioComplianceMetricEnabled) {
            isPortfolioComplianceMetricEnabled = portfolioComplianceMetricEnabled;
        }

        public boolean isPortfolioDelightMetricEnabled() {
            return isPortfolioDelightMetricEnabled;
        }

        public void setPortfolioDelightMetricEnabled(boolean portfolioDelightMetricEnabled) {
            isPortfolioDelightMetricEnabled = portfolioDelightMetricEnabled;
        }

        public boolean isPortfolioDisasterMetricEnabled() {
            return isPortfolioDisasterMetricEnabled;
        }

        public void setPortfolioDisasterMetricEnabled(boolean portfolioDisasterMetricEnabled) {
            isPortfolioDisasterMetricEnabled = portfolioDisasterMetricEnabled;
        }

        public boolean isPortfolioIncMetricEnabled() {
            return isPortfolioIncMetricEnabled;
        }

        public void setPortfolioIncMetricEnabled(boolean portfolioIncMetricEnabled) {
            isPortfolioIncMetricEnabled = portfolioIncMetricEnabled;
        }

        public boolean isPortfolioTaskCompletionMetricEnabled() {
            return isPortfolioTaskCompletionMetricEnabled;
        }

        public void setPortfolioTaskCompletionMetricEnabled(boolean portfolioTaskCompletionMetricEnabled) {
            isPortfolioTaskCompletionMetricEnabled = portfolioTaskCompletionMetricEnabled;
        }

        public boolean isEscalateIssueEnabled() {
            return isEscalateIssueEnabled;
        }

        public void setEscalateIssueEnabled(boolean escalateIssueEnabled) {
            isEscalateIssueEnabled = escalateIssueEnabled;
        }

        public boolean isSwipeIssueEnabled() {
            return isSwipeIssueEnabled;
        }

        public void setSwipeIssueEnabled(boolean swipeIssueEnabled) {
            isSwipeIssueEnabled = swipeIssueEnabled;
        }

        public boolean isSwipeUpdateIssueEnabled() {
            return isSwipeUpdateIssueEnabled;
        }

        public void setSwipeUpdateIssueEnabled(boolean swipeUpdateIssueEnabled) {
            isSwipeUpdateIssueEnabled = swipeUpdateIssueEnabled;
        }

        public boolean isSwipeResolveIssueEnabled() {
            return isSwipeResolveIssueEnabled;
        }

        public void setSwipeResolveIssueEnabled(boolean swipeResolveIssueEnabled) {
            isSwipeResolveIssueEnabled = swipeResolveIssueEnabled;
        }

        public boolean isUpdateIssueEnabled() {
            return isUpdateIssueEnabled;
        }

        public void setUpdateIssueEnabled(boolean updateIssueEnabled) {
            isUpdateIssueEnabled = updateIssueEnabled;
        }

        public boolean isResolveIssueEnabled() {
            return isResolveIssueEnabled;
        }

        public void setResolveIssueEnabled(boolean resolveIssueEnabled) {
            isResolveIssueEnabled = resolveIssueEnabled;
        }

        public boolean isStaffDetailsAddNewEnabled() {
            return isStaffDetailsAddNewEnabled;
        }

        public void setStaffDetailsAddNewEnabled(boolean staffDetailsAddNewEnabled) {
            isStaffDetailsAddNewEnabled = staffDetailsAddNewEnabled;
        }

        public boolean isStaffDetailsEditEnabled() {
            return isStaffDetailsEditEnabled;
        }

        public void setStaffDetailsEditEnabled(boolean staffDetailsEditEnabled) {
            isStaffDetailsEditEnabled = staffDetailsEditEnabled;
        }

        public boolean isStaffDetailsDeleteEnabled() {
            return isStaffDetailsDeleteEnabled;
        }

        public void setStaffDetailsDeleteEnabled(boolean staffDetailsDeleteEnabled) {
            isStaffDetailsDeleteEnabled = staffDetailsDeleteEnabled;
        }

        public boolean isStaffIncentivesDistributeEnabled() {
            return isStaffIncentivesDistributeEnabled;
        }

        public void setStaffIncentivesDistributeEnabled(boolean staffIncentivesDistributeEnabled) {
            isStaffIncentivesDistributeEnabled = staffIncentivesDistributeEnabled;
        }

        public boolean isStaffIncentivesEditEnabled() {
            return isStaffIncentivesEditEnabled;
        }

        public void setStaffIncentivesEditEnabled(boolean staffIncentivesEditEnabled) {
            isStaffIncentivesEditEnabled = staffIncentivesEditEnabled;
        }

        public boolean isStaffIncentivesWriteEnabled() {
            return isStaffIncentivesWriteEnabled;
        }

        public void setStaffIncentivesWriteEnabled(boolean staffIncentivesWriteEnabled) {
            isStaffIncentivesWriteEnabled = staffIncentivesWriteEnabled;
        }

        public boolean isDashboardMastheadHeaderEnabled() {
            return isDashboardMastheadHeaderEnabled;
        }

        public void setDashboardMastheadHeaderEnabled(boolean dashboardMastheadHeaderEnabled) {
            isDashboardMastheadHeaderEnabled = dashboardMastheadHeaderEnabled;
        }

        public boolean isDashboardEmptyScreenEnabled() {
            return isDashboardEmptyScreenEnabled;
        }

        public void setDashboardEmptyScreenEnabled(boolean dashboardEmptyScreenEnabled) {
            isDashboardEmptyScreenEnabled = dashboardEmptyScreenEnabled;
        }

        public boolean isPortfolioMastheadHeaderEnabled() {
            return isPortfolioMastheadHeaderEnabled;
        }

        public void setPortfolioMastheadHeaderEnabled(boolean portfolioMastheadHeaderEnabled) {
            isPortfolioMastheadHeaderEnabled = portfolioMastheadHeaderEnabled;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isProwlEnabled ? 1 : 0));
            dest.writeByte((byte) (isMasterLoginEnabled ? 1 : 0));
            dest.writeByte((byte) (isGoogleLoginEnabled ? 1 : 0));
            dest.writeByte((byte) (isDashboardMastheadEnabled ? 1 : 0));
            dest.writeByte((byte) (isDashboardMastheadHeaderEnabled ? 1 : 0));
            dest.writeByte((byte) (isDashboardComplianceMetricEnabled ? 1 : 0));
            dest.writeByte((byte) (isDashboardDelightMetricEnabled ? 1 : 0));
            dest.writeByte((byte) (isDashboardDisasterMetricEnabled ? 1 : 0));
            dest.writeByte((byte) (isDashboardEmptyScreenEnabled ? 1 : 0));
            dest.writeByte((byte) (isDashboardPortfolioEnabled ? 1 : 0));
            dest.writeByte((byte) (isDashboardExploreEnabled ? 1 : 0));
            dest.writeByte((byte) (isExploreCreateIssueEnabled ? 1 : 0));
            dest.writeByte((byte) (isExploreIssueLogEnabled ? 1 : 0));
            dest.writeByte((byte) (isExploreStaffDetailsEnabled ? 1 : 0));
            dest.writeByte((byte) (isExploreStaffIncentivesEnabled ? 1 : 0));
            dest.writeByte((byte) (isExploreHistoryEnabled ? 1 : 0));
            dest.writeByte((byte) (isSettingsEnabled ? 1 : 0));
            dest.writeByte((byte) (isSettingsTaskManagementEnabled ? 1 : 0));
            dest.writeByte((byte) (isSettingsFeedbackEnabled ? 1 : 0));
            dest.writeByte((byte) (isSettingsContactInfoEnabled ? 1 : 0));
            dest.writeByte((byte) (isSettingsTaskCompletionEnabled ? 1 : 0));
            dest.writeByte((byte) (isSettingsSignoutEnabled ? 1 : 0));
            dest.writeByte((byte) (isSettingsVersionDetailsEnabled ? 1 : 0));
            dest.writeByte((byte) (isPortfolioMastheadEnabled ? 1 : 0));
            dest.writeByte((byte) (isPortfolioMastheadHeaderEnabled ? 1 : 0));
            dest.writeByte((byte) (isPortfolioComplianceMetricEnabled ? 1 : 0));
            dest.writeByte((byte) (isPortfolioDelightMetricEnabled ? 1 : 0));
            dest.writeByte((byte) (isPortfolioDisasterMetricEnabled ? 1 : 0));
            dest.writeByte((byte) (isPortfolioIncMetricEnabled ? 1 : 0));
            dest.writeByte((byte) (isPortfolioTaskCompletionMetricEnabled ? 1 : 0));
            dest.writeByte((byte) (isEscalateIssueEnabled ? 1 : 0));
            dest.writeByte((byte) (isSwipeIssueEnabled ? 1 : 0));
            dest.writeByte((byte) (isSwipeUpdateIssueEnabled ? 1 : 0));
            dest.writeByte((byte) (isSwipeResolveIssueEnabled ? 1 : 0));
            dest.writeByte((byte) (isUpdateIssueEnabled ? 1 : 0));
            dest.writeByte((byte) (isResolveIssueEnabled ? 1 : 0));
            dest.writeByte((byte) (isStaffDetailsEditEnabled ? 1 : 0));
            dest.writeByte((byte) (isStaffDetailsDeleteEnabled ? 1 : 0));
            dest.writeByte((byte) (isStaffDetailsAddNewEnabled ? 1 : 0));
            dest.writeByte((byte) (isStaffIncentivesEditEnabled ? 1 : 0));
            dest.writeByte((byte) (isStaffIncentivesDistributeEnabled ? 1 : 0));
            dest.writeByte((byte) (isStaffIncentivesWriteEnabled ? 1 : 0));
        }
    }

    public static class UserRole implements Parcelable {

        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String name;

        @SerializedName("permissions")
        private PermissionList rolePermissions;

        protected UserRole(Parcel in) {
            id = in.readInt();
            name = in.readString();
            rolePermissions = in.readParcelable(PermissionList.class.getClassLoader());
        }

        public static final Creator<UserRole> CREATOR = new Creator<UserRole>() {
            @Override
            public UserRole createFromParcel(Parcel in) {
                return new UserRole(in);
            }

            @Override
            public UserRole[] newArray(int size) {
                return new UserRole[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        public PermissionList getRolePermissions() {
            return rolePermissions;
        }

        public void setRolePermissions(PermissionList rolePermissions) {
            this.rolePermissions = rolePermissions;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeParcelable(rolePermissions, flags);
        }
    }

    public static class HotelUserRole implements Parcelable {

        @SerializedName("roles")
        private ArrayList<UserRole> roles;

        @SerializedName("hotel_id")
        private int hotelID;

        protected HotelUserRole(Parcel in) {
            roles = in.createTypedArrayList(UserRole.CREATOR);
            hotelID = in.readInt();
        }

        public static final Creator<HotelUserRole> CREATOR = new Creator<HotelUserRole>() {
            @Override
            public HotelUserRole createFromParcel(Parcel in) {
                return new HotelUserRole(in);
            }

            @Override
            public HotelUserRole[] newArray(int size) {
                return new HotelUserRole[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        public ArrayList<UserRole> getRoles() {
            return roles;
        }

        public void setRoles(ArrayList<UserRole> roles) {
            this.roles = roles;
        }

        public int getHotelID() {
            return hotelID;
        }

        public void setHotelID(int hotelID) {
            this.hotelID = hotelID;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(roles);
            dest.writeInt(hotelID);
        }
    }
}
