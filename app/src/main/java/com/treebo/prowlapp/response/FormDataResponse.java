package com.treebo.prowlapp.response;

import com.amazonaws.com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.FormUpdateOutputModel;

/**
 * Created by sumandas on 28/04/2016.
 */
@Deprecated
public class FormDataResponse extends BaseResponse {

    @SerializedName("data")
    public FormUpdateOutputModel data;

}
