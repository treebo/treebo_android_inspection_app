package com.treebo.prowlapp.response.audit;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sumandas on 08/11/2016.
 */

public class RoomSingleResponse extends BaseResponse {

    public static final String TAG = "room_single_response";

    @SerializedName("data")
    private RoomSingleData roomSingleData;

    public RoomSingleData getRoomSingleData() {
        return roomSingleData;
    }

    public void setRoomSingleData(RoomSingleData roomSingleData) {
        this.roomSingleData = roomSingleData;
    }

    public static class RoomSingleData implements Parcelable {

        @SerializedName("room_id")
        private int mRoomId;

        @SerializedName("room_number")
        private String mRoomNo;

        @SerializedName("room_type")
        private String mRoomType;

        @SerializedName("description")
        public String mDescription;

        @SerializedName("is_ready")
        private String isReady;

        @SerializedName("is_audited")
        private String isAudited;

        @SerializedName("is_recommended")
        private String isRecommended;

        @SerializedName("is_available")
        private String isAvailable;

        @SerializedName("last_checkout_timestamp")
        private String mLastCheckoutTimeStamp;

        @SerializedName("included_categories")
        private HashMap<Integer, ArrayList<Integer>> mCheckpointList;

        @SerializedName("issues")
        private ArrayList<IssueListModelV3> mIssuesList;

        @SerializedName("audit_category_id")
        private int auditTypeId;

        @SerializedName("audit_type_description")
        private String auditTypeDescription;


        public RoomSingleData() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.mRoomId);
            dest.writeString(this.mRoomNo);
            dest.writeString(this.mRoomType);
            dest.writeString(this.mDescription);
            dest.writeString(this.isReady);
            dest.writeString(this.isAudited);
            dest.writeString(this.isRecommended);
            dest.writeString(this.isAvailable);
            dest.writeString(this.mLastCheckoutTimeStamp);
            dest.writeSerializable(this.mCheckpointList);
            dest.writeTypedList(this.mIssuesList);
            dest.writeInt(this.auditTypeId);
            dest.writeString(this.auditTypeDescription);
        }

        protected RoomSingleData(Parcel in) {
            this.mRoomId = in.readInt();
            this.mRoomNo = in.readString();
            this.mRoomType = in.readString();
            this.mDescription = in.readString();
            this.isReady = in.readString();
            this.isAudited = in.readString();
            this.isRecommended = in.readString();
            this.isAvailable = in.readString();
            this.mLastCheckoutTimeStamp = in.readString();
            this.mCheckpointList = (HashMap<Integer, ArrayList<Integer>>) in.readSerializable();
            this.mIssuesList = in.createTypedArrayList(IssueListModelV3.CREATOR);
            this.auditTypeId = in.readInt();
            this.auditTypeDescription = in.readString();
        }

        public static final Creator<RoomSingleData> CREATOR = new Creator<RoomSingleData>() {
            @Override
            public RoomSingleData createFromParcel(Parcel source) {
                return new RoomSingleData(source);
            }

            @Override
            public RoomSingleData[] newArray(int size) {
                return new RoomSingleData[size];
            }
        };

        public int getRoomId() {
            return mRoomId;
        }

        public void setRoomId(int mRoomId) {
            this.mRoomId = mRoomId;
        }

        public String getRoomNo() {
            return mRoomNo;
        }

        public void setRoomNo(String mRoomNo) {
            this.mRoomNo = mRoomNo;
        }

        public String getRoomType() {
            return mRoomType;
        }

        public void setRoomType(String mRoomType) {
            this.mRoomType = mRoomType;
        }

        public String getIsReady() {
            return isReady;
        }

        public void setIsReady(String isReady) {
            this.isReady = isReady;
        }

        public String getmDescription() {
            return mDescription;
        }

        public void setmDescription(String mDescription) {
            this.mDescription = mDescription;
        }

        public String getIsAudited() {
            return isAudited;
        }

        public void setIsAudited(String isAudited) {
            this.isAudited = isAudited;
        }

        public String getIsAvailable() {
            return isAvailable;
        }

        public void setIsAvailable(String isAvailable) {
            this.isAvailable = isAvailable;
        }

        public String getIsRecommended() {
            return isRecommended;
        }

        public void setIsRecommended(String isRecommended) {
            this.isRecommended = isRecommended;
        }

        public String getLastCheckoutTimeStamp() {
            return mLastCheckoutTimeStamp;
        }

        public void setLastCheckoutTimeStamp(String mLastCheckoutTimeStamp) {
            this.mLastCheckoutTimeStamp = mLastCheckoutTimeStamp;
        }

        public HashMap<Integer, ArrayList<Integer>> getCheckpointList() {
            return mCheckpointList;
        }

        public void setCheckpointList(HashMap<Integer, ArrayList<Integer>> mCheckpointList) {
            this.mCheckpointList = mCheckpointList;
        }

        public int getAuditTypeId() {
            return auditTypeId;
        }

        public void setAuditTypeId(int auditTypeId) {
            this.auditTypeId = auditTypeId;
        }

        public ArrayList<IssueListModelV3> getIssuesList() {
            return mIssuesList;
        }

        public void setIssuesList(ArrayList<IssueListModelV3> mIssuesList) {
            this.mIssuesList = mIssuesList;
        }

        public String getAuditTypeDescription() {
            return auditTypeDescription;
        }

        public void setAuditTypeDescription(String auditTypeDescription) {
            this.auditTypeDescription = auditTypeDescription;
        }
    }
}
