package com.treebo.prowlapp.response.audit;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.auditmodel.FraudAudit;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by abhisheknair on 17/01/17.
 */

public class FraudAuditResponse extends BaseResponse {

    @SerializedName("data")
    private FraudAudit fraudAuditData;

    public FraudAudit getFraudAuditData() {
        return fraudAuditData;
    }
}
