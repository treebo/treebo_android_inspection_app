package com.treebo.prowlapp.response.staffincentives;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 01/03/17.
 */

public class DepartmentListResponse extends BaseResponse {

    @SerializedName("data")
    private ArrayList<DepartmentObject> departmentList;

    public ArrayList<DepartmentObject> getDepartmentList() {
        return departmentList;
    }

}
