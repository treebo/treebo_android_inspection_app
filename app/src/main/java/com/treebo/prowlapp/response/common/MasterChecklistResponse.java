package com.treebo.prowlapp.response.common;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by sumandas on 09/11/2016.
 */

public class MasterChecklistResponse extends BaseResponse {

    @SerializedName("data")
    public MasterCheckListObject data;

    public static class MasterCheckListObject {

        @SerializedName("master_checklist")
        public ArrayList<Category> master_list;
    }
}
