package com.treebo.prowlapp.response.login;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;

/**
 * Created by devesh on 26/04/16.
 */
public class LoginResponse extends BaseResponse {

    @SerializedName("data")
    private LoginData data;

    @Override
    public String toString() {
        return "LoginResponse{" +
                "data=" + data +
                '}';
    }


    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }


    public static class LoginData implements Parcelable {

        @SerializedName("picture")
        private String profilePicUrl;

        @SerializedName("user_id")
        private int userId;

        @SerializedName("access_token")
        private String accessToken;

        @SerializedName("refresh_token")
        private String refreshToken;

        @SerializedName("token_type")
        private String tokenType;

        @SerializedName("expires_in")
        private String expiresIn;

        @SerializedName("email_id")
        private String emailId;

        @SerializedName("scope")
        private String scope;

        @SerializedName("message")
        private String message;

        @SerializedName("user_name")
        private String name = "";

        @SerializedName("roles")
        private RolesAndUserPermissions userRoles;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.userId);
            dest.writeString(this.accessToken);
            dest.writeString(this.refreshToken);
            dest.writeString(this.tokenType);
            dest.writeString(this.expiresIn);
            dest.writeString(this.emailId);
            dest.writeString(this.scope);
            dest.writeString(this.message);
            dest.writeString(this.name);
            dest.writeParcelable(this.userRoles, flags);
        }

        public LoginData() {
        }

        protected LoginData(Parcel in) {
            this.userId = in.readInt();
            this.accessToken = in.readString();
            this.refreshToken = in.readString();
            this.tokenType = in.readString();
            this.expiresIn = in.readString();
            this.emailId = in.readString();
            this.scope = in.readString();
            this.message = in.readString();
            this.name = in.readString();
            this.userRoles = in.readParcelable(RolesAndUserPermissions.class.getClassLoader());
        }

        public static final Creator<LoginData> CREATOR = new Creator<LoginData>() {
            @Override
            public LoginData createFromParcel(Parcel source) {
                return new LoginData(source);
            }

            @Override
            public LoginData[] newArray(int size) {
                return new LoginData[size];
            }
        };

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public void setUserEmail(String email) {
            this.emailId = email;
        }
    }

    public String getProfilePicUrl() {
        return getData().profilePicUrl;
    }

    public String getAccessToken() {
        return getData().accessToken;
    }

    public String getRefreshToken() {
        return getData().refreshToken;
    }

    public String getTokenType() {
        return getData().tokenType;
    }

    public String getExpiresIn() {
        return getData().expiresIn;
    }

    public String getScope() {
        return getData().scope;
    }

    public String getMessage() {
        return getData().message;
    }

    public String getEmailId() {
        return getData().emailId;
    }

    public int getUserId() {
        return getData().userId;
    }

    public String getUserName() {
        return getData().name;
    }

    public RolesAndUserPermissions getUserRoles() {
        return getData().userRoles;
    }

}
