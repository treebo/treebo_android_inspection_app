package com.treebo.prowlapp.response.staffincentives;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.Utils.Utils;

import java.text.DateFormatSymbols;
import java.util.ArrayList;

/**
 * Created by abhisheknair on 28/02/17.
 */

public class MonthlyIncentiveListResponse extends BaseResponse {

    @SerializedName("data")
    private ArrayList<MonthlyIncentiveListObject> monthlyIncentiveList;

    public static class MonthlyIncentiveListObject implements Parcelable {

        public MonthlyIncentiveListObject() {

        }

        @SerializedName("month")
        private int month;

        @SerializedName("year")
        private int year;

        @SerializedName("message")
        private String text;

        @SerializedName("status")
        private String status;

        @SerializedName("color")
        private String color;

        protected MonthlyIncentiveListObject(Parcel in) {
            month = in.readInt();
            year = in.readInt();
            text = in.readString();
            status = in.readString();
            color = in.readString();
        }

        public static final Creator<MonthlyIncentiveListObject> CREATOR = new Creator<MonthlyIncentiveListObject>() {
            @Override
            public MonthlyIncentiveListObject createFromParcel(Parcel in) {
                return new MonthlyIncentiveListObject(in);
            }

            @Override
            public MonthlyIncentiveListObject[] newArray(int size) {
                return new MonthlyIncentiveListObject[size];
            }
        };

        public int getMonth() {
            return month;
        }

        public int getYear() {
            return year;
        }

        public String getText() {
            return text;
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getMonthText() {
            return month != 0 ? new DateFormatSymbols().getMonths()[month - 1] : "";
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getColor() {
            return Utils.getMetricDisplayColor(color);
        }

        public void setColor(String color) {
            this.color = color;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(month);
            dest.writeInt(year);
            dest.writeString(text);
            dest.writeString(status);
            dest.writeString(color);
        }
    }

    public ArrayList<MonthlyIncentiveListObject> getMonthlyIncentiveList() {
        return monthlyIncentiveList;
    }
}
