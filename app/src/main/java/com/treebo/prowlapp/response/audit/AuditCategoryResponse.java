package com.treebo.prowlapp.response.audit;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by sumandas on 19/01/2017.
 */

public class AuditCategoryResponse extends BaseResponse {

    @SerializedName("data")
    public ArrayList<AuditCategory> auditCategories;
}
