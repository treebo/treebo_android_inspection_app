package com.treebo.prowlapp.response.staffincentives;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Utils.Utils;

import java.text.DateFormatSymbols;
import java.util.ArrayList;

/**
 * Created by abhisheknair on 02/03/17.
 */

public class StaffObject implements Parcelable {

    @SerializedName("staff_name")
    private String staffName;

    @SerializedName("staff_id")
    private int staffID;

    @SerializedName("staff_image_url")
    private String staffImageUrl;

    @SerializedName("incentive")
    private float incentive;

    @SerializedName("incentive_given")
    private float incentiveGiven;

    @SerializedName("reason")
    private String reason;

    @SerializedName("status")
    private String status;

    @SerializedName("is_eligible")
    private boolean isEligible;

    @SerializedName("starting_incentive")
    private float startingIncentive;

    @SerializedName("color")
    private String color;

    @SerializedName("history")
    private ArrayList<StaffHistory> historyArrayList;

    private boolean isPendingGiven;

    private boolean isIncentiveModified;

    public StaffObject() {

    }

    protected StaffObject(Parcel in) {
        staffName = in.readString();
        staffID = in.readInt();
        staffImageUrl = in.readString();
        incentive = in.readFloat();
        incentiveGiven = in.readFloat();
        reason = in.readString();
        status = in.readString();
        isEligible = in.readByte() != 0;
        startingIncentive = in.readFloat();
        color = in.readString();
        historyArrayList = in.createTypedArrayList(StaffHistory.CREATOR);
        isPendingGiven = in.readByte() != 0;
        isIncentiveModified = in.readByte() != 0;
    }

    public static final Creator<StaffObject> CREATOR = new Creator<StaffObject>() {
        @Override
        public StaffObject createFromParcel(Parcel in) {
            return new StaffObject(in);
        }

        @Override
        public StaffObject[] newArray(int size) {
            return new StaffObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(staffName);
        dest.writeInt(staffID);
        dest.writeString(staffImageUrl);
        dest.writeFloat(incentive);
        dest.writeFloat(incentiveGiven);
        dest.writeString(reason);
        dest.writeString(status);
        dest.writeByte((byte) (isEligible ? 1 : 0));
        dest.writeFloat(startingIncentive);
        dest.writeString(color);
        dest.writeTypedList(historyArrayList);
        dest.writeByte((byte) (isPendingGiven ? 1 : 0));
        dest.writeByte((byte) (isIncentiveModified ? 1 : 0));
    }

    public static class StaffHistory implements Parcelable {

        @SerializedName("month")
        private int month;

        @SerializedName("year")
        private int year;

        @SerializedName("incentive_given")
        private float incentiveGiven;

        protected StaffHistory(Parcel in) {
            month = in.readInt();
            year = in.readInt();
            incentiveGiven = in.readFloat();
        }

        public static final Creator<StaffHistory> CREATOR = new Creator<StaffHistory>() {
            @Override
            public StaffHistory createFromParcel(Parcel in) {
                return new StaffHistory(in);
            }

            @Override
            public StaffHistory[] newArray(int size) {
                return new StaffHistory[size];
            }
        };

        public String getMonth() {
            return month > 0 ? new DateFormatSymbols().getMonths()[month - 1] : "";
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public String getYear() {
            return String.valueOf(year);
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getIncentiveGiven() {
            return Math.round(incentiveGiven);
        }

        public void setIncentiveGiven(int incentiveGiven) {
            this.incentiveGiven = incentiveGiven;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(month);
            dest.writeInt(year);
            dest.writeFloat(incentiveGiven);
        }
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public int getStaffID() {
        return staffID;
    }

    public void setStaffID(int staffID) {
        this.staffID = staffID;
    }

    public String getStaffImageUrl() {
        return staffImageUrl;
    }

    public void setStaffImageUrl(String staffImageUrl) {
        this.staffImageUrl = staffImageUrl;
    }

    public int getIncentive() {
        return Math.round(incentive) > 0 ? Math.round(incentive) : 0;
    }

    public void setIncentive(int incentive) {
        this.incentive = incentive;
    }

    public int getIncentiveGiven() {
        return Math.round(incentiveGiven) > 0 ? Math.round(incentiveGiven) : 0;
    }

    public void setIncentiveGiven(int incentiveGiven) {
        this.incentiveGiven = incentiveGiven;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isEligible() {
        return isEligible;
    }

    public void setEligible(boolean eligible) {
        isEligible = eligible;
    }

    public int getStartingIncentive() {
        return Math.round(startingIncentive) > 0 ? Math.round(startingIncentive) : 0;
    }

    public void setStartingIncentive(int startingIncentive) {
        this.startingIncentive = startingIncentive;
    }

    public int getColor() {
        return Utils.getMetricDisplayColor(color);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public ArrayList<StaffHistory> getHistoryArrayList() {
        return historyArrayList;
    }

    public void setHistoryArrayList(ArrayList<StaffHistory> historyArrayList) {
        this.historyArrayList = historyArrayList;
    }

    public boolean isPendingGiven() {
        return isPendingGiven;
    }

    public void setPendingGiven(boolean pendingGiven) {
        isPendingGiven = pendingGiven;
    }

    public boolean isIncentiveModified() {
        return isIncentiveModified;
    }

    public void setIncentiveModified(boolean incentiveModified) {
        isIncentiveModified = incentiveModified;
    }
}
