package com.treebo.prowlapp.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.HotelDataResponse;

import java.util.ArrayList;

/**
 * Created by devesh on 03/05/16.
 */
@Deprecated
public class HotelsListResponse extends BaseResponse implements Parcelable {
    @SerializedName("data")
    public HotelsListResponseData data;

    public static class HotelsListResponseData implements Parcelable {

        @SerializedName("hotels")
        public ArrayList<HotelDataResponse> hotels;


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(hotels);
        }

        public HotelsListResponseData() {
        }

        protected HotelsListResponseData(Parcel in) {
            this.hotels = in.createTypedArrayList(HotelDataResponse.CREATOR);
        }

        public static final Creator<HotelsListResponseData> CREATOR = new Creator<HotelsListResponseData>() {
            @Override
            public HotelsListResponseData createFromParcel(Parcel source) {
                return new HotelsListResponseData(source);
            }

            @Override
            public HotelsListResponseData[] newArray(int size) {
                return new HotelsListResponseData[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.data, flags);
    }

    public HotelsListResponse() {
    }

    protected HotelsListResponse(Parcel in) {
        this.data = in.readParcelable(HotelsListResponseData.class.getClassLoader());
    }

    public static final Creator<HotelsListResponse> CREATOR = new Creator<HotelsListResponse>() {
        @Override
        public HotelsListResponse createFromParcel(Parcel source) {
            return new HotelsListResponse(source);
        }

        @Override
        public HotelsListResponse[] newArray(int size) {
            return new HotelsListResponse[size];
        }
    };

}
