package com.treebo.prowlapp.response.dashboard;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.dashboardmodel.HotelDashboardMetrics;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class HotelMetricsResponse extends BaseResponse {

    @SerializedName("data")
    public HotelDashboardMetrics data;
}
