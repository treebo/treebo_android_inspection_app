package com.treebo.prowlapp.response.dashboard;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class HotelTasksResponse extends BaseResponse {

    @SerializedName("data")
    public TaskList data;

    public static class TaskList implements Parcelable {

        @SerializedName("hot_tasks")
        public ArrayList<TaskModel> hotTasks;

        @SerializedName("pending_tasks")
        public ArrayList<TaskModel> pendingTasks;

        @SerializedName("today_tasks")
        public ArrayList<TaskModel> todayTasks;

        protected TaskList(Parcel in) {
            hotTasks = in.createTypedArrayList(TaskModel.CREATOR);
            pendingTasks = in.createTypedArrayList(TaskModel.CREATOR);
            todayTasks = in.createTypedArrayList(TaskModel.CREATOR);
        }

        public static final Creator<TaskList> CREATOR = new Creator<TaskList>() {
            @Override
            public TaskList createFromParcel(Parcel in) {
                return new TaskList(in);
            }

            @Override
            public TaskList[] newArray(int size) {
                return new TaskList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeTypedList(hotTasks);
            parcel.writeTypedList(pendingTasks);
            parcel.writeTypedList(todayTasks);
        }
    }
}
