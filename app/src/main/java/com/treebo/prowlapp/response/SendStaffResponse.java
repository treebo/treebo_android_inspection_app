package com.treebo.prowlapp.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by devesh on 02/05/16.
 */
public class SendStaffResponse extends BaseResponse {

    @SerializedName("data")
    public SendStaffResponseData data;

    public static class SendStaffResponseData {

        @SerializedName("message")
        public String message;

    }

}
