package com.treebo.prowlapp.response.portfolio;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioHotelV3;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by abhisheknair on 07/03/17.
 */

public class PortfolioSingleHotelResponse extends BaseResponse {

    @SerializedName("data")
    private PortfolioHotelV3 hotelDetails;

    public PortfolioHotelV3 getHotelDetails() {
        return hotelDetails;
    }
}
