package com.treebo.prowlapp.response.audit;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

/**
 * Created by sumandas on 03/05/2016.
 */
public class SubmitAuditResponse extends BaseResponse {
    @SerializedName("data")
    public AuditStatus data;

    public static class AuditStatus {
        @SerializedName("status")
        public String status;
    }
}
