package com.treebo.prowlapp.Models;

import com.google.gson.annotations.SerializedName;
@Deprecated
public class Answer {

    @SerializedName("query_id")
    private int query_id;
    private String answer,queryName;
    private String comment;
    private int severe;
    private String image_url;

    private boolean is_instantly_resolved;


    public Answer( String answer, String comment,int severe,String imageUrl,String queryName, int queryId) {
        this.query_id = queryId;
        this.answer = answer;
        this.comment = comment;
        this.severe=severe;
        image_url=imageUrl;
        this.queryName = queryName;
    }

    /**
     *
     * @return
     * The queryId
     */
    public int getQueryId() {
        return query_id;
    }

    /**
     *
     * @param queryId
     * The query_id
     */
    public void setQueryId(int queryId) {
        this.query_id = queryId;
    }

    /**
     *
     * @return
     * The answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     *
     * @param answer
     * The answer
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

//    public int getQuery_id() {
//        return query_id;
//    }
//
//    public void setQuery_id(int query_id) {
//        this.query_id = query_id;
//    }

    public int getSevere() {
        return severe;
    }

    public void setSevere(int severe) {
        this.severe = severe;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }
}