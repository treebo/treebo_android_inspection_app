package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
@Deprecated
public class Form implements Parcelable {

    public Form(){
        subsections = new ArrayList<>();
    }

    public Form(Parcel in){
        this();
        section=in.readString();
        in.readTypedList(subsections, Subsection.CREATOR);
    }

    @SerializedName("section")
    private String section;
    @SerializedName("subsections")
    private List<Subsection> subsections;

    /**
     *
     * @return
     * The section
     */
    public String getSection() {
        return section;
    }

    /**
     *
     * @param section
     * The section
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     *
     * @return
     * The subsections
     */
    public List<Subsection> getSubsections() {
        return subsections;
    }

    /**
     *
     * @param subsections
     * The subsections
     */
    public void setSubsections(List<Subsection> subsections) {
        this.subsections = subsections;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(section);
        dest.writeTypedList(subsections);
    }

    public static final Creator<Form> CREATOR = new Creator<Form>() {
        @Override
        public Form createFromParcel(Parcel source) {
            return new Form(source);
        }

        @Override
        public Form[] newArray(int size) {
            return new Form[size];
        }
    };

/*    public void updateQuery(UpdateQueryModel updateQueryModel){
        for(Subsection subsection:subsections){
            for(Query query:subsection.getQueries()){
                if(query.equals(updateQueryModel.mQueryId)){
                    query.setComment(updateQueryModel.mComments);
                    query.setImage_url(updateQueryModel.mImageUrls);
                    return;
                }
            }
        }
    }*/
}