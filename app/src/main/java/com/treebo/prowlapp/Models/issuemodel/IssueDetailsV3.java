package com.treebo.prowlapp.Models.issuemodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sumandas on 08/11/2016.
 */

public class IssueDetailsV3 implements Parcelable {

    @SerializedName("issue_theme")
    public String mIssueTheme;

    @SerializedName("severity")
    public int mSeverity;

    @SerializedName("repeat_pattern")
    public String mRepeatPattern;

    @SerializedName("created_date")
    public String mIssueCreatedDate;

    @SerializedName("mTurnaroundTime")
    public String mIssueTat;

    @SerializedName("issue_logs")
    public ArrayList<IssueLog> mIssueLog;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mIssueTheme);
        dest.writeInt(this.mSeverity);
        dest.writeString(this.mRepeatPattern);
        dest.writeString(this.mIssueCreatedDate);
        dest.writeString(this.mIssueTat);
        dest.writeTypedList(this.mIssueLog);
    }

    public IssueDetailsV3() {
    }

    protected IssueDetailsV3(Parcel in) {
        this.mIssueTheme = in.readString();
        this.mSeverity = in.readInt();
        this.mRepeatPattern = in.readString();
        this.mIssueCreatedDate = in.readString();
        this.mIssueTat = in.readString();
        this.mIssueLog = in.createTypedArrayList(IssueLog.CREATOR);
    }

    public static final Creator<IssueDetailsV3> CREATOR = new Creator<IssueDetailsV3>() {
        @Override
        public IssueDetailsV3 createFromParcel(Parcel source) {
            return new IssueDetailsV3(source);
        }

        @Override
        public IssueDetailsV3[] newArray(int size) {
            return new IssueDetailsV3[size];
        }
    };

    public static class IssueLog implements Parcelable{
        @SerializedName("images")
        public ArrayList<String> mImageUrls;

        @SerializedName("comment")
        public String mIssueComments;

        @SerializedName("source")
        public String mIssueSource;

        @SerializedName("resolution_date")
        public String mIssueResolutionDate;

        @SerializedName("update_date")
        public String mIssueUpdateDate;

        @SerializedName("is_user_first_action")
        public boolean isUserFirstAction;

        @SerializedName("user_id")
        public String mUserId;

        @SerializedName("user_name")
        public String mUserName;

        @SerializedName("review")
        public Review mReview;

        public IssueLog() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeStringList(this.mImageUrls);
            dest.writeString(this.mIssueComments);
            dest.writeString(this.mIssueSource);
            dest.writeString(this.mIssueResolutionDate);
            dest.writeString(this.mIssueUpdateDate);
            dest.writeByte(this.isUserFirstAction ? (byte) 1 : (byte) 0);
            dest.writeString(this.mUserId);
            dest.writeString(this.mUserName);
            dest.writeParcelable(this.mReview, flags);
        }

        protected IssueLog(Parcel in) {
            this.mImageUrls = in.createStringArrayList();
            this.mIssueComments = in.readString();
            this.mIssueSource = in.readString();
            this.mIssueResolutionDate = in.readString();
            this.mIssueUpdateDate = in.readString();
            this.isUserFirstAction = in.readByte() != 0;
            this.mUserId = in.readString();
            this.mUserName = in.readString();
            this.mReview = in.readParcelable(Review.class.getClassLoader());
        }

        public static final Creator<IssueLog> CREATOR = new Creator<IssueLog>() {
            @Override
            public IssueLog createFromParcel(Parcel source) {
                return new IssueLog(source);
            }

            @Override
            public IssueLog[] newArray(int size) {
                return new IssueLog[size];
            }
        };
    }

    public static class Review implements Parcelable{

        @SerializedName("text")
        public String mText;

        @SerializedName("title")
        public String mTitle;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.mText);
            dest.writeString(this.mTitle);
        }

        public Review() {
        }

        protected Review(Parcel in) {
            this.mText = in.readString();
            this.mTitle = in.readString();
        }

        public static final Creator<Review> CREATOR = new Creator<Review>() {
            @Override
            public Review createFromParcel(Parcel source) {
                return new Review(source);
            }

            @Override
            public Review[] newArray(int size) {
                return new Review[size];
            }
        };
    }
}
