package com.treebo.prowlapp.Models.issuemodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.Utils.Utils;

/**
 * Created by abhisheknair on 17/11/16.
 */

public class IssueListModelV3 implements Parcelable, Comparable<IssueListModelV3> {

    @SerializedName("checkpoint_name")
    private String checkpointText;

    @SerializedName("checkpoint_id")
    private int checkpointID;

    @SerializedName("category_name")
    private String categoryText;

    @SerializedName("subcheckpoint_name")
    private String subCheckpointText;

    @SerializedName("subcheckpoint_id")
    private int subCheckpointID;

    @SerializedName("room_number")
    private String roomNumber;

    @SerializedName("room_id")
    private String roomID;

    @SerializedName("audit_category_id")
    private int auditCategoryID;

    @SerializedName("audit_type")
    private String auditType;

    @SerializedName("creation_date")
    private String createdAt;

    @SerializedName("severity")
    private int severity;

    @SerializedName("issue_id")
    private int issueID;

    @SerializedName("issue_theme")
    private String issueTheme;

    @SerializedName("resolution_date")
    private String resolutionDate;

    @SerializedName("is_inc")
    private boolean isIssueNotCaught;

    @SerializedName("is_dnr_marked")
    private boolean isDnr;

    @SerializedName("hotel_name")
    private String hotelName;

    @SerializedName("status")
    private String status;

    private String dueDateText;

    private String sectionName;

    private boolean isOverdue;

    public IssueListModelV3() {

    }

    protected IssueListModelV3(Parcel in) {
        checkpointText = in.readString();
        checkpointID = in.readInt();
        categoryText = in.readString();
        subCheckpointText = in.readString();
        subCheckpointID = in.readInt();
        roomNumber = in.readString();
        roomID = in.readString();
        auditCategoryID = in.readInt();
        auditType = in.readString();
        createdAt = in.readString();
        severity = in.readInt();
        issueID = in.readInt();
        issueTheme = in.readString();
        resolutionDate = in.readString();
        isIssueNotCaught = in.readByte() != 0;
        isDnr = in.readByte() != 0;
        dueDateText = in.readString();
        sectionName = in.readString();
        hotelName = in.readString();
        status = in.readString();
    }

    public static final Creator<IssueListModelV3> CREATOR = new Creator<IssueListModelV3>() {
        @Override
        public IssueListModelV3 createFromParcel(Parcel in) {
            return new IssueListModelV3(in);
        }

        @Override
        public IssueListModelV3[] newArray(int size) {
            return new IssueListModelV3[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(checkpointText);
        parcel.writeInt(checkpointID);
        parcel.writeString(categoryText);
        parcel.writeString(subCheckpointText);
        parcel.writeInt(subCheckpointID);
        parcel.writeString(roomNumber);
        parcel.writeString(roomID);
        parcel.writeInt(auditCategoryID);
        parcel.writeString(auditType);
        parcel.writeString(createdAt);
        parcel.writeInt(severity);
        parcel.writeInt(issueID);
        parcel.writeString(issueTheme);
        parcel.writeString(resolutionDate);
        parcel.writeByte(isIssueNotCaught ? (byte) 1 : (byte) 0);
        parcel.writeByte(isDnr ? (byte) 1 : (byte) 0);
        parcel.writeString(dueDateText);
        parcel.writeString(sectionName);
        parcel.writeString(hotelName);
        parcel.writeString(status);
    }

    public int getIssueID() {
        return issueID;
    }

    public void setIssueID(int issueID) {
        this.issueID = issueID;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getCheckpointID() {
        return checkpointID;
    }

    public void setCheckpointID(int checkpointID) {
        this.checkpointID = checkpointID;
    }

    public int getSubCheckpointID() {
        return subCheckpointID;
    }

    public void setSubCheckpointID(int subCheckpointID) {
        this.subCheckpointID = subCheckpointID;
    }

    public int getAuditCategoryID() {
        return auditCategoryID;
    }

    public void setAuditCategoryID(int auditCategoryID) {
        this.auditCategoryID = auditCategoryID;
    }

    public String getAuditType() {
        return auditType.toLowerCase();
    }

    public void setAuditType(String auditType) {
        this.auditType = auditType;
    }

    public String getRoomID() {
        return roomID;
    }

    public void setRoomID(String roomID) {
        this.roomID = roomID;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getCategoryText() {
        return categoryText;
    }

    public void setCategoryText(String categoryText) {
        this.categoryText = categoryText;
    }

    public String getCheckpointText() {
        return checkpointText;
    }

    public void setCheckpointText(String checkpointText) {
        this.checkpointText = checkpointText;
    }

    public String getSubCheckpointText() {
        return subCheckpointText;
    }

    public void setSubCheckpointText(String subCheckpointText) {
        this.subCheckpointText = subCheckpointText;
    }

    public String getIssueTheme() {
        return issueTheme;
    }

    public void setIssueTheme(String issueTheme) {
        this.issueTheme = issueTheme;
    }

    public String getResolutionDate() {
        return resolutionDate;
    }

    public void setResolutionDate(String resolutionDate) {
        this.resolutionDate = resolutionDate;
    }

    public String getDueDateText() {
        return dueDateText;
    }

    public void setDueDateText() {
        this.dueDateText = DateUtils.getIssueDueDate(this.resolutionDate);
        if (dueDateText.contains("-"))
            setOverdue(true);
        else
            setOverdue(false);
    }

    public String getSectionName() {
        return String.valueOf(sectionName);
    }

    public void setSectionName(int sortCriteria) {
        this.sectionName = getSectionDataGivenSortCriteria(sortCriteria);
    }

    public boolean isOverdue() {
        return isOverdue;
    }

    public void setOverdue(boolean overdue) {
        isOverdue = overdue;
    }


    private String getSectionDataGivenSortCriteria(int sortCriteria) {
        switch (sortCriteria) {
            case Utils.DATE_NEW_OLD:
                return DateUtils.getCreatedAtDateComparedToToday(this.createdAt);

            case Utils.DATE_OLD_NEW:
                return DateUtils.getDisplayDateSectionHeaderFromTimeStamp(this.createdAt);

            case Utils.ISSUE_THEME:
                return this.issueTheme = (this.issueTheme != null) ? this.issueTheme : "";

            case Utils.SEVERITY_HIGH_LOW:
            case Utils.SEVERITY_LOW_HIGH:
                return Utils.getSeverityText(this.severity);

            case Utils.RESOLUTION_DATE:
                return DateUtils.getOverDueResolutionDate(this.resolutionDate);

            case Utils.SORT_BY_ROOM:
                return this.roomNumber;

            default:
                return "";
        }
    }

    public boolean issueNotCaught() {
        return isIssueNotCaught;
    }

    public boolean isDnr() {
        return isDnr;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public int compareTo(IssueListModelV3 issueListModelV3) {
        return this.resolutionDate.compareTo(issueListModelV3.getResolutionDate());
    }
}
