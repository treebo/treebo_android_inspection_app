package com.treebo.prowlapp.Models.dashboardmodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sumandas on 30/11/2016.
 */

public class IssueTaskModel {

    @SerializedName("overdue_issues")
    public int mOverdues;

    @SerializedName("open_issues")
    public int mToday;
}
