package com.treebo.prowlapp.Models.issuemodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 18/11/16.
 */

public class UpdateIssueModelV3 implements Parcelable {

    private String resolutionDate;
    private int userID;
    private String comment;
    private ArrayList<String> imageList;
    private String status;
    private int taskLogID;

    public UpdateIssueModelV3() {

    }

    public UpdateIssueModelV3(String resolutionDate, int userID, String comment,
                              ArrayList<String> imageList, String status, int taskLogID) {
        this.resolutionDate = resolutionDate;
        this.userID = userID;
        this.comment = comment;
        this.imageList = imageList;
        this.status = status;
        this.taskLogID = taskLogID;
    }

    protected UpdateIssueModelV3(Parcel in) {
        resolutionDate = in.readString();
        userID = in.readInt();
        comment = in.readString();
        imageList = in.createStringArrayList();
        status = in.readString();
        taskLogID = in.readInt();
    }

    public static final Creator<UpdateIssueModelV3> CREATOR = new Creator<UpdateIssueModelV3>() {
        @Override
        public UpdateIssueModelV3 createFromParcel(Parcel in) {
            return new UpdateIssueModelV3(in);
        }

        @Override
        public UpdateIssueModelV3[] newArray(int size) {
            return new UpdateIssueModelV3[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(resolutionDate);
        parcel.writeInt(userID);
        parcel.writeString(comment);
        parcel.writeStringList(imageList);
        parcel.writeString(status);
        parcel.writeInt(taskLogID);
    }

    public String getResolutionDate() {
        return resolutionDate;
    }

    public void setResolutionDate(String date) {
        this.resolutionDate = date;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setImageList(ArrayList<String> imageList) {
        this.imageList = imageList;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUserID() {
        return userID;
    }

    public String getComment() {
        return comment;
    }

    public ArrayList<String> getImageList() {
        return imageList;
    }

    public String getStatus() {
        return status;
    }

    public int getTaskLogID() {
        return taskLogID;
    }
}
