package com.treebo.prowlapp.Models;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.List;

/**
 * Created by devesh on 28/04/16.
 */
public class HotelStaffModelResponse extends BaseResponse {

    @SerializedName("data")
    public HotelStaffData data;

    public static class HotelStaffData{
        @SerializedName("hotel")
        String hotel;
        @SerializedName("staff")
        List<StaffModel> staffModelList;
    }

    public List<StaffModel> getStaffModelList(){
        return data.staffModelList;
    }


}
