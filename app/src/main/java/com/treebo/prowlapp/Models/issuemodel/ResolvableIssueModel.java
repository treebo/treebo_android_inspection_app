package com.treebo.prowlapp.Models.issuemodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abhisheknair on 08/11/16.
 */

public class ResolvableIssueModel implements Parcelable {

    @SerializedName("issueId")
    private int id;

    private String issueRootCategory;

    @SerializedName("root_cause")
    private String issueRootCause;

    @SerializedName("opensince")
    private String issueCreatedTime;

    public ResolvableIssueModel() {

    }

    protected ResolvableIssueModel(Parcel in) {
        id = in.readInt();
        issueRootCategory = in.readString();
        issueRootCause = in.readString();
        issueCreatedTime = in.readString();
    }

    public static final Creator<ResolvableIssueModel> CREATOR = new Creator<ResolvableIssueModel>() {
        @Override
        public ResolvableIssueModel createFromParcel(Parcel in) {
            return new ResolvableIssueModel(in);
        }

        @Override
        public ResolvableIssueModel[] newArray(int size) {
            return new ResolvableIssueModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(issueRootCategory);
        parcel.writeString(issueRootCause);
        parcel.writeString(issueCreatedTime);
    }

    public String getIssueRootCause() {
        return issueRootCause;
    }

    public void setIssueRootCause(String issueRootCause) {
        this.issueRootCause = issueRootCause;
    }

    public String getIssueRootCategory() {
        return issueRootCategory;
    }

    public void setIssueRootCategory(String issueRootCategory) {
        this.issueRootCategory = issueRootCategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIssueCreatedTime() {
        return issueCreatedTime;
    }

    public void setIssueCreatedTime(String issueCreatedTime) {
        this.issueCreatedTime = issueCreatedTime;
    }
}
