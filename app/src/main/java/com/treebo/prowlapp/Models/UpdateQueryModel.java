package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sumandas on 02/05/2016.
 */
public class UpdateQueryModel implements Parcelable {
    public String mQueryId="";
    public String mComments="";
    //public String mImageUrls="";

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mQueryId);
        dest.writeString(this.mComments);
        //dest.writeString(this.mImageUrls);
    }

    public UpdateQueryModel() {
    }

    protected UpdateQueryModel(Parcel in) {
        this.mQueryId = in.readString();
        this.mComments = in.readString();
        //this.mImageUrls = in.readString();
    }

    public static final Creator<UpdateQueryModel> CREATOR = new Creator<UpdateQueryModel>() {
        @Override
        public UpdateQueryModel createFromParcel(Parcel source) {
            return new UpdateQueryModel(source);
        }

        @Override
        public UpdateQueryModel[] newArray(int size) {
            return new UpdateQueryModel[size];
        }
    };
}
