package com.treebo.prowlapp.Models.auditmodel;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.treebo.prowlapp.dbflow.ProwlDatabase;

/**
 * Created by sumandas on 01/12/2016.
 */
@Table(database = ProwlDatabase.class)
public class AuditTypeMap extends BaseModel {


    public static final String COMMON_AUDIT = "common";
    public static final String ROOM_AUDIT = "room";

    @Column
    @PrimaryKey
    public int mAuditId;

    @Column
    public String mAuditType;

    public static void saveAuditType(int auditId, String auditType) {
        AuditTypeMap auditTypeMap = SQLite.select().from(AuditTypeMap.class)
                .where(AuditTypeMap_Table.mAuditId.eq(auditId))
                .querySingle();
        if (auditTypeMap == null) {
            auditTypeMap = new AuditTypeMap();
            auditTypeMap.mAuditId = auditId;
            auditTypeMap.mAuditType = auditType;
            auditTypeMap.save();
        }

    }

    public static String getAuditTypeFromId(int auditId) {
        AuditTypeMap auditTypeMap = SQLite.select().from(AuditTypeMap.class)
                .where(AuditTypeMap_Table.mAuditId.eq(auditId))
                .querySingle();
        if (auditTypeMap == null) {
            return "";
        } else {
            return auditTypeMap.mAuditType;
        }
    }
}
