package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sumandas on 21/10/2016.
 */

public class HotelDataResponse implements Parcelable {

    @SerializedName("city")
    public String hotelCity;

    @SerializedName("number_of_hotels")
    public int noOfHotels;

    @SerializedName("locality")
    public String locality;

    @SerializedName("name")
    public String hotelName;

    @SerializedName("hotel")
    public int hotelId;


    @SerializedName("online_rating")
    public double hotelOnlineRating;

    @SerializedName("open_issues")
    public String hotelOpenIssues;

    @SerializedName("qam_audit_percentage")
    public String hotelQamAuditPercentage;

    @SerializedName("last_audit_date")
    public String lastAuditDate;

    @SerializedName("current_audit_time")
    public String lastAuditTime;


    public HotelDataResponse() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.hotelCity);
        dest.writeInt(this.noOfHotels);
        dest.writeString(this.locality);
        dest.writeString(this.hotelName);
        dest.writeInt(this.hotelId);
        dest.writeDouble(this.hotelOnlineRating);
        dest.writeString(this.hotelOpenIssues);
        dest.writeString(this.hotelQamAuditPercentage);
        dest.writeString(this.lastAuditDate);
        dest.writeString(this.lastAuditTime);
    }

    protected HotelDataResponse(Parcel in) {
        this.hotelCity = in.readString();
        this.noOfHotels = in.readInt();
        this.locality = in.readString();
        this.hotelName = in.readString();
        this.hotelId = in.readInt();
        this.hotelOnlineRating = in.readDouble();
        this.hotelOpenIssues = in.readString();
        this.hotelQamAuditPercentage = in.readString();
        this.lastAuditDate = in.readString();
        this.lastAuditTime = in.readString();
    }

    public static final Creator<HotelDataResponse> CREATOR = new Creator<HotelDataResponse>() {
        @Override
        public HotelDataResponse createFromParcel(Parcel source) {
            return new HotelDataResponse(source);
        }

        @Override
        public HotelDataResponse[] newArray(int size) {
            return new HotelDataResponse[size];
        }
    };


    public String getHotelCity() {
        return hotelCity;
    }

    public String getHotelName() {
        return hotelName;
    }

    public int getHotelId() {
        return hotelId;
    }

    public double getHotelOnlineRating() {
        return hotelOnlineRating;
    }

    public String getHotelOpenIssues() {
        return hotelOpenIssues;
    }

    public String getHotelQamAuditPercentage() {
        return hotelQamAuditPercentage;
    }

    public String getLocality() {
        return locality;
    }

    public String getLastAuditDate() {
        return lastAuditDate;
    }

    public String getLastAuditTime() {
        return lastAuditTime;
    }

}
