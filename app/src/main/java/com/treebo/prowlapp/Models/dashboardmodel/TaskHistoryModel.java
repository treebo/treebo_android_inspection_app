package com.treebo.prowlapp.Models.dashboardmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class TaskHistoryModel implements Parcelable {

    @SerializedName("task_type")
    private String taskType;

    @SerializedName("last_updated_time")
    private String updatedAt;

    @SerializedName("hotel_name")
    private String hotelName;

    @SerializedName("issue")
    private Issue issueObject;

    @SerializedName("audit")
    private Audit auditObject;

    @SerializedName("manual")
    private Manual manualObject;

    public String getTaskType() {
        return taskType;
    }

    public String getUpdatedAt() {
        if (updatedAt.length() > 9)
            return updatedAt.substring(0, 10);
        else
            return updatedAt;
    }

    public String getHotelName() {
        return this.hotelName;
    }

    public Issue getIssueObject() {
        return issueObject;
    }

    public Audit getAuditObject() {
        return auditObject;
    }

    public Manual getManualObject() {
        return manualObject;
    }

    protected TaskHistoryModel(Parcel in) {
        taskType = in.readString();
        updatedAt = in.readString();
        hotelName = in.readString();
        issueObject = in.readParcelable(Issue.class.getClassLoader());
        auditObject = in.readParcelable(Audit.class.getClassLoader());
        manualObject = in.readParcelable(Manual.class.getClassLoader());
    }

    public static final Creator<TaskHistoryModel> CREATOR = new Creator<TaskHistoryModel>() {
        @Override
        public TaskHistoryModel createFromParcel(Parcel in) {
            return new TaskHistoryModel(in);
        }

        @Override
        public TaskHistoryModel[] newArray(int size) {
            return new TaskHistoryModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(taskType);
        parcel.writeString(updatedAt);
        parcel.writeString(hotelName);
        parcel.writeParcelable(issueObject, i);
        parcel.writeParcelable(auditObject, i);
        parcel.writeParcelable(manualObject, i);
    }

    public static class Issue implements Parcelable {

        @SerializedName("status")
        private String status = "";

        @SerializedName("subcheckpoint_id")
        private int subcheckpointID;

        @SerializedName("room_type")
        private int roomType;

        @SerializedName("room_number")
        private String roomNumber = "";

        protected Issue(Parcel in) {
            status = in.readString();
            subcheckpointID = in.readInt();
            roomType = in.readInt();
            roomNumber = in.readString();
        }

        public static final Creator<Issue> CREATOR = new Creator<Issue>() {
            @Override
            public Issue createFromParcel(Parcel in) {
                return new Issue(in);
            }

            @Override
            public Issue[] newArray(int size) {
                return new Issue[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(status);
            parcel.writeInt(subcheckpointID);
            parcel.writeInt(roomType);
            parcel.writeString(roomNumber);
        }

        public int getSubcheckpointID() {
            return subcheckpointID;
        }

        public int getRoomType() {
            return roomType;
        }

        public String getRoomNumber() {
            return roomNumber;
        }

        public boolean getResolutionStatus() {
            return "resolved".equals(this.status.toLowerCase());
        }
    }

    public static class Audit implements Parcelable {

        @SerializedName("type")
        private int auditType;

        protected Audit(Parcel in) {
            auditType = in.readInt();
        }

        public static final Creator<Audit> CREATOR = new Creator<Audit>() {
            @Override
            public Audit createFromParcel(Parcel in) {
                return new Audit(in);
            }

            @Override
            public Audit[] newArray(int size) {
                return new Audit[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(auditType);
        }

        public int getAuditType() {
            return auditType;
        }
    }

    public static class Manual implements Parcelable {

        @SerializedName("headline_text")
        private String headlineText;

        protected Manual(Parcel in) {
            headlineText = in.readString();
        }

        public static final Creator<Manual> CREATOR = new Creator<Manual>() {
            @Override
            public Manual createFromParcel(Parcel in) {
                return new Manual(in);
            }

            @Override
            public Manual[] newArray(int size) {
                return new Manual[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(headlineText);
        }

        public String getHeadlineText() {
            return headlineText;
        }
    }

}
