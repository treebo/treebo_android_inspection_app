package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by devesh on 01/05/16.
 */

public class SubHeadingScoresModel implements Parcelable {

    public double current;

    public String name;

    public double last_seven_days;

    public double last_thirty_days;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.current);
        dest.writeString(this.name);
        dest.writeDouble(this.last_seven_days);
        dest.writeDouble(this.last_thirty_days);
    }

    public SubHeadingScoresModel() {
    }

    protected SubHeadingScoresModel(Parcel in) {
        this.current = in.readDouble();
        this.name = in.readString();
        this.last_seven_days = in.readDouble();
        this.last_thirty_days = in.readDouble();
    }

    public static final Creator<SubHeadingScoresModel> CREATOR = new Creator<SubHeadingScoresModel>() {
        @Override
        public SubHeadingScoresModel createFromParcel(Parcel source) {
            return new SubHeadingScoresModel(source);
        }

        @Override
        public SubHeadingScoresModel[] newArray(int size) {
            return new SubHeadingScoresModel[size];
        }
    };
}