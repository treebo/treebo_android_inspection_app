package com.treebo.prowlapp.Models.dashboardmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Utils.Utils;

import static com.treebo.prowlapp.Utils.Constants.AUDIT_TASK_COMPLETED_STATE;

/**
 * Created by abhisheknair on 28/11/16.
 */

public class TaskModel implements Parcelable {

    @SerializedName("task_id")
    private int taskID;

    @SerializedName("task_type")
    private String taskCardType;

    @SerializedName("task_log_id")
    private int taskLogID;

    @SerializedName("geofence_enabled")
    private boolean isGeofenceEnabled;

    @SerializedName("headline_text")
    private String heading;

    @SerializedName("content")
    private String info;

    @SerializedName("content_link")
    private String contentLink;

    @SerializedName("completion_date")
    private String completionDate;

    @SerializedName("expiry_date")
    private String expiryDate;

    @SerializedName("tooltip")
    private String toolTipText;

    @SerializedName("action")
    private JsonObject action;

    public String sectionName;

    public boolean isPendingDelete;

    public boolean isSoftDeleted;

    private IssueTaskModel localIssueModel;

    @SerializedName("is_always_visible")
    private boolean isAlwaysVisible;

    @SerializedName("start_time")
    private String startTime;

    @SerializedName("end_time")
    private String endTime;

    @SerializedName("audit_task_type")
    private int auditTaskType;

    @SerializedName("visual_representation_type")
    private int visualRepresentationType;

    @SerializedName("month")
    private int month;

    @SerializedName("year")
    private int year;

    @SerializedName("total")
    private float totalIncentive;

    @SerializedName("distribute")
    private boolean distribute;

    @SerializedName("status")
    private String taskStatus = "";


    public TaskModel() {

    }

    protected TaskModel(Parcel in) {
        taskID = in.readInt();
        taskCardType = in.readString();
        taskLogID = in.readInt();
        isGeofenceEnabled = in.readByte() != 0;
        heading = in.readString();
        info = in.readString();
        contentLink = in.readString();
        completionDate = in.readString();
        expiryDate = in.readString();
        toolTipText = in.readString();
        sectionName = in.readString();
        isPendingDelete = in.readByte() != 0;
        isSoftDeleted = in.readByte() != 0;
        isAlwaysVisible = in.readByte() != 0;
        startTime = in.readString();
        endTime = in.readString();
        auditTaskType = in.readInt();
        visualRepresentationType = in.readInt();
        month = in.readInt();
        year = in.readInt();
        distribute = in.readByte() != 0;
        totalIncentive = in.readFloat();
        taskStatus = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(taskID);
        dest.writeString(taskCardType);
        dest.writeInt(taskLogID);
        dest.writeByte((byte) (isGeofenceEnabled ? 1 : 0));
        dest.writeString(heading);
        dest.writeString(info);
        dest.writeString(contentLink);
        dest.writeString(completionDate);
        dest.writeString(expiryDate);
        dest.writeString(toolTipText);
        dest.writeString(sectionName);
        dest.writeByte((byte) (isPendingDelete ? 1 : 0));
        dest.writeByte((byte) (isSoftDeleted ? 1 : 0));
        dest.writeByte((byte) (isAlwaysVisible ? 1 : 0));
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeInt(auditTaskType);
        dest.writeInt(visualRepresentationType);
        dest.writeInt(month);
        dest.writeInt(year);
        dest.writeByte((byte) (distribute ? 1 : 0));
        dest.writeFloat(totalIncentive);
        dest.writeString(taskStatus);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaskModel> CREATOR = new Creator<TaskModel>() {
        @Override
        public TaskModel createFromParcel(Parcel in) {
            return new TaskModel(in);
        }

        @Override
        public TaskModel[] newArray(int size) {
            return new TaskModel[size];
        }
    };

    public int getTaskID() {
        return taskID;
    }

    public int getTaskCardType() {
        if ("complete".equals(taskStatus.toLowerCase()) && "Audit".equals(taskCardType))
            return AUDIT_TASK_COMPLETED_STATE;
        else
            return Utils.getTaskTypeFromText(taskCardType);
    }

    public String getTaskTypeString() {
        return this.taskCardType;
    }

    public String getHeading() {
        return heading;
    }

    public boolean isGeofenceEnabled() {
        return isGeofenceEnabled;
    }

    public String getInfo() {
        return info;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public String getToolTipText() {
        return toolTipText;
    }

    public void setTaskID(int taskID) {
        this.taskID = taskID;
    }

    public void setTaskCardType(String taskCardType) {
        this.taskCardType = taskCardType;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public void setToolTipText(String toolTipText) {
        this.toolTipText = toolTipText;
    }

    public int getTaskLogID() {
        return taskLogID;
    }

    public void setGeofenceEnabled(boolean geofenceEnabled) {
        isGeofenceEnabled = geofenceEnabled;
    }

    public String getContentLink() {
        return contentLink;
    }

    public void setContentLink(String contentLink) {
        this.contentLink = contentLink;
    }

    public boolean isAlwaysVisible() {
        return isAlwaysVisible;
    }

    public void setAlwaysVisible(boolean alwaysVisible) {
        isAlwaysVisible = alwaysVisible;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getAuditTaskType() {
        return auditTaskType;
    }

    public void setAuditTaskType(int auditTaskType) {
        this.auditTaskType = auditTaskType;
    }

    public int getVisualRepresentationType() {
        return visualRepresentationType;
    }

    public void setVisualRepresentationType(int visualRepresentationType) {
        this.visualRepresentationType = visualRepresentationType;
    }


    public AuditTaskModel getAuditTaskModel() {
        return new Gson().fromJson(this.action, AuditTaskModel.class);
    }

    public IssueTaskModel getIssueTaskModel() {
        if (localIssueModel == null)
            localIssueModel = new Gson().fromJson(this.action, IssueTaskModel.class);
        return localIssueModel;
    }

    public void setLocalIssueModel(IssueTaskModel issue) {
        this.localIssueModel = issue;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isDistribute() {
        return distribute;
    }

    public void setDistribute(boolean distribute) {
        this.distribute = distribute;
    }

    public int getTotalIncentive() {
        return (totalIncentive < 0) ? 0 : Math.round(totalIncentive);
    }

    public String getTaskStatus() {
        return taskStatus == null ? "" : taskStatus.toLowerCase();
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }


}
