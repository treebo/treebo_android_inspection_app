package com.treebo.prowlapp.Models.portfoliomodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abhisheknair on 24/11/16.
 */

public class PortfolioHotelV3 implements Parcelable {

    @SerializedName("id")
    private int hotelID;

    @SerializedName("name")
    private String hotelName;

    @SerializedName("compliance")
    private int compliancePercent;

    @SerializedName("issues")
    private int openIssueCount;

    @SerializedName("pending_task")
    private int pendingTaskCount;

    private boolean isInCurrentLocation;

    public PortfolioHotelV3() {
    }

    protected PortfolioHotelV3(Parcel in) {
        hotelID = in.readInt();
        hotelName = in.readString();
        compliancePercent = in.readInt();
        openIssueCount = in.readInt();
        pendingTaskCount = in.readInt();
        isInCurrentLocation = in.readByte() != 0;
    }

    public static final Creator<PortfolioHotelV3> CREATOR = new Creator<PortfolioHotelV3>() {
        @Override
        public PortfolioHotelV3 createFromParcel(Parcel in) {
            return new PortfolioHotelV3(in);
        }

        @Override
        public PortfolioHotelV3[] newArray(int size) {
            return new PortfolioHotelV3[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(hotelID);
        parcel.writeString(hotelName);
        parcel.writeInt(compliancePercent);
        parcel.writeInt(openIssueCount);
        parcel.writeInt(pendingTaskCount);
        parcel.writeByte((byte) (isInCurrentLocation ? 1 : 0));
    }

    public void setHotelID(int hotelID) {
        this.hotelID = hotelID;
    }

    public String getHotelName() {
        return hotelName;
    }

    public int getHotelID() {
        return hotelID;
    }

    public int getCompliancePercent() {
        return compliancePercent;
    }

    public int getOpenIssueCount() {
        return openIssueCount;
    }

    public int getPendingTaskCount() {
        return pendingTaskCount;
    }

    public boolean isInCurrentLocation() {
        return isInCurrentLocation;
    }

    public void setInCurrentLocation(boolean inCurrentLocation) {
        isInCurrentLocation = inCurrentLocation;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public void setCompliancePercent(int compliancePercent) {
        this.compliancePercent = compliancePercent;
    }

    public void setOpenIssueCount(int openIssueCount) {
        this.openIssueCount = openIssueCount;
    }

    public void setPendingTaskCount(int pendingTaskCount) {
        this.pendingTaskCount = pendingTaskCount;
    }

    public boolean isInfoCallRequired() {
        return ((this.compliancePercent == 0) && (this.pendingTaskCount == 0) && (this.openIssueCount == 0));
    }
}
