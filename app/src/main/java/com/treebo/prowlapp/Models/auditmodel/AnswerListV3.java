package com.treebo.prowlapp.Models.auditmodel;



import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sumandas on 11/11/2016.
 */

public class AnswerListV3 {

    @SerializedName("listAnswer")
    public ArrayList<AnswerV3> listAnswer;

    public AnswerListV3(ArrayList<AnswerV3> listAnswer) {
        this.listAnswer = listAnswer;
    }
}
