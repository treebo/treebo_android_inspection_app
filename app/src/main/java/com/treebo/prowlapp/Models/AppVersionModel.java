package com.treebo.prowlapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sumandas on 20/05/2016.
 */
public class AppVersionModel {

    @SerializedName("version")
    public String version;

    @SerializedName("appId")
    public String appId;

    @SerializedName("apkLink")
    public String apkLink;
}
