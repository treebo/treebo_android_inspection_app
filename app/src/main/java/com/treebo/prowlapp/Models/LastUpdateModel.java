package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sumandas on 10/05/2016.
 */
@Deprecated
public class LastUpdateModel implements Parcelable {

    public String lastUpdate="";
    public String time="";

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.lastUpdate);
        dest.writeString(this.time);
    }

    public LastUpdateModel() {

    }

    protected LastUpdateModel(Parcel in) {
        this.lastUpdate = in.readString();
        this.time = in.readString();
    }

    public static final Creator<LastUpdateModel> CREATOR = new Creator<LastUpdateModel>() {
        @Override
        public LastUpdateModel createFromParcel(Parcel source) {
            return new LastUpdateModel(source);
        }

        @Override
        public LastUpdateModel[] newArray(int size) {
            return new LastUpdateModel[size];
        }
    };
}
