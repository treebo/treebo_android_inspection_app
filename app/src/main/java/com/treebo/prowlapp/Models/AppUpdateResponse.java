package com.treebo.prowlapp.Models;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by sumandas on 20/05/2016.
 */
public class AppUpdateResponse extends BaseResponse {

    @SerializedName("data")
    public ArrayList<AppVersionModel> data;

}
