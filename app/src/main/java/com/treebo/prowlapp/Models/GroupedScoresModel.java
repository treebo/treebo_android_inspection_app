package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by devesh on 01/05/16.
 */
public class GroupedScoresModel implements Parcelable {
    public String heading;
    public ScoresModel scores;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.heading);
        dest.writeParcelable(this.scores, flags);
    }

    public GroupedScoresModel() {
    }

    protected GroupedScoresModel(Parcel in) {
        this.heading = in.readString();
        this.scores = in.readParcelable(ScoresModel.class.getClassLoader());
    }

    public static final Creator<GroupedScoresModel> CREATOR = new Creator<GroupedScoresModel>() {
        @Override
        public GroupedScoresModel createFromParcel(Parcel source) {
            return new GroupedScoresModel(source);
        }

        @Override
        public GroupedScoresModel[] newArray(int size) {
            return new GroupedScoresModel[size];
        }
    };

    @Override
    public String toString() {
        return "GroupedScoresModel{" +
                "heading='" + heading + '\'' +
                ", scores=" + scores +
                '}';
    }
}
