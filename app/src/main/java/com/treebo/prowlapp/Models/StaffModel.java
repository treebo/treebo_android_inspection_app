package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by devesh on 28/04/16.
 */
public class StaffModel implements Parcelable, Cloneable {
    @SerializedName("id")
    public int id;

    @SerializedName("first_name")
    public String first_name;

    @SerializedName("last_name")
    public String last_name;

    @SerializedName("phone")
    public String phone;

    public transient boolean newStaffModel= false;

    @SerializedName("fathers_name")
    public String fathers_name;

    @SerializedName("salary")
    public int salary;

    @SerializedName("date_of_joining")
    public String dateOfJoining;


    @SerializedName("date_of_leaving")
    public String dateOfLeaving;

    @SerializedName("leave_start_date")
    public String leaveStartDate;

    @SerializedName("leave_end_date")
    public String leaveEndDate;

    @SerializedName("date_of_birth")
    public String dateOfBirth;

    @SerializedName("address_line_1")
    public String addressLine1;

    @SerializedName("address_line_2")
    public String addressLine2;

    @SerializedName("city")
    public String city;

    @SerializedName("district")
    public String district;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @SerializedName("state")
    public String state;

    @SerializedName("pin_code")
    public String pinCode;

    //String addressLine1,String addressLine2,String state, String city, String pinCode

    public ArrayList<String> getLanguagePrefRead() {
        return languagePrefRead;
    }

    public void setLanguagePrefRead(ArrayList<String> languagePrefRead) {
        this.languagePrefRead = languagePrefRead;
    }

    @SerializedName("language_pref_read")
    public ArrayList<String> languagePrefRead = new ArrayList<>();

    public ArrayList<String> getLanguagePrefSpeak() {
        return languagePrefSpeak;
    }

    public void setLanguagePrefSpeak(ArrayList<String> languagePrefSpeak) {
        this.languagePrefSpeak = languagePrefSpeak;
    }

    @SerializedName("language_pref_speak")
    public ArrayList<String> languagePrefSpeak = new ArrayList<>();

    public String getStaffImageUrl() {
        return staffImageUrl;
    }

    public void setStaffImageUrl(String staffImageUrl) {
        this.staffImageUrl = staffImageUrl;
    }

    @SerializedName("image_url")
    public String staffImageUrl;


    public int department;

    @SerializedName("department_name")
    public String departmentName;

    @SerializedName("bank_details")
    private BankDetails bankDetails;

    public int hotel;

    public int is_active;

    public ArrayList<String> leave_history=new ArrayList<>();

    public int getId() {
        return id;
    }

    public String getName() {
        return first_name;
    }

    public String getPhone() {
        return phone;
    }

    public int getSalary() {
        return salary;
    }

    public String getDateOfJoining() {
        return dateOfJoining;
    }

    public String getDateOfLeaving() {
        return dateOfLeaving;
    }

    public int getDepartment() {
        return department;
    }

    public int getHotel() {
        return hotel;
    }


    public String getFathers_name() {
        return fathers_name;
    }

    public String getAccountNumber() {
        return bankDetails != null ? bankDetails.getAccountNumber() : "";
    }

    public String getIfscCode() {
        return bankDetails != null ? bankDetails.getIfscCode() : "";
    }

    public String getBankName() {
        return bankDetails != null ? bankDetails.getBankName() : "";
    }

    public String getBranch() {
        return bankDetails != null ? bankDetails.getBranch() : "";
    }

    public String getBankCity() {
        return bankDetails != null ? bankDetails.getBankCity() : "";
    }




    public StaffModel() {
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


    @Override
    public String toString() {
        return "StaffModel{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", phone='" + phone + '\'' +
                ", newStaffModel=" + newStaffModel +
                ", fathers_name='" + fathers_name + '\'' +
                ", salary=" + salary +
                ", dateOfJoining='" + dateOfJoining + '\'' +
                ", dateOfLeaving='" + dateOfLeaving + '\'' +
                ", leaveStartDate='" + leaveStartDate + '\'' +
                ", leaveEndDate='" + leaveEndDate + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", state='" + state + '\'' +
                ", pinCode='" + pinCode + '\'' +
                ", languagePrefRead=" + languagePrefRead +
                ", languagePrefSpeak=" + languagePrefSpeak +
                ", staff_url='" + staffImageUrl + '\'' +
                ", department=" + department +
                ", departmentName='" + departmentName + '\'' +
                ", hotel=" + hotel +
                ", is_active=" + is_active +
                ", leave_history=" + leave_history +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.first_name);
        dest.writeString(this.last_name);
        dest.writeString(this.phone);
        dest.writeString(this.fathers_name);
        dest.writeInt(this.salary);
        dest.writeString(this.dateOfJoining);
        dest.writeString(this.dateOfLeaving);
        dest.writeString(this.leaveStartDate);
        dest.writeString(this.leaveEndDate);
        dest.writeString(this.dateOfBirth);
        dest.writeString(this.addressLine1);
        dest.writeString(this.addressLine2);
        dest.writeString(this.city);
        dest.writeString(this.district);
        dest.writeString(this.state);
        dest.writeString(this.pinCode);
        dest.writeStringList(this.languagePrefRead);
        dest.writeStringList(this.languagePrefSpeak);
        dest.writeString(this.staffImageUrl);
        dest.writeInt(this.department);
        dest.writeString(this.departmentName);
        dest.writeInt(this.hotel);
        dest.writeInt(this.is_active);
        dest.writeStringList(this.leave_history);
        dest.writeParcelable(this.bankDetails, flags);
    }

    protected StaffModel(Parcel in) {
        this.id = in.readInt();
        this.first_name = in.readString();
        this.last_name = in.readString();
        this.phone = in.readString();
        this.fathers_name = in.readString();
        this.salary = in.readInt();
        this.dateOfJoining = in.readString();
        this.dateOfLeaving = in.readString();
        this.leaveStartDate = in.readString();
        this.leaveEndDate = in.readString();
        this.dateOfBirth = in.readString();
        this.addressLine1 = in.readString();
        this.addressLine2 = in.readString();
        this.city = in.readString();
        this.district = in.readString();
        this.state = in.readString();
        this.pinCode = in.readString();
        this.languagePrefRead = in.createStringArrayList();
        this.languagePrefSpeak = in.createStringArrayList();
        this.staffImageUrl = in.readString();
        this.department = in.readInt();
        this.departmentName = in.readString();
        this.hotel = in.readInt();
        this.is_active = in.readInt();
        this.leave_history = in.createStringArrayList();
        this.bankDetails = in.readParcelable(BankDetails.class.getClassLoader());
    }

    public static final Creator<StaffModel> CREATOR = new Creator<StaffModel>() {
        @Override
        public StaffModel createFromParcel(Parcel source) {
            return new StaffModel(source);
        }

        @Override
        public StaffModel[] newArray(int size) {
            return new StaffModel[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StaffModel)) return false;

        StaffModel that = (StaffModel) o;

        if (getId() != that.getId()) return false;
        if (newStaffModel != that.newStaffModel) return false;
        if (getSalary() != that.getSalary()) return false;
        if (getDepartment() != that.getDepartment()) return false;
        if (getHotel() != that.getHotel()) return false;
        if (is_active != that.is_active) return false;
        if (first_name != null ? !first_name.equals(that.first_name) : that.first_name != null)
            return false;
        if (last_name != null ? !last_name.equals(that.last_name) : that.last_name != null)
            return false;
        if (getPhone() != null ? !getPhone().equals(that.getPhone()) : that.getPhone() != null)
            return false;
        if (getFathers_name() != null ? !getFathers_name().equals(that.getFathers_name()) : that.getFathers_name() != null)
            return false;
        if (getDateOfJoining() != null ? !getDateOfJoining().equals(that.getDateOfJoining()) : that.getDateOfJoining() != null)
            return false;
        if (getDateOfLeaving() != null ? !getDateOfLeaving().equals(that.getDateOfLeaving()) : that.getDateOfLeaving() != null)
            return false;
        if (leaveStartDate != null ? !leaveStartDate.equals(that.leaveStartDate) : that.leaveStartDate != null)
            return false;
        if (leaveEndDate != null ? !leaveEndDate.equals(that.leaveEndDate) : that.leaveEndDate != null)
            return false;
        if (dateOfBirth != null ? !dateOfBirth.equals(that.dateOfBirth) : that.dateOfBirth != null)
            return false;
        if (addressLine1 != null ? !addressLine1.equals(that.addressLine1) : that.addressLine1 != null)
            return false;
        if (addressLine2 != null ? !addressLine2.equals(that.addressLine2) : that.addressLine2 != null)
            return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (district != null ? !district.equals(that.district) : that.district != null)
            return false;
        if (getState() != null ? !getState().equals(that.getState()) : that.getState() != null)
            return false;
        if (pinCode != null ? !pinCode.equals(that.pinCode) : that.pinCode != null) return false;
        if (getLanguagePrefRead() != null ? !getLanguagePrefRead().equals(that.getLanguagePrefRead()) : that.getLanguagePrefRead() != null)
            return false;
        if (getLanguagePrefSpeak() != null ? !getLanguagePrefSpeak().equals(that.getLanguagePrefSpeak()) : that.getLanguagePrefSpeak() != null)
            return false;
        if (getStaffImageUrl() != null ? !getStaffImageUrl().equals(that.getStaffImageUrl()) : that.getStaffImageUrl() != null)
            return false;
        if (departmentName != null ? !departmentName.equals(that.departmentName) : that.departmentName != null)
            return false;
        return leave_history != null ? leave_history.equals(that.leave_history) : that.leave_history == null;

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (first_name != null ? first_name.hashCode() : 0);
        result = 31 * result + (last_name != null ? last_name.hashCode() : 0);
        result = 31 * result + (getPhone() != null ? getPhone().hashCode() : 0);
        result = 31 * result + (newStaffModel ? 1 : 0);
        result = 31 * result + (getFathers_name() != null ? getFathers_name().hashCode() : 0);
        result = 31 * result + getSalary();
        result = 31 * result + (getDateOfJoining() != null ? getDateOfJoining().hashCode() : 0);
        result = 31 * result + (getDateOfLeaving() != null ? getDateOfLeaving().hashCode() : 0);
        result = 31 * result + (leaveStartDate != null ? leaveStartDate.hashCode() : 0);
        result = 31 * result + (leaveEndDate != null ? leaveEndDate.hashCode() : 0);
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        result = 31 * result + (addressLine1 != null ? addressLine1.hashCode() : 0);
        result = 31 * result + (addressLine2 != null ? addressLine2.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (district != null ? district.hashCode() : 0);
        result = 31 * result + (getState() != null ? getState().hashCode() : 0);
        result = 31 * result + (pinCode != null ? pinCode.hashCode() : 0);
        result = 31 * result + (getLanguagePrefRead() != null ? getLanguagePrefRead().hashCode() : 0);
        result = 31 * result + (getLanguagePrefSpeak() != null ? getLanguagePrefSpeak().hashCode() : 0);
        result = 31 * result + (getStaffImageUrl() != null ? getStaffImageUrl().hashCode() : 0);
        result = 31 * result + getDepartment();
        result = 31 * result + (departmentName != null ? departmentName.hashCode() : 0);
        result = 31 * result + getHotel();
        result = 31 * result + is_active;
        result = 31 * result + (leave_history != null ? leave_history.hashCode() : 0);
        return result;
    }

    public static class BankDetails implements Parcelable {

        @SerializedName("account_number")
        private String accountNumber;

        @SerializedName("ifsc_code")
        private String ifscCode;

        @SerializedName("bank")
        private String bankName;

        @SerializedName("branch")
        private String branch;

        @SerializedName("city")
        private String bankCity;

        protected BankDetails(Parcel in) {
            accountNumber = in.readString();
            ifscCode = in.readString();
            bankName = in.readString();
            branch = in.readString();
            bankCity = in.readString();
        }

        public static final Creator<BankDetails> CREATOR = new Creator<BankDetails>() {
            @Override
            public BankDetails createFromParcel(Parcel in) {
                return new BankDetails(in);
            }

            @Override
            public BankDetails[] newArray(int size) {
                return new BankDetails[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(accountNumber);
            dest.writeString(ifscCode);
            dest.writeString(bankName);
            dest.writeString(branch);
            dest.writeString(bankCity);
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public String getIfscCode() {
            return ifscCode;
        }

        public String getBankName() {
            return bankName;
        }

        public String getBranch() {
            return branch;
        }

        public String getBankCity() {
            return bankCity;
        }
    }

}
