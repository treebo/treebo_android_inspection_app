package com.treebo.prowlapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sumandas on 04/05/2016.
 */
@Deprecated
public class RootCauseData {
    @SerializedName("root_causes")
    public ArrayList<RootCause> root_causes;
}
