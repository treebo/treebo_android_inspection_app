package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sumandas on 12/11/2016.
 */

public class CommonArea implements Parcelable {

    @SerializedName("audit_category_id")
    private int auditTypeId;

    @SerializedName("audit_type_description")
    private String auditTypeDescription;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("is_audited")
    private boolean isAudited;

    @SerializedName("last_checkout_timestamp")
    private String mLastCheckoutTimeStamp;

    @SerializedName("included_categories")
    private HashMap<Integer, ArrayList<Integer>> mCheckpointList;

    @SerializedName("issues")
    private ArrayList<IssueListModelV3> mIssuesList;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.auditTypeId);
        dest.writeString(this.auditTypeDescription);
        dest.writeString(this.mDescription);
        dest.writeByte((byte) (this.isAudited ? 1 : 0));
        dest.writeString(this.mLastCheckoutTimeStamp);
        dest.writeSerializable(this.mCheckpointList);
        dest.writeTypedList(this.mIssuesList);
    }

    public CommonArea() {
    }

    protected CommonArea(Parcel in) {
        this.auditTypeId = in.readInt();
        this.auditTypeDescription = in.readString();
        this.mDescription = in.readString();
        this.isAudited = in.readByte() != 0;
        this.mLastCheckoutTimeStamp = in.readString();
        this.mCheckpointList = (HashMap<Integer, ArrayList<Integer>>) in.readSerializable();
        this.mIssuesList = in.createTypedArrayList(IssueListModelV3.CREATOR);
    }

    public static final Creator<CommonArea> CREATOR = new Creator<CommonArea>() {
        @Override
        public CommonArea createFromParcel(Parcel source) {
            return new CommonArea(source);
        }

        @Override
        public CommonArea[] newArray(int size) {
            return new CommonArea[size];
        }
    };

    public ArrayList<IssueListModelV3> getmIssuesList() {
        return mIssuesList;
    }

    public void setmIssuesList(ArrayList<IssueListModelV3> mIssuesList) {
        this.mIssuesList = mIssuesList;
    }

    public HashMap<Integer, ArrayList<Integer>> getmCheckpointList() {
        return mCheckpointList;
    }

    public void setmCheckpointList(HashMap<Integer, ArrayList<Integer>> mCheckpointList) {
        this.mCheckpointList = mCheckpointList;
    }

    public String getmLastCheckoutTimeStamp() {
        return mLastCheckoutTimeStamp;
    }

    public void setmLastCheckoutTimeStamp(String mLastCheckoutTimeStamp) {
        this.mLastCheckoutTimeStamp = mLastCheckoutTimeStamp;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getAuditTypeId() {
        return auditTypeId;
    }

    public void setAuditTypeId(int auditTypeId) {
        this.auditTypeId = auditTypeId;
    }

    public String getAuditTypeDescription() {
        return auditTypeDescription;
    }

    public void setAuditTypeDescription(String auditTypeDescription) {
        this.auditTypeDescription = auditTypeDescription;
    }

    public boolean isAudited() {
        return isAudited;
    }

    public void setAudited(boolean audited) {
        isAudited = audited;
    }
}