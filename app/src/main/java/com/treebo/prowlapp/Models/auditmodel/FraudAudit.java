package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 17/01/17.
 */

public class FraudAudit implements Parcelable {

    @SerializedName("house_count_rooms")
    private ArrayList<String> houseCountRoomList;

    @SerializedName("threshold")
    private int threshold;

    @SerializedName("audit_category_id")
    private int auditCategoryID;

    @SerializedName("price_mismatch_rooms")
    private ArrayList<String> priceMismatchRoomList;

    @SerializedName("fraud_checkpoints")
    private ArrayList<FraudCheckpoint> fraudCheckpointArrayList;

    protected FraudAudit(Parcel in) {
        houseCountRoomList = in.createStringArrayList();
        threshold = in.readInt();
        auditCategoryID = in.readInt();
        priceMismatchRoomList = in.createStringArrayList();
        fraudCheckpointArrayList = in.createTypedArrayList(FraudCheckpoint.CREATOR);
    }

    public static final Creator<FraudAudit> CREATOR = new Creator<FraudAudit>() {
        @Override
        public FraudAudit createFromParcel(Parcel in) {
            return new FraudAudit(in);
        }

        @Override
        public FraudAudit[] newArray(int size) {
            return new FraudAudit[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(houseCountRoomList);
        parcel.writeInt(threshold);
        parcel.writeInt(auditCategoryID);
        parcel.writeStringList(priceMismatchRoomList);
        parcel.writeTypedList(fraudCheckpointArrayList);
    }


    public static class FraudCheckpoint implements Parcelable {

        @SerializedName("array")
        private ArrayList<FraudSubCheckpoint> subCheckpointArrayList;

        @SerializedName("id")
        private int id;

        @SerializedName("description")
        private String description;

        private boolean issueRaised;

        private boolean auditDone;

        public FraudCheckpoint() {

        }

        protected FraudCheckpoint(Parcel in) {
            subCheckpointArrayList = in.createTypedArrayList(FraudSubCheckpoint.CREATOR);
            id = in.readInt();
            description = in.readString();
            issueRaised = in.readByte() != 0;
        }

        public static final Creator<FraudCheckpoint> CREATOR = new Creator<FraudCheckpoint>() {
            @Override
            public FraudCheckpoint createFromParcel(Parcel in) {
                return new FraudCheckpoint(in);
            }

            @Override
            public FraudCheckpoint[] newArray(int size) {
                return new FraudCheckpoint[size];
            }
        };

        public String getDescription() {
            return description;
        }

        public void setDescription(String text) {
            this.description = text;
        }

        public int getId() {
            return id;
        }

        public void setId(int newID) {
            this.id = newID;
        }

        public boolean issueRaised() {
            return issueRaised;
        }

        public void setIssueRaised(boolean issueRaised) {
            this.issueRaised = issueRaised;
        }

        public ArrayList<FraudSubCheckpoint> getSubCheckpointArrayList() {
            return subCheckpointArrayList;
        }

        public boolean isAuditDone() {
            return auditDone;
        }

        public void setAuditDone(boolean auditDone) {
            this.auditDone = auditDone;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeTypedList(subCheckpointArrayList);
            parcel.writeInt(id);
            parcel.writeString(description);
            parcel.writeByte((byte) (issueRaised ? 1 : 0));
        }
    }

    public static class FraudSubCheckpoint implements Parcelable {

        @SerializedName("id")
        private int id;

        @SerializedName("description")
        private String description;

        private boolean isIssue;

        protected FraudSubCheckpoint(Parcel in) {
            id = in.readInt();
            description = in.readString();
            isIssue = in.readByte() != 0;
        }

        public static final Creator<FraudSubCheckpoint> CREATOR = new Creator<FraudSubCheckpoint>() {
            @Override
            public FraudSubCheckpoint createFromParcel(Parcel in) {
                return new FraudSubCheckpoint(in);
            }

            @Override
            public FraudSubCheckpoint[] newArray(int size) {
                return new FraudSubCheckpoint[size];
            }
        };


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(id);
            parcel.writeString(description);
            parcel.writeByte((byte) (isIssue ? 1 : 0));
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isIssue() {
            return isIssue;
        }

        public void setIssue(boolean issue) {
            isIssue = issue;
        }
    }

    public ArrayList<String> getHouseCountRoomList() {
        return houseCountRoomList;
    }

    public int getThreshold() {
        return threshold;
    }

    public ArrayList<String> getPriceMismatchRoomList() {
        return priceMismatchRoomList;
    }

    public ArrayList<FraudCheckpoint> getFraudCheckpointArrayList() {
        return fraudCheckpointArrayList;
    }

    public int getAuditCategoryID() {
        return auditCategoryID;
    }
}
