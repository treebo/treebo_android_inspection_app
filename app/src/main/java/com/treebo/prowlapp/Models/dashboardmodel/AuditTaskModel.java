package com.treebo.prowlapp.Models.dashboardmodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sumandas on 30/11/2016.
 */

public class AuditTaskModel {

    private static final int MAX_OF_ROOMS_TO_BE_AUDITED = 3;

    @SerializedName("recommended_rooms")
    private ArrayList<String> mRecommended;

    public String getRecommendedList() {
        if (mRecommended != null) {
            String list = "";
            if (mRecommended.size() > MAX_OF_ROOMS_TO_BE_AUDITED) {
                for (int i = 0; i < MAX_OF_ROOMS_TO_BE_AUDITED; i++) {
                    list += " " + mRecommended.get(i) + ",";
                }
            } else {
                list = mRecommended.toString();
            }
            return list.substring(1, list.length() - 1);
        } else
            return "";
    }

}
