package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.treebo.prowlapp.dbflow.ProwlDatabase;
import com.treebo.prowlapp.geofence.GeofenceUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.LocationUtils;
import com.treebo.prowlapp.Utils.Logger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by sumandas on 08/11/2016.
 */
@Table(database = ProwlDatabase.class)
public class HotelLocation extends BaseModel implements Parcelable {

    @Column
    @PrimaryKey
    @SerializedName("id")
    public int mHotelId;

    @Column
    @SerializedName("name")
    public String mHotelName;

    @Column
    @SerializedName("latitude")
    public double mLatitude;

    @Column
    @SerializedName("longitude")
    public double mLongitude;

    @Column
    @SerializedName("radius")
    public int mRadius;

    @Column
    public boolean isGeofenceAdded;

    @Column
    public long mGeoFenceAddedTime;

    public HotelLocation() {
    }

    /**
     * will return null if lat long not within hotel radius
     */
    public static ArrayList<HotelLocation> getHotelForLocation(double latitude, double longitude) {
        ArrayList<HotelLocation> inLocation = new ArrayList<>();
        List<HotelLocation> hotelLocations = SQLite.select().from(HotelLocation.class).queryList();
        for (HotelLocation hotelLocation : hotelLocations) {
            double distance = LocationUtils.getDistanceFromLatLonInMeters(hotelLocation.mLatitude, hotelLocation.mLongitude,
                    latitude, longitude);
            Logger.d("Distance: " + hotelLocation.mHotelName + "- " + String.valueOf(distance));
            if (distance < hotelLocation.mRadius) {
                Logger.d("Distance: " + hotelLocation.mHotelName + "- " + String.valueOf(distance));
                if (inLocation.size() > Constants.MAX_NUMBER_OF_CURRENT_HOTELS)
                    break;
                else
                    inLocation.add(hotelLocation);
            }
        }
        return inLocation;
    }

    /* Added to Support Audit Any Hotel Feature */
    public static List<HotelLocation> getAllHotels() {
        return SQLite.select().from(HotelLocation.class).queryList();
    }

    public static HotelLocation getHotelLocationById(int id) {
        return SQLite.select().from(HotelLocation.class)
                .where(HotelLocation_Table.mHotelId.eq(id)).querySingle();
    }

    public static void deleteHotelLocationById(int id) {
        SQLite.delete().from(HotelLocation.class).where(HotelLocation_Table.mHotelId.eq(id)).async().execute();
    }


    public static void removeStaleGeoFences() {
        List<HotelLocation> hotelLocations = SQLite.select().from(HotelLocation.class).queryList();
        Calendar calendar = Calendar.getInstance();
        for (HotelLocation hotelLocation : hotelLocations) {
            if (hotelLocation.isGeofenceAdded) {
                long timeDiff = hotelLocation.mGeoFenceAddedTime - calendar.getTimeInMillis();
                if (timeDiff / (3600000) > GeofenceUtils.GEOFENCE_EXPIRATION_IN_HOURS) {
                    hotelLocation.isGeofenceAdded = false;
                    hotelLocation.save();
                }
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mHotelId);
        dest.writeString(this.mHotelName);
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongitude);
        dest.writeInt(this.mRadius);
        dest.writeByte(this.isGeofenceAdded ? (byte) 1 : (byte) 0);
        dest.writeLong(this.mGeoFenceAddedTime);
    }

    protected HotelLocation(Parcel in) {
        this.mHotelId = in.readInt();
        this.mHotelName = in.readString();
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();
        this.mRadius = in.readInt();
        this.isGeofenceAdded = in.readByte() != 0;
        this.mGeoFenceAddedTime = in.readLong();
    }

    public static final Creator<HotelLocation> CREATOR = new Creator<HotelLocation>() {
        @Override
        public HotelLocation createFromParcel(Parcel source) {
            return new HotelLocation(source);
        }

        @Override
        public HotelLocation[] newArray(int size) {
            return new HotelLocation[size];
        }
    };
}
