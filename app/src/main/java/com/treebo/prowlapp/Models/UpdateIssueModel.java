package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by sumandas on 04/05/2016.
 */
public class UpdateIssueModel implements Parcelable {
    public int mIssueId;
    public String mRootCause;
    public String mComment;
    public ArrayList<String> mImageUrls;

    public UpdateIssueModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mIssueId);
        dest.writeString(this.mRootCause);
        dest.writeString(this.mComment);
        dest.writeStringList(this.mImageUrls);
    }

    protected UpdateIssueModel(Parcel in) {
        this.mIssueId = in.readInt();
        this.mRootCause = in.readString();
        this.mComment = in.readString();
        this.mImageUrls = in.createStringArrayList();
    }

    public static final Creator<UpdateIssueModel> CREATOR = new Creator<UpdateIssueModel>() {
        @Override
        public UpdateIssueModel createFromParcel(Parcel source) {
            return new UpdateIssueModel(source);
        }

        @Override
        public UpdateIssueModel[] newArray(int size) {
            return new UpdateIssueModel[size];
        }
    };
}
