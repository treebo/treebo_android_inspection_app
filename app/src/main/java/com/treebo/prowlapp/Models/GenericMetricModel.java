package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class GenericMetricModel implements Parcelable {

    @SerializedName("score")
    private int score;

    @SerializedName("color")
    private String color;

    @SerializedName("difference")
    private int difference;

    public GenericMetricModel() {

    }

    protected GenericMetricModel(Parcel in) {
        score = in.readInt();
        color = in.readString();
        difference = in.readInt();
    }

    public static final Creator<GenericMetricModel> CREATOR = new Creator<GenericMetricModel>() {
        @Override
        public GenericMetricModel createFromParcel(Parcel in) {
            return new GenericMetricModel(in);
        }

        @Override
        public GenericMetricModel[] newArray(int size) {
            return new GenericMetricModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(score);
        parcel.writeString(color);
        parcel.writeInt(difference);
    }

    public int getScore() {
        return score;
    }

    public String getColor() {
        return color;
    }

    public int getDifference() {
        return difference;
    }
}
