package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
@Deprecated
public class Query implements Parcelable{

    public Query(){

    }

    @Expose
    private String comment;
    @SerializedName("query_type")
    @Expose
    private String queryType;
    @Expose
    private Integer severe;
    @SerializedName("query_name")
    @Expose
    private String queryName;
    @Expose
    private String answer;
    @SerializedName("query_id")
    @Expose
    private Integer queryId;
    @SerializedName("query_sub_name")
    @Expose
    private String subQuery;

    private String image_url;


    public String getSubQuery() {
        return subQuery;
    }

    public void setSubQuery(String subQuery) {
        this.subQuery = subQuery;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     * The queryType
     */
    public String getQueryType() {
        return queryType;
    }

    /**
     *
     * @param queryType
     * The query_type
     */
    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    /**
     *
     * @return
     * The severe
     */
    public Integer getSevere() {
        return severe;
    }

    /**
     *
     * @param severe
     * The severe
     */
    public void setSevere(Integer severe) {
        this.severe = severe;
    }

    /**
     *
     * @return
     * The queryName
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     *
     * @param queryName
     * The query_name
     */
    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    /**
     *
     * @return
     * The answer
     */
    public String getAnswer() {
        return answer;
    }

    /**
     *
     * @param answer
     * The answer
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    /**
     *
     * @return
     * The queryId
     */
    public Integer getQueryId() {
        return queryId;
    }

    /**
     *
     * @param queryId
     * The query_id
     */
    public void setQueryId(Integer queryId) {
        this.queryId = queryId;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.comment);
        dest.writeString(this.queryType);
        dest.writeValue(this.severe);
        dest.writeString(this.queryName);
        dest.writeString(this.answer);
        dest.writeValue(this.queryId);
        dest.writeString(this.subQuery);
        dest.writeString(this.image_url);
    }

    protected Query(Parcel in) {
        this.comment = in.readString();
        this.queryType = in.readString();
        this.severe = (Integer) in.readValue(Integer.class.getClassLoader());
        this.queryName = in.readString();
        this.answer = in.readString();
        this.queryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subQuery = in.readString();
        this.image_url = in.readString();
    }

    public static final Creator<Query> CREATOR = new Creator<Query>() {
        @Override
        public Query createFromParcel(Parcel source) {
            return new Query(source);
        }

        @Override
        public Query[] newArray(int size) {
            return new Query[size];
        }
    };
}