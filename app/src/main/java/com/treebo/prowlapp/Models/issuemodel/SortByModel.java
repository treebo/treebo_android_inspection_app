package com.treebo.prowlapp.Models.issuemodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abhisheknair on 16/11/16.
 */

public class SortByModel implements Parcelable {

    public int sortCriteriaId;
    public String sortTitle;
    public boolean isSelected;

    public SortByModel() {

    }

    protected SortByModel(Parcel in) {
        sortCriteriaId = in.readInt();
        sortTitle = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<SortByModel> CREATOR = new Creator<SortByModel>() {
        @Override
        public SortByModel createFromParcel(Parcel in) {
            return new SortByModel(in);
        }

        @Override
        public SortByModel[] newArray(int size) {
            return new SortByModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(sortCriteriaId);
        parcel.writeString(sortTitle);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
    }
}
