package com.treebo.prowlapp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sumandas on 04/05/2016.
 */
@Deprecated
public class RootCause {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;
}
