package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abhisheknair on 09/11/16.
 */

public class RoomV3Object implements Parcelable {

    @SerializedName("room_id")
    public int id;

    @SerializedName("room_number")
    public String roomNumber;

    @SerializedName("room_type")
    public String roomType;

    @SerializedName("is_available")
    public boolean isAvailable;

    @SerializedName("is_audited")
    public boolean isAudited;

    @SerializedName("is_ready")
    public boolean isReady;

    @SerializedName("last_checkout_timestamp")
    public String lastCheckOutTimeStamp;

    @SerializedName("description")
    public String description;

    @SerializedName("is_recomended")
    public boolean isRecommended;


    @SerializedName("audit_category_id")
    public int auditTypeId;

    @SerializedName("audit_type_description")
    public String auditTypeDescription;

    public String cantAuditReason;

    public boolean isPendingAudit;

    public RoomV3Object() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public String getLastCheckOutTimeStamp() {
        return lastCheckOutTimeStamp;
    }

    public void setLastCheckOutTimeStamp(String lastCheckOutTimeStamp) {
        this.lastCheckOutTimeStamp = lastCheckOutTimeStamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRecommended() {
        return isRecommended;
    }

    public void setRecommended(boolean recommended) {
        isRecommended = recommended;
    }


    public boolean isAudited() {
        return isAudited;
    }

    public void setAudited(boolean audited) {
        isAudited = audited;
    }


    public int getAuditTypeId() {
        return auditTypeId;
    }

    public void setAuditTypeId(int auditTypeId) {
        this.auditTypeId = auditTypeId;
    }

    public String getAuditTypeDescription() {
        return auditTypeDescription;
    }

    public void setAuditTypeDescription(String auditTypeDescription) {
        this.auditTypeDescription = auditTypeDescription;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.roomNumber);
        dest.writeString(this.roomType);
        dest.writeByte(this.isAvailable ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isAudited ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isReady ? (byte) 1 : (byte) 0);
        dest.writeString(this.lastCheckOutTimeStamp);
        dest.writeString(this.description);
        dest.writeByte(this.isRecommended ? (byte) 1 : (byte) 0);
        dest.writeInt(this.auditTypeId);
        dest.writeString(this.auditTypeDescription);
        dest.writeByte(this.isPendingAudit ? (byte) 1 : (byte) 0);
    }

    protected RoomV3Object(Parcel in) {
        this.id = in.readInt();
        this.roomNumber = in.readString();
        this.roomType = in.readString();
        this.isAvailable = in.readByte() != 0;
        this.isAudited = in.readByte() != 0;
        this.isReady = in.readByte() != 0;
        this.lastCheckOutTimeStamp = in.readString();
        this.description = in.readString();
        this.isRecommended = in.readByte() != 0;
        this.auditTypeId = in.readInt();
        this.auditTypeDescription = in.readString();
        this.isPendingAudit = in.readByte() != 0;
    }

    public static final Creator<RoomV3Object> CREATOR = new Creator<RoomV3Object>() {
        @Override
        public RoomV3Object createFromParcel(Parcel source) {
            return new RoomV3Object(source);
        }

        @Override
        public RoomV3Object[] newArray(int size) {
            return new RoomV3Object[size];
        }
    };
}
