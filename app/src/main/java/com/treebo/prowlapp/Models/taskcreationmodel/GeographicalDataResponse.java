package com.treebo.prowlapp.Models.taskcreationmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 23/06/17.
 */

public class GeographicalDataResponse extends BaseResponse implements Parcelable {

    @SerializedName("data")
    private RegionList data;

    protected GeographicalDataResponse(Parcel in) {
        data = in.readParcelable(RegionList.class.getClassLoader());
    }

    public GeographicalDataResponse() {

    }

    public static final Creator<GeographicalDataResponse> CREATOR = new Creator<GeographicalDataResponse>() {
        @Override
        public GeographicalDataResponse createFromParcel(Parcel in) {
            return new GeographicalDataResponse(in);
        }

        @Override
        public GeographicalDataResponse[] newArray(int size) {
            return new GeographicalDataResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(data, flags);
    }

    public RegionList getData() {
        return data;
    }

    public static class RegionList implements Parcelable {

        @SerializedName("regions")
        private ArrayList<Region> regionList;

        protected RegionList(Parcel in) {
            regionList = in.createTypedArrayList(Region.CREATOR);
        }

        public static final Creator<RegionList> CREATOR = new Creator<RegionList>() {
            @Override
            public RegionList createFromParcel(Parcel in) {
                return new RegionList(in);
            }

            @Override
            public RegionList[] newArray(int size) {
                return new RegionList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(regionList);
        }

        public ArrayList<Region> getRegionList() {
            return regionList;
        }
    }

    public static class Region implements Parcelable {

        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String regionName;

        @SerializedName("permitted_users")
        private int[] permittedUserList;

        @SerializedName("clusters")
        private ArrayList<Cluster> clusterList;

        protected Region(Parcel in) {
            id = in.readInt();
            regionName = in.readString();
            permittedUserList = in.createIntArray();
            clusterList = in.createTypedArrayList(Cluster.CREATOR);
        }

        public static final Creator<Region> CREATOR = new Creator<Region>() {
            @Override
            public Region createFromParcel(Parcel in) {
                return new Region(in);
            }

            @Override
            public Region[] newArray(int size) {
                return new Region[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(regionName);
            dest.writeTypedList(clusterList);
            dest.writeIntArray(permittedUserList);
        }

        public int getId() {
            return id;
        }

        public String getRegionName() {
            return regionName;
        }

        public ArrayList<Integer> getPermittedUserList() {
            ArrayList<Integer> list = new ArrayList<>();
            for (int aPermittedUserList : permittedUserList)
                list.add(aPermittedUserList);
            return list;
        }

        public ArrayList<Cluster> getClusterList() {
            return clusterList;
        }
    }

    public static class Cluster implements Parcelable {

        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String clusterName;

        @SerializedName("permitted_users")
        private int[] permittedUserList;

        @SerializedName("cities")
        private ArrayList<City> cityList;

        protected Cluster(Parcel in) {
            id = in.readInt();
            clusterName = in.readString();
            permittedUserList = in.createIntArray();
            cityList = in.createTypedArrayList(City.CREATOR);
        }

        public static final Creator<Cluster> CREATOR = new Creator<Cluster>() {
            @Override
            public Cluster createFromParcel(Parcel in) {
                return new Cluster(in);
            }

            @Override
            public Cluster[] newArray(int size) {
                return new Cluster[size];
            }
        };

        public int getId() {
            return id;
        }

        public String getClusterName() {
            return clusterName;
        }

        public ArrayList<Integer> getPermittedUserList() {
            ArrayList<Integer> list = new ArrayList<>();
            for (int aPermittedUserList : permittedUserList)
                list.add(aPermittedUserList);
            return list;
        }

        public ArrayList<City> getCityList() {
            return cityList;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(clusterName);
            dest.writeTypedList(cityList);
            dest.writeIntArray(permittedUserList);
        }
    }

    public static class City implements Parcelable {

        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String cityName;

        @SerializedName("permitted_users")
        private int[] permittedUserList;

        @SerializedName("hotels")
        private ArrayList<Hotel> hotelList;

        protected City(Parcel in) {
            id = in.readInt();
            cityName = in.readString();
            permittedUserList = in.createIntArray();
            hotelList = in.createTypedArrayList(Hotel.CREATOR);
        }

        public static final Creator<City> CREATOR = new Creator<City>() {
            @Override
            public City createFromParcel(Parcel in) {
                return new City(in);
            }

            @Override
            public City[] newArray(int size) {
                return new City[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(cityName);
            dest.writeIntArray(permittedUserList);
            dest.writeTypedList(hotelList);
        }

        public int getId() {
            return id;
        }

        public String getCityName() {
            return cityName;
        }

        public ArrayList<Integer> getPermittedUserList() {
            ArrayList<Integer> list = new ArrayList<>();
            for (int aPermittedUserList : permittedUserList)
                list.add(aPermittedUserList);
            return list;
        }

        public ArrayList<Hotel> getHotelList() {
            return hotelList;
        }
    }

    public static class Hotel implements Parcelable {

        @SerializedName("id")
        private int id;

        @SerializedName("name")
        private String name;

        @SerializedName("city_name")
        private String city;

        protected Hotel(Parcel in) {
            id = in.readInt();
            name = in.readString();
            city = in.readString();
        }

        public static final Creator<Hotel> CREATOR = new Creator<Hotel>() {
            @Override
            public Hotel createFromParcel(Parcel in) {
                return new Hotel(in);
            }

            @Override
            public Hotel[] newArray(int size) {
                return new Hotel[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(city);
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getCity() {
            return city;
        }
    }

    public boolean isRegionEnabledForUser(int userID) {
        for (Region region : this.getData().getRegionList()) {
            for (Integer id : region.getPermittedUserList()) {
                if (id == userID)
                    return true;
            }

        }
        return false;
    }

    public boolean isClusterEnabledForUser(int userID) {
        for (Region region : this.getData().getRegionList()) {
            for (Cluster cluster : region.getClusterList()) {
                for (Integer id : cluster.getPermittedUserList()) {
                    if (id == userID)
                        return true;
                }
            }
        }
        return false;
    }

    public boolean isCityEnabledForUser(int userID) {
        for (Region region : this.getData().getRegionList()) {
            for (Cluster cluster : region.getClusterList()) {
                for (City city : cluster.getCityList()) {
                    for (Integer id : city.getPermittedUserList()) {
                        if (id == userID)
                            return true;
                    }
                }
            }
        }
        return false;
    }

    public ArrayList<String> getHotelList() {
        ArrayList<String> hotelList = new ArrayList<>();
        for (Region region : this.getData().getRegionList()) {
            for (Cluster cluster : region.getClusterList()) {
                for (City city : cluster.getCityList()) {
                    for (Hotel hotel : city.getHotelList()) {
                        hotelList.add(hotel.getName());
                    }
                }
            }
        }
        return hotelList;
    }

    public ArrayList<String> getCityList() {
        ArrayList<String> cityList = new ArrayList<>();
        for (Region region : this.getData().getRegionList()) {
            for (Cluster cluster : region.getClusterList()) {
                for (City city : cluster.getCityList()) {
                    cityList.add(city.getCityName());
                }
            }
        }
        return cityList;
    }

    public ArrayList<String> getClusterList() {
        ArrayList<String> clusterList = new ArrayList<>();
        for (Region region : this.getData().getRegionList()) {
            for (Cluster cluster : region.getClusterList()) {
                clusterList.add(cluster.getClusterName());
            }
        }
        return clusterList;
    }

    public ArrayList<String> getRegionList() {
        ArrayList<String> regionList = new ArrayList<>();
        for (Region region : this.getData().getRegionList()) {
            regionList.add(region.getRegionName());
        }
        return regionList;
    }
}
