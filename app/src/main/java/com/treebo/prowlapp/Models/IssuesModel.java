package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sumandas on 30/04/2016
 * .
 */
@Deprecated
public class IssuesModel implements Parcelable {

    @SerializedName("issueId")
    public int id;

    public String source="";

    @SerializedName("is_repeat")
    public boolean isRepeat;

    @SerializedName("severity")
    public String severity="";

    @SerializedName("root_cause")
    public String rootCause="";

    @SerializedName("target_resolution_date")
    public String resolutionDate="";

    @SerializedName("resolved_date")
    public String resolvedDate="";

    @SerializedName("comment")
    public String comment="";

    @SerializedName("status")
    public String status="";

    @SerializedName("lastUpdate")
    public ArrayList<LastUpdateModel> lastUpdate=new ArrayList<>();

    @SerializedName("opensince")
    public String opensince="";

    @SerializedName("imageUrl")
    public ArrayList<String> imageUrl=new ArrayList<>();

    public int submission;

    public String category="";
    public String room_number="";

    public String checkpoint="";

    public String scoring_guideline="";

    public IssuesModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.source);
        dest.writeByte(isRepeat ? (byte) 1 : (byte) 0);
        dest.writeString(this.severity);
        dest.writeString(this.rootCause);
        dest.writeString(this.resolutionDate);
        dest.writeString(this.resolvedDate);
        dest.writeString(this.comment);
        dest.writeString(this.status);
        dest.writeTypedList(lastUpdate);
        dest.writeString(this.opensince);
        dest.writeStringList(this.imageUrl);
        dest.writeInt(this.submission);
        dest.writeString(this.category);
        dest.writeString(this.room_number);
        dest.writeString(this.checkpoint);
        dest.writeString(this.scoring_guideline);
    }

    protected IssuesModel(Parcel in) {
        this.id = in.readInt();
        this.source = in.readString();
        this.isRepeat = in.readByte() != 0;
        this.severity = in.readString();
        this.rootCause = in.readString();
        this.resolutionDate = in.readString();
        this.resolvedDate = in.readString();
        this.comment = in.readString();
        this.status = in.readString();
        this.lastUpdate = in.createTypedArrayList(LastUpdateModel.CREATOR);
        this.opensince = in.readString();
        this.imageUrl = in.createStringArrayList();
        this.submission = in.readInt();
        this.category = in.readString();
        this.room_number = in.readString();
        this.checkpoint = in.readString();
        this.scoring_guideline = in.readString();
    }

    public static final Creator<IssuesModel> CREATOR = new Creator<IssuesModel>() {
        @Override
        public IssuesModel createFromParcel(Parcel source) {
            return new IssuesModel(source);
        }

        @Override
        public IssuesModel[] newArray(int size) {
            return new IssuesModel[size];
        }
    };
}
