package com.treebo.prowlapp.Models.dashboardmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.treebo.prowlapp.Utils.Constants;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 28/11/16.
 */

public class SectionedTaskListModel implements Parcelable {

    private String sectionName;

    private ArrayList<TaskModel> taskList;

    public SectionedTaskListModel() {

    }

    protected SectionedTaskListModel(Parcel in) {
        sectionName = in.readString();
        taskList = in.createTypedArrayList(TaskModel.CREATOR);
    }

    public static final Creator<SectionedTaskListModel> CREATOR = new Creator<SectionedTaskListModel>() {
        @Override
        public SectionedTaskListModel createFromParcel(Parcel in) {
            return new SectionedTaskListModel(in);
        }

        @Override
        public SectionedTaskListModel[] newArray(int size) {
            return new SectionedTaskListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(sectionName);
        parcel.writeTypedList(taskList);
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public ArrayList<TaskModel> getTaskList() {
        return taskList;
    }

    public int getTaskListSize() {
        int count = 0;
        for (TaskModel model: this.taskList) {
            if (model.getTaskCardType() != Constants.BOTTOM_DUMMY_TASK)
                count++;
        }
        return count;
    }

    public void setTaskList(ArrayList<TaskModel> taskList) {
        this.taskList = taskList;
    }

    public void clearTaskList() {
        this.taskList.clear();
    }
}
