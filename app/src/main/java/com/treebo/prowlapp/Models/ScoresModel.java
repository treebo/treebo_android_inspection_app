package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.amazonaws.com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by devesh on 01/05/16.
 */
public class ScoresModel implements Parcelable {

    public double overall;
    @SerializedName("tql_month")
    public String tqlMonth;

    public List<SubHeadingScoresModel> subHeading;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.overall);
        dest.writeString(this.tqlMonth);
        dest.writeTypedList(this.subHeading);
    }

    public ScoresModel() {
    }

    protected ScoresModel(Parcel in) {
        this.overall = in.readDouble();
        this.tqlMonth = in.readString();
        this.subHeading = in.createTypedArrayList(SubHeadingScoresModel.CREATOR);
    }

    public static final Creator<ScoresModel> CREATOR = new Creator<ScoresModel>() {
        @Override
        public ScoresModel createFromParcel(Parcel source) {
            return new ScoresModel(source);
        }

        @Override
        public ScoresModel[] newArray(int size) {
            return new ScoresModel[size];
        }
    };

    @Override
    public String toString() {
        return "ScoresModel{" +
                "overall=" + overall +
                ", tqlMonth='" + tqlMonth + '\'' +
                ", subHeading=" + subHeading +
                '}';
    }
}
