package com.treebo.prowlapp.Models.auditmodel;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.MainApplication;

/**
 * Created by abhisheknair on 19/01/17.
 */

public class FraudRoomObject implements Parcelable {

    private String roomName;

    private String fraudReason;

    private int textColorResource;

    public FraudRoomObject() {

    }

    protected FraudRoomObject(Parcel in) {
        roomName = in.readString();
        fraudReason = in.readString();
        textColorResource = in.readInt();
    }

    public static final Creator<FraudRoomObject> CREATOR = new Creator<FraudRoomObject>() {
        @Override
        public FraudRoomObject createFromParcel(Parcel in) {
            return new FraudRoomObject(in);
        }

        @Override
        public FraudRoomObject[] newArray(int size) {
            return new FraudRoomObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(roomName);
        dest.writeString(fraudReason);
        dest.writeInt(textColorResource);
    }

    public String getFraudReason() {
        return fraudReason;
    }

    public void setFraudReason(String fraudReason) {
        Context context = MainApplication.getAppContext();
        this.fraudReason = fraudReason;
        if (fraudReason.equals(context.getResources().getStringArray(R.array.house_count_reason_list)[0])
                || fraudReason.equals(context.getResources().getStringArray(R.array.price_mismatch_list)[0]))
            textColorResource = R.color.warning_red;
        else
            textColorResource = R.color.green_color_primary;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public int getTextColor() {
        return this.textColorResource;
    }
}
