package com.treebo.prowlapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
@Deprecated
public class FormUpdateOutputModel {

    @SerializedName("form_type")
    private String formType;
    @SerializedName("timestamp")
    private Integer timestamp;
    @SerializedName("id")
    private Integer id;
    @SerializedName("form")
    private List<Form> form = new ArrayList<Form>();

    /**
     *
     * @return
     * The formType
     */
    public String getFormType() {
        return formType;
    }

    /**
     *
     * @param formType
     * The form_type
     */
    public void setFormType(String formType) {
        this.formType = formType;
    }

    /**
     *
     * @return
     * The timestamp
     */
    public Integer getTimestamp() {
        return timestamp;
    }

    /**
     *
     * @param timestamp
     * The timestamp
     */
    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The form
     */
    public List<Form> getForm() {
        return form;
    }

    /**
     *
     * @param form
     * The form
     */
    public void setForm(List<Form> form) {
        this.form = form;
    }

}