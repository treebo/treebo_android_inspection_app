package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;

import java.util.ArrayList;

/**
 * Created by sumandas on 11/11/2016.
 */

public class AuditSaveObject implements Parcelable {

    public ArrayList<AnswerV3> mAnswerList = new ArrayList<>();

    private ArrayList<IssueListModelV3> mPreMarkedIssues;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.mAnswerList);
        dest.writeTypedList(this.mPreMarkedIssues);
    }

    public AuditSaveObject() {
    }

    protected AuditSaveObject(Parcel in) {
        mAnswerList = in.createTypedArrayList(AnswerV3.CREATOR);
        mPreMarkedIssues = in.createTypedArrayList(IssueListModelV3.CREATOR);
    }

    public static final Creator<AuditSaveObject> CREATOR = new Creator<AuditSaveObject>() {
        @Override
        public AuditSaveObject createFromParcel(Parcel source) {
            return new AuditSaveObject(source);
        }

        @Override
        public AuditSaveObject[] newArray(int size) {
            return new AuditSaveObject[size];
        }
    };

    public ArrayList<IssueListModelV3> getPreMarkedIssues() {
        return mPreMarkedIssues;
    }

    public void setPreMarkedIssues(ArrayList<IssueListModelV3> mPreMarkedIssues) {
        this.mPreMarkedIssues = mPreMarkedIssues;
    }

    public ArrayList<AnswerV3> getCreatedIssueList() {
        return mAnswerList == null ? new ArrayList<>() : mAnswerList;
    }
}
