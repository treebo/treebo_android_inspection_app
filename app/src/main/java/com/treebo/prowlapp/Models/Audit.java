package com.treebo.prowlapp.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sumandas on 29/04/2016.
 */
@Deprecated
public class Audit {

    @SerializedName("blind_rooms_count")
    public String blind_rooms_count;

    @SerializedName("completion_ratio")
    public String completion_ratio;

    @SerializedName("types")
    public ArrayList<RoomTypeList> types;


}
