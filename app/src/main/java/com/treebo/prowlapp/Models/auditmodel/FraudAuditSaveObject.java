package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 20/01/17.
 */

public class FraudAuditSaveObject implements Parcelable {

    private ArrayList<FraudAuditSubmitObject> mFraudIssuesRaisedList;

    private ArrayList<FraudRoomObject> mHouseCountList;

    private ArrayList<FraudRoomObject> mPriceMismatchList;

    private int threshold;

    private int auditCategoryID;

    private ArrayList<FraudAudit.FraudCheckpoint> mCategoryList;


    public FraudAuditSaveObject() {
    }

    protected FraudAuditSaveObject(Parcel in) {
        mFraudIssuesRaisedList = in.createTypedArrayList(FraudAuditSubmitObject.CREATOR);
        mHouseCountList = in.createTypedArrayList(FraudRoomObject.CREATOR);
        mPriceMismatchList = in.createTypedArrayList(FraudRoomObject.CREATOR);
        threshold = in.readInt();
        auditCategoryID = in.readInt();
        mCategoryList = in.createTypedArrayList(FraudAudit.FraudCheckpoint.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mFraudIssuesRaisedList);
        dest.writeTypedList(mHouseCountList);
        dest.writeTypedList(mPriceMismatchList);
        dest.writeInt(threshold);
        dest.writeInt(auditCategoryID);
        dest.writeTypedList(mCategoryList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FraudAuditSaveObject> CREATOR = new Creator<FraudAuditSaveObject>() {
        @Override
        public FraudAuditSaveObject createFromParcel(Parcel in) {
            return new FraudAuditSaveObject(in);
        }

        @Override
        public FraudAuditSaveObject[] newArray(int size) {
            return new FraudAuditSaveObject[size];
        }
    };

    public ArrayList<FraudAuditSubmitObject> getList() {
        return mFraudIssuesRaisedList == null ? new ArrayList<>() : mFraudIssuesRaisedList;
    }

    public void setList(ArrayList<FraudAuditSubmitObject> mList) {
        this.mFraudIssuesRaisedList = mList;
    }

    public ArrayList<FraudRoomObject> getHouseCountList() {
        return mHouseCountList;
    }

    public void setHouseCountList(ArrayList<FraudRoomObject> mHouseCountList) {
        this.mHouseCountList = mHouseCountList;
    }

    public ArrayList<FraudRoomObject> getPriceMismatchList() {
        return mPriceMismatchList;
    }

    public void setPriceMismatchList(ArrayList<FraudRoomObject> mPriceMismatchList) {
        this.mPriceMismatchList = mPriceMismatchList;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public int getAuditCategoryID() {
        return auditCategoryID;
    }

    public void setAuditCategoryID(int auditCategoryID) {
        this.auditCategoryID = auditCategoryID;
    }

    public ArrayList<FraudAudit.FraudCheckpoint> getCategoryList() {
        return mCategoryList;
    }

    public void setCategoryList(ArrayList<FraudAudit.FraudCheckpoint> mCategoryList) {
        this.mCategoryList = mCategoryList;
    }
}
