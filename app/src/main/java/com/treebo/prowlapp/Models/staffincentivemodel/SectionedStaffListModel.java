package com.treebo.prowlapp.Models.staffincentivemodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.treebo.prowlapp.response.staffincentives.StaffObject;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 02/03/17.
 */

public class SectionedStaffListModel implements Parcelable {

    private String sectionName;

    private ArrayList<StaffObject> staffList = new ArrayList<>();

    public SectionedStaffListModel() {

    }

    protected SectionedStaffListModel(Parcel in) {
        sectionName = in.readString();
        staffList = in.createTypedArrayList(StaffObject.CREATOR);
    }

    public static final Creator<SectionedStaffListModel> CREATOR = new Creator<SectionedStaffListModel>() {
        @Override
        public SectionedStaffListModel createFromParcel(Parcel in) {
            return new SectionedStaffListModel(in);
        }

        @Override
        public SectionedStaffListModel[] newArray(int size) {
            return new SectionedStaffListModel[size];
        }
    };

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public ArrayList<StaffObject> getStaffList() {
        return staffList;
    }

    public void setStaffList(ArrayList<StaffObject> staffList) {
        this.staffList = staffList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sectionName);
        dest.writeTypedList(staffList);
    }
}
