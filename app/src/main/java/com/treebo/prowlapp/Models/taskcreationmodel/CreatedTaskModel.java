package com.treebo.prowlapp.Models.taskcreationmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Utils.Constants;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class CreatedTaskModel implements Parcelable {

    @SerializedName("id")
    private Integer taskID;

    @SerializedName("task_name")
    private String taskName;

    @SerializedName("task_details")
    private String taskDetails;

    @SerializedName("content_link")
    private String contentLink;

    @SerializedName("tooltip_text")
    private String tooltipText;

    @SerializedName("start_time")
    private String startTime;

    @SerializedName("end_time")
    private String endTime;

    @SerializedName("appear_date")
    private String appearDate;

    @SerializedName("completion_date")
    private String completionDate;

    @SerializedName("expiry_date")
    private String expiryDate;

    @SerializedName("all_hotels")
    private boolean isTaskForAllHotels;

    @SerializedName("recurrence_enabled")
    private boolean isRecurrenceEnabled;

    @SerializedName("city_ids")
    private int[] cityIDList;

    @SerializedName("hotel_ids")
    private int[] hotelIDList;

    @SerializedName("cluster_ids")
    private int[] clusterIDList;

    @SerializedName("region_ids")
    private int[] regionIDList;

    @SerializedName("created_by_id")
    private Integer createdByID;

    @SerializedName("geofence_enabled")
    private boolean isGeofenceEnabled;

    @SerializedName("task_type")
    private String taskType;

    @SerializedName("visual_representation_type")
    private String visualRepType;

    @SerializedName("priority")
    private Integer taskPriority;

    @SerializedName("pinned")
    private boolean isPinned;

    @SerializedName("recurrence")
    private Recurrence recurrence;

    @SerializedName("task_logs")
    private ArrayList<TaskLog> taskLogs;

    private String sectionHeader;

    public void setTaskID(int taskID) {
        this.taskID = taskID;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public void setTaskDetails(String taskDetails) {
        this.taskDetails = taskDetails;
    }

    public void setContentLink(String contentLink) {
        this.contentLink = contentLink;
    }

    public void setTooltipText(String tooltipText) {
        this.tooltipText = tooltipText;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setAppearDate(String appearDate) {
        this.appearDate = appearDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setTaskForAllHotels(boolean taskForAllHotels) {
        isTaskForAllHotels = taskForAllHotels;
    }

    public void setRecurrenceEnabled(boolean recurrenceEnabled) {
        isRecurrenceEnabled = recurrenceEnabled;
    }

    public void setCityIDList(int[] cityIDList) {
        this.cityIDList = cityIDList;
    }

    public void setHotelIDList(int[] hotelIDList) {
        this.hotelIDList = hotelIDList;
    }

    public void setClusterIDList(int[] clusterIDList) {
        this.clusterIDList = clusterIDList;
    }

    public void setRegionIDList(int[] regionIDList) {
        this.regionIDList = regionIDList;
    }

    public void setCreatedByID(int createdByID) {
        this.createdByID = createdByID;
    }

    public void setGeofenceEnabled(boolean geofenceEnabled) {
        isGeofenceEnabled = geofenceEnabled;
    }

    public void setVisualRepType(String visualRepType) {
        this.visualRepType = visualRepType;
    }

    public void setTaskPriority(int taskPriority) {
        this.taskPriority = taskPriority;
    }

    public void setPinned(boolean pinned) {
        isPinned = pinned;
    }

    public void setRecurrence(Recurrence recurrence) {
        this.recurrence = recurrence;
    }

    public void setTaskLogs(ArrayList<TaskLog> taskLogs) {
        this.taskLogs = taskLogs;
    }

    public void setSectionHeader(String sectionHeader) {
        this.sectionHeader = sectionHeader;
    }

    public String getSectionHeader() {
        return sectionHeader == null ? "" : sectionHeader;
    }

    public Integer getTaskID() {
        return taskID;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getTaskDetails() {
        return taskDetails;
    }

    public String getContentLink() {
        return contentLink;
    }

    public String getTooltipText() {
        return tooltipText;
    }

    public String getStartTime() {
        return startTime != null ? startTime : "-";
    }

    public String getEndTime() {
        return endTime != null ? endTime : "-";
    }

    public String getAppearDate() {
        return appearDate != null ? appearDate : "-";
    }

    public String getCompletionDate() {
        return completionDate != null ? completionDate : "-";
    }

    public String getExpiryDate() {
        return expiryDate != null ? expiryDate : "-";
    }

    public boolean isTaskForAllHotels() {
        return isTaskForAllHotels;
    }

    public boolean isRecurrenceEnabled() {
        return isRecurrenceEnabled;
    }

    public int[] getCityIDList() {
        return cityIDList == null ? new int[0] : cityIDList;
    }

    public int[] getHotelIDList() {
        return hotelIDList == null ? new int[0] : hotelIDList;
    }

    public int[] getClusterIDList() {
        return clusterIDList == null ? new int[0] : clusterIDList;
    }

    public int[] getRegionIDList() {
        return regionIDList == null ? new int[0] : regionIDList;
    }

    public int getCreatedByID() {
        return createdByID;
    }

    public boolean isGeofenceEnabled() {
        return isGeofenceEnabled;
    }

    public int getTaskType() {
        switch (taskType) {
            case "Bottom Dummy":
                return Constants.BOTTOM_DUMMY_TASK;

            default:
                return Constants.CREATED_TASK;
        }
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getVisualRepType() {
        return visualRepType;
    }

    public int getTaskPriority() {
        return taskPriority;
    }

    public boolean isPinned() {
        return isPinned;
    }

    public Recurrence getRecurrence() {
//        return recurrence;
        Recurrence recurrence = new Recurrence();
        recurrence.setId(2);
        recurrence.setSeparationCount(3);
        recurrence.setMaxOccurrences(4);
        return recurrence;
    }

    public ArrayList<TaskLog> getTaskLogs() {
        return taskLogs;
    }

    public int getStartedAndCompletedTaskLogCount() {
        int count = 0;
        if (taskLogs != null) {
            for (TaskLog log : taskLogs) {
                if (log.getTaskStatus().toLowerCase().equals("complete")
                        || log.getTaskStatus().toLowerCase().equals("started")) {
                    count++;
                }
            }
        }
        return count;
    }

    public CreatedTaskModel() {

    }

    protected CreatedTaskModel(Parcel in) {
        taskID = in.readInt();
        taskName = in.readString();
        taskDetails = in.readString();
        contentLink = in.readString();
        tooltipText = in.readString();
        startTime = in.readString();
        endTime = in.readString();
        appearDate = in.readString();
        completionDate = in.readString();
        expiryDate = in.readString();
        isTaskForAllHotels = in.readByte() != 0;
        isRecurrenceEnabled = in.readByte() != 0;
        cityIDList = in.createIntArray();
        hotelIDList = in.createIntArray();
        clusterIDList = in.createIntArray();
        regionIDList = in.createIntArray();
        createdByID = in.readInt();
        isGeofenceEnabled = in.readByte() != 0;
        taskType = in.readString();
        visualRepType = in.readString();
        taskPriority = in.readInt();
        isPinned = in.readByte() != 0;
        recurrence = in.readParcelable(Recurrence.class.getClassLoader());
        taskLogs = in.createTypedArrayList(TaskLog.CREATOR);
        sectionHeader = in.readString();
    }

    public static final Creator<CreatedTaskModel> CREATOR = new Creator<CreatedTaskModel>() {
        @Override
        public CreatedTaskModel createFromParcel(Parcel in) {
            return new CreatedTaskModel(in);
        }

        @Override
        public CreatedTaskModel[] newArray(int size) {
            return new CreatedTaskModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(taskID);
        dest.writeString(taskName);
        dest.writeString(taskDetails);
        dest.writeString(contentLink);
        dest.writeString(tooltipText);
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeString(appearDate);
        dest.writeString(completionDate);
        dest.writeString(expiryDate);
        dest.writeByte((byte) (isTaskForAllHotels ? 1 : 0));
        dest.writeByte((byte) (isRecurrenceEnabled ? 1 : 0));
        dest.writeIntArray(cityIDList);
        dest.writeIntArray(hotelIDList);
        dest.writeIntArray(clusterIDList);
        dest.writeIntArray(regionIDList);
        dest.writeInt(createdByID);
        dest.writeByte((byte) (isGeofenceEnabled ? 1 : 0));
        dest.writeString(taskType);
        dest.writeString(visualRepType);
        dest.writeInt(taskPriority);
        dest.writeByte((byte) (isPinned ? 1 : 0));
        dest.writeParcelable(recurrence, flags);
        dest.writeTypedList(taskLogs);
        dest.writeString(sectionHeader);
    }


    public static class Recurrence implements Parcelable {

        @SerializedName("id")
        private int id;

        @SerializedName("separation_count")
        private int separationCount;

        @SerializedName("max_occurrences")
        private Integer maxOccurrences;

        @SerializedName("end_date")
        private String endDate;

        @SerializedName("is_valid_occurrence")
        private boolean isValidOccurrence;

        public Recurrence() {

        }

        protected Recurrence(Parcel in) {
            id = in.readInt();
            separationCount = in.readInt();
            maxOccurrences = in.readInt();
            endDate = in.readString();
            isValidOccurrence = in.readByte() != 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(separationCount);
            dest.writeInt(maxOccurrences == null ? 0 : maxOccurrences);
            dest.writeString(endDate);
            dest.writeByte((byte) (isValidOccurrence ? 1 : 0));
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Recurrence> CREATOR = new Creator<Recurrence>() {
            @Override
            public Recurrence createFromParcel(Parcel in) {
                return new Recurrence(in);
            }

            @Override
            public Recurrence[] newArray(int size) {
                return new Recurrence[size];
            }
        };

        public int getId() {
            return id;
        }

        public int getSeparationCount() {
            return separationCount;
        }

        public int getMaxOccurrences() {
            return maxOccurrences != null ? maxOccurrences : 0;
        }

        public String getEndDate() {
            return endDate != null ? endDate : "";
        }

        public boolean isValidOccurrence() {
            return isValidOccurrence;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setSeparationCount(int separationCount) {
            this.separationCount = separationCount;
        }

        public void setMaxOccurrences(Integer maxOccurrences) {
            this.maxOccurrences = maxOccurrences;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public void setValidOccurrence(boolean validOccurrence) {
            isValidOccurrence = validOccurrence;
        }

    }

    public static class TaskLog implements Parcelable {

        @SerializedName("id")
        private int id;

        @SerializedName("hotel")
        private String hotelName;

        @SerializedName("user")
        private String userEmail;

        @SerializedName("status")
        private String taskStatus;

        @SerializedName("end_date")
        private String endDate;

        @SerializedName("city")
        private String city;

        private boolean isSelected = true;

        protected TaskLog(Parcel in) {
            id = in.readInt();
            hotelName = in.readString();
            userEmail = in.readString();
            taskStatus = in.readString();
            endDate = in.readString();
            city = in.readString();
            isSelected = in.readByte() != 0;
        }

        public static final Creator<TaskLog> CREATOR = new Creator<TaskLog>() {
            @Override
            public TaskLog createFromParcel(Parcel in) {
                return new TaskLog(in);
            }

            @Override
            public TaskLog[] newArray(int size) {
                return new TaskLog[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(hotelName);
            dest.writeString(userEmail);
            dest.writeString(taskStatus);
            dest.writeString(endDate);
            dest.writeString(city);
            dest.writeByte((byte) (isSelected ? 1 : 0));
        }

        public int getId() {
            return id;
        }

        public String getHotelName() {
            return hotelName;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public String getTaskStatus() {
            return taskStatus;
        }

        public String getEndDate() {
            return endDate;
        }

        public String getCity() {
            return city;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
