package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by devesh on 02/05/16.
 */
public class DepartmentModel implements Parcelable{

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("type")
    public String type;

    @SerializedName("weight")
    public int weight;


    protected DepartmentModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        type = in.readString();
        weight = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(type);
        dest.writeInt(weight);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DepartmentModel> CREATOR = new Creator<DepartmentModel>() {
        @Override
        public DepartmentModel createFromParcel(Parcel in) {
            return new DepartmentModel(in);
        }

        @Override
        public DepartmentModel[] newArray(int size) {
            return new DepartmentModel[size];
        }
    };
}
