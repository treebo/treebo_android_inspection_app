package com.treebo.prowlapp.Models.portfoliomodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.GenericMetricModel;

/**
 * Created by abhisheknair on 24/11/16.
 */

public class PortfolioMetrics implements Parcelable {

    @SerializedName("task_completion")
    private GenericMetricModel taskCompletionRation;

    @SerializedName("issue_not_caught")
    private GenericMetricModel issueNotCaughtCount;

    @SerializedName("compliance")
    private GenericMetricModel compliance;

    @SerializedName("delight_score")
    private GenericMetricModel delightPercent;

    @SerializedName("disaster_score")
    private GenericMetricModel disasterPercent;


    protected PortfolioMetrics(Parcel in) {
        taskCompletionRation = in.readParcelable(GenericMetricModel.class.getClassLoader());
        issueNotCaughtCount = in.readParcelable(GenericMetricModel.class.getClassLoader());
        compliance = in.readParcelable(GenericMetricModel.class.getClassLoader());
        delightPercent = in.readParcelable(GenericMetricModel.class.getClassLoader());
        disasterPercent = in.readParcelable(GenericMetricModel.class.getClassLoader());
    }

    public static final Creator<PortfolioMetrics> CREATOR = new Creator<PortfolioMetrics>() {
        @Override
        public PortfolioMetrics createFromParcel(Parcel in) {
            return new PortfolioMetrics(in);
        }

        @Override
        public PortfolioMetrics[] newArray(int size) {
            return new PortfolioMetrics[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(taskCompletionRation, i);
        parcel.writeParcelable(issueNotCaughtCount, i);
        parcel.writeParcelable(compliance, i);
        parcel.writeParcelable(delightPercent, i);
        parcel.writeParcelable(disasterPercent, i);
    }

    public GenericMetricModel getTaskCompletionRatio() {
        return taskCompletionRation != null ? taskCompletionRation : new GenericMetricModel();
    }

    public GenericMetricModel getIssueNotCaughtCount() {
        return issueNotCaughtCount != null ? issueNotCaughtCount : new GenericMetricModel();
    }

    public GenericMetricModel getDelightPercent() {
        return delightPercent != null ? delightPercent : new GenericMetricModel();
    }

    public GenericMetricModel getCompliance() {
        return compliance != null ? compliance : new GenericMetricModel();
    }

    public GenericMetricModel getDisasterPercent() {
        return disasterPercent != null ? disasterPercent : new GenericMetricModel();
    }
}
