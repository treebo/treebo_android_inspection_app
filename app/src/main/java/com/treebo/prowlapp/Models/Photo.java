package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.treebo.prowlapp.dbflow.ProwlDatabase;

/**
 * Created by sumandas on 16/06/2016.
 */

@Table(database = ProwlDatabase.class)
public class Photo extends BaseModel implements Parcelable {

    @PrimaryKey
    public String mPhotoId;

    //is hotel_queryId/hotel_room_queryId for audit
    //is issueId for issues
    @Column
    public String mKey;

    @Column(defaultValue = "")
    public String mDeviceFilePath;

    @Column(defaultValue = "")
    public String mCloudFilePath;

    @Column
    public boolean isUploadedToCloud;


    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }

    public String getmDeviceFilePath() {
        return mDeviceFilePath == null ? "" : mDeviceFilePath;
    }

    public void setmDeviceFilePath(String mDeviceFilePath) {
        this.mDeviceFilePath = mDeviceFilePath;
    }

    public String getmCloudFilePath() {
        return mCloudFilePath == null ? "" : mCloudFilePath;
    }

    public void setmCloudFilePath(String mCloudFilePath) {
        this.mCloudFilePath = mCloudFilePath;
    }

    public boolean isUploadedToCloud() {
        return isUploadedToCloud;
    }

    public void setIsUploadedToCloud(boolean isUploadedToCloud) {
        this.isUploadedToCloud = isUploadedToCloud;
    }

    public Photo() {
    }

    public String getmPhotoId() {
        return mPhotoId;
    }

    public void setmPhotoId(String mPhotoId) {
        this.mPhotoId = mPhotoId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mPhotoId);
        dest.writeString(this.mKey);
        dest.writeString(this.mDeviceFilePath);
        dest.writeString(this.mCloudFilePath);
        dest.writeByte(this.isUploadedToCloud ? (byte) 1 : (byte) 0);
    }

    protected Photo(Parcel in) {
        this.mPhotoId = in.readString();
        this.mKey = in.readString();
        this.mDeviceFilePath = in.readString();
        this.mCloudFilePath = in.readString();
        this.isUploadedToCloud = in.readByte() != 0;
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
