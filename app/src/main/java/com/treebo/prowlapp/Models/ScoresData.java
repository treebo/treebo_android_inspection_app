package com.treebo.prowlapp.Models;

/**
 * Created by devesh on 01/05/16.
 */
public class ScoresData {
    public String mName;
    public double mValue;

    public ScoresData(String name ,double value){
        this.mName= name;
        this.mValue = value;
    }

    @Override
    public String toString() {
        return "ScoresData{" +
                "mName='" + mName + '\'' +
                ", mValue=" + mValue +
                '}';
    }
}
