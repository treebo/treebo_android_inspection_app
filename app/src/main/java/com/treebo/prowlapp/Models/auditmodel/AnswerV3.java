package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sumandas on 08/11/2016.
 */

public class AnswerV3 implements Parcelable {

    @SerializedName("category_id")
    private Integer category_id;

    @SerializedName("checkpoint_id")
    private Integer checkpoint_id;

    @SerializedName("subcheckpoint_id")
    private Integer subcheckpoint_id;

    @SerializedName("comment")
    private String comment;

    @SerializedName("image_url")
    private String image_url;

    @SerializedName("is_instantly_resolved")
    private Boolean is_instantly_resolved;

    @SerializedName("created_at")
    private String createdAt;

    private String subcheckpointText;
    private String checkpointText;

    private String categoryText;

    private int tat;

    @Expose(serialize = false)
    public boolean isPendingDelete;

    @Expose(serialize = false)
    public boolean isSoftDeleted;

    @Expose(serialize = false)
    public boolean isAuditDone;

    public AnswerV3() {

    }

    public AnswerV3(int categoryId, int checkPointId, int subCheckPointId, String comment, String imageUrl, boolean is_instantly_resolved) {
        this.category_id = categoryId;
        this.checkpoint_id = checkPointId;
        this.subcheckpoint_id = subCheckPointId;
        this.comment = comment;
        this.image_url = imageUrl;
        this.is_instantly_resolved = is_instantly_resolved;
    }

    public AnswerV3(int categoryId, boolean auditDone) {
        category_id = categoryId;
        isAuditDone = auditDone;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public Integer getCheckpoint_id() {
        return checkpoint_id;
    }

    public void setCheckpoint_id(Integer checkpoint_id) {
        this.checkpoint_id = checkpoint_id;
    }

    public Integer getSubcheckpoint_id() {
        return subcheckpoint_id;
    }

    public void setSubcheckpoint_id(Integer subcheckpoint_id) {
        this.subcheckpoint_id = subcheckpoint_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getIs_instantly_resolved() {
        return is_instantly_resolved;
    }

    public void setIs_instantly_resolved(Boolean is_instantly_resolved) {
        this.is_instantly_resolved = is_instantly_resolved;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getSubcheckpointText() {
        return subcheckpointText;
    }

    public void setSubcheckpointText(String subcheckpointText) {
        this.subcheckpointText = subcheckpointText;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCategoryText() {
        return categoryText;
    }

    public void setCategoryText(String categoryText) {
        this.categoryText = categoryText;
    }

    public String getCheckpointText() {
        return checkpointText;
    }

    public void setCheckpointText(String checkpointText) {
        this.checkpointText = checkpointText;
    }

    public int getTat() {
        return tat;
    }

    public void setTat(int tat) {
        this.tat = tat;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AnswerV3)) {
            return false;
        }
        AnswerV3 answerV3 = (AnswerV3) o;
        return (category_id.equals(answerV3.category_id) &&
                checkpoint_id.equals(answerV3.checkpoint_id) &&
                subcheckpoint_id.equals(answerV3.subcheckpoint_id));
    }

    public boolean auditDoneEquals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof AnswerV3)) {
            return false;
        }
        AnswerV3 answerV3 = (AnswerV3) o;
        return (category_id.equals(answerV3.category_id) && (isAuditDone == answerV3.isAuditDone));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.category_id);
        dest.writeValue(this.checkpoint_id);
        dest.writeValue(this.subcheckpoint_id);
        dest.writeString(this.comment);
        dest.writeString(this.image_url);
        dest.writeValue(this.is_instantly_resolved);
        dest.writeString(this.createdAt);
        dest.writeString(this.subcheckpointText);
        dest.writeString(this.checkpointText);
        dest.writeString(this.categoryText);
        dest.writeByte(this.isPendingDelete ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isSoftDeleted ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isAuditDone ? (byte) 1 : (byte) 0);
        dest.writeInt(this.tat);
    }

    protected AnswerV3(Parcel in) {
        this.category_id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.checkpoint_id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subcheckpoint_id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.comment = in.readString();
        this.image_url = in.readString();
        this.is_instantly_resolved = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.createdAt = in.readString();
        this.subcheckpointText = in.readString();
        this.checkpointText = in.readString();
        this.categoryText = in.readString();
        this.isPendingDelete = in.readByte() != 0;
        this.isSoftDeleted = in.readByte() != 0;
        this.isAuditDone = in.readByte() != 0;
        this.tat = in.readInt();
    }

    public static final Creator<AnswerV3> CREATOR = new Creator<AnswerV3>() {
        @Override
        public AnswerV3 createFromParcel(Parcel source) {
            return new AnswerV3(source);
        }

        @Override
        public AnswerV3[] newArray(int size) {
            return new AnswerV3[size];
        }
    };
}