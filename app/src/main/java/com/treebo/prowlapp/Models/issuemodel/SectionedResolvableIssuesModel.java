package com.treebo.prowlapp.Models.issuemodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.treebo.prowlapp.Models.auditmodel.AnswerV3;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 08/11/16.
 */

public class SectionedResolvableIssuesModel implements Parcelable {

    private String sectionName;
    private String sectionCutoff;
    private ArrayList<AnswerV3> issueList;
    private String createdAt;

    public SectionedResolvableIssuesModel() {

    }

    protected SectionedResolvableIssuesModel(Parcel in) {
        sectionName = in.readString();
        sectionCutoff = in.readString();
        issueList = in.createTypedArrayList(AnswerV3.CREATOR);
        createdAt = in.readString();
    }

    public static final Creator<SectionedResolvableIssuesModel> CREATOR = new Creator<SectionedResolvableIssuesModel>() {
        @Override
        public SectionedResolvableIssuesModel createFromParcel(Parcel in) {
            return new SectionedResolvableIssuesModel(in);
        }

        @Override
        public SectionedResolvableIssuesModel[] newArray(int size) {
            return new SectionedResolvableIssuesModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(sectionName);
        parcel.writeString(sectionCutoff);
        parcel.writeTypedList(issueList);
        parcel.writeString(createdAt);
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public ArrayList<AnswerV3> getIssueList() {
        return issueList;
    }

    public void setIssueList(ArrayList<AnswerV3> issueList) {
        this.issueList = issueList;
    }

    public String getSectionCutoff() {
        return sectionCutoff;
    }

    public void setSectionCutoff(String sectionCutoff) {
        this.sectionCutoff = sectionCutoff;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
