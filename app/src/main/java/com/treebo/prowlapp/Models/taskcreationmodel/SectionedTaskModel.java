package com.treebo.prowlapp.Models.taskcreationmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.treebo.prowlapp.Utils.Constants;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class SectionedTaskModel implements Parcelable {

    private String sectionName;
    private ArrayList<CreatedTaskModel> taskList = new ArrayList<>();

    public SectionedTaskModel() {

    }

    protected SectionedTaskModel(Parcel in) {
        sectionName = in.readString();
        taskList = in.createTypedArrayList(CreatedTaskModel.CREATOR);
    }

    public static final Creator<CreatedTaskModel> CREATOR = new Creator<CreatedTaskModel>() {
        @Override
        public CreatedTaskModel createFromParcel(Parcel in) {
            return new CreatedTaskModel(in);
        }

        @Override
        public CreatedTaskModel[] newArray(int size) {
            return new CreatedTaskModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(sectionName);
        parcel.writeTypedList(taskList);
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public ArrayList<CreatedTaskModel> getTaskList() {
        return taskList;
    }

    public int getTaskListSize() {
        int count = 0;
        for (CreatedTaskModel model: this.taskList) {
            if (model.getTaskType() != Constants.BOTTOM_DUMMY_TASK)
                count++;
        }
        return count;
    }

    public void setTaskList(ArrayList<CreatedTaskModel> taskList) {
        this.taskList = taskList;
    }

    public void setIssueInList(CreatedTaskModel issue) {
        this.taskList.add(issue);
    }
}

