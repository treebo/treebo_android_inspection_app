package com.treebo.prowlapp.Models.dashboardmodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class SectionedHistoryTaskListModel implements Parcelable {

    private String sectionName;

    private ArrayList<TaskHistoryModel> mTaskHistory;

    public SectionedHistoryTaskListModel() {

    }

    protected SectionedHistoryTaskListModel(Parcel in) {
        sectionName = in.readString();
        mTaskHistory = in.createTypedArrayList(TaskHistoryModel.CREATOR);
    }

    public static final Creator<SectionedHistoryTaskListModel> CREATOR = new Creator<SectionedHistoryTaskListModel>() {
        @Override
        public SectionedHistoryTaskListModel createFromParcel(Parcel in) {
            return new SectionedHistoryTaskListModel(in);
        }

        @Override
        public SectionedHistoryTaskListModel[] newArray(int size) {
            return new SectionedHistoryTaskListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(sectionName);
        parcel.writeTypedList(mTaskHistory);
    }

    public String getSectionName() {
        return sectionName;
    }

    public ArrayList<TaskHistoryModel> getmTaskHistory() {
        return mTaskHistory;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public void setmTaskHistory(ArrayList<TaskHistoryModel> mTaskHistory) {
        this.mTaskHistory = mTaskHistory;
    }
}
