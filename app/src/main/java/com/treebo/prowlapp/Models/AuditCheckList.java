package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.treebo.prowlapp.Models.auditmodel.Checkpoint;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 02/11/16.
 */
@Deprecated
public class AuditCheckList implements Parcelable {

    public String title = "";
    public ArrayList<Checkpoint> checkPoints = new ArrayList<>();


    public AuditCheckList() {}

    protected AuditCheckList(Parcel in) {
        title = in.readString();
        checkPoints = in.createTypedArrayList(Checkpoint.CREATOR);
    }

    public static final Creator<AuditCheckList> CREATOR = new Creator<AuditCheckList>() {
        @Override
        public AuditCheckList createFromParcel(Parcel in) {
            return new AuditCheckList(in);
        }

        @Override
        public AuditCheckList[] newArray(int size) {
            return new AuditCheckList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeTypedList(checkPoints);
    }
}