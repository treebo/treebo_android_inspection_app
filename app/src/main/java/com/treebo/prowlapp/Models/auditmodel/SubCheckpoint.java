
package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.treebo.prowlapp.dbflow.ProwlDatabase;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.util.List;

/**
 * Created by sumandas on 03/11/2016.
 */

@Table(database = ProwlDatabase.class)
public class SubCheckpoint extends BaseModel implements Parcelable {


    @Column
    @PrimaryKey
    @SerializedName("id")
    public int mSubCheckpointId;


    @Column
    @SerializedName("description")
    public String mDescription;


    @Column
    @ForeignKey(tableClass = Checkpoint.class)
    public int mCheckpointId;

    @Column
    @SerializedName("tat")
    public int mTurnaroundTime;

    @Column
    @SerializedName("is_extra_info_required")
    public String isCommentRequired;

    private boolean isIssue;

    private String issueText;

    private String photoUrls;

    private boolean isPendingIssue;

    private IssueListModelV3 pendingIssueDetails;

    public SubCheckpoint() {

    }


    protected SubCheckpoint(Parcel in) {
        mSubCheckpointId = in.readInt();
        mDescription = in.readString();
        mCheckpointId = in.readInt();
        mTurnaroundTime = in.readInt();
        isCommentRequired = in.readString();
        isIssue = in.readByte() != 0;
        issueText = in.readString();
        photoUrls = in.readString();
        isPendingIssue = in.readByte() != 0;
        pendingIssueDetails = in.readParcelable(IssueListModelV3.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mSubCheckpointId);
        dest.writeString(mDescription);
        dest.writeInt(mCheckpointId);
        dest.writeInt(mTurnaroundTime);
        dest.writeString(isCommentRequired);
        dest.writeByte((byte) (isIssue ? 1 : 0));
        dest.writeString(issueText);
        dest.writeString(photoUrls);
        dest.writeByte((byte) (isPendingIssue ? 1 : 0));
        dest.writeParcelable(pendingIssueDetails, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubCheckpoint> CREATOR = new Creator<SubCheckpoint>() {
        @Override
        public SubCheckpoint createFromParcel(Parcel in) {
            return new SubCheckpoint(in);
        }

        @Override
        public SubCheckpoint[] newArray(int size) {
            return new SubCheckpoint[size];
        }
    };

    public static List<SubCheckpoint> getAllCheckPoints() {
        List<SubCheckpoint> subCheckpoints =
                SQLite.select().from(SubCheckpoint.class).queryList();
        return subCheckpoints;
    }


    public String getIssueText() {
        return issueText;
    }

    public void setIssueText(String issueText) {
        this.issueText = issueText;
    }

    public String getPhotoUrls() {
        return photoUrls;
    }

    public void setPhotoUrls(String photoUrls) {
        this.photoUrls = photoUrls;
    }

    public boolean isCommentRequired() {
        if (TextUtils.isEmpty(isCommentRequired))
            LoginSharedPrefManager.getInstance().setMasterListFetched(false);
        return !("N").equals(isCommentRequired);
    }

    public void setCommentRequired(boolean commentRequired) {
        isCommentRequired = commentRequired ? "Y" : "N";
    }

    public int getmTurnaroundTime() {
        return mTurnaroundTime;
    }

    public void setmTurnaroundTime(int mTurnaroundTime) {
        this.mTurnaroundTime = mTurnaroundTime;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getmCheckpointId() {
        return mCheckpointId;
    }

    public void setmCheckpointId(int mCheckpointId) {
        this.mCheckpointId = mCheckpointId;
    }

    public int getmSubCheckpointId() {
        return mSubCheckpointId;
    }

    public void setmSubCheckpointId(int mSubCheckpointId) {
        this.mSubCheckpointId = mSubCheckpointId;
    }

    public boolean issue() {
        return isIssue;
    }

    public void setIssue(boolean issue) {
        isIssue = issue;
    }

    public boolean isPendingIssue() {
        return isPendingIssue;
    }

    public void setPendingIssue(boolean pendingIssue) {
        isPendingIssue = pendingIssue;
    }

    public IssueListModelV3 getPendingIssueDetails() {
        return pendingIssueDetails;
    }

    public void setPendingIssueDetails(IssueListModelV3 pendingIssueDetails) {
        this.pendingIssueDetails = pendingIssueDetails;
        this.pendingIssueDetails.setDueDateText();
    }
}
