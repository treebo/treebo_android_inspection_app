package com.treebo.prowlapp.Models.dashboardmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.treebo.prowlapp.Models.GenericMetricModel;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class HotelDashboardMetrics implements Parcelable {

    @SerializedName("compliance")
    private GenericMetricModel compliance;

    @SerializedName("delight_score")
    private GenericMetricModel delightPercent;

    @SerializedName("disaster_score")
    private GenericMetricModel disasterPercent;

    protected HotelDashboardMetrics(Parcel in) {
        compliance = in.readParcelable(GenericMetricModel.class.getClassLoader());
        delightPercent = in.readParcelable(GenericMetricModel.class.getClassLoader());
        disasterPercent = in.readParcelable(GenericMetricModel.class.getClassLoader());
    }

    public static final Creator<HotelDashboardMetrics> CREATOR = new Creator<HotelDashboardMetrics>() {
        @Override
        public HotelDashboardMetrics createFromParcel(Parcel in) {
            return new HotelDashboardMetrics(in);
        }

        @Override
        public HotelDashboardMetrics[] newArray(int size) {
            return new HotelDashboardMetrics[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(compliance, i);
        parcel.writeParcelable(delightPercent, i);
        parcel.writeParcelable(disasterPercent, i);
    }

    public GenericMetricModel getCompliance() {
        return compliance;
    }

    public GenericMetricModel getDelightPercent() {
        return delightPercent;
    }

    public GenericMetricModel getDisasterPercent() {
        return disasterPercent;
    }
}
