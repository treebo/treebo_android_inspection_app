package com.treebo.prowlapp.Models.issuemodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 15/11/16.
 */

public class SectionedIssueListModel implements Parcelable {

    private String sectionName;
    private ArrayList<IssueListModelV3> issueList = new ArrayList<>();

    public SectionedIssueListModel() {

    }

    protected SectionedIssueListModel(Parcel in) {
        sectionName = in.readString();
        issueList = in.createTypedArrayList(IssueListModelV3.CREATOR);
    }

    public static final Creator<SectionedIssueListModel> CREATOR = new Creator<SectionedIssueListModel>() {
        @Override
        public SectionedIssueListModel createFromParcel(Parcel in) {
            return new SectionedIssueListModel(in);
        }

        @Override
        public SectionedIssueListModel[] newArray(int size) {
            return new SectionedIssueListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(sectionName);
        parcel.writeTypedList(issueList);
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public ArrayList<IssueListModelV3> getIssueList() {
        return issueList;
    }

    public void setIssueList(ArrayList<IssueListModelV3> issueList) {
        this.issueList = issueList;
    }

    public void setIssueInList(IssueListModelV3 issue) {
        this.issueList.add(issue);
    }
}
