package com.treebo.prowlapp.Models.taskcreationmodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abhisheknair on 23/06/17.
 */

public class TextSubtextModel implements Parcelable {

    private int id;

    private String text;

    private String subText;

    private boolean isSelected;

    public TextSubtextModel() {

    }

    protected TextSubtextModel(Parcel in) {
        id = in.readInt();
        text = in.readString();
        subText = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(text);
        dest.writeString(subText);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TextSubtextModel> CREATOR = new Creator<TextSubtextModel>() {
        @Override
        public TextSubtextModel createFromParcel(Parcel in) {
            return new TextSubtextModel(in);
        }

        @Override
        public TextSubtextModel[] newArray(int size) {
            return new TextSubtextModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text != null ? text : "";
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSubText() {
        return subText != null ? subText : "";
    }

    public void setSubText(String subText) {
        this.subText = subText;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
