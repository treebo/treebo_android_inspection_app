package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.treebo.prowlapp.dbflow.ProwlDatabase;

import java.util.List;

/**
 * Created by abhisheknair on 19/04/17.
 */
@Table(database = ProwlDatabase.class)
public class PeriodicAuditComplete extends BaseModel implements Parcelable {

    @Column
    @PrimaryKey(autoincrement = true)
    public int id;

    @Column
    public int mHotelID;

    @Column
    public int mAuditCategoryID;

    @Column
    public int mTaskLogID;

    @Column
    public String mTaskEndTime;

    public PeriodicAuditComplete(int hotelID, int auditCategoryID, int taskLogID, String endTime) {
        this.mHotelID = hotelID;
        this.mAuditCategoryID = auditCategoryID;
        this.mTaskLogID = taskLogID;
        this.mTaskEndTime = endTime;
    }

    public PeriodicAuditComplete() {

    }


    protected PeriodicAuditComplete(Parcel in) {
        mHotelID = in.readInt();
        mAuditCategoryID = in.readInt();
        mTaskLogID = in.readInt();
        mTaskEndTime = in.readString();
    }

    public static final Creator<PeriodicAuditComplete> CREATOR = new Creator<PeriodicAuditComplete>() {
        @Override
        public PeriodicAuditComplete createFromParcel(Parcel in) {
            return new PeriodicAuditComplete(in);
        }

        @Override
        public PeriodicAuditComplete[] newArray(int size) {
            return new PeriodicAuditComplete[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mHotelID);
        dest.writeInt(mAuditCategoryID);
        dest.writeInt(mTaskLogID);
        dest.writeString(mTaskEndTime);
    }

    public static int getSubAuditDoneCountForTask(int taskLogID) {
        List<PeriodicAuditComplete> auditCompleteObjectList = SQLite.select().from(PeriodicAuditComplete.class)
                .where(PeriodicAuditComplete_Table.mTaskLogID.eq(taskLogID)).queryList();
        return auditCompleteObjectList.size();
    }

    public static void deleteSubmittedPeriodicAudit(int taskLogID) {
        SQLite.delete().from(PeriodicAuditComplete.class).where(PeriodicAuditComplete_Table.mTaskLogID.eq(taskLogID));
    }

    public static List<PeriodicAuditComplete> getSubAuditDoneList(int taskLogID) {
        return SQLite.select().from(PeriodicAuditComplete.class)
                .where(PeriodicAuditComplete_Table.mTaskLogID.eq(taskLogID)).queryList();
    }

    public static boolean isSubAuditInList(int taskLogID, int auditCategoryID) {
        PeriodicAuditComplete auditCompleteObject = SQLite.select().from(PeriodicAuditComplete.class)
                .where(PeriodicAuditComplete_Table.mTaskLogID.eq(taskLogID),
                        PeriodicAuditComplete_Table.mAuditCategoryID.eq(auditCategoryID))
                .querySingle();
        return auditCompleteObject != null;
    }
}
