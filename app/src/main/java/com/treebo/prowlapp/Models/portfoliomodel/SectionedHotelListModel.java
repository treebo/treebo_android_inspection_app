package com.treebo.prowlapp.Models.portfoliomodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 24/11/16.
 */

public class SectionedHotelListModel implements Parcelable {

    private String sectionName;
    private ArrayList<PortfolioHotelV3> hotelList = new ArrayList<>();

    public SectionedHotelListModel() {

    }

    protected SectionedHotelListModel(Parcel in) {
        sectionName = in.readString();
        hotelList = in.createTypedArrayList(PortfolioHotelV3.CREATOR);
    }

    public static final Creator<SectionedHotelListModel> CREATOR = new Creator<SectionedHotelListModel>() {
        @Override
        public SectionedHotelListModel createFromParcel(Parcel in) {
            return new SectionedHotelListModel(in);
        }

        @Override
        public SectionedHotelListModel[] newArray(int size) {
            return new SectionedHotelListModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(sectionName);
        parcel.writeTypedList(hotelList);
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public void setHotelList(ArrayList<PortfolioHotelV3> hotelList) {
        this.hotelList = hotelList;
    }

    public String getSectionName() {
        return sectionName;
    }

    public ArrayList<PortfolioHotelV3> getHotelList() {
        return hotelList;
    }

    public void setHotelInHotelList(PortfolioHotelV3 hotel) {
        if (this.hotelList == null)
            this.hotelList = new ArrayList<>();
        this.hotelList.add(hotel);
    }
}

