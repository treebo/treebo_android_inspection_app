
package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.treebo.prowlapp.dbflow.ProwlDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumandas on 03/11/2016.
 */

@Table(database = ProwlDatabase.class)
public class Category extends BaseModel implements Parcelable {

    public static final String INCLUDED_CATEGORY_LIST = "included_category_list";

    @Column
    @PrimaryKey
    @SerializedName("id")
    public int mCategoryId;

    @Column
    @SerializedName("description")
    public String mDescription;

    private boolean auditDone;

    private boolean issueRaised;

    @SerializedName("array")
    public ArrayList<Checkpoint> mCheckPoints = new ArrayList<>();

    private boolean isPendingIssue;

    public Category() {

    }

    protected Category(Parcel in) {
        mCategoryId = in.readInt();
        mDescription = in.readString();
        in.readTypedList(this.mCheckPoints, Checkpoint.CREATOR);
        auditDone = in.readByte() != 0;
        issueRaised = in.readByte() != 0;
        isPendingIssue = in.readByte() != 0;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "mCheckPoints")
    public List<Checkpoint> getCheckpoints() {
        if (mCheckPoints == null || mCheckPoints.isEmpty()) {
            mCheckPoints = new ArrayList<>(SQLite.select()
                    .from(Checkpoint.class)
                    .where(Checkpoint_Table.mCategoryId_mCategoryId.eq(mCategoryId))
                    .queryList());
        }
        return mCheckPoints;
    }


    public static Category getCategoryById(int categoryId) {
        Category category = SQLite.select().
                from(Category.class).
                where(Category_Table.mCategoryId.eq(categoryId))
                .querySingle();
        return category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mCategoryId);
        parcel.writeString(mDescription);
        parcel.writeTypedList(mCheckPoints);
        parcel.writeByte((byte) (auditDone ? 1 : 0));
        parcel.writeByte((byte) (issueRaised ? 1 : 0));
        parcel.writeByte((byte) (isPendingIssue ? 1 : 0));
    }

    @Override
    public boolean equals(final Object object) {
        if (object == null || object == this || !(object instanceof Category))
            return false;

        Category category = (Category) object;
        if (category.mCategoryId != this.mCategoryId) return false;
        if (category.mDescription.equals(this.mDescription)) return false;
        if (!category.mCheckPoints.equals(this.mCheckPoints)) return false;

        return true;
    }

    public int getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(int mCategoryId) {
        this.mCategoryId = mCategoryId;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public boolean isAuditDone() {
        return auditDone;
    }

    public void setAuditDone(boolean auditDone) {
        this.auditDone = auditDone;
    }

    public boolean issueRaised() {
        return issueRaised;
    }

    public void setIssueRaised(boolean issueRaised) {
        this.issueRaised = issueRaised;
    }

    public ArrayList<Checkpoint> getmCheckPoints() {
        return mCheckPoints;
    }

    public void setCheckPoints(ArrayList<Checkpoint> mCheckPoints) {
        this.mCheckPoints = mCheckPoints;
    }

    public boolean isPendingIssue() {
        return isPendingIssue;
    }

    public void setPendingIssue(boolean pendingIssue) {
        isPendingIssue = pendingIssue;
    }
}
