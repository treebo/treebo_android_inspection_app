package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by sumandas on 29/04/2016.
 */
@Deprecated
public class RoomTypeList  implements Parcelable{

    @SerializedName("type")
    public String type;

    @SerializedName("rooms")
    public ArrayList<Room> rooms;

    public RoomTypeList(){
        rooms = new ArrayList<>();
    }

    public RoomTypeList(Parcel in){
        this();
        type=in.readString();
        in.readTypedList(rooms, Room.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeTypedList(rooms);
    }

    public static final Creator<RoomTypeList> CREATOR = new Creator<RoomTypeList>() {
        @Override
        public RoomTypeList createFromParcel(Parcel source) {
            return new RoomTypeList(source);
        }

        @Override
        public RoomTypeList[] newArray(int size) {
            return new RoomTypeList[size];
        }
    };
}
