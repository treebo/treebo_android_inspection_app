package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.treebo.prowlapp.dbflow.ProwlDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Created by sumandas on 14/11/2016.
 */
@Table(database = ProwlDatabase.class)
public class CompleteAudit extends BaseModel implements Parcelable {

    public static final int MAX_OF_ROOMS_TO_BE_AUDITED = 3;

    @Column
    @PrimaryKey
    public int mHotelId;

    @Column
    public boolean isCommonAudited;

    @Column
    public int mAvailableRooms = MAX_OF_ROOMS_TO_BE_AUDITED;

    @Column
    public String mRoomListedAudited; //comma separated audited rooms numbers

    @Column
    public int mDayofYear;


    public CompleteAudit(int hotelId) {
        mHotelId = hotelId;
        mDayofYear = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
    }

    public CompleteAudit() {

    }

    public ArrayList<String> getAuditedRooms() {
        ArrayList<String> roomList = new ArrayList<>();
        if (TextUtils.isEmpty(mRoomListedAudited)) {
            return roomList;
        } else {
            if (!mRoomListedAudited.contains(",")) {
                roomList.add(mRoomListedAudited);
                return roomList;
            } else {
                roomList = new ArrayList<>(Arrays.asList(mRoomListedAudited.split(",")));
                return roomList;
            }

        }

    }

    public boolean isDailyAuditCompleted() {
        return (isCommonAudited &&  mAvailableRooms <= 0);
    }

    public boolean isOneRoomAudited() {
        int count = isCommonAudited ? 1 : 0;
        count = count + getAuditedRooms().size();
        return count == 1;
    }

    public boolean isAuditStarted() {
        ArrayList<String> roomList = getAuditedRooms();
        if (isCommonAudited || roomList.size() > 0) {
            return true;
        } else {
            return false;
        }
    }


    public void addAuditedRoomNumber(String roomNumber) {
        if (TextUtils.isEmpty(mRoomListedAudited)) {
            mRoomListedAudited = roomNumber;
        } else {
            mRoomListedAudited = mRoomListedAudited + "," + roomNumber;
        }
    }

    public static CompleteAudit getCompleteAuditByHotelId(int mHotelId) {
        Calendar calendar = Calendar.getInstance();
        int currentDay = calendar.get(Calendar.DAY_OF_YEAR);
        return SQLite.select().from(CompleteAudit.class)
                .where(CompleteAudit_Table.mHotelId.eq(mHotelId))
                .and(CompleteAudit_Table.mDayofYear.eq(currentDay))
                .querySingle();
    }

    public static void updateAvailableRoomCountForAudit(int hotelId, int mAvailableRoom) {
        CompleteAudit currentAudit = getCompleteAuditByHotelId(hotelId);
        if (currentAudit == null) {
            currentAudit = new CompleteAudit(hotelId);
            currentAudit.mAvailableRooms = mAvailableRoom;
            currentAudit.save();
        } else {
            currentAudit.mAvailableRooms = mAvailableRoom;
            currentAudit.update();
        }

    }

    public static void updateCommonAreaAuditStatusForHotel(int hotelId) {
        CompleteAudit currentAudit = getCompleteAuditByHotelId(hotelId);
        if (currentAudit == null) {
            currentAudit = new CompleteAudit(hotelId);
            currentAudit.isCommonAudited = true;
            currentAudit.save();
        } else {
            currentAudit.isCommonAudited = true;
            currentAudit.update();
        }
    }

    public static void deletePreviousCompleteAudits() {
        Calendar calendar = Calendar.getInstance();
        int currentDay = calendar.get(Calendar.DAY_OF_YEAR);
        SQLite.delete().from(CompleteAudit.class).where(CompleteAudit_Table.mDayofYear.isNot(currentDay));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mHotelId);
        dest.writeByte(this.isCommonAudited ? (byte) 1 : (byte) 0);
        dest.writeInt(this.mAvailableRooms);
        dest.writeString(this.mRoomListedAudited);
        dest.writeInt(this.mDayofYear);
    }

    protected CompleteAudit(Parcel in) {
        this.mHotelId = in.readInt();
        this.isCommonAudited = in.readByte() != 0;
        this.mAvailableRooms = in.readInt();
        this.mRoomListedAudited = in.readString();
        this.mDayofYear = in.readInt();
    }

    public static final Creator<CompleteAudit> CREATOR = new Creator<CompleteAudit>() {
        @Override
        public CompleteAudit createFromParcel(Parcel source) {
            return new CompleteAudit(source);
        }

        @Override
        public CompleteAudit[] newArray(int size) {
            return new CompleteAudit[size];
        }
    };
}
