package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
@Deprecated
public class Subsection implements Parcelable{

    private String subsection;
    private List<Query> queries;

    public Subsection(){
        queries=new ArrayList<>();
    }

    public Subsection(Parcel in){
        this();
        subsection=in.readString();
        in.readTypedList(queries, Query.CREATOR);
    }

    /**
     *
     * @return
     * The subsection
     */
    public String getSubsection() {
        return subsection;
    }

    /**
     *
     * @param subsection
     * The subsection
     */
    public void setSubsection(String subsection) {
        this.subsection = subsection;
    }

    /**
     *
     * @return
     * The queries
     */
    public List<Query> getQueries() {
        return queries;
    }

    /**
     *
     * @param queries
     * The queries
     */
    public void setQueries(List<Query> queries) {
        this.queries = queries;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subsection);
        dest.writeTypedList(queries);
    }

    public static final Creator<Subsection> CREATOR = new Creator<Subsection>() {
        @Override
        public Subsection createFromParcel(Parcel source) {
            return new Subsection(source);
        }

        @Override
        public Subsection[] newArray(int size) {
            return new Subsection[size];
        }
    };

}