package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.treebo.prowlapp.dbflow.ProwlDatabase;
import com.treebo.prowlapp.Utils.Constants;

import java.util.List;

/**
 * Created by sumandas on 18/01/2017.
 */
@Table(database = ProwlDatabase.class)
public class AuditCategory extends BaseModel implements Parcelable {

    @Column
    @PrimaryKey
    @SerializedName("audit_category_id")
    public int mAuditCategoryId;


    @Column
    @SerializedName("audit_task_id")
    public int mAuditId;

    @Column
    @SerializedName("name")
    public String mAuditName;

    @Column
    @SerializedName("constant_name")
    public String mConstantAuditName;

    @Column
    @SerializedName("audit_task_name")
    public String mAuditTaskConstantName;

    @Column
    @SerializedName("create_issues")
    public String isIssueCreatable;

    public AuditCategory() {
    }

    public static AuditCategory getAuditCategoryById(int id) {
        return SQLite.select()
                .from(AuditCategory.class)
                .where(AuditCategory_Table.mAuditCategoryId.eq(id))
                .querySingle();
    }


    public static List<AuditCategory> getSubAudits(int auditId) {
        return SQLite.select()
                .from(AuditCategory.class)
                .where(AuditCategory_Table.mAuditId.eq(auditId)).queryList();
    }

    public static List<AuditCategory> getIssueCreatableAuditList() {
        return SQLite.select()
                .from(AuditCategory.class)
                .where(AuditCategory_Table.isIssueCreatable.eq("true"))
                .queryList();
    }

    public static int getAuditCategoryIDFromConstant(String constant) {
        AuditCategory fraudCategory =
                SQLite.select().from(AuditCategory.class).where(AuditCategory_Table.mConstantAuditName.eq(constant)).querySingle();
        return fraudCategory != null ? fraudCategory.mAuditCategoryId : 0;
    }

    public static String getAuditCategoryConstantByID(int auditID) {
        AuditCategory fraudCategory =
                SQLite.select().from(AuditCategory.class).where(AuditCategory_Table.mAuditCategoryId.eq(auditID)).querySingle();
        return fraudCategory != null ? fraudCategory.mConstantAuditName : "";
    }

    public static boolean isPeriodicAudit(int auditID) {
        String auditConstant = getAuditCategoryConstantByID(auditID);
        return !(Constants.COMMON_AREA_AUDIT_STRING.equals(auditConstant)
                || Constants.ROOM_AUDIT_STRING.equals(auditConstant));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mAuditCategoryId);
        dest.writeInt(this.mAuditId);
        dest.writeString(this.mAuditName);
        dest.writeString(this.mConstantAuditName);
        dest.writeString(this.mAuditTaskConstantName);
        dest.writeString(this.isIssueCreatable);
    }

    protected AuditCategory(Parcel in) {
        this.mAuditCategoryId = in.readInt();
        this.mAuditId = in.readInt();
        this.mAuditName = in.readString();
        this.mConstantAuditName = in.readString();
        this.mAuditTaskConstantName = in.readString();
        isIssueCreatable = in.readString();
    }

    public static final Creator<AuditCategory> CREATOR = new Creator<AuditCategory>() {
        @Override
        public AuditCategory createFromParcel(Parcel source) {
            return new AuditCategory(source);
        }

        @Override
        public AuditCategory[] newArray(int size) {
            return new AuditCategory[size];
        }
    };
}
