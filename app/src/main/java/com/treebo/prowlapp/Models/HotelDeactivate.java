package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sumandas on 07/12/2016.
 */

public class HotelDeactivate implements Parcelable {

    @SerializedName("hotel_id")
    public int mHotelId;

    @SerializedName("status")
    public String mStatus;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mHotelId);
        dest.writeString(this.mStatus);
    }

    public HotelDeactivate() {
    }

    protected HotelDeactivate(Parcel in) {
        this.mHotelId = in.readInt();
        this.mStatus = in.readString();
    }

    public static final Creator<HotelDeactivate> CREATOR = new Creator<HotelDeactivate>() {
        @Override
        public HotelDeactivate createFromParcel(Parcel source) {
            return new HotelDeactivate(source);
        }

        @Override
        public HotelDeactivate[] newArray(int size) {
            return new HotelDeactivate[size];
        }
    };
}
