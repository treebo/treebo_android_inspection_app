package com.treebo.prowlapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


/**
 * Created by sumandas on 29/04/2016.
 */
@Deprecated
public class Room  implements Parcelable{

    @SerializedName("color")
    public String color;

    @SerializedName("number")
    public String number;

    public Room(){

    }

    public Room(Parcel in){
        color=in.readString();
        number=in.readString();
    }

    @Override
    public int hashCode(){
        int result = 37;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StaffModel)) return false;
        if(o==null){
            return false;
        }
        if(!color.equals(((Room)o).color))
            return false;
        return number == ((Room) o).number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(color);
        dest.writeString(number);

    }

    public static final Creator<Room> CREATOR = new Creator<Room>() {
        @Override
        public Room createFromParcel(Parcel source) {
            return new Room(source);
        }

        @Override
        public Room[] newArray(int size) {
            return new Room[size];
        }
    };

}
