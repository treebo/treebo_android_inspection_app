package com.treebo.prowlapp.Models;

import java.util.List;

/**
 * Created by Kuliza152 on 10/8/2015.
 */
@Deprecated
public class AnswerListWithSectionName {
    private String sectionName;
    private List<Answer> listAnswer;

    public AnswerListWithSectionName(String sectionName, List<Answer> listAnswer) {
        this.sectionName = sectionName;
        this.listAnswer = listAnswer;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public List<Answer> getListAnswer() {
        return listAnswer;
    }

    public void setListAnswer(List<Answer> listAnswer) {
        this.listAnswer = listAnswer;
    }
}
