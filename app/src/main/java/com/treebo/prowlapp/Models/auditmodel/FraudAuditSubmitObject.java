package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abhisheknair on 20/01/17.
 */

public class FraudAuditSubmitObject implements Parcelable {

    @SerializedName("fraud_type")
    private String fraud_type;

    @SerializedName("room")
    private String room;

    @SerializedName("l2_id")
    private String l2_id;

    @SerializedName("comment")
    private String comment;

    @SerializedName("image_url")
    private String image_url;

    @SerializedName("is_fraud")
    private String is_fraud;

    @SerializedName("treebo_pms_price")
    private Integer treebo_pms_price;

    @SerializedName("hotel_pms_price")
    private Integer hotel_pms_price;

    public FraudAuditSubmitObject(String fraudType, String roomNumber, String comment, String imageUrl,
                                  String isFraud) {
        this.fraud_type = fraudType;
        this.room = roomNumber;
        this.comment = comment;
        this.image_url = imageUrl;
        this.is_fraud = isFraud;
    }

    public FraudAuditSubmitObject(String fraudType, int level2ID, String comment, String imageUrl) {
        this.fraud_type = fraudType;
        this.l2_id = String.valueOf(level2ID);
        this.comment = comment;
        this.image_url = imageUrl;
    }

    public FraudAuditSubmitObject(String fraudType, String roomNumber,
                                  String isFraud, Integer treeboPrice, Integer hotelPrice) {
        this.fraud_type = fraudType;
        this.room = roomNumber;
        this.is_fraud = isFraud;
        this.treebo_pms_price = treeboPrice;
        this.hotel_pms_price = hotelPrice;
    }

    protected FraudAuditSubmitObject(Parcel in) {
        fraud_type = in.readString();
        room = in.readString();
        comment = in.readString();
        image_url = in.readString();
        is_fraud = in.readString();
        l2_id = in.readString();
        hotel_pms_price = (Integer) in.readSerializable();
        treebo_pms_price = (Integer) in.readSerializable();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fraud_type);
        dest.writeString(room);
        dest.writeString(comment);
        dest.writeString(image_url);
        dest.writeString(is_fraud);
        dest.writeString(l2_id);
        dest.writeSerializable(hotel_pms_price);
        dest.writeSerializable(treebo_pms_price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FraudAuditSubmitObject> CREATOR = new Creator<FraudAuditSubmitObject>() {
        @Override
        public FraudAuditSubmitObject createFromParcel(Parcel in) {
            return new FraudAuditSubmitObject(in);
        }

        @Override
        public FraudAuditSubmitObject[] newArray(int size) {
            return new FraudAuditSubmitObject[size];
        }
    };

    public String getIsFraud() {
        return is_fraud;
    }

    public void setIsFraud(String isFraud) {
        this.is_fraud = isFraud;
    }

    public String getFraud_type() {
        return fraud_type;
    }

    public void setFraud_type(String fraud_type) {
        this.fraud_type = fraud_type;
    }

    public String getLevel2ID() {
        return l2_id;
    }

    public void setLevel2ID(String level2ID) {
        this.l2_id = level2ID;
    }

    public String getRoomNumber() {
        return room;
    }

    public void setRoomNumber(String roomNumber) {
        this.room = roomNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImageUrl() {
        return image_url;
    }

    public void setImageUrl(String imageUrl) {
        this.image_url = imageUrl;
    }
}
