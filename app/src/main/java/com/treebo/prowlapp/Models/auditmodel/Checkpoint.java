
package com.treebo.prowlapp.Models.auditmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.treebo.prowlapp.dbflow.ProwlDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumandas on 03/11/2016.
 */

@Table(database = ProwlDatabase.class)
public class Checkpoint extends BaseModel implements Parcelable {

    @Column
    @PrimaryKey
    @SerializedName("id")
    public int mCheckpointId;


    @Column
    @SerializedName("description")
    public String mDescription;

    @Column
    @ForeignKey(tableClass = Category.class)
    public int mCategoryId;

    @SerializedName("array")
    public ArrayList<SubCheckpoint> mSubCheckpoints = new ArrayList<>();

    private boolean isIncluded;

    private boolean isIssue;

    private String issueText;

    private boolean isPendingIssue;

    public Checkpoint() {

    }

    protected Checkpoint(Parcel in) {
        mCheckpointId = in.readInt();
        mDescription = in.readString();
        mCategoryId = in.readInt();
        in.readTypedList(mSubCheckpoints, SubCheckpoint.CREATOR);
        isIssue = in.readByte() != 0;
        issueText = in.readString();
        isIncluded = in.readByte() != 0;
        isPendingIssue = in.readByte() != 0;
    }

    public static final Creator<Checkpoint> CREATOR = new Creator<Checkpoint>() {
        @Override
        public Checkpoint createFromParcel(Parcel in) {
            return new Checkpoint(in);
        }

        @Override
        public Checkpoint[] newArray(int size) {
            return new Checkpoint[size];
        }
    };

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "mSubCheckpoints")
    public List<SubCheckpoint> getSubCheckpoints() {
        if (mSubCheckpoints == null || mSubCheckpoints.isEmpty()) {
            mSubCheckpoints = new ArrayList<>(SQLite.select()
                    .from(SubCheckpoint.class)
                    .where(SubCheckpoint_Table.mCheckpointId_mCheckpointId.eq(mCategoryId))
                    .queryList());
        }
        return mSubCheckpoints;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mCheckpointId);
        parcel.writeString(mDescription);
        parcel.writeInt(mCategoryId);
        parcel.writeTypedList(mSubCheckpoints);
        parcel.writeByte((byte) (isIssue ? 1 : 0));
        parcel.writeString(issueText);
        parcel.writeByte((byte) (isIncluded ? 1 : 0));
        parcel.writeByte((byte) (isPendingIssue ? 1 : 0));
    }

    public int getmCheckpointId() {
        return mCheckpointId;
    }

    public void setmCheckpointId(int mCheckpointId) {
        this.mCheckpointId = mCheckpointId;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getmCategoryId() {
        return mCategoryId;
    }

    public void setmCategoryId(int mCategoryId) {
        this.mCategoryId = mCategoryId;
    }

    public boolean isIncluded() {
        return isIncluded;
    }

    public void setIncluded(boolean included) {
        isIncluded = included;
    }

    public ArrayList<SubCheckpoint> getmSubCheckpoints() {
        return mSubCheckpoints;
    }

    public void setmSubCheckpoints(ArrayList<SubCheckpoint> mSubCheckpoints) {
        this.mSubCheckpoints = mSubCheckpoints;
    }

    public boolean issue() {
        return isIssue;
    }

    public void setIssue(boolean issue) {
        isIssue = issue;
    }

    public String getIssueText() {
        return issueText;
    }

    public void setIssueText(String issueText) {
        this.issueText = issueText;
    }

    public boolean isPendingIssue() {
        return isPendingIssue;
    }

    public void setPendingIssue(boolean pendingIssue) {
        isPendingIssue = pendingIssue;
    }
}
