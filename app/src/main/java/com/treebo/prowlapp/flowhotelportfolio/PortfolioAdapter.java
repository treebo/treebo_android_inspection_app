package com.treebo.prowlapp.flowhotelportfolio;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.PortfolioResponse;
import com.treebo.prowlapp.Utils.Utils;

import java.util.List;

/**
 * Created by sakshamdhawan on 05/05/16.
 */
@Deprecated
public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioAdapter.PortfolioViewHolder> {
    List<PortfolioResponse.PortfolioResponseData> mPortfolioResponseData;
    Context mContext;
    int mGuestReviewsPosition;

    public PortfolioAdapter(List<PortfolioResponse.PortfolioResponseData> portfolioResponseData , int guestReviewsPosition ) {
        this.mPortfolioResponseData =portfolioResponseData;
        mGuestReviewsPosition = guestReviewsPosition;
    }

    @Override
    public PortfolioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewGroup rootView;
        mContext = parent.getContext();
        switch (viewType) {
            case 0:
                rootView = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.scores_row, parent, false);
                return new PortfolioViewHolder(rootView);
            case 1:
                rootView = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.scores_row_with_star, parent, false);
                return new StarredPortfolioViewHolder(rootView);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(PortfolioViewHolder holder, int position) {
        holder.categoryNameTextView.setText(mPortfolioResponseData.get(position).department);

        if(holder instanceof StarredPortfolioViewHolder){

        }

        if (position == mGuestReviewsPosition) {
            Double value1 = mPortfolioResponseData.get(position).score;
            int colorScheme = Utils.getBackgroundColorForGuestReviews(value1);
            GradientDrawable bgShape = (GradientDrawable)((StarredPortfolioViewHolder)holder).coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));

            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                ((StarredPortfolioViewHolder)holder).starredImageView.setVisibility(View.GONE);
                return;
            }else{
                ((StarredPortfolioViewHolder)holder).starredImageView.setVisibility(View.VISIBLE);
            }
            holder.categoryValueTextView.setText(value1 + "");
            return;
        }
        if (mPortfolioResponseData.get(position).department.equalsIgnoreCase("QAM Audit")) {
            Double value1 = mPortfolioResponseData.get(position).score;
            int colorScheme = Utils.getBackgroundColorForQAMAudit(value1);
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));
            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                return;
            }
            if (Utils.checkIfFloatHasDecimal(value1)) {
                holder.categoryValueTextView.setText(value1 + "%");
            } else {
                holder.categoryValueTextView.setText(value1.intValue() + "%");
            }
            return;
        }
        if (mPortfolioResponseData.get(position).department.contains("FOT")) {
            Double value1 = mPortfolioResponseData.get(position).score;
            int colorScheme = Utils.getBackgroundColorForFOTAudit(value1.intValue());
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));
            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                return;
            }
            if (Utils.checkIfFloatHasDecimal(value1)) {
                holder.categoryValueTextView.setText(value1 + "%");
            } else {
                holder.categoryValueTextView.setText(value1.intValue() + "%");
            }
        }

        if (mPortfolioResponseData.get(position).department.contains("Guest Feedback")) {
            Double value1 = mPortfolioResponseData.get(position).score;
            int colorScheme = Utils.getBackgroundColorForGuestFeedback(value1.intValue());
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));
            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                return;
            }
            holder.categoryValueTextView.setText(value1 + "");
            holder.coloredContainer.setBackgroundResource(Utils.getBackgroundDrawableForGuestFeedback(value1));
            holder.categoryValueTextView.setText(value1.intValue() + "");
            return;

        }if (mPortfolioResponseData.get(position).department.contains("Net Promoter Score")) {
            Double value1 = mPortfolioResponseData.get(position).score;
            int colorScheme = Utils.getBackgroundColorForGuestFeedback(value1.intValue());
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));
            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                return;
            }
            holder.categoryValueTextView.setText(value1 + "");
            holder.coloredContainer.setBackgroundResource(Utils.getBackgroundDrawableForGuestFeedback( value1));
            holder.categoryValueTextView.setText(value1.intValue() + "");
            return;

        }
        else {
            Double value = mPortfolioResponseData.get(position).score;
            holder.coloredContainer.setBackgroundResource(Utils.getBackgroundDrawableForFOTAudit(value));
            holder.categoryValueTextView.setText(value + "");

        }




    }

    @Override
    public int getItemCount() {
        if (mPortfolioResponseData == null) {
            return 0;
        }
        return mPortfolioResponseData.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mGuestReviewsPosition) {
            return 1;
        } else
            return 0;
    }


    public static class StarredPortfolioViewHolder extends PortfolioViewHolder {
        public ImageView starredImageView;
        StarredPortfolioViewHolder(View itemView){
            super(itemView);
            starredImageView = (ImageView) itemView.findViewById(R.id.image_view_star);
        }
    }




    public static class PortfolioViewHolder extends RecyclerView.ViewHolder {

        TextView categoryNameTextView;
        TextView categoryValueTextView;
        RelativeLayout coloredContainer;

        public PortfolioViewHolder(View itemView) {
            super(itemView);
            categoryNameTextView = (TextView) itemView.findViewById(R.id.category_name);
            categoryValueTextView = (TextView) itemView.findViewById(R.id.category_value);
            coloredContainer = (RelativeLayout) itemView.findViewById(R.id.value_star_container);
        }
    }




}
