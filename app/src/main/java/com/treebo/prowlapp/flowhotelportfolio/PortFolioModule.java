package com.treebo.prowlapp.flowhotelportfolio;

import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 07/08/2016.
 */
@Deprecated
@Module
public class PortFolioModule {

    @Provides
    PortfolioPresenter providesPortFolioPresenter(){
        return new PortfolioPresenter();
    }

    @Provides
    RestClient providesRestClient(){
        return new RestClient();
    }

    @Provides
    PortfolioUseCase providesPortFolioUsecase(RestClient restClient){
        return new PortfolioUseCase(restClient);
    }

}
