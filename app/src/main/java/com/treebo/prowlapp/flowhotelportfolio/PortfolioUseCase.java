package com.treebo.prowlapp.flowhotelportfolio;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.PortfolioResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by devesh on 05/05/16.
 */
@Deprecated
public class PortfolioUseCase extends UseCase<PortfolioResponse> {
    int mUserId;
    RestClient mRestClient;

    public PortfolioUseCase(RestClient restClient){
        mRestClient=restClient;
    }

    public void setUserId(int userId){
        mUserId=userId;
    }

    @Override
    protected Observable<PortfolioResponse> getObservable() {
        return mRestClient.getAllPortfolio(mUserId);
    }
}
