package com.treebo.prowlapp.flowhotelportfolio;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.errors.NavigationalErrorCode;
import com.treebo.prowlapp.exception.ForcedLogoutException;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.mvpviews.PortfolioView;
import com.treebo.prowlapp.response.PortfolioResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;

import javax.inject.Inject;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sakshamdhawan on 05/05/16.
 */
@Deprecated
public class PortfolioFragment extends BaseFragment implements PortfolioView {

    View rootView;

    @Inject
    ProgressDialog progressDialog;

    @Inject
    PortfolioPresenter mPortfolioPresenter;

    int guestReviewsPosition = -1;
    PortfolioAdapter mPortfolioAdapter;

    @Inject
    LoginSharedPrefManager mLoginManager;

    @Inject
    RxBus mRxBus;

    @Inject
    MixpanelAPI mixpanelAPI;


    @BindView(R.id.portfolio_recycler_view)
    RecyclerView mPortfolioRecycleView;

    @BindView(R.id.retry_view)
    RelativeLayout retryView;

    @Inject
    PortfolioUseCase mPortfolioUseCase;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PortfolioComponent portfolioComponent=DaggerPortfolioComponent.builder()
                .activityModule(new ActivityModule(getContext()))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        portfolioComponent.injectFragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_portfolio, container, false);
        ButterKnife.bind(this, rootView);
        if (mLoginManager.isUserLoggedIn()) {
            getPortfolio();
        } else {
            mRxBus.postEvent(new ForcedLogoutException(NavigationalErrorCode.FORCED_LOGOUT.getMessage()));
        }
        MixPanelManager.trackEvent(mixpanelAPI,MixPanelManager.SCREEN_PORTFOLIO_SCORES,MixPanelManager.EVENT_PORTFOLIO_VIEW_LOAD);
        return rootView;
    }

    public void showSnackbarAndExit(String msg){
        SnackbarUtils.show(rootView, msg);
        new Handler().postDelayed(() -> getActivity().finish(), 2000);
    }

    private void getPortfolio() {
        mPortfolioUseCase.setUserId(mLoginManager.getUserId());
        PortfolioResponseObserver portfolioResponseObserver=providesPortFolioResponseObserver(
                mPortfolioPresenter,mPortfolioUseCase);
        mPortfolioPresenter.getAllPortfolio(mPortfolioUseCase,portfolioResponseObserver);
    }

    @Inject
    public void initView() {
       mPortfolioPresenter.setMvpView(this);
    }

    public static PortfolioFragment newInstance( ) {
        PortfolioFragment fragment = new PortfolioFragment();
        return fragment;
    }

    @Override
    public void onPortfolioSuccess(PortfolioResponse response) {
        findGuestReviewsPosition(response);
        if(response.data == null || response.data.size() == 0 ){
            showSnackbarAndExit(Constants.NO_RESULTS_FOUND);
            return;

        }
        mPortfolioRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mPortfolioAdapter = new PortfolioAdapter(response.data,guestReviewsPosition);
        mPortfolioRecycleView.setAdapter(mPortfolioAdapter);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(mPortfolioUseCase != null){
            mPortfolioUseCase.unsubscribe();
        }
    }

    @Override
    public void onPortfolioFailed(String msg) {
        SnackbarUtils.show(rootView, msg);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void showRetry() {
        retryView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        retryView.setVisibility(View.GONE);
    }

    @Override
    public boolean showError(String errorMessage) {
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(getActivity(), ((dialog, which) -> {
                    dialog.dismiss();
                    onRetryButtonClicked();
                }),
                (dialog1 -> {
                    dialog1.dismiss();
                }));
    }


    private void findGuestReviewsPosition(PortfolioResponse portfolioResponse) {
        for ( PortfolioResponse.PortfolioResponseData portfolioResponseData: portfolioResponse.data) {
            if (portfolioResponseData.department.equalsIgnoreCase("Guest Reviews")) {
                guestReviewsPosition = portfolioResponse.data.indexOf(portfolioResponseData);
            }
        }
    }

    @BindView(R.id.retry_button)
    Button retryButton;

    @OnClick(R.id.retry_button)
    void onRetryButtonClicked(){
        getPortfolio();
    }


    @OnClick(R.id.image_view_back)
    void click(){
        getActivity().finish();
    }



    PortfolioResponseObserver providesPortFolioResponseObserver(PortfolioPresenter portfolioPresenter,
                                                                PortfolioUseCase portfolioUseCase){
        return new PortfolioResponseObserver(portfolioPresenter,portfolioUseCase,this);
    }

}
