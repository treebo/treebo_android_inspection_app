package com.treebo.prowlapp.flowhotelportfolio;

import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.PortfolioView;
import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by devesh on 05/05/16.
 */
@Deprecated
public class PortfolioPresenter implements BasePresenter {

    PortfolioView mBaseView;

    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView = (PortfolioView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }
    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }


    @Override
    public BaseView getView() {
        return mBaseView;
    }

    public void getAllPortfolio(PortfolioUseCase portfolioUseCase,
                                PortfolioResponseObserver portfolioResponseObserver) {
        hideRetry();
        showLoading();
        portfolioUseCase.execute(portfolioResponseObserver);
    }
}
