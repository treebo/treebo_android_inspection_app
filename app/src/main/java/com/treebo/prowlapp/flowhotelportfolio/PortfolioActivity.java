package com.treebo.prowlapp.flowhotelportfolio;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.widget.FrameLayout;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.fragments.BaseFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

@Deprecated
public class PortfolioActivity extends AppCompatActivity {

    FrameLayout mContainerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portfolio);
        if(savedInstanceState == null) {
            addFragment(PortfolioFragment.newInstance());
        }
    }

    public void addFragment(BaseFragment fragment) {
        mContainerLayout = (FrameLayout) findViewById(R.id.container);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, PortfolioActivity.class);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

}
