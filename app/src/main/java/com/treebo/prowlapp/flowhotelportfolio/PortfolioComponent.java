package com.treebo.prowlapp.flowhotelportfolio;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerFragment;

import dagger.Component;

/**
 * Created by sumandas on 07/08/2016.
 */
@Deprecated
@PerFragment
@Component(dependencies = ApplicationComponent.class,
        modules = {PortFolioModule.class,ActivityModule.class}
)
public interface PortfolioComponent {
    void injectFragment(PortfolioFragment portfolioFragment);
}
