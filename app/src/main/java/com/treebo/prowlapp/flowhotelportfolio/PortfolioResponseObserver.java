package com.treebo.prowlapp.flowhotelportfolio;

/**
 * Created by sumandas on 16/08/2016.
 */

import com.treebo.prowlapp.mvpviews.PortfolioView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.PortfolioResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;
@Deprecated
public class PortfolioResponseObserver extends BaseObserver<PortfolioResponse> {

    PortfolioUseCase mPortfolioUseCase;
    PortfolioView mPortfolioView;

    public PortfolioResponseObserver(BasePresenter presenter, PortfolioUseCase portfolioUseCase, PortfolioView portfolioView) {
        super(presenter, portfolioUseCase);
        mPortfolioUseCase=portfolioUseCase;
        mPortfolioView=portfolioView;
    }

    @Override
    public void onCompleted() {
        mPortfolioView.hideLoading();

    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
    }

    @Override
    public void onNext(PortfolioResponse response) {
        if (response.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)) {
            mPortfolioView.onPortfolioSuccess(response);
        } else {
            mPortfolioView.hideLoading();
            mPortfolioView.onPortfolioFailed(response.msg);
        }
    }
}