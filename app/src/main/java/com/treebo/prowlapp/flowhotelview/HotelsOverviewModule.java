package com.treebo.prowlapp.flowhotelview;

import com.treebo.prowlapp.flowhotelview.presenter.HotelsListPresenter;
import com.treebo.prowlapp.flowhotelview.usecase.GetAllHotelsUseCase;
import com.treebo.prowlapp.flowhotelview.usecase.LogoutUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.presenter.AppUpdatePresenter;
import com.treebo.prowlapp.presenterImpl.AppUpdatePresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 08/08/2016.
 */
@Module
public class HotelsOverviewModule {

    @Provides
    HotelsListPresenter providesHotelPresenter(){
        return new HotelsListPresenter();
    }

    @Provides
    AppUpdatePresenter providesAppUpdatePresenter(){
        return new AppUpdatePresenterImpl();
    }

    @Provides
    RestClient providesRestClient(){
        return new RestClient();
    }


    @Provides
    GetAllHotelsUseCase providesGetAllHotelsUseCase(RestClient restClient){
        return new GetAllHotelsUseCase(restClient);
    }

    @Provides
    LogoutUseCase providesLogoutUseCase(RestClient restClient){
        return new LogoutUseCase(restClient);
    }
}
