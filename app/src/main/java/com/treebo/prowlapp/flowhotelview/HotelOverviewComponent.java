package com.treebo.prowlapp.flowhotelview;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;

import dagger.Component;

/**
 * Created by sumandas on 08/08/2016.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,modules
        = {ActivityModule.class,HotelsOverviewModule.class})
public interface HotelOverviewComponent {

    void injectListFragment(HotelsListFragment hotelsListFragment);
    void injectBottomSheetFragment(HotelsBottomSheetFragment bottomSheetFragment);
    void injectActivity(HotelsOverViewActivity hotelsOverViewActivity);

}
