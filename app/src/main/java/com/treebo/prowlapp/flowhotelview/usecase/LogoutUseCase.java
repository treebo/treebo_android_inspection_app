package com.treebo.prowlapp.flowhotelview.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.login.LogoutResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by sumandas on 21/08/2016.
 */
public class LogoutUseCase extends UseCase<LogoutResponse> {

    public RestClient mRestClient;

    public void setmAccessToken(String mAccessToken) {
        this.mAccessToken = mAccessToken;
    }

    private String mAccessToken;

    public LogoutUseCase(RestClient restClient) {
        mRestClient=restClient;
    }



    @Override
    protected Observable<LogoutResponse> getObservable() {
        return mRestClient.logout(mAccessToken);
    }
}
