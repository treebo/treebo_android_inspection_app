package com.treebo.prowlapp.flowhotelview;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;


import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.errors.NavigationalErrorCode;
import com.treebo.prowlapp.events.AuditIssueSubmitEvent;
import com.treebo.prowlapp.exception.ForcedLogoutException;
import com.treebo.prowlapp.flowhotelview.observer.HotelsListSubscriber;
import com.treebo.prowlapp.flowhotelview.observer.LogoutObserver;
import com.treebo.prowlapp.flowhotelview.presenter.HotelsListPresenter;
import com.treebo.prowlapp.flowhotelview.usecase.GetAllHotelsUseCase;
import com.treebo.prowlapp.flowhotelview.usecase.LogoutUseCase;
import com.treebo.prowlapp.flowmain.MainActivity;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.mvpviews.HotelsListMVPView;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.HotelsListResponse;
import com.treebo.prowlapp.response.login.LogoutResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;

/**
 * Created by devesh on 15/03/16.
 */
@Deprecated
public class HotelsListFragment extends BaseFragment implements HotelsListAdapter.OnHotelClicked, HotelsListMVPView, HotelsBottomSheetFragment.OnBottomSheetError {

    @BindView(R.id.portfolio_audit_score)
    TextView mTextAuditScore;
    @BindView(R.id.portfolio_no_hotels)
    TextView mTextNoHotels;
    @BindView(R.id.portfolio_severe_issues)
    TextView mTextSevereIssues;
    @BindView(R.id.portfolio_text_view_rating)
    TextView mTextStarRating;
    @BindView(R.id.portfoli_star_rating_wrapper)
    RelativeLayout mStarRatingWrapper;
    @BindView(R.id.retry_view)
    RelativeLayout mRetryView;
    @BindView(R.id.version)
    TextView mVersionCode;

    @Inject
    ProgressDialog progressDialog;
    RecyclerView mHotelsRecyclerView;

    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout mSwipeRefreshLayout;

    ImageView imageViewExport;
    CoordinatorLayout mParentLayout;


    @Inject
    Navigator mNavigator;


    private ArrayList<HotelDataResponse> mHotelDataList;
    HotelDataResponse mSelectedHotel;

    @Inject
    public HotelsListPresenter mHotelsListPresenter;

    View rootView;
    HotelsListAdapter mAdapter;

    @Inject
    public RxBus mAuditIssueSubmittedBus;

    @Inject
    public AuditPreferenceManager mAuditPreferenceManager;


    @Inject
    public LoginSharedPrefManager mLoginSharedPrefManager;

    @Inject
    public MixpanelAPI mixpanelAPI;

    public OnSubmittedSubscriber onSubmittedSubscriber;
    public boolean isAuditOrIssueSubmitted;

    public boolean isInForeground;

    @Inject
    public GetAllHotelsUseCase mGetAllHotelsUseCase;

    @Inject
    public LogoutUseCase mLogoutUseCase;



    @Override
    public void onHotelClicked(HotelDataResponse hotelData) {
        if(!getActivity().isFinishing() || !getActivity().isChangingConfigurations()){
            HotelsListFragment.this.mSelectedHotel = hotelData;
            HotelsBottomSheetFragment hotelsBottomSheetFragment = HotelsBottomSheetFragment.newInstance(mSelectedHotel);

            hotelsBottomSheetFragment.show(getActivity().getSupportFragmentManager(), null);
            hotelsBottomSheetFragment.setBottomSheetErrorLister(this);
        }

    }

    public void setPortfolioRow(String auditScore, String severeIssues, int no_of_hotels, String star_rating) {
        String hotels = "";
        if (no_of_hotels == 0)
            hotels = "No hotels";
        else if (no_of_hotels == 1)
            hotels = "1 Hotel";
        else
            hotels = no_of_hotels + " Hotels";
        mTextNoHotels.setText(hotels);
        try {
            int noSevereIssues=Integer.parseInt(severeIssues);
            if (noSevereIssues == 0) {
                mTextSevereIssues.setTextColor(getResources().getColor(R.color.emerald));
            } else {
                mTextSevereIssues.setTextColor(getResources().getColor(R.color.coral));
            }
            mTextSevereIssues.setText(severeIssues);
        } catch (Exception e) {
            e.printStackTrace();
        }


        NumberFormat formatter = new DecimalFormat("#0.0");
        Double value = Double.parseDouble(star_rating);
        mTextStarRating.setText("" + formatter.format(value));
        GradientDrawable bgShape = (GradientDrawable) mStarRatingWrapper.getBackground();
        bgShape.setColor(ContextCompat.getColor(getContext(), Utils.getStarRatingValue(value)));

        int percentage = (int) Double.parseDouble(auditScore);
        mTextAuditScore.setText("" + percentage + "%");
        mTextAuditScore.setTextColor(ContextCompat.getColor(getActivity(), Utils.getQAMOverallColor(percentage)));
    }

    @Override
    public void onHotelsListSuccess(HotelsListResponse response) {
        mHotelDataList = response.data.hotels;

        for(HotelDataResponse hotelData:mHotelDataList){
            if(hotelData.getHotelName().equalsIgnoreCase(Constants.PORTFOLIO)){
                mHotelDataList.remove(hotelData);
                setPortfolioRow(hotelData.hotelQamAuditPercentage,hotelData.hotelOpenIssues,hotelData.noOfHotels, String.valueOf(hotelData.hotelOnlineRating));
            }
        }
        mAdapter = new HotelsListAdapter(getContext(), mHotelDataList);
        mAdapter.setOnHotelClickedListener(this);
        mHotelsRecyclerView.setAdapter(mAdapter);
        mSwipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onHotelsListFailed(String msg) {
        SnackbarUtils.show(rootView, msg);

    }

    @Override
    public void onLogoutSuccess(LogoutResponse response) {
        MixPanelManager.trackEvent(mixpanelAPI,MixPanelManager.SCREEN_HOME,
                MixPanelManager.EVENT_LOGOUT_SUCCESS);
        mLoginSharedPrefManager.clearPreferences();
        Intent loginIntent = new Intent(getActivity(), MainActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    @Override
    public void onLogoutFailure(LogoutResponse response) {
        SnackbarUtils.show(rootView, response.msg);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void showRetry() {
        mRetryView.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideRetry() {
        mRetryView.setVisibility(View.GONE);
    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(rootView, errorMessage);
        Log.d("HotelsListFragment", errorMessage.toString());
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public void showEmptyView() {
        mRetryView.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideEmptyView() {
        mRetryView.setVisibility(View.GONE);

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(getActivity(), ((dialog, which) -> {
                    dialog.dismiss();
                    onRetryButtonClicked();
                }),
                (dialog1 -> {
                    dialog1.dismiss();
                }));

    }

    @Override
    public void onBottomSheetError(String message) {
        showError(message);
    }

    public static HotelsListFragment newInstance() {
        HotelsListFragment fragment = new HotelsListFragment();

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HotelOverviewComponent hotelOverViewComponent=DaggerHotelOverviewComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent()).
                        activityModule(new ActivityModule(getActivity())).build();
        hotelOverViewComponent.injectListFragment(this);
        onSubmittedSubscriber=new OnSubmittedSubscriber();
        mAuditIssueSubmittedBus.toObservable().subscribe(onSubmittedSubscriber);


    }

    @Inject
    public void initView() {
        mHotelsListPresenter.setMvpView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (progressDialog != null) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
                Log.e("HotelsListFragment", e.toString());
            }
            progressDialog = null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.hotel_list_layout, container, false);
        ButterKnife.bind(this, rootView);
        mHotelsRecyclerView = (RecyclerView) rootView.findViewById(R.id.hotels_list_recycler_view);
        mHotelsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mSwipeRefreshLayout.setOnRefreshListener(() -> getAllHotels());


        imageViewExport = (ImageView) rootView.findViewById(R.id.image_view_export);
        mParentLayout = (CoordinatorLayout) rootView.findViewById(R.id.main_content);
        imageViewExport.setOnClickListener(v -> {
            if (!Utils.isInternetConnected(getActivity())) {
                SnackbarUtils.show(mParentLayout, Constants.INTERNET_NOT_AVAILABLE);
                return;
            }
            AlertDialogUtils.showConfirmDialog(getActivity(), "Prowl", "Are you sure you want to logout?", (dialog, which) -> {
                MixPanelManager.trackEvent(mixpanelAPI,MixPanelManager.SCREEN_HOME,
                        MixPanelManager.EVENT_LOGOUT_ATTEMPT);
                mHotelsListPresenter.logout(mLoginSharedPrefManager.getAccessToken(),mLogoutUseCase,providesLogoutObserver());
            });
        });

        if (!mLoginSharedPrefManager.isUserLoggedIn()) {
            mAuditIssueSubmittedBus.postEvent(new ForcedLogoutException(NavigationalErrorCode.FORCED_LOGOUT.getMessage()));
        } else {
            getAllHotels();
        }
        bindViews();
        return rootView;

    }

    private void bindViews() {
        PackageManager manager = getActivity().getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        mVersionCode.setText("v"+info.versionName);
    }

    private void getAllHotels() {
        HotelsListSubscriber hotelsListSubscriber=providesHotelsListSubscriber();
        mGetAllHotelsUseCase.setmUserId(mLoginSharedPrefManager.getUserId());
        mHotelsListPresenter.getAllHotels(mGetAllHotelsUseCase,hotelsListSubscriber);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        isInForeground=true;
        ((BaseActivity) getActivity()).setActionBarTitle("HOTELS OVERVIEW");
        if (isAuditOrIssueSubmitted) {
            isAuditOrIssueSubmitted=false;
            getAllHotels();
        }
        if (mAuditPreferenceManager.isAuditRowUpdated()) {
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
                mAuditPreferenceManager.setAuditRowUpdated(false);
            }
        }

    }

    @Override
    public void onPause() {
        isInForeground=false;
        super.onPause();
    }


    public class OnSubmittedSubscriber extends Subscriber<Object> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(Object event) {
            if(event instanceof AuditIssueSubmitEvent){
                if(isInForeground){
                    getActivity().runOnUiThread(() -> getAllHotels());
                }else{
                    isAuditOrIssueSubmitted=true;
                }
            }

        }
    }

    @OnClick(R.id.portfolio_row_hotel_list)
    void Click() {
        Log.d("portfolio clicked", "");
        if (!Utils.isInternetConnected(getActivity())) {
            showError(Constants.INTERNET_NOT_AVAILABLE);
            return;
        }
        MixPanelManager.trackEvent(mixpanelAPI,MixPanelManager.SCREEN_HOME,
                MixPanelManager.EVENT_PORTFOLIO_VIEW_TAP);
        mNavigator.navigateToPortfolioActivity(getActivity());

    }

    @OnClick(R.id.retry_button)
    void onRetryButtonClicked() {
        getAllHotels();
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        onSubmittedSubscriber.unsubscribe();
        if(mGetAllHotelsUseCase != null){
            mGetAllHotelsUseCase.unsubscribe();
        }
    }


    public HotelsListSubscriber providesHotelsListSubscriber(){
        return new HotelsListSubscriber(mHotelsListPresenter, mGetAllHotelsUseCase,this);
    }

    public LogoutObserver providesLogoutObserver(){
        return new LogoutObserver(mHotelsListPresenter,mLogoutUseCase,this);
    }

}




