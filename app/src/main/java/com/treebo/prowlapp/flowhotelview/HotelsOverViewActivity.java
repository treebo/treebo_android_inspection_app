package com.treebo.prowlapp.flowhotelview;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.AppVersionModel;
import com.treebo.prowlapp.mvpviews.UpdateAppView;
import com.treebo.prowlapp.presenter.AppUpdatePresenter;
import com.treebo.prowlapp.pushMessaging.services.AppUpdateService;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AppUpdateHelper;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.PermissionHelper;

import java.util.ArrayList;

import javax.inject.Inject;


/**
 * Created by devesh on 15/03/16.
 */
@Deprecated
public class HotelsOverViewActivity extends BaseActivity implements AppUpdateHelper.GetLatestVersionInfo ,UpdateAppView{


    private static final String TAG = HotelsOverViewActivity.class.getSimpleName();
    FrameLayout mContainerLayout;

    @Inject
    public AppUpdatePresenter mUpdatePresenter;

    @Inject
    public ProgressDialog progressDialog;

    @Inject
    public MixpanelAPI mMixpanelApi;

    @Inject
    public LoginSharedPrefManager mLoginSharedPrefManager;

    public AppUpdateHelper appUpdateHelper;

    private AppUpdateHelper.UpdateCallFinished mUpdateCallback;

    public HotelOverviewComponent mHotelOverViewComponent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hotels_overview_layout);
        if (savedInstanceState == null) {
            addFragment(HotelsListFragment.newInstance());
        }

        mHotelOverViewComponent=DaggerHotelOverviewComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent()).
                        activityModule(new ActivityModule(this)).build();
        mHotelOverViewComponent.injectActivity(this);


        //if device id is registered then check if new app update is available or not
        if (mLoginSharedPrefManager.isDeviceRegistered()) {
            //start service to check for app update
            startService(new Intent(this, AppUpdateService.class));
        }

        if(Build.VERSION.SDK_INT >= 23){
            if(PermissionHelper.hasNoPermissionToExternalStorage(this)){
                ArrayList<String> permissons = new ArrayList<>();
                permissons.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                permissons.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                String[] items = permissons.toArray(new String[permissons.size()]);
                this.requestPermissions(items, 1);
                MixPanelManager.trackEvent(mMixpanelApi,MixPanelManager.SCREEN_HOME,
                        MixPanelManager.EVENT_REQUEST_STORAGE_PERMISSION);
            }
        }

        appUpdateHelper = new AppUpdateHelper(this,
                "prowl.apk","Updating Prowl","/download/",this
                ,AppUpdateHelper.FREQUENCY_ONE_DAY,mMixpanelApi);

        appUpdateHelper.checkAppUpdateAndInstall();
    }


    @Inject
    public void initView() {
        mUpdatePresenter.setMvpView(this);
    }

    public static Intent getCallingIntent(Activity activity){
        Intent intent = new Intent(activity , HotelsOverViewActivity.class );
        return intent;

    }

    @Override
    public void onAppUpdate(AppVersionModel appVersion) {
        final AppUpdateHelper.UpdateInfo updateInfo =new AppUpdateHelper.UpdateInfo();
        updateInfo.latestVersionCode = Long.parseLong(appVersion.version);
        updateInfo.updateUrl= appVersion.apkLink;
        Log.d("UPDATE CALL RESPONSE","latest version:"+ updateInfo.latestVersionCode );
        mUpdateCallback.onUpdateCallFinished(updateInfo);
    }

    @Override
    protected void onDestroy() {
        mMixpanelApi.flush();
        super.onDestroy();
        if(appUpdateHelper != null){
            appUpdateHelper.unregisterReceivers();
        }
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        if(!isFinishing()){
            progressDialog.show();
        }
    }

    @Override
    public void hideLoading() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return this;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void addFragment(BaseFragment fragment) {
        Log.v(TAG, "addFragment called ");
        mContainerLayout = (FrameLayout) findViewById(R.id.container);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStackImmediate();
        } else {
            this.finish();
        }
    }

    @Override
    public void makeLatestVersionInfoCall(AppUpdateHelper.UpdateCallFinished updateCallFinished) {
        mUpdateCallback=updateCallFinished;
        mUpdatePresenter.getAppUpdate();
    }
}