package com.treebo.prowlapp.flowhotelview;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;
import com.treebo.prowlapp.Utils.AuditUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.ProgressPercentView;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Years;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by devesh on 15/03/16.
 */
@Deprecated
public class HotelsListAdapter extends RecyclerView.Adapter<HotelsListAdapter.HotelsViewHolder> {

    List<HotelDataResponse> mHotelDataList;
    OnHotelClicked onHotelClicked;
    private Context mContext;
    int mNoofDays=-1;

    private static final String TAG = "HotelsListAdapter";


    public HotelsListAdapter( Context context,List<HotelDataResponse> hotelDataList) {
        mContext=context;
        mHotelDataList = hotelDataList;
    }


    public void setOnHotelClickedListener(OnHotelClicked onHotelClickedListener) {
        this.onHotelClicked = onHotelClickedListener;
    }

    @Override
    public HotelsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View singleCardLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_list_row, parent, false);
        return new HotelsViewHolder(singleCardLayout);

    }

    @Override
    public void onBindViewHolder(final HotelsViewHolder holder,final  int position) {
        holder.mPosition = position;

        Form savedLastAuditForm= AuditPreferenceManager.getInstance().getLastPausedAuditForHotel(Integer.toString(mHotelDataList.get(position).getHotelId()));
        if(savedLastAuditForm!=null){
            float percentage= AuditUtils.getPercentTaskRemainingInAudit(savedLastAuditForm);
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.progressBar.setProgressUpdate(percentage);
            holder.audit_status.setText((String.format(mContext.getResources().getString(R.string.paused_audit_task),
                    AuditUtils.getPercentageTaskRemainingInAudit(savedLastAuditForm))));
        }else{
            holder.progressBar.setVisibility(View.INVISIBLE);
            String auditText = getAuditText(mHotelDataList.get(position).getLastAuditDate(),mHotelDataList.get(position).getLastAuditTime() );
            holder.audit_status.setText(auditText);
            holder.audit_status.setTextColor(ContextCompat.getColor(mContext, Utils.getLastAuditTextColor(mNoofDays)));
        }

        holder.hotelName.setText(mHotelDataList.get(position).getHotelName());

        if(!TextUtils.isEmpty(mHotelDataList.get(position).getLocality()))
        holder.hotelArea.setText(mHotelDataList.get(position).getLocality()+", ");
        holder.hotelArea.setText(mHotelDataList.get(position).getHotelCity());

        int percentage = (int) Double.parseDouble (mHotelDataList.get(position).getHotelQamAuditPercentage());
        holder.auditDataValue.setText("" + percentage + "%");
        holder.auditDataValue.setTextColor(ContextCompat.getColor(mContext, Utils.getQAMOverallColor(percentage)));

        NumberFormat formatter = new DecimalFormat("#0.0");
        Double value = Double.valueOf(formatter.format(mHotelDataList.get(position).getHotelOnlineRating()));
        holder.ratingDataValue.setText("" + value);
        GradientDrawable bgShape = (GradientDrawable)holder.star_rating_wrapper.getBackground();
        bgShape.setColor(ContextCompat.getColor(mContext, Utils.getStarRatingValue(value)));
        if(Integer.parseInt(mHotelDataList.get(position).getHotelOpenIssues()) == 0){
            holder.severeIssues.setTextColor(mContext.getResources().getColor(R.color.emerald));
        }else{
            holder.severeIssues.setTextColor(mContext.getResources().getColor(R.color.coral));
        }
        holder.severeIssues.setText("" + mHotelDataList.get(position).getHotelOpenIssues());
        holder.itemView.setOnClickListener((View v)->onHotelClicked.onHotelClicked(mHotelDataList.get(position)));

    }

    private String getAuditText(String lastAuditDate, String time) {

        String result = "";
        if (lastAuditDate == null || lastAuditDate.equals(""))
            return result;

        Date date = null;
        try {
            date = new SimpleDateFormat(Constants.DATE_FORMAT).parse(lastAuditDate);
        } catch (ParseException e) {
            result = "Last Audit Done On " + lastAuditDate +" at "+time;
            Log.d("Couldn't parse date", " sent from server");
            return result;

        }
        if (date != null) {
            DateTime start = new DateTime(date);
            DateTime end = new DateTime();
            if (start.isAfter(end)) {
                result = "Last Audit Done On " + lastAuditDate+ " at "+time ;
                return result;
            }

            int days = Days.daysBetween(start.toLocalDate(), end.toLocalDate()).getDays();
            mNoofDays = days;
            int months = Months.monthsBetween(start.toLocalDate(), end.toLocalDate()).getMonths();
            int years = Years.yearsBetween(start.toLocalDate(), end.toLocalDate()).getYears();

            result = "Last Audit, ";
            if (years > 0)
                if (months > 0)
                    result = result + getPluralText("year", years) +
                            " " + getPluralText("month", months);
                else
                    result = result + getPluralText("year", years);

            else if (months > 0)
                if (days > 0)
                    result = result + getPluralText("month", months) +
                            " " + getPluralText("day", days);
                else
                    result = result + getPluralText("month", months);
            else if (days > 0) {
                result = result + getPluralText("day", days);

            } else if (days == 0) {
                if (lastAuditDate != null && !lastAuditDate.equals("")) {
                    result = "Last Audit, Today " + time;
                    return result;
                } else
                    return "Last Audit, Today ";
            }
            result = result + " ago";
            return result;

        }
        return "";
    }

    String getPluralText(String text, int n){
        if(n>1)
            return n + " "+ text + "s";
        else
            return n+ text;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
      return mHotelDataList.size();
    }


    public class HotelsViewHolder extends RecyclerView.ViewHolder {

        TextView hotelName;
        TextView hotelArea;
        TextView auditDataValue;
        TextView severeIssues;
        TextView ratingDataValue;

        ProgressPercentView progressBar;

        RelativeLayout star_rating_wrapper;
        TextView audit_status;

        View itemView;
        int mPosition = -1;

        public HotelsViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            hotelName = (TextView) itemView.findViewById(R.id.hotel_list_heading);
            hotelArea = (TextView) itemView.findViewById(R.id.hotel_list_location);
            auditDataValue = (TextView) itemView.findViewById(R.id.hotel_list_percentage);
            ratingDataValue = (TextView) itemView.findViewById(R.id.text_view_rating);

            progressBar=(ProgressPercentView) itemView.findViewById(R.id.hotel_percent_complete);
            severeIssues = (TextView) itemView.findViewById(R.id.hotel_list_no_of_issues);
            star_rating_wrapper = (RelativeLayout)itemView.findViewById(R.id.star_rating_wrapper);
            audit_status=(TextView) itemView.findViewById(R.id.hotel_list_audit_status);

        }
    }

    public interface OnHotelClicked {
        void onHotelClicked(HotelDataResponse hotelData);
    }





}
