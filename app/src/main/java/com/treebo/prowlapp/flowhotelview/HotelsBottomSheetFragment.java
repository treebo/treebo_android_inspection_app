package com.treebo.prowlapp.flowhotelview;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.Utils;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/**
 * Created by sakshamdhawan on 01/05/16.
 */
@Deprecated
public class HotelsBottomSheetFragment extends BottomSheetDialogFragment {

    public static String HOTEL_DATA_KEY = "hotel_data";
    private HotelDataResponse mSelectedHotel;
    public OnBottomSheetError bottomSheetErrorListener;

    @Inject
    MixpanelAPI mMixpanelAPI;

    @Inject
    Navigator mNavigator;

    public void setBottomSheetErrorLister(OnBottomSheetError onBottomSheetError) {
        bottomSheetErrorListener = onBottomSheetError;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };


    public static HotelsBottomSheetFragment newInstance(HotelDataResponse hotelData) {
        HotelsBottomSheetFragment fragment = new HotelsBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(HOTEL_DATA_KEY, hotelData);
        fragment.setArguments(bundle);
        HotelOverviewComponent hotelOverViewComponent=DaggerHotelOverviewComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent()).
                        activityModule(new ActivityModule(fragment.getContext())).build();
        hotelOverViewComponent.injectBottomSheetFragment(fragment);
        return fragment;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.bottom_sheet, null);
        dialog.setContentView(contentView);


        if (getArguments() != null) {
            mSelectedHotel = getArguments().getParcelable(HOTEL_DATA_KEY);
        }
        ((TextView) contentView.findViewById(R.id.bottom_sheet_heading_text)).setText(mSelectedHotel.hotelName);
        ImageView cross = (ImageView) contentView.findViewById(R.id.bottom_sheet_cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        RelativeLayout audits = (RelativeLayout) contentView.findViewById(R.id.audits_layout);
        audits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.isInternetConnected(getActivity())) {
                    if (bottomSheetErrorListener != null) {
                        dismiss();
                        bottomSheetErrorListener.onBottomSheetError(Constants.INTERNET_NOT_AVAILABLE);

                    }
                    return;
                }
                mNavigator.navigateToHotelAuditActivity(getActivity(), mSelectedHotel);
                MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ALL_HOTELS_BOTTOM_SHEET
                        ,MixPanelManager.EVENT_AUDITS_TAP);
                dismiss();

            }
        });


        RelativeLayout issues = (RelativeLayout) contentView.findViewById(R.id.issues_layout);
        issues.setOnClickListener(view -> {
            if (!Utils.isInternetConnected(getActivity())) {
                if (bottomSheetErrorListener != null) {
                    dismiss();
                    bottomSheetErrorListener.onBottomSheetError(Constants.INTERNET_NOT_AVAILABLE);
                }
                return;
            }
            MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ALL_HOTELS_BOTTOM_SHEET
                    ,MixPanelManager.EVENT_ISSUES_TAP);
            mNavigator.navigateToHotelIssuesActivity(getActivity(), mSelectedHotel.getHotelId());
            dismiss();

        });

        RelativeLayout scores = (RelativeLayout) contentView.findViewById(R.id.scores_layout);
        scores.setOnClickListener(view -> {
            if (!Utils.isInternetConnected(getActivity())) {
                if (bottomSheetErrorListener != null) {
                    dismiss();
                    bottomSheetErrorListener.onBottomSheetError(Constants.INTERNET_NOT_AVAILABLE);
                }
                return;
            }
            MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ALL_HOTELS_BOTTOM_SHEET
                    ,MixPanelManager.EVENT_SCORE_TAP);
            mNavigator.navigateToScoresActivity(getActivity(), mSelectedHotel);
            dismiss();
        });

        RelativeLayout staff = (RelativeLayout) contentView.findViewById(R.id.staff_layout);
        staff.setOnClickListener(view -> {
            if (!Utils.isInternetConnected(getActivity())) {
                if (bottomSheetErrorListener != null) {
                    dismiss();
                    bottomSheetErrorListener.onBottomSheetError(Constants.INTERNET_NOT_AVAILABLE);
                }
                return;

            }
            MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ALL_HOTELS_BOTTOM_SHEET
                    ,MixPanelManager.EVENT_STAFF_TAP);
            mNavigator.navigateToStaffActivity(getActivity(), mSelectedHotel);
            dismiss();


        });

        RelativeLayout incentives = (RelativeLayout) contentView.findViewById(R.id.incentives_layout);
        incentives.setOnClickListener(view -> {
            if (!Utils.isInternetConnected(getActivity())) {
                if (bottomSheetErrorListener != null) {
                    dismiss();
                    bottomSheetErrorListener.onBottomSheetError(Constants.INTERNET_NOT_AVAILABLE);
                }
                return;
            }
            MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ALL_HOTELS_BOTTOM_SHEET
                    ,MixPanelManager.EVENT_INCENTIVES_TAP);
            mNavigator.navigateToIncentivesActivity(getActivity(), mSelectedHotel);
            dismiss();

        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    public interface OnBottomSheetError {
        void onBottomSheetError(String message);
    }


}
