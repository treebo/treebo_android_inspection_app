package com.treebo.prowlapp.flowhotelview.observer;

import com.treebo.prowlapp.flowhotelview.presenter.HotelsListPresenter;
import com.treebo.prowlapp.flowhotelview.usecase.LogoutUseCase;
import com.treebo.prowlapp.mvpviews.HotelsListMVPView;
import com.treebo.prowlapp.response.login.LogoutResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by sumandas on 21/08/2016.
 */
public class LogoutObserver extends BaseObserver<LogoutResponse> {

    HotelsListPresenter mHotelsListPresenter;
    HotelsListMVPView mView;
    LogoutUseCase mLogoutUseCase;


    public LogoutObserver(HotelsListPresenter presenter, LogoutUseCase useCase, HotelsListMVPView view){
        super(presenter, useCase);
        mHotelsListPresenter=presenter;
        mLogoutUseCase=useCase;
        mView=view;
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(LogoutResponse response) {
        if(response.status.equals(Constants.STATUS_SUCCESS)){
            mView.onLogoutSuccess(response);
        }else {
            mView.onLogoutFailure(response);
        }
    }
}
