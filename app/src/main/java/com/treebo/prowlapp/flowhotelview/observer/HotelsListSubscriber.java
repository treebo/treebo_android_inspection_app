package com.treebo.prowlapp.flowhotelview.observer;

import com.treebo.prowlapp.flowhotelview.presenter.HotelsListPresenter;
import com.treebo.prowlapp.flowhotelview.usecase.GetAllHotelsUseCase;
import com.treebo.prowlapp.mvpviews.HotelsListMVPView;
import com.treebo.prowlapp.response.HotelsListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by sumandas on 16/08/2016.
 */

public class HotelsListSubscriber extends BaseObserver<HotelsListResponse> {

    HotelsListPresenter mHotelsListPresenter;
    GetAllHotelsUseCase mGetAllHotelsUseCase;
    HotelsListMVPView mView;


    public HotelsListSubscriber(HotelsListPresenter presenter, GetAllHotelsUseCase useCase, HotelsListMVPView view){
        super(presenter, useCase);
        mHotelsListPresenter=presenter;
        mGetAllHotelsUseCase=useCase;
        mView=view;
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(HotelsListResponse listResponse) {
        if(listResponse.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)){
            mView.onHotelsListSuccess(listResponse);
        }else {
            mView.onHotelsListFailed(listResponse.msg);
        }
    }
}