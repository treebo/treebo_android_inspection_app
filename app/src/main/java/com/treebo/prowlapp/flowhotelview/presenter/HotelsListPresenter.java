package com.treebo.prowlapp.flowhotelview.presenter;

import com.treebo.prowlapp.flowhotelview.observer.HotelsListSubscriber;
import com.treebo.prowlapp.flowhotelview.observer.LogoutObserver;
import com.treebo.prowlapp.flowhotelview.usecase.GetAllHotelsUseCase;
import com.treebo.prowlapp.flowhotelview.usecase.LogoutUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.HotelsListMVPView;
import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by devesh on 03/05/16.
 */
@Deprecated
public class HotelsListPresenter implements BasePresenter {

    HotelsListMVPView mBaseView;

    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView = (HotelsListMVPView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }


    public void getAllHotels(GetAllHotelsUseCase getAllHotelsUseCase, HotelsListSubscriber hotelsListSubscriber){
        hideRetry();
        hideEmptyView();
        showLoading();
        getAllHotelsUseCase.execute(hotelsListSubscriber);
    }

    public void logout(String accessToken,LogoutUseCase logoutUseCase, LogoutObserver logoutObserver){
        hideRetry();
        hideEmptyView();
        showLoading();
        logoutUseCase.setmAccessToken(accessToken);
        logoutUseCase.execute(logoutObserver);
    }

    private void hideEmptyView() {
        mBaseView.hideEmptyView();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
    }

    @Override
    public BaseView getView() {
        return mBaseView;
    }


    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }




}