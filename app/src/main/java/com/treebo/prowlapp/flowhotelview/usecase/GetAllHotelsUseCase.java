package com.treebo.prowlapp.flowhotelview.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.HotelsListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by devesh on 03/05/16.
 */
public class GetAllHotelsUseCase extends UseCase<HotelsListResponse> {


    private int mUserId;
    RestClient mRestClient;

    public GetAllHotelsUseCase(RestClient restClient) {
        mRestClient=restClient;
    }

    public void setmUserId(int mUserId) {
        this.mUserId = mUserId;
    }

    @Override
    protected Observable<HotelsListResponse> getObservable() {
        return mRestClient.getAllHotels(mUserId);
    }
}
