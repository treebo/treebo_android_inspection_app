package com.treebo.prowlapp.flowissuenew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.usecase.UseCase;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by abhisheknair on 03/05/17.
 */

public class PostEscalationUseCase extends UseCase<BaseResponse> {

    RestClient mRestClient;
    int mHotelID;
    String mComment;
    int mUserID;
    ArrayList<Integer> mEscalationIDList;
    int mIssueID;

    public PostEscalationUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setFields(int hotelID, int issueID, int userID,
                          String comment, ArrayList<Integer> escalationIDList) {
        this.mHotelID = hotelID;
        this.mEscalationIDList = escalationIDList;
        this.mUserID = userID;
        this.mComment = comment;
        this.mIssueID = issueID;
    }


    @Override
    protected Observable<BaseResponse> getObservable() {
        return mRestClient.escalateIssue(mHotelID, mEscalationIDList, mComment, mUserID, mIssueID);
    }
}
