package com.treebo.prowlapp.flowissuenew.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.views.RoundedCornersTransformation;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 01/05/2016.
 */
public class ImageUrlAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static int sCorner = 25;
    public static int sMargin = 4;

    private ArrayList<String> mImageUrls=new ArrayList<>();
    private Context mContext;
    private ListInteractor mListInteractor;

    private boolean isRemove;

    public ImageUrlAdapter(Context context ,ArrayList<String> imageUrls,boolean canRemove) {
        mImageUrls=imageUrls;
        mContext = context;
        isRemove=canRemove ;
    }

    public void setmImageUrls(ArrayList<String> mImageUrls) {
        this.mImageUrls = mImageUrls;
        notifyDataSetChanged();
    }

    public void setListInteractor(ListInteractor listInteractor){
        this.mListInteractor = listInteractor;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup rootView;
        if(isRemove)
            rootView = (ViewGroup) inflater.inflate(R.layout.image_view_with_cross, parent, false);
        else
            rootView = (ViewGroup) inflater.inflate(R.layout.image_placeholder, parent, false);
        return new IndividualViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final String url = mImageUrls.get(position);
        if(isRemove){
            ((IndividualViewHolder) holder).crossImage.setVisibility(View.VISIBLE);
            ((IndividualViewHolder) holder).crossImage.setOnClickListener((View v)->{
                mImageUrls.remove(position);
                notifyDataSetChanged();
                if(mImageUrls.size()==0){
                    if(mListInteractor !=null){
                        mListInteractor.onListEmpty();
                    }
                }
            });

        }else{
            ((IndividualViewHolder) holder).crossImage.setVisibility(View.GONE);
        }
        ((IndividualViewHolder) holder).itemView.setOnClickListener(v -> {
            if (mListInteractor != null) {
                mListInteractor.imageItemClicked(position, mImageUrls.size(), url);
            }

        });
        Glide.with(mContext).load(mImageUrls.get(position))
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .bitmapTransform(new RoundedCornersTransformation(mContext,sCorner, sMargin))
                .diskCacheStrategy(DiskCacheStrategy.ALL).crossFade().into(((IndividualViewHolder) holder).issueImage);
    }

    @Override
    public int getItemCount() {
        return mImageUrls.size();
    }

    public class IndividualViewHolder extends RecyclerView.ViewHolder {
        public ImageView issueImage;
        public ImageView crossImage;

        public IndividualViewHolder(View itemView) {
            super(itemView);
            issueImage = (ImageView) itemView.findViewById(R.id.issue_image_placeholder);
            crossImage = (ImageView) itemView.findViewById(R.id.cross_item);
        }
    }

    public interface ListInteractor {
        void onListEmpty();
        void imageItemClicked(int pos, int total, String imageUrl);
    }

}
