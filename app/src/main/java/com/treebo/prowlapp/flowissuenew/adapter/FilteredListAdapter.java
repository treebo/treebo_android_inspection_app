package com.treebo.prowlapp.flowissuenew.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import androidx.annotation.NonNull;

/**
 * Created by abhisheknair on 04/05/17.
 */

public class FilteredListAdapter extends ArrayAdapter<EscalationUserListResponse.EscalationUser> {

    private List<EscalationUserListResponse.EscalationUser> mList;
    private Filter filter;
    private int layoutResourceId;

    public FilteredListAdapter(Context context,
                               List<EscalationUserListResponse.EscalationUser> list, int resource) {
        super(context, resource, list);
        this.mList = list;
        this.layoutResourceId = resource;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public EscalationUserListResponse.EscalationUser getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mList.get(position).hashCode();
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
        TreeboTextView name = (TreeboTextView) convertView.findViewById(R.id.name);
        name.setText(mList.get(position).getFirstName() + " " + mList.get(position).getLastName());
        TreeboTextView email = (TreeboTextView) convertView.findViewById(R.id.email);
        email.setText(mList.get(position).getEmail());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new AppFilter(mList);
        return filter;
    }

    /**
     * Filter method used by the adapter. Return true if the object should remain in the list
     *
     * @param obj  object we are checking for inclusion in the adapter
     * @param mask current text in the edit text we are completing against
     * @return true if we should keep the item in the adapter
     */
    private boolean keepObject(EscalationUserListResponse.EscalationUser obj, String mask) {
        mask = mask.toLowerCase();
        return obj.getFirstName().toLowerCase().startsWith(mask) || obj.getEmail().toLowerCase().startsWith(mask);
    }

    /**
     * Class for filtering Adapter, relies on keepObject in FilteredArrayAdapter
     * <p>
     * based on gist by Tobias Schürg
     * in turn inspired by inspired by Alxandr
     * (http://stackoverflow.com/a/2726348/570168)
     */
    private class AppFilter extends Filter {

        private ArrayList<EscalationUserListResponse.EscalationUser> sourceObjects;

        public AppFilter(List<EscalationUserListResponse.EscalationUser> objects) {
            setSourceObjects(objects);
        }

        public void setSourceObjects(List<EscalationUserListResponse.EscalationUser> objects) {
            synchronized (this) {
                sourceObjects = new ArrayList<>(objects);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence chars) {
            FilterResults result = new FilterResults();
            if (chars != null && chars.length() > 0) {
                String mask = chars.toString();
                ArrayList<EscalationUserListResponse.EscalationUser> keptObjects = new ArrayList<>();

                for (EscalationUserListResponse.EscalationUser object : sourceObjects) {
                    if (keepObject(object, mask))
                        keptObjects.add(object);
                }
                result.count = keptObjects.size();
                result.values = keptObjects;
            } else {
                // add all objects
                result.values = sourceObjects;
                result.count = sourceObjects.size();
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results.count > 0) {
                FilteredListAdapter.this.addAll((Collection) results.values);
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
