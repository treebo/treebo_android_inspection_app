package com.treebo.prowlapp.flowissuenew;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowissuenew.activity.EscalateIssueActivity;
import com.treebo.prowlapp.flowissuenew.activity.IssueDetailsActivity;
import com.treebo.prowlapp.flowissuenew.activity.IssueListActivity;
import com.treebo.prowlapp.flowissuenew.activity.UpdateIssueActivity;

import dagger.Component;

/**
 * Created by abhisheknair on 15/11/16.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, IssueModuleNew.class})
public interface IssueComponentNew {
    void injectIssueListActivity(IssueListActivity issueListActivity);

    void injectIssueDetailsActivity(IssueDetailsActivity issueDetailsActivity);

    void injectUpdateIssueActivity(UpdateIssueActivity updateIssueActivity);

    void injectEscalateIssueActivity(EscalateIssueActivity escalateIssueActivity);
}
