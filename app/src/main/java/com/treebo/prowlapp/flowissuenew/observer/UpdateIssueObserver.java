package com.treebo.prowlapp.flowissuenew.observer;

import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.usecase.UpdateIssueUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;

/**
 * Created by abhisheknair on 18/11/16.
 */

public class UpdateIssueObserver extends BaseObserver<BaseResponse> {

    private IIssueContract.IUpdateIssueView mView;
    private UpdateIssueUseCase mUseCase;

    public UpdateIssueObserver(BasePresenter presenter, UpdateIssueUseCase useCase, IIssueContract.IUpdateIssueView view) {
        super(presenter, useCase);
        this.mUseCase = useCase;
        this.mView = view;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(BaseResponse baseResponse) {
        mView.hideLoading();
        if (baseResponse.status.equals("success")) {
            mView.onUpdateIssueSuccess();
        } else {
            mView.onUpdateIssueFailure(baseResponse.msg);
        }
    }
}
