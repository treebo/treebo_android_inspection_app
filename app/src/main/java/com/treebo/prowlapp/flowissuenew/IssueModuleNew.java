package com.treebo.prowlapp.flowissuenew;

import com.treebo.prowlapp.flowissuenew.presenter.EscalateIssuePresenter;
import com.treebo.prowlapp.flowissuenew.presenter.IssueDetailsPresenter;
import com.treebo.prowlapp.flowissuenew.presenter.IssueListPresenter;
import com.treebo.prowlapp.flowissuenew.presenter.UpdateIssuePresenter;
import com.treebo.prowlapp.flowissuenew.usecase.EscalationUserListUseCase;
import com.treebo.prowlapp.flowissuenew.usecase.IssueDetailsUseCase;
import com.treebo.prowlapp.flowissuenew.usecase.IssueListUseCase;
import com.treebo.prowlapp.flowissuenew.usecase.IssuesNotCaughtUseCase;
import com.treebo.prowlapp.flowissuenew.usecase.PostEscalationUseCase;
import com.treebo.prowlapp.flowissuenew.usecase.UpdateIssueUseCase;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by abhisheknair on 15/11/16.
 */
@Module
public class IssueModuleNew {

    @Provides
    RestClient providesRestClient() {
        return new RestClient();
    }

    @Provides
    IssueListPresenter providesIssueListPresenter() {
        return new IssueListPresenter();
    }

    @Provides
    IssueDetailsPresenter providesIssueDetailsPresenter() {
        return new IssueDetailsPresenter();
    }

    @Provides
    UpdateIssuePresenter providesUpdateIssuePresenter() {
        return new UpdateIssuePresenter();
    }

    @Provides
    EscalateIssuePresenter providesEscalateIssuePresenter() {
        return new EscalateIssuePresenter();
    }

    @Provides
    IssueListUseCase providesIssueListUseCase(RestClient client) {
        return new IssueListUseCase(client);
    }

    @Provides
    IssueDetailsUseCase providesIssueDetailsUseCase(RestClient restClient) {
        return new IssueDetailsUseCase(restClient);
    }

    @Provides
    UpdateIssueUseCase providesUpdateIssueUseCase(RestClient restClient) {
        return new UpdateIssueUseCase(restClient);
    }

    @Provides
    EscalationUserListUseCase providesEscalationUserListUseCase(RestClient restClient) {
        return new EscalationUserListUseCase(restClient);
    }

    @Provides
    PostEscalationUseCase providesPostEscalationUseCase(RestClient restClient) {
        return new PostEscalationUseCase(restClient);
    }

    @Provides
    IssuesNotCaughtUseCase providesIssuesNotCaughtUseCase(RestClient restClient) {
        return new IssuesNotCaughtUseCase(restClient);
    }

}
