package com.treebo.prowlapp.flowissuenew.stategy;

/**
 * Created by sumandas on 19/11/2016.
 */

public interface IIssueStrategy {
    String getScreenTitle();
    String getBottomButtonTitle();
    String getStatus();
    int getResultCode();
}
