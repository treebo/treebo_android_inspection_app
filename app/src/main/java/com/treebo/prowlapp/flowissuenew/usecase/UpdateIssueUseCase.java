package com.treebo.prowlapp.flowissuenew.usecase;

import com.treebo.prowlapp.Models.issuemodel.UpdateIssueModelV3;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.PhotoUtils;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by abhisheknair on 18/11/16.
 */

public class UpdateIssueUseCase extends UseCase<BaseResponse> {

    private RestClient mRestClient;

    int hotelID;
    int issueID;
    String resolutionDate;
    String comment;
    ArrayList<String> images;
    String issueStatus;
    String userID;
    int taskLogID;

    public UpdateIssueUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setFields(int hotelID, int issueID, UpdateIssueModelV3 modelV3) {
        this.hotelID = hotelID;
        this.issueID = issueID;
        this.resolutionDate = modelV3.getResolutionDate();
        this.comment = modelV3.getComment();
        this.images = modelV3.getImageList();
        this.issueStatus = modelV3.getStatus();
        this.userID = String.valueOf(modelV3.getUserID());
        this.taskLogID = modelV3.getTaskLogID();
    }

    @Override
    protected Observable<BaseResponse> getObservable() {
        return mRestClient.updateIssue(hotelID, issueID, resolutionDate, comment, PhotoUtils.getCSVForUrlList(images),
                issueStatus, userID, taskLogID);
    }
}
