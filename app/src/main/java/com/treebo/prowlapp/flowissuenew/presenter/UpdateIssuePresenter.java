package com.treebo.prowlapp.flowissuenew.presenter;

import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.observer.UpdateIssueObserver;
import com.treebo.prowlapp.flowissuenew.usecase.UpdateIssueUseCase;
import com.treebo.prowlapp.Models.issuemodel.UpdateIssueModelV3;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 18/11/16.
 */

public class UpdateIssuePresenter implements IIssueContract.IUpdateIssuePresenter {

    private IIssueContract.IUpdateIssueView mView;
    private UpdateIssueUseCase mUseCase;

    @Override
    public void postIssueUpdate(int hotelID, int issueID, UpdateIssueModelV3 issue) {
        mView.showLoading();
        mUseCase.setFields(hotelID, issueID, issue);
        mUseCase.execute(providesUpdateIssueObserver());
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IIssueContract.IUpdateIssueView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    public void setUpdateIssueUseCase(UpdateIssueUseCase useCase) {
        this.mUseCase = useCase;
    }

    private UpdateIssueObserver providesUpdateIssueObserver() {
        return new UpdateIssueObserver(this, mUseCase, mView);
    }
}
