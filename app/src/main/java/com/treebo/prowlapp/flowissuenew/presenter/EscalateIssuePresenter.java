package com.treebo.prowlapp.flowissuenew.presenter;

import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.observer.GetEscalationUserListObserver;
import com.treebo.prowlapp.flowissuenew.observer.PostEscalationObserver;
import com.treebo.prowlapp.flowissuenew.usecase.EscalationUserListUseCase;
import com.treebo.prowlapp.flowissuenew.usecase.PostEscalationUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 03/05/17.
 */

public class EscalateIssuePresenter implements IIssueContract.IEscalateIssuePresenter {

    private IIssueContract.IEscalateIssueView mView;

    private PostEscalationUseCase mPostEscalationUseCase;

    private EscalationUserListUseCase mUserListUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IIssueContract.IEscalateIssueView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void getEscalationUserDetails() {
        mView.showLoading();
        mUserListUseCase.execute(providesGetEscalationUserListObserver());
    }

    @Override
    public void postEscalation(int hotelID, ArrayList<Integer> escalationUserList, int userID, int issueID,
                               String  comment) {
        mView.showLoading();
        mPostEscalationUseCase.setFields(hotelID, issueID, userID, comment, escalationUserList);
        mPostEscalationUseCase.execute(providesPostEscalationObserver());
    }

    @Override
    public void populateUserList(EscalationUserListResponse response) {
        mView.hideLoading();
        mView.setUpUserList(response);
    }

    @Override
    public void getListError(String msg) {
        mView.hideLoading();
        mView.showError(msg);
    }

    @Override
    public void handlePostSuccess() {
        mView.hideLoading();
        mView.showSuccessScreen();
    }

    @Override
    public void handleAPIError(String msg) {
        mView.hideLoading();
        mView.showError(msg);
    }

    public void setPostEscalationUseCase(PostEscalationUseCase useCase) {
        this.mPostEscalationUseCase = useCase;
    }

    public void setUseListUseCase(EscalationUserListUseCase useCase) {
        this.mUserListUseCase = useCase;
    }

    private PostEscalationObserver providesPostEscalationObserver() {
        return new PostEscalationObserver(this, mPostEscalationUseCase);
    }

    private GetEscalationUserListObserver providesGetEscalationUserListObserver() {
        return new GetEscalationUserListObserver(this, mUserListUseCase);
    }
}
