package com.treebo.prowlapp.flowissuenew.observer;

import com.treebo.prowlapp.flowissuenew.presenter.EscalateIssuePresenter;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 03/05/17.
 */

public class GetEscalationUserListObserver extends BaseObserver<EscalationUserListResponse> {

    private EscalateIssuePresenter mPresenter;

    public GetEscalationUserListObserver(EscalateIssuePresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(EscalationUserListResponse escalationUserListResponse) {
        if (escalationUserListResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.populateUserList(escalationUserListResponse);
        } else {
            mPresenter.getListError(escalationUserListResponse.msg);
        }
    }
}
