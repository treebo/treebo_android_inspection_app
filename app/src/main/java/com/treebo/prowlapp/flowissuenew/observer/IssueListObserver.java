package com.treebo.prowlapp.flowissuenew.observer;

import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.presenter.IssueListPresenter;
import com.treebo.prowlapp.flowissuenew.usecase.IssueListUseCase;
import com.treebo.prowlapp.response.issue.IssueListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 16/11/16.
 */

public class IssueListObserver extends BaseObserver<IssueListResponse> {

    private IIssueContract.IIssueListPresenter mPresenter;

    public IssueListObserver(IssueListPresenter presenter, IssueListUseCase useCase) {
        super(presenter, useCase);
        mPresenter = presenter;
    }

    @Override
    public void onCompleted() {
        mPresenter.resume();
    }

    @Override
    public void onNext(IssueListResponse response) {
        mPresenter.resume();
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onLoadIssueListSuccess(response);
        } else {
            mPresenter.onLoadIssueListFailure("Loading issue list failed");
        }
    }
}
