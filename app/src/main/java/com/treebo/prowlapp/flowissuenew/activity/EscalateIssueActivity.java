package com.treebo.prowlapp.flowissuenew.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;

import com.tokenautocomplete.TokenCompleteTextView;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowissuenew.DaggerIssueComponentNew;
import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.IssueComponentNew;
import com.treebo.prowlapp.flowissuenew.adapter.FilteredListAdapter;
import com.treebo.prowlapp.flowissuenew.presenter.EscalateIssuePresenter;
import com.treebo.prowlapp.flowissuenew.usecase.EscalationUserListUseCase;
import com.treebo.prowlapp.flowissuenew.usecase.PostEscalationUseCase;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;
import com.treebo.prowlapp.views.UserAutoCompleteView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 03/05/17.
 */

public class EscalateIssueActivity extends AppCompatActivity implements IIssueContract.IEscalateIssueView,
        TextWatcher {


    @Inject
    protected EscalationUserListUseCase mUserListUseCase;

    @Inject
    protected PostEscalationUseCase mPostEscalationUseCase;

    @Inject
    protected EscalateIssuePresenter mPresenter;

    @Inject
    protected LoginSharedPrefManager mSharedPrefs;

    private int mHotelID;
    private String mHotelName;
    private int mIssueID;

    private View mLoadingView;
    private View mSuccessView;
    private TreeboTextView mUpdateCommentTitle;
    private TreeboEditText mUpdateCommentText;
    private TreeboButton mDone;
    private boolean submitted;
    private TreeboTextView mCommentErrorText;
    private UserAutoCompleteView mEmailTextView;
    private FilteredListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escalation);
        setToolBarColor(R.color.white);
        SegmentAnalyticsManager.setmLoginManager(mSharedPrefs);

        IssueComponentNew issueComponent = DaggerIssueComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        issueComponent.injectEscalateIssueActivity(this);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mHotelID = intent.getIntExtra(Utils.HOTEL_ID, 0);
            mHotelName = intent.getStringExtra(Utils.HOTEL_NAME);
            mIssueID = intent.getIntExtra(Utils.ISSUE_ID, 0);
        } else {
            mHotelName = savedInstanceState.getString(Utils.HOTEL_NAME);
            mHotelID = savedInstanceState.getInt(Utils.HOTEL_ID);
            mIssueID = savedInstanceState.getInt(Utils.ISSUE_ID);
        }

        View toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        TreeboTextView title = (TreeboTextView) toolbar.findViewById(R.id.toolbar_title_text);
        title.setText(getString(R.string.escalate_issue));
        View crossBtn = toolbar.findViewById(R.id.room_picker_back);
        crossBtn.setOnClickListener(view -> onBackPressed());

        mEmailTextView = (UserAutoCompleteView) findViewById(R.id.email_input_view);
        mEmailTextView.setPrefix("To: ");
        char[] splitChar = {',', ';', ' '};
        mEmailTextView.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        mEmailTextView.allowDuplicates(false);
        mEmailTextView.setSplitChar(splitChar);
        mCommentErrorText = (TreeboTextView) findViewById(R.id.feedback_error_tv);
        mUpdateCommentTitle = (TreeboTextView) findViewById(R.id.feedback_title_text);
        mUpdateCommentText = (TreeboEditText) findViewById(R.id.feedback_edittext);
        mUpdateCommentText.addTextChangedListener(this);
        mUpdateCommentText.setOnClickListener(view -> {
            mUpdateCommentText.setHint("");
            setEditTextBackGround(true);
        });
        mUpdateCommentText.setOnFocusChangeListener((view, b) -> {
            if (!b)
                Utils.hideSoftKeyboard(findViewById(android.R.id.content));
        });
        mLoadingView = findViewById(R.id.layout_progress);
        mSuccessView = findViewById(R.id.audit_success_layout);
        TreeboTextView textView = (TreeboTextView) mSuccessView.findViewById(R.id.audit_successfully_submitted);
        textView.setText(getString(R.string.escalation_submit_success));
        mDone = (TreeboButton) findViewById(R.id.feedback_submit_btn);
        mDone.setOnClickListener(view -> {
            Utils.hideSoftKeyboard(findViewById(android.R.id.content));
            if (Utils.isInternetConnected(this)) {
                if (!TextUtils.isEmpty(mUpdateCommentText.getText())) {
                    if (getIDList().size() == 0) {
                        mCommentErrorText.setText(getString(R.string.no_user_error));
                        mCommentErrorText.setVisibility(View.VISIBLE);
                    } else {
                        mPresenter.postEscalation(mHotelID, getIDList(),
                                mSharedPrefs.getUserId(), mIssueID, mUpdateCommentText.getText().toString());
                    }
                } else {
                    setError(true);
                }
            } else {
                SnackbarUtils.show(findViewById(android.R.id.content), getString(R.string.no_internet_msg));
            }
        });
        if (mSharedPrefs.getEscalationUserList().size() == 0) {
            mPresenter.getEscalationUserDetails();
        } else {
            setAdapterWithData(mSharedPrefs.getEscalationUserList());
        }

    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setUseListUseCase(mUserListUseCase);
        mPresenter.setPostEscalationUseCase(mPostEscalationUseCase);
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(mUpdateCommentText.getText()) && !submitted)
            showStopUpdateDialog();
        else {
            finish();
            overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        }
    }

    private ArrayList<Integer> getIDList() {
        List<EscalationUserListResponse.EscalationUser> list = mEmailTextView.getObjects();
        ArrayList<Integer> idList = new ArrayList<>();
        for (EscalationUserListResponse.EscalationUser object : list) {
            idList.add(object.getId());
        }
        return idList;
    }

    private void setEditTextBackGround(boolean enabled) {
        if (!enabled) {
            mUpdateCommentTitle.setVisibility(View.GONE);
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle_border_gray));
        } else {
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle_border_green));
            mUpdateCommentTitle.setVisibility(View.VISIBLE);
        }
    }

    private void setError(boolean isError) {
        if (isError) {
            mCommentErrorText.setVisibility(View.VISIBLE);
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle_border_red));
        } else {
            mCommentErrorText.setVisibility(View.GONE);
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_edittext_selector));
        }
    }

    private void showStopUpdateDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_update_back, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog stopUpdateDialog = dialogBuilder.create();
        stopUpdateDialog.show();

        TreeboTextView cancelBtn = (TreeboTextView) dialogView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(view -> stopUpdateDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            stopUpdateDialog.dismiss();
            finish();
            overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        });

    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
        mDone.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
        mDone.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void setUpUserList(EscalationUserListResponse response) {
        mSharedPrefs.setEscalationUserList(response.getUserList());
        setAdapterWithData(response.getUserList());
    }

    @Override
    public void showSuccessScreen() {
        submitted = true;
        mDone.setVisibility(View.GONE);
        mSuccessView.setVisibility(View.VISIBLE);
        setToolBarColor(R.color.green_color_primary);
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            onBackPressed();
        }, 2000);
    }

    private void setAdapterWithData(ArrayList<EscalationUserListResponse.EscalationUser> users) {
        adapter = new FilteredListAdapter(EscalateIssueActivity.this, users, R.layout.item_email_dropdown);
        mEmailTextView.setAdapter(adapter);
        mEmailTextView.setOnFocusChangeListener((v, hasFocus) -> {
                    if (hasFocus)
                        mEmailTextView.showDropDown();
                }
        );
    }

    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() == 0) {
            mUpdateCommentText.setHint(getString(R.string.what_can_we_do_better));
            setEditTextBackGround(false);
        } else if (editable.length() > 0 && editable.length() < 2) {
            setError(false);
            setEditTextBackGround(true);
        } else {
            setEditTextBackGround(true);
        }
    }
}
