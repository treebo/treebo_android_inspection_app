package com.treebo.prowlapp.flowissuenew.presenter;

import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.observer.IssueListObserver;
import com.treebo.prowlapp.flowissuenew.observer.IssuesNotCaughtListObserver;
import com.treebo.prowlapp.flowissuenew.usecase.IssueListUseCase;
import com.treebo.prowlapp.flowissuenew.usecase.IssuesNotCaughtUseCase;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.issue.IssueListResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 15/11/16.
 */

public class IssueListPresenter implements IIssueContract.IIssueListPresenter {

    private IIssueContract.IIssueListView mView;
    private IssueListUseCase mUseCase;
    private IssuesNotCaughtUseCase mIncUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IIssueContract.IIssueListView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {
        mView.setUpViewAsPerPermissions();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        mView.hideLoading();
    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void onCreate() {
        if (mView != null) {
            mView.setUpRecyclerView();
        }
    }

    @Override
    public void loadIssueList(int hotelId) {
        // Make API call
        mView.showLoading();
        mUseCase.setHotelId(hotelId);
        mUseCase.execute(providesIssueListObserver());
    }

    @Override
    public void loadIssuesNotCaught(int userID) {
        mView.showLoading();
        mIncUseCase.setUserID(userID);
        mIncUseCase.execute(providesIncListObserver());
    }

    @Override
    public void setAdapterData(ArrayList<IssueListModelV3> issueList, int sortCriteria) {
        if (mView != null)
            mView.updateRecyclerView(issueList, sortCriteria, true);
    }

    @Override
    public void onLoadIssueListSuccess(IssueListResponse response) {
        mView.setResponseData(response);
    }

    @Override
    public void onLoadIssueListFailure(String msg) {
        mView.showError(msg);
    }

    public void setIssueListUseCase(IssueListUseCase useCase) {
        this.mUseCase = useCase;
    }

    public void setIssueNotCaughtUseCase(IssuesNotCaughtUseCase useCase) {
        this.mIncUseCase = useCase;
    }

    private IssueListObserver providesIssueListObserver() {
        return new IssueListObserver(this, mUseCase);
    }

    private IssuesNotCaughtListObserver providesIncListObserver() {
        return new IssuesNotCaughtListObserver(this, mIncUseCase);
    }
}
