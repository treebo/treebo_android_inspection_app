package com.treebo.prowlapp.flowissuenew.activity;

import android.Manifest;
import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding.view.RxView;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowissuenew.DaggerIssueComponentNew;
import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.IssueComponentNew;
import com.treebo.prowlapp.flowissuenew.presenter.UpdateIssuePresenter;
import com.treebo.prowlapp.flowissuenew.stategy.IIssueStrategy;
import com.treebo.prowlapp.flowissuenew.stategy.IssueStrategyFactory;
import com.treebo.prowlapp.flowissuenew.usecase.UpdateIssueUseCase;
import com.treebo.prowlapp.flowupdateauditOrissue.ImageAdapter;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.Models.issuemodel.UpdateIssueModelV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AmazonS3UploadTask;
import com.treebo.prowlapp.Utils.CompressImageUtils;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.Utils.Logger;
import com.treebo.prowlapp.Utils.NetworkUtil;
import com.treebo.prowlapp.Utils.PermissionHelper;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utilities;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 18/11/16.
 */

public class UpdateIssueActivity extends AppCompatActivity implements IIssueContract.IUpdateIssueView,
        DatePickerDialog.OnDateSetListener, ImageAdapter.ListInteractor, TextWatcher {

    private static final Integer PIC_PHOTO = 1;

    private TreeboTextView mDateText;
    private TreeboEditText mUpdateCommentText;
    private TreeboTextView mUpdateCommentTitle;
    private View mSuccessView;

    private int mHotelID;
    private int mIssueID;
    private int mUserID;

    @Inject
    public UpdateIssuePresenter mPresenter;

    @Inject
    public UpdateIssueUseCase mUseCase;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    ArrayList<Photo> mPhotoList = new ArrayList<>();
    ArrayList<String> mPhotoUrlList = new ArrayList<>();

    private CompositeSubscription mSubscription = new CompositeSubscription();

    private String mIssueResolutionDate;

    private RecyclerView mImageList;
    private ImageAdapter mImageAdapter;
    private View mCommentErrorText;

    String mFilePathToBeUploaded = "";
    private String mPhotokey = "prowl";
    Photo mPhotoGettingUploaded;
    private ArrayList<Photo> mPhotosAdded = new ArrayList<>();
    private File mTempPickerDir = new File(Environment.getExternalStorageDirectory(), File.separator + Utils.PROWL_DIR + File.separator);
    private TreeboButton mDone;

    private boolean isResolveIssue;

    private IIssueStrategy mIssueStrategy;

    private boolean isResolutionDateChanged = false;
    private boolean disableBackPress;
    private Context mContext;
    private View mLoadingView;
    private int mTaskLogID;


    public static Intent getCallingIntent(Activity activity) {
        return new Intent(activity, UpdateIssueActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_update_issue);
        setToolBarColor(R.color.white);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mHotelID = intent.getIntExtra(Utils.HOTEL_ID, -1);
            mIssueID = intent.getIntExtra(Utils.ISSUE_ID, -1);
            mUserID = intent.getIntExtra(Utils.USER_ID, -1);
            mTaskLogID = intent.getIntExtra(Utils.TASK_LOG_ID, 0);
            isResolveIssue = intent.getBooleanExtra(Utils.ISSUE_RESOLVE, false);
            mIssueResolutionDate = intent.getStringExtra(Utils.ISSUE_RESOLUTION_DATE);
        } else {
            mHotelID = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            mIssueID = savedInstanceState.getInt(Utils.ISSUE_ID, -1);
            mUserID = savedInstanceState.getInt(Utils.USER_ID, -1);
            mTaskLogID = savedInstanceState.getInt(Utils.TASK_LOG_ID, 0);
            isResolveIssue = savedInstanceState.getBoolean(Utils.ISSUE_RESOLVE, false);
            mIssueResolutionDate = savedInstanceState.getString(Utils.ISSUE_RESOLUTION_DATE);
            mFilePathToBeUploaded = savedInstanceState.getString(Utils.PHOTO_PATH);
            mPhotoGettingUploaded = savedInstanceState.getParcelable(Utils.PHOTO_OBJECT);
        }

        mIssueStrategy = IssueStrategyFactory.getIssueStrategy(this, isResolveIssue);

        View toolbar = findViewById(R.id.toolbar_update_issue);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        TreeboTextView title = (TreeboTextView) toolbar.findViewById(R.id.toolbar_title_text);
        title.setText(mIssueStrategy.getScreenTitle());
        View crossBtn = toolbar.findViewById(R.id.room_picker_back);
        crossBtn.setOnClickListener(view -> onBackPressed());

        View dateView = findViewById(R.id.update_issue_date_picker);
        mLoadingView = findViewById(R.id.loader_layout);

        mSubscription.add(RxView.clicks(dateView).throttleFirst(4, TimeUnit.SECONDS).subscribe(s -> {
            openDatePicker();
        }));

        mDateText = (TreeboTextView) dateView.findViewById(R.id.update_issue_date_text);
        mDateText.setText(DateUtils.getDateAndMonthInFullFromTimeStamp(mIssueResolutionDate));
        if (isResolveIssue)
            dateView.setVisibility(View.GONE);

        mSuccessView = findViewById(R.id.layout_success);
        TreeboTextView successText = (TreeboTextView) mSuccessView.findViewById(R.id.audit_successfully_submitted);
        if (isResolveIssue)
            successText.setText(getString(R.string.issue_resolved_success));
        else
            successText.setText(getString(R.string.issue_update_success));
        mSuccessView.setVisibility(View.GONE);

        mUpdateCommentText = (TreeboEditText) findViewById(R.id.update_issue_comment_edittext);
        mUpdateCommentText.addTextChangedListener(this);
        mUpdateCommentText.setOnClickListener(view -> {
            mUpdateCommentText.setHint("");
            setEditTextBackGround(true);
        });
        mUpdateCommentText.setOnFocusChangeListener((view, b) -> {
            if (!b)
                Utils.hideSoftKeyboard(findViewById(android.R.id.content));
        });

        mUpdateCommentTitle = (TreeboTextView) findViewById(R.id.update_add_comment_title_text);

        mCommentErrorText = findViewById(R.id.update_comment_error_tv);

        View photoLayout = findViewById(R.id.add_photo_iv);
        photoLayout.setOnClickListener((View v) -> {
            if (Build.VERSION.SDK_INT >= 23) {
                if (PermissionHelper.hasNoPermissionToCamera(this)
                        || PermissionHelper.hasNoPermissionToExternalStorage(this)) {
                    SnackbarUtils.show(findViewById(android.R.id.content), getResources().getString(R.string.must_provide_camera));
                    ArrayList<String> permissons = new ArrayList<>();
                    if (PermissionHelper.hasNoPermissionToCamera(this)) {
                        permissons.add(Manifest.permission.CAMERA);
                    }

                    if (PermissionHelper.hasNoPermissionToExternalStorage(this)) {
                        permissons.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        permissons.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    }
                    String[] items = permissons.toArray(new String[permissons.size()]);
                    this.requestPermissions(items, 1);
                    return;
                }

            }
            Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent1.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri());
            startActivityForResult(intent1, PIC_PHOTO);
        });
        mImageList = (RecyclerView) findViewById(R.id.image_recyclerView);

        mImageList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mImageList.setItemAnimator(new DefaultItemAnimator());


        mImageAdapter = new ImageAdapter(this, mPhotoList, true);
        mImageAdapter.setListInteractor(this);
        mImageList.setAdapter(mImageAdapter);
        mImageAdapter.notifyDataSetChanged();

        mDone = (TreeboButton) findViewById(R.id.comment_done_btn);
        mDone.setText(mIssueStrategy.getBottomButtonTitle());

        mSubscription.add(RxView.clicks(mDone).throttleFirst(5, TimeUnit.SECONDS).subscribe(s -> {
            if (mUpdateCommentText.getText().length() > 0 || isResolveIssue) {
                if (Utils.isInternetConnected(this)) {
                    disableBackPress = true;
                    mPresenter.postIssueUpdate(mHotelID, mIssueID, getUpdateModel(true));
                } else {
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
                }
            } else {
                setError(true);
            }
        }));

        IssueComponentNew updateIssueComponent = DaggerIssueComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        updateIssueComponent.injectUpdateIssueActivity(this);


    }

    private void setError(boolean isError) {
        if (isError) {
            mCommentErrorText.setVisibility(View.VISIBLE);
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle_border_red));
        } else {
            mCommentErrorText.setVisibility(View.GONE);
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_edittext_selector));
        }
    }

    private void setEditTextBackGround(boolean enabled) {
        if (!enabled) {
            mUpdateCommentTitle.setVisibility(View.GONE);
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle_border_gray));
        } else {
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle_border_green));
            mUpdateCommentTitle.setVisibility(View.VISIBLE);
        }
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setUpdateIssueUseCase(mUseCase);
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(mUpdateCommentText.getText()) || mPhotoList.size() > 0)
            showStopUpdateDialog();
        else {
            if (!disableBackPress) {
                finish();
                overridePendingTransition(R.anim.no_change, R.anim.slide_down);
            }
        }
    }

    private UpdateIssueModelV3 getUpdateModel(boolean setResolutionDate) {
        String resolutionDate;

        if (setResolutionDate) {
            resolutionDate = mIssueResolutionDate;
        } else {
            resolutionDate = isResolutionDateChanged ? mIssueResolutionDate : "";
        }
        return new UpdateIssueModelV3(resolutionDate, mUserID, mUpdateCommentText.getText().toString(),
                mPhotoUrlList, mIssueStrategy.getStatus(), mTaskLogID);
    }

    private void openDatePicker() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date resolutionDate = new Date();
        try {
            resolutionDate = sdf.parse(mIssueResolutionDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar now = Calendar.getInstance();
        Calendar resolutionDateCalender = Calendar.getInstance();
        resolutionDateCalender.setTime(resolutionDate);
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                UpdateIssueActivity.this,
                resolutionDateCalender.get(Calendar.YEAR),
                resolutionDateCalender.get(Calendar.MONTH),
                resolutionDateCalender.get(Calendar.DAY_OF_MONTH),
                getString(R.string.cancel));
        dpd.setAccentColor(ContextCompat.getColor(this, R.color.emerald));
        dpd.dismissOnPause(true);
        dpd.setOnDateSetListener(this);
        dpd.setMinDate(now);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        disableBackPress = true;
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        view.dismiss();
        if (mUpdateCommentText.getText().length() == 0) {
            mUpdateCommentText.setHint(getString(R.string.add_comment));
            setEditTextBackGround(false);
        }
        String newDate = year + "-" +
                (monthOfYear + 1) + "-" + dayOfMonth;
        if (!newDate.equals(mIssueResolutionDate)) {
            isResolutionDateChanged = true;
        }
        mIssueResolutionDate = newDate;
        mDateText.setText(DateUtils.getDateAndMonthInFullFromTimeStamp(mIssueResolutionDate));
    }

    @Override
    public void onListEmpty() {
        mImageList.setVisibility(View.GONE);
    }

    @Override
    public void imageItemClicked(int pos, int total, String imageUrl) {
        mPhotoList.remove(pos);
        mImageAdapter.notifyDataSetChanged();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() == 0) {
            mUpdateCommentText.setHint(getString(R.string.comment_hint_text));
            setEditTextBackGround(false);
        } else if (editable.length() > 0 && editable.length() < 2) {
            setError(false);
            setEditTextBackGround(true);
        } else {
            setEditTextBackGround(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    if (!mFilePathToBeUploaded.isEmpty()) {
                        String filePath = mTempPickerDir + File.separator + mFilePathToBeUploaded;
                        File file = new File(filePath);

                        RxUtils.build(Observable.just(CompressImageUtils.compressImage(file))).subscribe(file1 -> {
                            persistPicture(file1);
                            if (NetworkUtil.isConnected(UpdateIssueActivity.this)) {
                                uploadPicture(file1);
                            } else {
                                SnackbarUtils.show(findViewById(android.R.id.content), getString(R.string.unable_to_upload_photo_no_network));
                            }

                            Log.d("UpdateIssueActivity", "File compress successfully: %s" + file1.getAbsolutePath());
                        });

                    }
                    break;
                } else if (resultCode == RESULT_CANCELED) {
                    Logger.d("IMAGE Canceled", "cancel");
                }
        }
    }


    private void uploadPicture(File fileToUpload) {
        String filePath = fileToUpload.getPath();
        AmazonS3UploadTask amazonS3UploadTask = new AmazonS3UploadTask(this, filePath, uploadImageCallback);
        amazonS3UploadTask.execute();
    }

    AmazonS3UploadTask.CallBackData uploadImageCallback = new AmazonS3UploadTask.CallBackData() {

        @Override
        public void onSuccess(String cloudUrl) {
            mPhotoGettingUploaded.setIsUploadedToCloud(true);
            mPhotoGettingUploaded.setmCloudFilePath(cloudUrl);
            mPhotoList.add(0, mPhotoGettingUploaded);
            if (mImageList.getVisibility() != View.VISIBLE) {
                mImageList.setVisibility(View.VISIBLE);
            }
            mImageAdapter.notifyDataSetChanged();
            Log.d("UpdateIssueActivity", cloudUrl);
            mPhotoUrlList.add(cloudUrl);
        }

        public void onError(Object result) {
            if (!((Activity) mContext).isFinishing())
                Utilities.cancelProgressDialog();
            SnackbarUtils.show(findViewById(android.R.id.content), getString(R.string.unable_to_attach_photo));

        }
    };

    private void persistPicture(File file) {
        mPhotoGettingUploaded = new Photo();
        mPhotoGettingUploaded.setmPhotoId(mFilePathToBeUploaded);
        mPhotoGettingUploaded.setmDeviceFilePath(file.getPath());
        mPhotoGettingUploaded.setmKey(mPhotokey);
        mPhotosAdded.add(0, mPhotoGettingUploaded);
    }

    private Uri getPhotoFileUri() {
        mFilePathToBeUploaded = mPhotokey + "_" + new SimpleDateFormat("yyyyMMddhhmmss'.jpg'").format(new Date());
        mFilePathToBeUploaded = mFilePathToBeUploaded.replaceAll(" ", "_").toLowerCase();
        mTempPickerDir.mkdirs();
        File issueFile = new File(mTempPickerDir, mFilePathToBeUploaded);
        return FileProvider.getUriForFile(
                this,
                getApplicationContext()
                        .getPackageName() + ".provider", issueFile);
    }

    @Override
    public void onUpdateIssueSuccess() {
        mSuccessView.setVisibility(View.VISIBLE);
        setToolBarColor(R.color.green_color_primary);
        mDone.setVisibility(View.GONE);
        Intent intent = new Intent();
        intent.putExtra(Utils.UPDATED_ISSUE_OBJECT, getUpdateModel(false));
        setResult(RESULT_OK, intent);

        Observable.timer(2, TimeUnit.SECONDS).subscribe(aLong -> {
            finish();
            overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        });

    }

    @Override
    public void onUpdateIssueFailure(String msg) {
        disableBackPress = true;
        showRetry();
    }

    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    private void showStopUpdateDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_update_back, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog stopUpdateDialog = dialogBuilder.create();
        stopUpdateDialog.show();

        TreeboTextView cancelBtn = (TreeboTextView) dialogView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(view -> stopUpdateDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            stopUpdateDialog.dismiss();
            finish();
            overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }


    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Utils.HOTEL_ID, mHotelID);
        outState.putInt(Utils.ISSUE_ID, mIssueID);
        outState.putInt(Utils.USER_ID, mUserID);
        outState.putInt(Utils.TASK_LOG_ID, mTaskLogID);
        outState.putBoolean(Utils.ISSUE_RESOLVE, isResolveIssue);
        outState.putString(Utils.ISSUE_RESOLUTION_DATE, mIssueResolutionDate);
        outState.putString(Utils.PHOTO_PATH, mFilePathToBeUploaded);
        outState.putParcelable(Utils.PHOTO_OBJECT, mPhotoGettingUploaded);
        super.onSaveInstanceState(outState);
    }
}
