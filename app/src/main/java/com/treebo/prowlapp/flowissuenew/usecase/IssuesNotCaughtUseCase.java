package com.treebo.prowlapp.flowissuenew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.issue.IssueListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 26/05/17.
 */

public class IssuesNotCaughtUseCase extends UseCase<IssueListResponse> {

    private RestClient mRestClient;
    private int mUserID;

    public IssuesNotCaughtUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setUserID(int userID) {
        this.mUserID = userID;
    }

    @Override
    protected Observable<IssueListResponse> getObservable() {
        return mRestClient.getIssueNotCaughtList(mUserID);
    }
}
