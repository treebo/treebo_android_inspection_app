package com.treebo.prowlapp.flowissuenew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 03/05/17.
 */

public class EscalationUserListUseCase extends UseCase<EscalationUserListResponse> {

    private RestClient mRestClient;

    public EscalationUserListUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    @Override
    protected Observable<EscalationUserListResponse> getObservable() {
        return mRestClient.getEscalationUserList();
    }
}
