package com.treebo.prowlapp.flowissuenew.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.issuemodel.IssueDetailsV3;
import com.treebo.prowlapp.Utils.IssueFormatUtils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 16/11/2016.
 */

public class IssueActionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<IssueDetailsV3.IssueLog> mIssueLogs = new ArrayList<>();
    private Context mContext;
    private ImageUrlAdapter.ListInteractor mListInteractor;

    public IssueActionAdapter(Context context, ArrayList<IssueDetailsV3.IssueLog> issueLogs
            , ImageUrlAdapter.ListInteractor listInteractor) {
        mIssueLogs = issueLogs;
        mContext = context;
        mListInteractor = listInteractor;
    }


    public void setmIssueLogs(ArrayList<IssueDetailsV3.IssueLog> mIssueLogs) {
        this.mIssueLogs = mIssueLogs;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup rootView;
        rootView = (ViewGroup) inflater.inflate(R.layout.item_detail_issue, parent, false);
        return new IndividualViewHolder(rootView, mListInteractor);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final IssueDetailsV3.IssueLog issueLog = mIssueLogs.get(position);

        ((IndividualViewHolder) holder).mActionTitle.setText(IssueFormatUtils.getActionTitle(issueLog));
        ((IndividualViewHolder) holder).mActionDate.setText(IssueFormatUtils.getIssueUpdateDate(issueLog));
        ((IndividualViewHolder) holder).mIssueTitleText.setText(IssueFormatUtils.getCommentOrReviewText(issueLog));
        String reviewTitle = IssueFormatUtils.getReviewTitleOrResolutionDate(issueLog);
        if (reviewTitle.isEmpty()) {
            ((IndividualViewHolder) holder).mIssueTitle.setVisibility(View.GONE);
        } else {
            ((IndividualViewHolder) holder).mIssueTitle.setVisibility(View.VISIBLE);
            ((IndividualViewHolder) holder).mIssueTitle.setText(reviewTitle);
        }

        if (issueLog.mImageUrls == null || issueLog.mImageUrls.isEmpty()) {
            ((IndividualViewHolder) holder).mImageList.setVisibility(View.GONE);
        } else {
            ((IndividualViewHolder) holder).mImageList.setVisibility(View.VISIBLE);
            ((IndividualViewHolder) holder).mImageUrlAdapter.setmImageUrls(issueLog.mImageUrls);
            ((IndividualViewHolder) holder).mImageUrlAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public int getItemCount() {
        return mIssueLogs.size();
    }

    public class IndividualViewHolder extends RecyclerView.ViewHolder {

        public TreeboTextView mActionTitle;
        public TreeboTextView mActionDate;
        public TreeboTextView mIssueTitle;
        public TreeboTextView mIssueTitleText;
        public RecyclerView mImageList;
        public ImageUrlAdapter mImageUrlAdapter;

        public IndividualViewHolder(View itemView, ImageUrlAdapter.ListInteractor interactor) {
            super(itemView);
            mActionTitle = (TreeboTextView) itemView.findViewById(R.id.issue_item_action_title);
            mActionDate = (TreeboTextView) itemView.findViewById(R.id.issue_item_action_date);
            mIssueTitle = (TreeboTextView) itemView.findViewById(R.id.issue_item_reason_text);
            mIssueTitleText = (TreeboTextView) itemView.findViewById(R.id.issue_item_review_title);
            mImageList = (RecyclerView) itemView.findViewById(R.id.issue_photo_list);
            mImageList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            mImageList.setItemAnimator(new DefaultItemAnimator());
            mImageUrlAdapter = new ImageUrlAdapter(mContext, new ArrayList<>(), false);
            mImageList.setAdapter(mImageUrlAdapter);
            mImageUrlAdapter.setListInteractor(interactor);

        }
    }


}
