package com.treebo.prowlapp.flowissuenew.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.material.appbar.AppBarLayout;
import com.jakewharton.rxbinding.view.RxView;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowissuenew.DaggerIssueComponentNew;
import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.IssueComponentNew;
import com.treebo.prowlapp.flowissuenew.adapter.ImageUrlAdapter;
import com.treebo.prowlapp.flowissuenew.adapter.IssueActionAdapter;
import com.treebo.prowlapp.flowissuenew.presenter.IssueDetailsPresenter;
import com.treebo.prowlapp.flowissuenew.usecase.IssueDetailsUseCase;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.fragments.ImageShowFragment;
import com.treebo.prowlapp.Models.issuemodel.IssueDetailsV3;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.Models.issuemodel.UpdateIssueModelV3;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.response.issue.IssueDetailResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.Utils.IssueFormatUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 15/11/16.
 */

public class IssueDetailsActivity extends AppCompatActivity implements IIssueContract.IIssueDetailsView, ImageUrlAdapter.ListInteractor {

    private IssueDetailsV3 mIssue;
    private IssueListModelV3 mIssueFromList;
    private int mHotelId;

    @Inject
    public IssueDetailsPresenter mDetailsPresenter;

    @Inject
    public IssueDetailsUseCase mIssueDetailsUseCase;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public Navigator mNavigator;

    public IssueActionAdapter mActionAdapter;
    public RecyclerView mActionView;

    public TreeboTextView mIssueSources;
    public TreeboTextView mIssueTime;

    public TreeboTextView mIssueCreatedDate;

    public TreeboTextView mIssueArea;
    public TreeboTextView mIssCheckpoint;
    public TreeboTextView mIssueSubCheckpoint;

    public View mLoaderView;

    public AppBarLayout mAppBarLayout;

    CompositeSubscription mSubscription = new CompositeSubscription();
    private int mTaskLogID;
    private ImageView mIssueIcon;
    private boolean isPreMarkedIssue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_details);
        IssueComponentNew issueComponent = DaggerIssueComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        issueComponent.injectIssueDetailsActivity(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);

        mIssueSources = (TreeboTextView) findViewById(R.id.issues_source_value);
        mIssueTime = (TreeboTextView) findViewById(R.id.issue_due_date);
        mActionView = (RecyclerView) findViewById(R.id.issues_update_adapter);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.issue_details_toolbar);

        mIssueIcon = (ImageView) findViewById(R.id.issue_details_type_image);
        mIssueArea = (TreeboTextView) findViewById(R.id.issue_details_room_number);
        mIssCheckpoint = (TreeboTextView) findViewById(R.id.issue_details_checkpoint);
        mIssueSubCheckpoint = (TreeboTextView) findViewById(R.id.issue_details_subcheckpoint);
        mIssueCreatedDate = (TreeboTextView) findViewById(R.id.issues_date_value);
        mActionView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        mActionView.setItemAnimator(new DefaultItemAnimator());

        mLoaderView = findViewById(R.id.loader_layout);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mIssueFromList = intent.getParcelableExtra(Utils.ISSUES_OBJECT);
            mHotelId = intent.getIntExtra(Utils.HOTEL_ID, -1);
            mTaskLogID = intent.getIntExtra(Utils.TASK_LOG_ID, 0);
            isPreMarkedIssue = intent.getBooleanExtra(Utils.IS_FROM_PRE_EXISTING_ISSUE, false);
        } else {
            mIssueFromList = savedInstanceState.getParcelable(Utils.ISSUES_OBJECT);
            mIssue = savedInstanceState.getParcelable(Utils.ISSUES_RESPONSE_OBJECT);
            mHotelId = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            mTaskLogID = savedInstanceState.getInt(Utils.TASK_LOG_ID, 0);
            isPreMarkedIssue = savedInstanceState.getBoolean(Utils.IS_FROM_PRE_EXISTING_ISSUE);
        }
        RolesAndUserPermissions.PermissionList userPermissions = mLoginManager.getUserPermissionList(mHotelId);

        SegmentAnalyticsManager.trackScreenOnSegment(SegmentAnalyticsManager.ISSUE_DETAILS_SCREEN, new Properties());

        View updateInfoBtn = findViewById(R.id.button_update_info);
        View markResolved = findViewById(R.id.marked_resolved);
        if (mIssueFromList.getStatus().toLowerCase().equals(getString(R.string.resolved).toLowerCase())) {
            updateInfoBtn.setVisibility(View.GONE);
            markResolved.setVisibility(View.GONE);
        } else {
            updateInfoBtn.setVisibility(userPermissions.isUpdateIssueEnabled() ? View.VISIBLE : View.GONE);
            markResolved.setVisibility(userPermissions.isResolveIssueEnabled() ? View.VISIBLE : View.GONE);
        }

        mSubscription.add(RxView.clicks(updateInfoBtn).throttleFirst(1, TimeUnit.SECONDS).subscribe(s -> {
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.ISSUE_UPDATE,
                    new Properties().putValue(SegmentAnalyticsManager.SOURCE_SCREEN,
                            isPreMarkedIssue ? SegmentAnalyticsManager.IS_PRE_MARKED_ISSUE : SegmentAnalyticsManager.ISSUE_DETAILS_SCREEN));
            mNavigator.navigateToIssueResolveActivity(this, mIssueFromList, false, mHotelId, mLoginManager.getUserId(), mTaskLogID);
        }));

        mSubscription.add(RxView.clicks(markResolved).throttleFirst(1, TimeUnit.SECONDS).subscribe(s -> {
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.ISSUE_RESOLVE,
                    new Properties().putValue(SegmentAnalyticsManager.SOURCE_SCREEN,
                            isPreMarkedIssue ? SegmentAnalyticsManager.IS_PRE_MARKED_ISSUE : SegmentAnalyticsManager.ISSUE_DETAILS_SCREEN));
            mNavigator.navigateToIssueResolveActivity(this, mIssueFromList, true, mHotelId, mLoginManager.getUserId(), mTaskLogID);
        }));


        mAppBarLayout.setBackgroundColor(IssueFormatUtils.getSeverityColor(this, mIssueFromList.getSeverity()));


        View backBtn = findViewById(R.id.issue_back_button);
        if (backBtn != null)
            backBtn.setOnClickListener(view -> finish());


        mDetailsPresenter.loadIssueDetails(mHotelId, mIssueFromList.getIssueID());

    }

    public void populateViews() {
        setToolBarColor(IssueFormatUtils.getSeverityColor(this, mIssueFromList.getSeverity()));
        mIssueSources.setText(IssueFormatUtils.getAllSources(mIssue));
        mIssueTime.setText(getString(R.string.due_date, mIssueFromList.getDueDateText()));
        mIssueTime.setTextColor(IssueFormatUtils.getSeverityColor(this, mIssueFromList.getSeverity()));
        mIssueIcon.setImageDrawable(IssueFormatUtils.getTintedIconForAuditType(this, mIssueFromList.getAuditCategoryID(), R.color.white));
        mIssueArea.setText(IssueFormatUtils.getAreaNameCheck(mIssueFromList));
        mIssCheckpoint.setText(mIssueFromList.getCategoryText() + "-" + mIssueFromList.getCheckpointText());
        mIssueSubCheckpoint.setText(mIssueFromList.getSubCheckpointText());
        mIssueCreatedDate.setText(DateUtils.getDisplayDateFromTimeStamp(mIssueFromList.getCreatedAt()));
    }

    @Override
    public void onBackPressed() {
        if (mAppBarLayout.getVisibility() == View.GONE)
            mAppBarLayout.setVisibility(View.VISIBLE);
        super.onBackPressed();
    }

    @Inject
    public void setUp() {
        mDetailsPresenter.setMvpView(this);
        mDetailsPresenter.setmUseCase(mIssueDetailsUseCase);
    }


    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        setToolBarColor(ContextCompat.getColor(this, R.color.activity_gray_background));
        mAppBarLayout.setVisibility(View.GONE);
        mLoaderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mAppBarLayout.setVisibility(View.VISIBLE);
        mLoaderView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Utils.ISSUES_OBJECT, mIssueFromList);
        outState.putParcelable(Utils.ISSUES_RESPONSE_OBJECT, mIssue);
        outState.putInt(Utils.HOTEL_ID, mHotelId);

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }


    public static Intent getCallingIntent(Activity activity) {
        return new Intent(activity, IssueDetailsActivity.class);
    }

    @Override
    public void onListEmpty() {

    }

    @Override
    public void imageItemClicked(int pos, int total, String imageUrl) {
        mAppBarLayout.setVisibility(View.GONE);
        addFragment(ImageShowFragment.newInstance(pos, total, imageUrl));
    }

    public void addFragment(BaseFragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.issue_detail_root_view, fragment, fragment.getClass()
                .getSimpleName())
                .addToBackStack(null).commit();
    }

    @Override
    public void onLoadIssueDetailsSuccess(IssueDetailResponse response) {
        mIssue = response.data;
        if (response.data.mIssueLog != null && !response.data.mIssueLog.isEmpty()) {
            mActionAdapter = new IssueActionAdapter(this, response.data.mIssueLog, this);
            mActionView.setAdapter(mActionAdapter);
            mActionAdapter.notifyDataSetChanged();
        }
        populateViews();
    }

    @Override
    public void onLoadIssueDetailsFailure(String msg) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.REQUEST_UPDATE_ISSUE:
                if (resultCode == RESULT_OK) {
                    UpdateIssueModelV3 updateModel = data.getExtras().getParcelable(Utils.UPDATED_ISSUE_OBJECT);
                    if (!TextUtils.isEmpty(updateModel.getResolutionDate())) {
                        mIssueTime.setText(getString(R.string.due_date, DateUtils.getIssueDueDate(updateModel.getResolutionDate())));
                        mIssueFromList.setResolutionDate(updateModel.getResolutionDate());
                    }
                    IssueDetailsV3.IssueLog issueLog = new IssueDetailsV3.IssueLog();
                    issueLog.mIssueResolutionDate = updateModel.getResolutionDate();
                    issueLog.mImageUrls = updateModel.getImageList();
                    issueLog.mIssueComments = updateModel.getComment();
                    issueLog.mUserId = Integer.toString(updateModel.getUserID());
                    issueLog.mUserName = mLoginManager.getUsername();
                    if (!TextUtils.isEmpty(updateModel.getComment()) && (updateModel.getImageList() != null || updateModel.getImageList().size() > 0)) {
                        Date date = new Date();
                        issueLog.mIssueUpdateDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
                        mIssue.mIssueLog.add(0, issueLog);
                        if (mActionAdapter == null) {
                            mActionAdapter = new IssueActionAdapter(this, mIssue.mIssueLog, this);
                            mActionView.setAdapter(mActionAdapter);
                        } else {
                            mActionAdapter.setmIssueLogs(mIssue.mIssueLog);
                        }
                        mActionAdapter.notifyDataSetChanged();
                    }
                    setResult(Utils.RESULT_UPDATE_ISSUE, data);
                }
                break;

            case Constants.REQUEST_RESOLVE_ISSUE:
                if (resultCode == RESULT_OK) {
                    setResult(Utils.RESULT_RESOLVE_ISSUE);
                    finish();
                }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }

    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(color);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }
}
