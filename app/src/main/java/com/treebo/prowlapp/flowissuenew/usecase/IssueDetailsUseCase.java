package com.treebo.prowlapp.flowissuenew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.issue.IssueDetailResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by sumandas on 17/11/2016.
 */

public class IssueDetailsUseCase extends UseCase<IssueDetailResponse> {

    RestClient mRestClient;

    private int mHotelId;
    private int mIssueId;

    public IssueDetailsUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setFields(int hotelId,int issueId) {
        mHotelId = hotelId;
        mIssueId =issueId;
    }

    @Override
    protected Observable<IssueDetailResponse> getObservable() {
        return mRestClient.getIssueDetails(mHotelId,mIssueId);
    }
}
