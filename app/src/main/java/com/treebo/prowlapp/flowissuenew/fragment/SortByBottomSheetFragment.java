package com.treebo.prowlapp.flowissuenew.fragment;

import android.app.Dialog;
import android.os.Bundle;

import android.view.View;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowissuenew.adapter.SortByAdapter;
import com.treebo.prowlapp.Models.issuemodel.SortByModel;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 16/11/16.
 */

public class SortByBottomSheetFragment extends BottomSheetDialogFragment
        implements SortByAdapter.onSortCriteriaClicked {


    private applySortOnIssueList mListener;
    private ArrayList<SortByModel> mList;

    public static SortByBottomSheetFragment newInstance(ArrayList<SortByModel> list) {
        SortByBottomSheetFragment fragment = new SortByBottomSheetFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(Utils.ISSUE_SORT_ORDER, list);
        fragment.setArguments(args);
        return fragment;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View rootView = View.inflate(getContext(), R.layout.layout_sort_by, null);
        dialog.setContentView(rootView);
        dialog.setOnShowListener(view -> {
            BottomSheetBehavior.from((View) rootView.getParent())
                    .setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) rootView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        mList = getArguments().getParcelableArrayList(Utils.ISSUE_SORT_ORDER);
        View closeBtn = rootView.findViewById(R.id.sort_by_bottom_sheet_close_icon);
        closeBtn.setOnClickListener(view -> dismiss());

        RecyclerView sortRv = (RecyclerView) rootView.findViewById(R.id.sort_by_rv);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext());
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        sortRv.setLayoutManager(layoutManager1);
        sortRv.setItemAnimator(new DefaultItemAnimator());

        SortByAdapter adapter = new SortByAdapter(getContext(), mList, this);
        sortRv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public interface applySortOnIssueList {
        void sortIssuesByCriteria(SortByModel sortCriteria);
    }

    public void setApplySortListener(applySortOnIssueList listener) {
        this.mListener = listener;
    }

    @Override
    public void sortListBy(SortByModel sortCriteria) {
        dismiss();
        mListener.sortIssuesByCriteria(sortCriteria);
    }
}
