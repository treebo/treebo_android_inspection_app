package com.treebo.prowlapp.flowissuenew.observer;

import com.treebo.prowlapp.flowissuenew.presenter.IssueListPresenter;
import com.treebo.prowlapp.response.issue.IssueListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 26/05/17.
 */

public class IssuesNotCaughtListObserver extends BaseObserver<IssueListResponse> {

    private IssueListPresenter mPresenter;

    public IssuesNotCaughtListObserver(IssueListPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        mPresenter = presenter;
    }

    @Override
    public void onCompleted() {
        mPresenter.resume();
    }

    @Override
    public void onNext(IssueListResponse issueListResponse) {
        if (issueListResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onLoadIssueListSuccess(issueListResponse);
        } else {
            mPresenter.onLoadIssueListFailure(issueListResponse.msg);
        }
    }
}
