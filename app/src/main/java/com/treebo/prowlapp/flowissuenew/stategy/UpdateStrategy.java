package com.treebo.prowlapp.flowissuenew.stategy;

import android.content.Context;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.Utils;

/**
 * Created by sumandas on 19/11/2016.
 */

public class UpdateStrategy implements IIssueStrategy {

    private Context mContext;

    @Override
    public int getResultCode() {
        return Utils.RESULT_UPDATE_ISSUE;
    }

    public UpdateStrategy(Context context){
        mContext=context;
    }

    @Override
    public String getScreenTitle() {
        return mContext.getResources().getString(R.string.update_info);
    }

    @Override
    public String getBottomButtonTitle() {
        return mContext.getResources().getString(R.string.update);
    }

    @Override
    public String getStatus() {
        return Utils.ISSUE_UPDATE;
    }
}
