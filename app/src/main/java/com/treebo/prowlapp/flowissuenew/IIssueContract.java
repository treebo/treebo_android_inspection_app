package com.treebo.prowlapp.flowissuenew;


import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.Models.issuemodel.UpdateIssueModelV3;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;
import com.treebo.prowlapp.response.issue.IssueDetailResponse;
import com.treebo.prowlapp.response.issue.IssueListResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 15/11/16.
 */

public interface IIssueContract {

    interface IIssueListView extends BaseView {
        void setUpRecyclerView();

        void updateRecyclerView(ArrayList<IssueListModelV3> issueList, int sortCriteria, boolean scrollToTop);

        void setResponseData(IssueListResponse response);
    }

    interface IIssueListPresenter extends BasePresenter {
        void onCreate();

        void loadIssueList(int hotelId);

        void loadIssuesNotCaught(int userID);

        void setAdapterData(ArrayList<IssueListModelV3> issueList, int sortCriteria);

        void onLoadIssueListSuccess(IssueListResponse response);

        void onLoadIssueListFailure(String msg);
    }

    interface IIssueDetailsView extends BaseView {
        void onLoadIssueDetailsSuccess(IssueDetailResponse response);

        void onLoadIssueDetailsFailure(String msg);
    }

    interface IIssueDetailsPresenter extends BasePresenter {
        void loadIssueDetails(int hotelId, int issueId);
    }

    interface IUpdateIssueView extends BaseView {
        void onUpdateIssueSuccess();

        void onUpdateIssueFailure(String msg);
    }

    interface IUpdateIssuePresenter extends BasePresenter {
        void postIssueUpdate(int hotelID, int issueID, UpdateIssueModelV3 issue);
    }

    interface IEscalateIssueView extends BaseView {
        void setUpUserList(EscalationUserListResponse response);

        void showSuccessScreen();
    }

    interface IEscalateIssuePresenter extends BasePresenter {
        void getEscalationUserDetails();

        void postEscalation(int hotelID, ArrayList<Integer> escalationUserList,
                            int userID, int issueID, String message);

        void populateUserList(EscalationUserListResponse response);

        void getListError(String msg);

        void handlePostSuccess();

        void handleAPIError(String msg);
    }
}
