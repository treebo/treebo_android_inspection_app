package com.treebo.prowlapp.flowissuenew.observer;

import com.treebo.prowlapp.flowissuenew.presenter.EscalateIssuePresenter;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 03/05/17.
 */

public class PostEscalationObserver extends BaseObserver<BaseResponse> {

    private EscalateIssuePresenter mPresenter;

    public PostEscalationObserver(EscalateIssuePresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(BaseResponse baseResponse) {
        if (baseResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.handlePostSuccess();
        } else {
            mPresenter.handleAPIError(baseResponse.msg);
        }
    }
}
