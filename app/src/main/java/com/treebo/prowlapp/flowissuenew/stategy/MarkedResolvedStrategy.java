package com.treebo.prowlapp.flowissuenew.stategy;

import android.content.Context;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.Utils;

/**
 * Created by sumandas on 19/11/2016.
 */

public class MarkedResolvedStrategy implements IIssueStrategy {

    private Context mContext;

    public MarkedResolvedStrategy(Context context){
        mContext=context;
    }


    @Override
    public int getResultCode() {
        return Utils.RESULT_RESOLVE_ISSUE;
    }

    @Override
    public String getStatus() {
        return Utils.ISSUE_CLOSE;
    }

    @Override
    public String getScreenTitle() {
        return mContext.getResources().getString(R.string.mark_resolved);
    }

    @Override
    public String getBottomButtonTitle() {
        return mContext.getResources().getString(R.string.resolve);
    }
}
