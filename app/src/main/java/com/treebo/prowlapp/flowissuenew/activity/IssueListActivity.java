package com.treebo.prowlapp.flowissuenew.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.Models.issuemodel.SectionedIssueListModel;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.IssueResolvedEventV3;
import com.treebo.prowlapp.events.PermissionsUpdateEvent;
import com.treebo.prowlapp.flowissuenew.DaggerIssueComponentNew;
import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.IssueComponentNew;
import com.treebo.prowlapp.flowissuenew.adapter.SectionedIssueListAdapter;
import com.treebo.prowlapp.flowissuenew.fragment.SortByBottomSheetFragment;
import com.treebo.prowlapp.flowissuenew.presenter.IssueListPresenter;
import com.treebo.prowlapp.flowissuenew.usecase.IssueListUseCase;
import com.treebo.prowlapp.flowissuenew.usecase.IssuesNotCaughtUseCase;
import com.treebo.prowlapp.Models.issuemodel.SortByModel;
import com.treebo.prowlapp.Models.issuemodel.UpdateIssueModelV3;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.response.issue.IssueListResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;
import com.tubb.smrv.SwipeMenuRecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 15/11/16.
 */

public class IssueListActivity extends AppCompatActivity implements IIssueContract.IIssueListView,
        SectionedIssueListAdapter.onIssueClickListener, SortByBottomSheetFragment.applySortOnIssueList {

    @Inject
    public IssueListPresenter mPresenter;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public RestClient mRestClient;

    @Inject
    public IssueListUseCase mUseCase;

    @Inject
    public IssuesNotCaughtUseCase mIncUseCase;

    @Inject
    public Navigator mNavigator;

    @Inject
    public RxBus mRxBus;

    private int mHotelId;
    private String mHotelName;

    public IssueListModelV3 mIssueClicked;
    int mSortCriteria;

    private ArrayList<IssueListModelV3> mIssueList = new ArrayList<>();
    private ArrayList<SectionedIssueListModel> mSectionedIssueList = new ArrayList<>();

    private SwipeMenuRecyclerView mIssueListRecyclerView;
    private SectionedIssueListAdapter mAdapter;
    private TreeboTextView mSortAppliedTv;
    private ImageView mSortImage;
    private View mLoadingView;
    private RolesAndUserPermissions.PermissionList mUserPermissions;

    private SortByBottomSheetFragment mSortByBottomSheetFragment;
    private TreeboTextView mSortCriteriaText;

    private ArrayList<SortByModel> mSortCategories = Utils.getAllSortCriterion();
    private int mTaskLogID;
    private boolean isIncList;

    private final CompositeSubscription mSubscription = new CompositeSubscription();


    public static Intent getCallingIntent(Activity activity) {
        return new Intent(activity, IssueListActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_list);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }
        IssueComponentNew issueComponent = DaggerIssueComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        issueComponent.injectIssueListActivity(this);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mSortCriteria = intent.getIntExtra(Utils.ISSUE_SORT_ORDER, -1);
            mHotelId = intent.getIntExtra(Utils.HOTEL_ID, -1);
            mTaskLogID = intent.getIntExtra(Utils.TASK_LOG_ID, 0);
            mHotelName = intent.getStringExtra(Utils.HOTEL_NAME);
            isIncList = intent.getBooleanExtra(Utils.IS_INC_LIST, false);
        } else {
            mSortCriteria = savedInstanceState.getInt(Utils.ISSUE_SORT_ORDER, -1);
            mHotelId = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            mHotelName = savedInstanceState.getString(Utils.HOTEL_NAME);
            mTaskLogID = savedInstanceState.getInt(Utils.TASK_LOG_ID, 0);
            mIssueClicked = savedInstanceState.getParcelable(Utils.ISSUE_CLICKED);
            isIncList = savedInstanceState.getBoolean(Utils.IS_INC_LIST);
        }
        mUserPermissions = mLoginManager.getUserPermissionList(mHotelId);
        View backBtn = findViewById(R.id.toolbar_issues_back_btn);
        if (backBtn != null)
            backBtn.setOnClickListener(view -> finish());

        mIssueListRecyclerView = (SwipeMenuRecyclerView) findViewById(R.id.issues_recycler_view);

        View sortByView = findViewById(R.id.sort_by_rl);
        sortByView.setOnClickListener(view ->
                showSortBottomSheet());
        mSortImage = (ImageView) sortByView.findViewById(R.id.sort_by_icon);
        mSortCriteriaText = (TreeboTextView) sortByView.findViewById(R.id.sort_criteria_text);

        TreeboTextView titleText = (TreeboTextView) findViewById(R.id.toolbar_issues_title_tv);
        titleText.setText(isIncList ? getString(R.string.issue_not_caught) : getString(R.string.issues));

        mLoadingView = findViewById(R.id.loader_layout);
        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));

        mPresenter.onCreate();

        if (isIncList)
            mPresenter.loadIssuesNotCaught(mLoginManager.getUserId());
        else
            mPresenter.loadIssueList(mHotelId);

        mIssueListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                    sortByView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof PermissionsUpdateEvent) {
                mUserPermissions = mLoginManager.getUserPermissionList(mHotelId);
                mAdapter.setmUserPermissions(mUserPermissions);
                mAdapter.notifyDataSetChanged();
                mPresenter.updateViewWithPermissions();
            }
        }));

    }

    private void showSortBottomSheet() {
        mSortByBottomSheetFragment = SortByBottomSheetFragment.newInstance(mSortCategories);
        mSortByBottomSheetFragment.setApplySortListener(this);
        mSortByBottomSheetFragment.show(getSupportFragmentManager(), null);
    }

    private void showEscalateIssueDialog(IssueListModelV3 issue) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_unmark_issues, null);
        TreeboTextView titleText = (TreeboTextView) dialogView.findViewById(R.id.checkpoint_name_tv);
        titleText.setText(getString(R.string.issue_heading,
                issue.getCategoryText(), issue.getCheckpointText()));
        TreeboTextView subtitleText = (TreeboTextView) dialogView.findViewById(R.id.unmark_text_tv);
        subtitleText.setText(getString(R.string.escalate_issue));

        dialogBuilder.setView(dialogView);
        final AlertDialog unmarkIssueDialog = dialogBuilder.create();
        unmarkIssueDialog.show();

        View closeBtn = dialogView.findViewById(R.id.close_iv);
        closeBtn.setOnClickListener(view -> unmarkIssueDialog.dismiss());
        subtitleText.setOnClickListener(view -> {
            mNavigator.navigateToEscalateIssueScreen(this, issue, mHotelId, mHotelName);
            mAdapter = new SectionedIssueListAdapter(this, mSectionedIssueList, isIncList, this, mUserPermissions);
            mIssueListRecyclerView.setAdapter(mAdapter);
            unmarkIssueDialog.dismiss();
        });

    }


    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setIssueListUseCase(mUseCase);
        mPresenter.setIssueNotCaughtUseCase(mIncUseCase);

    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void issueClicked(IssueListModelV3 issue) {
        mIssueClicked = issue;
        if (Utils.isInternetConnected(this))
            mNavigator.navigateToIssueDetailsActivity(this, issue, mHotelId,
                    Constants.REQUEST_ISSUE_DETAILS, mTaskLogID, false);
        else
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void issueUpdateResolveClicked(IssueListModelV3 issue, boolean isResolved) {
        mIssueClicked = issue;
        SegmentAnalyticsManager.trackEventOnSegment(isResolved ? SegmentAnalyticsManager.ISSUE_RESOLVE : SegmentAnalyticsManager.ISSUE_UPDATE,
                new Properties().putValue(SegmentAnalyticsManager.SOURCE_SCREEN, SegmentAnalyticsManager.ISSUE_LIST_SCREEN));
        mNavigator.navigateToIssueResolveActivity(this, issue, isResolved, mHotelId, mLoginManager.getUserId(), mTaskLogID);
    }

    @Override
    public void onLongClicked(IssueListModelV3 issue) {
        if (mUserPermissions.isEscalateIssueEnabled()
                && !issue.getStatus().toLowerCase().equals(getString(R.string.resolved).toLowerCase()))
            showEscalateIssueDialog(issue);
    }


    @Override
    public void setUpRecyclerView() {
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mIssueListRecyclerView.setLayoutManager(layoutManager1);
        mIssueListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new SectionedIssueListAdapter(this, mSectionedIssueList, isIncList, this, mUserPermissions);
        mIssueListRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void updateRecyclerView(ArrayList<IssueListModelV3> issueList, int sortCriteria, boolean scrollToTop) {
        mSortCriteria = sortCriteria;
        populateCheckpointSubCheckpointTextFields(sortCriteria);
        getSectionedListGivenSortOrder(mIssueList, sortCriteria);
        if (sortCriteria == Utils.SEVERITY_HIGH_LOW || sortCriteria == Utils.DATE_NEW_OLD)
            mAdapter.reverseListOrder();
        else
            mAdapter.notifyDataSetChanged();
        if (scrollToTop)
            mIssueListRecyclerView.smoothScrollToPosition(0);
    }

    @Override
    public void setResponseData(IssueListResponse response) {
        if (mSortCriteria == -1)
            mSortCriteria = Utils.SEVERITY_HIGH_LOW;
        mIssueList = response.data;
        mPresenter.setAdapterData(response.data, mSortCriteria);
    }

    private void populateCheckpointSubCheckpointTextFields(int sortCriteria) {
        for (IssueListModelV3 issue : mIssueList) {
            issue.setSectionName(sortCriteria);
            issue.setDueDateText();
        }
    }

    private void getSectionedListGivenSortOrder(ArrayList<IssueListModelV3> issueList, int sortCriteria) {
        Set<String> sectionNames = Utils.getUniqueValues(issueList);
        ArrayList<String> listSection = new ArrayList<>();
        if (sortCriteria == Utils.SEVERITY_HIGH_LOW || sortCriteria == Utils.SEVERITY_LOW_HIGH) {
            Set<Map.Entry<String, Integer>> entrySet = Utils.getSeverityPriorityHashMap();
            List<Map.Entry<String, Integer>> list = new ArrayList<>(entrySet);
            Collections.sort(list, (o1, o2) -> (o2.getValue()).compareTo(o1.getValue()));
            for (Map.Entry<String, Integer> entry : list) {
                listSection.add(entry.getKey());
            }
            Collections.sort(mIssueList, (o1, o2) -> String.valueOf(o1.getResolutionDate()).compareTo(String.valueOf(o2.getResolutionDate())));
        } else {
            listSection = new ArrayList<>(sectionNames);
            Collections.sort(listSection);
            if (sortCriteria == Utils.RESOLUTION_DATE && listSection.contains("Future Dues")) {
                Collections.rotate(listSection, -1);
                Collections.sort(mIssueList, (o1, o2) -> String.valueOf(o1.getResolutionDate()).compareTo(String.valueOf(o2.getResolutionDate())));
            }  else {
                Collections.sort(mIssueList, (o1, o2) -> String.valueOf(o1.getSeverity()).compareTo(String.valueOf(o2.getSeverity())));
            }
        }
        formSectionedList(listSection);
    }

    private void formSectionedList(ArrayList<String> sectionNames) {
        mSectionedIssueList.clear();
        for (String section : sectionNames) {
            SectionedIssueListModel model = new SectionedIssueListModel();
            model.setSectionName(section);
            mSectionedIssueList.add(model);
        }
        for (IssueListModelV3 issue : mIssueList) {
            for (SectionedIssueListModel section : mSectionedIssueList) {
                if (issue.getSectionName() != null && issue.getSectionName().equals(section.getSectionName())) {
                    section.setIssueInList(issue);
                    break;
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.REQUEST_ISSUE_DETAILS:
                if (resultCode == Utils.RESULT_RESOLVE_ISSUE) {
                    mIssueList.remove(mIssueClicked);
                    updateRecyclerView(mIssueList, mSortCriteria, false);
                    postResolvedIssueEvent();
                }
                if (resultCode == Utils.RESULT_UPDATE_ISSUE) {
                    updateIssue(data.getExtras().getParcelable(Utils.UPDATED_ISSUE_OBJECT));
                }
                break;
            case Constants.REQUEST_RESOLVE_ISSUE:
                if (resultCode == RESULT_OK) {
                    mIssueList.remove(mIssueClicked);
                    updateRecyclerView(mIssueList, mSortCriteria, false);
                    postResolvedIssueEvent();
                }
                break;
            case Constants.REQUEST_UPDATE_ISSUE:
                if (resultCode == RESULT_OK) {
                    updateIssue(data.getExtras().getParcelable(Utils.UPDATED_ISSUE_OBJECT));
                }
                break;
        }
    }

    private void postResolvedIssueEvent() {
        populateCheckpointSubCheckpointTextFields(Utils.RESOLUTION_DATE);
        int overdueIssueCount = 0;
        int todaysIssueCount = 0;
        for (IssueListModelV3 issue : mIssueList) {
            if (issue.getSectionName().equals(getString(R.string.overdue)))
                overdueIssueCount++;
            else if (issue.getSectionName().equals(getString(R.string.resolve_today)))
                todaysIssueCount++;
        }
        IssueResolvedEventV3 event = new IssueResolvedEventV3(overdueIssueCount, todaysIssueCount);
        mRxBus.postEvent(event);
    }

    public void updateIssue(UpdateIssueModelV3 issue) {
        String issueDate = issue.getResolutionDate();
        if (!TextUtils.isEmpty(issueDate)) {//do not update unnecessarily if no change in resolution date
            mIssueClicked.setResolutionDate(issueDate);
            updateRecyclerView(mIssueList, mSortCriteria, false);
        }
    }

    @Override
    public void sortIssuesByCriteria(SortByModel sortCriteria) {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SORT_CLICK,
                new Properties().putValue(SegmentAnalyticsManager.SORT_CRITERIA, sortCriteria.sortTitle));
        mSortCriteriaText.setText(sortCriteria.sortTitle);
        mSortCriteriaText.setVisibility(View.VISIBLE);
        updateSortCategoryList(sortCriteria);
        mPresenter.setAdapterData(mIssueList, sortCriteria.sortCriteriaId);
    }

    private void updateSortCategoryList(SortByModel sortCriteria) {
        for (SortByModel sort : mSortCategories) {
            sort.isSelected = (sort.sortCriteriaId == sortCriteria.sortCriteriaId);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(Utils.ISSUE_SORT_ORDER, mSortCriteria);
        outState.putInt(Utils.HOTEL_ID, mHotelId);
        outState.putInt(Utils.TASK_LOG_ID, mTaskLogID);
        outState.putParcelable(Utils.ISSUE_CLICKED, mIssueClicked);
        super.onSaveInstanceState(outState);
    }
}
