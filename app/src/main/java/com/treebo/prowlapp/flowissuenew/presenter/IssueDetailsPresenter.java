package com.treebo.prowlapp.flowissuenew.presenter;

import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.observer.IssueDetailsObserver;
import com.treebo.prowlapp.flowissuenew.usecase.IssueDetailsUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 15/11/16.
 */

public class IssueDetailsPresenter implements IIssueContract.IIssueDetailsPresenter {

    private IIssueContract.IIssueDetailsView mView;


    private IssueDetailsUseCase mUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IIssueContract.IIssueDetailsView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void loadIssueDetails(int hotelId, int issueId) {
        mView.showLoading();
        mUseCase.setFields(hotelId, issueId);
        mUseCase.execute(providesIssueDetailsObserver());
    }

    public void setmUseCase(IssueDetailsUseCase useCase) {
        mUseCase = useCase;
    }


    IssueDetailsObserver providesIssueDetailsObserver() {
        return new IssueDetailsObserver(this, mUseCase, mView);
    }
}
