package com.treebo.prowlapp.flowissuenew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.issuemodel.SortByModel;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 16/11/16.
 */

public class SortByAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<SortByModel> mSortByList;
    private Context mContext;
    private onSortCriteriaClicked mListener;

    public SortByAdapter(Context context, ArrayList<SortByModel> list, onSortCriteriaClicked listener) {
        this.mContext = context;
        this.mSortByList = list;
        this.mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sort_by,
                parent, false);
        return new SortByViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SortByModel criteria = mSortByList.get(position);
        ((SortByViewHolder) holder).sortCriteria = criteria;
        ((SortByViewHolder) holder).text.setText(criteria.sortTitle);
        if (criteria.isSelected)
            ((SortByViewHolder) holder).text.setTextColor(ContextCompat
                    .getColor(mContext, R.color.green_color_primary));
        else
            ((SortByViewHolder) holder).text.setTextColor(ContextCompat
                    .getColor(mContext, R.color.text_color_primary));
    }

    @Override
    public int getItemCount() {
        return mSortByList.size();
    }

    private class SortByViewHolder extends RecyclerView.ViewHolder {

        private SortByModel sortCriteria;
        private TreeboTextView text;

        public SortByViewHolder(View itemView, onSortCriteriaClicked listener) {
            super(itemView);
            text = (TreeboTextView) itemView.findViewById(R.id.sort_by_title_tv);
            itemView.setOnClickListener(view -> {
                listener.sortListBy(sortCriteria);

            });
        }
    }

    public interface onSortCriteriaClicked {
        void sortListBy(SortByModel sortCriteria);
    }
}
