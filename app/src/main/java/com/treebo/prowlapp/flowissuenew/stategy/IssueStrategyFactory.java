package com.treebo.prowlapp.flowissuenew.stategy;

import android.content.Context;

/**
 * Created by sumandas on 19/11/2016.
 */

public class IssueStrategyFactory {

    public static IIssueStrategy getIssueStrategy(Context context,boolean isResolveIssue){
        if(isResolveIssue){
            return new MarkedResolvedStrategy(context);
        }else{
            return new UpdateStrategy(context);
        }
    }

}
