package com.treebo.prowlapp.flowissuenew.observer;

import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.usecase.IssueDetailsUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.issue.IssueDetailResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by sumandas on 17/11/2016.
 */

public class IssueDetailsObserver extends BaseObserver<IssueDetailResponse> {

    private IIssueContract.IIssueDetailsView mView;
    private IssueDetailsUseCase mUseCase;

    public IssueDetailsObserver(BasePresenter presenter, IssueDetailsUseCase useCase,
                                IIssueContract.IIssueDetailsView view) {
        super(presenter, useCase);
        mView = view;
        mUseCase = useCase;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(IssueDetailResponse response) {
        mView.hideLoading();
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mView.onLoadIssueDetailsSuccess(response);
        } else {
            mView.onLoadIssueDetailsFailure("Loading issue details failed");
        }
    }
}
