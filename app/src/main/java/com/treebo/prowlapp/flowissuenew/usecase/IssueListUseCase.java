package com.treebo.prowlapp.flowissuenew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.issue.IssueListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 16/11/16.
 */

public class IssueListUseCase extends UseCase<IssueListResponse> {

    RestClient mRestClient;

    private int mHotelId;

    public IssueListUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setHotelId(int hotelId) {
        mHotelId = hotelId;
    }

    @Override
    protected Observable<IssueListResponse> getObservable() {
        return mRestClient.getIssueList(mHotelId);
    }
}
