package com.treebo.prowlapp.flowissuenew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Models.issuemodel.SectionedIssueListModel;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.IssueFormatUtils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;
import com.tubb.smrv.SwipeHorizontalMenuLayout;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by abhisheknair on 15/11/16.
 */

public class SectionedIssueListAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private ArrayList<SectionedIssueListModel> mList;
    private onIssueClickListener mListener;
    private Context mContext;
    private RolesAndUserPermissions.PermissionList mUserPermissions;
    private boolean isIncList;

    public SectionedIssueListAdapter(Context context, ArrayList<SectionedIssueListModel> list,
                                     boolean isIncList,
                                     onIssueClickListener listener,
                                     RolesAndUserPermissions.PermissionList permissions) {
        this.mContext = context;
        this.mList = list;
        mUserPermissions = permissions;
        this.isIncList = isIncList;
        this.mListener = listener;
    }

    public void reverseListOrder() {
        Collections.reverse(mList);
        notifyDataSetChanged();
    }

    public void setmUserPermissions(RolesAndUserPermissions.PermissionList permissions) {
        this.mUserPermissions = permissions;
    }

    @Override
    public int getSectionCount() {
        if (mList == null)
            return 0;
        else
            return mList.size();
    }

    @Override
    public int getItemCount(int i) {
        if (mList == null)
            return 0;
        else
            return mList.get(i).getIssueList().size();
    }


    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        SectionedIssueListModel section = mList.get(i);
        String sectionName;
        switch (section.getSectionName()) {
            case "0":
                sectionName = mContext.getString(R.string.common_area);
                break;

            case "null":
                sectionName = mContext.getString(R.string.others);
                break;

            default:
                sectionName = section.getSectionName();
                break;
        }
        ((IssueSectionViewHolder) viewHolder).sectionTitle.setText(sectionName);
        ((IssueSectionViewHolder) viewHolder).sectionCount.setText(String.valueOf(section.getIssueList().size()));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int section, int i1, int i2) {
        IssueListModelV3 issue = mList.get(section).getIssueList().get(i1);
        ((IssuesViewHolder) viewHolder).issue = issue;
        ((IssuesViewHolder) viewHolder).dueDate.setText(mContext.getString(R.string.due_date, issue.getDueDateText()));
        ((IssuesViewHolder) viewHolder).icon.setImageDrawable(IssueFormatUtils.getTintedIconForAuditType(mContext,
                issue.getAuditCategoryID(), R.color.warm_grey_2));
        ((IssuesViewHolder) viewHolder).isDnr.setVisibility(issue.isDnr() ? View.VISIBLE : View.GONE);
        ((IssuesViewHolder) viewHolder).isIssueNotCaught.setVisibility(issue.issueNotCaught() ? View.VISIBLE : View.GONE);
        ((IssuesViewHolder) viewHolder).hotelName.setText(issue.getHotelName());
        ((IssuesViewHolder) viewHolder).hotelName.setVisibility(isIncList ? View.VISIBLE : View.GONE);
        setProblemAreaText(((IssuesViewHolder) viewHolder).roomId, issue);
        setDueDateText(((IssuesViewHolder) viewHolder).dueDate, issue.isOverdue());
        setSubcheckpointTextAsPerSeverity(((IssuesViewHolder) viewHolder).subHeadingText, issue);
        ((IssuesViewHolder) viewHolder).headingText.setText(mContext.getString(R.string.issue_heading,
                issue.getCategoryText(), issue.getCheckpointText()));
        ((IssuesViewHolder) viewHolder).issueUpdate.setOnClickListener(view -> {
            mListener.issueUpdateResolveClicked(issue, false);
            ((SwipeHorizontalMenuLayout) viewHolder.itemView).smoothCloseMenu();
        });
        ((IssuesViewHolder) viewHolder).issueResolved.setOnClickListener(view -> {
            mListener.issueUpdateResolveClicked(issue, true);
            ((SwipeHorizontalMenuLayout) viewHolder.itemView).smoothCloseMenu();
        });
        ((SwipeHorizontalMenuLayout) viewHolder.itemView).setSwipeEnable(mUserPermissions.isSwipeIssueEnabled());
        ((IssuesViewHolder) viewHolder).issueResolved.setVisibility(mUserPermissions.isSwipeResolveIssueEnabled()
                ? View.VISIBLE : View.GONE);
        ((IssuesViewHolder) viewHolder).issueUpdate.setVisibility(mUserPermissions.isSwipeUpdateIssueEnabled()
                ? View.VISIBLE : View.GONE);
        if (issue.getStatus().toLowerCase().equals(mContext.getString(R.string.resolved).toLowerCase())) {
            ((SwipeHorizontalMenuLayout) viewHolder.itemView).setSwipeEnable(false);
        }
    }

    private void setProblemAreaText(TreeboTextView roomTv, IssueListModelV3 issue) {
        switch (AuditCategory.getAuditCategoryConstantByID(issue.getAuditCategoryID())) {
            case Constants.ROOM_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.room_number_placeholder, issue.getRoomNumber()));
                break;

            case Constants.COMMON_AREA_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.common_area));
                break;

            case Constants.BREAKFAST_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.breakfast_issue));
                break;

            case Constants.FRAUD_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.fraud_issue));
                break;

            case Constants.KITCHEN_PANTRY_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.kitchen_pantry_issue));
                break;

            case Constants.FnB_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.fnb_issue));
                break;

            case Constants.RESTAURANT_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.restaurant_issue));
                break;

            case Constants.RESTAURANT_WASHROOM_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.restaurant_washroom_issue));
                break;

            case Constants.BACK_OF_THE_HOUSE_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.back_of_the_house_issue));
                break;

            case Constants.GYM_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.gym_issue));
                break;

            case Constants.SPA_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.spa_issue));
                break;

            case Constants.POOL_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.pool_issue));
                break;

            case Constants.POLICY_AND_CONDUCT_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.policy_conduct_issue));
                break;

            case Constants.ACCO_FOOD_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.acco_food_issue));
                break;

            case Constants.STORAGE_SPACE_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.storage_space_issue));
                break;

            case Constants.SAFETY_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.safety_issue));
                break;

            case Constants.GARBAGE_MANAGEMENT_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.garbage_management_issue));
                break;

            case Constants.WATER_STORAGE_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.water_storage_issue));
                break;

            case Constants.DISABLED_FRIENDLY_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.disabled_friendly_issue));
                break;

            case Constants.HK_TOOLS_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.house_keeping_tools_issue));
                break;

            case Constants.FRONT_OFFICE_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.front_office_tools_issue));
                break;

            case Constants.LUXURY_AMENITIES_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.luxury_amenities_issue));
                break;

            case Constants.CLOAK_ROOM_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.cloak_room_issue));
                break;

            case Constants.DRIVERS_QUARTERS_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.drivers_quarters_issue));
                break;

            case Constants.PARKING_AREA_STRING:
                roomTv.setText(mContext.getString(R.string.parking_areas_issue));
                break;

            case Constants.IN_HOUSE_LAUNDRY_AUDIT_STRING:
                roomTv.setText(mContext.getString(R.string.in_house_laundry_issue));
                break;

            default:
                break;
        }
    }


    private void setSubcheckpointTextAsPerSeverity(TreeboTextView subHeadingText, IssueListModelV3 issue) {
        switch (issue.getSeverity()) {
            case Constants.HIGH_SEVERITY:
                subHeadingText.setTextColor(ContextCompat.getColor(mContext, R.color.warning_red));
                break;

            case Constants.MEDIUM_SEVERITY:
                subHeadingText.setTextColor(ContextCompat.getColor(mContext, R.color.warning_orange));
                break;

            case Constants.LOW_SEVERITY:
            default:
                subHeadingText.setTextColor(ContextCompat.getColor(mContext, R.color.warning_yellow));
                break;

        }
        subHeadingText.setText(issue.getSubCheckpointText());
    }

    private void setDueDateText(TreeboTextView dueDate, boolean isOverdue) {
        if (isOverdue) {
            dueDate.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_rounded_rectangle_red));
            dueDate.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        } else {
            dueDate.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_rounded_rectangle_gray));
            dueDate.setTextColor(ContextCompat.getColor(mContext, R.color.text_color_primary));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_issue_swipe_layout,
                    parent, false);
            return new IssuesViewHolder(view, mListener);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_issue_section_header,
                    parent, false);
            return new IssueSectionViewHolder(view);
        }
    }

    private class IssueSectionViewHolder extends RecyclerView.ViewHolder {

        TreeboTextView sectionTitle;
        TreeboTextView sectionCount;

        IssueSectionViewHolder(View itemView) {
            super(itemView);
            sectionTitle = (TreeboTextView) itemView.findViewById(R.id.issue_section_header_title);
            sectionCount = (TreeboTextView) itemView.findViewById(R.id.issue_section_item_count);
        }
    }

    private class IssuesViewHolder extends RecyclerView.ViewHolder {

        IssueListModelV3 issue;
        TreeboTextView dueDate;
        TreeboTextView headingText;
        TreeboTextView subHeadingText;
        TreeboTextView roomId;
        TreeboTextView hotelName;
        ImageView icon;
        View issueView;
        TreeboButton issueUpdate;
        TreeboButton issueResolved;
        View isIssueNotCaught;
        View isDnr;


        IssuesViewHolder(View itemView, onIssueClickListener listener) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.issue_icon);
            roomId = (TreeboTextView) itemView.findViewById(R.id.issue_room_number_tv);
            dueDate = (TreeboTextView) itemView.findViewById(R.id.issue_due_date);
            headingText = (TreeboTextView) itemView.findViewById(R.id.issue_checkpoint_tv);
            subHeadingText = (TreeboTextView) itemView.findViewById(R.id.issue_subcheckpoint_tv);
            hotelName = (TreeboTextView) itemView.findViewById(R.id.issue_hotelname_tv);
            issueUpdate = (TreeboButton) itemView.findViewById(R.id.swipe_update);
            issueResolved = (TreeboButton) itemView.findViewById(R.id.swipe_resolve);
            issueView = itemView.findViewById(R.id.smContentView);
            issueView.setOnClickListener(view -> listener.issueClicked(issue));
            isIssueNotCaught = itemView.findViewById(R.id.issue_inc);
            isDnr = itemView.findViewById(R.id.issue_dnr);
            issueView.setOnLongClickListener(view -> {
                listener.onLongClicked(issue);
                return true;
            });

        }
    }

    public interface onIssueClickListener {
        void issueClicked(IssueListModelV3 issue);

        void issueUpdateResolveClicked(IssueListModelV3 issue, boolean isResolved);

        void onLongClicked(IssueListModelV3 issue);
    }
}
