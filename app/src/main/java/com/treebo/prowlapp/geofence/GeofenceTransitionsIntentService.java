/**
 * Copyright 2014 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.treebo.prowlapp.geofence;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

public class GeofenceTransitionsIntentService extends IntentService {

    protected static final String TAG = "GeofenceTransitions";


    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public RxBus mRxBus;

    public GeofenceTransitionsIntentService() {
        // Use the TAG to name the worker thread.
        super(TAG);
        MainApplication.get().getApplicationComponent().injectGoefenceService(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceUtils.getErrorString(this,
                    geofencingEvent.getErrorCode());
            Log.e(TAG, errorMessage);
            return;
        }

        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT || geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();
            String geofenceTransitionDetails = getGeofenceTransitionDetails(
                    this,
                    geofenceTransition,
                    triggeringGeofences
            );
            getHotelDetails();
            Log.i(TAG, geofenceTransitionDetails);
        }
    }

    private String getGeofenceTransitionDetails(
            Context context,
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        ArrayList triggeringGeofencesIdsList = new ArrayList();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ", triggeringGeofencesIdsList);

        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
    }

    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofence_transition_entered);
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);
            default:
                return getString(R.string.unknown_geofence_transition);
        }
    }

    private void getHotelDetails() {
        ArrayList<HotelLocation> hotelList = HotelLocation.getHotelForLocation(mLoginManager.getLatitude(), mLoginManager.getLongitude());
        if (hotelList.size() > 0) {
            if (hotelList.size() == 1) {
                HotelLocation detectedHotel = hotelList.get(0);
                Log.d("GeofenceTransition", detectedHotel.mHotelName + " " + detectedHotel.mHotelId);
                mLoginManager.setCurrentHotelLocationId(detectedHotel.mHotelId);
                mLoginManager.setCurrentHotelLocationName(detectedHotel.mHotelName);
                mRxBus.postEvent(new HotelChangedEvent(hotelList));
            } else {
                Log.d("GeofenceTransition", "Multiple Hotels Detected: " + hotelList.toString());
                HashSet<String> idSet = new HashSet<>();
                for (HotelLocation location: hotelList) {
                    idSet.add(String.valueOf(location.mHotelId));
                }
                mLoginManager.setMultipleCurrentHotelLocationIDs(idSet);
                mRxBus.postEvent(new HotelChangedEvent(hotelList));
            }
        } else {
            Log.d("GeofenceTransition", "outside hotel");
            mLoginManager.setCurrentHotelLocationId(-1);
            mRxBus.postEvent(new HotelChangedEvent(hotelList));
        }
    }
}
