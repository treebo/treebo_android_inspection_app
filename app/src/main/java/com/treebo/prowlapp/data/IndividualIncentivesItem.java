package com.treebo.prowlapp.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by devesh on 14/04/16.
 */

public class IndividualIncentivesItem extends IncentivesItem implements Parcelable,Serializable {
    public String heading;
    public List<IndividualEntry> individualEntryList;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.heading);
        dest.writeList(this.individualEntryList);
    }

    public IndividualIncentivesItem() {
    }

    protected IndividualIncentivesItem(Parcel in) {
        this.heading = in.readString();
        this.individualEntryList = new ArrayList<>();
        in.readList(this.individualEntryList, IndividualEntry.class.getClassLoader());
    }

    public static final Creator<IndividualIncentivesItem> CREATOR = new Creator<IndividualIncentivesItem>() {
        @Override
        public IndividualIncentivesItem createFromParcel(Parcel source) {
            return new IndividualIncentivesItem(source);
        }

        @Override
        public IndividualIncentivesItem[] newArray(int size) {
            return new IndividualIncentivesItem[size];
        }
    };

    @Override
    public String toString() {
        return "IndividualIncentivesItem{" +
                "heading='" + heading + '\'' +
                ", individualEntryList=" + individualEntryList +
                '}';
    }
}
