package com.treebo.prowlapp.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sakshamdhawan on 01/05/16.
 */
public class DepartmentInformation implements Parcelable {

    public String departmant_name;
    public int todaysChanges;
    public int score;
    public int till_date;


    public DepartmentInformation(){}

    public DepartmentInformation(int todaysChanges, int score, int till_date, String departmant_name){
        this.departmant_name =departmant_name;
        this.todaysChanges=todaysChanges;
        this.score=score;
        this.till_date=till_date;

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.departmant_name);
        dest.writeInt(this.todaysChanges);
        dest.writeInt(this.score);
        dest.writeInt(this.till_date);
    }

    protected DepartmentInformation(Parcel in) {
        this.departmant_name = in.readString();
        this.todaysChanges = in.readInt();
        this.score = in.readInt();
        this.till_date = in.readInt();
    }

    public static final Creator<DepartmentInformation> CREATOR = new Creator<DepartmentInformation>() {
        @Override
        public DepartmentInformation createFromParcel(Parcel source) {
            return new DepartmentInformation(source);
        }

        @Override
        public DepartmentInformation[] newArray(int size) {
            return new DepartmentInformation[size];
        }
    };

    @Override
    public String toString() {
        return "DepartmentInformation{" +
                "departmant_name='" + departmant_name + '\'' +
                ", todaysChanges=" + todaysChanges +
                ", score=" + score +
                ", till_date=" + till_date +
                '}';
    }
}
