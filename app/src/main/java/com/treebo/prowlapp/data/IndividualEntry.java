package com.treebo.prowlapp.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by devesh on 14/04/16.
 */
public class IndividualEntry implements Parcelable {
/*
    public IndividualEntry(String first_name,String last_name, String salary, String sia, String cv, String tia, String staff_url) {
        this.first_name=first_name;
        this.last_name =last_name;
        this.salary = salary;
        this.sia = sia;
        this.cv = cv;
        this.tia = tia;
        this.staff_url = staff_url;
    }
*/

    @SerializedName("first_name")
    public String first_name;

    @SerializedName("last_name")
    public String last_name;

    @SerializedName("salary")
    public String salary;

    @SerializedName("sia")
    public String sia;

    @SerializedName("cv")
    public String cv;

    @SerializedName("tia")
    public String tia;

    @SerializedName("staff_url")
    public String staff_url;

    @SerializedName("staff_id")
    public String staff_id;

    @SerializedName("has_joined_this_month")
    public boolean has_joined_this_month;

    @SerializedName("got_audited_this_month")
    public boolean got_audited_this_month;

    @SerializedName("monthly_incentive")
    public MonthlyIncentiveModel monthly_incentive;


    public static class MonthlyIncentive implements Parcelable{
        @SerializedName("month")
        public String month;

        @SerializedName("incentive")
        public String incentive;

        @SerializedName("starting_incentive")
        public String starting_incentive;


        public MonthlyIncentive() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.month);
            dest.writeString(this.incentive);
            dest.writeString(this.starting_incentive);
        }

        protected MonthlyIncentive(Parcel in) {
            this.month = in.readString();
            this.incentive = in.readString();
            this.starting_incentive = in.readString();
        }

        public static final Creator<MonthlyIncentive> CREATOR = new Creator<MonthlyIncentive>() {
            @Override
            public MonthlyIncentive createFromParcel(Parcel source) {
                return new MonthlyIncentive(source);
            }

            @Override
            public MonthlyIncentive[] newArray(int size) {
                return new MonthlyIncentive[size];
            }
        };
    }

    public static class MonthlyIncentiveModel implements Parcelable{
        public ArrayList<MonthlyIncentive> incentive_list;
        public boolean is_paid;

        public MonthlyIncentiveModel() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(this.incentive_list);
            dest.writeByte(this.is_paid ? (byte) 1 : (byte) 0);
        }

        protected MonthlyIncentiveModel(Parcel in) {
            this.incentive_list = in.createTypedArrayList(MonthlyIncentive.CREATOR);
            this.is_paid = in.readByte() != 0;
        }

        public static final Creator<MonthlyIncentiveModel> CREATOR = new Creator<MonthlyIncentiveModel>() {
            @Override
            public MonthlyIncentiveModel createFromParcel(Parcel source) {
                return new MonthlyIncentiveModel(source);
            }

            @Override
            public MonthlyIncentiveModel[] newArray(int size) {
                return new MonthlyIncentiveModel[size];
            }
        };
    }

    public IndividualEntry() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.first_name);
        dest.writeString(this.last_name);
        dest.writeString(this.salary);
        dest.writeString(this.sia);
        dest.writeString(this.cv);
        dest.writeString(this.tia);
        dest.writeString(this.staff_url);
        dest.writeString(this.staff_id);
        dest.writeByte(this.got_audited_this_month ? (byte) 1 : (byte) 0);
        dest.writeByte(this.has_joined_this_month ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.monthly_incentive, flags);
    }

    protected IndividualEntry(Parcel in) {
        this.first_name = in.readString();
        this.last_name = in.readString();
        this.salary = in.readString();
        this.sia = in.readString();
        this.cv = in.readString();
        this.tia = in.readString();
        this.staff_url = in.readString();
        this.staff_id = in.readString();
        this.got_audited_this_month = in.readByte() != 0;
        this.has_joined_this_month = in.readByte() != 0;
        this.monthly_incentive = in.readParcelable(MonthlyIncentiveModel.class.getClassLoader());
    }

    public static final Creator<IndividualEntry> CREATOR = new Creator<IndividualEntry>() {
        @Override
        public IndividualEntry createFromParcel(Parcel source) {
            return new IndividualEntry(source);
        }

        @Override
        public IndividualEntry[] newArray(int size) {
            return new IndividualEntry[size];
        }
    };
}
