package com.treebo.prowlapp.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by devesh on 14/04/16.
 */

public class IncentivesItem implements Parcelable,Serializable {

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public IncentivesItem() {
    }

    protected IncentivesItem(Parcel in) {
    }

    public static final Creator<IncentivesItem> CREATOR = new Creator<IncentivesItem>() {
        @Override
        public IncentivesItem createFromParcel(Parcel source) {
            return new IncentivesItem(source);
        }

        @Override
        public IncentivesItem[] newArray(int size) {
            return new IncentivesItem[size];
        }
    };
}
