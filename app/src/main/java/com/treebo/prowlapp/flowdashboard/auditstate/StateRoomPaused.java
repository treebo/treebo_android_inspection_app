package com.treebo.prowlapp.flowdashboard.auditstate;

import android.content.Context;
import android.view.View;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;

import javax.inject.Inject;

/**
 * Created by sumandas on 29/11/2016.
 */

public class StateRoomPaused implements IAuditState {

    private Context mContext;


    @Inject
    public AuditPrefManagerV3 mAuditManager;

    public StateRoomPaused(Context context) {
        mContext = context;
        MainApplication.get().getApplicationComponent().injectRoomState(this);
    }

    @Override
    public boolean isCardClickEnabled() {
        return false;
    }

    @Override
    public int getAuditIcon() {
        return R.drawable.ic_room;
    }

    @Override
    public String getStateText() {
        String key = mAuditManager.getCurrentAuditKey();
        return AuditUtilsV3.getRoomFromKey(key);
    }

    @Override
    public String getRecommendedText() {
        return "";
    }


    @Override
    public String getAuditState() {
        return mContext.getResources().getString(R.string.paused);
    }

    @Override
    public int getRestartVisibility() {
        return View.VISIBLE;
    }

    @Override
    public int getResumeVisibility() {
        return View.VISIBLE;
    }

    @Override
    public int getStateVisibility() {
        return View.VISIBLE;
    }

    @Override
    public int getRecommendedVisibility() {
        return View.GONE;
    }

}
