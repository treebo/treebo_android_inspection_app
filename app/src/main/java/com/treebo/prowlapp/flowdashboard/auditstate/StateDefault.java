package com.treebo.prowlapp.flowdashboard.auditstate;

import android.view.View;

import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;

/**
 * Created by sumandas on 29/11/2016.
 */

public class StateDefault implements IAuditState {

    private TaskModel mTaskModel;


    public StateDefault(TaskModel taskModel) {
        mTaskModel = taskModel;
    }

    @Override
    public boolean isCardClickEnabled() {
        return true;
    }

    @Override
    public int getAuditIcon() {
        return -1;
    }

    @Override
    public String getStateText() {
        return null;
    }

    @Override
    public String getRecommendedText() {
        return mTaskModel.getAuditTaskModel().getRecommendedList();
    }


    @Override
    public String getAuditState() {
        return "";
    }


    @Override
    public int getRestartVisibility() {
        return View.GONE;
    }

    @Override
    public int getResumeVisibility() {
        return View.GONE;
    }

    @Override
    public int getStateVisibility() {

        return View.GONE;
    }

    @Override
    public int getRecommendedVisibility() {

        return View.VISIBLE;
    }

}
