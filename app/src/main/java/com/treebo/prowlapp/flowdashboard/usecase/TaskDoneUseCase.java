package com.treebo.prowlapp.flowdashboard.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 30/11/16.
 */

public class TaskDoneUseCase extends UseCase<BaseResponse> {

    private RestClient mRestClient;

    private int taskID;
    private int taskLogID;
    private int userID;
    private int hotelID;
    private boolean isDone;

    public void setFields(int taskID, int taskLogID, int userID, int hotelID, boolean isDone) {
        this.taskID = taskID;
        this.taskLogID = taskLogID;
        this.userID = userID;
        this.hotelID = hotelID;
        this.isDone = isDone;
    }

    public int getTaskID() {
        return this.taskID;
    }

    public TaskDoneUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    @Override
    protected Observable<BaseResponse> getObservable() {
        if (isDone)
            return mRestClient.submitTaskDone(taskID, taskLogID, userID, hotelID, "Complete");
        else
            return mRestClient.submitTaskDone(taskID, taskLogID, userID, hotelID, "Started");
    }
}
