package com.treebo.prowlapp.flowdashboard.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class FeedbackSubmitUseCase extends UseCase<BaseResponse> {

    private RestClient mRestClient;

    private int mUserID;

    private String mFeedbackText;

    public FeedbackSubmitUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setFields(int userID, String feedback) {
        this.mUserID = userID;
        this.mFeedbackText = feedback;
    }

    @Override
    protected Observable<BaseResponse> getObservable() {
        return mRestClient.submitFeedback(mUserID, mFeedbackText);
    }
}
