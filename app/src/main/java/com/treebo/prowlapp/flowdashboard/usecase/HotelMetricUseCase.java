package com.treebo.prowlapp.flowdashboard.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.dashboard.HotelMetricsResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class HotelMetricUseCase extends UseCase<HotelMetricsResponse> {

    RestClient mRestClient;

    int mUserID;

    int mHotelID;

    public HotelMetricUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setHotelAndUserID(int hotelID, int userID) {
        this.mUserID = userID;
        this.mHotelID = hotelID;
    }

    @Override
    protected Observable<HotelMetricsResponse> getObservable() {
        return mRestClient.getHotelMetrics(mUserID, mHotelID);
    }
}
