package com.treebo.prowlapp.flowdashboard.periodicstate;

import android.content.Context;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 28/02/17.
 */

public class GenericPeriodicAuditCard extends BasePeriodicAuditCard implements IPeriodicAuditCard {

    private Context mContext;
    private TaskModel mTaskModel;


    public GenericPeriodicAuditCard(Context context, TaskModel taskModel) {
        super(taskModel);
        mContext = context;
        mTaskModel = taskModel;
    }

    @Override
    public int getAuditIcon() {
        switch (mTaskModel.getAuditTaskType()) {
            case Constants.HR_AUDIT_TASK_TYPE:
                return R.drawable.ic_hr_audit;

            case Constants.STORAGE_SPACE_AUDIT_TASK_TYPE:
                return R.drawable.ic_storage;

            case Constants.SAFETY_AUDIT_TASK_TYPE:
                return R.drawable.ic_fire_safety_audit;

            case Constants.GARBAGE_MANAGEMENT_AUDIT_TASK_TYPE:
                return R.drawable.ic_garbage;

            case Constants.WATER_STORAGE_AUDIT_TASK_TYPE:
                return R.drawable.ic_water_storage;

            case Constants.DISABLED_FRIENDLY_AUDIT_TASK_TYPE:
                return R.drawable.ic_disabled_friendly;

            case Constants.HK_TOOLS_AUDIT_TASK_TYPE:
                return R.drawable.ic_house_keeping;

            case Constants.FRONT_OFFICE_AUDIT_TASK_TYPE:
                return R.drawable.ic_front_office_tools;

            case Constants.LUXURY_AMENITIES_AUDIT_TASK_TYPE:
                return R.drawable.ic_luxury_amenities;

            case Constants.OTHER_AREAS_AUDIT_TASK_TYPE:
                return R.drawable.ic_driver_quarters;

            case Constants.PARKING_AREA_TASK_TYPE:
                return R.drawable.ic_parking;

            case Constants.IN_HOUSE_LAUNDRY_AUDIT_TASK_TYPE:
                return R.drawable.ic_inhouse_laundry;

            default:
                return R.drawable.ic_fraud_audit;
        }
    }

    @Override
    public int getAuditIconTint() {
        return R.color.green_color_primary;
    }

    @Override
    public String getAuditText() {
        return mTaskModel.getHeading();
    }
}
