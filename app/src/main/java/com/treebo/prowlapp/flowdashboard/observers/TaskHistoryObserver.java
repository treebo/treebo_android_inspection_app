package com.treebo.prowlapp.flowdashboard.observers;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.usecase.TaskHistoryUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.dashboard.TaskHistoryResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class TaskHistoryObserver extends BaseObserver<TaskHistoryResponse> {

    private TaskHistoryUseCase mUseCase;
    private DashboardContract.ITaskHistoryView mView;

    public TaskHistoryObserver(BasePresenter presenter, DashboardContract.ITaskHistoryView view,
                               TaskHistoryUseCase useCase) {
        super(presenter, useCase);
        mView = view;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(TaskHistoryResponse response) {
        mView.hideLoading();
        if (response.status.equals(Constants.STATUS_SUCCESS))
            mView.onLoadSuccess(response.data);
        else
            mView.onLoadFailure(response.msg);
    }
}
