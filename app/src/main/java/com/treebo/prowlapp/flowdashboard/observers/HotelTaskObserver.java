package com.treebo.prowlapp.flowdashboard.observers;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.usecase.HotelTasksUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.dashboard.HotelTasksResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class HotelTaskObserver extends BaseObserver<HotelTasksResponse> {

    DashboardContract.IDashboardView mView;
    HotelTasksUseCase mUseCase;

    public HotelTaskObserver(BasePresenter presenter, DashboardContract.IDashboardView view,
                             HotelTasksUseCase useCase) {
        super(presenter, useCase);
        mUseCase = useCase;
        mView = view;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(HotelTasksResponse response) {
        mView.hideLoading();
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mView.onLoadTasksSuccess(response);
        } else {
            mView.onLoadTasksFailure("Loading tasks failed");
        }
    }
}
