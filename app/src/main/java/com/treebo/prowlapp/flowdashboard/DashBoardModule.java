package com.treebo.prowlapp.flowdashboard;

import com.treebo.prowlapp.flowauditnew.usecase.AuditCategoryUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomSingleUseCase;
import com.treebo.prowlapp.flowdashboard.presenter.ContactInfoPresenter;
import com.treebo.prowlapp.flowdashboard.presenter.CreateIssuePresenter;
import com.treebo.prowlapp.flowdashboard.presenter.DashboardPresenter;
import com.treebo.prowlapp.flowdashboard.presenter.HistoryPresenter;
import com.treebo.prowlapp.flowdashboard.presenter.SubmitFeedbackPresenter;
import com.treebo.prowlapp.flowdashboard.usecase.ContactInfoUseCase;
import com.treebo.prowlapp.flowdashboard.usecase.FeedbackSubmitUseCase;
import com.treebo.prowlapp.flowdashboard.usecase.HotelMetricUseCase;
import com.treebo.prowlapp.flowdashboard.usecase.HotelTasksUseCase;
import com.treebo.prowlapp.flowdashboard.usecase.TaskDoneUseCase;
import com.treebo.prowlapp.flowdashboard.usecase.TaskHistoryUseCase;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 20/11/2016.
 */
@Module
public class DashBoardModule {


    @Provides
    RestClient providesRestClient() {
        return new RestClient();
    }

    @Provides
    DashboardPresenter providesDashboardPresenter() {
        return new DashboardPresenter();
    }

    @Provides
    SubmitFeedbackPresenter providesSubmitFeedbackPresenter() {
        return new SubmitFeedbackPresenter();
    }

    @Provides
    HistoryPresenter providesHistoryPresenter() {
        return new HistoryPresenter();
    }

    @Provides
    ContactInfoPresenter providesContactInfoPresenter() {
        return new ContactInfoPresenter();
    }

    @Provides
    CreateIssuePresenter providesCreateIssuePresenter() {
        return new CreateIssuePresenter();
    }

    @Provides
    HotelMetricUseCase providesHotelMetricUseCase(RestClient restClient) {
        return new HotelMetricUseCase(restClient);
    }

    @Provides
    HotelTasksUseCase providesHotelTasksUseCase(RestClient restClient) {
        return new HotelTasksUseCase(restClient);
    }

    @Provides
    FeedbackSubmitUseCase providesSubmitFeedbackUseCase(RestClient restClient) {
        return new FeedbackSubmitUseCase(restClient);
    }

    @Provides
    TaskHistoryUseCase providesTaskHistoryUseCase(RestClient restClient) {
        return new TaskHistoryUseCase(restClient);
    }

    @Provides
    TaskDoneUseCase providesTaskDoneUseCase(RestClient restClient) {
        return new TaskDoneUseCase(restClient);
    }

    @Provides
    ContactInfoUseCase providesContactInfoUseCase(RestClient restClient) {
        return new ContactInfoUseCase(restClient);
    }

    @Provides
    AuditCategoryUseCase providesAuditCategoryUseCase(RestClient restClient) {
        return new AuditCategoryUseCase(restClient);
    }

    @Provides
    RoomSingleUseCase providesRoomSingleUseCase(RestClient restClient) {
        return new RoomSingleUseCase(restClient);
    }
}
