package com.treebo.prowlapp.flowdashboard.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class ExploreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private onExploreItemClickListener mListener;
    private ArrayList<String> mExploreOptionsList;

    public ExploreAdapter(ArrayList<String> optionList, onExploreItemClickListener listener) {
        mExploreOptionsList = optionList;
        mListener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_explore,
                parent, false);
        return new ItemViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).text.setText(mExploreOptionsList.get(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return mExploreOptionsList.size();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        private TreeboTextView text;

        public ItemViewHolder(View itemView, onExploreItemClickListener listener) {
            super(itemView);
            text = (TreeboTextView) itemView.findViewById(R.id.item_explore_tv);
            itemView.setOnClickListener(view -> listener.onItemClicked(text.getText().toString()));
        }
    }

    public interface onExploreItemClickListener {
        void onItemClicked(String position);
    }
}
