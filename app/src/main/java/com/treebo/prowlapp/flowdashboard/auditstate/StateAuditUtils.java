package com.treebo.prowlapp.flowdashboard.auditstate;

import android.content.Context;
import android.text.TextUtils;

import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.Models.auditmodel.AuditTypeMap;
import com.treebo.prowlapp.Models.auditmodel.CompleteAudit;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;

/**
 * Created by sumandas on 30/11/2016.
 */

public class StateAuditUtils {

    public static IAuditState newState(Context context, String state, TaskModel model, int hotelId) {
        switch (state) {
            case "default":
                return new StateDefault(model);
            case AuditTypeMap.COMMON_AUDIT:
                return new StateCommonPaused(context);
            case AuditTypeMap.ROOM_AUDIT:
                return new StateRoomPaused(context);
            case "pending":
                return new StatePending(context, hotelId);
            case "done":
                return new StateDone();
            default:
                return new StateDefault(model);
        }
    }

    public static String getState(int hotelId, AuditPrefManagerV3 auditManager) {
        CompleteAudit completeAudit = CompleteAudit.getCompleteAuditByHotelId(hotelId);
        String key = auditManager.getCurrentAuditKey();
        boolean isAuditNotStarted = (completeAudit == null || !completeAudit.isAuditStarted());
        if (TextUtils.isEmpty(key) && isAuditNotStarted) {//default state
            return "default";
        }
        if (!TextUtils.isEmpty(key)) {//room or common paused state
            return AuditUtilsV3.getAuditTypeFromKey(key);

        }
        if (!isAuditNotStarted) {//pending state or completed
            if (completeAudit.isDailyAuditCompleted()) {
                return "done";
            } else {
                return "pending";
            }

        }
        return "default";
    }
}
