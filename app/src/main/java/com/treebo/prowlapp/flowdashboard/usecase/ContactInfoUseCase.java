package com.treebo.prowlapp.flowdashboard.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.dashboard.ContactInfoResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 21/04/17.
 */

public class ContactInfoUseCase extends UseCase<ContactInfoResponse> {

    RestClient mRestClient;

    public ContactInfoUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    @Override
    protected Observable<ContactInfoResponse> getObservable() {
        return mRestClient.getContactInformation();
    }
}
