package com.treebo.prowlapp.flowdashboard.observers;

import com.treebo.prowlapp.flowdashboard.presenter.CreateIssuePresenter;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 29/06/17.
 */

public class RoomChecklistObserver extends BaseObserver<RoomSingleResponse> {

    private CreateIssuePresenter mPresenter;

    public RoomChecklistObserver(CreateIssuePresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(RoomSingleResponse response) {
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onLoadRoomSuccess(response);
        } else {
            mPresenter.onLoadFailure(response.msg);
        }
    }
}
