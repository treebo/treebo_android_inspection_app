package com.treebo.prowlapp.flowdashboard.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AppUpdateEvent;
import com.treebo.prowlapp.events.AuditStateChangeEvent;
import com.treebo.prowlapp.events.DepartmentIncentivesGivenEvent;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.events.IssueResolvedEventV3;
import com.treebo.prowlapp.events.PeriodicAuditStateChangeEvent;
import com.treebo.prowlapp.events.PermissionsUpdateEvent;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.activity.ConfirmActionActivity;
import com.treebo.prowlapp.flowauditnew.activity.RoomPickerActivity;
import com.treebo.prowlapp.flowdashboard.DaggerDashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.adapter.SectionedTasksAdapter;
import com.treebo.prowlapp.flowdashboard.periodicstate.PeriodicStateUtils;
import com.treebo.prowlapp.flowdashboard.presenter.DashboardPresenter;
import com.treebo.prowlapp.flowdashboard.usecase.HotelMetricUseCase;
import com.treebo.prowlapp.flowdashboard.usecase.HotelTasksUseCase;
import com.treebo.prowlapp.flowdashboard.usecase.TaskDoneUseCase;
import com.treebo.prowlapp.geofence.GeofenceTransitionsIntentService;
import com.treebo.prowlapp.geofence.GeofenceUtils;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Models.auditmodel.AuditTypeMap;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.CompleteAudit;
import com.treebo.prowlapp.Models.dashboardmodel.HotelDashboardMetrics;
import com.treebo.prowlapp.Models.dashboardmodel.IssueTaskModel;
import com.treebo.prowlapp.Models.dashboardmodel.SectionedTaskListModel;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioHotelV3;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.response.dashboard.HotelMetricsResponse;
import com.treebo.prowlapp.response.dashboard.HotelTasksResponse;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 11/11/16.
 */

public class DashboardActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status>, DashboardContract.IDashboardView,
        SectionedTasksAdapter.taskCardActionListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "DashboardActivity";


    @Inject
    public Navigator mNavigator;


    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public AuditPrefManagerV3 mAuditManager;

    @Inject
    public RxBus mRxBus;

    @Inject
    public DashboardPresenter mDashboardPresenter;

    @Inject
    public HotelMetricUseCase mHotelMetricUseCase;

    @Inject
    public HotelTasksUseCase mHotelTasksUseCase;

    @Inject
    public TaskDoneUseCase mTaskDoneUseCase;

    private HotelLocation mHotelLocation;

    private TreeboTextView mHotelNameTv;

    private Geofence mGeofence;

    private PendingIntent mGeofencePendingIntent;

    private GoogleApiClient mGoogleApiClient;

    private final CompositeSubscription mSubscription = new CompositeSubscription();
    private boolean isInHotelLocation;
    private RecyclerView mTasksRecyclerView;
    private final ArrayList<SectionedTaskListModel> mSectionedTaskList = new ArrayList<>();

    private final ArrayList<TaskModel> mMasterTaskList = new ArrayList<>();
    private final ArrayList<TaskModel> mDisplayTaskList = new ArrayList<>();

    private boolean isFromExplore;
    private TreeboTextView mComplianceScore;
    private TreeboTextView mDelightScore;
    private TreeboTextView mDisasterScore;
    private SectionedTasksAdapter mAdapter;
    private int mHotelId;
    private String mHotelName;
    private View mLoadingView;
    private View mExtraInformationView;
    private View mEmptyView;
    private TreeboTextView mExtraInformationViewText;
    private TreeboTextView mExtraInformationViewTitle;
    private View mAppBarView;
    private TreeboTextView mComplianceChange;
    private TreeboTextView mDelightChange;
    private TreeboTextView mDisasterChange;
    private ImageView mComplianceArrowIv;
    private ImageView mDelightArrowIv;
    private ImageView mDisasterArrowIv;
    private boolean isUndoClicked;

    private View mCardInformationView;
    private TreeboTextView mCardInformationViewText;
    private TreeboTextView mCardInformationViewTitle;
    private TaskModel mCurrentTask;
    private int mComplianceScoreInteger;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private View mNoIncentivesView;
    private int mClickedStaffIncentiveTaskLogID;
    private TreeboTextView mNoIncentivesSubtitle;
    private boolean isActivityPaused;
    private View mComplianceScoreRl;
    private View mDelightScoreRl;
    private View mDisasterScoreRl;
    private View mPortfolioIcon;
    private View mPortfolioBtn;
    private View mExploreIcon;
    private View mExploreBtn;
    private boolean isEmptyViewPermissionEnabled;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_new);
        setToolBarColor(R.color.card_activity_background);

        DashboardComponent dashboardComponent = DaggerDashboardComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        dashboardComponent.injectDashboardActivity(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);

        if (mLoginManager.getServerAppVersion() > BuildConfig.VERSION_CODE) {
            mNavigator.navigateToBlockerScreen(this);
        }
        if (savedInstanceState == null) {
            mHotelName = getIntent().getStringExtra(Utils.HOTEL_NAME);
            mHotelId = getIntent().getIntExtra(Utils.HOTEL_ID, -1);
            isFromExplore = getIntent().getBooleanExtra(Utils.IS_FROM_EXPLORE, false);
            isInHotelLocation = getIntent().getBooleanExtra(Utils.IS_IN_HOTEL, false);
        } else {
            mHotelId = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            isFromExplore = savedInstanceState.getBoolean(Utils.IS_FROM_EXPLORE);
            mHotelName = savedInstanceState.getString(Utils.HOTEL_NAME);
            isInHotelLocation = savedInstanceState.getBoolean(Utils.IS_IN_HOTEL, false);
            isEmptyViewPermissionEnabled = savedInstanceState.getBoolean(Utils.IS_DASHBOARD_EMPTY_SCREEN_ENABLED,
                    false);
        }

        mAppBarView = findViewById(R.id.toolbar_dashboard);
        TreeboTextView title = (TreeboTextView) mAppBarView.findViewById(R.id.dashboard_title_tv);
        mHotelNameTv = (TreeboTextView) mAppBarView.findViewById(R.id.dashboard_hotel_name_tv);
        mComplianceScoreRl = mAppBarView.findViewById(R.id.compliance_rl);
        mDelightScoreRl = mAppBarView.findViewById(R.id.delight_rl);
        mDisasterScoreRl = mAppBarView.findViewById(R.id.disaster_rl);

        mComplianceScore = (TreeboTextView) mAppBarView.findViewById(R.id.masthead_compliance_score_tv);
        mDelightScore = (TreeboTextView) mAppBarView.findViewById(R.id.masthead_delight_score_tv);
        mDisasterScore = (TreeboTextView) mAppBarView.findViewById(R.id.masthead_disaster_score_tv);

        mComplianceChange = (TreeboTextView) mAppBarView.findViewById(R.id.compliance_change_tv);
        mDelightChange = (TreeboTextView) mAppBarView.findViewById(R.id.delight_change_tv);
        mDisasterChange = (TreeboTextView) mAppBarView.findViewById(R.id.disaster_change_tv);

        mComplianceArrowIv = (ImageView) mAppBarView.findViewById(R.id.compliance_arrow_iv);
        mDelightArrowIv = (ImageView) mAppBarView.findViewById(R.id.delight_arrow_iv);
        mDisasterArrowIv = (ImageView) mAppBarView.findViewById(R.id.disaster_arrow_iv);

        mLoadingView = findViewById(R.id.loader_layout);
        mEmptyView = findViewById(R.id.layout_dashboard_empty_view);
        ImageView gifView = (ImageView) mEmptyView.findViewById(R.id.gifImageView);
        Glide.with(this).load(R.drawable.diamond)
                .asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(gifView);
        setSpannableTextOnEmptyView((TreeboTextView) mEmptyView.findViewById(R.id.audit_more_rooms_btn));
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.dashboard_swipe_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mNoIncentivesView = findViewById(R.id.layout_no_incentive);
        View crossBtn = mNoIncentivesView.findViewById(R.id.ic_close);
        crossBtn.setOnClickListener(view -> {
            mNoIncentivesView.setVisibility(View.GONE);
            mAppBarView.setVisibility(View.VISIBLE);
        });
        TreeboTextView noIncentivesTitle = (TreeboTextView) mNoIncentivesView.findViewById(R.id.title_tv);
        noIncentivesTitle.setText(getString(R.string.no_incentive_title, "All Departments"));
        mNoIncentivesSubtitle = (TreeboTextView) mNoIncentivesView.findViewById(R.id.subtext_tv);
        TreeboButton noIncentivesBtn = (TreeboButton) mNoIncentivesView.findViewById(R.id.okay_btn);
        noIncentivesBtn.setOnClickListener(view -> {
            for (TaskModel taskModel : mMasterTaskList) {
                if (taskModel.getTaskLogID() == mClickedStaffIncentiveTaskLogID) {
                    taskModel.isSoftDeleted = true;
                    refreshSectionedList();
                    mAdapter.refreshList(!isFromExplore);
                    mDashboardPresenter.onTaskDone(taskModel.getTaskID(), taskModel.getTaskLogID(), mLoginManager.getUserId(), mHotelId);
                }
            }
            mNoIncentivesView.setVisibility(View.GONE);
            mAppBarView.setVisibility(View.VISIBLE);
        });
        if (isFromExplore)
            title.setText(getString(R.string.explore));
        else
            title.setText(getString(R.string.you_are_in));
        mHotelNameTv.setText(mHotelName);
        SegmentAnalyticsManager.trackScreenOnSegment(SegmentAnalyticsManager.DASHBOARD_VIEW,
                new Properties().putValue(SegmentAnalyticsManager.SOURCE_SCREEN,
                        isInHotelLocation ? SegmentAnalyticsManager.SOURCE_FROM_GEOFENCE : SegmentAnalyticsManager.PORTFOLIO_SCREEN)
                        .putValue(SegmentAnalyticsManager.IS_IN_GEOFENCE, isInHotelLocation)
                        .putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName));

        mTasksRecyclerView = (RecyclerView) findViewById(R.id.dashboard_recyclerview);

        setUpTooltipLayout();
        mCardInformationView = findViewById(R.id.task_card_info_layout);
        mCardInformationViewText = (TreeboTextView) mCardInformationView.findViewById(R.id.card_info_text);
        mCardInformationViewTitle = (TreeboTextView) mCardInformationView.findViewById(R.id.card_info_title);
        View closeCardInfo = mCardInformationView.findViewById(R.id.ic_card_info_close);
        closeCardInfo.setOnClickListener(view -> toggleCardInfoLayout(false));
        View cardInfoDoneBtn = mCardInformationView.findViewById(R.id.im_done_btn);
        cardInfoDoneBtn.setOnClickListener(view -> {
            onDoneBtnClick(mCurrentTask);
            toggleCardInfoLayout(false);
        });

        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                HotelChangedEvent hotelChangedEvent = ((HotelChangedEvent) event);
                if (hotelChangedEvent.mHotelList == null) {
                    title.setText(getString(R.string.explore));
                    isInHotelLocation = false;
                    isFromExplore = true;

                } else {
                    boolean shouldLocationBeUpdated = true;
                    HotelLocation hotelDetected = new HotelLocation();
                    for (HotelLocation hotel : hotelChangedEvent.mHotelList) {
                        if (mHotelId == hotel.mHotelId) {
                            shouldLocationBeUpdated = false;
                            hotelDetected = hotel;
                        }
                    }
                    if (shouldLocationBeUpdated) {
                        //reload the scores and tasks
                        if (hotelChangedEvent.mHotelList.size() == 1) {
                            hotelDetected = hotelChangedEvent.mHotelList.get(0);
                            mHotelId = hotelDetected.mHotelId;
                            title.setText(getString(R.string.you_are_in));
                            isInHotelLocation = true;
                            isFromExplore = false;
                            mHotelName = hotelDetected.mHotelName;
                            mHotelNameTv.setText(mHotelName);
                            mDashboardPresenter.loadTasksForHotel(mHotelId, mLoginManager.getUserId());
                            mDashboardPresenter.loadScoresForHotel(mHotelId, mLoginManager.getUserId());
                        } else {
                            title.setText(getString(R.string.explore));
                            isInHotelLocation = false;
                            isFromExplore = true;
                        }
                    }
                }
                mAdapter.refreshList(!isFromExplore);
            } else if (event instanceof AuditStateChangeEvent) {
                switch (((AuditStateChangeEvent) event).auditStateDescription) {
                    case AuditStateChangeEvent.FRAUD_AUDIT_SUBMIT:
                        for (TaskModel taskModel : mMasterTaskList) {
                            if (taskModel.getAuditTaskType() == Constants.FRAUD_AUDIT_TASK_TYPE) {
                                taskModel.isSoftDeleted = true;
                                refreshSectionedList();
                                mAdapter.refreshList(!isFromExplore);
                                mDashboardPresenter.onTaskDone(taskModel.getTaskID(), taskModel.getTaskLogID(), mLoginManager.getUserId(), mHotelId);
                            }
                        }
                        break;

                    case AuditStateChangeEvent.PERIODIC_AUDIT_SUBMIT:
                    case AuditStateChangeEvent.FnB_PERIODIC_AUDIT_SUBMIT:
                        int taskLogId = ((PeriodicAuditStateChangeEvent) event).mTaskLogID;
                        for (TaskModel taskModel : mMasterTaskList) {
                            if (taskModel.getTaskLogID() == taskLogId) {
                                taskModel.isSoftDeleted = true;
                                refreshSectionedList();
                                mAdapter.refreshList(!isFromExplore);
                                mDashboardPresenter.onTaskDone(taskModel.getTaskID(), taskModel.getTaskLogID(), mLoginManager.getUserId(), mHotelId);
                            }
                        }
                        break;

                    case AuditStateChangeEvent.ROOM_AUDIT_SUBMIT:
                    case AuditStateChangeEvent.COMMON_AREA_AUDIT_SUBMIT:
                    case AuditStateChangeEvent.AUDIT_COMPLETE:
                    case AuditStateChangeEvent.UPDATE_AVAILABLE_ROOM_COUNT:
                        int auditTaskID = 0, auditTaskLogID = 0;
                        mDashboardPresenter.loadScoresForHotel(mHotelId, mLoginManager.getUserId());
                        for (TaskModel taskModel : mMasterTaskList) {
                            if (taskModel.getTaskCardType() == Constants.AUDIT_TASK) {
                                auditTaskID = taskModel.getTaskID();
                                auditTaskLogID = taskModel.getTaskLogID();
                            }
                        }

                        CompleteAudit completeAudit = CompleteAudit.getCompleteAuditByHotelId(mHotelId);
                        if (completeAudit != null) {
                            if (((AuditStateChangeEvent) event).auditStateDescription.equals(AuditStateChangeEvent.ROOM_AUDIT_SUBMIT)) {
                                completeAudit.mAvailableRooms--;
                                completeAudit.update();
                            }
                            if (completeAudit.isDailyAuditCompleted()) {
                                for (TaskModel taskModel : mMasterTaskList) {
                                    if (taskModel.getTaskCardType() == Constants.AUDIT_TASK) {
                                        taskModel.setTaskStatus(getString(R.string.complete));
                                        refreshSectionedList();
                                        mAdapter.refreshList(!isFromExplore);
                                        mDashboardPresenter.onTaskDone(taskModel.getTaskID(), taskModel.getTaskLogID(), mLoginManager.getUserId(), mHotelId);
                                    }
                                }
                            } else if (completeAudit.isOneRoomAudited()) {
                                mDashboardPresenter.onTaskStarted(auditTaskID, auditTaskLogID, mLoginManager.getUserId(), mHotelId);
                            }
                        }
                        break;

                }
                mAdapter.refreshList(!isFromExplore);
            } else if (event instanceof IssueResolvedEventV3) {
                for (int i = 0; i < mMasterTaskList.size(); i++) {
                    mDashboardPresenter.loadScoresForHotel(mHotelId, mLoginManager.getUserId());
                    TaskModel issueTask = mMasterTaskList.get(i);
                    if (issueTask.getTaskCardType() == Constants.ISSUE_TASK) {
                        if (((IssueResolvedEventV3) event).overdueIssueCount == 0 && ((IssueResolvedEventV3) event).todaysIssueCount == 0) {
                            issueTask.isSoftDeleted = true;
                            mDashboardPresenter.onTaskDone(issueTask.getTaskID(), issueTask.getTaskLogID(), mLoginManager.getUserId(), mHotelId);
                        } else {
                            IssueTaskModel issueTaskModel = new IssueTaskModel();
                            issueTaskModel.mOverdues = ((IssueResolvedEventV3) event).overdueIssueCount;
                            issueTaskModel.mToday = ((IssueResolvedEventV3) event).todaysIssueCount;
                            issueTask.setLocalIssueModel(issueTaskModel);
                        }
                        mMasterTaskList.set(i, issueTask);
                        break;
                    }
                }
                refreshSectionedList();
                mAdapter.refreshList(!isFromExplore);
            } else if (event instanceof AppUpdateEvent) {
                if (!isActivityPaused)
                    mNavigator.navigateToBlockerScreen(this);
            } else if (event instanceof DepartmentIncentivesGivenEvent) {
                int month = ((DepartmentIncentivesGivenEvent) event).getMonth();
                int year = ((DepartmentIncentivesGivenEvent) event).getYear();
                for (TaskModel taskModel : mMasterTaskList) {
                    if (taskModel.getTaskCardType() == Constants.STAFF_INCENTIVE_TASK
                            && taskModel.getMonth() == month & taskModel.getYear() == year) {
                        taskModel.isSoftDeleted = true;
                        refreshSectionedList();
                        mAdapter.refreshList(!isFromExplore);
                        mDashboardPresenter.onTaskDone(taskModel.getTaskID(), taskModel.getTaskLogID(), mLoginManager.getUserId(), mHotelId);
                    }
                }
            } else if (event instanceof PermissionsUpdateEvent) {
                mDashboardPresenter.updateViewWithPermissions();
            }
        }));

        buildGoogleApiClient();
        mGoogleApiClient.connect();

        purgeStaleGeofences();
        mPortfolioIcon = findViewById(R.id.ic_portfolio_icon);
        mPortfolioBtn = findViewById(R.id.portfolio_btn);
        mPortfolioBtn.setOnClickListener(view -> {
            PortfolioHotelV3 hotelV3 = new PortfolioHotelV3();
            hotelV3.setHotelID(mHotelId);
            hotelV3.setHotelName(mHotelName);
            hotelV3.setCompliancePercent(mComplianceScoreInteger);
            if (Utils.isInternetConnected(this)) {
                mNavigator.navigateToPortfolioActivity(this, hotelV3, isInHotelLocation);
            } else
                Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
        });
        mPortfolioIcon.setOnClickListener(view -> {
            PortfolioHotelV3 hotelV3 = new PortfolioHotelV3();
            hotelV3.setHotelID(mHotelId);
            hotelV3.setHotelName(mHotelName);
            hotelV3.setCompliancePercent(mComplianceScoreInteger);
            if (Utils.isInternetConnected(this)) {
                mNavigator.navigateToPortfolioActivity(this, hotelV3, isInHotelLocation);
            } else
                Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
        });


        mExploreIcon = findViewById(R.id.ic_explore_icon);
        mExploreBtn = findViewById(R.id.explore_btn);
        mExploreBtn.setOnClickListener(view -> {
            mNavigator.navigateToExploreActivity(this, mHotelName, mHotelId);
        });
        mExploreIcon.setOnClickListener(view -> {
            mNavigator.navigateToExploreActivity(this, mHotelName, mHotelId);
        });

        setUpRecyclerView();
        mDashboardPresenter.updateViewWithPermissions();
        mDashboardPresenter.loadTasksForHotel(mHotelId, mLoginManager.getUserId());
        mDashboardPresenter.loadScoresForHotel(mHotelId, mLoginManager.getUserId());

    }

    @Inject
    public void setUp() {
        mDashboardPresenter.setMvpView(this);
        mDashboardPresenter.setMetricUseCase(mHotelMetricUseCase);
        mDashboardPresenter.setHotelTaskUseCase(mHotelTasksUseCase);
        mDashboardPresenter.setTaskDoneUseCase(mTaskDoneUseCase);
    }

    @Override
    public void onBackPressed() {
        if (mCardInformationView.getVisibility() == View.VISIBLE) {
            mCardInformationView.setVisibility(View.GONE);
            mAppBarView.setVisibility(View.VISIBLE);
        } else if (mExtraInformationView.getVisibility() == View.VISIBLE) {
            mExtraInformationView.setVisibility(View.GONE);
            mAppBarView.setVisibility(View.VISIBLE);
        } else if (mNoIncentivesView.getVisibility() == View.VISIBLE) {
            mExtraInformationView.setVisibility(View.GONE);
            mAppBarView.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }

    private void setUpTooltipLayout() {
        mExtraInformationView = findViewById(R.id.extra_information_layout);
        mExtraInformationViewTitle = (TreeboTextView) mExtraInformationView.findViewById(R.id.extra_info_title);
        mExtraInformationViewText = (TreeboTextView) mExtraInformationView.findViewById(R.id.extra_info_text);
        View closeView = mExtraInformationView.findViewById(R.id.ic_extra_info_close);
        closeView.setOnClickListener(view -> toggleTooltipLayout(false));
        mExtraInformationViewTitle.setOnClickListener(view -> toggleTooltipLayout(false));
        mExtraInformationViewTitle.setOnClickListener(view -> toggleTooltipLayout(false));
        mExtraInformationView.setOnClickListener(view -> toggleTooltipLayout(false));
    }


    private void toggleTooltipLayout(boolean show) {
        mExtraInformationView.setVisibility(show ? View.VISIBLE : View.GONE);
        mAppBarView.setVisibility(!show ? View.VISIBLE : View.GONE);
        setToolBarColor(show ? R.color.extra_information_layout_bg : R.color.card_activity_background);
    }

    private void toggleCardInfoLayout(boolean show) {
        mCardInformationView.setVisibility(show ? View.VISIBLE : View.GONE);
        mAppBarView.setVisibility(!show ? View.VISIBLE : View.GONE);
        setToolBarColor(show ? R.color.white : R.color.card_activity_background);
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void purgeStaleGeofences() {
        Observable observable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                HotelLocation.removeStaleGeoFences();
                subscriber.onCompleted();
            }
        });
        observable.subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe();
    }


    private void setUpRecyclerView() {
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mTasksRecyclerView.setLayoutManager(layoutManager1);
        mTasksRecyclerView.setItemAnimator(new DefaultItemAnimator());
        initSwipe();
        mAdapter = new SectionedTasksAdapter(this, mSectionedTaskList, this, mHotelId, mAuditManager, mRxBus);
        mTasksRecyclerView.setAdapter(mAdapter);
    }

    private void setSpannableTextOnEmptyView(TreeboTextView textView) {
        if (isFromExplore) {
            textView.setVisibility(View.GONE);
        } else {
            if (textView != null) {
                SpannableString ss = new SpannableString(getString(R.string.not_enough_audit_more_rooms));
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SELECT_ANOTHER_ROOM, new Properties());
                        Intent intent = new Intent(DashboardActivity.this, RoomPickerActivity.class);
                        intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
                        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
                        intent.putExtra(Utils.HOTEL_ID, mHotelId);
                        startActivity(intent);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                        ds.setColor(ContextCompat.getColor(DashboardActivity.this, R.color.blue_5768e9));
                    }
                };
                ss.setSpan(clickableSpan, 12, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                textView.setText(ss);
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                textView.setHighlightColor(ContextCompat.getColor(DashboardActivity.this, R.color.blue_5768e9));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
        Utils.unSubscribe(mSubscription);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mHotelLocation = HotelLocation.getHotelLocationById(mLoginManager.getCurrentHotelLocationId());

        if (mHotelLocation != null) {
            if (!mHotelLocation.isGeofenceAdded) {
                populateGeofence(mHotelLocation);
                addGeofences();
            }
        }
        Log.i(TAG, "Connected to GoogleApiClient");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofence(mGeofence);
        return builder.build();
    }

    private void addGeofences() {
        if (!mGoogleApiClient.isConnected()) {
            Log.d(TAG, "Google api client not connected");
            return;
        }
        try {
            Log.d(TAG, "adding geofence for hotel" + mHotelId);
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    getGeofencingRequest(),
                    getGeofencePendingIntent()
            ).setResultCallback(this);
        } catch (SecurityException securityException) {
            logSecurityException(securityException);
        }
    }

    /**
     * Removes geofences, which stops further notifications when the device enters or exits
     * previously registered geofences.
     */
    public void removeGeofences() {
        if (!mGoogleApiClient.isConnected()) {
            Log.d(TAG, "Google api client not connected");
            return;
        }
        try {
            LocationServices.GeofencingApi.removeGeofences(
                    mGoogleApiClient,
                    getGeofencePendingIntent()
            ).setResultCallback(this);
        } catch (SecurityException securityException) {
            logSecurityException(securityException);
        }
    }

    private void logSecurityException(SecurityException securityException) {
        Log.e(TAG, "Invalid location permission. " +
                "You need to use ACCESS_FINE_LOCATION with geofences", securityException);
    }

    @Override
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.d(TAG, "geofences added/removed successfully");
            mHotelLocation.isGeofenceAdded = !mHotelLocation.isGeofenceAdded;
            mHotelLocation.mGeoFenceAddedTime = Calendar.getInstance().getTimeInMillis();
            mHotelLocation.save();
        } else {
            String errorMessage = GeofenceUtils.getErrorString(this, status.getStatusCode());
            Log.e(TAG, errorMessage);
        }
    }

    private PendingIntent getGeofencePendingIntent() {
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    private void populateGeofence(HotelLocation hotelLocation) {
        mGeofence = new Geofence.Builder()
                .setRequestId(Integer.toString(hotelLocation.mHotelId))
                .setCircularRegion(
                        hotelLocation.mLatitude,
                        hotelLocation.mLongitude,
                        hotelLocation.mRadius
                )
                .setExpirationDuration(GeofenceUtils.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
    }

    @Override
    public void setUpViewAsPerPermissions() {
        RolesAndUserPermissions.PermissionList permissions = mLoginManager.getUserPermissionList(mHotelId);
        if (permissions != null) {
            isEmptyViewPermissionEnabled = permissions.isDashboardEmptyScreenEnabled();
            mAppBarView.setVisibility(permissions.isDashboardMastheadEnabled() ? View.VISIBLE : View.GONE);
            mHotelNameTv.setVisibility(permissions.isDashboardMastheadHeaderEnabled() ? View.VISIBLE : View.GONE);
            mComplianceScoreRl.setVisibility(permissions.isDashboardComplianceMetricEnabled() ? View.VISIBLE : View.GONE);
            mDelightScoreRl.setVisibility(permissions.isDashboardDelightMetricEnabled() ? View.VISIBLE : View.GONE);
            mDisasterScoreRl.setVisibility(permissions.isDashboardDisasterMetricEnabled() ? View.VISIBLE : View.GONE);
            View floatingBottomBar = findViewById(R.id.floating_bottom_bar);
            View bottomBarDivider = findViewById(R.id.bottom_bar_divider);
            if (!permissions.isDashboardPortfolioEnabled() && !permissions.isDashboardExploreEnabled()) {
                floatingBottomBar.setVisibility(View.GONE);
            } else if (!permissions.isDashboardPortfolioEnabled()) {
                bottomBarDivider.setVisibility(View.GONE);
                mPortfolioBtn.setVisibility(View.GONE);
                mPortfolioIcon.setVisibility(View.GONE);
            } else if (!permissions.isDashboardExploreEnabled()) {
                bottomBarDivider.setVisibility(View.GONE);
                mExploreBtn.setVisibility(View.GONE);
                mExploreIcon.setVisibility(View.GONE);
            } else {
                floatingBottomBar.setVisibility(View.VISIBLE);
                bottomBarDivider.setVisibility(View.VISIBLE);
                mExploreBtn.setVisibility(View.VISIBLE);
                mExploreIcon.setVisibility(View.VISIBLE);
                mPortfolioBtn.setVisibility(View.VISIBLE);
                mPortfolioIcon.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onLoadScoreSuccess(HotelMetricsResponse response) {
        mDashboardPresenter.setUpMetrics(response.data);
    }

    @Override
    public void onLoadScoreFailure(String msg) {

    }

    @Override
    public void onLoadTasksSuccess(HotelTasksResponse response) {
        mDashboardPresenter.setUpTaskAdapterData(response.data);
    }

    @Override
    public void onLoadTasksFailure(String msg) {

    }

    @Override
    public void onTaskDoneSuccess() {

    }

    @Override
    public void onTaskDoneFailure(int taskID) {
        TaskModel failedTask = findTaskInList(taskID);
        if (failedTask != null) {
            int index = getIndexOfTaskInList(failedTask);
            failedTask.isPendingDelete = false;
            failedTask.isSoftDeleted = false;
            if (index != -1)
                mMasterTaskList.set(index, failedTask);
            refreshSectionedList();
            mAdapter.refreshList(!isFromExplore);
        }
    }

    @Override
    public void setMetricsData(HotelDashboardMetrics metrics) {
        mComplianceScoreInteger = metrics.getCompliance().getScore();
        Utils.animateTextView(this, 0, metrics.getCompliance().getScore(), mComplianceScore, true,
                R.dimen.textsize_28);
        Utils.animateTextView(this, 0, metrics.getDelightPercent().getScore(), mDelightScore, true,
                R.dimen.textsize_28);
        Utils.animateTextView(this, 0, metrics.getDisasterPercent().getScore(), mDisasterScore, true,
                R.dimen.textsize_28);

        mComplianceScore.setTextColor(ContextCompat.getColor(this,
                Utils.getMetricDisplayColor(metrics.getCompliance().getColor())));
        mDelightScore.setTextColor(ContextCompat.getColor(this,
                Utils.getMetricDisplayColor(metrics.getDelightPercent().getColor())));
        mDisasterScore.setTextColor(ContextCompat.getColor(this,
                Utils.getMetricDisplayColor(metrics.getDisasterPercent().getColor())));

        mComplianceChange.setText(String.valueOf(metrics.getCompliance().getDifference()));
        mDelightChange.setText(String.valueOf(metrics.getDelightPercent().getDifference()));
        mDisasterChange.setText(String.valueOf(metrics.getDisasterPercent().getDifference()));

        setUpDifferenceMetrics(mComplianceChange, mComplianceArrowIv, metrics.getCompliance().getDifference());
        setUpDifferenceMetrics(mDelightChange, mDelightArrowIv, metrics.getDelightPercent().getDifference());
        setUpDifferenceMetrics(mDisasterChange, mDisasterArrowIv, metrics.getDisasterPercent().getDifference());

    }

    private void setUpDifferenceMetrics(TreeboTextView textView, ImageView imageView, int diff) {
        imageView.setVisibility((diff == 0) ? View.GONE : View.VISIBLE);
        if (diff < 0) {
            imageView.setImageResource(R.drawable.ic_arrow_downward);
            diff *= -1;
        } else if (diff > 0) {
            imageView.setImageResource(R.drawable.ic_arrow_upward);
        }
        textView.setText(String.valueOf(diff));
    }

    @Override
    public void setUpAdapterData(HotelTasksResponse.TaskList taskList) {
        mSectionedTaskList.clear();
        mMasterTaskList.clear();
        // Adding Dummy Task at the bottom
        TaskModel dummyTask = new TaskModel();
        dummyTask.setTaskCardType(getString(R.string.bottom_dummy_task));
        if (taskList.todayTasks != null && taskList.todayTasks.size() > 0) {
            taskList.todayTasks.add(dummyTask);
        } else if (taskList.pendingTasks != null && taskList.pendingTasks.size() > 0) {
            taskList.pendingTasks.add(dummyTask);
        } else if (taskList.hotTasks != null && taskList.hotTasks.size() > 0) {
            taskList.hotTasks.add(dummyTask);
        }
        if (taskList.hotTasks != null && taskList.hotTasks.size() > 0) {
            SectionedTaskListModel model = new SectionedTaskListModel();
            model.setSectionName(getString(R.string.hot_task));
            model.setTaskList(taskList.hotTasks);
            for (int i = 0; i < taskList.hotTasks.size(); i++)
                taskList.hotTasks.get(i).sectionName = getString(R.string.hot_task);
            mMasterTaskList.addAll(taskList.hotTasks);
            mSectionedTaskList.add(model);
        }

        if (taskList.pendingTasks != null && taskList.pendingTasks.size() > 0) {
            SectionedTaskListModel model1 = new SectionedTaskListModel();
            model1.setSectionName(getString(R.string.pending_task_string));
            model1.setTaskList(taskList.pendingTasks);
            for (int i = 0; i < taskList.pendingTasks.size(); i++)
                taskList.pendingTasks.get(i).sectionName = getString(R.string.pending_task_string);
            mMasterTaskList.addAll(taskList.pendingTasks);
            mSectionedTaskList.add(model1);
        }

        if (taskList.todayTasks != null && taskList.todayTasks.size() > 0) {
            SectionedTaskListModel model2 = new SectionedTaskListModel();
            model2.setSectionName(getString(R.string.today_task));
            model2.setTaskList(taskList.todayTasks);
            for (int i = 0; i < taskList.todayTasks.size(); i++)
                taskList.todayTasks.get(i).sectionName = getString(R.string.today_task);
            mMasterTaskList.addAll(taskList.todayTasks);
            mSectionedTaskList.add(model2);
        }
        mDisplayTaskList.clear();
        if (mSectionedTaskList.size() == 0 || areAllTasksComplete()) {
            if (isEmptyViewPermissionEnabled) {
                mEmptyView.setVisibility(View.VISIBLE);
                mTasksRecyclerView.setVisibility(View.GONE);
            }
        } else {
            for (SectionedTaskListModel model : mSectionedTaskList) {
                mDisplayTaskList.addAll(model.getTaskList());
            }
        }

        mAdapter.refreshList(!isFromExplore);
    }

    private void refreshSectionedList() {
        for (int i = 0; i < mSectionedTaskList.size(); i++) {
            SectionedTaskListModel model = mSectionedTaskList.get(i);
            model.clearTaskList();
            ArrayList<TaskModel> list = new ArrayList<>();
            for (TaskModel task : mMasterTaskList) {
                if (task.sectionName.equals(model.getSectionName()) && !task.isSoftDeleted)
                    list.add(task);
            }
            model.setTaskList(list);
            mSectionedTaskList.set(i, model);
        }
        mDisplayTaskList.clear();
        for (SectionedTaskListModel model : mSectionedTaskList) {
            mDisplayTaskList.addAll(model.getTaskList());
        }
        if (mDisplayTaskList.size() == 0 || areAllTasksComplete()) {
            if (isEmptyViewPermissionEnabled) {
                mEmptyView.setVisibility(View.VISIBLE);
                mTasksRecyclerView.setVisibility(View.GONE);
            }
        }
    }

    private boolean areAllTasksComplete() {
        return mMasterTaskList.size() == 1
                && mMasterTaskList.get(0).getTaskStatus().equals(getString(R.string.complete).toLowerCase());
    }

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (viewHolder.getItemViewType() == Constants.MANUAL_TASK) {
                    int position = viewHolder.getAdapterPosition();
                    int subtraction;
                    if (position <= mAdapter.getItemCount(0))
                        subtraction = 1;
                    else if (mAdapter.getSectionCount() == 3 &&
                            (position > (mAdapter.getItemCount(0) + mAdapter.getItemCount(1))) && (position > (mAdapter.getItemCount(1) + mAdapter.getItemCount(2))))
                        subtraction = 3;
                    else
                        subtraction = 2;
                    TaskModel task = mDisplayTaskList.get(Math.max(0, viewHolder.getAdapterPosition() - subtraction));
                    if ((!task.isGeofenceEnabled() || (isInHotelLocation && task.isGeofenceEnabled())) && Utils.isInternetConnected(getApplicationContext()))
                        onDoneBtnClick(task);
                    else
                        mAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                } else {
                    mAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE && viewHolder.getItemViewType() == Constants.MANUAL_TASK) {

                    Bitmap icon;
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    Paint p = new Paint();

                    if (dX < 0) {
                        p.setColor(ContextCompat.getColor(recyclerView.getContext(), R.color.green_color_primary));
                        c.drawRect((float) itemView.getRight() + dX, (float) itemView.getTop(),
                                (float) itemView.getRight(), (float) itemView.getBottom(), p);
                        // TODO Complete the logic
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mTasksRecyclerView);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(Utils.IS_FROM_EXPLORE, isFromExplore);
        outState.putInt(Utils.HOTEL_ID, mHotelId);
        outState.putString(Utils.HOTEL_NAME, mHotelName);
        outState.putBoolean(Utils.IS_IN_HOTEL, isInHotelLocation);
        outState.putBoolean(Utils.IS_DASHBOARD_EMPTY_SCREEN_ENABLED, isEmptyViewPermissionEnabled);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void onDoneBtnClick(TaskModel task) {
        if (Utils.isInternetConnected(this)) {
            int index = getIndexOfTaskInList(task);
            task.isPendingDelete = true;
            if (index != -1)
                mMasterTaskList.set(index, task);
            refreshSectionedList();
            mAdapter.refreshList(!isFromExplore);
            new Handler().postDelayed(() -> {
                if (!isUndoClicked) {
                    task.isSoftDeleted = true;
                    mDashboardPresenter.onTaskDone(task.getTaskID(), task.getTaskLogID(), mLoginManager.getUserId(), mHotelId);
                    SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.TASK_DONE_CLICK,
                            new Properties().putValue(SegmentAnalyticsManager.TASK_CARD_ID, task.getTaskID()));
                } else {
                    task.isPendingDelete = false;
                    task.isSoftDeleted = false;
                }
                refreshSectionedList();
                mAdapter.refreshList(!isFromExplore);
                isUndoClicked = false;
            }, 2000);
        } else
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onPeriodicTaskCardClicked(TaskModel task) {
        if (task.getAuditTaskType() == Constants.FRAUD_AUDIT_TASK_TYPE) {
            mNavigator.navigateToFraudAuditActivity(this, mHotelId, task, mHotelName, isInHotelLocation);
        } else {
            mNavigator.navigateToPeriodicActivity(this, task, mHotelId, mHotelName, isInHotelLocation);
        }
    }

    @Override
    public void onToolTipClick(TaskModel task) {
        mExtraInformationViewTitle.setText(task.getHeading());
        mExtraInformationViewText.setText(task.getToolTipText());
        toggleTooltipLayout(true);
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.TOOLTIP_CLICK,
                new Properties().putValue(SegmentAnalyticsManager.TASK_CARD_ID, task.getTaskID())
                        .putValue(SegmentAnalyticsManager.IS_IN_GEOFENCE, isInHotelLocation)
                        .putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                        .putValue(SegmentAnalyticsManager.TASK_TYPE, task.getTaskTypeString()));
    }


    @Override
    public void onRestartAudit(TaskModel task) {
        showConfirmationDialog(task, false);
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_ACTION_CLICK,
                new Properties()
                        .putValue(SegmentAnalyticsManager.AUDIT_CARD_ACTION, SegmentAnalyticsManager.ACTION_TRASH)
                        .putValue(SegmentAnalyticsManager.AUDIT_TYPE, task.getHeading()));
    }

    @Override
    public void onResumeAudit(TaskModel task) {
        startAuditActivity(false, task);
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_ACTION_CLICK,
                new Properties()
                        .putValue(SegmentAnalyticsManager.AUDIT_CARD_ACTION, SegmentAnalyticsManager.ACTION_RESUME)
                        .putValue(SegmentAnalyticsManager.AUDIT_TYPE, task.getHeading()));
    }

    @Override
    public void onPeriodicRestartAudit(TaskModel task) {
        showConfirmationDialog(task, true);
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_ACTION_CLICK,
                new Properties()
                        .putValue(SegmentAnalyticsManager.AUDIT_CARD_ACTION, SegmentAnalyticsManager.ACTION_TRASH)
                        .putValue(SegmentAnalyticsManager.AUDIT_TYPE, task.getHeading()));
    }

    @Override
    public void onPeriodicResumeAudit(TaskModel task) {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_ACTION_CLICK,
                new Properties()
                        .putValue(SegmentAnalyticsManager.AUDIT_CARD_ACTION, SegmentAnalyticsManager.ACTION_RESUME)
                        .putValue(SegmentAnalyticsManager.AUDIT_TYPE, task.getHeading()));
        if (task.getAuditTaskType() == Constants.FRAUD_AUDIT_TASK_TYPE) {
            startFraudAuditActivity(false, task);
        } else {
            startPeriodicAuditActivity(false, task);
        }
    }

    @Override
    public void onStaffIncentivesTaskClicked(TaskModel task) {
        if (task.isDistribute()) {
            MonthlyIncentiveListResponse.MonthlyIncentiveListObject object
                    = new MonthlyIncentiveListResponse.MonthlyIncentiveListObject();
            object.setMonth(task.getMonth());
            object.setYear(task.getYear());
            object.setStatus(getString(R.string.status_pending));
            mNavigator.navigateToMonthlyDepartmentListActivity(this, object, mHotelId);
        } else {
            mClickedStaffIncentiveTaskLogID = task.getTaskLogID();
            String month = new DateFormatSymbols().getMonths()[task.getMonth() - 1];
            mNoIncentivesView.setVisibility(View.VISIBLE);
            mNoIncentivesSubtitle.setText(getString(R.string.no_incentive_subtext, "All Departments", month, task.getYear()));
            mAppBarView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAuditMoreRoomsClicked(TaskModel task) {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SELECT_ANOTHER_ROOM, new Properties());
        Intent intent = new Intent(this, RoomPickerActivity.class);
        intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        intent.putExtra(Utils.HOTEL_ID, mHotelId);
        intent.putExtra(Constants.TASK_MODEL, task);
        startActivity(intent);
    }

    @Override
    public void onCloseAuditMoreRoomsClicked(TaskModel task) {
        for (TaskModel taskModel : mMasterTaskList) {
            if (taskModel.getTaskCardType() == Constants.AUDIT_TASK_COMPLETED_STATE) {
                taskModel.isSoftDeleted = true;
                refreshSectionedList();
                mAdapter.refreshList(!isFromExplore);
            }
        }
    }

    private void startFraudAuditActivity(boolean trash, TaskModel task) {
        if (trash) {
            int fraudAuditCategoryID = AuditCategory.getAuditCategoryIDFromConstant(Constants.FRAUD_AUDIT_STRING);
            String key = AuditUtilsV3.createAuditKey(mHotelId, fraudAuditCategoryID, null);
            mAuditManager.removeAudit(key);
            mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.CLEAR_AUDIT));
        } else {
            mNavigator.navigateToFraudAuditActivity(this, mHotelId, task, mHotelName, isInHotelLocation);
        }
    }

    private void startPeriodicAuditActivity(boolean trash, TaskModel task) {
        int auditId = PeriodicStateUtils.getPausedCategoryAuditId(mAuditManager, mHotelId, task.getAuditTaskType());
        if (auditId != -1) {
            String key = AuditUtilsV3.createAuditKey(mHotelId, auditId, null);
            HashMap<Integer, ArrayList<Integer>> categoryMap = mAuditManager.getCachedCheckPoints(
                    AuditUtilsV3.createSavedCheckPointKey(mHotelId, auditId, null));
            ArrayList<Category> includedCheckPoints = AuditUtilsV3.checkpointList(categoryMap);
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.TASK_CARD_CLICK,
                    new Properties().putValue(SegmentAnalyticsManager.AUDIT_CARD_ACTION,
                            trash ? SegmentAnalyticsManager.ACTION_TRASH : SegmentAnalyticsManager.ACTION_RESUME));
            if (trash) {
                mAuditManager.removeAudit(key);
                mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.CLEAR_AUDIT));
            } else {
                if (Utils.isInternetConnected(this)) {
                    if (task.getVisualRepresentationType() == Constants.VISUAL_REPRESENTATION_TYPE_1) {
                        mNavigator.navigateToAuditActivity(this, includedCheckPoints, auditId, mHotelId, mHotelName,
                                isInHotelLocation, null, -1, true, task);
                    } else {
                        mNavigator.navigateToAuditType2Activity(this, task, auditId, includedCheckPoints, mHotelId, mHotelName,
                                isInHotelLocation, false);
                    }

                } else {
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    private void startAuditActivity(boolean thrash, TaskModel task) {
        String key = mAuditManager.getCurrentAuditKey();
        String auditType = AuditUtilsV3.getAuditTypeFromKey(key);
        int auditId = Integer.parseInt(AuditUtilsV3.getAuditIdFromKey(key));
        String room = null;
        if (auditType.equals(AuditTypeMap.ROOM_AUDIT)) {
            room = AuditUtilsV3.getRoomFromKey(key);
        } else if (auditType.equals(AuditTypeMap.COMMON_AUDIT)) {
            room = Constants.KEY_COMMON_CODE;
        }
        HashMap<Integer, ArrayList<Integer>> categoryMap = mAuditManager.getCachedCheckPoints(
                AuditUtilsV3.createSavedCheckPointKey(mHotelId, auditId, room));
        ArrayList<Category> includedCheckPoints = AuditUtilsV3.checkpointList(categoryMap);
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.TASK_CARD_CLICK,
                new Properties().putValue(SegmentAnalyticsManager.AUDIT_CARD_ACTION,
                        thrash ? SegmentAnalyticsManager.ACTION_TRASH : SegmentAnalyticsManager.ACTION_RESUME));
        if (thrash) {
            mAuditManager.removeAudit(key);
            mAuditManager.clearCurrentAuditKey();
            //clear the cache check points as we go to daily audit activity where we get the included_categories again
            //for periodic audits there is no endpoint call so do not clear cache there
            mAuditManager.removeCachedCheckPoints(AuditUtilsV3.createSavedCheckPointKey(mHotelId, auditId, room));
            mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.CLEAR_AUDIT));
            if (Utils.isInternetConnected(this)) {
               mNavigator.navigateToDailyAuditActivity(this, mHotelName, isInHotelLocation, task, mHotelId);
//                mNavigator.navigateToDailyAuditActivity(this, mHotelName, true, task, mHotelId);
            } else {
                Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
            }
        } else {
            if (Utils.isInternetConnected(this)) {
                mNavigator.navigateToAuditActivity(this, includedCheckPoints, auditId, mHotelId, mHotelName,
                        isInHotelLocation, room, mAuditManager.getCurrentRoomId(), true, task);
            } else {
                Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
            }

        }
    }


    @Override
    public void onStartAudit(TaskModel task) {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.TASK_CARD_CLICK,
                new Properties().putValue(SegmentAnalyticsManager.TASK_TYPE, SegmentAnalyticsManager.CARD_TYPE_AUDIT));
        if (Utils.isInternetConnected(this))
             mNavigator.navigateToDailyAuditActivity(this, mHotelName, isInHotelLocation, task, mHotelId);
//            mNavigator.navigateToDailyAuditActivity(this, mHotelName, true, task, mHotelId);
        else
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onIssueCardClicked(TaskModel taskModel) {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.TASK_CARD_CLICK,
                new Properties().putValue(SegmentAnalyticsManager.TASK_TYPE, SegmentAnalyticsManager.CARD_TYPE_ISSUE));
        SegmentAnalyticsManager.trackScreenOnSegment(SegmentAnalyticsManager.ISSUE_LOG_VIEW,
                new Properties().putValue(SegmentAnalyticsManager.SOURCE_SCREEN, SegmentAnalyticsManager.TASK_CARD_CLICK));
        if (Utils.isInternetConnected(this))
            mNavigator.navigateToIssueListActivity(this, mHotelId, mHotelName, Utils.RESOLUTION_DATE, taskModel.getTaskLogID(), false);
        else
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onUndoBtnClicked(TaskModel task) {
        isUndoClicked = true;
    }

    @Override
    public void onManualTaskCardClicked(TaskModel task) {
        mCurrentTask = task;
        mCardInformationViewTitle.setText(task.getHeading());
        Spannable spannable = Utils.applyCustomLinkSpanToUrlSpan(this, task.getInfo(), task.getContentLink());
        mCardInformationViewText.setMovementMethod(LinkMovementMethod.getInstance());
        mCardInformationViewText.setText(spannable, TextView.BufferType.SPANNABLE);
        toggleCardInfoLayout(true);
    }

    private int getIndexOfTaskInList(TaskModel entryTask) {
        for (int i = 0; i < mMasterTaskList.size(); i++) {
            TaskModel task = mMasterTaskList.get(i);
            if (task.getTaskID() == entryTask.getTaskID())
                return i;
        }
        return -1;
    }

    private TaskModel findTaskInList(int taskID) {
        for (TaskModel task : mMasterTaskList) {
            if (task.getTaskID() == taskID)
                return task;
        }
        return null;
    }


    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

    private void showConfirmationDialog(TaskModel task, boolean isPeriodic) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_update_back, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog stopUpdateDialog = dialogBuilder.create();
        stopUpdateDialog.show();

        TreeboTextView text = (TreeboTextView) dialogView.findViewById(R.id.stop_update_text);
        text.setText(getString(R.string.trash_confirmation_text));

        TreeboTextView cancelBtn = (TreeboTextView) dialogView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(view -> stopUpdateDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            stopUpdateDialog.dismiss();
            if (isPeriodic) {
                if (task.getAuditTaskType() == Constants.FRAUD_AUDIT_TASK_TYPE) {
                    startFraudAuditActivity(true, task);
                } else {
                    startPeriodicAuditActivity(true, task);
                }
            } else {
                startAuditActivity(true, task);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
        if (mLoginManager.getServerAppVersion() > BuildConfig.VERSION_CODE) {
            mNavigator.navigateToBlockerScreen(this);
        } else {
            Bundle savedBundle = getIntent().getBundleExtra(Utils.DASHBOARD_BUNDLE);
            if (savedBundle != null) {
                mHotelId = savedBundle.getInt(Utils.HOTEL_ID, -1);
                isFromExplore = savedBundle.getBoolean(Utils.IS_FROM_EXPLORE);
                mHotelName = savedBundle.getString(Utils.HOTEL_NAME);
                isInHotelLocation = savedBundle.getBoolean(Utils.IS_IN_HOTEL, false);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
        isActivityPaused = true;
        Bundle outState = new Bundle();
        outState.putBoolean(Utils.IS_FROM_EXPLORE, isFromExplore);
        outState.putInt(Utils.HOTEL_ID, mHotelId);
        outState.putString(Utils.HOTEL_NAME, mHotelName);
        outState.putBoolean(Utils.IS_IN_HOTEL, isInHotelLocation);
        getIntent().putExtra(Utils.DASHBOARD_BUNDLE, outState);
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        if (Utils.isInternetConnected(this)) {
            mDashboardPresenter.loadTasksForHotel(mHotelId, mLoginManager.getUserId());
            mDashboardPresenter.loadScoresForHotel(mHotelId, mLoginManager.getUserId());
        } else {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
        }
    }

}
