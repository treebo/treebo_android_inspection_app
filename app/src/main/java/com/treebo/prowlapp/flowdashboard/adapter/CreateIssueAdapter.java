package com.treebo.prowlapp.flowdashboard.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Utils.IssueFormatUtils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 29/06/17.
 */

public class CreateIssueAdapter extends RecyclerView.Adapter<CreateIssueAdapter.CreateIssueViewHolder> {

    private onAuditTypeSelected mListener;
    private ArrayList<AuditCategory> mList;
    private Context mContext;

    public CreateIssueAdapter(Context context,
                              ArrayList<AuditCategory> list, onAuditTypeSelected listener) {
        this.mContext = context;
        this.mList = list;
        this.mListener = listener;
    }

    @Override
    public CreateIssueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_create_issue,
                parent, false);
        return new CreateIssueViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(CreateIssueViewHolder holder, int position) {
        AuditCategory category = mList.get(position);
        holder.auditCategory = category;
        holder.text.setText(category.mAuditName);
        holder.icon.setImageDrawable(IssueFormatUtils.getTintedIconForAuditType(mContext,
                category.mAuditCategoryId, R.color.green_color_primary));
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public class CreateIssueViewHolder extends RecyclerView.ViewHolder {

        private AuditCategory auditCategory;
        private ImageView icon;
        private TreeboTextView text;

        public CreateIssueViewHolder(View itemView, onAuditTypeSelected listener) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.ic_audit_type);
            text = (TreeboTextView) itemView.findViewById(R.id.item_header_tv);
            itemView.setOnClickListener(view ->
                    listener.onAuditTypeSelected(auditCategory));
        }
    }

    public interface onAuditTypeSelected {

        void onAuditTypeSelected(AuditCategory auditCategory);
    }
}
