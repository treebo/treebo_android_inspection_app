package com.treebo.prowlapp.flowdashboard.periodicstate;

import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.Utils.DateUtils;

import java.text.ParseException;

/**
 * Created by sumandas on 17/01/2017.
 */

public abstract class BasePeriodicAuditCard implements IPeriodicAuditCard {

    TaskModel mTaskModel;

    public BasePeriodicAuditCard(TaskModel taskModel) {
        mTaskModel = taskModel;
    }


    public boolean isEnabled() throws ParseException {
        if (mTaskModel.getStartTime() == null || mTaskModel.getEndTime() == null) {
            return true;
        } else {
            return DateUtils.isTimeWithin(mTaskModel.getStartTime(), mTaskModel.getEndTime());
        }
    }

    public boolean isAlwaysVisible() {
        return mTaskModel.isAlwaysVisible();
    }

    public int isAuditPaused(AuditPrefManagerV3 auditManager, int hotelId, TaskModel taskModel) {
         return PeriodicStateUtils.getStateOfCard(auditManager, hotelId, taskModel);
    }


}
