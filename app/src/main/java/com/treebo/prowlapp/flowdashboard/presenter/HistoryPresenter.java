package com.treebo.prowlapp.flowdashboard.presenter;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.observers.TaskHistoryObserver;
import com.treebo.prowlapp.flowdashboard.usecase.TaskHistoryUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class HistoryPresenter implements DashboardContract.ITaskHistoryPresenter {

    private DashboardContract.ITaskHistoryView mView;

    private TaskHistoryUseCase mUseCase;

    @Override
    public void fetchTaskHistory(int userID) {
        mView.showLoading();
        mUseCase.setUserID(userID);
        mUseCase.execute(providesTaskHistoryObserver());
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (DashboardContract.ITaskHistoryView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    public void setTaskHistoryUseCase(TaskHistoryUseCase useCase) {
        this.mUseCase = useCase;
    }

    private TaskHistoryObserver providesTaskHistoryObserver() {
        return new TaskHistoryObserver(this, mView, mUseCase);
    }
}
