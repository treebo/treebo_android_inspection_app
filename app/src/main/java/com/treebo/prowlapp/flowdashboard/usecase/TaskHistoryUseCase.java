package com.treebo.prowlapp.flowdashboard.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.dashboard.TaskHistoryResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class TaskHistoryUseCase extends UseCase<TaskHistoryResponse> {

    RestClient mRestClient;

    int mUserID;


    public TaskHistoryUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setUserID(int userID) {
        this.mUserID = userID;
    }

    @Override
    protected Observable<TaskHistoryResponse> getObservable() {
        return mRestClient.getTaskHistory(mUserID);
    }
}
