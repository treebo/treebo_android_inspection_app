package com.treebo.prowlapp.flowdashboard.adapter;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 29/06/17.
 */

public class SelectRoomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<AuditAreaResponse.AuditAreaDetails> mAllRoomList;
    private ArrayList<AuditAreaResponse.AuditAreaDetails> mDisplayList;
    private onRoomClickListener mAuditRoomListener;

    public SelectRoomAdapter(onRoomClickListener roomClickListener,
                             ArrayList<AuditAreaResponse.AuditAreaDetails> rooms) {
        mAllRoomList = rooms;
        mDisplayList = rooms;
        mAuditRoomListener = roomClickListener;
    }

    public void filter(String text) {
        ArrayList<AuditAreaResponse.AuditAreaDetails> list = new ArrayList<>();
        if (TextUtils.isEmpty(text)) {
            list = mAllRoomList;
        } else {
            for (AuditAreaResponse.AuditAreaDetails details: mAllRoomList) {
                if (details.getRoomNumber().toLowerCase().contains(text.toLowerCase())) {
                    list.add(details);
                }
            }
        }
        mDisplayList = list;
        notifyDataSetChanged();
    }

    public void setData(ArrayList<AuditAreaResponse.AuditAreaDetails> rooms) {
        mAllRoomList = rooms;
        mDisplayList = rooms;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_room,
                parent, false);
        return new OccupiedRoomViewHolder(view, mAuditRoomListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // Set fields as per model
        AuditAreaResponse.AuditAreaDetails room = mDisplayList.get(position);
        ((OccupiedRoomViewHolder) holder).room = room;
        ((OccupiedRoomViewHolder) holder).mRoomNumber.setText(room.getRoomNumber());
    }

    @Override
    public int getItemCount() {
        return mDisplayList != null ? mDisplayList.size() : 0;
    }

    private class OccupiedRoomViewHolder extends RecyclerView.ViewHolder {

        AuditAreaResponse.AuditAreaDetails room;
        TreeboTextView mRoomNumber;
        TreeboTextView mRoomType;

        OccupiedRoomViewHolder(View itemView, onRoomClickListener listener) {
            super(itemView);
            mRoomNumber = (TreeboTextView) itemView.findViewById(R.id.room_number_tv);
            mRoomType = (TreeboTextView) itemView.findViewById(R.id.room_type_tv);
            itemView.setOnClickListener(view -> listener.onRoomClicked(room));
        }
    }

    public interface onRoomClickListener {
        void onRoomClicked(AuditAreaResponse.AuditAreaDetails room);
    }
}

