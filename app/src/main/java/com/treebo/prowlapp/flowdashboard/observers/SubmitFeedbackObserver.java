package com.treebo.prowlapp.flowdashboard.observers;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.usecase.FeedbackSubmitUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class SubmitFeedbackObserver extends BaseObserver<BaseResponse> {

    private FeedbackSubmitUseCase mUseCase;
    private DashboardContract.ISubmitFeedbackView mView;

    public SubmitFeedbackObserver(BasePresenter presenter, DashboardContract.ISubmitFeedbackView view,
                                  FeedbackSubmitUseCase useCase) {
        super(presenter, useCase);
        this.mView = view;
        this.mUseCase = useCase;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(BaseResponse baseResponse) {
        mView.hideLoading();
        if (baseResponse.status.equals(Constants.STATUS_SUCCESS))
            mView.onSubmitSuccess();
        else
            mView.onSubmitFailure(baseResponse.msg);
    }
}
