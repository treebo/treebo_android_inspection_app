package com.treebo.prowlapp.flowdashboard.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;

import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding.view.RxView;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowdashboard.DaggerDashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.presenter.SubmitFeedbackPresenter;
import com.treebo.prowlapp.flowdashboard.usecase.FeedbackSubmitUseCase;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class SubmitFeedbackActivity extends AppCompatActivity implements DashboardContract.ISubmitFeedbackView,
        TextWatcher {

    @Inject
    public SubmitFeedbackPresenter mPresenter;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public FeedbackSubmitUseCase mUseCase;

    private TreeboTextView mUpdateCommentTitle;
    private TreeboEditText mUpdateCommentText;
    private TreeboButton mDone;
    private View mCommentErrorText;
    private boolean submitted;
    private View mSuccessView;

    CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_feedback);
        setToolBarColor(R.color.white);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);

        View toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        TreeboTextView title = (TreeboTextView) toolbar.findViewById(R.id.toolbar_title_text);
        title.setText(getString(R.string.feedback_string));
        View crossBtn = toolbar.findViewById(R.id.room_picker_back);
        crossBtn.setOnClickListener(view -> onBackPressed());

        mSuccessView = findViewById(R.id.audit_success_layout);
        TreeboTextView textView = (TreeboTextView) mSuccessView.findViewById(R.id.audit_successfully_submitted);
        textView.setText(getString(R.string.feedback_submit_success));

        mCommentErrorText = findViewById(R.id.feedback_error_tv);
        mUpdateCommentTitle = (TreeboTextView) findViewById(R.id.feedback_title_text);
        mUpdateCommentText = (TreeboEditText) findViewById(R.id.feedback_edittext);
        mUpdateCommentText.addTextChangedListener(this);
        mUpdateCommentText.setOnClickListener(view -> {
            mUpdateCommentText.setHint("");
            setEditTextBackGround(true);
        });
        mUpdateCommentText.setOnFocusChangeListener((view, b) -> {
            if (!b)
                Utils.hideSoftKeyboard(findViewById(android.R.id.content));
        });

        mDone = (TreeboButton) findViewById(R.id.feedback_submit_btn);

        mSubscription.add(RxView.clicks(mDone).throttleFirst(15, TimeUnit.SECONDS).subscribe(s -> {
            if (mUpdateCommentText.getText().length() > 0) {
                SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.FEEDBACK_SUBMIT, new Properties());
                Utils.hideSoftKeyboard(findViewById(android.R.id.content));
                if (Utils.isInternetConnected(this))
                    mPresenter.postFeedback(mLoginManager.getUserId(), mUpdateCommentText.getText().toString());
                else
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
            } else {
                setError(true);
            }
        }));

        DashboardComponent dashboardComponent = DaggerDashboardComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        dashboardComponent.injectFeedbackActivity(this);
        mUpdateCommentText.performClick();
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setSubmitUseCase(mUseCase);
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(mUpdateCommentText.getText()) && !submitted)
            showStopUpdateDialog();
        else {
            finish();
            overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        }
    }

    private void setEditTextBackGround(boolean enabled) {
        if (!enabled) {
            mUpdateCommentTitle.setVisibility(View.GONE);
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle_border_gray));
        } else {
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle_border_green));
            mUpdateCommentTitle.setVisibility(View.VISIBLE);
        }
    }

    private void setError(boolean isError) {
        if (isError) {
            mCommentErrorText.setVisibility(View.VISIBLE);
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rectangle_border_red));
        } else {
            mCommentErrorText.setVisibility(View.GONE);
            mUpdateCommentText.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_edittext_selector));
        }
    }

    @Override
    public void onSubmitSuccess() {
        submitted = true;
        mDone.setVisibility(View.GONE);
        mSuccessView.setVisibility(View.VISIBLE);
        setToolBarColor(R.color.green_color_primary);
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            onBackPressed();
        }, 2000);
    }

    @Override
    public void onSubmitFailure(String msg) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable.length() == 0) {
            mUpdateCommentText.setHint(getString(R.string.what_can_we_do_better));
            setEditTextBackGround(false);
        } else if (editable.length() > 0 && editable.length() < 2) {
            setError(false);
            setEditTextBackGround(true);
        } else {
            setEditTextBackGround(true);
        }
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    private void showStopUpdateDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_update_back, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog stopUpdateDialog = dialogBuilder.create();
        stopUpdateDialog.show();

        TreeboTextView cancelBtn = (TreeboTextView) dialogView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(view -> stopUpdateDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            stopUpdateDialog.dismiss();
            finish();
            overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }
}
