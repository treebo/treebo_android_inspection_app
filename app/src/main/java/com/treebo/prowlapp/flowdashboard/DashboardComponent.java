package com.treebo.prowlapp.flowdashboard;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowdashboard.activity.ContactInfoActivity;
import com.treebo.prowlapp.flowdashboard.activity.CreateIssueActivity;
import com.treebo.prowlapp.flowdashboard.activity.DashboardActivity;
import com.treebo.prowlapp.flowdashboard.activity.ExploreActivity;
import com.treebo.prowlapp.flowdashboard.activity.HistoryActivity;
import com.treebo.prowlapp.flowdashboard.activity.SubmitFeedbackActivity;

import dagger.Component;

/**
 * Created by sumandas on 20/11/2016.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, DashBoardModule.class})
public interface DashboardComponent {
    void injectDashboardActivity(DashboardActivity dashboardActivity);

    void injectFeedbackActivity(SubmitFeedbackActivity feedbackActivity);

    void injectExploreActivity(ExploreActivity exploreActivity);

    void injectHistoryActivity(HistoryActivity historyActivity);

    void injectContactInfoActivity(ContactInfoActivity contactInfoActivity);

    void injectCreateIssueActivity(CreateIssueActivity createIssueActivity);
}
