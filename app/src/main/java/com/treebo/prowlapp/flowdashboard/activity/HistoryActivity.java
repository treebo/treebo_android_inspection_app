package com.treebo.prowlapp.flowdashboard.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.WindowManager;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowdashboard.DaggerDashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.adapter.TaskHistorySectionedAdapter;
import com.treebo.prowlapp.flowdashboard.presenter.HistoryPresenter;
import com.treebo.prowlapp.flowdashboard.usecase.TaskHistoryUseCase;
import com.treebo.prowlapp.Models.dashboardmodel.SectionedHistoryTaskListModel;
import com.treebo.prowlapp.Models.dashboardmodel.TaskHistoryModel;
import com.treebo.prowlapp.response.dashboard.TaskHistoryResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class HistoryActivity extends AppCompatActivity implements DashboardContract.ITaskHistoryView {

    @Inject
    public HistoryPresenter mPresenter;

    @Inject
    public TaskHistoryUseCase mUseCase;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    private ArrayList<SectionedHistoryTaskListModel> mSectionedList = new ArrayList<>();
    private ArrayList<TaskHistoryModel> mTaskHistoryList;
    private RecyclerView mTaskRecyclerView;
    private TaskHistorySectionedAdapter mAdapter;

    private View mLoadingView;
    private TreeboTextView bottomText;

    @Override
    public void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        setContentView(R.layout.activity_history);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.activity_gray_background));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        DashboardComponent dashboardComponent = DaggerDashboardComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        dashboardComponent.injectHistoryActivity(this);

        bottomText = (TreeboTextView) findViewById(R.id.history_bottom_text);
        mLoadingView = findViewById(R.id.loader_layout);

        View backBtn = findViewById(R.id.history_back_iv);
        backBtn.setOnClickListener(view -> onBackPressed());

        mTaskRecyclerView = (RecyclerView) findViewById(R.id.task_history_rv);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mTaskRecyclerView.setLayoutManager(layoutManager1);
        mTaskRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new TaskHistorySectionedAdapter(this, mSectionedList);
        mTaskRecyclerView.setAdapter(mAdapter);

        mPresenter.fetchTaskHistory(mLoginManager.getUserId());
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setTaskHistoryUseCase(mUseCase);
    }

    @Override
    public void onLoadSuccess(TaskHistoryResponse.TaskHistory response) {
        if (response != null) {
            bottomText.setText(getString(R.string.bottom_text, response.userName));
            mAdapter.setFields(response.userName, response.userProfilePic);
            mTaskHistoryList = response.taskList;
            getSectionedList();
            mAdapter.notifyDataSetChanged();
        }
    }

    private void getSectionedList() {
        Set<String> sectionNames = getUniqueValues(mTaskHistoryList);
        for (int i = 0; i < sectionNames.size(); i++) {
            ArrayList<TaskHistoryModel> list = new ArrayList<>();
            SectionedHistoryTaskListModel model = new SectionedHistoryTaskListModel();
            String date = String.valueOf(sectionNames.toArray()[i]);
            model.setSectionName(DateUtils.getUpdatedAtDateComparedToToday(date));
            for (TaskHistoryModel issue : mTaskHistoryList) {
                if (issue.getUpdatedAt().equals(date))
                    list.add(issue);
            }
            model.setmTaskHistory(list);
            mSectionedList.add(model);
        }
    }

    public static Set<String> getUniqueValues(ArrayList<TaskHistoryModel> taskList) {
        Set<String> uniqueValues = new HashSet<String>();
        for (TaskHistoryModel issue : taskList) {
            uniqueValues.add(issue.getUpdatedAt());
        }
        return uniqueValues;
    }

    @Override
    public void onLoadFailure(String msg) {

    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }
}
