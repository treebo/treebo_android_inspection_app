package com.treebo.prowlapp.flowdashboard.observers;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.usecase.HotelMetricUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.dashboard.HotelMetricsResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class HotelMetricObserver extends BaseObserver<HotelMetricsResponse> {

    HotelMetricUseCase mUseCase;
    DashboardContract.IDashboardView mView;

    public HotelMetricObserver(BasePresenter presenter, DashboardContract.IDashboardView view,
                               HotelMetricUseCase useCase) {
        super(presenter, useCase);
        mView = view;
        mUseCase = useCase;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(HotelMetricsResponse response) {
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mView.onLoadScoreSuccess(response);
        } else {
            mView.onLoadScoreFailure("Loading metrics failed");
        }
    }
}
