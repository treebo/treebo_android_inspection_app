package com.treebo.prowlapp.flowdashboard.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.view.WindowManager;

import com.google.android.material.snackbar.Snackbar;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.PermissionsUpdateEvent;
import com.treebo.prowlapp.flowdashboard.DaggerDashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardComponent;
import com.treebo.prowlapp.flowdashboard.adapter.ExploreAdapter;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class ExploreActivity extends AppCompatActivity implements ExploreAdapter.onExploreItemClickListener {

    @Inject
    public Navigator mNavigator;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    private RecyclerView mExploreRecyclerView;
    private TreeboTextView mTitle;

    private ArrayList<String> mOptionsList;
    private int mHotelID;
    private String mHotelName;

    private final CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        DashboardComponent dashboardComponent = DaggerDashboardComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        dashboardComponent.injectExploreActivity(this);

        View crossBtn = findViewById(R.id.close_explore_iv);
        crossBtn.setOnClickListener(view -> onBackPressed());

        mHotelName = getIntent().getStringExtra(Utils.HOTEL_NAME);
        mTitle = (TreeboTextView) findViewById(R.id.explore_activity_title);
        mTitle.setText(mHotelName);

        mHotelID = getIntent().getIntExtra(Utils.HOTEL_ID, -1);

        mExploreRecyclerView = (RecyclerView) findViewById(R.id.explore_options_rv);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mExploreRecyclerView.setLayoutManager(layoutManager1);
        mExploreRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mOptionsList = setUpDisplayListAsPerUserPermissions();
        ExploreAdapter adapter = new ExploreAdapter(mOptionsList, this);
        mExploreRecyclerView.setAdapter(adapter);

        mSubscription.add(RxUtils.build(RxBus.getInstance().toObservable()).subscribe(event -> {
            if (event instanceof PermissionsUpdateEvent) {
                mOptionsList = setUpDisplayListAsPerUserPermissions();
                ExploreAdapter exploreAdapter = new ExploreAdapter(mOptionsList, this);
                mExploreRecyclerView.setAdapter(exploreAdapter);
            }
        }));

    }

    private ArrayList<String> setUpDisplayListAsPerUserPermissions() {
        RolesAndUserPermissions.PermissionList permissions = mLoginManager.getUserPermissionList(mHotelID);
        String[] optionsArray = getResources().getStringArray(R.array.explore_options);
        ArrayList<String> displayList =
                new ArrayList<>(Arrays.asList(optionsArray));
        if (permissions != null) {
            if (!permissions.isExploreIssueLogEnabled()) {
                displayList.remove(optionsArray[0]);
            }
            if (!permissions.isExploreCreateIssueEnabled()
                    || !mLoginManager.isIssueCreationEnabled()) {
                displayList.remove(optionsArray[1]);
            }
            if (!permissions.isExploreStaffDetailsEnabled()) {
                displayList.remove(optionsArray[2]);
            }
            if (!permissions.isExploreStaffIncentivesEnabled()) {
                displayList.remove(optionsArray[3]);
            }
            if (!permissions.isExploreHistoryEnabled()) {
                displayList.remove(optionsArray[4]);
            }
        }
        return displayList;
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down);
    }

    @Override
    public void onItemClicked(String position) {
        if (Utils.isInternetConnected(this)) {
            switch (position) {
                case "Issue log":
                    SegmentAnalyticsManager.trackScreenOnSegment(SegmentAnalyticsManager.ISSUE_LOG_VIEW,
                            new Properties().putValue(SegmentAnalyticsManager.SOURCE_SCREEN, SegmentAnalyticsManager.EXPLORE_CLICK));
                    mNavigator.navigateToIssueListActivity(this, mHotelID, mHotelName, Utils.SEVERITY_HIGH_LOW, 0, false);
                    break;

                case "Create an issue":
                    mNavigator.navigateToCreateIssueActivity(this, mHotelID, mHotelName);
                    break;

                case "Staff details":
                    // Go to staff details
                    HotelDataResponse response = new HotelDataResponse();
                    response.hotelId = mHotelID;
                    mNavigator.navigateToStaffActivity(this, response);
                    break;

                case "Staff incentives":
                    mNavigator.navigateToMonthlyIncentiveListActivity(this, mHotelID);
                    break;

                case "History":
                    mNavigator.navigateToHistoryActivity(this);
                    break;

            }
            finish();
        } else
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
    }
}
