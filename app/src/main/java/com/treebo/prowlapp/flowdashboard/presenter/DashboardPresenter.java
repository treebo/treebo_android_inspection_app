package com.treebo.prowlapp.flowdashboard.presenter;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.observers.HotelMetricObserver;
import com.treebo.prowlapp.flowdashboard.observers.HotelTaskObserver;
import com.treebo.prowlapp.flowdashboard.observers.TaskDoneObserver;
import com.treebo.prowlapp.flowdashboard.usecase.HotelMetricUseCase;
import com.treebo.prowlapp.flowdashboard.usecase.HotelTasksUseCase;
import com.treebo.prowlapp.flowdashboard.usecase.TaskDoneUseCase;
import com.treebo.prowlapp.Models.dashboardmodel.HotelDashboardMetrics;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.dashboard.HotelTasksResponse;

/**
 * Created by abhisheknair on 28/11/16.
 */

public class DashboardPresenter implements DashboardContract.IDashboardPresenter {

    private DashboardContract.IDashboardView mView;

    private HotelTasksUseCase mHotelTaskUseCase;

    private HotelMetricUseCase mHotelMetricUseCase;

    private TaskDoneUseCase mTaskDoneUseCase;

    private int mHotelID, mUserID;

    @Override
    public void loadTasksForHotel(int hotelID, int userID) {
        mView.showLoading();
        mHotelID = hotelID;
        mUserID = userID;
        mHotelTaskUseCase.setHotelAndUserID(hotelID, userID);
        mHotelTaskUseCase.execute(providesTaskListObserver());
    }

    @Override
    public void loadScoresForHotel(int hotelID, int userID) {
        mHotelMetricUseCase.setHotelAndUserID(hotelID, userID);
        mHotelMetricUseCase.execute(providesMetricsObserver());
    }

    @Override
    public void setUpMetrics(HotelDashboardMetrics metrics) {
        mView.setMetricsData(metrics);
    }

    @Override
    public void setUpTaskAdapterData(HotelTasksResponse.TaskList tasks) {
        mView.setUpAdapterData(tasks);
    }


    public void onTaskDone(int taskID, int taskLogID, int userID, int hotelID) {
        mTaskDoneUseCase.setFields(taskID, taskLogID, userID, hotelID, true);
        mTaskDoneUseCase.execute(providesTaskDoneObserver());
    }

    public void onTaskStarted(int taskID, int taskLogID, int userID, int hotelID) {
        mTaskDoneUseCase.setFields(taskID, taskLogID, userID, hotelID, false);
        mTaskDoneUseCase.execute(providesTaskDoneObserver());
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (DashboardContract.IDashboardView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {
        mView.setUpViewAsPerPermissions();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    public void setHotelTaskUseCase(HotelTasksUseCase useCase) {
        this.mHotelTaskUseCase = useCase;
    }

    private HotelTaskObserver providesTaskListObserver() {
        return new HotelTaskObserver(this, mView, mHotelTaskUseCase);
    }

    public void setMetricUseCase(HotelMetricUseCase useCase) {
        this.mHotelMetricUseCase = useCase;
    }

    private HotelMetricObserver providesMetricsObserver() {
        return new HotelMetricObserver(this, mView, mHotelMetricUseCase);
    }

    public void setTaskDoneUseCase(TaskDoneUseCase useCase) {
        this.mTaskDoneUseCase = useCase;
    }

    private TaskDoneObserver providesTaskDoneObserver() {
        return new TaskDoneObserver(this, mView, mTaskDoneUseCase);
    }
}
