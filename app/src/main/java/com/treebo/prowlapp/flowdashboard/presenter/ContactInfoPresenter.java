package com.treebo.prowlapp.flowdashboard.presenter;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.observers.ContactInfoObserver;
import com.treebo.prowlapp.flowdashboard.usecase.ContactInfoUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.dashboard.ContactInfoResponse;

/**
 * Created by abhisheknair on 21/04/17.
 */

public class ContactInfoPresenter implements DashboardContract.IContactInfoPresenter {

    private DashboardContract.IContactInfoView mView;

    private ContactInfoUseCase mUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        this.mView = (DashboardContract.IContactInfoView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return (BaseView) mView;
    }

    @Override
    public void fetchContactInfo() {
        mView.showLoading();
        mUseCase.execute(providesContactInfoObserver());
    }

    @Override
    public void fetchSuccess(ContactInfoResponse response) {
        mView.hideLoading();
        mView.setUpData(response);
    }

    @Override
    public void fetchFailure(String message) {
        mView.hideLoading();
        mView.showError(message);
    }

    public void setContactInfoUseCase(ContactInfoUseCase useCase) {
        this.mUseCase = useCase;
    }

    private ContactInfoObserver providesContactInfoObserver() {
        return new ContactInfoObserver(this, mUseCase);
    }
}
