package com.treebo.prowlapp.flowdashboard.observers;

import com.treebo.prowlapp.flowdashboard.presenter.ContactInfoPresenter;
import com.treebo.prowlapp.response.dashboard.ContactInfoResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 21/04/17.
 */

public class ContactInfoObserver extends BaseObserver<ContactInfoResponse> {

    private ContactInfoPresenter mPresenter;

    public ContactInfoObserver(ContactInfoPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(ContactInfoResponse response) {
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.fetchSuccess(response);
        } else {
            mPresenter.fetchFailure(response.msg);
        }
    }
}
