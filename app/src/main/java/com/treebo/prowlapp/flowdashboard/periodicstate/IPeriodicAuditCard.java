package com.treebo.prowlapp.flowdashboard.periodicstate;

/**
 * Created by sumandas on 13/01/2017.
 */

public interface IPeriodicAuditCard {
    int getAuditIcon();

    int getAuditIconTint();

    String getAuditText();

}
