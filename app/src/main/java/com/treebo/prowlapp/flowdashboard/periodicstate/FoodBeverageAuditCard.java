package com.treebo.prowlapp.flowdashboard.periodicstate;

import android.content.Context;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;

/**
 * Created by sumandas on 17/01/2017.
 */
public class FoodBeverageAuditCard extends BasePeriodicAuditCard implements IPeriodicAuditCard {

    private Context mContext;

    private TaskModel mTaskModel;

    public FoodBeverageAuditCard(Context context, TaskModel taskModel) {
        super(taskModel);
        mContext = context;
        mTaskModel = taskModel;
    }

    @Override
    public int getAuditIcon() {
        return R.drawable.ic_food_beverage_audit;
    }

    @Override
    public int getAuditIconTint() {
        return R.color.green_color_primary;
    }

    @Override
    public String getAuditText() {
        return mContext.getResources().getString(R.string.food_beverage_audit);
    }

}

