package com.treebo.prowlapp.flowdashboard.periodicstate;

import android.content.Context;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;

/**
 * Created by sumandas on 13/01/2017.
 */

public class BreakFastAuditCard extends BasePeriodicAuditCard implements IPeriodicAuditCard {

    private Context mContext;

    private TaskModel mTaskModel;

    public BreakFastAuditCard(Context context, TaskModel taskModel) {
        super(taskModel);
        mContext = context;
        mTaskModel = taskModel;
    }

    @Override
    public int getAuditIcon() {
        return R.drawable.ic_breakfast_audit;
    }

    @Override
    public int getAuditIconTint() {
        return R.color.green_color_primary;
    }

    @Override
    public String getAuditText() {
        return mContext.getResources().getString(R.string.breakfast_audit);
    }

}
