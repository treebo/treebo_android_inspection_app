package com.treebo.prowlapp.flowdashboard.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.dashboard.HotelTasksResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class HotelTasksUseCase extends UseCase<HotelTasksResponse> {

    RestClient mRestClient;

    int mUserID;

    int mHotelID;

    public HotelTasksUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setHotelAndUserID(int hotelID, int userID) {
        this.mUserID = userID;
        this.mHotelID = hotelID;
    }

    @Override
    protected Observable<HotelTasksResponse> getObservable() {
        return mRestClient.getTaskList(mUserID, mHotelID);
    }
}
