package com.treebo.prowlapp.flowdashboard.periodicstate;

import android.content.Context;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;

/**
 * Created by abhisheknair on 21/02/17.
 */

public class BackOfTheHouseAuditCard extends BasePeriodicAuditCard implements IPeriodicAuditCard {

    private Context mContext;

    private TaskModel mTaskModel;

    public BackOfTheHouseAuditCard(Context context, TaskModel taskModel) {
        super(taskModel);
        mContext = context;
        mTaskModel = taskModel;
    }

    @Override
    public int getAuditIcon() {
        return R.drawable.ic_back_of_the_house;
    }

    @Override
    public int getAuditIconTint() {
        return R.color.green_color_primary;
    }

    @Override
    public String getAuditText() {
        return mContext.getResources().getString(R.string.back_of_the_house_audit);
    }

}
