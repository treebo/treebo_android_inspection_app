package com.treebo.prowlapp.flowdashboard.observers;

import com.treebo.prowlapp.flowdashboard.presenter.CreateIssuePresenter;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 29/06/17.
 */

public class CreateIssueAuditCategoryObserver extends BaseObserver<AuditAreaResponse> {

    private CreateIssuePresenter mPresenter;

    public CreateIssueAuditCategoryObserver(CreateIssuePresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(AuditAreaResponse response) {
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onLoadSuccess(response);
        } else {
            mPresenter.onLoadFailure(response.msg);
        }
    }
}
