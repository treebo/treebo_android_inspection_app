package com.treebo.prowlapp.flowdashboard.presenter;

import com.treebo.prowlapp.flowauditnew.usecase.AuditCategoryUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomSingleUseCase;
import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.observers.CreateIssueAuditCategoryObserver;
import com.treebo.prowlapp.flowdashboard.observers.RoomChecklistObserver;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;

/**
 * Created by abhisheknair on 29/06/17.
 */

public class CreateIssuePresenter implements DashboardContract.ICreateIssuePresenter {

    private AuditCategoryUseCase mAuditCategoryUseCase;
    private RoomSingleUseCase mRoomSingleUseCase;
    private DashboardContract.ICreateIssueView mView;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (DashboardContract.ICreateIssueView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void onLoadSuccess(AuditAreaResponse response) {
        mView.hideLoading();
        mView.moveToCreateIssue(response);
    }

    @Override
    public void loadRoomAudit(int hotelId, int roomId) {
        mView.showLoading();
        mRoomSingleUseCase.setFields(hotelId, roomId);
        mRoomSingleUseCase.execute(providesRoomSingleObserver());
    }

    @Override
    public void onLoadRoomSuccess(RoomSingleResponse response) {
        mView.hideLoading();
        mView.createIssueForRoom(response);
    }

    @Override
    public void onLoadFailure(String errorMessage) {
        mView.hideLoading();
        mView.showError(errorMessage);
    }

    @Override
    public void loadAuditCategory(int hotelId, int auditId) {
        mView.showLoading();
        mAuditCategoryUseCase.setFields(hotelId, auditId);
        mAuditCategoryUseCase.execute(providesAuditObserver());
    }


    public void setAuditCategoryUseCase(AuditCategoryUseCase mAuditCategoryUseCase) {
        this.mAuditCategoryUseCase = mAuditCategoryUseCase;
    }

    public CreateIssueAuditCategoryObserver providesAuditObserver() {
        return new CreateIssueAuditCategoryObserver(this, mAuditCategoryUseCase);
    }

    public void setRoomSingleUseCase(RoomSingleUseCase roomSingleUseCase) {
        this.mRoomSingleUseCase = roomSingleUseCase;
    }

    public RoomChecklistObserver providesRoomSingleObserver() {
        return new RoomChecklistObserver(this, mRoomSingleUseCase);
    }
}
