package com.treebo.prowlapp.flowdashboard.auditstate;

import android.content.Context;
import android.view.View;

import com.treebo.prowlapp.R;

/**
 * Created by sumandas on 29/11/2016.
 */

public class StateCommonPaused implements IAuditState {

    private Context mContext;

    public StateCommonPaused(Context context) {
        mContext = context;
    }

    @Override
    public boolean isCardClickEnabled() {
        return false;
    }


    @Override
    public int getAuditIcon() {
        return R.drawable.ic_common_area_gray;
    }

    @Override
    public String getStateText() {
        return "";
    }

    @Override
    public String getRecommendedText() {
        return "";
    }

    @Override
    public String getAuditState() {
        return mContext.getResources().getString(R.string.paused);
    }

    @Override
    public int getRestartVisibility() {
        return View.VISIBLE;
    }

    @Override
    public int getResumeVisibility() {
        return View.VISIBLE;
    }

    @Override
    public int getStateVisibility() {
        return View.VISIBLE;
    }

    @Override
    public int getRecommendedVisibility() {
        return View.GONE;
    }


}
