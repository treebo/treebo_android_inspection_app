package com.treebo.prowlapp.flowdashboard.periodicstate;

/**
 * Created by sumandas on 17/01/2017.
 */

public interface IPeriodicStateContext {
    IPeriodicAuditCard getState();

    void setState(IPeriodicAuditCard state);

}
