package com.treebo.prowlapp.flowdashboard.observers;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.usecase.TaskDoneUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 30/11/16.
 */

public class TaskDoneObserver extends BaseObserver<BaseResponse> {

    private DashboardContract.IDashboardView mView;
    private TaskDoneUseCase mUseCase;

    public TaskDoneObserver(BasePresenter presenter, DashboardContract.IDashboardView view,
                            TaskDoneUseCase useCase) {
        super(presenter, useCase);
        mUseCase = useCase;
        mView = view;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(BaseResponse baseResponse) {
        if (baseResponse.status.equals(Constants.STATUS_SUCCESS))
            mView.onTaskDoneSuccess();
        else
            mView.onTaskDoneFailure(mUseCase.getTaskID());
    }
}
