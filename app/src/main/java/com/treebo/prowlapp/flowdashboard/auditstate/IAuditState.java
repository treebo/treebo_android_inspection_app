package com.treebo.prowlapp.flowdashboard.auditstate;

/**
 * Created by sumandas on 29/11/2016.
 */

public interface IAuditState {

    int getAuditIcon();
    String getStateText();
    String getAuditState();
    String getRecommendedText();
    boolean isCardClickEnabled();

    int getRestartVisibility();
    int getResumeVisibility();
    int getStateVisibility();
    int getRecommendedVisibility();

}
