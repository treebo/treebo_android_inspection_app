package com.treebo.prowlapp.flowdashboard.presenter;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.observers.SubmitFeedbackObserver;
import com.treebo.prowlapp.flowdashboard.usecase.FeedbackSubmitUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class SubmitFeedbackPresenter implements DashboardContract.ISubmitFeedbackPresenter {

    private DashboardContract.ISubmitFeedbackView mView;

    private FeedbackSubmitUseCase mUseCase;

    @Override
    public void postFeedback(int userID, String feedback) {
        mView.showLoading();
        mUseCase.setFields(userID, feedback);
        mUseCase.execute(providesSubmitObserver());

    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (DashboardContract.ISubmitFeedbackView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    public void setSubmitUseCase(FeedbackSubmitUseCase useCase) {
        this.mUseCase = useCase;
    }

    private SubmitFeedbackObserver providesSubmitObserver() {
        return new SubmitFeedbackObserver(this, mView, mUseCase);
    }
}
