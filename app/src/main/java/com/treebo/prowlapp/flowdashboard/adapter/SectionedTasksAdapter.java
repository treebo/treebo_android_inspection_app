package com.treebo.prowlapp.flowdashboard.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.events.AuditStateChangeEvent;
import com.treebo.prowlapp.events.IssueResolvedEventV3;
import com.treebo.prowlapp.flowdashboard.auditstate.IAuditState;
import com.treebo.prowlapp.flowdashboard.auditstate.IStateContext;
import com.treebo.prowlapp.flowdashboard.auditstate.StateAuditUtils;
import com.treebo.prowlapp.flowdashboard.periodicstate.BasePeriodicAuditCard;
import com.treebo.prowlapp.flowdashboard.periodicstate.IPeriodicAuditCard;
import com.treebo.prowlapp.flowdashboard.periodicstate.IPeriodicStateContext;
import com.treebo.prowlapp.flowdashboard.periodicstate.PeriodicStateUtils;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Models.auditmodel.PeriodicAuditComplete;
import com.treebo.prowlapp.Models.dashboardmodel.SectionedTaskListModel;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.Utils.IssueFormatUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhisheknair on 28/11/16.
 */

public class SectionedTasksAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private ArrayList<SectionedTaskListModel> mSectionedList;
    private taskCardActionListener mTaskCardListener;
    private Context mContext;
    private int mHotelId;
    private AuditPrefManagerV3 mAuditManager;
    private boolean isInGeofence = true;
    private RxBus mRxBus;

    public SectionedTasksAdapter(Context context, ArrayList<SectionedTaskListModel> list, taskCardActionListener listener,
                                 int hotelId, AuditPrefManagerV3 auditManager, RxBus rxBus) {
        mSectionedList = list;
        mTaskCardListener = listener;
        mContext = context;
        mHotelId = hotelId;
        mAuditManager = auditManager;
        mRxBus = rxBus;

    }

    public void refreshList(boolean isInGeofence) {
        this.isInGeofence = isInGeofence;
        notifyDataSetChanged();
    }

    @Override
    public int getSectionCount() {
        if (mSectionedList == null)
            return 0;
        else
            return mSectionedList.size();
    }

    @Override
    public int getItemCount(int i) {
        if (mSectionedList == null)
            return 0;
        else
            return mSectionedList.get(i).getTaskList().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        SectionedTaskListModel model = mSectionedList.get(i);
        ((TaskSectionViewHolder) viewHolder).sectionTitle.setText(model.getSectionName());
        ((TaskSectionViewHolder) viewHolder).sectionCount
                .setText(String.valueOf(model.getTaskListSize()));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, int i1, int i2) {
        TaskModel task = mSectionedList.get(i).getTaskList().get(i1);

        switch (task.getTaskCardType()) {
            case Constants.MANUAL_TASK:
                SectionedTaskListModel model = mSectionedList.get(i);
                if (model.getSectionName().equals(mContext.getString(R.string.pending_task_string))) {
                    ((ManualTaskViewHolder) viewHolder).daysOpen.setText(DateUtils.getResolutionOverflowToday(task.getCompletionDate()));
                }
                if (task.isPendingDelete) {
                    ((ManualTaskViewHolder) viewHolder).resolvedHeader.setText(task.getHeading());
                    ((ManualTaskViewHolder) viewHolder).pendingView.setVisibility(View.VISIBLE);
                } else {
                    ((ManualTaskViewHolder) viewHolder).task = task;
                    ((ManualTaskViewHolder) viewHolder).header.setText(task.getHeading());
                    Spannable spannable = Utils.applyCustomLinkSpanToUrlSpan(mContext, task.getInfo(), task.getContentLink());
                    ((ManualTaskViewHolder) viewHolder).infoText.setMovementMethod(LinkMovementMethod.getInstance());
                    ((ManualTaskViewHolder) viewHolder).infoText.setText(spannable, TextView.BufferType.SPANNABLE);
                    ((ManualTaskViewHolder) viewHolder).pendingView.setVisibility(View.GONE);
                }
                if (!isInGeofence && task.isGeofenceEnabled()) {
                    ((ManualTaskViewHolder) viewHolder).disabledState.setVisibility(View.VISIBLE);
                } else {
                    ((ManualTaskViewHolder) viewHolder).disabledState.setVisibility(View.GONE);
                }
                break;

            case Constants.AUDIT_TASK:
                ((AuditTaskViewHolder) viewHolder).task = task;

                //get the current state of audit card
                String state = StateAuditUtils.getState(mHotelId, mAuditManager);
                IAuditState auditState = StateAuditUtils.newState(mContext, state, task, mHotelId);
                ((AuditTaskViewHolder) viewHolder).setState(auditState);

                //recommended
                int visibility = ((AuditTaskViewHolder) viewHolder).mAuditState.getRecommendedVisibility();
                ((AuditTaskViewHolder) viewHolder).recommended.setVisibility(visibility);
                if (visibility == View.VISIBLE) {
                    ((AuditTaskViewHolder) viewHolder).recommended.setText(mContext.getString(R.string.recommended_rooms_card_text));
                }
                //state part
                if (state.equals("done"))
                    mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.AUDIT_COMPLETE));
                visibility = ((AuditTaskViewHolder) viewHolder).mAuditState.getStateVisibility();
                ((AuditTaskViewHolder) viewHolder).stateLayout.setVisibility(visibility);
                if (visibility == View.VISIBLE) {
                    int resId = ((AuditTaskViewHolder) viewHolder).mAuditState.getAuditIcon();
                    if (resId != -1) {
                        ((AuditTaskViewHolder) viewHolder).icon.setBackground(ContextCompat.getDrawable(mContext, resId));
                    }
                    ((AuditTaskViewHolder) viewHolder).auditState.setText(((AuditTaskViewHolder) viewHolder).mAuditState.getAuditState());
                    ((AuditTaskViewHolder) viewHolder).auditStateDesc.setText(((AuditTaskViewHolder) viewHolder).mAuditState.getStateText());
                    ((AuditTaskViewHolder) viewHolder).timeLeft.setText(DateUtils.getTimeTimeRemainingFromEODFormatted());

                    ((AuditTaskViewHolder) viewHolder).timeLeftDesc.setText(DateUtils.getTimeRemainingFromEODDesc());
                }

                //restart and resume buttons
                visibility = ((AuditTaskViewHolder) viewHolder).mAuditState.getRestartVisibility();
                ((AuditTaskViewHolder) viewHolder).trashAuditBtn.setVisibility(visibility);
                ((AuditTaskViewHolder) viewHolder).divider.setVisibility(visibility);
                visibility = ((AuditTaskViewHolder) viewHolder).mAuditState.getResumeVisibility();
                ((AuditTaskViewHolder) viewHolder).resumeAuditBtn.setVisibility(visibility);

                //check if entire card is clickable
                if (((AuditTaskViewHolder) viewHolder).mAuditState.isCardClickEnabled()) {
                    ((AuditTaskViewHolder) viewHolder).itemView.setOnClickListener(view -> mTaskCardListener.onStartAudit(task));
                } else {
                    ((AuditTaskViewHolder) viewHolder).itemView.setClickable(false);
                }
                if (!isInGeofence && task.isGeofenceEnabled())
                    ((AuditTaskViewHolder) viewHolder).disabledState.setVisibility(View.VISIBLE);
                else
                    ((AuditTaskViewHolder) viewHolder).disabledState.setVisibility(View.GONE);
                break;

            case Constants.ISSUE_TASK:
                ((IssueTaskViewHolder) viewHolder).task = task;
                int overdueIssues = task.getIssueTaskModel().mOverdues;
                int todaysIssues = task.getIssueTaskModel().mToday;
                ((IssueTaskViewHolder) viewHolder).overdue.setText(String.valueOf(overdueIssues));
                ((IssueTaskViewHolder) viewHolder).today.setText(String.valueOf(todaysIssues));
                if (overdueIssues == 0 && todaysIssues == 0)
                    mRxBus.postEvent(new IssueResolvedEventV3(overdueIssues, todaysIssues));
                break;

            case Constants.PERIODIC_AUDIT_TASK:
                ((PeridicAuditTaskViewHolder) viewHolder).task = task;
                BasePeriodicAuditCard periodicAuditCard = PeriodicStateUtils.getCard(mContext, task);
                if (periodicAuditCard != null) {
                    int resId = periodicAuditCard.getAuditIcon();
                    if (resId != -1) {
                        ((PeridicAuditTaskViewHolder) viewHolder).icon.setBackground(
                                Utils.getTintedDrawable(mContext, ContextCompat.getDrawable(mContext, resId), ContextCompat.getColor(
                                        mContext, R.color.green_color_primary)));

                    }
                    if (!TextUtils.isEmpty(task.getInfo())) {
                        ((PeridicAuditTaskViewHolder) viewHolder).auditDescription.setText(task.getInfo());
                        ((PeridicAuditTaskViewHolder) viewHolder).auditDescription.setVisibility(View.VISIBLE);
                    } else {
                        ((PeridicAuditTaskViewHolder) viewHolder).auditDescription.setVisibility(View.GONE);
                    }
                    ((PeridicAuditTaskViewHolder) viewHolder).auditType.setText(periodicAuditCard.getAuditText());
                    int cardState = periodicAuditCard.isAuditPaused(mAuditManager, mHotelId, task);
                    switch (cardState) {
                        case PeriodicStateUtils.PAUSED:
                            ((PeridicAuditTaskViewHolder) viewHolder).resumeAuditBtn.setVisibility(View.VISIBLE);
                            ((PeridicAuditTaskViewHolder) viewHolder).trashAuditBtn.setVisibility(View.VISIBLE);
                            ((PeridicAuditTaskViewHolder) viewHolder).divider.setVisibility(View.VISIBLE);
                            ((PeridicAuditTaskViewHolder) viewHolder).itemView.setClickable(false);
                            if (task.getEndTime() != null) {
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeft.setText(DateUtils.getTimeRemainingFromEndTime(task.getEndTime()));
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeftDesc.setText(DateUtils.getTimeRemainingFromEndTimeDesc(task.getEndTime()));
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeft.setVisibility(View.VISIBLE);
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeftDesc.setVisibility(View.VISIBLE);
                                ((PeridicAuditTaskViewHolder) viewHolder).pauseIcon.setVisibility(View.VISIBLE);
                                ((PeridicAuditTaskViewHolder) viewHolder).pausedText.setText(mContext.getString(R.string.paused));
                                ((PeridicAuditTaskViewHolder) viewHolder).pausedText.setVisibility(View.VISIBLE);
                                ((PeridicAuditTaskViewHolder) viewHolder).auditDescription.setVisibility(View.GONE);
                                ((PeridicAuditTaskViewHolder) viewHolder).dudeDateText.setVisibility(View.GONE);
                                ((PeridicAuditTaskViewHolder) viewHolder).dudeDateTv.setVisibility(View.GONE);
                                ((PeridicAuditTaskViewHolder) viewHolder).partialAuditStateText.setVisibility(View.INVISIBLE);
                            } else {
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeft.setVisibility(View.GONE);
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeftDesc.setVisibility(View.GONE);
                                ((PeridicAuditTaskViewHolder) viewHolder).pauseIcon.setVisibility(View.GONE);
                                ((PeridicAuditTaskViewHolder) viewHolder).pausedText.setVisibility(View.GONE);
                                ((PeridicAuditTaskViewHolder) viewHolder).partialAuditStateText.setVisibility(View.GONE);
                                if (!TextUtils.isEmpty(task.getInfo()))
                                    ((PeridicAuditTaskViewHolder) viewHolder).auditDescription.setVisibility(View.VISIBLE);
                                ((PeridicAuditTaskViewHolder) viewHolder).dudeDateText.setVisibility(View.VISIBLE);
                                ((PeridicAuditTaskViewHolder) viewHolder).dudeDateTv.setVisibility(View.VISIBLE);
                            }
                            break;

                        case PeriodicStateUtils.DEFAULT:
                            ((PeridicAuditTaskViewHolder) viewHolder).resumeAuditBtn.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).trashAuditBtn.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).divider.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).partialAuditStateText.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).itemView.setClickable(true);
                            if (!TextUtils.isEmpty(task.getInfo()))
                                ((PeridicAuditTaskViewHolder) viewHolder).auditDescription.setVisibility(View.VISIBLE);
                            String dueDate = DateUtils.getTaskDueDate(task.getCompletionDate());
                            ((PeridicAuditTaskViewHolder) viewHolder).dudeDateTv.setText(dueDate);
                            if (dueDate.equals(mContext.getString(R.string.today)))
                                ((PeridicAuditTaskViewHolder) viewHolder).dudeDateTv
                                        .setTextColor(ContextCompat.getColor(mContext, R.color.warning_red));
                            else
                                ((PeridicAuditTaskViewHolder) viewHolder).dudeDateTv
                                        .setTextColor(ContextCompat.getColor(mContext, R.color.green_color_primary));
                            ((PeridicAuditTaskViewHolder) viewHolder).timeLeft.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).timeLeftDesc.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).pauseIcon.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).pausedText.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).dudeDateText.setVisibility(View.VISIBLE);
                            ((PeridicAuditTaskViewHolder) viewHolder).dudeDateTv.setVisibility(View.VISIBLE);
                            try {
                                if ((isInGeofence || !task.isGeofenceEnabled()) && periodicAuditCard.isEnabled()) {
                                    ((PeridicAuditTaskViewHolder) viewHolder).itemView.setEnabled(true);
                                    ((PeridicAuditTaskViewHolder) viewHolder).disabledState.setVisibility(View.GONE);
                                } else {
                                    ((PeridicAuditTaskViewHolder) viewHolder).itemView.setEnabled(false);
                                    ((PeridicAuditTaskViewHolder) viewHolder).disabledState.setVisibility(View.VISIBLE);
                                }
                            } catch (ParseException e) {
                                Log.e("ParseException", "Parseexception in getting time bounds");
                            }
                            break;

                        case PeriodicStateUtils.PARTIALLY_DONE:
                            List<AuditCategory> subAuditCategoryIDList = AuditCategory.getSubAudits(task.getAuditTaskType());
                            List<PeriodicAuditComplete> auditCompleteObjectList = PeriodicAuditComplete.getSubAuditDoneList(task.getTaskLogID());
                            SpannableStringBuilder partialAuditText = new SpannableStringBuilder();
                            for (int count = 0; count < subAuditCategoryIDList.size(); count++) {
                                AuditCategory subAudit = subAuditCategoryIDList.get(count);
                                if (!isSubAuditCompleted(auditCompleteObjectList, subAudit.mAuditCategoryId)) {
                                    if (count >= 3) {
                                        partialAuditText.append(" ...");
                                        break;
                                    } else {
                                        partialAuditText.append(partialAuditText.length() == 0 ? " " : ",  ");
                                        Drawable d = IssueFormatUtils.getTintedDrawable(mContext,
                                                ContextCompat.getDrawable(mContext, PeriodicStateUtils.getSubAuditIcon(subAudit.mAuditCategoryId)),
                                                ContextCompat.getColor(mContext, R.color.green_color_primary));
                                        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                                        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
                                        partialAuditText.setSpan(span,
                                                partialAuditText.length() - 1, partialAuditText.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                                    }
                                }
                            }
                            int subAuditDoneCount = PeriodicAuditComplete.getSubAuditDoneCountForTask(task.getTaskLogID());
                            String pendingAuditText = mContext.getResources().getQuantityString(R.plurals.pending_audit_count,
                                    subAuditCategoryIDList.size() - subAuditDoneCount);
                            ((PeridicAuditTaskViewHolder) viewHolder).partialAuditStateText.setText(partialAuditText);
                            ((PeridicAuditTaskViewHolder) viewHolder).partialAuditStateText.setVisibility(View.VISIBLE);
                            ((PeridicAuditTaskViewHolder) viewHolder).pausedText.setText(mContext.getString(R.string.total_staff_for_incentives,
                                    subAuditCategoryIDList.size() - subAuditDoneCount, pendingAuditText));
                            ((PeridicAuditTaskViewHolder) viewHolder).pausedText.setVisibility(View.VISIBLE);
                            ((PeridicAuditTaskViewHolder) viewHolder).resumeAuditBtn.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).trashAuditBtn.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).divider.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).itemView.setClickable(true);
                            ((PeridicAuditTaskViewHolder) viewHolder).dudeDateTv.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).auditDescription.setVisibility(View.GONE);
                            if (task.getCompletionDate() != null) {
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeft.setText(DateUtils.getTimeRemainingFromExpiryDateString(task.getCompletionDate()));
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeftDesc.setText(DateUtils.getTimeRemainingFromExpiryDateDesc(task.getCompletionDate()));
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeft.setVisibility(View.VISIBLE);
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeftDesc.setVisibility(View.VISIBLE);
                            } else {
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeft.setVisibility(View.GONE);
                                ((PeridicAuditTaskViewHolder) viewHolder).timeLeftDesc.setVisibility(View.GONE);
                            }
                            ((PeridicAuditTaskViewHolder) viewHolder).pauseIcon.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).dudeDateText.setVisibility(View.GONE);
                            ((PeridicAuditTaskViewHolder) viewHolder).dudeDateTv.setVisibility(View.GONE);
                            try {
                                if ((isInGeofence || !task.isGeofenceEnabled()) && periodicAuditCard.isEnabled()) {
                                    ((PeridicAuditTaskViewHolder) viewHolder).itemView.setEnabled(true);
                                    ((PeridicAuditTaskViewHolder) viewHolder).disabledState.setVisibility(View.GONE);
                                } else {
                                    ((PeridicAuditTaskViewHolder) viewHolder).itemView.setEnabled(false);
                                    ((PeridicAuditTaskViewHolder) viewHolder).disabledState.setVisibility(View.VISIBLE);
                                }
                            } catch (ParseException e) {
                                Log.e("ParseException", "Parseexception in getting time bounds");
                            }
                            break;

                    }
                }

                break;

            case Constants.STAFF_INCENTIVE_TASK:
                ((IncentivesTaskViewHolder) viewHolder).task = task;
                ((IncentivesTaskViewHolder) viewHolder).headingTv.setText(task.getHeading());
                String dueDate = DateUtils.getTaskDueDate(task.getCompletionDate());
                ((IncentivesTaskViewHolder) viewHolder).dudeDateTv.setText(dueDate);
                if (dueDate.equals(mContext.getString(R.string.today)))
                    ((IncentivesTaskViewHolder) viewHolder).dudeDateTv.setTextColor(ContextCompat.getColor(mContext, R.color.warning_red));
                else
                    ((IncentivesTaskViewHolder) viewHolder).dudeDateTv.setTextColor(ContextCompat.getColor(mContext, R.color.green_color_primary));
                break;


            case Constants.AUDIT_TASK_COMPLETED_STATE:
                ((AuditTaskDoneViewHolder) viewHolder).task = task;
                ((AuditTaskDoneViewHolder) viewHolder).header.setText(task.getHeading());
                if (!isInGeofence && task.isGeofenceEnabled())
                    ((AuditTaskDoneViewHolder) viewHolder).disabledState.setVisibility(View.VISIBLE);
                else
                    ((AuditTaskDoneViewHolder) viewHolder).disabledState.setVisibility(View.GONE);
                break;

        }
    }

    private boolean isSubAuditCompleted(List<PeriodicAuditComplete> list, int auditCategoryID) {
        for (PeriodicAuditComplete object : list) {
            if (object.mAuditCategoryID == auditCategoryID)
                return true;
        }
        return false;
    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        return mSectionedList.get(section).getTaskList().get(relativePosition).getTaskCardType();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Constants.MANUAL_TASK) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_manual_task, parent, false);
            return new ManualTaskViewHolder(view, mTaskCardListener);
        } else if (viewType == Constants.AUDIT_TASK) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_task_card, parent, false);
            return new AuditTaskViewHolder(view, mTaskCardListener);
        } else if (viewType == Constants.ISSUE_TASK) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_issue_task_card, parent, false);
            return new IssueTaskViewHolder(view, mTaskCardListener);
        } else if (viewType == Constants.PERIODIC_AUDIT_TASK) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_periodic_task, parent, false);
            return new PeridicAuditTaskViewHolder(view, mTaskCardListener);
        } else if (viewType == Constants.STAFF_INCENTIVE_TASK) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_staff_incentives_task, parent, false);
            return new IncentivesTaskViewHolder(view, mTaskCardListener);
        } else if (viewType == Constants.AUDIT_TASK_COMPLETED_STATE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_task_completed, parent, false);
            return new AuditTaskDoneViewHolder(view, mTaskCardListener);
        } else if (viewType == Constants.BOTTOM_DUMMY_TASK) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom_dummy_task, parent, false);
            return new BottomDummyViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_section_header,
                    parent, false);
            view.setPadding(0, 40, 0, 10);
            return new TaskSectionViewHolder(view);
        }
    }

    private class TaskSectionViewHolder extends RecyclerView.ViewHolder {

        TreeboTextView sectionTitle;
        TreeboTextView sectionCount;

        TaskSectionViewHolder(View itemView) {
            super(itemView);
            sectionTitle = (TreeboTextView) itemView.findViewById(R.id.task_section_header_title);
            sectionCount = (TreeboTextView) itemView.findViewById(R.id.task_section_item_count);
        }
    }

    private class BottomDummyViewHolder extends RecyclerView.ViewHolder {

        BottomDummyViewHolder(View itemView) {
            super(itemView);
        }
    }

    private class AuditTaskDoneViewHolder extends RecyclerView.ViewHolder {

        TaskModel task;
        TreeboTextView header;
        TreeboTextView subText;
        TreeboButton auditMoreRooms;
        ImageView closeBtn;
        View disabledState;

        AuditTaskDoneViewHolder(View itemView, taskCardActionListener listener) {
            super(itemView);
            header = (TreeboTextView) itemView.findViewById(R.id.task_card_header);
            subText = (TreeboTextView) itemView.findViewById(R.id.task_subtext_tv);
            auditMoreRooms = (TreeboButton) itemView.findViewById(R.id.audit_more_rooms_btn);
            disabledState = itemView.findViewById(R.id.not_in_geofence);
            auditMoreRooms.setOnClickListener(view ->
                    listener.onAuditMoreRoomsClicked(task));
            closeBtn = (ImageView) itemView.findViewById(R.id.task_close_iv);
            closeBtn.setOnClickListener(view -> {
                listener.onCloseAuditMoreRoomsClicked(task);
            });
        }
    }

    private class ManualTaskViewHolder extends RecyclerView.ViewHolder {

        TaskModel task;
        TreeboTextView header;
        TreeboTextView infoText;
        ImageView toolTipClick;
        TreeboTextView doneBtn;
        ImageView icon;
        View pendingView;
        TreeboTextView undoBtn;
        TreeboTextView resolvedHeader;
        TreeboTextView daysOpen;
        View disabledState;

        ManualTaskViewHolder(View itemView, taskCardActionListener listener) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.task_card_iv);
            doneBtn = (TreeboTextView) itemView.findViewById(R.id.task_done_btn);
            doneBtn.setOnClickListener(view -> listener.onDoneBtnClick(task));
            header = (TreeboTextView) itemView.findViewById(R.id.task_card_header);
            infoText = (TreeboTextView) itemView.findViewById(R.id.task_info_tv);
            toolTipClick = (ImageView) itemView.findViewById(R.id.task_tooltip_iv);
            toolTipClick.setOnClickListener(view -> listener.onToolTipClick(task));
            daysOpen = (TreeboTextView) itemView.findViewById(R.id.task_time_open);
            pendingView = itemView.findViewById(R.id.resolved_layout);
            undoBtn = (TreeboTextView) itemView.findViewById(R.id.undo_btn);
            undoBtn.setOnClickListener(view -> listener.onUndoBtnClicked(task));
            resolvedHeader = (TreeboTextView) itemView.findViewById(R.id.manual_task_heading);
            disabledState = itemView.findViewById(R.id.not_in_geofence);
            itemView.setOnClickListener(view -> listener.onManualTaskCardClicked(task));
        }
    }

    private class IncentivesTaskViewHolder extends RecyclerView.ViewHolder {

        TaskModel task;
        TreeboTextView headingTv;
        TreeboTextView dudeDateTv;
        TreeboTextView dudeDateText;
        ImageView tooltip;

        public IncentivesTaskViewHolder(View itemView, taskCardActionListener listener) {
            super(itemView);
            headingTv = (TreeboTextView) itemView.findViewById(R.id.task_card_header);
            tooltip = (ImageView) itemView.findViewById(R.id.task_tooltip_iv);
            tooltip.setOnClickListener(view -> listener.onToolTipClick(task));
            dudeDateText = (TreeboTextView) itemView.findViewById(R.id.due_date_text);
            dudeDateTv = (TreeboTextView) itemView.findViewById(R.id.due_date_tv);
            itemView.setOnClickListener(view -> listener.onStaffIncentivesTaskClicked(task));
        }
    }

    private class IssueTaskViewHolder extends RecyclerView.ViewHolder {

        TaskModel task;
        TreeboTextView overdue;
        TreeboTextView today;
        ImageView toolTipClick;
        View disabledState;


        IssueTaskViewHolder(View itemView, taskCardActionListener listener) {
            super(itemView);
            overdue = (TreeboTextView) itemView.findViewById(R.id.issue_overdues_number);
            today = (TreeboTextView) itemView.findViewById(R.id.issue_today_number);
            toolTipClick = (ImageView) itemView.findViewById(R.id.task_tooltip_iv);
            toolTipClick.setOnClickListener(view -> listener.onToolTipClick(task));
            disabledState = itemView.findViewById(R.id.not_in_geofence);
            itemView.setOnClickListener(view -> listener.onIssueCardClicked(task));
        }
    }

    private class PeridicAuditTaskViewHolder extends RecyclerView.ViewHolder implements IPeriodicStateContext {
        IPeriodicAuditCard mPeriodicAuditCard;

        TaskModel task;
        ImageView icon;
        TreeboTextView timeLeft;
        TreeboTextView timeLeftDesc;
        TreeboTextView auditType;
        TreeboTextView auditDescription;
        TreeboButton trashAuditBtn;
        TreeboButton resumeAuditBtn;
        ImageView toolTipClick;
        ImageView pauseIcon;
        TreeboTextView partialAuditStateText;
        TreeboTextView pausedText;
        TreeboTextView dudeDateTv;
        TreeboTextView dudeDateText;
        View disabledState;
        View divider;

        PeridicAuditTaskViewHolder(View itemView, taskCardActionListener listener) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.task_card_iv);

            auditType = (TreeboTextView) itemView.findViewById(R.id.task_card_header);
            auditDescription = (TreeboTextView) itemView.findViewById(R.id.task_info_tv);
            timeLeft = (TreeboTextView) itemView.findViewById(R.id.audit_card_time_left);
            timeLeftDesc = (TreeboTextView) itemView.findViewById(R.id.audit_card_time_left_desc);
            divider = itemView.findViewById(R.id.divider);
            trashAuditBtn = (TreeboButton) itemView.findViewById(R.id.task_trash_btn);
            resumeAuditBtn = (TreeboButton) itemView.findViewById(R.id.task_resume_btn);
            trashAuditBtn.setOnClickListener(view -> listener.onRestartAudit(task));
            resumeAuditBtn.setOnClickListener(view -> listener.onResumeAudit(task));
            pauseIcon = (ImageView) itemView.findViewById(R.id.ic_pause);
            partialAuditStateText = (TreeboTextView) itemView.findViewById(R.id.pending_state_text);
            pausedText = (TreeboTextView) itemView.findViewById(R.id.pause_text);
            toolTipClick = (ImageView) itemView.findViewById(R.id.task_tooltip_iv);
            disabledState = itemView.findViewById(R.id.task_card_disabled);
            toolTipClick.setOnClickListener(view -> listener.onToolTipClick(task));
            dudeDateText = (TreeboTextView) itemView.findViewById(R.id.due_date_text);
            dudeDateTv = (TreeboTextView) itemView.findViewById(R.id.due_date_tv);
            itemView.setOnClickListener(view -> listener.onPeriodicTaskCardClicked(task));
            resumeAuditBtn.setOnClickListener(view -> listener.onPeriodicResumeAudit(task));
            trashAuditBtn.setOnClickListener(view -> listener.onPeriodicRestartAudit(task));
        }

        @Override
        public IPeriodicAuditCard getState() {
            return mPeriodicAuditCard;
        }

        @Override
        public void setState(IPeriodicAuditCard card) {
            mPeriodicAuditCard = card;
        }

    }


    private class AuditTaskViewHolder extends RecyclerView.ViewHolder implements IStateContext {

        IAuditState mAuditState;

        TaskModel task;
        ImageView icon;
        TreeboTextView recommended;
        TreeboTextView auditStateDesc;
        TreeboTextView timeLeft;
        TreeboTextView timeLeftDesc;
        TreeboTextView auditState;
        TreeboButton trashAuditBtn;
        TreeboButton resumeAuditBtn;
        RelativeLayout stateLayout;
        ImageView toolTipClick;
        View disabledState;
        View divider;


        AuditTaskViewHolder(View itemView, taskCardActionListener listener) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.audit_card_icon);
            recommended = (TreeboTextView) itemView.findViewById(R.id.task_recommended_tv);
            auditStateDesc = (TreeboTextView) itemView.findViewById(R.id.audit_card_state_text_desc);
            auditState = (TreeboTextView) itemView.findViewById(R.id.audit_card_state);
            stateLayout = (RelativeLayout) itemView.findViewById(R.id.audit_state_layout);
            timeLeft = (TreeboTextView) itemView.findViewById(R.id.audit_card_time_left);
            timeLeftDesc = (TreeboTextView) itemView.findViewById(R.id.audit_card_time_left_desc);
            divider = itemView.findViewById(R.id.divider);
            trashAuditBtn = (TreeboButton) itemView.findViewById(R.id.task_trash_btn);
            resumeAuditBtn = (TreeboButton) itemView.findViewById(R.id.task_resume_btn);
            trashAuditBtn.setOnClickListener(view -> listener.onRestartAudit(task));
            resumeAuditBtn.setOnClickListener(view -> listener.onResumeAudit(task));
            toolTipClick = (ImageView) itemView.findViewById(R.id.task_tooltip_iv);
            disabledState = itemView.findViewById(R.id.not_in_geofence);
            toolTipClick.setOnClickListener(view -> listener.onToolTipClick(task));

            resumeAuditBtn.setOnClickListener(view -> listener.onResumeAudit(task));
            trashAuditBtn.setOnClickListener(view -> listener.onRestartAudit(task));

        }


        @Override
        public IAuditState getState() {
            return mAuditState;
        }

        @Override
        public void setState(IAuditState auditState) {
            mAuditState = auditState;
        }
    }

    public interface taskCardActionListener {
        void onDoneBtnClick(TaskModel task);

        void onToolTipClick(TaskModel task);

        void onStartAudit(TaskModel task);

        void onRestartAudit(TaskModel task);

        void onResumeAudit(TaskModel task);

        void onIssueCardClicked(TaskModel task);

        void onUndoBtnClicked(TaskModel task);

        void onManualTaskCardClicked(TaskModel task);

        void onPeriodicTaskCardClicked(TaskModel task);

        void onPeriodicRestartAudit(TaskModel task);

        void onPeriodicResumeAudit(TaskModel task);

        void onStaffIncentivesTaskClicked(TaskModel task);

        void onAuditMoreRoomsClicked(TaskModel task);

        void onCloseAuditMoreRoomsClicked(TaskModel task);

    }

}
