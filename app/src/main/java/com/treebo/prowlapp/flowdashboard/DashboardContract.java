package com.treebo.prowlapp.flowdashboard;

import com.treebo.prowlapp.Models.dashboardmodel.HotelDashboardMetrics;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.response.dashboard.ContactInfoResponse;
import com.treebo.prowlapp.response.dashboard.HotelMetricsResponse;
import com.treebo.prowlapp.response.dashboard.HotelTasksResponse;
import com.treebo.prowlapp.response.dashboard.TaskHistoryResponse;

/**
 * Created by abhisheknair on 28/11/16.
 */

public interface DashboardContract {

    interface IDashboardView extends BaseView {

        void setUpViewAsPerPermissions();

        void onLoadScoreSuccess(HotelMetricsResponse response);

        void onLoadScoreFailure(String msg);

        void onLoadTasksSuccess(HotelTasksResponse response);

        void onLoadTasksFailure(String msg);

        void onTaskDoneSuccess();

        void onTaskDoneFailure(int taskID);

        void setMetricsData(HotelDashboardMetrics metrics);

        void setUpAdapterData(HotelTasksResponse.TaskList taskList);
    }

    interface IDashboardPresenter extends BasePresenter {
        void loadTasksForHotel(int hotelID, int userID);

        void loadScoresForHotel(int hotelID, int userID);

        void setUpMetrics(HotelDashboardMetrics metrics);

        void setUpTaskAdapterData(HotelTasksResponse.TaskList response);

        void onTaskDone(int taskID, int taskLogID, int userID, int hotelID);
    }

    interface ISubmitFeedbackView extends BaseView {
        void onSubmitSuccess();

        void onSubmitFailure(String msg);
    }

    interface ISubmitFeedbackPresenter extends BasePresenter {
        void postFeedback(int userID, String feedback);
    }

    interface ITaskHistoryView extends BaseView {
        void onLoadSuccess(TaskHistoryResponse.TaskHistory response);

        void onLoadFailure(String msg);
    }

    interface ITaskHistoryPresenter extends BasePresenter {
        void fetchTaskHistory(int userID);
    }

    interface IContactInfoView extends BaseView {
        void setUpData(ContactInfoResponse response);
    }

    interface IContactInfoPresenter extends BasePresenter {
        void fetchContactInfo();

        void fetchSuccess(ContactInfoResponse response);

        void fetchFailure(String message);
    }

    interface ICreateIssueView extends BaseView {

        void moveToCreateIssue(AuditAreaResponse auditArea);

        void createIssueForRoom(RoomSingleResponse response);

    }

    interface ICreateIssuePresenter extends BasePresenter {

        void loadAuditCategory(int hotelId, int auditId);

        void loadRoomAudit(int hotelId, int roomId);

        void onLoadRoomSuccess(RoomSingleResponse response);

        void onLoadSuccess(AuditAreaResponse response);

        void onLoadFailure(String errorMessage);
    }
}
