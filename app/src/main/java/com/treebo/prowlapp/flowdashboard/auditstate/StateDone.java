package com.treebo.prowlapp.flowdashboard.auditstate;

import android.view.View;

/**
 * Created by sumandas on 02/12/2016.
 */

public class StateDone implements IAuditState {
    @Override
    public int getAuditIcon() {
        return -1;
    }

    @Override
    public String getStateText() {
        return "";
    }

    @Override
    public String getAuditState() {
        return "";
    }

    @Override
    public String getRecommendedText() {
        return "";
    }

    @Override
    public boolean isCardClickEnabled() {
        return false;
    }

    @Override
    public int getRestartVisibility() {
        return View.GONE;
    }

    @Override
    public int getResumeVisibility() {
        return View.GONE;
    }

    @Override
    public int getStateVisibility() {
        return View.GONE;
    }

    @Override
    public int getRecommendedVisibility() {
        return View.GONE;
    }
}
