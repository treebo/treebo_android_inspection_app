package com.treebo.prowlapp.flowdashboard.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.usecase.AuditCategoryUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomSingleUseCase;
import com.treebo.prowlapp.flowdashboard.DaggerDashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.adapter.CreateIssueAdapter;
import com.treebo.prowlapp.flowdashboard.adapter.SelectRoomAdapter;
import com.treebo.prowlapp.flowdashboard.presenter.CreateIssuePresenter;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboEditText;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 29/06/17.
 */

public class CreateIssueActivity extends AppCompatActivity
        implements CreateIssueAdapter.onAuditTypeSelected, DashboardContract.ICreateIssueView,
        TextWatcher, SelectRoomAdapter.onRoomClickListener {

    @Inject
    protected AuditCategoryUseCase mAuditCategoryUseCase;

    @Inject
    protected CreateIssuePresenter mPresenter;

    @Inject
    protected Navigator mNavigator;

    @Inject
    protected RoomSingleUseCase mRoomSingleUseCase;

    private View mLoadingView;
    private View mSelectRoomView;

    private SelectRoomAdapter mSelectRoomAdapter;
    private ArrayList<AuditAreaResponse.AuditAreaDetails> mRoomList;

    private int mHotelId;
    private String mHotelName;
    private int mAuditCategoryIDRequested;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_issue);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        DashboardComponent dashboardComponent = DaggerDashboardComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        dashboardComponent.injectCreateIssueActivity(this);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mHotelName = intent.getStringExtra(Utils.HOTEL_NAME);
            mHotelId = intent.getIntExtra(Utils.HOTEL_ID, -1);
        } else {
            mHotelName = savedInstanceState.getString(Utils.HOTEL_NAME);
            mHotelId = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
        }

        mLoadingView = findViewById(R.id.layout_progress);
        mSelectRoomView = findViewById(R.id.select_room_layout);

        TreeboEditText searchRoom = (TreeboEditText) mSelectRoomView.findViewById(R.id.search_et);
        searchRoom.addTextChangedListener(this);

        View selectRoomBackBtn = mSelectRoomView.findViewById(R.id.ic_back);
        selectRoomBackBtn.setOnClickListener(view -> mSelectRoomView.setVisibility(View.GONE));

        RecyclerView roomRv = (RecyclerView) mSelectRoomView.findViewById(R.id.select_room_rv);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        roomRv.setLayoutManager(layoutManager1);
        roomRv.setItemAnimator(new DefaultItemAnimator());
        roomRv.setLayoutManager(layoutManager1);
        mSelectRoomAdapter = new SelectRoomAdapter(this, mRoomList);
        roomRv.setAdapter(mSelectRoomAdapter);

        View backBtn = findViewById(R.id.toolbar_back_btn);
        backBtn.setOnClickListener(view -> onBackPressed());
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this);
        layoutManager2.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(layoutManager2);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        ArrayList<AuditCategory> list = new ArrayList<>(AuditCategory.getIssueCreatableAuditList());
        CreateIssueAdapter adapter = new CreateIssueAdapter(this, list, this);
        recyclerView.setAdapter(adapter);
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setAuditCategoryUseCase(mAuditCategoryUseCase);
        mPresenter.setRoomSingleUseCase(mRoomSingleUseCase);
    }

    @Override
    public void onBackPressed() {
        if (mSelectRoomView.getVisibility() == View.VISIBLE) {
            mSelectRoomView.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onAuditTypeSelected(AuditCategory auditCategory) {
        mAuditCategoryIDRequested = auditCategory.mAuditCategoryId;
        if (auditCategory.mAuditCategoryId == Constants.ROOM_AUDIT_TYPE_ID
                && mRoomList != null && mRoomList.size() > 0) {
            mSelectRoomAdapter.setData(mRoomList);
            mSelectRoomView.setVisibility(View.VISIBLE);
        } else {
            mPresenter.loadAuditCategory(mHotelId, auditCategory.mAuditId);
        }
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void moveToCreateIssue(AuditAreaResponse auditAreaResponse) {
        if (auditAreaResponse.mAuditAreaObject.mAuditAreas == null
                || auditAreaResponse.mAuditAreaObject.mAuditAreas.size() == 0) {
            showError(getString(R.string.audit_not_applicable_to_hotel));
        } else {
            AuditAreaResponse.AuditArea auditArea = new AuditAreaResponse.AuditArea();
            if (auditAreaResponse.mAuditAreaObject.mAuditAreas.size() == 1) {
                auditArea = auditAreaResponse.mAuditAreaObject.mAuditAreas.get(0);
            } else {
                for (AuditAreaResponse.AuditArea area : auditAreaResponse.mAuditAreaObject.mAuditAreas) {
                    if (area.mAuditCategoryId == mAuditCategoryIDRequested) {
                        auditArea = area;
                        break;
                    }
                }
            }
            if (auditArea.mDetails == null) {
                showError(getString(R.string.audit_not_applicable_to_hotel));
            } else if (auditArea.mAuditCategoryId == Constants.ROOM_AUDIT_TYPE_ID) {
                mRoomList = auditArea.mDetails;
                mSelectRoomAdapter.setData(mRoomList);
                mSelectRoomView.setVisibility(View.VISIBLE);
            } else {
                ArrayList<Category> includedCheckPoints =
                        AuditUtilsV3.checkpointList(auditArea.mDetails.get(0).mCheckpointList);
                for (Category category : includedCheckPoints) {
                    category.setAuditDone(true);
                }
                mNavigator.navigateToAuditType2Activity(this, new TaskModel(), auditArea.mAuditCategoryId,
                        includedCheckPoints, mHotelId, mHotelName, true, false, true, -1);
            }
        }
    }

    @Override
    public void createIssueForRoom(RoomSingleResponse response) {
        ArrayList<Category> includedCheckPoints =
                AuditUtilsV3.checkpointList(response.getRoomSingleData().getCheckpointList());
        for (Category category : includedCheckPoints) {
            category.setAuditDone(true);
        }
        mNavigator.navigateToAuditType2Activity(this, new TaskModel(), Constants.ROOM_AUDIT_TYPE_ID,
                includedCheckPoints, mHotelId, mHotelName, true, false, true,
                response.getRoomSingleData().getRoomId());
        mSelectRoomView.setVisibility(View.GONE);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        mSelectRoomAdapter.filter(s.toString());
    }

    @Override
    public void onRoomClicked(AuditAreaResponse.AuditAreaDetails room) {
        mPresenter.loadRoomAudit(mHotelId, room.getRoomID());
    }
}
