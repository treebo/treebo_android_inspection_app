package com.treebo.prowlapp.flowdashboard.periodicstate;

import android.content.Context;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Models.auditmodel.AuditSaveObject;
import com.treebo.prowlapp.Models.auditmodel.FraudAuditSaveObject;
import com.treebo.prowlapp.Models.auditmodel.PeriodicAuditComplete;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.Utils.Constants;

import java.util.List;

/**
 * Created by sumandas on 18/01/2017.
 */

public class PeriodicStateUtils {

    public static final int DEFAULT = 0;
    public static final int PAUSED = 1;
    public static final int PARTIALLY_DONE = 2;


    public static BasePeriodicAuditCard getCard(Context context, TaskModel taskModel) {
        switch (taskModel.getAuditTaskType()) {
            case Constants.BREAKFAST_AUDIT_TASK_TYPE:
                return new BreakFastAuditCard(context, taskModel);

            case Constants.FnB_AUDIT_TASK_TYPE:
                return new FoodBeverageAuditCard(context, taskModel);

            case Constants.FRAUD_AUDIT_TASK_TYPE:
                return new FraudAuditCard(context, taskModel);

            case Constants.BACK_OF_THE_HOUSE_AUDIT_TASK_TYPE:
                return new BackOfTheHouseAuditCard(context, taskModel);

            default:
                return new GenericPeriodicAuditCard(context, taskModel);
        }
    }


    public static int getSubAuditIcon(int subAuditId) {
        switch (AuditCategory.getAuditCategoryConstantByID(subAuditId)) {
            case Constants.BREAKFAST_AUDIT_STRING:
                return R.drawable.ic_breakfast_audit;

            case Constants.FRAUD_AUDIT_STRING:
                return R.drawable.ic_fraud_room;

            case Constants.KITCHEN_PANTRY_AUDIT_STRING:
                return R.drawable.ic_kitchen;

            case Constants.FnB_AUDIT_STRING:
                return R.drawable.ic_food_beverage_audit;

            case Constants.RESTAURANT_AUDIT_STRING:
                return R.drawable.ic_food_beverage_audit;

            case Constants.RESTAURANT_WASHROOM_AUDIT_STRING:
                return R.drawable.ic_washroom;

            case Constants.COMMON_AREA_AUDIT_STRING:
                return R.drawable.ic_common_area_gray;

            case Constants.ROOM_AUDIT_STRING:
                return R.drawable.ic_room;

            case Constants.BACK_OF_THE_HOUSE_AUDIT_STRING:
                return R.drawable.ic_back_of_the_house;

            case Constants.GYM_AUDIT_STRING:
                return R.drawable.ic_gym;

            case Constants.SPA_AUDIT_STRING:
                return R.drawable.ic_spa;

            case Constants.POOL_AUDIT_STRING:
                return R.drawable.ic_pool;

            case Constants.POLICY_AND_CONDUCT_AUDIT_STRING:
                return R.drawable.ic_hr_audit;

            case Constants.ACCO_FOOD_AUDIT_STRING:
                return R.drawable.ic_accomodation;

            case Constants.STORAGE_SPACE_AUDIT_STRING:
                return R.drawable.ic_storage;

            case Constants.SAFETY_AUDIT_STRING:
                return R.drawable.ic_fire_safety_audit;

            case Constants.GARBAGE_MANAGEMENT_AUDIT_STRING:
                return R.drawable.ic_garbage;

            case Constants.WATER_STORAGE_AUDIT_STRING:
                return R.drawable.ic_water_storage;

            case Constants.DISABLED_FRIENDLY_AUDIT_STRING:
                return R.drawable.ic_disabled_friendly;

            case Constants.HK_TOOLS_AUDIT_STRING:
                return R.drawable.ic_house_keeping;

            case Constants.FRONT_OFFICE_AUDIT_STRING:
                return R.drawable.ic_front_office_tools;

            case Constants.LUXURY_AMENITIES_AUDIT_STRING:
                return R.drawable.ic_luxury_amenities;

            case Constants.CLOAK_ROOM_AUDIT_STRING:
                return R.drawable.ic_other_amenities;

            case Constants.DRIVERS_QUARTERS_AUDIT_STRING:
                return R.drawable.ic_driver_quarters;

            case Constants.PARKING_AREA_STRING:
                return R.drawable.ic_parking;

            case Constants.IN_HOUSE_LAUNDRY_AUDIT_STRING:
                return R.drawable.ic_inhouse_laundry;

            default:
                return R.drawable.ic_common_area_gray;
        }
    }

    public static int getStateOfCard(AuditPrefManagerV3 auditManager, int hotelId, TaskModel taskModel) {
        List<AuditCategory> auditCategories = AuditCategory.getSubAudits(taskModel.getAuditTaskType());
        int state = DEFAULT;
        for (AuditCategory auditCategory : auditCategories) {
            String key = AuditUtilsV3.createAuditKey(hotelId, auditCategory.mAuditCategoryId, null);
            if (auditCategory.mConstantAuditName.equals(Constants.FRAUD_AUDIT_STRING)) {
                FraudAuditSaveObject fraudAuditSaveObject = auditManager.getSavedFraudAudit(key);
                if (fraudAuditSaveObject != null) {
                    state = PAUSED;
                    break;
                }
            } else {
                AuditSaveObject auditSaveObject = auditManager.getSavedAudit(key);
                if (auditSaveObject != null && auditSaveObject.getCreatedIssueList().size() > 0) {
                    state = PAUSED;
                    break;
                } else if (PeriodicAuditComplete.getSubAuditDoneCountForTask(taskModel.getTaskLogID()) > 0)
                    state = PARTIALLY_DONE;
            }
        }
        return state;
    }


    public static int getPausedCategoryAuditId(AuditPrefManagerV3 auditManager, int hotelId, int auditId) {
        List<AuditCategory> auditCategories = AuditCategory.getSubAudits(auditId);
        for (AuditCategory auditCategory : auditCategories) {
            String key = AuditUtilsV3.createAuditKey(hotelId, auditCategory.mAuditCategoryId, null);
            AuditSaveObject auditSaveObject = auditManager.getSavedAudit(key);
            if (auditSaveObject != null) {
                return auditCategory.mAuditCategoryId;
            }
        }
        return -1;
    }

}
