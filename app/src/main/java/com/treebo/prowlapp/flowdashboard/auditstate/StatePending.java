package com.treebo.prowlapp.flowdashboard.auditstate;

import android.content.Context;
import android.view.View;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.CompleteAudit;

/**
 * Created by sumandas on 29/11/2016.
 */

public class StatePending implements IAuditState {

    private Context mContext;
    private int mHotelId;
    private CompleteAudit mCompleteAudit;

    public StatePending(Context context, int hotelId) {
        mContext = context;
        mHotelId = hotelId;
        mCompleteAudit = CompleteAudit.getCompleteAuditByHotelId(mHotelId);
    }

    @Override
    public boolean isCardClickEnabled() {
        return true;
    }


    @Override
    public int getAuditIcon() {
        if (!mCompleteAudit.isCommonAudited) {
            return R.drawable.ic_common_area_gray;
        } else {
            return R.drawable.ic_room;
        }

    }

    @Override
    public String getStateText() {
        String stateText = "";
        if (!mCompleteAudit.isCommonAudited && mCompleteAudit.mAvailableRooms > 0) {
            stateText = stateText + ",";
        }
        String roomsLeftString = mCompleteAudit.mAvailableRooms <= 0
                ? "" : String.valueOf(mCompleteAudit.mAvailableRooms)
                        + mContext.getResources().getQuantityString(R.plurals.room_count, mCompleteAudit.mAvailableRooms);
        stateText = stateText + roomsLeftString;
        return stateText;
    }

    @Override
    public String getRecommendedText() {
        return "";
    }


    @Override
    public String getAuditState() {
        return mContext.getResources().getString(R.string.pending);
    }


    @Override
    public int getRestartVisibility() {
        return View.GONE;
    }

    @Override
    public int getResumeVisibility() {
        return View.GONE;
    }

    @Override
    public int getStateVisibility() {

        return View.VISIBLE;
    }

    @Override
    public int getRecommendedVisibility() {

        return View.GONE;
    }
}
