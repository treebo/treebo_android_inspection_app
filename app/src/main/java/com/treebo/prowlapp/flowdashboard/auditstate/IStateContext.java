package com.treebo.prowlapp.flowdashboard.auditstate;

/**
 * Created by sumandas on 29/11/2016.
 */

public interface IStateContext {

    IAuditState getState();
    void setState(IAuditState auditState);

}
