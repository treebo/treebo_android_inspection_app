package com.treebo.prowlapp.flowdashboard.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.WindowManager;

import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowdashboard.DaggerDashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardComponent;
import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.adapter.ContactInfoAdapter;
import com.treebo.prowlapp.flowdashboard.presenter.ContactInfoPresenter;
import com.treebo.prowlapp.flowdashboard.usecase.ContactInfoUseCase;
import com.treebo.prowlapp.response.dashboard.ContactInfoResponse;
import com.treebo.prowlapp.usecase.UseCase;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 21/04/17.
 */

public class ContactInfoActivity extends AppCompatActivity
        implements DashboardContract.IContactInfoView, ContactInfoAdapter.contactInfoClickListener {

    @Inject
    public ContactInfoPresenter mPresenter;

    @Inject
    public ContactInfoUseCase mUseCase;

    private ArrayList<ContactInfoResponse.ContactInfoObject> mList;
    private RecyclerView mContactInfoRecyclerView;
    private ContactInfoAdapter mAdapter;

    private View mLoadingView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.activity_gray_background));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        DashboardComponent dashboardComponent = DaggerDashboardComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        dashboardComponent.injectContactInfoActivity(this);

        mLoadingView = findViewById(R.id.loader_layout);

        View backBtn = findViewById(R.id.history_back_iv);
        backBtn.setOnClickListener(view -> onBackPressed());

        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setEnabled(false);

        mContactInfoRecyclerView = (RecyclerView) findViewById(R.id.task_history_rv);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mContactInfoRecyclerView.setLayoutManager(layoutManager1);
        mContactInfoRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new ContactInfoAdapter(mList, this);
        mContactInfoRecyclerView.setAdapter(mAdapter);

        mPresenter.fetchContactInfo();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down);
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setContactInfoUseCase(mUseCase);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void setUpData(ContactInfoResponse response) {
        mList = response.getContactInfoList();
        mAdapter.refreshList(response.getContactInfoList());
    }

    @Override
    public void onSlackIconClick(ContactInfoResponse.ContactInfoObject object) {
        String deepLinkUrl = object.isSlackChannel()
                ? getString(R.string.slack_channel_deeplink_url, BuildConfig.TREEBO_SLACK_TEAM_ID, object.getChannelID())
                : getString(R.string.slack_user_deeplink_url, BuildConfig.TREEBO_SLACK_TEAM_ID, object.getSlackUserID());
        try {
            Uri uri = Uri.parse(deepLinkUrl);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            String url = getString(R.string.slack_browser_url,
                    object.isSlackChannel() ? object.getChannelID() : object.getSlackUserID());
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }
    }

    @Override
    public void onEmailIconClick(ContactInfoResponse.ContactInfoObject object) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + object.getEmailID()));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Prowl App: Contact");
        intent.putExtra(Intent.EXTRA_TEXT, "I want to contact you");
        startActivity(intent);
    }

    @Override
    public void onPhoneIconClick(ContactInfoResponse.ContactInfoObject object) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + object.getPhoneNumber()));
        startActivity(intent);
    }
}
