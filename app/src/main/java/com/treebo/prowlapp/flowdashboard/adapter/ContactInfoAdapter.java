package com.treebo.prowlapp.flowdashboard.adapter;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.dashboard.ContactInfoResponse;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by abhisheknair on 21/04/17.
 */

public class ContactInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private contactInfoClickListener mListener;
    private ArrayList<ContactInfoResponse.ContactInfoObject> mList;

    private static final int VIEW_TYPE_SLACK_CHANNEL = 0;
    private static final int VIEW_TYPE_PERSON = 1;

    public ContactInfoAdapter(ArrayList<ContactInfoResponse.ContactInfoObject> list, contactInfoClickListener listener) {
        this.mList = list;
        this.mListener = listener;
    }

    public void refreshList(ArrayList<ContactInfoResponse.ContactInfoObject> list) {
        mList = new ArrayList<>();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_PERSON:
                View personView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_info,
                        parent, false);
                return new ContactInfoViewHolder(personView, mListener);

            case VIEW_TYPE_SLACK_CHANNEL:
            default:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_slack_channel,
                        parent, false);
                return new ContactSlackChannelHolder(view, mListener);

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContactInfoResponse.ContactInfoObject contactInfoObject = mList.get(position);
        if (contactInfoObject.isSlackChannel()) {
            ((ContactSlackChannelHolder) holder).object = contactInfoObject;
            ((ContactSlackChannelHolder) holder).speciality.setText(contactInfoObject.getDependency());
            ((ContactSlackChannelHolder) holder).name.setText(contactInfoObject.getName());
        } else {
            ((ContactInfoViewHolder) holder).object = contactInfoObject;
            ((ContactInfoViewHolder) holder).speciality.setText(contactInfoObject.getDependency());
            ((ContactInfoViewHolder) holder).name.setText(contactInfoObject.getName());
            ((ContactInfoViewHolder) holder).slack.setVisibility(TextUtils.isEmpty(contactInfoObject.getSlackUserID()) ? GONE : VISIBLE);
            ((ContactInfoViewHolder) holder).email.setVisibility(TextUtils.isEmpty(contactInfoObject.getEmailID()) ? GONE : VISIBLE);
            ((ContactInfoViewHolder) holder).phone.setVisibility(TextUtils.isEmpty(contactInfoObject.getPhoneNumber()) ? GONE : VISIBLE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).isSlackChannel() ? VIEW_TYPE_SLACK_CHANNEL : VIEW_TYPE_PERSON;
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public class ContactInfoViewHolder extends RecyclerView.ViewHolder {

        private ContactInfoResponse.ContactInfoObject object;
        private TreeboTextView speciality;
        private TreeboTextView name;
        private AppCompatImageView avatar;
        private TreeboTextView slack;
        private TreeboTextView email;
        private TreeboTextView phone;

        public ContactInfoViewHolder(View itemView, contactInfoClickListener listener) {
            super(itemView);
            speciality = (TreeboTextView) itemView.findViewById(R.id.title);
            name = (TreeboTextView) itemView.findViewById(R.id.name);
            avatar = (AppCompatImageView) itemView.findViewById(R.id.avatar);
            slack = (TreeboTextView) itemView.findViewById(R.id.ic_slack);
            email = (TreeboTextView) itemView.findViewById(R.id.ic_email);
            phone = (TreeboTextView) itemView.findViewById(R.id.ic_phone);
            slack.setOnClickListener(view -> {
                listener.onSlackIconClick(object);
            });
            email.setOnClickListener(view -> {
                listener.onEmailIconClick(object);
            });
            phone.setOnClickListener(view -> {
                listener.onPhoneIconClick(object);
            });
        }
    }

    public class ContactSlackChannelHolder extends RecyclerView.ViewHolder {

        private ContactInfoResponse.ContactInfoObject object;
        private TreeboTextView speciality;
        private TreeboTextView name;
        private AppCompatImageView avatar;

        public ContactSlackChannelHolder(View itemView, contactInfoClickListener listener) {
            super(itemView);
            speciality = (TreeboTextView) itemView.findViewById(R.id.title);
            name = (TreeboTextView) itemView.findViewById(R.id.name);
            avatar = (AppCompatImageView) itemView.findViewById(R.id.avatar);
            itemView.setOnClickListener(view -> listener.onSlackIconClick(object));
        }
    }

    public interface contactInfoClickListener {
        void onSlackIconClick(ContactInfoResponse.ContactInfoObject object);

        void onEmailIconClick(ContactInfoResponse.ContactInfoObject object);

        void onPhoneIconClick(ContactInfoResponse.ContactInfoObject object);
    }
}
