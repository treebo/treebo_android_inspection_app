package com.treebo.prowlapp.flowdashboard.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.dashboardmodel.SectionedHistoryTaskListModel;
import com.treebo.prowlapp.Models.dashboardmodel.TaskHistoryModel;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class TaskHistorySectionedAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private ArrayList<SectionedHistoryTaskListModel> mSectionedList;

    private String mUserName;

    private String mUserProfilePic;

    private Context mContext;

    public TaskHistorySectionedAdapter(Context context, ArrayList<SectionedHistoryTaskListModel> list) {
        this.mSectionedList = list;
        mContext = context;
    }

    public void setFields(String userName, String profile) {
        this.mUserName = userName;
        this.mUserProfilePic = profile;
    }

    @Override
    public int getSectionCount() {
        return mSectionedList.size();
    }

    @Override
    public int getItemCount(int i) {
        return mSectionedList.get(i).getmTaskHistory().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        SectionedHistoryTaskListModel model = mSectionedList.get(i);
        ((TaskSectionViewHolder) viewHolder).sectionTitle.setText(model.getSectionName());
        ((TaskSectionViewHolder) viewHolder).sectionCount
                .setText(String.valueOf(model.getmTaskHistory().size()));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, int i1, int i2) {
        TaskHistoryModel task = mSectionedList.get(i).getmTaskHistory().get(i1);
//        ((TaskHistoryViewHolder) viewHolder).profilePic.setImageDrawable();
        int taskType = Utils.getTaskTypeFromText(task.getTaskType());
        switch (taskType) {
            case Constants.AUDIT_TASK:
                ((TaskHistoryViewHolder) viewHolder).heading
                        .setText(Utils.getTaskHistoryHeadingString(mContext, mContext.getString(R.string.daily_audit), mUserName, task.getHotelName()));
                ((TaskHistoryViewHolder) viewHolder).roomIcon.setVisibility(View.GONE);
                ((TaskHistoryViewHolder) viewHolder).subHeading.setVisibility(View.GONE);
                break;

            case Constants.ISSUE_TASK:
                String resolutionText = task.getIssueObject().getResolutionStatus() ? mContext.getString(R.string.issue_resolved) : mContext.getString(R.string.issue_updated);
                ((TaskHistoryViewHolder) viewHolder).heading
                        .setText(Utils.getTaskHistoryHeadingString(mContext, resolutionText, mUserName, task.getHotelName()));
                if (task.getIssueObject().getRoomType() == 0) {
                    ((TaskHistoryViewHolder) viewHolder).roomIcon
                            .setImageResource(R.drawable.ic_common_area_gray);
                    ((TaskHistoryViewHolder) viewHolder).subHeading
                            .setText(Utils.getTaskHistoryIssueHeadingString("",
                                    task.getIssueObject().getSubcheckpointID()));
                } else {
                    ((TaskHistoryViewHolder) viewHolder).roomIcon
                            .setImageResource(R.drawable.ic_room);
                    ((TaskHistoryViewHolder) viewHolder).subHeading
                            .setText(Utils.getTaskHistoryIssueHeadingString(task.getIssueObject().getRoomNumber(),
                                    task.getIssueObject().getSubcheckpointID()));
                }
                ((TaskHistoryViewHolder) viewHolder).roomIcon.setVisibility(View.VISIBLE);
                ((TaskHistoryViewHolder) viewHolder).subHeading.setVisibility(View.VISIBLE);
                break;

            case Constants.MANUAL_TASK:
                ((TaskHistoryViewHolder) viewHolder).heading
                        .setText(Utils.getTaskHistoryHeadingString(mContext,
                                task.getManualObject().getHeadlineText(), mUserName, task.getHotelName()));
                ((TaskHistoryViewHolder) viewHolder).roomIcon.setVisibility(View.GONE);
                ((TaskHistoryViewHolder) viewHolder).subHeading.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_history,
                    parent, false);
            return new TaskHistoryViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_section_header,
                    parent, false);
            return new TaskSectionViewHolder(view);
        }
    }

    private class TaskSectionViewHolder extends RecyclerView.ViewHolder {

        TreeboTextView sectionTitle;
        TreeboTextView sectionCount;

        TaskSectionViewHolder(View itemView) {
            super(itemView);
            sectionTitle = (TreeboTextView) itemView.findViewById(R.id.task_section_header_title);
            sectionCount = (TreeboTextView) itemView.findViewById(R.id.task_section_item_count);
        }
    }

    private class TaskHistoryViewHolder extends RecyclerView.ViewHolder {

        private ImageView profilePic;
        private TreeboTextView heading;
        private TreeboTextView subHeading;
        private ImageView roomIcon;

        public TaskHistoryViewHolder(View itemView) {
            super(itemView);
            profilePic = (ImageView) itemView.findViewById(R.id.task_history_profile_pic);
            heading = (TreeboTextView) itemView.findViewById(R.id.task_history_heading);
            subHeading = (TreeboTextView) itemView.findViewById(R.id.task_history_subheading);
            roomIcon = (ImageView) itemView.findViewById(R.id.task_history_room_pic);
        }
    }


}
