package com.treebo.prowlapp.flowincentives.fragment;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.data.DepartmentInformation;
import com.treebo.prowlapp.flowincentives.IncentivesActivity;
import com.treebo.prowlapp.flowincentives.adapter.IncentivesSummaryAdapter;
import com.treebo.prowlapp.fragments.BaseFragment;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sakshamdhawan on 04/05/16.
 */
@Deprecated
public class IncentivesSummaryFragment extends BaseFragment implements IncentivesSummaryAdapter.OnDepartmentClicked {

    private static final String INCENTIVES_SUMMARY_KEY = "incentives_summary_key";
    public ArrayList<DepartmentInformation> departmentInformationList;
    public View rootView;
    public IncentivesSummaryAdapter incentivesSummaryAdapter;
    public boolean isDepartmentListUpdated;


    @BindView(R.id.incentives_summary_recycler_view)
    RecyclerView incentivesSummaryRecycleView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTAG("IncentivesSummaryFragment");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           departmentInformationList = getArguments().getParcelableArrayList(INCENTIVES_SUMMARY_KEY);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isDepartmentListUpdated){
            incentivesSummaryAdapter.notifyDataSetChanged();
            isDepartmentListUpdated =false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_incentives_summary, container, false);
        ButterKnife.bind(this, rootView);
        Log.d("IncentivesFragment" , departmentInformationList.toString());
        incentivesSummaryAdapter = new IncentivesSummaryAdapter(getContext(), departmentInformationList);
        incentivesSummaryAdapter.setOnDepartmentClickedListener(this);
        incentivesSummaryRecycleView.setAdapter(incentivesSummaryAdapter);

        incentivesSummaryRecycleView = (RecyclerView) rootView.findViewById(R.id.incentives_summary_recycler_view);
        incentivesSummaryRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return rootView;
    }


    public static IncentivesSummaryFragment newInstance(ArrayList<DepartmentInformation> departmentInformationList) {
        IncentivesSummaryFragment fragment = new IncentivesSummaryFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(INCENTIVES_SUMMARY_KEY , departmentInformationList);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onDepartmentClicked(String heading) {
        ((IncentivesActivity)getActivity()).openDepartment(heading);
    }

    @OnClick(R.id.image_view_back)
    void click(){
        getActivity().onBackPressed();
    }

    public void updateDepartmentListings( ArrayList<DepartmentInformation> departmentInformationList){
        this.departmentInformationList=departmentInformationList;
        if(isVisible()){
            incentivesSummaryAdapter.mDepartmentInformationList=departmentInformationList;
            incentivesSummaryAdapter.notifyDataSetChanged();
        }else{
            isDepartmentListUpdated =true;
        }
    }


}
