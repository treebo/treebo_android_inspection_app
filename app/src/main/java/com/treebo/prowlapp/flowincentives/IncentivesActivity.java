package com.treebo.prowlapp.flowincentives;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.data.DepartmentInformation;
import com.treebo.prowlapp.data.IndividualIncentivesItem;
import com.treebo.prowlapp.flowhotelview.HotelsOverViewActivity;
import com.treebo.prowlapp.flowincentives.fragment.IncentivesFragment;
import com.treebo.prowlapp.flowincentives.fragment.IncentivesSummaryFragment;
import com.treebo.prowlapp.flowincentives.presenter.IncentivesPresenter;
import com.treebo.prowlapp.flowstaff.HotelStaffActivity;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.mvpviews.IncentivesView;
import com.treebo.prowlapp.response.HotelIncentivesResponse;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.MapperUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Deprecated
public class IncentivesActivity extends AppCompatActivity implements IncentivesView {

    ViewPager pager;
    FrameLayout mContainerLayout;
    private static final String TAG = HotelsOverViewActivity.class.getSimpleName();

    public static final String HOTEL_DATA = "hotel_data" ;
    HotelDataResponse mHotelData;
    public IncentivesPresenter presenter;
    ProgressDialog progress;
    ArrayList<IndividualIncentivesItem> individualIncentivesItems;
    ArrayList<DepartmentInformation> departmentInformationList;
    HashMap<String,IndividualIncentivesItem> individualIncentivesItemsHashMap= new HashMap<String, IndividualIncentivesItem>();
    HashMap<String,DepartmentInformation> departmentInformationHashMap= new HashMap<String, DepartmentInformation>();
    ArrayList<IncentivesFragment> incentivesFragmentList= new ArrayList<IncentivesFragment>();
    ArrayList<FragmentWithHeadings> incentivesFragmentListWithHeadings= new ArrayList<FragmentWithHeadings>();
    ArrayList<String> headingsList = new ArrayList<String>();

    public boolean isStaffAdded;
    public String mDepartmentEnteredIntoStaff;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.include_view_pager);
        Intent intent = getIntent();
        Bundle bundle  = intent.getExtras();
        mHotelData = bundle.getParcelable(HOTEL_DATA);
        initPresenter();
        getIncentives();

    }

    @Override
    public void onIncentivesLoaded(HotelIncentivesResponse hotelIncentivesResponse) {

        individualIncentivesItems = MapperUtils.mapIndividualIncentivesItem(hotelIncentivesResponse);
        departmentInformationList =  MapperUtils.mapDepartmentInformation(hotelIncentivesResponse);

        for( IndividualIncentivesItem individualIncentivesItem:individualIncentivesItems ){
                individualIncentivesItemsHashMap.put(individualIncentivesItem.heading,individualIncentivesItem);
            }
            for(DepartmentInformation departmentInformation:departmentInformationList){
                departmentInformationHashMap.put(departmentInformation.departmant_name,departmentInformation);
        }

        Iterator it = individualIncentivesItemsHashMap.entrySet().iterator();
        incentivesFragmentList.clear();
        incentivesFragmentListWithHeadings.clear();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();

            incentivesFragmentList.add( IncentivesFragment.newInstance( pair.getKey().toString(),(IndividualIncentivesItem) pair.getValue(),
                    departmentInformationHashMap.remove(pair.getKey())));

            incentivesFragmentListWithHeadings.add(new FragmentWithHeadings(pair.getKey().toString(), IncentivesFragment.newInstance( pair.getKey().toString(),(IndividualIncentivesItem) pair.getValue(),
                    departmentInformationHashMap.remove(pair.getKey()))));

            headingsList.add(pair.getKey().toString());
        }

       if(departmentInformationList.size()==0) {
           View v = findViewById(R.id.container);
           SnackbarUtils.show(v, Constants.NO_INCENTIVES);
           new Handler().postDelayed(new Runnable() {
               @Override
               public void run() {
                   finish();
               }
           }, 2000);
       }
        else {
           if(isStaffAdded){
               isStaffAdded=false;
               FragmentManager fm=getSupportFragmentManager();
               Fragment fragment=fm.findFragmentByTag(IncentivesFragment.class.getSimpleName());
               if(fragment!=null){
                   for(IndividualIncentivesItem individualIncentivesItem:individualIncentivesItems){
                       if(individualIncentivesItem.heading.equals(mDepartmentEnteredIntoStaff)){
                           ((IncentivesFragment)fragment).updateDepartmentIncentives(individualIncentivesItem);
                           break;
                       }
                   }
               }
               fragment=fm.findFragmentByTag(IncentivesSummaryFragment.class.getSimpleName());
               if(fragment!=null){
                   ((IncentivesSummaryFragment)fragment).updateDepartmentListings(departmentInformationList);
               }

           }else{
               addFragment(IncentivesSummaryFragment.newInstance(departmentInformationList));
           }

       }

    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progress = new ProgressDialog(IncentivesActivity.this);
        progress.setTitle("Loading");
        progress.setMessage("Please wait...");
        progress.setCancelable(false);
          if(!this.isFinishing())
         progress.show();
    }

    @Override
    public void hideLoading() {
        progress.dismiss();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(this, (dialog, which) -> {
            dialog.dismiss();
            getIncentives();
        }, (dialog1 -> {
            dialog1.dismiss();
            this.finish();
        }));


    }


    public static Intent getCallingIntent(Context context, HotelDataResponse hotelData) {
        Intent intent = new Intent(context, IncentivesActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(HOTEL_DATA, hotelData);
        intent.putExtras(bundle);
        return intent ;
    }

    private void initPresenter() {
        presenter = new IncentivesPresenter();
        presenter.setMvpView(this);
    }

    private void getIncentives(){
        presenter.getAllIncentives(mHotelData.hotelId);

    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }


    public void openDepartment(String heading) {

        int indexFragment = headingsList.indexOf(heading);
        Log.e("heading clicked",heading);
        Log.e("index", "" + indexFragment);
        if(indexFragment<0) {
            View view = findViewById(R.id.container);
            SnackbarUtils.show(view, Constants.NO_EMPLOYEE_ERROR_MESSAGE);
            return;
        }
        addFragment(incentivesFragmentList.get(indexFragment));

    }


    void setScreen(int position){
        pager.setCurrentItem(position);

    }

    void setScreen(String heading){

        setScreen(headingsList.indexOf(heading));
    }

    private  class FragmentWithHeadings{
        String heading;
        IncentivesFragment fragment;
        public FragmentWithHeadings(String heading, IncentivesFragment fragment){
            this.heading = heading;
            this.fragment=fragment;
        }
    }

    public void addFragment(BaseFragment fragment) {

        Log.e(TAG, "addFragment called ");
       mContainerLayout = (FrameLayout) findViewById(R.id.container);
        FragmentManager fm = getSupportFragmentManager();
       fm.beginTransaction().replace(R.id.container, fragment, fragment.getClass().getSimpleName()).addToBackStack(fragment.getTAG()).commit();


    }


    public  void addStaffToDepartment(String departmentHeading){
        mDepartmentEnteredIntoStaff=departmentHeading;
        Intent intentToLaunch = HotelStaffActivity.getCallingIntent(this);
        Bundle bundle = new Bundle();
        bundle.putParcelable(HotelStaffActivity.HOTEL_DATA, mHotelData);
        bundle.putBoolean(Utils.IS_FROM_INCENTIVES, true);
        bundle.putString(Utils.INCENTIVES_DEPARTMENT,departmentHeading);
        intentToLaunch.putExtras(bundle);
        startActivityForResult(intentToLaunch, Utils.ADD_STAFF_FROM_INCENTIVES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case Utils.ADD_STAFF_FROM_INCENTIVES:
                if(resultCode==Utils.ADDED_STAFF_FROM_INCENTIVES){
                    isStaffAdded=true;
                    presenter.getAllIncentives(mHotelData.hotelId);
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.emerald));
            }
            changeStatusBarColor(R.color.emerald);

        }

    }

    public void changeStatusBarColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

}
