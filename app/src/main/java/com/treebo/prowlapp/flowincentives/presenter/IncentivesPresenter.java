package com.treebo.prowlapp.flowincentives.presenter;

import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.IncentivesView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.HotelIncentivesResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.GetIncentivesUseCase;
import com.treebo.prowlapp.usecase.UseCase;

/**
 * Created by devesh on 29/04/16.
 */
@Deprecated
public class IncentivesPresenter  implements BasePresenter {

    IncentivesView mBaseView;

    UseCase getIncentivesUseCase;


    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView = (IncentivesView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    public void getAllIncentives(int hotel_id){
        hideRetry();
        showLoading();
        getIncentivesUseCase = new GetIncentivesUseCase(hotel_id);
        getIncentivesUseCase.execute(new IncentivesSubscriber(this));

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if(getIncentivesUseCase != null){
            getIncentivesUseCase.unsubscribe();
        }

    }

    @Override
    public BaseView getView() {
        return mBaseView;
    }


    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideLoading() {
        mBaseView.hideLoading();
    }

    private void showRetry() {
        mBaseView.showRetry();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }

    class IncentivesSubscriber extends BaseObserver<HotelIncentivesResponse> {

        public IncentivesSubscriber(BasePresenter presenter) {
            super(presenter, getIncentivesUseCase);
        }


        @Override
        public void onCompleted() {
         mBaseView.hideLoading();
        }


        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onNext(HotelIncentivesResponse hotelIncentivesResponse) {
            mBaseView.onIncentivesLoaded(hotelIncentivesResponse);
        }
    }

}
