package com.treebo.prowlapp.flowincentives.presenter;

import com.treebo.prowlapp.data.IndividualEntry;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.MarkIncentivePaidView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.MarkIncentiveDone;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.StaffIncentivePaidUseCase;

/**
 * Created by sumandas on 03/07/2016.
 */
@Deprecated
public class StaffIncentivesPaidPresenter implements BasePresenter {

    StaffIncentivePaidUseCase mUsecase;

    MarkIncentivePaidView mBaseView;
    IndividualEntry mEntry;

    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView = (MarkIncentivePaidView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    public void markIncentivesPaid(IndividualEntry entry){
        hideRetry();
        showLoading();
        mEntry=entry;
        mUsecase = new StaffIncentivePaidUseCase(entry);
        mUsecase.execute(new IncentivesSubscriber(this));

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if(mUsecase != null){
            mUsecase.unsubscribe();
        }

    }

    @Override
    public BaseView getView() {
        return mBaseView;
    }


    private void showLoading() {
        mBaseView.showLoading();
    }


    private void hideRetry() {
        mBaseView.hideRetry();
    }

    class IncentivesSubscriber extends BaseObserver<MarkIncentiveDone> {

        public IncentivesSubscriber(BasePresenter presenter) {
            super(presenter, mUsecase);
        }


        @Override
        public void onCompleted() {
            mBaseView.hideLoading();
        }


        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onNext(MarkIncentiveDone response) {
            if(response.status.equals("success")){
                mBaseView.onMarkIncentiveDone(mEntry);
            }

        }
    }

}
