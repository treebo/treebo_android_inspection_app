package com.treebo.prowlapp.flowincentives.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.data.IndividualEntry;
import com.treebo.prowlapp.data.IndividualIncentivesItem;
import com.treebo.prowlapp.flowincentives.presenter.StaffIncentivesPaidPresenter;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.NetworkUtil;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.StringUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.tubb.smrv.SwipeHorizontalMenuLayout;

/**
 * Created by sakshamdhawan on 29/04/16.
 */
@Deprecated
public class IncentivesListAdapter extends RecyclerView.Adapter<IncentivesListAdapter.IncentivesViewHolder> {
    public IndividualIncentivesItem individualIncentivesItem;
    private Context mContext;
    private int mColorResource;
    private StaffIncentivesPaidPresenter mPresenter;
    private Activity mActivity;


    public IncentivesListAdapter(StaffIncentivesPaidPresenter presenter, IndividualIncentivesItem individualIncentivesItem, int colorResource,Activity activity) {
        this.individualIncentivesItem = individualIncentivesItem;
        this.mColorResource = colorResource;
        mPresenter=presenter;
        mActivity=activity;
    }

    @Override
    public IncentivesListAdapter.IncentivesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View singleCardLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.incentives_swipe_layout_row, parent, false);
        return new IncentivesViewHolder(singleCardLayout);
    }

    @Override
    public void onBindViewHolder(IncentivesViewHolder holder, int position) {
        holder.mPosition = position;
        final IndividualEntry entry=individualIncentivesItem.individualEntryList.get(position);
        SwipeHorizontalMenuLayout itemView = (SwipeHorizontalMenuLayout) holder.itemView;
        if(entry.monthly_incentive.is_paid){
            itemView.setSwipeEnable(false);
        }else{
            itemView.setSwipeEnable(true);
        }

        holder.mStaffImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.audit_categories_bullet));
        holder.totalAmount.setText(StringUtils.addRupeeSymbol(entry.tia));
        holder.totalAmount.setTextColor(ContextCompat.getColor(mContext, mColorResource));

        if(entry.got_audited_this_month){
            holder.startingAmount.setPaintFlags(holder.startingAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            holder.startingAmount.setPaintFlags(holder.startingAmount.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }

        holder.startingAmount.setText(StringUtils.addRupeeSymbol(individualIncentivesItem.individualEntryList.get(position).sia));

        if(entry.has_joined_this_month){
            if(!TextUtils.isEmpty(individualIncentivesItem.individualEntryList.get(position).staff_url)) {
                Utils.convertImageUrlToRoundedView(mContext , individualIncentivesItem.individualEntryList.get(position).staff_url,holder.mStaffImageView );
            }
        }else{
            if(!TextUtils.isEmpty(individualIncentivesItem.individualEntryList.get(position).staff_url)) {
                Utils.getPaidRoundedImageDrawable(mContext,individualIncentivesItem.individualEntryList.get(position)
                        .staff_url,holder.mStaffImageView ,individualIncentivesItem.individualEntryList.get(position)
                        .monthly_incentive.is_paid);
            }else{
                Utils.getPaidRoundedImageDrawable(mContext,holder.mStaffImageView ,individualIncentivesItem.individualEntryList.get(position)
                        .monthly_incentive.is_paid);
            }
        }


        holder.name.setText(StringUtils.capitalize(individualIncentivesItem.individualEntryList.get(position).first_name) + " " + individualIncentivesItem.individualEntryList.get(position).last_name) ;

        holder.firstMonthIncentive.setText(StringUtils.addRupeeSymbol(entry.monthly_incentive.incentive_list.get(0).incentive));
        holder.secondMonthIncentive.setText(StringUtils.addRupeeSymbol(entry.monthly_incentive.incentive_list.get(1).incentive));
        holder.thirdMonthIncentive.setText(StringUtils.addRupeeSymbol(entry.monthly_incentive.incentive_list.get(2).incentive));

        holder.firstMonthIncentive.setTextColor(ContextCompat.getColor(mContext,
                Utils.getIndividualIncentivesColor(entry.monthly_incentive.incentive_list.get(0).starting_incentive,
                        entry.monthly_incentive.incentive_list.get(0).incentive)));

        holder.secondMonthIncentive.setTextColor(ContextCompat.getColor(mContext,
                Utils.getIndividualIncentivesColor(entry.monthly_incentive.incentive_list.get(1).starting_incentive,
                        entry.monthly_incentive.incentive_list.get(1).incentive)));

        holder.thirdMonthIncentive.setTextColor(ContextCompat.getColor(mContext,
                Utils.getIndividualIncentivesColor(entry.monthly_incentive.incentive_list.get(2).starting_incentive,
                        entry.monthly_incentive.incentive_list.get(2).incentive)));

        holder.firstMonth.setText(entry.monthly_incentive.incentive_list.get(0).month.substring(0, 3).toUpperCase());
        holder.secondMonth.setText(entry.monthly_incentive.incentive_list.get(1).month.substring(0, 3).toUpperCase());
        holder.thirdMonth.setText(entry.monthly_incentive.incentive_list.get(2).month.substring(0, 3).toUpperCase());

        holder.confirmIncentivesGiven.setOnClickListener(v -> {

            if (!NetworkUtil.isConnected(mContext)) {
                AlertDialogUtils.showInternetDisconnectedDialog(mActivity,
                        (dialog, which) -> {
                            dialog.dismiss();
                            onRetryButtonClicked(entry,holder.itemView);
                            return;
                        }, (dialog) -> {
                            dialog.dismiss();
                            return;
                        }
                );
                return;
            }
                mPresenter.markIncentivesPaid(entry);
            itemView.smoothCloseMenu();

        });
    }

    public void onRetryButtonClicked(IndividualEntry entry,View view){
        if (!NetworkUtil.isConnected(mContext)) {
            SnackbarUtils.show(view,mContext.getString(R.string.network_error));
        }else{
            mPresenter.markIncentivesPaid(entry);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    @Override
    public int getItemCount() {
        return individualIncentivesItem.individualEntryList.size()>0?individualIncentivesItem.individualEntryList.size():0;
    }


    public class IncentivesViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView totalAmount;
        TextView startingAmount;
        TextView thirdMonthIncentive;
        TextView secondMonthIncentive;
        TextView firstMonthIncentive;
        TextView thirdMonth;
        TextView secondMonth;
        TextView firstMonth;
        View itemView;
        int mPosition = -1;
        ImageView mStaffImageView;

        ImageButton confirmIncentivesGiven;


        public IncentivesViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            mStaffImageView= (ImageView) itemView.findViewById(R.id.staff_row_bullet);
            name = (TextView) itemView.findViewById(R.id.person_name);
            totalAmount = (TextView) itemView.findViewById(R.id.total_amount);
            startingAmount = (TextView) itemView.findViewById(R.id.starting_amount);
            thirdMonthIncentive = (TextView) itemView.findViewById(R.id.third_last_month_incentive);
            secondMonthIncentive= (TextView) itemView.findViewById(R.id.second_last_month_incentive);
            firstMonthIncentive= (TextView) itemView.findViewById(R.id.last_month_incentive);

            thirdMonth = (TextView) itemView.findViewById(R.id.third_last_month);
            secondMonth= (TextView) itemView.findViewById(R.id.second_last_month);
            firstMonth= (TextView) itemView.findViewById(R.id.last_month);
            confirmIncentivesGiven=(ImageButton)itemView.findViewById(R.id.confirm_incentives_given);

            this.itemView=itemView;
        }
    }




}
