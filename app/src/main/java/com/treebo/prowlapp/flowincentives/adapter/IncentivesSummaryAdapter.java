package com.treebo.prowlapp.flowincentives.adapter;


import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.data.DepartmentInformation;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.ButterKnife;

/**
 * Created by sakshamdhawan on 04/05/16.
 */
@Deprecated
public class IncentivesSummaryAdapter extends RecyclerView.Adapter<IncentivesSummaryAdapter.IncentivesSummaryViewHolder> {
    public ArrayList<DepartmentInformation> mDepartmentInformationList;
    private OnDepartmentClicked mOnDepartmentClicked;
    private Context mContext;

    public IncentivesSummaryAdapter(Context context, ArrayList<DepartmentInformation> departmentInformationList) {
        this.mDepartmentInformationList = departmentInformationList;
        this.mContext = context;
    }

    public void setOnDepartmentClickedListener(OnDepartmentClicked onDepartmentClicked) {
        this.mOnDepartmentClicked = onDepartmentClicked;
    }

    @Override
    public IncentivesSummaryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View singleCardLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.incentives_view, parent, false);
        return new IncentivesSummaryViewHolder(singleCardLayout);
    }

    @Override
    public void onBindViewHolder(IncentivesSummaryViewHolder holder, final int position) {
        if (mDepartmentInformationList.get(position) != null) {
            holder.title.setText(mDepartmentInformationList.get(position).departmant_name);

            char per = mDepartmentInformationList.get(position).till_date > 0 ? '+' : 0;
            holder.monthValue.setText("" + per + mDepartmentInformationList.get(position).till_date);
            if (mDepartmentInformationList.get(position).till_date < 0)
                holder.monthValue.setTextColor(ContextCompat.getColor(mContext, R.color.coral));

            per = mDepartmentInformationList.get(position).todaysChanges > 0 ? '+' : 0;
            holder.daySummaryCombined.setText(mDepartmentInformationList.get(position).score + "% (" + per + mDepartmentInformationList.get(position).todaysChanges + ") " + "Last Audit");

            holder.summaryDate.setText("1 " + Utils.getCurrentMonth().substring(0, 3) + " - " + Utils.getCurrentDate() + " " + Utils.getCurrentMonth().substring(0, 3));

        }

        holder.itemView.setOnClickListener(view ->
                mOnDepartmentClicked.onDepartmentClicked(mDepartmentInformationList.get(position).departmant_name));


    }

    @Override
    public int getItemCount() {
        return mDepartmentInformationList.size();
    }


    public class IncentivesSummaryViewHolder extends RecyclerView.ViewHolder {


        TextView title;
        TextView monthValue;
        TextView daySummaryCombined;
        TextView summaryDate;
        View itemView;


        public IncentivesSummaryViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            title = (TextView) itemView.findViewById(R.id.incentives_summary_department_title);
            monthValue = (TextView) itemView.findViewById(R.id.incentives_summary_monthly_value);
            daySummaryCombined = (TextView) itemView.findViewById(R.id.incentives_summary_day_text);
            summaryDate = (TextView) itemView.findViewById(R.id.incentives_summary_date);
            ButterKnife.bind(this, itemView);
        }
    }


    public interface OnDepartmentClicked {
        void onDepartmentClicked(String heading);
    }

}
