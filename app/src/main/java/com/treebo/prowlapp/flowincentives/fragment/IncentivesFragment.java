package com.treebo.prowlapp.flowincentives.fragment;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.data.DepartmentInformation;
import com.treebo.prowlapp.data.IndividualEntry;
import com.treebo.prowlapp.data.IndividualIncentivesItem;
import com.treebo.prowlapp.flowincentives.IncentivesActivity;
import com.treebo.prowlapp.flowincentives.adapter.IncentivesListAdapter;
import com.treebo.prowlapp.flowincentives.presenter.StaffIncentivesPaidPresenter;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.mvpviews.MarkIncentivePaidView;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.tubb.smrv.SwipeMenuRecyclerView;

@Deprecated
public class IncentivesFragment extends BaseFragment implements AppBarLayout.OnOffsetChangedListener,MarkIncentivePaidView {

    private static final String FRAGMENT_KEY = "fragment_key";
    private static final String HEADING_KEY = "heading";
    private static final String DEPARTMENT_INFORMATION_KEY = "department_information_key";
    private SwipeMenuRecyclerView mIncentivesRecycleView;

    public IndividualIncentivesItem individualIncentivesItem;
    public String heading;
    private DepartmentInformation departmentInformation;
    View rootView;
    AppBarLayout appBarLayout;
    RelativeLayout mEmptyStaffView;
    TextView mAddStaffView;
    IncentivesListAdapter mIncentivesListAdapter;

    StaffIncentivesPaidPresenter mPaidPresenter;

    ProgressDialog progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTAG("IncentivesFragment");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            individualIncentivesItem = getArguments().getParcelable(FRAGMENT_KEY);
            heading = getArguments().getString(HEADING_KEY);
            departmentInformation = getArguments().getParcelable(DEPARTMENT_INFORMATION_KEY);
        }
        initPresenter();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_incentives, container, false);


        rootView.findViewById(R.id.incentives_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = getActivity().getWindow();
                    window.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.emerald));
                }

                getActivity().onBackPressed();
            }
        });

        setAppbarListener();


        if (departmentInformation != null) {
            setBackgroundColor();
            SetBackDropImage();

            setDayAndMonthSummary();
        } else {
            setNoDataView();
            Log.d("department", "null");
        }

        setTitle();
        setUpRecycleView();

        return rootView;
    }

    private void initPresenter() {
        mPaidPresenter = new StaffIncentivesPaidPresenter();
        mPaidPresenter.setMvpView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPaidPresenter.pause();
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        mPaidPresenter.destroy();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (appBarLayout != null) {
            appBarLayout.setExpanded(true, true);
        }
    }

    private void setAppbarListener() {
        appBarLayout = (AppBarLayout) rootView.findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(this);
    }

    private void setNoDataView() {
        Log.d("inside", "setNo data");
        rootView.findViewById(R.id.department_info).setVisibility(View.GONE);
        rootView.findViewById(R.id.no_data).setVisibility(View.VISIBLE);
    }

    private void setUpRecycleView() {
        mIncentivesRecycleView = (SwipeMenuRecyclerView) rootView.findViewById(R.id.incentives_list_recycler_view);
        mEmptyStaffView=(RelativeLayout) rootView.findViewById(R.id.staff_empty_view);
        mIncentivesRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));


        if (individualIncentivesItem != null) {
            int amountColor = R.color.emerald;
            if (departmentInformation.till_date < 0) {
                amountColor = R.color.coral;
            }
            mIncentivesListAdapter = new IncentivesListAdapter(mPaidPresenter,individualIncentivesItem, amountColor,getActivity());
            mIncentivesRecycleView.setAdapter(mIncentivesListAdapter);
            if (!individualIncentivesItem.individualEntryList.isEmpty()) {
                mIncentivesRecycleView.setVisibility(View.VISIBLE);
                mEmptyStaffView.setVisibility(View.GONE);
            } else {
                mIncentivesRecycleView.setVisibility(View.GONE);
                mEmptyStaffView.setVisibility(View.VISIBLE);
                mAddStaffView = (TextView) mEmptyStaffView.findViewById(R.id.add_staff_button_empty);
                mAddStaffView.setOnClickListener(v ->
                {
                    ((IncentivesActivity) getActivity()).addStaffToDepartment(heading);

                });
            }

        }

    }

    private void setDayAndMonthSummary() {
        char per;
        if (!TextUtils.isEmpty(String.valueOf(departmentInformation.score)))
            if(departmentInformation.score==0){
                ((TextView) rootView.findViewById(R.id.incentive_text1)).setText("NA");
            }else{
                ((TextView) rootView.findViewById(R.id.incentive_text1)).setText(String.valueOf(departmentInformation.score) + "%");
            }

        if (!TextUtils.isEmpty(String.valueOf(departmentInformation.todaysChanges))) {
            per = departmentInformation.todaysChanges > 0 ? '+' : 0;
            ((TextView) rootView.findViewById(R.id.incentive_text2)).setText("(" + per + String.valueOf(departmentInformation.todaysChanges) + ")");
        }
        if (!TextUtils.isEmpty(String.valueOf(departmentInformation.till_date))) {
            per = departmentInformation.till_date > 0 ? '+' : 0;
            ((TextView) rootView.findViewById(R.id.incentive_text4)).setText(per + String.valueOf(departmentInformation.till_date));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && departmentInformation.till_date < 0) {
                Window window = getActivity().getWindow();
                window.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.coral));
            }
        }
        ((TextView) rootView.findViewById(R.id.incentive_text5)).setText(Utils.getCurrentMonth() + " 1 - " + Utils.getCurrentDate());
    }


    private void setTitle() {
        TextView title = ((TextView) rootView.findViewById(R.id.incentives_title_text));
        title.setText(heading);
    }

    private void setBackgroundColor() {
        if (departmentInformation != null)
            if (departmentInformation.till_date < 0) {
                rootView.findViewById(R.id.collapsing_toolbar).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.coral));
                rootView.findViewById(R.id.appbar).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.coral));
                rootView.findViewById(R.id.toolbar).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.coral));
            } else {
                rootView.findViewById(R.id.collapsing_toolbar).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.emerald));
                rootView.findViewById(R.id.appbar).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.emerald));
                rootView.findViewById(R.id.toolbar).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.emerald));

            }

    }

    private void SetBackDropImage() {
        ImageView backDropImage = ((ImageView) rootView.findViewById(R.id.backdrop));
        Log.d("department name ", departmentInformation.departmant_name);

        switch (departmentInformation.departmant_name) {
            case "Owner / Manager":
                backDropImage.setImageResource(R.drawable.noun_437491_cc);
                break;
            case "House Keeping":
                backDropImage.setImageResource(R.drawable.noun_19395_cc);
                break;
            case "Front Office":
                backDropImage.setImageResource(R.drawable.front_office);
                break;
            case "Food & Beverage":
                backDropImage.setImageResource(R.drawable.page_1);
                break;

        }

    }


    public static IncentivesFragment newInstance(String heading, IndividualIncentivesItem individualIncentivesItem, DepartmentInformation departmentInformation) {
        IncentivesFragment fragment = new IncentivesFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(FRAGMENT_KEY, individualIncentivesItem);
        bundle.putString(HEADING_KEY, heading);
        bundle.putParcelable(DEPARTMENT_INFORMATION_KEY, departmentInformation);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        Log.d("offset", String.valueOf(offset));
        if (Math.abs(offset) >= appBarLayout.getTotalScrollRange())
            ((TextView) rootView.findViewById(R.id.collapsing_bar_title)).setText(heading);
        else {
            ((TextView) rootView.findViewById(R.id.collapsing_bar_title)).setText("");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPaidPresenter.resume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume();
        } else {
        }
    }

    public void  updateDepartmentIncentives(IndividualIncentivesItem individualIncentivesItem){
        this.individualIncentivesItem=individualIncentivesItem;
        mIncentivesListAdapter.individualIncentivesItem=individualIncentivesItem;
        mIncentivesRecycleView.setVisibility(View.VISIBLE);
        mEmptyStaffView.setVisibility(View.GONE);
        mIncentivesListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onMarkIncentiveDone(IndividualEntry entry) {
        entry.monthly_incentive.is_paid=true;
        setUpRecycleView();

    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Please wait...");
        progress.setCancelable(false);
        if(!getActivity().isFinishing())
            progress.show();
    }

    @Override
    public void hideLoading() {
        if(progress!=null){
            progress.dismiss();
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(getActivity(), (dialog, which) -> {
            dialog.dismiss();
            //TODO retry call
        }, (dialog1 -> {
            dialog1.dismiss();
            getActivity().finish();
        }));



    }

}