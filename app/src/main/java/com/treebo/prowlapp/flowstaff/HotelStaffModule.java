package com.treebo.prowlapp.flowstaff;

import com.treebo.prowlapp.flowstaff.presenter.HotelStaffPresenter;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by abhisheknair on 25/05/17.
 */
@Module
public class HotelStaffModule {

    @Provides
    RestClient providesRestClient() {
        return new RestClient();
    }

    @Provides
    HotelStaffPresenter providesPortfolioPresenter() {
        return new HotelStaffPresenter();
    }


}
