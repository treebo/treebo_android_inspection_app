package com.treebo.prowlapp.flowstaff.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowstaff.fragment.StatesBottomSheetFragment;

import java.lang.ref.WeakReference;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by aa on 11/07/16.
 */
public class StatesBottomSheetAdapter extends RecyclerView.Adapter<StatesBottomSheetAdapter.StateViewHolder> {

    private String[] mStatesList;
    private StatesBottomSheetFragment.StateSelectedListener mStateSelectedListener;

    public StatesBottomSheetAdapter(String[] statesList,
                                    StatesBottomSheetFragment.StateSelectedListener stateSelectedListener) {
        mStatesList = statesList;
        mStateSelectedListener = stateSelectedListener;
    }

    @Override
    public StateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StateViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_state_list, parent, false), mStateSelectedListener);
    }

    @Override
    public void onBindViewHolder(StateViewHolder stateViewHolder, int position) {
        stateViewHolder.tvStateName.setText(mStatesList[position]);
    }

    @Override
    public int getItemCount() {
        if (mStatesList == null) {
            return 0;
        } else {
            return mStatesList.length;
        }
    }

    public static class StateViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public WeakReference<StatesBottomSheetFragment.StateSelectedListener> mStateSelectListenerWeakReference;
        public TextView tvStateName;

        public StateViewHolder(View itemView,
                               StatesBottomSheetFragment.StateSelectedListener stateSelectedListenerWeakReference) {
            super(itemView);
            mStateSelectListenerWeakReference = new WeakReference<>(stateSelectedListenerWeakReference);
            tvStateName = (TextView) itemView.findViewById(R.id.tv_state_name);

            tvStateName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_state_name:
                    StatesBottomSheetFragment.StateSelectedListener stateSelectedListener = mStateSelectListenerWeakReference.get();
                    if (stateSelectedListener != null) {
                        stateSelectedListener.onStateSelected(getAdapterPosition());
                    }
                    break;

                default:
                    break;
            }
        }
    }
}
