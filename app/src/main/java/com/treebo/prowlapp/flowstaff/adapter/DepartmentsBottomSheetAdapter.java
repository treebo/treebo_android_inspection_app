package com.treebo.prowlapp.flowstaff.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowstaff.fragment.DepartmentsBottomSheetFragment;
import com.treebo.prowlapp.Models.DepartmentModel;

import java.lang.ref.WeakReference;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by aa on 11/07/16.
 */
public class DepartmentsBottomSheetAdapter extends RecyclerView.Adapter<DepartmentsBottomSheetAdapter.DepartmentViewHolder> {

    private List<DepartmentModel> mDepartmentModelList;
    private DepartmentsBottomSheetFragment.DepartmentSelectedListener mDepartmentSelectedListener;

    public DepartmentsBottomSheetAdapter(List<DepartmentModel> departmentsList,
                                         DepartmentsBottomSheetFragment.DepartmentSelectedListener departmentSelectedListener) {
        mDepartmentModelList = departmentsList;
        mDepartmentSelectedListener = departmentSelectedListener;
    }

    @Override
    public DepartmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DepartmentViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_department_list, parent, false), mDepartmentSelectedListener);
    }

    @Override
    public void onBindViewHolder(DepartmentViewHolder departmentViewHolder, int position) {
        departmentViewHolder.tvDepartmentName.setText(mDepartmentModelList.get(position).name);
    }

    @Override
    public int getItemCount() {
        if(mDepartmentModelList == null) {
            return 0;
        } else {
            return mDepartmentModelList.size();
        }
    }

    public static class DepartmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public WeakReference<DepartmentsBottomSheetFragment.DepartmentSelectedListener> mDepartmentSelectedListenerWeakReference;
        public TextView tvDepartmentName;

        public DepartmentViewHolder(View itemView,
                                    DepartmentsBottomSheetFragment.DepartmentSelectedListener departmentSelectedListener) {
            super(itemView);
            mDepartmentSelectedListenerWeakReference = new WeakReference<>(departmentSelectedListener);
            tvDepartmentName = (TextView) itemView.findViewById(R.id.tv_department_name);

            tvDepartmentName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_department_name:
                    DepartmentsBottomSheetFragment.DepartmentSelectedListener departmentSelectedListener =
                            mDepartmentSelectedListenerWeakReference.get();
                    if(departmentSelectedListener != null) {
                        departmentSelectedListener.onDepartmentSelected(getAdapterPosition());
                    }
                    break;

                default:
                    break;
            }
        }
    }
}
