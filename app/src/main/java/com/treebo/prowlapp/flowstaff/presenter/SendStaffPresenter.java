package com.treebo.prowlapp.flowstaff.presenter;

import com.treebo.prowlapp.flowstaff.IStaffContract;
import com.treebo.prowlapp.flowstaff.observer.UpdateStaffBankDetailsObserver;
import com.treebo.prowlapp.flowstaff.usecase.UpdateStaffBankDetailsUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.SendStaffView;
import com.treebo.prowlapp.response.AddStaffUserResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.AddStaffUseCase;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by devesh on 02/05/16.
 */
public class SendStaffPresenter implements IStaffContract.IAddStaffPresenter {

    UseCase addStaffUseCase;

    SendStaffView mBaseView;

    UpdateStaffBankDetailsUseCase mBankDetailsUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView = (SendStaffView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }


    public void addStaff(String first_name, String last_name, String phone, String fathers_name, String staff_url,
            String address_line_1, String address_line_2, String state, String city, String pin_code,
            int department, int hotel, int salary,
            Date date_of_joining, Date date_of_leaving, Date leave_start_date, Date leave_end_date,
            Date date_of_birth, ArrayList<String> languagesRead, ArrayList<String> languagesSpeak,
            boolean isBankDetailsEnabled, String accountNo, String ifsc, String bankName,
            String branch, String bankCity) {
        hideRetry();
        showLoading();
        addStaffUseCase = new AddStaffUseCase(first_name, last_name, phone, fathers_name, staff_url,
                address_line_1, address_line_2, state, city, pin_code,
                department, hotel, salary,
                date_of_joining, date_of_leaving, leave_start_date, leave_end_date, date_of_birth,
                languagesRead, languagesSpeak);
        // Log.d("AddStaffUseCase",addStaffUseCase.toString());
        AddStaffObserver observer = new AddStaffObserver(this);
        observer.setFields(isBankDetailsEnabled, first_name, accountNo, ifsc, bankName, branch, bankCity);
        addStaffUseCase.execute(observer);

    }


    @Override
    public void destroy() {
        if (addStaffUseCase != null) {
            addStaffUseCase.unsubscribe();
        }
    }

    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideLoading() {
        mBaseView.hideLoading();
    }

    private void showRetry() {
        mBaseView.showRetry();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }


    @Override
    public BaseView getView() {
        return mBaseView;
    }

    @Override
    public void postBankDetails(int staffID, String staffName, String accountNo, String ifsc, String name, String branch, String city) {
        mBankDetailsUseCase = new UpdateStaffBankDetailsUseCase(staffID, staffName, accountNo, ifsc, name,
                branch, city);
        mBankDetailsUseCase.execute(new UpdateStaffBankDetailsObserver(this, mBankDetailsUseCase));
    }

    @Override
    public void onAddStaffSuccess() {
        mBaseView.onStaffAdded(false);
    }

    @Override
    public void showError(String message) {
        mBaseView.showError(message);
    }

    @Override
    public void handleBankDetailsError(String message) {
        mBaseView.onStaffAdded(true);
    }

    public class AddStaffObserver extends BaseObserver<AddStaffUserResponse> {

        private SendStaffPresenter mPresenter;
        private boolean isBankDetailsEnabled;
        private String staffName;
        private String accountNumber;
        private String ifscCode;
        private String bankName;
        private String branch;
        private String city;

        public AddStaffObserver(SendStaffPresenter presenter) {
            super(presenter, addStaffUseCase);
            this.mPresenter = presenter;
        }

        public void setFields(boolean isBankDetailsEnabled,String staffName, String accountNo,
                              String ifsc, String name, String branch, String city) {
            this.isBankDetailsEnabled = isBankDetailsEnabled;
            this.staffName = staffName;
            this.accountNumber = accountNo;
            this.ifscCode = ifsc;
            this.bankName = name;
            this.branch = branch;
            this.city = city;
        }

        @Override
        public void onCompleted() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            super.onError(e);
        }

        @Override
        public void onNext(AddStaffUserResponse response) {
            if (response.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)) {
                if (isBankDetailsEnabled)  {
                    mPresenter.postBankDetails(response.getStaffID(), staffName, accountNumber,
                            ifscCode, bankName, branch, city);
                } else {
                    mPresenter.onAddStaffSuccess();
                }
            } else {
                mBaseView.onStaffAddFailure(response.msg);
            }
        }
    }


}
