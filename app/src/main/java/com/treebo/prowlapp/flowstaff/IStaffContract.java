package com.treebo.prowlapp.flowstaff;

import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by abhisheknair on 12/05/17.
 */

public interface IStaffContract {

    interface IUpdateStaffPresenter extends BasePresenter {
        void showError(String message);

        void onSuccess();
    }

    interface IAddStaffPresenter extends BasePresenter {
        void postBankDetails(int staffID, String staffName, String accountNo, String ifsc,
                             String name, String branch, String city);

        void onAddStaffSuccess();

        void showError(String message);

        void handleBankDetailsError(String message);
    }
}
