package com.treebo.prowlapp.flowstaff.usecase;

import android.util.Log;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 12/05/17.
 */

public class UpdateStaffBankDetailsUseCase extends UseCase<BaseResponse> {

    private int staffID;
    private String userName;
    private String bankAccountNumber;
    private String bankIfscCode;
    private String bankName;
    private String bankBranch;
    private String bankCity;

    public UpdateStaffBankDetailsUseCase(int id, String name, String accountNumber,
                                         String ifscCode, String bank,
                                         String branch, String city) {
        this.staffID = id;
        this.userName = name;
        this.bankAccountNumber = accountNumber;
        this.bankIfscCode = ifscCode;
        this.bankName = bank;
        this.bankBranch = branch;
        this.bankCity = city;
    }

    @Override
    protected Observable<BaseResponse> getObservable() {
        Log.d("RestClient", "Post bank details API call started");
        return new RestClient().postStaffBankDetails(staffID, userName, bankAccountNumber, bankIfscCode,
                bankName, bankBranch, bankCity);
    }
}
