package com.treebo.prowlapp.flowstaff.presenter;

import com.treebo.prowlapp.Models.HotelStaffModelResponse;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.HotelStaffView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.GetAllStaffUseCase;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by devesh on 28/04/16.
 */
public class HotelStaffPresenter implements BasePresenter {

    HotelStaffView mBaseView;

    UseCase getAllStaffUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView =(HotelStaffView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    public void getAllStaff(int hotelId){
        hideRetry();
        showLoading();
        getAllStaffUseCase = new GetAllStaffUseCase(hotelId);
        getAllStaffUseCase.execute(new StaffResponseObserver(HotelStaffPresenter.this));
    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if(getAllStaffUseCase != null){
            getAllStaffUseCase.unsubscribe();
        }

    }


    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideLoading() {
        mBaseView.hideLoading();
    }

    private void showRetry() {
        mBaseView.showRetry();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }


    @Override
    public BaseView getView() {
        return mBaseView;
    }

    public class StaffResponseObserver extends BaseObserver<HotelStaffModelResponse> {

        public StaffResponseObserver(BasePresenter presenter) {
            super(presenter, getAllStaffUseCase);
        }

        @Override
        public void onCompleted() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            super.onError(e);
        }

        @Override
        public void onNext(HotelStaffModelResponse response) {
            if (response.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)) {
                mBaseView.onStaffLoaded(response.getStaffModelList());
            } else {
                mBaseView.onStaffLoadFailure(response.msg);
            }
        }
    }


}
