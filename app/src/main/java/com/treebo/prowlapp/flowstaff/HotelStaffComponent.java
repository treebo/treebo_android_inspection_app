package com.treebo.prowlapp.flowstaff;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;

import dagger.Component;

/**
 * Created by abhisheknair on 25/05/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, HotelStaffModule.class})
public interface HotelStaffComponent {
    void injectHotelStaffActivity(HotelStaffActivity hotelStaffActivity);
}