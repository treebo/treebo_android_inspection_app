package com.treebo.prowlapp.flowstaff.fragment;

import android.app.ProgressDialog;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.adapter.SlidingTabAdapter;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.flowincentives.IncentivesActivity;
import com.treebo.prowlapp.flowstaff.HotelStaffActivity;
import com.treebo.prowlapp.flowstaff.adapter.StaffSectionedAdapter;
import com.treebo.prowlapp.flowstaff.presenter.HotelStaffPresenter;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.DepartmentWiseStaffModel;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.Models.StaffModel;
import com.treebo.prowlapp.mvpviews.HotelStaffView;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.MapperUtils;
import com.treebo.prowlapp.Utils.NetworkUtil;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.views.SlidingTabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by devesh on 02/05/16.
 */
public class StaffListFragment extends BaseFragment implements HotelStaffView, StaffSectionedAdapter.OnStaffItemCLickedListener, SlidingTabLayout.OnTabSelectedListener {
    RecyclerView mStaffList;
    HotelStaffPresenter presenter;
    ProgressDialog progress;
    StaffSectionedAdapter mAdapter;
    View mRootView;
    TextView addStaffButton;

    RolesAndUserPermissions.PermissionList mUserPermissions;

    @BindView(R.id.retry_view)
    RelativeLayout retryView;

    @BindView(R.id.retry_button)
    Button retryButton;

    @OnClick(R.id.retry_button)
    void onRetryButtonClicked() {
        getAllStaff();
    }

    @BindView(R.id.staff_empty_view)
    RelativeLayout relativeLayoutStaffEmptyView;

    @BindView(R.id.add_staff_button_empty)
    TextView staffAddButtonEmpty;

    List<DepartmentWiseStaffModel> mDepartmentWiseStaffModels;

    HashMap<Integer, Integer> mDepartmentStaffPositionHashMap = new HashMap<>();

    //Optmisation mechanism for Position to first Visible;
    HashMap<Integer, Integer> departmentPositionCache = new HashMap<>();

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private static final String HOTEL_DATA = "hotel_data";
    private HotelDataResponse mHotelData;


    public static StaffListFragment newInstance(HotelDataResponse hotelData,
                                                RolesAndUserPermissions.PermissionList permissions) {
        StaffListFragment fragment = new StaffListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(HOTEL_DATA, hotelData);
        bundle.putParcelable(LoginSharedPrefManager.PREF_USER_PERMISSIONS, permissions);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTAG("StaffListFragment");
        if (savedInstanceState == null) {
            mHotelData = getArguments().getParcelable(HOTEL_DATA);
            mUserPermissions = getArguments().getParcelable(LoginSharedPrefManager.PREF_USER_PERMISSIONS);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_list_staff, container, false);
        ButterKnife.bind(this, mRootView);


        mStaffList = (RecyclerView) mRootView.findViewById(R.id.section_subsection);
        mSlidingTabLayout = (SlidingTabLayout) mRootView.findViewById(R.id.section_tab_strip);
        mStaffList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_SETTLING || newState == RecyclerView.SCROLL_STATE_IDLE || newState == RecyclerView.SCROLL_STATE_DRAGGING) {


                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                int index = getSectionIndexForFirstVisibleItem(firstVisiblePosition);
                mViewPager.setCurrentItem(index);
            }
        });

        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setSelectedIndicatorColors(0xFFFFFFFF);

        mStaffList.setLayoutManager(new LinearLayoutManager(getContext()));
        initPresenter();
        mSlidingTabLayout.setTabTextColor(getResources().getColor(R.color.white));
        addStaffButton = (TextView) mRootView.findViewById(R.id.toolbar_add_new);
        addStaffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetworkUtil.isConnected(getActivity())) {
                    SnackbarUtils.show(mRootView, Constants.INTERNET_NOT_AVAILABLE);
                } else {
                    if (getActivity() instanceof HotelStaffActivity) {
                        ((HotelStaffActivity) getActivity()).addFragment(StaffPersonAddFragment.newInstance(mHotelData, mUserPermissions));
                    } else if (getActivity() instanceof IncentivesActivity) {
                        ((IncentivesActivity) getActivity()).addFragment(StaffPersonAddFragment.newInstance(mHotelData, mUserPermissions));
                    }

                }
            }
        });
        addStaffButton.setVisibility(mUserPermissions.isStaffDetailsAddNewEnabled() ? View.VISIBLE : View.GONE);


        if (mHotelData != null) {
            getAllStaff();
        } else {
            SnackbarUtils.show(mRootView, Constants.ERROR_UNKNOWN);
        }


        return mRootView;
    }

    private int getHeaderPositionForTab(int position) {
        return departmentPositionCache.get(position);
    }

    private void populateDepartmentSparseArray() {
        mDepartmentStaffPositionHashMap = MapperUtils.populateDepartmentStaffMap(mDepartmentWiseStaffModels);
        departmentPositionCache = MapperUtils.populateFirstVisiblePositionsMap(mDepartmentWiseStaffModels);

    }

    private int getSectionIndexForFirstVisibleItem(int firstVisiblePosition) {
        return mDepartmentStaffPositionHashMap.get(firstVisiblePosition);

    }

    private void getAllStaff() {
        presenter.getAllStaff(mHotelData.hotelId);
    }

    private void initPresenter() {
        presenter = new HotelStaffPresenter();
        presenter.setMvpView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }


    @Override
    public void onStaffLoaded(List<StaffModel> staffModelList) {
        mDepartmentWiseStaffModels = MapperUtils.getDepartmentWiseStaff(staffModelList);
        List<String> tabTitles = new ArrayList<>();
        for (DepartmentWiseStaffModel model : mDepartmentWiseStaffModels) {
            tabTitles.add(model.title);
        }
        populateDepartmentSparseArray();

        mViewPager = new ViewPager(getContext());
        mViewPager.setAdapter(new SlidingTabAdapter(getActivity(), tabTitles));
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnTabSelectedListener(this);

        if (mDepartmentWiseStaffModels.size() == 0) {
            mRootView.findViewById(R.id.staff_empty_view).setVisibility(View.VISIBLE);
            addStaffButton.setVisibility(View.GONE);
            GradientDrawable bgShape = (GradientDrawable) staffAddButtonEmpty.getBackground();
            bgShape.setGradientRadius(3);

        } else {
            mAdapter = new StaffSectionedAdapter(getActivity(), mDepartmentWiseStaffModels);
            mAdapter.setOnStaffItemCLickedListener(this);
            mStaffList.setAdapter(mAdapter);
        }
    }

    @Override
    public void onStaffLoadFailure(String msg) {

    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Please wait..");
        progress.setCancelable(false);
        if (!getActivity().isFinishing())
            progress.show();
    }

    @Override
    public void hideLoading() {
        if (progress != null && !getActivity().isFinishing()) {
            progress.dismiss();
        }
    }

    @Override
    public void showRetry() {
        retryView.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideRetry() {
        retryView.setVisibility(View.GONE);

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        return true;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(getActivity(), ((dialog, which) -> {
                    dialog.dismiss();
                    onRetryButtonClicked();
                }),
                (dialog1 -> {
                    dialog1.dismiss();
                }));

    }

    @Override
    public void onStaffItemClicked(StaffModel model) {
        ((HotelStaffActivity) getActivity()).addFragment(StaffPersonFragment.newInstance(model, mHotelData, mUserPermissions));
    }

    @OnClick(R.id.image_view_back)
    void goBack() {
        getActivity().finish();
    }

    @OnClick(R.id.add_staff_button_empty)
    void add() {
        if (!NetworkUtil.isConnected(getActivity())) {
            SnackbarUtils.show(mRootView, Constants.INTERNET_NOT_AVAILABLE);
        } else {
            ((HotelStaffActivity) getActivity()).addFragment(StaffPersonAddFragment.newInstance(mHotelData, mUserPermissions));
        }
    }

    @Override
    public void onTabSelected(int newPos) {
        int headerPosition = getHeaderPositionForTab(newPos);
        ((LinearLayoutManager) mStaffList.getLayoutManager()).scrollToPositionWithOffset(headerPosition, 0);
    }
}
