package com.treebo.prowlapp.flowstaff.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.events.LanguageChangeEvent;
import com.treebo.prowlapp.flowstaff.HotelStaffActivity;
import com.treebo.prowlapp.flowstaff.presenter.UpdateStaffPresenter;
import com.treebo.prowlapp.flowstaff.presenter.UserProfileEditPresenter;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.job.OfflineJobUtils;
import com.treebo.prowlapp.Models.DepartmentModel;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.Models.StaffModel;
import com.treebo.prowlapp.mvpviews.UpdateStaffView;
import com.treebo.prowlapp.mvpviews.UserProfileEditView;
import com.treebo.prowlapp.response.BankListResponse;
import com.treebo.prowlapp.response.DepartmentResponse;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.AmazonS3UploadTask;
import com.treebo.prowlapp.Utils.CompressImageUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.Utils.Logger;
import com.treebo.prowlapp.Utils.NetworkUtil;
import com.treebo.prowlapp.Utils.PermissionHelper;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.StringUtils;
import com.treebo.prowlapp.Utils.Utilities;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.core.content.FileProvider;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;
import rxAndroid.schedulers.AndroidSchedulers;

/**
 * Created by sakshamdhawan on 02/05/16.
 */
public class StaffPersonEditFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener,
        UserProfileEditView, SnapBottomSheetFragment.BottomSheetListener, UpdateStaffView,
        DepartmentsBottomSheetFragment.DepartmentSelectedListener, StatesBottomSheetFragment.StateSelectedListener {

    private static final int SELECT_PHOTO = 10101;

    ProgressDialog progress;

    public static final String HOTEL_DATA = "hotel_data";

    HotelDataResponse mHotelData;

    public static String STAFF_DATA = "staff_data";

    private StaffModel mStaffModel;
    private StaffModel mStaffModelEdited = new StaffModel();

    UserProfileEditPresenter mUserProfileEditPresenter;
    UpdateStaffPresenter mUpdateStaffPresenter;
    ViewGroup mRootView;

    String mFilePathToBeUploaded = "";


    private final String TAG_DATEPICKER_START_TIME_PERIOD = "StartTimePeriod";
    private final String TAG_DATEPICKER_END_TIME_PERIOD = "EndTimePeriod";
    private final String TAG_DATEPICKER_START_LEAVING_PERIOD = "StartLeavingPeriod";
    private final String TAG_DATEPICKER_END_LEAVING_PERIOD = "EndLeavingPeriod";
    private final String TAG_DATEPICKER_DATE_OF_BIRTH = "date_of_birth";

    Date dateRepresentationTenureStartPeriod, dateRepresentationLeaveStartTimePeriod;
    Date dateRepresentationTenureEndTmePeriod, dateRepresentationLeaveEndTimePeriod;
    Date formattedDateOfBirth;


    private List<DepartmentModel> mDepartmentModelList;
    private DepartmentModel mSelectedDepartment;
    private DepartmentsBottomSheetFragment mDepartmentsBottomSheetFragment;

    private String[] mStatesList;
    private StatesBottomSheetFragment mStatesBottomSheetFragment;

    @BindView(R.id.first_name)
    TextInputEditText first_name;

    @BindView(R.id.second_name)
    TextInputEditText last_name;

    @BindView(R.id.tv_selected_department)
    TextView mTvSelectedDepartment;

    @BindView(R.id.salary)
    TextInputEditText salary;

    @BindView(R.id.phone_number)
    TextInputEditText phone_number;

    @BindView(R.id.time_period_start)
    TextView time_period_start;

    @BindView(R.id.time_period_end)
    TextView time_period_end;

    @BindView(R.id.leaving_period_start)
    TextView leaving_period_start;

    @BindView(R.id.leaving_period_end)
    TextView leaving_period_end;

    @BindView(R.id.fathers_name)
    TextInputEditText fatherName;

    @BindView(R.id.staff_row_bullet)
    RelativeLayout addImageLayout;

    @BindView(R.id.date_of_birth_selector)
    TextView date_of_birth_selector;

    @BindView(R.id.tv_selected_state)
    TextView mTvSelectedState;

    @BindView(R.id.staff_row_photo)
    ImageView staffImage;

    @BindView(R.id.address_line1)
    TextInputEditText mAddressLine1;

    @BindView(R.id.address_line2)
    TextInputEditText mAddressLine2;

    @BindView(R.id.city_district)
    TextInputEditText mCityDistrict;

    @BindView(R.id.pincode)
    TextInputEditText mPinCode;

    @BindView(R.id.written_lang_layout)
    RelativeLayout writtenLangLayout;

    @BindView(R.id.spoken_lang_layout)
    RelativeLayout spokenLangLayout;

    @BindView(R.id.choose_sub_speak)
    TextView chooseSubSpoken;

    @BindView(R.id.choose_sub_read)
    TextView chooseSubRead;

    @BindView(R.id.bank_account_number_tv)
    TreeboEditText mBankAccountNumber;

    @BindView(R.id.ifsc_number_tv)
    TreeboEditText mBankIfscText;

    @BindView(R.id.bank_name_tv)
    TreeboTextView mBankNameText;

    @BindView(R.id.bank_branch_tv)
    TreeboEditText mBankBranchText;

    @BindView(R.id.bank_city_tv)
    TreeboEditText mBankCityText;

    //TODO: Remove thos when done.
    @BindView(R.id.delete_photograph)
    TextView deletePhoto;

    @BindView(R.id.bank_details_switch)
    SwitchCompat bankSwitch;


    OnLanguageChangeSubscriber onLanguageChangeSubscriber;

    private File mTempPickerDir = new File(Environment.getExternalStorageDirectory(), File.separator + Utils.PROWL_DIR + File.separator);

    private static final int PIC_PHOTO = 1;
    private BankListResponse.Bank mBankSelected;
    private RolesAndUserPermissions.PermissionList mUserPermissions;


    public static StaffPersonEditFragment newInstance(StaffModel staffModel,
                                                      HotelDataResponse hotelData,
                                                      RolesAndUserPermissions.PermissionList permissions) {
        StaffPersonEditFragment staffPersonEditFragment = new StaffPersonEditFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(LoginSharedPrefManager.PREF_USER_PERMISSIONS, permissions);
        bundle.putParcelable(STAFF_DATA, staffModel);
        bundle.putParcelable(HOTEL_DATA, hotelData);
        staffPersonEditFragment.setArguments(bundle);
        return staffPersonEditFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setTAG("StaffPersonEditFragment");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserPermissions = getArguments().getParcelable(LoginSharedPrefManager.PREF_USER_PERMISSIONS);
            mStaffModel = getArguments().getParcelable(STAFF_DATA);
            mHotelData = getArguments().getParcelable(HOTEL_DATA);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = (ViewGroup) inflater.inflate(R.layout.fragment_staff_person_edit, container, false);
        ButterKnife.bind(this, mRootView);
        setPreFilledValues();
        initPresenter();
        bindViews();

        if (!NetworkUtil.isConnected(getActivity())) {
            getActivity().onBackPressed();
            SnackbarUtils.show(mRootView, Constants.INTERNET_NOT_AVAILABLE);
        } else {
            mUserProfileEditPresenter.getAllDepartments(mHotelData.getHotelId());
        }
        return mRootView;
    }

    private void bindViews() {

        mStatesList = getContext().getResources().getStringArray(R.array.states);

        setLanguageText();

        //set state
        if (!TextUtils.isEmpty(mStaffModel.state)) {
            mTvSelectedState.setText(mStaffModel.state);
            mStaffModelEdited.setState(mStaffModel.state);
        } else {
            mTvSelectedState.setText(mStatesList[0]);
            mStaffModelEdited.setState(mStatesList[0]);
        }

        onLanguageChangeSubscriber = new OnLanguageChangeSubscriber();
        RxBus.getInstance().toObservable().subscribe(onLanguageChangeSubscriber);

        date_of_birth_selector.setOnTouchListener((v, event) -> {
            if (MotionEvent.ACTION_DOWN == event.getAction()) {
                clearRootFocus();
                showDateChooserDialog(TAG_DATEPICKER_DATE_OF_BIRTH);
            }
            return false;
        });

    }

    private void initPresenter() {
        mUserProfileEditPresenter = new UserProfileEditPresenter();
        mUserProfileEditPresenter.setMvpView(this);

        mUpdateStaffPresenter = new UpdateStaffPresenter();
        mUpdateStaffPresenter.setMvpView(this);
    }

    private void setPreFilledValues() {
        first_name.setText(mStaffModel.first_name);
        last_name.setText(mStaffModel.last_name);
        salary.setText(String.valueOf(mStaffModel.salary));
        phone_number.setText(mStaffModel.phone);
        fatherName.setText(mStaffModel.fathers_name);
        try {
            mStaffModelEdited = (StaffModel) mStaffModel.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }


        if (!TextUtils.isEmpty(mStaffModel.addressLine1)) {
            mAddressLine1.setText(mStaffModel.addressLine1);
            mStaffModelEdited.addressLine1 = mStaffModel.addressLine1;
        }

        if (!TextUtils.isEmpty(mStaffModel.addressLine2)) {
            mAddressLine2.setText(mStaffModel.addressLine2);
            mStaffModelEdited.addressLine2 = mStaffModel.addressLine2;

        }

        if (!TextUtils.isEmpty(mStaffModel.city)) {
            mCityDistrict.setText(mStaffModel.city);
            mStaffModelEdited.city = mStaffModel.city;

        }

        if (!TextUtils.isEmpty(mStaffModel.pinCode)) {
            mPinCode.setText(mStaffModel.pinCode);
            mStaffModelEdited.pinCode = mStaffModel.pinCode;

        }

        if (!TextUtils.isEmpty(mStaffModel.getStaffImageUrl())) {
            Utils.convertImageUrlToRoundedView(getActivity(), mStaffModel.staffImageUrl, staffImage);
            mStaffModelEdited.staffImageUrl = mStaffModel.staffImageUrl;
        }

        if (TextUtils.isEmpty(mStaffModel.dateOfBirth)) {
            date_of_birth_selector.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        } else {
            try {
                formattedDateOfBirth = new SimpleDateFormat("dd-MM-yyyy").parse(mStaffModel.dateOfBirth);
                date_of_birth_selector.setText(new SimpleDateFormat("dd-MMM-yyyy").format(formattedDateOfBirth));
                date_of_birth_selector.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                mStaffModelEdited.dateOfBirth = formattedDateOfBirth.toString();
            } catch (Exception e) {
                e.printStackTrace();
                date_of_birth_selector.setText(mStaffModel.dateOfBirth);
                date_of_birth_selector.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            }

        }

        if (mStaffModel.dateOfJoining == null || TextUtils.isEmpty(mStaffModel.dateOfJoining)) {
            time_period_start.setText(Constants.SELECT_DATE);
            time_period_start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        } else {

            try {
                dateRepresentationTenureStartPeriod = new SimpleDateFormat("dd-MM-yyyy").parse(mStaffModel.dateOfJoining);
                time_period_start.setText(new SimpleDateFormat("dd-MMM-yyyy").format(dateRepresentationTenureStartPeriod));
                time_period_start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                mStaffModelEdited.dateOfJoining = mStaffModel.dateOfJoining;
            } catch (Exception e) {
                e.printStackTrace();
                time_period_start.setText(mStaffModel.dateOfJoining);
                time_period_start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            }
        }


        if (mStaffModel.dateOfLeaving == null || TextUtils.isEmpty(mStaffModel.dateOfLeaving)) {
            time_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            time_period_end.setText(Constants.PRESENT);
        } else {

            try {
                dateRepresentationTenureEndTmePeriod = new SimpleDateFormat("dd-MM-yyyy").parse(mStaffModel.dateOfLeaving);
                time_period_end.setText(new SimpleDateFormat("dd-MMM-yyyy").format(dateRepresentationTenureEndTmePeriod));
                time_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                mStaffModelEdited.dateOfLeaving = mStaffModel.dateOfLeaving;
            } catch (Exception e) {
                time_period_end.setText(mStaffModel.dateOfLeaving);
                time_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                e.printStackTrace();
            }
        }

        if (mStaffModel.leaveStartDate == null || TextUtils.isEmpty(mStaffModel.leaveStartDate)) {
            leaving_period_start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            leaving_period_start.setText(Constants.SELECT_DATE);
        } else {

            try {
                dateRepresentationLeaveStartTimePeriod = new SimpleDateFormat("dd-MM-yyyy").parse(mStaffModel.leaveStartDate);
                leaving_period_start.setText(new SimpleDateFormat("dd-MMM-yyyy").format(dateRepresentationLeaveStartTimePeriod));
                leaving_period_start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                mStaffModelEdited.leaveStartDate = mStaffModel.leaveStartDate;

            } catch (Exception e) {
                e.printStackTrace();
                leaving_period_start.setText(mStaffModel.leaveStartDate);
                leaving_period_start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            }
        }

        if (mStaffModel.leaveEndDate == null || TextUtils.isEmpty(mStaffModel.leaveEndDate)) {
            leaving_period_end.setText(Constants.SELECT_DATE);
            leaving_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        } else {
            try {
                dateRepresentationLeaveEndTimePeriod = new SimpleDateFormat("dd-MM-yyyy").parse(mStaffModel.leaveEndDate);
                leaving_period_end.setText(new SimpleDateFormat("dd-MMM-yyyy").format(dateRepresentationLeaveEndTimePeriod));
                leaving_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                mStaffModelEdited.leaveEndDate = mStaffModel.leaveEndDate;
            } catch (Exception e) {
                e.printStackTrace();
                leaving_period_end.setText(mStaffModel.leaveEndDate);
                leaving_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            }
        }

        if (!TextUtils.isEmpty(mStaffModel.getAccountNumber())) {
            mBankAccountNumber.setText(mStaffModel.getAccountNumber());
        }
        if (!TextUtils.isEmpty(mStaffModel.getIfscCode())) {
            mBankIfscText.setText(mStaffModel.getIfscCode());
        }
        if (!TextUtils.isEmpty(mStaffModel.getBankName())) {
            mBankNameText.setText(mStaffModel.getBankName());
            ArrayList<BankListResponse.Bank> bankList = LoginSharedPrefManager.getInstance().getBankList();
            if (bankList.size() != 0) {
                for (BankListResponse.Bank bank : bankList) {
                    if (mStaffModel.getBankName().toLowerCase().equals(bank.getBankName().toLowerCase()))
                        mBankSelected = bank;
                }
            }

        }
        if (!TextUtils.isEmpty(mStaffModel.getBranch())) {
            mBankBranchText.setText(mStaffModel.getBranch());
        }
        if (!TextUtils.isEmpty(mStaffModel.getBankCity())) {
            mBankCityText.setText(mStaffModel.getBankCity());
        }

        addImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startImageUpload();

            }
        });

    }

    private void startImageUpload() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (PermissionHelper.hasNoPermissionToCamera(getActivity())) {
                SnackbarUtils.show(mRootView, getResources().getString(R.string.must_provide_camera));
                ArrayList<String> permissons = new ArrayList<>();
                permissons.add(Manifest.permission.CAMERA);
                if (PermissionHelper.hasNoPermissionToExternalStorage(getActivity())) {
                    permissons.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                    permissons.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
                String[] items = permissons.toArray(new String[permissons.size()]);
                this.requestPermissions(items, 1);
                return;
            }
        }
        showBottomSheet();
    }

    private void showBottomSheet() {
        SnapBottomSheetFragment bottomSheetFragment = SnapBottomSheetFragment.newInstance();
        bottomSheetFragment.show(getActivity().getSupportFragmentManager(), null);
        bottomSheetFragment.setBottomSheetListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PIC_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    if (!mFilePathToBeUploaded.isEmpty()) {
                        String filePath = mTempPickerDir + File.separator + mFilePathToBeUploaded;
                        File file = new File(filePath);
                        Observable.just(file)
                                .flatMap(filetoUpload -> Observable.just(CompressImageUtils.compressImage(filetoUpload)))
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(new Subscriber<File>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.d("StaffPersonEditFragment", "Failed to compress file.");
                                    }

                                    @Override
                                    public void onNext(File file) {
                                        uploadPicture(file);
                                        Log.d("StaffPersonEditFragment", "File compress successfully: %s" + file.getAbsolutePath());
                                    }
                                });
                    }
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Logger.d("IMAGE Canceled", "cancel");
                }
                break;
            case SELECT_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    final Uri imageUri = data.getData();
                    String filePath = Utils.getDataColumn(getActivity(), imageUri, null, null);
                    File file = new File(filePath);
                    Observable.just(file)
                            .flatMap(filetoUpload -> Observable.just(CompressImageUtils.compressImage(filetoUpload)))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new Subscriber<File>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.d("StaffPersonEditFragment", "Failed to compress file.");
                                }

                                @Override
                                public void onNext(File file) {
                                    uploadPicture(file);
                                    Log.d("StaffPersonEditFragment", "File compress successfully: %s" + file.getAbsolutePath());
                                }
                            });

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Logger.d("IMAGE Canceled", "cancel");
                }
                break;
        }
    }

    private void uploadPicture(File file) {
        String filePath = file.getPath();
        AmazonS3UploadTask amazonS3UploadTask = new AmazonS3UploadTask(getActivity(), filePath, uploadImageCallback);
        amazonS3UploadTask.execute();
    }

    AmazonS3UploadTask.CallBackData uploadImageCallback = new AmazonS3UploadTask.CallBackData() {

        @Override
        public void onSuccess(String cloudUrl) {
            attachImage(cloudUrl);
        }

        public void onError(Object result) {
            if (!getActivity().isFinishing())
                Utilities.cancelProgressDialog();
            SnackbarUtils.show(mRootView, getString(R.string.unable_to_attach_photo));
        }
    };

    private void attachImage(String cloudUrl) {
        Utils.convertImageUrlToRoundedView(getActivity(), cloudUrl, staffImage);
        mStaffModelEdited.setStaffImageUrl(cloudUrl);
    }


    @Override
    public void onPause() {
        super.onPause();
        Utils.hideSoftKeyboard(mRootView);
    }

    @OnClick(R.id.written_lang_layout)
    void onClickWrittenLangs() {
        ((HotelStaffActivity) getActivity()).addFragment(LanguageListFragment.newInstance(mStaffModel, "read"));
    }

    @OnClick(R.id.spoken_lang_layout)
    void onClickSpokenLangs() {
        ((HotelStaffActivity) getActivity()).addFragment(LanguageListFragment.newInstance(mStaffModel, "speak"));
    }

    @OnClick(R.id.time_period_start)
    void onClickStart() {
        SnackbarUtils.show(mRootView, Constants.ERROR_MESSAGE_CANNOT_EDIT_JOINING_DATE);
    }

    @OnClick(R.id.time_period_end)
    void onClickEnd() {
        if (mUserPermissions.isStaffDetailsDeleteEnabled()) {
            clearRootFocus();
            showDateChooserDialog(TAG_DATEPICKER_END_TIME_PERIOD);
        }
    }

    @OnClick(R.id.leaving_period_start)
    void onClickStartLeaving() {
        clearRootFocus();
        showDateChooserDialog(TAG_DATEPICKER_START_LEAVING_PERIOD);
    }


    @OnClick(R.id.leaving_period_end)
    void onClickEndLeaving() {
        clearRootFocus();
        showDateChooserDialog(TAG_DATEPICKER_END_LEAVING_PERIOD);
    }


    @OnClick(R.id.tv_department_label)
    void onClickDepartmentLabel() {
        clearRootFocus();
        showDepartmentsBottomSheet();
    }

    @OnClick(R.id.tv_selected_department)
    void onClickSelectedDepartment() {
        clearRootFocus();
        showDepartmentsBottomSheet();
    }

    @OnClick(R.id.tv_state_label)
    void onClickStateLabel() {
        clearRootFocus();
        showStatesBottomSheet();
    }

    @OnClick(R.id.tv_selected_state)
    void onClickSelectedState() {
        clearRootFocus();
        showStatesBottomSheet();
    }

    private void showDepartmentsBottomSheet() {
        mDepartmentsBottomSheetFragment = DepartmentsBottomSheetFragment.newInstance(mDepartmentModelList);
        mDepartmentsBottomSheetFragment.setDepartmentSelectedListener(this);
        mDepartmentsBottomSheetFragment.show(getChildFragmentManager(), null);
    }

    private void showStatesBottomSheet() {
        mStatesBottomSheetFragment = StatesBottomSheetFragment.newInstance(mStatesList);
        mStatesBottomSheetFragment.setStateSelectedListener(this);
        mStatesBottomSheetFragment.show(getChildFragmentManager(), null);
    }


    void showDateChooserDialog(String tag) {

        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                StaffPersonEditFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH),
                Constants.CLEAR
        );
        // todo: if you re-import the library, remember to change cancelText to clear

        dpd.setAccentColor(ContextCompat.getColor(getContext(), R.color.emerald));

        dpd.setOnCancelListener(dialogInterface -> {

            switch (tag) {


                case TAG_DATEPICKER_END_TIME_PERIOD:
                    dateRepresentationTenureEndTmePeriod = null;
                    time_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                    time_period_end.setText(Constants.PRESENT);
                    break;
                case TAG_DATEPICKER_START_LEAVING_PERIOD:
                    dateRepresentationLeaveStartTimePeriod = null;
                    leaving_period_start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                    leaving_period_start.setText(Constants.SELECT_DATE);
                    break;
                case TAG_DATEPICKER_END_LEAVING_PERIOD:
                    dateRepresentationLeaveEndTimePeriod = null;
                    leaving_period_end.setText(Constants.SELECT_DATE);
                    leaving_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                    break;

                case TAG_DATEPICKER_DATE_OF_BIRTH:
                    formattedDateOfBirth = null;
                    date_of_birth_selector.setText("");
                    date_of_birth_selector.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                    clearRootFocus();
                    break;
            }
        });
        dpd.show(getActivity().getFragmentManager(), tag);
    }

    private Uri getPhotoFileUri() {
        mFilePathToBeUploaded = "staff_image_" + mStaffModel.first_name + "_" + mStaffModel.last_name + new SimpleDateFormat("yyyyMMddhhmmss'.jpg'").format(new Date());
        mFilePathToBeUploaded = mFilePathToBeUploaded.replaceAll(" ", "_").toLowerCase();
        mTempPickerDir.mkdirs();
        File issueFile = new File(mTempPickerDir, mFilePathToBeUploaded);
        Uri outputUri = FileProvider.getUriForFile(
                getActivity(),
                getActivity().getApplicationContext()
                        .getPackageName() + ".provider", issueFile);
        return outputUri;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        Date dateRepresentation = cal.getTime();
        if (view.getTag().equals(TAG_DATEPICKER_START_TIME_PERIOD)) {
            time_period_start.setText(new SimpleDateFormat("dd-MMM-yyyy").format(dateRepresentation));
            time_period_start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            dateRepresentationTenureStartPeriod = cal.getTime();
            return;
        }
        if (view.getTag().equals(TAG_DATEPICKER_END_TIME_PERIOD)) {
            time_period_end.setText(new SimpleDateFormat("dd-MMM-yyyy").format(dateRepresentation));
            time_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            dateRepresentationTenureEndTmePeriod = cal.getTime();
            return;
        }
        if (view.getTag().equals(TAG_DATEPICKER_START_LEAVING_PERIOD)) {
            leaving_period_start.setText(new SimpleDateFormat("dd-MMM-yyyy").format(dateRepresentation));
            leaving_period_start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            dateRepresentationLeaveStartTimePeriod = cal.getTime();
            return;
        }
        if (view.getTag().equals(TAG_DATEPICKER_END_LEAVING_PERIOD)) {
            leaving_period_end.setText(new SimpleDateFormat("dd-MMM-yyyy").format(dateRepresentation));
            leaving_period_end.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            dateRepresentationLeaveEndTimePeriod = cal.getTime();
            return;
        }
        if (view.getTag().equals(TAG_DATEPICKER_DATE_OF_BIRTH)) {
            date_of_birth_selector.setText(new SimpleDateFormat("dd-MMM-yyyy").format(dateRepresentation));
            date_of_birth_selector.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            date_of_birth_selector.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            if (!DateUtils.isDateValid(cal.getTime())) {
                showError(Constants.INVALID_DATE_OF_BIRTH);
                return;
            }
            formattedDateOfBirth = cal.getTime();
            clearRootFocus();

            return;
        }


    }

    @OnClick(R.id.confirm_button)
    void send_data() {
        Utils.hideSoftKeyboard(mRootView);

        if (!NetworkUtil.isConnected(getActivity())) {
            AlertDialogUtils.showInternetDisconnectedDialog(getActivity(),
                    (dialog, which) -> {
                        dialog.dismiss();
                        checkFormValidityAndUpdateStaff();
                        return;
                    }, (dialog) -> {
                        dialog.dismiss();
                        return;
                    }
            );
            return;
        }

        checkFormValidityAndUpdateStaff();

    }

    @OnClick(R.id.toolbar_cancel)
    public void cancelClicked() {
        backClicked();
    }


    private void checkFormValidityAndUpdateStaff() {

        if (!NetworkUtil.isConnected(getActivity())) {
            SnackbarUtils.show(mRootView, Constants.INTERNET_NOT_AVAILABLE);
            return;
        }

        if (TextUtils.isEmpty(first_name.getText().toString())) {

            showError(Constants.ERROR_MESSAGE_FIRST_NAME_EMPTY);
            return;
        }
        if (TextUtils.isEmpty(last_name.getText().toString())) {

            showError(Constants.ERROR_MESSAGE_LAST_NAME_EMPTY);
            return;
        }
        if (TextUtils.isEmpty(salary.getText().toString())) {
            showError(Constants.ERROR_MESSAGE_SALARY_MISSING);
            return;
        }
        if (Integer.parseInt(salary.getText().toString()) == 0) {
            showError(Constants.ERROR_MESSAGE_SALARY_ZERO);
            return;
        }

        if (TextUtils.isEmpty(phone_number.getText().toString())) {
            showError(Constants.ERROR_MESSAGE_PHONE_NUMBER_EMPTY);
            return;
        }
        if (!StringUtils.validatePhoneNumber(phone_number.getText().toString())) {
            showError(Constants.ERROR_MESSAGE_PHONE_NUMBER_INVALID);
            return;
        }


        if (TextUtils.isEmpty(mAddressLine1.getText().toString())) {
            showError(Constants.ERROR_MESSAGE_ADDRESS1_EMPTY);
            return;
        }
        if (TextUtils.isEmpty(mCityDistrict.getText().toString())) {
            showError(Constants.ERROR_MESSAGE_CITY_DISTRICT_EMPTY);
            return;
        }

        if (TextUtils.isEmpty(mPinCode.getText().toString())) {
            showError(Constants.ERROR_MESSAGE_PINCODE_EMPTY);
            return;
        }

        if (!StringUtils.validatePinCode(mPinCode.getText().toString())) {
            showError(Constants.ERROR_MESSAGE_PINCODE_INVALID);
            return;
        }

        if (TextUtils.isEmpty(mStaffModelEdited.state)) {
            showError(Constants.ERROR_MESSAGE_STATE_EMPTY);
            return;
        }
        if (TextUtils.isEmpty(date_of_birth_selector.getText().toString())) {
            showError(Constants.ERROR_MESSAGE_DATE_OF_BIRTH_EMPTY);
            return;
        }

        mStaffModelEdited.salary = Integer.parseInt(salary.getText().toString());
        mStaffModelEdited.phone = phone_number.getText().toString();
        mStaffModelEdited.first_name = first_name.getText().toString();
        mStaffModelEdited.last_name = last_name.getText().toString();
        mStaffModelEdited.phone = phone_number.getText().toString();
        mStaffModelEdited.fathers_name = fatherName.getText().toString().trim();
        mStaffModelEdited.addressLine1 = mAddressLine1.getText().toString().trim();
        mStaffModelEdited.addressLine2 = mAddressLine2.getText().toString().trim();
        mStaffModelEdited.city = mCityDistrict.getText().toString().trim();
        mStaffModelEdited.pinCode = mPinCode.getText().toString().trim();

        if (dateRepresentationTenureStartPeriod != null && dateRepresentationTenureEndTmePeriod != null) {
            if (dateRepresentationTenureStartPeriod.after(dateRepresentationTenureEndTmePeriod)) {
                showError(Constants.ERROR_MESSAGE_TENURE_END_DATE_GREATER);
                return;
            }
        }

        if (dateRepresentationLeaveStartTimePeriod == null && dateRepresentationLeaveEndTimePeriod != null) {
            showError(Constants.ERROR_MESSAGE_LEAVE_START_NOT_FILLED);
            return;
        }

        if (dateRepresentationLeaveStartTimePeriod != null && dateRepresentationLeaveEndTimePeriod == null) {
            showError(Constants.ERROR_MESSAGE_LEAVE_END_NOT_FILLED);
            return;
        }

        if (dateRepresentationLeaveStartTimePeriod != null && dateRepresentationLeaveEndTimePeriod != null) {
            if (dateRepresentationLeaveStartTimePeriod.after(dateRepresentationLeaveEndTimePeriod)) {
                showError(Constants.ERROR_MESSAGE_LEAVE_END_DATE_GREATER);
                return;
            }

            if (!mStaffModel.leaveStartDate.isEmpty() && !mStaffModel.leaveEndDate.isEmpty()) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
                try {
                    if (dateRepresentationLeaveEndTimePeriod.after(dateFormat.parse(mStaffModel.leaveEndDate))
                            && dateRepresentationLeaveStartTimePeriod.after(dateFormat.parse(mStaffModel.leaveEndDate))) {
                        showError(Constants.ERROR_MESSAGE_ALREADY_ON_LEAVE);
                        return;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        if (dateRepresentationTenureEndTmePeriod != null && dateRepresentationLeaveEndTimePeriod != null) {
            if (dateRepresentationLeaveEndTimePeriod.after(dateRepresentationTenureEndTmePeriod)) {
                showError(Constants.ERROR_MESSAGE_LEAVE_END_DATE_GREATER_TENURE);
                return;
            }
        }

        if (dateRepresentationTenureStartPeriod != null && dateRepresentationLeaveStartTimePeriod != null) {
            if (dateRepresentationLeaveStartTimePeriod.before(dateRepresentationTenureStartPeriod)) {
                showError(Constants.ERROR_MESSAGE_LEAVE_END_DATE_GREATER_TENURE);
                return;
            }
        }
        if (!DateUtils.isDateValid(formattedDateOfBirth)) {
            showError(Constants.INVALID_DATE_OF_BIRTH);
            return;
        }

        if (bankSwitch.isChecked()) {
            if (TextUtils.isEmpty(mBankAccountNumber.getText().toString())
                    && TextUtils.isEmpty(mBankIfscText.getText().toString())
                    && TextUtils.isEmpty(mBankBranchText.getText().toString())
                    && TextUtils.isEmpty(mBankCityText.getText().toString())
                    && TextUtils.isEmpty(mBankNameText.getText().toString())) {
                showError(getString(R.string.bank_details_error));
                return;
            }
        } else {
            updateCurrentStaff();
            return;
        }

        if (TextUtils.isEmpty(mBankAccountNumber.getText().toString()) ||
                (mBankSelected != null && !StringUtils.validateBankAccountNumber(mBankSelected.getValidator(),
                        mBankAccountNumber.getText().toString()))) {
            showError(getString(R.string.generic_bank_details_error, getString(R.string.bank_account_number)));
            mBankAccountNumber.setError(getString(R.string.details_empty));
            return;
        }

        if (TextUtils.isEmpty(mBankAccountNumber.getText().toString()) ||
                (mBankSelected != null && !StringUtils.validateBankAccountNumber(mBankSelected.getValidator(),
                        mBankAccountNumber.getText().toString()))) {
            showError(getString(R.string.generic_bank_details_error, getString(R.string.bank_account_number)));
            mBankAccountNumber.setError(getString(R.string.details_empty));
            mBankAccountNumber.requestFocus();
            return;
        }

        if (!StringUtils.validateIfscCode(mBankIfscText.getText().toString())) {
            showError(getString(R.string.generic_bank_details_error, getString(R.string.bank_ifsc_code)));
            mBankIfscText.setError(getString(R.string.details_empty));
            mBankIfscText.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(mBankBranchText.getText().toString())) {
            showError(getString(R.string.generic_bank_details_error, getString(R.string.bank_branch)));
            mBankBranchText.setError(getString(R.string.details_empty));
            mBankBranchText.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(mBankNameText.getText().toString())) {
            showError(getString(R.string.generic_bank_details_error, getString(R.string.bank_name)));
            mBankNameText.setError(getString(R.string.details_empty));
            mBankNameText.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(mBankCityText.getText().toString())) {
            showError(getString(R.string.generic_bank_details_error, getString(R.string.bank_city)));
            mBankCityText.setError(getString(R.string.details_empty));
            mBankCityText.requestFocus();
        }

        updateCurrentStaff();

    }

    private void updateCurrentStaff() {
        if (!NetworkUtil.isConnected(getActivity())) {
            SnackbarUtils.show(mRootView, Constants.INTERNET_NOT_AVAILABLE);
            return;
        }

        try {
            mUpdateStaffPresenter.updateStaff(
                    mStaffModel.id,
                    mStaffModelEdited.first_name,
                    mStaffModelEdited.last_name,
                    mStaffModelEdited.phone,
                    mStaffModelEdited.fathers_name,
                    mStaffModelEdited.staffImageUrl,
                    mStaffModelEdited.addressLine1,
                    mStaffModelEdited.addressLine2,
                    mStaffModelEdited.state,
                    mStaffModelEdited.city,
                    mStaffModelEdited.pinCode,
                    mSelectedDepartment.id,
                    mHotelData.hotelId,
                    mStaffModelEdited.salary,
                    dateRepresentationTenureStartPeriod,
                    dateRepresentationTenureEndTmePeriod,
                    dateRepresentationLeaveStartTimePeriod,
                    dateRepresentationLeaveEndTimePeriod,
                    formattedDateOfBirth,
                    mStaffModelEdited.languagePrefRead,
                    mStaffModelEdited.languagePrefSpeak,
                    bankSwitch.isChecked(),
                    mBankAccountNumber.getText().toString(),
                    mBankIfscText.getText().toString(),
                    mBankNameText.getText().toString(),
                    mBankBranchText.getText().toString(),
                    mBankCityText.getText().toString()
            );
        } catch (Exception e) {
            SnackbarUtils.show(mRootView, "Something went wrong.");
        }
    }


    @Override
    public void onDepartmentsFetched(DepartmentResponse response) {
        mDepartmentModelList = response.data.departments;
        getSelectedDepartment();

    }

    private void getSelectedDepartment() {
        for (int i = 0, size = mDepartmentModelList.size(); i < size; i++) {
            if (mDepartmentModelList.get(i).name.equals(mStaffModel.departmentName)) {
                mSelectedDepartment = mDepartmentModelList.get(i);
                break;
            }
        }
        mTvSelectedDepartment.setText(mStaffModel.departmentName);
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Please wait...");
        progress.setCancelable(false);
        if (getActivity() != null && !getActivity().isFinishing())
            progress.show();
    }

    @Override
    public void hideLoading() {
        if (progress != null && getActivity() != null && !getActivity().isFinishing())
            progress.dismiss();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        return true;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(getActivity(), ((dialog, which) -> {
                    dialog.dismiss();
                    onRetryButtonClicked();
                }),
                dialog1 -> {
                    dialog1.dismiss();
                });

    }

    @OnClick(R.id.retry_button)
    void onRetryButtonClicked() {
        updateCurrentStaff();

    }

    @OnCheckedChanged(R.id.bank_details_switch)
    void onSwitchToggled() {
        mBankAccountNumber.setEnabled(bankSwitch.isChecked());
        mBankNameText.setEnabled(bankSwitch.isChecked());
        mBankIfscText.setEnabled(bankSwitch.isChecked());
        mBankCityText.setEnabled(bankSwitch.isChecked());
        mBankBranchText.setEnabled(bankSwitch.isChecked());
    }


    @OnClick(R.id.image_view_back)
    void backClicked() {
        if (!mStaffModel.equals(mStaffModelEdited)) {
            AlertDialogUtils.showConfirmDialog(getActivity(), "Edit Profile", Constants.DISCARD_CHANGES,
                    (dialog, which) ->
                            getActivity().onBackPressed());
        } else {
            getActivity().onBackPressed();
        }
    }

    @OnClick(R.id.bank_name_tv)
    void bankNameClicked() {
        ArrayList<BankListResponse.Bank> bankList = LoginSharedPrefManager.getInstance().getBankList();
        final ArrayList<String> bankNames;
        if (bankList.size() == 0) {
            OfflineJobUtils.fetchBankList();
            bankNames = new ArrayList<>(Arrays.asList(getContext().getResources().getStringArray(R.array.states)));
        } else {
            bankNames = new ArrayList<>();
            for (BankListResponse.Bank bank : bankList) {
                bankNames.add(bank.getBankName());
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.select_bank)
                .setItems(bankNames.toArray(new String[0]), (dialog, selectedIndex) -> {
                    String bankName = bankNames.get(selectedIndex);
                    mBankSelected = bankList.size() > 0 ? bankList.get(selectedIndex) : new BankListResponse.Bank();
                    mBankNameText.setText(bankName);
                    dialog.dismiss();
                });
        builder.create().show();
    }


    @Override
    public void onUpdateSuccess() {
        SnackbarUtils.show(mRootView, "Successfully updated.");
        ((HotelStaffActivity) getActivity()).clearAllFragments();
        ((HotelStaffActivity) getActivity()).addFragment(StaffListFragment.newInstance(mHotelData, mUserPermissions));
    }

    @Override
    public void onBottomSheetSelected(@SnapBottomSheetFragment.PictureState int pictureState) {
        switch (pictureState) {
            case SnapBottomSheetFragment.TAKE_PICTURE:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri());
                startActivityForResult(intent, PIC_PHOTO);
                break;
            case SnapBottomSheetFragment.CHOOSE_PICTURE:
                pickPhotoFromGallery();
                break;
            case SnapBottomSheetFragment.DELETE_PICTURE:
                staffImage.setImageDrawable(getResources().getDrawable(R.drawable.audit_categories_bullet));
                mStaffModelEdited.setStaffImageUrl("");
                break;

        }
    }

    @Override
    public void onDepartmentSelected(int position) {
        mSelectedDepartment = mDepartmentModelList.get(position);
        mTvSelectedDepartment.setText(mSelectedDepartment.name);
        if (mDepartmentsBottomSheetFragment != null) {
            mDepartmentsBottomSheetFragment.dismiss();
            mDepartmentsBottomSheetFragment = null;
        }
    }

    @Override
    public void onStateSelected(int position) {
        mStaffModelEdited.setState(mStatesList[position]);
        mTvSelectedState.setText(mStaffModelEdited.getState());
        if (mStatesBottomSheetFragment != null) {
            mStatesBottomSheetFragment.dismiss();
            mStatesBottomSheetFragment = null;
        }
    }


    private void pickPhotoFromGallery() {
        choosePhotoFromGallery();
    }

    public void choosePhotoFromGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public class OnLanguageChangeSubscriber extends Subscriber<Object> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(Object event) {
            if (event instanceof LanguageChangeEvent) {
                setLanguageText();
                mRootView.requestLayout();
            }
        }
    }

    @Override
    public void onUpdateFailed() {
        SnackbarUtils.show(mRootView, "Something went wrong.");
    }


    public void clearRootFocus() {
        if (mRootView != null) {
            mRootView.clearFocus();
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null) {
            setLanguageText();
        }
    }

    private void setLanguageText() {
        String readText = StringUtils.convertToCSV(mStaffModel.getLanguagePrefRead());
        if (TextUtils.isEmpty(readText)) {
            chooseSubRead.setText(getResources().getString(R.string.choose_langs));
            chooseSubRead.setTextColor(getActivity().getResources().getColor(R.color.warm_grey));

        } else {
            chooseSubRead.setText(readText);
            chooseSubRead.setTextColor(getActivity().getResources().getColor(R.color.emerald));
        }

        String speakText = StringUtils.convertToCSV(mStaffModel.getLanguagePrefSpeak());
        if (TextUtils.isEmpty(speakText)) {
            chooseSubSpoken.setText(getResources().getString(R.string.choose_langs));
            chooseSubSpoken.setTextColor(getActivity().getResources().getColor(R.color.warm_grey));
        } else {
            chooseSubSpoken.setText(speakText);
            chooseSubSpoken.setTextColor(getActivity().getResources().getColor(R.color.emerald));
        }

    }

    @Deprecated
    @OnClick(R.id.delete_photograph)
    void deleteClicked() {
        AlertDialogUtils.showConfirmDialog(getActivity(), "Delete Photo", "Are you sure you want to delete this photo?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                staffImage.setImageDrawable(getResources().getDrawable(R.drawable.audit_categories_bullet));
                deletePhoto.setVisibility(View.GONE);
                mStaffModelEdited.setStaffImageUrl("");
            }
        });

    }
}


