package com.treebo.prowlapp.flowstaff.fragment;

import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.events.LanguageChangeEvent;
import com.treebo.prowlapp.flowstaff.adapter.LanguagesListAdapter;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.StaffModel;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by devesh on 27/05/16.
 */
public class LanguageListFragment extends BaseFragment implements LanguagesListAdapter.OnLanguageSelectedListener {

    private static final String LANGUAGE_DATA = "language_data";
    private static final String STAFF_MODEL_EXTRA = "staff_model";
    private static final String TYPE = "type";

    private ArrayList<String> selectedLanguages = new ArrayList<>();

    private ArrayList<String> earlierSelectedLanguages = new ArrayList<>();

    private ViewGroup mRootView;

    private RecyclerView mListView;

    private LanguagesListAdapter mLanguagesAdapter;

    private ArrayList<String> allLanguages;

    private StaffModel staffModel;

    @BindView(R.id.image_view_back)
    public ImageView backButton;

    @BindView(R.id.toolbar_title)
    public TextView mTitle;

    @BindView(R.id.num_selected)
    public TextView selectedNums;

    String mType;

    @BindView(R.id.done)
    public Button doneButton;

    private RxBus rxBus;


    public  LanguageListFragment(){

    }
    public static LanguageListFragment newInstance(StaffModel staffModel, String type){
        LanguageListFragment fragment = new LanguageListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TYPE, type);
        bundle.putParcelable(STAFF_MODEL_EXTRA, staffModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            staffModel = getArguments().getParcelable(STAFF_MODEL_EXTRA);
            mType = getArguments().getString(TYPE);
        }
        rxBus = RxBus.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = (ViewGroup) inflater.inflate(R.layout.language_list_layout, container , false);
        ButterKnife.bind(this, mRootView);
        mListView = (RecyclerView)mRootView.findViewById(R.id.list_view);
        mListView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mListView.setHasFixedSize(true);
        mTitle.setText("Languages");
        populateAllLanguages();
        if(mType.equalsIgnoreCase("read")) {
            earlierSelectedLanguages = staffModel.getLanguagePrefRead();
            mTitle.setText("Languages you Read");

        }
        else if(mType.equalsIgnoreCase("speak")){
            mTitle.setText("Languages you Speak");
            earlierSelectedLanguages = staffModel.getLanguagePrefSpeak();
        }

        if(earlierSelectedLanguages.size() == 1 && TextUtils.isEmpty(earlierSelectedLanguages.get(0).trim())){
            earlierSelectedLanguages = new ArrayList<>();
        }

        selectedLanguages = Utils.createDeepCopy(earlierSelectedLanguages);

       // populateDummyLangs();

        mLanguagesAdapter = new LanguagesListAdapter(allLanguages, selectedLanguages,mType);
        mLanguagesAdapter.setOnLangSelectedListener(this);
        selectedNums.setText(selectedLanguages.size() + " selected");
        mListView.setAdapter(mLanguagesAdapter);
        return mRootView;
    }

    private void populateDummyLangs() {
        selectedLanguages = new ArrayList<>();
        selectedLanguages.add("English");
        selectedLanguages.add("Hindi");
        selectedLanguages.add("Punjabi");
    }

    private void populateAllLanguages() {
        if(allLanguages != null){
            allLanguages.clear();
        }
        allLanguages = new ArrayList<>();
        allLanguages.addAll(Arrays.asList(getActivity().getResources().getStringArray(R.array.languages)));
    }

    @OnClick(R.id.done)
    public void onDoneClicked(){
        earlierSelectedLanguages = Utils.createDeepCopy(selectedLanguages);
        setLanguages(earlierSelectedLanguages, mType);
        getActivity().onBackPressed();
    }

    @OnClick(R.id.image_view_back)
    public void onBackClicked(){
        setLanguages(earlierSelectedLanguages, mType);
        getActivity().onBackPressed();
    }


    @Override
    public void onLanguageSelected(ArrayList<String> selectedLangs, String type) {
        selectedNums.setText(selectedLangs.size() + " selected");
        selectedLanguages = mLanguagesAdapter.getSelectedLanguages();
    }

    private void setLanguages(ArrayList<String> selectedLangs, String type){
        if(type.equalsIgnoreCase("read")) {
            staffModel.setLanguagePrefRead(selectedLangs);
            rxBus.postEvent(new LanguageChangeEvent(selectedLangs ,type));
        }
        else if(type.equalsIgnoreCase("speak")) {
            staffModel.setLanguagePrefSpeak(selectedLangs);
            rxBus.postEvent(new LanguageChangeEvent(selectedLangs ,type));
        }
    }
}
