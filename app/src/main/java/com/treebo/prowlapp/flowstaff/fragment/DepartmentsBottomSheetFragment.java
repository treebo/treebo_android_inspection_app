package com.treebo.prowlapp.flowstaff.fragment;



import android.app.Dialog;
import android.os.Bundle;

import android.view.View;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowstaff.adapter.DepartmentsBottomSheetAdapter;
import com.treebo.prowlapp.Models.DepartmentModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by devesh on 23/06/16.
 */
public class DepartmentsBottomSheetFragment extends BottomSheetDialogFragment {

    private DepartmentSelectedListener mDepartmentSelectedListener;

    private static final String KEY_DEPARTMENT_LIST = "DEPARTMENT_LIST";

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    public static DepartmentsBottomSheetFragment newInstance(List<DepartmentModel> departmentModelList) {
        DepartmentsBottomSheetFragment fragment = new DepartmentsBottomSheetFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_DEPARTMENT_LIST, (ArrayList) departmentModelList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View rootView = View.inflate(getContext(), R.layout.bottom_sheet_department_list, null);

        dialog.setContentView(rootView);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) rootView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        setDepartmentsList(rootView);
    }

    private void setDepartmentsList(View rootView) {

        RecyclerView rvDepartmentList = (RecyclerView) rootView.findViewById(R.id.rv_department_list);

        if (rvDepartmentList != null) {
            rvDepartmentList.setLayoutManager(new LinearLayoutManager(getContext()));

            rvDepartmentList.setAdapter(new DepartmentsBottomSheetAdapter(
                    getArguments().getParcelableArrayList(KEY_DEPARTMENT_LIST),
                    mDepartmentSelectedListener)
            );
        }

    }

    public void setDepartmentSelectedListener(DepartmentSelectedListener departmentSelectedListener) {
        mDepartmentSelectedListener = departmentSelectedListener;
    }

    public interface DepartmentSelectedListener {
        void onDepartmentSelected(int position);
    }
}
