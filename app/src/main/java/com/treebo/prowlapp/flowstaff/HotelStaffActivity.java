package com.treebo.prowlapp.flowstaff;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowstaff.fragment.StaffListFragment;
import com.treebo.prowlapp.flowstaff.fragment.StaffPersonAddFragment;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.Utils;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

/**
 * Created by devesh on 30/04/16.
 */
public class HotelStaffActivity extends AppCompatActivity {

    @Inject
    public LoginSharedPrefManager mSharedPrefManager;

    public static final String HOTEL_DATA = "hotel_data";
    private static final String TAG = HotelStaffActivity.class.getSimpleName();
    public static String TITLE_STAFF = "Staff";
    private HotelDataResponse mHotelData;
    RelativeLayout mRoot;

    public boolean isFromIncentives;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, HotelStaffActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HotelStaffComponent portfolioComponent = DaggerHotelStaffComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        portfolioComponent.injectHotelStaffActivity(this);
        if (savedInstanceState == null) {
            if (getIntent() != null && getIntent().getExtras() != null) {
                mHotelData = getIntent().getExtras().getParcelable(HOTEL_DATA);
            }
        }
        isFromIncentives = getIntent().getBooleanExtra(Utils.IS_FROM_INCENTIVES, false);

        setContentView(R.layout.activity_staff);
        mRoot = (RelativeLayout) findViewById(R.id.root);
        if (mHotelData == null) {
            showError("No Hotel found.");
            this.finish();
        } else {
            RolesAndUserPermissions.PermissionList userPermissions
                    = mSharedPrefManager.getUserPermissionList(mHotelData.getHotelId());

            if (isFromIncentives) {
                addFragment(StaffPersonAddFragment.newInstance(mHotelData, userPermissions,
                        getIntent().getStringExtra(Utils.INCENTIVES_DEPARTMENT)));
            } else {
                addFragment(StaffListFragment.newInstance(mHotelData, userPermissions));
            }
        }

    }


    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRoot, errorMessage);
        return false;
    }

    public void clearAllFragments() {
        FragmentManager fm = this.getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public void addFragment(BaseFragment fragment) {
        Log.v(TAG, "addFragment called ");
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }


}
