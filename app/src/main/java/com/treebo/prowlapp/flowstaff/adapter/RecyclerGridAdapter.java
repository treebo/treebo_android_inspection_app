package com.treebo.prowlapp.flowstaff.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.StaffModel;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by devesh on 06/04/16.
 */
public class RecyclerGridAdapter extends RecyclerView.Adapter<RecyclerGridAdapter.GridViewHolder> {
    List<StaffModel> userItemList;

    public RecyclerGridAdapter(List<StaffModel> userItems) {
        this.userItemList = userItems;
    }


    @Override
    public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hotel_staff, parent, false);
        return new GridViewHolder(rootView);
    }

    @Override
    public int getItemCount() {
        return userItemList.size();
    }

    @Override
    public void onBindViewHolder(GridViewHolder holder, int position) {
        holder.nameTextView.setText(userItemList.get(position).getName());
        holder.numberTextView.setText(userItemList.get(position).getPhone());
    }

    class GridViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView ;
        TextView numberTextView ;

        public GridViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.user_name);
            numberTextView = (TextView) itemView.findViewById(R.id.user_number);
        }
    }
}
