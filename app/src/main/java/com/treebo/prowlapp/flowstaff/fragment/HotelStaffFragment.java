package com.treebo.prowlapp.flowstaff.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.flowstaff.adapter.RecyclerGridAdapter;
import com.treebo.prowlapp.flowstaff.presenter.HotelStaffPresenter;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.Models.StaffModel;
import com.treebo.prowlapp.mvpviews.HotelStaffView;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

/**
 * Created by devesh on 05/04/16.
 */
public class HotelStaffFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener,HotelStaffView {

    private static final String HOTEL_STAFF_KEY = "hotel_staff_key" ;

    private static final String TAG = HotelStaffFragment.class.getSimpleName();

    private RelativeLayout mUserInfoLayout;

    public HotelStaffFragment(){
        super();
    }

    List<StaffModel> staffModelList = new ArrayList<>();

    RecyclerView mRecyclerView;

    ImageView mAddUserLayout;

    ImageView calView;

    HotelStaffPresenter mHotelStaffPresenter;

    private HotelDataResponse mHotelData;

    ProgressDialog progress;

    RecyclerGridAdapter mRecyclerViewAdapter;

    ViewGroup rootView;

    @BindView(R.id.button_save)
    Button saveButton;


    public static HotelStaffFragment newInstance(HotelDataResponse hotelData){
        HotelStaffFragment fragment = new HotelStaffFragment();
        Bundle args = new Bundle();
        args.putParcelable(HOTEL_STAFF_KEY, hotelData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            mHotelData = getArguments().getParcelable(HOTEL_STAFF_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = (ViewGroup)inflater.inflate(R.layout.fragment_hotel_staff, container, false);
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
        initPresenter();
        getAllStaff();

    }

    private void getAllStaff() {
        mHotelStaffPresenter.getAllStaff(mHotelData.getHotelId());

    }

    private void initPresenter() {
        mHotelStaffPresenter = new HotelStaffPresenter();
        mHotelStaffPresenter.setMvpView(this);
    }


    private void bindViews(View rootView) {

        mRecyclerView  = (RecyclerView)rootView.findViewById(R.id.users_grid);
        mAddUserLayout = (ImageView) rootView.findViewById(R.id.image_plus);
        mUserInfoLayout = (RelativeLayout) rootView.findViewById(R.id.add_user_parent);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false));
        mRecyclerViewAdapter = new RecyclerGridAdapter(staffModelList);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        calView = (ImageView) rootView.findViewById(R.id.img_cal);
        calView.setOnClickListener(v -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    HotelStaffFragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH),
                    Constants.CANCEL
            );

            dpd.setMaxDate(Calendar.getInstance());
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        });

        mAddUserLayout.setOnClickListener(v -> openAddUserLayout());
    }


    private void openAddUserLayout() {
        mRecyclerView.setVisibility(View.GONE);
        mUserInfoLayout.setVisibility(View.VISIBLE);
        mAddUserLayout.setVisibility(View.GONE);

    }
    @Override
    public void onResume() {
        super.onResume();
        Log.v(TAG, "onResume");
        ((BaseActivity)getActivity()).setActionBarTitle(mHotelData.getHotelName().toUpperCase());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

    }


    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Please wait...");
        progress.setCancelable(true);
        progress.show();

    }

    @Override
    public void hideLoading() {
        if(progress!=null){
            progress.hide();
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(getActivity(), ((dialog, which) -> {
                    dialog.dismiss();
                    getAllStaff();
                }),
                dialog1 -> {
                    dialog1.dismiss();
                });

    }

    @Override
    public void onStaffLoaded(List<StaffModel> staffModelList) {
        this.staffModelList = staffModelList;
        mRecyclerViewAdapter = new RecyclerGridAdapter(staffModelList);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

    @Override
    public void onStaffLoadFailure(String msg) {
        SnackbarUtils.show(rootView, msg);

    }



}
