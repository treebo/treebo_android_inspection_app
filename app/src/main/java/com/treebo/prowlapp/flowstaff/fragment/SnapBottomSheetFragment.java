package com.treebo.prowlapp.flowstaff.fragment;

import android.app.Dialog;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.treebo.prowlapp.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/**
 * Created by devesh on 07/06/16.
 */
public class SnapBottomSheetFragment extends BottomSheetDialogFragment {
    public static final int TAKE_PICTURE = 1;
    public static final int DELETE_PICTURE = 2;
    public static final int CHOOSE_PICTURE = 3;

    private View mRootView;

    private BottomSheetListener mBottomSheetErrorListener;

    public SnapBottomSheetFragment() {
        // Required empty constructor
    }

    @IntDef({TAKE_PICTURE, DELETE_PICTURE, CHOOSE_PICTURE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PictureState {
    }

    public static SnapBottomSheetFragment newInstance() {
        SnapBottomSheetFragment fragment = new SnapBottomSheetFragment();
        return fragment;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    public void setBottomSheetListener(BottomSheetListener listener) {
        mBottomSheetErrorListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        mRootView = View.inflate(getContext(), R.layout.bottom_sheet_snap, null);
        dialog.setContentView(mRootView);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) mRootView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        setUpLayoutClickListeners();

    }

    private void setUpLayoutClickListeners() {

        ImageView cross = (ImageView) mRootView.findViewById(R.id.bottom_sheet_cross);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        RelativeLayout takePhoto = (RelativeLayout) mRootView.findViewById(R.id.take_a_photo_layout);
        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBottomSheetErrorListener != null) {
                    dismiss();
                    mBottomSheetErrorListener.onBottomSheetSelected(TAKE_PICTURE);
                }
            }
        });

        RelativeLayout choosePhoto = (RelativeLayout) mRootView.findViewById(R.id.choose_photo_layout);
        choosePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBottomSheetErrorListener != null) {
                    dismiss();
                    mBottomSheetErrorListener.onBottomSheetSelected(CHOOSE_PICTURE);
                }
            }
        });

        RelativeLayout deletePhoto = (RelativeLayout) mRootView.findViewById(R.id.delete_photo_layout);
        deletePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBottomSheetErrorListener != null) {
                    dismiss();
                    mBottomSheetErrorListener.onBottomSheetSelected(DELETE_PICTURE);
                }
            }
        });

    }

    public interface BottomSheetListener {
        void onBottomSheetSelected(@PictureState int pictureState);
    }
}
