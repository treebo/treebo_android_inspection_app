package com.treebo.prowlapp.flowstaff.presenter;

import android.util.Log;

import com.treebo.prowlapp.flowstaff.IStaffContract;
import com.treebo.prowlapp.flowstaff.observer.UpdateStaffBankDetailsObserver;
import com.treebo.prowlapp.flowstaff.usecase.UpdateStaffBankDetailsUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.UpdateStaffView;
import com.treebo.prowlapp.response.UpdateStaffResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UpdateStaffUseCase;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by devesh on 03/05/16.
 */


public class UpdateStaffPresenter implements IStaffContract.IUpdateStaffPresenter {

    UpdateStaffView mBaseView;

    UseCase mUpdateStaffUseCase;

    UpdateStaffBankDetailsUseCase mBankDetailsUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView = (UpdateStaffView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    public void updateStaff(int staff_id, String first_name, String last_name, String phone, String fathers_name, String staff_url,
                            String address_line_1, String address_line_2, String state, String city, String pin_code,
                            int department, int hotel, int salary,
                            Date date_of_joining, Date date_of_leaving, Date leave_start_date, Date leave_end_date, Date date_of_birth,
                            ArrayList<String> languagesRead, ArrayList<String> languagesSpeak,
                            boolean isBankDetailsEnabled, String accountNo, String ifsc, String bankName, String branch,
                            String bankCity)

    {
        hideRetry();
        showLoading();
        mUpdateStaffUseCase = new UpdateStaffUseCase(staff_id, first_name, last_name, phone, fathers_name, staff_url,
                address_line_1, address_line_2, state, city, pin_code,
                department, hotel, salary,
                date_of_joining, date_of_leaving, leave_start_date, leave_end_date, date_of_birth,
                languagesRead, languagesSpeak);
        UpdateStaffSubscriber subscriber = new UpdateStaffSubscriber(this);
        subscriber.setFields(isBankDetailsEnabled, staff_id, first_name, accountNo, ifsc, bankName, branch, bankCity);
        mUpdateStaffUseCase.execute(subscriber);
    }


    public void updateBankDetails(int staffID, String staffName, String accountNo, String ifsc, String name, String branch,
                                  String city) {
        mBankDetailsUseCase = new UpdateStaffBankDetailsUseCase(staffID, staffName, accountNo, ifsc, name,
                branch, city);
        mBankDetailsUseCase.execute(new UpdateStaffBankDetailsObserver(this, mBankDetailsUseCase));
    }

    @Override
    public void onSuccess() {
        mBaseView.onUpdateSuccess();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if (mUpdateStaffUseCase != null) {
            mUpdateStaffUseCase.unsubscribe();
        }
    }

    @Override
    public BaseView getView() {
        return mBaseView;
    }


    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideLoading() {
        mBaseView.hideLoading();
    }

    private void showRetry() {
        mBaseView.showRetry();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }

    @Override
    public void showError(String message) {
        mBaseView.showError(message);
    }

    public class UpdateStaffSubscriber extends BaseObserver<UpdateStaffResponse> {

        private UpdateStaffPresenter mPresenter;
        private boolean isBankDetailsEnabled;
        private int staffID;
        private String staffName;
        private String accountNumber;
        private String ifscCode;
        private String bankName;
        private String branch;
        private String city;

        public UpdateStaffSubscriber(UpdateStaffPresenter basePresenter) {
            super(basePresenter, mUpdateStaffUseCase);
            mPresenter = basePresenter;
        }

        public void setFields(boolean isBankDetailsEnabled, int staffID, String staffName,
                              String accountNo, String ifsc, String name, String branch,
                              String city) {
            this.isBankDetailsEnabled = isBankDetailsEnabled;
            this.staffID = staffID;
            this.staffName = staffName;
            this.accountNumber = accountNo;
            this.ifscCode = ifsc;
            this.bankName = name;
            this.branch = branch;
            this.city = city;
        }


        @Override
        public void onError(Throwable e) {
            super.onError(e);
            hideLoading();
            mPresenter.showError(e.getMessage());
        }

        @Override
        public void onCompleted() {
            hideLoading();
        }

        @Override
        public void onNext(UpdateStaffResponse staffResponse) {
            Log.d("update response", staffResponse.toString());

            if (staffResponse.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)) {
                if (isBankDetailsEnabled) {
                    mPresenter.updateBankDetails(staffID, staffName, accountNumber, ifscCode,
                            bankName, branch, city);
                } else {
                    mPresenter.onSuccess();
                }
            } else {
                mPresenter.showError(staffResponse.msg);
            }
        }
    }


}
