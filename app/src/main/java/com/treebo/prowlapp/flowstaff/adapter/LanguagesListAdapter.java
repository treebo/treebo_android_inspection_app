package com.treebo.prowlapp.flowstaff.adapter;

import android.content.Context;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.treebo.prowlapp.R;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by devesh on 27/05/16.
 */
public class LanguagesListAdapter extends RecyclerView.Adapter<LanguagesViewHolder> {
    private Context mContext;
    private ArrayList<String> languagesList;
    private ArrayList<String> mSelectedLanguages;
    SparseArray<String> selectedLangsCache;
    OnLanguageSelectedListener mListener;
    String mType;


    public void setOnLangSelectedListener(OnLanguageSelectedListener listener){
        mListener = listener;
    }

    public LanguagesListAdapter(ArrayList<String> languages, ArrayList<String> selectedLanguages, String type) {
        this.languagesList = languages;
        this.mSelectedLanguages = selectedLanguages;
        if(mSelectedLanguages == null){
            mSelectedLanguages = new ArrayList<>();
        }
        mType = type;
        setUpLangsCache();
    }

    private void setUpLangsCache() {
        selectedLangsCache = new SparseArray<>();
        for (String lang : mSelectedLanguages) {
            if (languagesList.contains(lang)) {
                selectedLangsCache.put(languagesList.indexOf(lang), lang);
            }
        }
    }


    @Override
    public LanguagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.languages_list_item, parent, false);
        return new LanguagesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LanguagesViewHolder holder, int position) {
        if(mSelectedLanguages.contains(languagesList.get(position))){
            holder.languageCheckbox.setSelected(true);
        }else{
            holder.languageCheckbox.setSelected(false);
        }
        holder.languageTextView.setText(languagesList.get(position));

        holder.languageCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.languageCheckbox.isSelected()){
                    holder.languageCheckbox.setSelected(false);
                    mSelectedLanguages.remove(languagesList.get(position));
                    selectedLangsCache.remove(position);
                    notifyDataSetChanged();
                }else{
                    holder.languageCheckbox.setSelected(true);
                    mSelectedLanguages.add(languagesList.get(position));
                    selectedLangsCache.put(position, languagesList.get(position) );
                    notifyDataSetChanged();
                }
                mListener.onLanguageSelected(mSelectedLanguages,mType);
            }
        });
    }

    public ArrayList<String> getSelectedLanguages(){
        return mSelectedLanguages;
    }

    @Override
    public int getItemCount() {
        return languagesList.size();
    }

    public interface OnLanguageSelectedListener{
        void onLanguageSelected(ArrayList<String> selectedLangs,String mType);
    }

}


class LanguagesViewHolder extends RecyclerView.ViewHolder {
    TextView languageTextView;
    ImageView languageCheckbox;

    public LanguagesViewHolder(View itemView) {
        super(itemView);
        languageTextView = (TextView) itemView.findViewById(R.id.text_language);
        languageCheckbox = (ImageView) itemView.findViewById(R.id.check_box);
    }

}
