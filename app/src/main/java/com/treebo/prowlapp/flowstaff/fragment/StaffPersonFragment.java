package com.treebo.prowlapp.flowstaff.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowissues.activity.LeaveHistoryActivity;
import com.treebo.prowlapp.flowstaff.HotelStaffActivity;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.Models.StaffModel;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.NetworkUtil;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.StringUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sakshamdhawan on 02/05/16.
 */
public class StaffPersonFragment extends BaseFragment {

    public static final String HOTEL_DATA = "hotel_data";
    public static String STAFF_DATA = "staff_data";

    private StaffModel mStaffModel;
    ViewGroup rootView;
    HotelDataResponse mHotelData;
    private RolesAndUserPermissions.PermissionList mUserPermissions;

    @BindView(R.id.staff_name)
    TextView staff_name;

    @BindView(R.id.staff_phone_number_value)
    TextView staff_phone_number;

    @BindView(R.id.staff_date_of_joining_value)
    TextView staff_date_of_joining;

    @BindView(R.id.toolbar_title)
    TextView title;

    @BindView(R.id.person_department)
    TextView department;

    @BindView(R.id.fathers_name_value)
    TextView fatherName;

    @BindView(R.id.father_layout)
    LinearLayout mFatherLayout;

    @BindView(R.id.issues_expanded_row_heading)
    TextView tenureTitle;

    @BindView(R.id.to_filler)
    TextView toFiller;

    @BindView(R.id.staff_date_of_leaving_value)
    TextView mLeaveDateValue;

    @BindView(R.id.toolbar_edit)
    TextView mEditStaff;

    @BindView(R.id.staff_salary_value)
    TextView mSalary;

    @BindView(R.id.staff_date_of_leave_value)
    TextView mDateOfVacationStart;

    @BindView(R.id.staff_date_of_leave_end_value)
    TextView mDateOfVacationEnd;

    @BindView(R.id.vacation_layout)
    RelativeLayout mVacationLayout;

    @BindView(R.id.choose_sub_speak)
    TextView languagesSpoken;

    @BindView(R.id.choose_sub_read)
    TextView languagesRead;

    @BindView(R.id.leave_history_layout)
    RelativeLayout mLeaveLayout;
    @BindView(R.id.on_leave_till)
    TextView mOnLeaveTill;
    @BindView(R.id.staff_address_value)
    TextView staffAddressValue;

    @BindView(R.id.staff_row_bullet)
    ImageView staffImage;

    @BindView(R.id.staff_dob_value)
    TextView dobValue;

    @BindView(R.id.staff_dob)
    TextView dobTitle;

    @BindView(R.id.staff_dob_line)
    View dobLine;

    @BindView(R.id.bank_account_number_tv)
    TreeboTextView mBankAccountNumber;

    @BindView(R.id.ifsc_number_tv)
    TreeboTextView mBankIfscText;

    @BindView(R.id.bank_name_tv)
    TreeboTextView mBankNameText;

    @BindView(R.id.bank_branch_tv)
    TreeboTextView mBankBranchText;

    @BindView(R.id.bank_city_tv)
    TreeboTextView mBankCityText;


    public static StaffPersonFragment newInstance(StaffModel staffModel,
                                                  HotelDataResponse hotelData,
                                                  RolesAndUserPermissions.PermissionList permissions) {
        StaffPersonFragment staffListFragment = new StaffPersonFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(STAFF_DATA, staffModel);
        bundle.putParcelable(HOTEL_DATA, hotelData);
        bundle.putParcelable(LoginSharedPrefManager.PREF_USER_PERMISSIONS, permissions);
        staffListFragment.setArguments(bundle);
        return staffListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setTAG("StaffPersonFragment");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mStaffModel = getArguments().getParcelable(STAFF_DATA);
            mHotelData = getArguments().getParcelable(HOTEL_DATA);
            mUserPermissions = getArguments().getParcelable(LoginSharedPrefManager.PREF_USER_PERMISSIONS);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_staff_person, container, false);
        ButterKnife.bind(this, rootView);
        String name = StringUtils.capitalize(mStaffModel.first_name) + " " + StringUtils.capitalize(mStaffModel.last_name);
        if (name.length() > 15) {
            name.substring(0, 15);
        }
        title.setText(name);
        SetValues();
        return rootView;
    }

    private void SetValues() {
        Log.d("staff model", mStaffModel.toString());
        if (mStaffModel.is_active == 0 || !mUserPermissions.isStaffDetailsEditEnabled()) {
            mEditStaff.setVisibility(View.INVISIBLE);
        }
        staff_name.setText(mStaffModel.first_name + " " + mStaffModel.last_name);
        staff_phone_number.setText(mStaffModel.phone);

        tenureTitle.setText((mStaffModel.is_active == 1 && TextUtils.isEmpty(mStaffModel.dateOfLeaving)) ? getString(R.string.date_of_joining) : getString(R.string.tenure));
        mSalary.setText(getString(R.string.Rs) + " " + String.valueOf(mStaffModel.salary));
        if (mStaffModel.is_active == 1) {
            if (mStaffModel.dateOfLeaving == null || mStaffModel.dateOfLeaving.isEmpty()) {
                toFiller.setVisibility(View.INVISIBLE);
                mLeaveDateValue.setVisibility(View.INVISIBLE);
            } else {
                toFiller.setVisibility(View.VISIBLE);
                mLeaveDateValue.setVisibility(View.VISIBLE);
                Date leavedate = null;
                try {
                    leavedate = new SimpleDateFormat(Constants.DATE_FORMAT).parse(mStaffModel.dateOfLeaving);
                } catch (ParseException e) {
                    e.printStackTrace();

                }
                if (leavedate != null)
                    mLeaveDateValue.setText(new SimpleDateFormat("dd-MMM-yyyy").format(leavedate));
                else
                    mLeaveDateValue.setText(mStaffModel.dateOfLeaving);
            }

        } else {
            toFiller.setVisibility(View.VISIBLE);
            mLeaveDateValue.setVisibility(View.VISIBLE);
            Date leavedate = null;
            if (mStaffModel.dateOfLeaving != null) {
                try {
                    leavedate = new SimpleDateFormat(Constants.DATE_FORMAT).parse(mStaffModel.dateOfLeaving);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (leavedate != null)
                mLeaveDateValue.setText(new SimpleDateFormat("dd-MMM-yyyy").format(leavedate));
            else
                mLeaveDateValue.setText(mStaffModel.dateOfLeaving);

        }
        if (mStaffModel.fathers_name == null || mStaffModel.fathers_name.isEmpty()) {
            mFatherLayout.setVisibility(View.GONE);
        } else {
            mFatherLayout.setVisibility(View.VISIBLE);
            fatherName.setText(mStaffModel.fathers_name);
        }

        if (StringUtils.doesArrayListHaveMoreItems(mStaffModel.getLanguagePrefSpeak())) {
            languagesSpoken.setText(StringUtils.convertToCSV(mStaffModel.getLanguagePrefSpeak()));
            languagesSpoken.setTextColor(getResources().getColor(R.color.emerald));
        } else {
            languagesSpoken.setText("None");

        }

        if (StringUtils.doesArrayListHaveMoreItems(mStaffModel.getLanguagePrefRead())) {
            languagesRead.setText(StringUtils.convertToCSV(mStaffModel.getLanguagePrefRead()));
            languagesRead.setTextColor(getResources().getColor(R.color.emerald));
        } else {
            languagesRead.setText("None");
        }

        if (!TextUtils.isEmpty(mStaffModel.staffImageUrl)) {
            attachImage(mStaffModel.staffImageUrl);
        }

        if (!TextUtils.isEmpty(mStaffModel.dateOfBirth)) {
            dobValue.setVisibility(View.VISIBLE);
            dobTitle.setVisibility(View.VISIBLE);
            dobLine.setVisibility(View.VISIBLE);
            try {
                Date birthDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse(mStaffModel.dateOfBirth);
                dobValue.setText(new SimpleDateFormat("dd-MMM-yyyy").format(birthDate));
            } catch (ParseException e) {
                e.printStackTrace();
                dobValue.setText(mStaffModel.dateOfBirth);
            }
        }


        if (mStaffModel.leave_history != null && !mStaffModel.leave_history.isEmpty()) {
            mLeaveLayout.setVisibility(View.VISIBLE);
            String lastLeaveDate = mStaffModel.leave_history.get(mStaffModel.leave_history.size() - 1);
            String[] leavedates = new String[2];
            leavedates[0] = lastLeaveDate.substring(0, 10);
            leavedates[1] = lastLeaveDate.substring(12, lastLeaveDate.length()).trim();
            Date date = new Date();
            Date leaveStartDate;
            Date leaveEndDate;
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
                leaveStartDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse(leavedates[0]);
                leaveEndDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse(leavedates[1]);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(leaveEndDate);
                calendar.add(Calendar.DATE, 1);
                leaveEndDate = dateFormat.parse(dateFormat.format(calendar.getTime()));
                if (date.after(leaveStartDate) && date.before(leaveEndDate)) {
                    mOnLeaveTill.setVisibility(View.VISIBLE);
                    mOnLeaveTill.setText(String.format(getString(R.string.on_leave_till), leavedates[1]));
                } else {
                    mOnLeaveTill.setVisibility(View.INVISIBLE);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            mLeaveLayout.setOnClickListener((View v) -> {
                Intent intent = new Intent(getActivity(), LeaveHistoryActivity.class);
                intent.putExtra(Utils.LEAVE_HISTORY_LIST, mStaffModel.leave_history);
                startActivity(intent);

            });

        } else {
            mLeaveLayout.setVisibility(View.GONE);
        }


        Date date = null;
        try {
            date = new SimpleDateFormat(Constants.DATE_FORMAT).parse(mStaffModel.dateOfJoining);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (date != null) {
            staff_date_of_joining.setText(new SimpleDateFormat("dd-MMM-yyyy").format(date));
        } else if (!TextUtils.isEmpty(mStaffModel.dateOfJoining)) {
            staff_date_of_joining.setText(mStaffModel.dateOfJoining);
        } else {

        }

        department.setText(mStaffModel.departmentName);

        if (mStaffModel.leaveStartDate == null || mStaffModel.leaveStartDate.isEmpty()) {
            mVacationLayout.setVisibility(View.GONE);
        } else {
            date = null;
            try {
                date = new SimpleDateFormat(Constants.DATE_FORMAT).parse(mStaffModel.leaveStartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (date != null)
                mDateOfVacationStart.setText(new SimpleDateFormat("dd-MMM-yyyy").format(date));
            else
                mDateOfVacationStart.setText(mStaffModel.leaveStartDate);

            date = null;
            try {
                date = new SimpleDateFormat(Constants.DATE_FORMAT).parse(mStaffModel.leaveEndDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (date != null)
                mDateOfVacationEnd.setText(new SimpleDateFormat("dd-MMM-yyyy").format(date));
            else
                mDateOfVacationEnd.setText(mStaffModel.leaveEndDate);
        }
        StringBuilder stringBuilder = new StringBuilder();
        if (!TextUtils.isEmpty(mStaffModel.addressLine1)) {
            stringBuilder.append(mStaffModel.addressLine1);
            stringBuilder.append(" , ");
        }
        if (!TextUtils.isEmpty(mStaffModel.addressLine2)) {
            stringBuilder.append(mStaffModel.addressLine2);
            stringBuilder.append(System.getProperty("line.separator"));
        }
        if (!TextUtils.isEmpty(mStaffModel.city)) {
            stringBuilder.append(mStaffModel.city);
            stringBuilder.append(" - ");
        }
        if (!TextUtils.isEmpty(mStaffModel.pinCode)) {
            stringBuilder.append(mStaffModel.pinCode);
            stringBuilder.append(System.getProperty("line.separator"));
        }
        if (!TextUtils.isEmpty(mStaffModel.state)) {
            stringBuilder.append(mStaffModel.state);
        }
        staffAddressValue.setText(stringBuilder.toString());

        if (!TextUtils.isEmpty(mStaffModel.getAccountNumber())) {
            mBankAccountNumber.setText(mStaffModel.getAccountNumber());
        }
        if (!TextUtils.isEmpty(mStaffModel.getIfscCode())) {
            mBankIfscText.setText(mStaffModel.getIfscCode());
        }
        if (!TextUtils.isEmpty(mStaffModel.getBankName())) {
            mBankNameText.setText(mStaffModel.getBankName());
        }
        if (!TextUtils.isEmpty(mStaffModel.getBranch())) {
            mBankBranchText.setText(mStaffModel.getBranch());
        }
        if (!TextUtils.isEmpty(mStaffModel.getBankCity())) {
            mBankCityText.setText(mStaffModel.getBankCity());
        }

    }

    private void attachImage(String cloudUrl) {
        Utils.convertImageUrlToRoundedView(getActivity(), cloudUrl, staffImage);
    }

    @OnClick(R.id.toolbar_edit)
    void onClickToolbarEdit() {
        if (!NetworkUtil.isConnected(getActivity())) {
            SnackbarUtils.show(rootView, Constants.INTERNET_NOT_AVAILABLE);
            return;
        }
        ((HotelStaffActivity) getActivity()).addFragment(StaffPersonEditFragment
                .newInstance(mStaffModel, mHotelData, mUserPermissions));
    }


    @OnClick(R.id.image_view_back)
    void onClickBack() {
        Log.d("back clicked", "");
        getActivity().onBackPressed();
    }


}
