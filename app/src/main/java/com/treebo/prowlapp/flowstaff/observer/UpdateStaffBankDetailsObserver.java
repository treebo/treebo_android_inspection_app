package com.treebo.prowlapp.flowstaff.observer;

import android.util.Log;

import com.treebo.prowlapp.flowstaff.presenter.SendStaffPresenter;
import com.treebo.prowlapp.flowstaff.presenter.UpdateStaffPresenter;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 12/05/17.
 */

public class UpdateStaffBankDetailsObserver extends BaseObserver<BaseResponse> {

    private UpdateStaffPresenter mUpdateStaffPresenter;
    private SendStaffPresenter mAddStaffPresenter;
    private boolean isUpdateStaff = false;

    public UpdateStaffBankDetailsObserver(UpdateStaffPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mUpdateStaffPresenter = presenter;
        isUpdateStaff = true;
    }

    public UpdateStaffBankDetailsObserver(SendStaffPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mAddStaffPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(BaseResponse baseResponse) {
        Log.d("update response", baseResponse.toString());

        if (baseResponse.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)) {
            if (isUpdateStaff)
                mUpdateStaffPresenter.onSuccess();
            else
                mAddStaffPresenter.onAddStaffSuccess();
        } else {
            if (isUpdateStaff)
                mUpdateStaffPresenter.showError(baseResponse.msg);
            else
                mAddStaffPresenter.handleBankDetailsError(baseResponse.msg);
        }
    }
}
