package com.treebo.prowlapp.flowstaff.fragment;

import android.app.Dialog;
import android.os.Bundle;

import android.view.View;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowstaff.adapter.StatesBottomSheetAdapter;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by aa on 11/07/16.
 */
public class StatesBottomSheetFragment extends BottomSheetDialogFragment {

    private StateSelectedListener mStateSelectedListener;

    private static final String KEY_STATES_LIST = "STATES_LIST";

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    public static StatesBottomSheetFragment newInstance(String[] statesList) {
        StatesBottomSheetFragment fragment = new StatesBottomSheetFragment();
        Bundle args = new Bundle();
        args.putStringArray(KEY_STATES_LIST, statesList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View rootView = View.inflate(getContext(), R.layout.bottom_sheet_state_list, null);

        dialog.setContentView(rootView);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) rootView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        setStateList(rootView);
    }

    private void setStateList(View rootView) {

        RecyclerView rvStateList = (RecyclerView) rootView.findViewById(R.id.rv_state_list);

        if (rvStateList != null) {
            rvStateList.setLayoutManager(new LinearLayoutManager(getContext()));

            rvStateList.setAdapter(new StatesBottomSheetAdapter(
                    getArguments().getStringArray(KEY_STATES_LIST),
                    mStateSelectedListener)
            );
        }

    }

    public void setStateSelectedListener(StateSelectedListener stateSelectedListener) {
        mStateSelectedListener = stateSelectedListener;
    }

    public interface StateSelectedListener {
        void onStateSelected(int position);
    }
}
