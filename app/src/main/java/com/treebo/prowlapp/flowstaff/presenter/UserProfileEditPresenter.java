package com.treebo.prowlapp.flowstaff.presenter;

import android.os.Handler;

import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.UserProfileEditView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.DepartmentResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.GetDepartmentsUseCase;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by devesh on 02/05/16.
 */
public class UserProfileEditPresenter implements BasePresenter {

    UserProfileEditView mBaseView;

    UseCase mGetDepartmentsUseCase;

    int hotelId;


    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView = (UserProfileEditView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    public void getAllDepartments(int hotel_id) {
        hideRetry();
        showLoading();
        hotelId = hotel_id;
        mGetDepartmentsUseCase = new GetDepartmentsUseCase(hotelId);
        mGetDepartmentsUseCase.execute(new DepartsmentsResponseObserver(this));
    }

    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideLoading() {
        mBaseView.hideLoading();
    }

    private void showRetry() {
        mBaseView.showRetry();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if (mGetDepartmentsUseCase != null) {
            mGetDepartmentsUseCase.unsubscribe();
        }
    }

    @Override
    public BaseView getView() {
        return mBaseView;
    }

    public class DepartsmentsResponseObserver extends BaseObserver<DepartmentResponse> {

        public DepartsmentsResponseObserver(BasePresenter basePresenter) {
            super(basePresenter, mGetDepartmentsUseCase);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            hideLoading();
            mBaseView.showError(e.getMessage());
        }

        @Override
        public void onCompleted() {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideLoading();
                }
            }, 2000);
        }

        @Override
        public void onNext(DepartmentResponse scoresResponse) {
            if (scoresResponse.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)) {
                mBaseView.onDepartmentsFetched(scoresResponse);
            } else {
                mBaseView.showError(scoresResponse.msg);
            }
        }
    }
}
