package com.treebo.prowlapp.flowstaff.adapter;

import android.content.Context;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.DepartmentWiseStaffModel;
import com.treebo.prowlapp.Models.StaffModel;
import com.treebo.prowlapp.Utils.Utils;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by devesh on 01/05/16.
 */
public class StaffSectionedAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    List<DepartmentWiseStaffModel> departmentWiseStaffModels;
    private OnStaffItemCLickedListener mListener;
    private Context mContext;


    public StaffSectionedAdapter(Context context,List<DepartmentWiseStaffModel> departmentWiseStaffModels) {
        mContext=context;
        this.departmentWiseStaffModels = departmentWiseStaffModels;
    }

    @Override
    public int getSectionCount() {
        return departmentWiseStaffModels.size();
    }

    @Override
    public int getItemCount(int section) {
        return departmentWiseStaffModels.get(section).staff.size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {
        String sectionName = departmentWiseStaffModels.get(section).title;
        ((SectionViewHolder) holder).sectionHeading.setText(sectionName);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int section, final int relativePosition, final int absolutePosition) {
        List<StaffModel> departmentStaff = departmentWiseStaffModels.get(section).staff;
        ((StaffRowViewHolder) holder).staffImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.audit_categories_bullet));
        final StaffModel currentStaff = departmentStaff.get(relativePosition);
        String staffName = currentStaff.first_name + " "+ currentStaff.last_name;
        String staffDept = currentStaff.departmentName;
        ((StaffRowViewHolder) holder).staffRowName.setText(staffName);
        ((StaffRowViewHolder) holder).divider.setVisibility(relativePosition == getItemCount(section) - 1 ? View.INVISIBLE : View.VISIBLE);
        if(currentStaff.is_active==0){
            ((StaffRowViewHolder) holder).exEmployee.setVisibility(View.VISIBLE);
            ((StaffRowViewHolder) holder).staffRowName.setTextColor(mContext.getResources().getColor(R.color.grey_400));

        }else{
            ((StaffRowViewHolder) holder).exEmployee.setVisibility(View.INVISIBLE);
            ((StaffRowViewHolder) holder).staffRowName.setTextColor(mContext.getResources().getColor(R.color.black_100));
        }
        ((StaffRowViewHolder) holder).itemView.setOnClickListener(v -> {
            if(mListener != null){
                mListener.onStaffItemClicked(currentStaff);
            }
        });
        if(!TextUtils.isEmpty(currentStaff.staffImageUrl)) {
            Utils.convertImageUrlToRoundedView(mContext , currentStaff.staffImageUrl,((StaffRowViewHolder) holder).staffImage);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.staff_row_title, parent, false);
                return new SectionViewHolder(v);
            case VIEW_TYPE_ITEM:
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.staff_row, parent, false);
                return new StaffRowViewHolder(v);
        }
    }


    public class StaffRowViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        TextView staffRowName;
        TextView exEmployee;
        ImageView staffImage;
        View divider;

        public StaffRowViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            staffRowName = (TextView) itemView.findViewById(R.id.staff_row_name);
            exEmployee = (TextView) itemView.findViewById(R.id.staff_last_date);
            staffImage = (ImageView) itemView.findViewById(R.id.staff_row_bullet) ;
            divider=itemView.findViewById(R.id.staff_divider);

        }
    }

    public class SectionViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        TextView sectionHeading;


        public SectionViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            sectionHeading = (TextView) itemView.findViewById(R.id.heading);

        }
    }

    public void setOnStaffItemCLickedListener(OnStaffItemCLickedListener listener){
        this.mListener = listener;
    }
    public interface OnStaffItemCLickedListener{
        void onStaffItemClicked(StaffModel model);
    }

}
