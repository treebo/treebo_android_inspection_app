package com.treebo.prowlapp.net;

import android.os.Build;

import com.amazonaws.com.google.gson.Gson;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.apis.RestApi;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.errors.ErrorHandler;
import com.treebo.prowlapp.Models.AnswerListWithSectionName;
import com.treebo.prowlapp.Models.AppUpdateResponse;
import com.treebo.prowlapp.Models.HotelStaffModelResponse;
import com.treebo.prowlapp.Models.auditmodel.AnswerListV3;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.auditmodel.FraudAuditSubmitObject;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.pushMessaging.responseModels.RegisterDeviceIdResponse;
import com.treebo.prowlapp.response.AddStaffUserResponse;
import com.treebo.prowlapp.response.BankListResponse;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.response.ChangeOTPResponse;
import com.treebo.prowlapp.response.DepartmentResponse;
import com.treebo.prowlapp.response.FormDataResponse;
import com.treebo.prowlapp.response.HotelIncentivesResponse;
import com.treebo.prowlapp.response.HotelRoomResponse;
import com.treebo.prowlapp.response.HotelsListResponse;
import com.treebo.prowlapp.response.IssuesListResponse;
import com.treebo.prowlapp.response.MarkIncentiveDone;
import com.treebo.prowlapp.response.PortfolioResponse;
import com.treebo.prowlapp.response.RootCauseResponse;
import com.treebo.prowlapp.response.ScoresResponse;
import com.treebo.prowlapp.response.UpdateIssueResponse;
import com.treebo.prowlapp.response.UpdateStaffResponse;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.response.audit.AuditCategoryResponse;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import com.treebo.prowlapp.response.audit.FraudAuditResponse;
import com.treebo.prowlapp.response.audit.FraudAuditSubmitResponse;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.response.common.HotelLocationResponse;
import com.treebo.prowlapp.response.common.MasterChecklistResponse;
import com.treebo.prowlapp.response.common.SystemPropertyResponse;
import com.treebo.prowlapp.response.common.UserPermissionsResponse;
import com.treebo.prowlapp.response.dashboard.ContactInfoResponse;
import com.treebo.prowlapp.response.dashboard.HotelMetricsResponse;
import com.treebo.prowlapp.response.dashboard.HotelTasksResponse;
import com.treebo.prowlapp.response.dashboard.TaskHistoryResponse;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;
import com.treebo.prowlapp.response.issue.IssueDetailResponse;
import com.treebo.prowlapp.response.issue.IssueListResponse;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.response.login.LogoutResponse;
import com.treebo.prowlapp.response.portfolio.PendingTasksListResponse;
import com.treebo.prowlapp.response.portfolio.PortfolioHotelListResponseV3;
import com.treebo.prowlapp.response.portfolio.PortfolioMetricsResponse;
import com.treebo.prowlapp.response.portfolio.PortfolioSingleHotelResponse;
import com.treebo.prowlapp.response.staffincentives.DepartmentListResponse;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.response.staffincentives.StaffListResponse;
import com.treebo.prowlapp.response.taskcreation.TaskCreationResponse;
import com.treebo.prowlapp.response.taskcreation.TaskListResponse;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.Utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import rx.Observable;
import rxAndroid.SchedulersCompat;

/**
 * Created by devesh on 26/04/16.
 */
public class RestClient {


    @Inject
    public RestApi mRestApi;


    public RestClient() {
        MainApplication.get().getRestComponent().injectRestClient(this);
    }


    public Observable<LoginResponse> attemptLogin(String email, String otp) {
        RequestBody loginRequest = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", email)
                .addFormDataPart("password", otp)
                .build();
        Observable<LoginResponse> loginRequestObservable = mRestApi.login(loginRequest);
        return loginRequestObservable;
    }


    public Observable<ChangeOTPResponse> changeOtp(String mPhoneNo) {
        RequestBody otpRequest = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("phone", mPhoneNo)
                .build();
        Observable<ChangeOTPResponse> changeOTPResponseObservable = mRestApi.forgotpassword(otpRequest);
        return changeOTPResponseObservable;
    }


    public Observable<AddStaffUserResponse> addStaffUser(String first_name, String last_name,
                                                         String phone, String fathers_name,
                                                         String staff_url, String address_line_1,
                                                         String address_line_2, String state,
                                                         String city, String pin_code,
                                                         int department, int hotel, int salary,
                                                         Date date_of_joining, Date date_of_leaving,
                                                         Date leave_start_date, Date leave_end_date,
                                                         Date date_of_birth,
                                                         ArrayList<String> languagesRead,
                                                         ArrayList<String> languagesSpeak) {

        String formattedJoiningDate = "", formattedLeavingDate = "", formattedLeaveStartDate = "",
                formattedLeaveEndDate = "", formattedBirthDate = "";
        if (staff_url == null) {
            staff_url = "";
        }
        if (date_of_joining != null)
            formattedJoiningDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(date_of_joining);
        if (date_of_leaving != null)
            formattedLeavingDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(date_of_leaving);
        if (leave_start_date != null)
            formattedLeaveStartDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(leave_start_date);
        if (leave_end_date != null)
            formattedLeaveEndDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(leave_end_date);
        if (date_of_birth != null)
            formattedBirthDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(date_of_birth);

        RequestBody addStaffUserRequest = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("first_name", first_name)
                .addFormDataPart("last_name", last_name)
                .addFormDataPart("phone", phone)
                .addFormDataPart("fathers_name", fathers_name)
                .addFormDataPart("staff_url", staff_url)
                .addFormDataPart("address_line_1", address_line_1)
                .addFormDataPart("address_line_2", address_line_2)
                .addFormDataPart("state", state)
                .addFormDataPart("city", city)
                .addFormDataPart("pin_code", pin_code)
                .addFormDataPart("department", "" + department)
                .addFormDataPart("hotel", "" + hotel)
                .addFormDataPart("salary", "" + salary)
                .addFormDataPart("date_of_birth", formattedBirthDate)
                .addFormDataPart("date_of_joining", formattedJoiningDate)
                .addFormDataPart("date_of_leaving", formattedLeavingDate)
                .addFormDataPart("leave_start_date", formattedLeaveStartDate)
                .addFormDataPart("leave_end_date", formattedLeaveEndDate)
                .addFormDataPart("language_pref_read", new Gson().toJson(languagesRead))
                .addFormDataPart("language_pref_speak", new Gson().toJson(languagesSpeak))
                .build();
        Observable<AddStaffUserResponse> addStaffUserResponse = mRestApi
                .addStaffUser(addStaffUserRequest);
        return addStaffUserResponse.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<AddStaffUserResponse>applyExecutorSchedulers());
    }

    public Observable<LoginResponse> refreshAccessToken(RequestBody refreshTokenRequest) {
        return mRestApi.refreshAccessToken(refreshTokenRequest);
    }


    public Observable<UpdateIssueResponse> updateIssue(String issueId, String status,
                                                       String rootCause, String description,
                                                       String imageUrls) {
        RequestBody updateIssueBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("issueid", issueId)
                .addFormDataPart("status", status)
                .addFormDataPart("rootcause", rootCause)
                .addFormDataPart("description", description)
                .addFormDataPart("image", imageUrls)
                .build();
        Observable<UpdateIssueResponse> updateUserResponse = mRestApi.updateIssue(updateIssueBody);
        return updateUserResponse.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<UpdateIssueResponse>applyExecutorSchedulers());
    }

    public retrofit2.Call<UpdateIssueResponse> updateIssueOffline(String issueId, String status,
                                                                  String rootCause, String description,
                                                                  String imageUrls) {
        RequestBody updateIssueBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("issueid", issueId)
                .addFormDataPart("status", status)
                .addFormDataPart("rootcause", rootCause)
                .addFormDataPart("description", description)
                .addFormDataPart("image", imageUrls)
                .build();
        Call<UpdateIssueResponse> updateUserResponse = mRestApi.updateIssueOffline(updateIssueBody);
        return updateUserResponse;
    }

    public Observable<MarkIncentiveDone> markIncentivesPaid(String staffId) {
        RequestBody updateIssueBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("staff_id", staffId)
                .build();
        Observable<MarkIncentiveDone> updateUserResponse = mRestApi
                .markIncentivesPaid(updateIssueBody);
        return updateUserResponse.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<UpdateIssueResponse>applyExecutorSchedulers());
    }

    public Observable<SubmitAuditResponse> submitAudit(AnswerListWithSectionName answer,
                                                       String hotelId, String userId,
                                                       String formId, String source) {
        String submissionDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String answerWrap = new Gson().toJson(answer);
        ArrayList<AnswerListWithSectionName> array = new ArrayList<>();
        array.add(answer);
        answerWrap = new Gson().toJson(array);
        RequestBody updateIssueBody = null;
        try {
            updateIssueBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("answer", Utils.compress(answerWrap))
                    .addFormDataPart("hotel_id", hotelId)
                    .addFormDataPart("user_id", userId)
                    .addFormDataPart("submission_date", submissionDate)
                    .addFormDataPart("form_id", formId)
                    .addFormDataPart("source", source)
                    .build();


        } catch (Exception e) {
            e.printStackTrace();
        }
        Observable<SubmitAuditResponse> submitAuditResponse = mRestApi.submitAudit(updateIssueBody);
        return submitAuditResponse.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<SubmitAuditResponse>applyExecutorSchedulers());

    }

    public Call<SubmitAuditResponse> submitAuditOffline(AnswerListWithSectionName answer,
                                                        String hotelId, String formId, String userId,
                                                        String source, String submissionDate) {
        if (submissionDate.isEmpty()) {
            submissionDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        }
        String answerWrap = new Gson().toJson(answer);
        ArrayList<AnswerListWithSectionName> array = new ArrayList<>();
        array.add(answer);
        answerWrap = new Gson().toJson(array);
        RequestBody updateIssueBody = null;
        try {
            updateIssueBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("answer", Utils.compress(answerWrap))
                    .addFormDataPart("hotel_id", hotelId)
                    .addFormDataPart("user_id", userId)
                    .addFormDataPart("submission_date", submissionDate)
                    .addFormDataPart("form_id", formId)
                    .addFormDataPart("source", source)
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Call<SubmitAuditResponse> submitAuditResponse = mRestApi.submitAuditOffline(updateIssueBody);
        return submitAuditResponse;
    }

    public Observable<HotelStaffModelResponse> getAllStaffUsers(int hotel_id) {
        Observable<HotelStaffModelResponse> allStaffUsersResponse = mRestApi.getHotelStaff(hotel_id);
        return allStaffUsersResponse.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<HotelStaffModelResponse>applyExecutorSchedulers());
    }

    public Observable<ScoresResponse> getAllScores(int hotel_id) {
        Observable<ScoresResponse> allStaffUsersResponse = mRestApi.getAllScores(hotel_id);
        return allStaffUsersResponse.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<ScoresResponse>applyExecutorSchedulers());
    }

    public Observable<HotelIncentivesResponse> getAllIncentives(int hotel_id) {
        Observable<HotelIncentivesResponse> hotelIncentivesResponseObservable = mRestApi
                .getAllIncentives(hotel_id);
        return hotelIncentivesResponseObservable;
    }

    public Observable<FormDataResponse> getFormMetaData() {
        Observable<FormDataResponse> formMetaData = mRestApi.getFormMetaData();
        return formMetaData.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<FormDataResponse>applyExecutorSchedulers());
    }

    public Observable<IssuesListResponse> getIssuesList(int hotelId) {
        Observable<IssuesListResponse> issueList = mRestApi.getIssuesList(hotelId);
        return issueList.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<IssuesListResponse>applyExecutorSchedulers());
    }

    public Observable<HotelRoomResponse> getHotelRooms(int hotelId) {
        Observable<HotelRoomResponse> hotelResponseData = mRestApi.getHotelRooms(hotelId);
        return hotelResponseData.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<HotelRoomResponse>applyExecutorSchedulers());
    }

    public Call<HotelRoomResponse> getHotelRoomsOffline(int hotelId) {
        Call<HotelRoomResponse> hotelResponseData = mRestApi.getHotelRoomsOffline(hotelId);
        return hotelResponseData;
    }

    public Observable<DepartmentResponse> getAllDepartments(int hotelId) {
        Observable<DepartmentResponse> departmentResponseObservable = mRestApi
                .getAllDepartments(hotelId);
        return departmentResponseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<DepartmentResponse>applyExecutorSchedulers());
    }

    public Observable<UpdateStaffResponse> updateStaffDetails(int staff_id, String first_name,
                                                              String last_name, String phone,
                                                              String fathers_name, String staff_url,
                                                              String address_line_1, String address_line_2,
                                                              String state, String city, String pin_code,
                                                              int department, int hotel, int salary,
                                                              Date date_of_joining, Date date_of_leaving,
                                                              Date leave_start_date, Date leave_end_date,
                                                              Date date_of_birth,
                                                              ArrayList<String> languagesRead,
                                                              ArrayList<String> languagesSpeak) {
        String formattedJoiningDate = "", formattedLeavingDate = "", formattedLeaveStartDate = "",
                formattedLeaveEndDate = "", formattedBirthDate = "";

        if (date_of_joining != null)
            formattedJoiningDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(date_of_joining);
        if (date_of_leaving != null)
            formattedLeavingDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(date_of_leaving);
        if (leave_start_date != null)
            formattedLeaveStartDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(leave_start_date);
        if (leave_end_date != null)
            formattedLeaveEndDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(leave_end_date);
        if (date_of_birth != null)
            formattedBirthDate = new SimpleDateFormat(Constants.DATE_FORMAT).format(date_of_birth);
        if (staff_url == null) {
            staff_url = "";
        }


        RequestBody uodateStaffUserRequest = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("first_name", first_name)
                .addFormDataPart("last_name", last_name)
                .addFormDataPart("phone", phone)
                .addFormDataPart("fathers_name", fathers_name)
                .addFormDataPart("staff_url", staff_url)
                .addFormDataPart("address_line_1", address_line_1)
                .addFormDataPart("address_line_2", address_line_2)
                .addFormDataPart("state", state)
                .addFormDataPart("city", city)
                .addFormDataPart("pin_code", pin_code)
                .addFormDataPart("department", "" + department)
                .addFormDataPart("hotel", "" + hotel)
                .addFormDataPart("salary", "" + salary)
                .addFormDataPart("date_of_birth", formattedBirthDate)
                .addFormDataPart("date_of_joining", formattedJoiningDate)
                .addFormDataPart("language_pref_read", new Gson().toJson(languagesRead))
                .addFormDataPart("language_pref_speak", new Gson().toJson(languagesSpeak))
                .addFormDataPart("date_of_leaving", formattedLeavingDate)
                .addFormDataPart("leave_start_date", formattedLeaveStartDate)
                .addFormDataPart("leave_end_date", formattedLeaveEndDate)
                .build();
        Observable<UpdateStaffResponse> addStaffUserResponse = mRestApi.updateStaffDetails(staff_id,
                uodateStaffUserRequest);
        return addStaffUserResponse.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<AddStaffUserResponse>applyExecutorSchedulers());
    }

    public Observable<HotelsListResponse> getAllHotels(int userId) {
        Observable<HotelsListResponse> responseObservable = mRestApi.getAllHotels(userId);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<HotelsListResponse>applyExecutorSchedulers());
    }

    public Observable<PortfolioResponse> getAllPortfolio(int userId) {
        Observable<PortfolioResponse> responseObservable = mRestApi.getAllPortfolio(userId);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<HotelsListResponse>applyExecutorSchedulers());
    }

    public Observable<RootCauseResponse> getRootCause() {
        Observable<RootCauseResponse> responseObservable = mRestApi.getRootCause();
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<RootCauseResponse>applyExecutorSchedulers());
    }

    public Observable<AppUpdateResponse> getUpdateDetails(int appId) {
        Observable<AppUpdateResponse> responseObservable = mRestApi.getUpdateDetails(appId);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<RootCauseResponse>applyExecutorSchedulers());
    }

    //call for device id registration for push messages through FCM
    public Call<RegisterDeviceIdResponse> registerDeviceId(String deviceId) {
        return mRestApi.registerDeviceId(deviceId);
    }

    public Observable<LogoutResponse> logout(String accessToken) {
        RequestBody logoutRequest = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("token", accessToken)
                .build();
        Observable<LogoutResponse> responseObservable = mRestApi.logoutAsync(logoutRequest);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyExecutorSchedulers());

    }

    public Observable<LoginResponse> googleLogin(String idToken, String appVersion) {
        Observable<LoginResponse> responseObservable = mRestApi.googleLogin(idToken, appVersion);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<AuditAreaResponse> getAuditTypePeriodic(int hotelId, int auditId, int userId) {
        Observable<AuditAreaResponse> responseObservable = mRestApi.getAuditTypePeriodic(hotelId,
                auditId, 4.2f,userId);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }
    public Observable<AuditAreaResponse> getAuditTypePeriodicRemote(int hotelId, int auditId, int userId, String auditSubmissionType) {
        Observable<AuditAreaResponse> responseObservable = mRestApi.getAuditTypePeriodicRemote(hotelId,
                auditId, 4.2f,userId,auditSubmissionType);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }


    public Observable<CommonAreaTypeResponse> getAuditTypeCommon(int hotelId) {
        Observable<CommonAreaTypeResponse> responseObservable = mRestApi.getAuditTypeCommon(hotelId);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<RoomSingleResponse> getAuditTypeRoom(int hotelId, int roomId) {
        Observable<RoomSingleResponse> responseObservable = mRestApi.getAuditTypeRoom(hotelId,
                roomId);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<RoomListResponse> getRoomList(int hotelId) {
        Observable<RoomListResponse> responseObservable = mRestApi.getRoomsList(hotelId);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }


    public Call<MasterChecklistResponse> getMasterList() {
        Call<MasterChecklistResponse> responseObservable = mRestApi.getMasterList();
        return responseObservable;

    }

    public Call<AuditCategoryResponse> getAuditList() {
        Call<AuditCategoryResponse> responseObservable = mRestApi.getAuditCategories();
        return responseObservable;

    }

    public Call<HotelLocationResponse> getHotelLocations() {
        Call<HotelLocationResponse> responseObservable = mRestApi.getHotelLocations();
        return responseObservable;

    }

    public Call<SystemPropertyResponse> getSystemProperties() {
        return mRestApi.getSystemProperties();
    }

    public Observable<SubmitAuditResponse> createNewIssue(ArrayList<AnswerV3> issueList, int roomId,
                                                          int userId, int auditTypeID,
                                                          int hotelId, String source) {
        String answer;
        AnswerListV3 answerListV3 = new AnswerListV3(issueList);
        answer = new Gson().toJson(answerListV3);
        String submissionDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Observable<SubmitAuditResponse> responseObservable = null;
        try {
            responseObservable = mRestApi.createNewIssue(submissionDate, userId,
                    auditTypeID, hotelId, source, Utils.compress(answer), roomId != -1 ? roomId : null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<SubmitAuditResponse> submitAuditV3(ArrayList<AnswerV3> issueList, int roomId,
                                                         int userId, int auditTypeID,
                                                         int hotelId, String source,
                                                         int taskLogID,
                                                         String currentDate, String audit_submission_type) {
        String answer;
        AnswerListV3 answerListV3 = new AnswerListV3(issueList);
        answer = new Gson().toJson(answerListV3);
        String submissionDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Observable<SubmitAuditResponse> responseObservable = null;
        try {
            if(audit_submission_type.equals("Remote")){
                responseObservable = mRestApi.submitRoomCommonAuditRemote(submissionDate, userId,
                        auditTypeID, hotelId, source, Utils.compress(answer), roomId, audit_submission_type,currentDate);
            }
            else{
            responseObservable = mRestApi.submitRoomCommonAudit(submissionDate, userId,
                    auditTypeID, hotelId, source, Utils.compress(answer), roomId, taskLogID,currentDate);}
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<SubmitAuditResponse> submitPeriodicAuditV3(ArrayList<AnswerV3> issueList,
                                                                 int userId, int auditTypeID,
                                                                 int hotelId, String source,
                                                                 int taskLogID, String currentDate, String audit_submission_type) {
        String answer;
        AnswerListV3 answerListV3 = new AnswerListV3(issueList);
        answer = new Gson().toJson(answerListV3);
        String submissionDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Observable<SubmitAuditResponse> responseObservable = null;
        try {
            if(audit_submission_type.equals("Remote")){
                responseObservable = mRestApi.submitPeriodicAuditRemote(submissionDate, userId,
                        auditTypeID, hotelId, source, Utils.compress(answer), audit_submission_type, currentDate);
            }
            else {
                responseObservable = mRestApi.submitPeriodicAudit(submissionDate, userId,
                        auditTypeID, hotelId, source, Utils.compress(answer), taskLogID, currentDate);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<FraudAuditSubmitResponse> submitFraudAudit(ArrayList<FraudAuditSubmitObject> fraudIssues,
                                                                 int hotelID, int userID,
                                                                 int taskLogID) {
        String answer = new Gson().toJson(fraudIssues);
        String submissionDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Observable<FraudAuditSubmitResponse> responseObservable = null;
        try {
            responseObservable = mRestApi.submitFraudAudit(hotelID, userID, taskLogID,
                    submissionDate, Utils.compress(answer));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Call<SubmitAuditResponse> submitAuditV3Offline(ArrayList<AnswerV3> issueList, int roomId,
                                                          int userId, int auditType,
                                                          int hotelId, String source) {
        String answer;
        AnswerListV3 answerListV3 = new AnswerListV3(issueList);
        answer = new Gson().toJson(answerListV3);
        String submissionDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Call<SubmitAuditResponse> response = null;
        try {
            response = mRestApi.submitAuditOffline(Utils.compress(answer), roomId, submissionDate
                    , userId, auditType, hotelId, source);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public Observable<IssueListResponse> getIssueList(int hotelId) {
        Observable<IssueListResponse> responseObservable =
                mRestApi.getIssueList(hotelId, Constants.PENDING);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<IssueDetailResponse> getIssueDetails(int hotelId, int issueId) {
        Observable<IssueDetailResponse> responseObservable =
                mRestApi.getIssueDetails(hotelId, issueId);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<BaseResponse> updateIssue(int hotelId, int issueId, String resolutionDate,
                                                String comment, String images, String issueStatus,
                                                String userID, int taskLogID) {
        Observable<BaseResponse> responseObservable =
                mRestApi.updateIssue(hotelId, issueId, resolutionDate, comment, images, issueStatus,
                        userID, Constants.SOURCE_QAM, taskLogID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<PortfolioHotelListResponseV3> getAllHotelsInPortfolio(int userID) {
        Observable<PortfolioHotelListResponseV3> responseObservable =
                mRestApi.getAllHotelsInPortfolio(userID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<PortfolioSingleHotelResponse> getSingleHotelDetails(int userID, int hotelID) {
        Observable<PortfolioSingleHotelResponse> responseObservable =
                mRestApi.getSingleHotelDetails(userID, hotelID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<PortfolioMetricsResponse> getPortfolioMetrics(int userID) {
        Observable<PortfolioMetricsResponse> responseObservable =
                mRestApi.getPortfolioMetrics(userID);         // Actual endpoint call
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<HotelMetricsResponse> getHotelMetrics(int userID, int hotelID) {
        Observable<HotelMetricsResponse> responseObservable =
                mRestApi.getHotelMetrics(hotelID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<HotelTasksResponse> getTaskList(int userID, int hotelID) {
        Observable<HotelTasksResponse> responseObservable =
                mRestApi.getTaskList(userID, hotelID, 3.5f);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<HotelTasksResponse> getTaskListRemote(int userID, int hotelID) {
        Observable<HotelTasksResponse> responseObservable =
                mRestApi.getTaskListRemote(userID, hotelID, 3.5f);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<BaseResponse> submitFeedback(int userID, String feedback) {
        Observable<BaseResponse> responseObservable =
                mRestApi.submitFeedback(feedback, userID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<TaskHistoryResponse> getTaskHistory(int userID) {
        Observable<TaskHistoryResponse> responseObservable =
                mRestApi.getTaskHistory(userID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<BaseResponse> submitTaskDone(int taskID, int taskLogID, int userID,
                                                   int hotelID, String status) {
        Observable<BaseResponse> responseObservable = mRestApi.taskDone(hotelID, userID,
                taskID, taskLogID, status);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }
    public Observable<BaseResponse> submitTaskDoneRemote(int taskID, int userID,
                                                   int hotelID, String status) {
        Observable<BaseResponse> responseObservable = mRestApi.taskDoneRemote(hotelID, userID,
                taskID, status);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<BaseResponse> postRoomNotReady(int roomID, String reason) {
        Observable<BaseResponse> responseObservable = mRestApi.roomNotReady(roomID, reason);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<FraudAuditResponse> getFraudAuditCheckpoints(int hotelID) {
        Observable<FraudAuditResponse> responseObservable =
                mRestApi.getFraudAuditCheckpoints(hotelID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Call<SubmitAuditResponse> logLocationErrorDetails(double latitude, double longitude,
                                                             int userID, float accuracy) {
        return mRestApi.logLocationNotDetected(latitude, longitude, userID, accuracy,
                Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE);
    }

    public Observable<StaffListResponse> getOngoingStaffList(int hotelId, int departmentID,
                                                             String type, int month, int year) {
        Observable<StaffListResponse> responseObservable = mRestApi
                .getStaffIncentiveList(hotelId, departmentID, type, month, year);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<DepartmentListResponse> getDepartmentList(int hotelId, String type,
                                                                int month, int year) {
        Observable<DepartmentListResponse> responseObservable = mRestApi
                .getDepartmentList(hotelId, type, month, year);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<MonthlyIncentiveListResponse> getMonthlyIncentiveList(int hotelId) {
        Observable<MonthlyIncentiveListResponse> responseObservable = mRestApi
                .getMonthlyIncentiveList(hotelId);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<BaseResponse> incentiveGiven(int hotelId, int deptID, int staffID,
                                                   String reason, int month, int year,
                                                   int incentiveGiven) {
        Observable<BaseResponse> responseObservable = mRestApi
                .incentivePaid(hotelId, deptID, staffID, month, year, reason, incentiveGiven);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<ContactInfoResponse> getContactInformation() {
        Observable<ContactInfoResponse> responseObservable = mRestApi
                .getContactInformation();
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<PendingTasksListResponse> getPendingTasksInfo(int userID) {
        Observable<PendingTasksListResponse> responseObservable = mRestApi
                .getPendingTasksList(userID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<EscalationUserListResponse> getEscalationUserList() {
        Observable<EscalationUserListResponse> responseObservable = mRestApi
                .getEscalationUserList();
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<BaseResponse> escalateIssue(int hotelID, ArrayList<Integer> escalationIDList,
                                                  String comment, int userID, int issueID) {
        String userList = "";
        for (Integer id : escalationIDList) {
            userList += String.valueOf(id) + ",";
        }
        Observable<BaseResponse> responseObservable = mRestApi
                .postIssueEscalation(hotelID,
                        userList.length() > 1 ? userList.substring(0, userList.length() - 1) : userList,
                        issueID, userID, comment);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<BaseResponse> postStaffBankDetails(int staffID, String userName, String accountNo,
                                                         String ifscCode, String bankName,
                                                         String bankBranch, String city) {
        Observable<BaseResponse> responseObservable = mRestApi
                .postStaffBankDetails(staffID, userName, accountNo, ifscCode, bankName, bankBranch, city);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Call<BankListResponse> getMasterBankList() {
        return mRestApi.getMasterBankList();
    }

    public Call<UserPermissionsResponse> getUserPermissions(int userId) {
        return mRestApi.getUserPermissions(userId);
    }

    public Observable<IssueListResponse> getIssueNotCaughtList(int userID) {
        Observable<IssueListResponse> responseObservable = mRestApi.getIssueNotCaughtList(userID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<GeographicalDataResponse> getGeographicalData() {
        Observable<GeographicalDataResponse> responseObservable = mRestApi.getGeographicalData();
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<TaskListResponse> getCreatedTaskList(int userID) {
        Observable<TaskListResponse> responseObservable = mRestApi
//                .getCreatedTaskList();
                .getCreatedTaskList(userID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<BaseResponse> deleteCreatedTask(int userID, int taskID) {
        Observable<BaseResponse> responseObservable = mRestApi.deleteCreatedTask(userID, taskID);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<TaskCreationResponse> editCreatedTask(int userID, CreatedTaskModel task) {
        Observable<TaskCreationResponse> responseObservable = mRestApi.editCreatedTask(userID,
                task.getTaskID(), task);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<TaskCreationResponse> createTask(int userID, CreatedTaskModel task) {
        Observable<TaskCreationResponse> responseObservable = mRestApi.createTask(userID, task);
        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }

    public Observable<HotelLocationResponse> getPortfolioHotelsLocation(int userID) {
        Observable<HotelLocationResponse> responseObservable = mRestApi.getPortfolioHotelLocations(userID);

        return responseObservable.concatMap((response) -> {
            return ErrorHandler.filterServerResponseErrors(response);
        }).compose(SchedulersCompat.<LogoutResponse>applyIoSchedulers());
    }


}
