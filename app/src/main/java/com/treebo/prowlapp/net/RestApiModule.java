package com.treebo.prowlapp.net;

import android.os.Build;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.apis.RestApi;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.typeadapters.DoubleTypeAdapter;
import com.treebo.prowlapp.typeadapters.FloatTypeAdapter;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by sumandas on 23/07/2016.
 */
@Module
public class RestApiModule {

    public static final String API_ROOT = "http://" + BuildConfig.API_ROOT;
    public static final String SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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";
    LoginSharedPrefManager mLoginManager;

    public RestApiModule(LoginSharedPrefManager loginSharedPrefManager) {
        mLoginManager = loginSharedPrefManager;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor, Interceptor authInterceptor) {
        return new OkHttpClient.Builder()
                .readTimeout(Constants.TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .connectTimeout(Constants.TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(authInterceptor)
                .build();
    }

    @Provides
    @Singleton
    Interceptor providesAuthInterceptor() {
        return chain -> {
            Request original = chain.request();
            //if auth token is null header is omitted by retrofit and is sent as a no auth request
            Request.Builder requestBuilder = original.newBuilder()
                    .header("Authorization",
                            mLoginManager.getTokenType() + " " + mLoginManager.getAccessToken())
                    .header("USERID", String.valueOf(mLoginManager.getUserId()))
                    .header("DEVICEID", TextUtils.concat(Build.MANUFACTURER, Build.MODEL).toString())
                    .header("APPVERSION", BuildConfig.VERSION_NAME)
                    .method(original.method(), original.body());

            Request request = requestBuilder.build();
            return chain.proceed(request);
        };
    }


    @Provides
    @Singleton
    HttpLoggingInterceptor providesLoginInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return loggingInterceptor;
    }

    @Provides
    @Singleton
    GsonConverterFactory providesGsonConverterFactory() {

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(double.class, new DoubleTypeAdapter())
                .registerTypeAdapter(float.class, new FloatTypeAdapter())
                .enableComplexMapKeySerialization()
                .serializeNulls()
                .setVersion(1.0)
                .create();
        GsonConverterFactory factory = GsonConverterFactory.create(gson);
        return factory;
    }


    @Provides
    @Singleton
    CallAdapter.Factory providesRxJavaCallAdapterFactory() {
        return RxErrorHandlingCallAdapterFactory.create();
    }

    @Provides
    @Singleton
    MainThreadExecutor providesMainThreadExecutor() {
        return new MainThreadExecutor();
    }


    @Provides
    @Singleton
    RestApi providesRestAdapter(OkHttpClient client,
                                GsonConverterFactory gsonConverterFactory,
                                CallAdapter.Factory rxErrorHandlingCallAdapterFactory,
                                MainThreadExecutor mainThreadExecutor) {

        Retrofit sRetrofit = new Retrofit.Builder()
                .baseUrl(API_ROOT)
                .client(client)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxErrorHandlingCallAdapterFactory)
                .callbackExecutor(mainThreadExecutor)
                .build();
        return sRetrofit.create(RestApi.class);
    }

}
