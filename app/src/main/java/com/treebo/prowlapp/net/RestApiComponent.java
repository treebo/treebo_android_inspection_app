package com.treebo.prowlapp.net;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by sumandas on 25/07/2016.
 */
@Singleton
@Component(modules = RestApiModule.class)
public interface RestApiComponent {
    void injectRestClient(RestClient restClient);
}
