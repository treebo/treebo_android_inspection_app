package com.treebo.prowlapp.dbflow;

import com.treebo.prowlapp.dbflow.inteface.IPhotoManager;
import com.treebo.prowlapp.Models.Photo;

import java.util.ArrayList;

/**
 * Created by sumandas on 17/06/2016.
 */
public class PhotoManagerImpl implements IPhotoManager {
    @Override
    public boolean addPhoto(Photo photo) {
        return false;
    }

    @Override
    public boolean addPhotos(ArrayList<Photo> photos) {
        return false;
    }

    @Override
    public ArrayList<Photo> getPhotosByKey(String key) {
        return null;
    }


    @Override
    public boolean deletePhoto(String photoId) {
        return false;
    }

    @Override
    public boolean deletePhotosByKey(String key) {
        return false;
    }
}

