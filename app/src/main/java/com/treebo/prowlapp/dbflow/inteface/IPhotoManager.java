package com.treebo.prowlapp.dbflow.inteface;

import com.treebo.prowlapp.Models.Photo;

import java.util.ArrayList;

/**
 * Created by sumandas on 17/06/2016.
 */
public interface IPhotoManager {
    boolean addPhoto(Photo photo);
    boolean addPhotos(ArrayList<Photo> photos);
    ArrayList<Photo> getPhotosByKey(String key);

    boolean deletePhoto(String photoId);
    boolean deletePhotosByKey(String key);

}
