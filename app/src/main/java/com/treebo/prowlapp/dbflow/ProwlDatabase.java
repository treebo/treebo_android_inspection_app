package com.treebo.prowlapp.dbflow;

import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;

/**
 * Created by sumandas on 17/06/2016.
 */
@Database(name = ProwlDatabase.NAME, version = ProwlDatabase.VERSION)
public class ProwlDatabase {
    public static final String NAME = "ProwlDatabase";
    public static final int VERSION = 7;

    @Migration(version = 7, priority = 1, database = ProwlDatabase.class)
    public static class AddColumnMigration extends AlterTableMigration<AuditCategory> {

        public AddColumnMigration(Class<AuditCategory> table) {
            super(table);
        }

        @Override
        public void onPreMigrate() {
            super.onPreMigrate();
            addColumn(SQLiteType.TEXT, "isIssueCreatable");
        }
    }
}
