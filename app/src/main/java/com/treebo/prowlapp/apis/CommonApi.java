package com.treebo.prowlapp.apis;

import android.util.Log;

import com.treebo.prowlapp.errors.ErrorHandler;
import com.treebo.prowlapp.exception.UnauthorizedAccessException;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.HttpCodeUtils;
import com.treebo.prowlapp.Utils.Utils;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.functions.Func1;
import rxAndroid.SchedulersCompat;

/**
 * Created by devesh on 03/05/16.
 */
public class CommonApi {

    @SuppressWarnings("unchecked")
    public static <T> Func1<Throwable, ? extends Observable<? extends T>> refreshTokenAndRetry(final Observable<T> toBeResumed) {
        return (throwable) -> {
            // Here check if the error thrown really is a 401
            if (HttpCodeUtils.isHttp401Error(throwable) || throwable instanceof UnauthorizedAccessException) {
                return refreshToken()
                        .retry(Constants.NUM_RETRIES)
                        .flatMap((userEntity) -> {
                                    if (userEntity.code != Constants.REFRESH_TOKEN_FAILED) {
                                        Utils.persistAuthToken(userEntity);
                                        return toBeResumed;
                                    } else {
                                        return ErrorHandler.filterServerResponseErrors(userEntity);
                                    }
                                }
                        )
                        .compose(SchedulersCompat.<T>applyExecutorSchedulers());
            }
            // re-throw this error because it's not recoverable from here
            return Observable.error(throwable);
        };
    }


    public static Observable<LoginResponse> refreshToken() {
        return new RestClient().refreshAccessToken(getRefreshTokenRequestBody());
    }

    public static RequestBody getRefreshTokenRequestBody() {
        String refreshToken = LoginSharedPrefManager.getInstance().getRefreshToken();
        String userName = LoginSharedPrefManager.getInstance().getEmailId();
        Log.v("refresh token", refreshToken);
        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", userName)
                .addFormDataPart("refresh_token", refreshToken)
                .addFormDataPart("app_version", Constants.VERSION_V3)
                .build();

    }


}