
package com.treebo.prowlapp.apis;


import com.treebo.prowlapp.Models.AppUpdateResponse;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.HotelStaffModelResponse;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.pushMessaging.responseModels.RegisterDeviceIdResponse;
import com.treebo.prowlapp.response.AddStaffUserResponse;
import com.treebo.prowlapp.response.BankListResponse;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.response.ChangeOTPResponse;
import com.treebo.prowlapp.response.DepartmentResponse;
import com.treebo.prowlapp.response.FormDataResponse;
import com.treebo.prowlapp.response.HotelIncentivesResponse;
import com.treebo.prowlapp.response.HotelRoomResponse;
import com.treebo.prowlapp.response.HotelsListResponse;
import com.treebo.prowlapp.response.IssuesListResponse;
import com.treebo.prowlapp.response.MarkIncentiveDone;
import com.treebo.prowlapp.response.PortfolioResponse;
import com.treebo.prowlapp.response.RootCauseResponse;
import com.treebo.prowlapp.response.ScoresResponse;
import com.treebo.prowlapp.response.UpdateIssueResponse;
import com.treebo.prowlapp.response.UpdateStaffResponse;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.response.audit.AuditCategoryResponse;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import com.treebo.prowlapp.response.audit.FraudAuditResponse;
import com.treebo.prowlapp.response.audit.FraudAuditSubmitResponse;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.response.common.HotelLocationResponse;
import com.treebo.prowlapp.response.common.MasterChecklistResponse;
import com.treebo.prowlapp.response.common.SystemPropertyResponse;
import com.treebo.prowlapp.response.common.UserPermissionsResponse;
import com.treebo.prowlapp.response.dashboard.ContactInfoResponse;
import com.treebo.prowlapp.response.dashboard.HotelMetricsResponse;
import com.treebo.prowlapp.response.dashboard.HotelTasksResponse;
import com.treebo.prowlapp.response.dashboard.TaskHistoryResponse;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;
import com.treebo.prowlapp.response.issue.IssueDetailResponse;
import com.treebo.prowlapp.response.issue.IssueListResponse;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.response.login.LogoutResponse;
import com.treebo.prowlapp.response.portfolio.PendingTasksListResponse;
import com.treebo.prowlapp.response.portfolio.PortfolioHotelListResponseV3;
import com.treebo.prowlapp.response.portfolio.PortfolioMetricsResponse;
import com.treebo.prowlapp.response.portfolio.PortfolioSingleHotelResponse;
import com.treebo.prowlapp.response.staffincentives.DepartmentListResponse;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.response.staffincentives.StaffListResponse;
import com.treebo.prowlapp.response.taskcreation.TaskCreationResponse;
import com.treebo.prowlapp.response.taskcreation.TaskListResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;


/**
 * Created by devesh on 26/04/16.
 */
public interface RestApi {

    @POST("/prowl/users/login/")
    Observable<LoginResponse> login(@Body RequestBody loginRequest);

    @POST("/prowl/users/forgotpassword/")
    Observable<ChangeOTPResponse> forgotpassword(@Body RequestBody loginRequest);

    @POST("/prowl/rest/v1/staff/add/")
    Observable<AddStaffUserResponse> addStaffUser(@Body RequestBody addStaffRequest);

    @POST("/prowl/users/refresh/")
    Observable<LoginResponse> refreshAccessToken(@Body RequestBody refreshTokenRequest);

    @GET("/prowl/rest/v1/hotels/{hotel_id}/staff/")
    Observable<HotelStaffModelResponse> getHotelStaff(@Path("hotel_id") int hotelId);

    @GET("/prowl/rest/v1/scores")
    Observable<ScoresResponse> getAllScores(@Query("hotel_id") int hotelId);

    @GET("/prowl/rest/v1/incentives")
    Observable<HotelIncentivesResponse> getAllIncentives(@Query("hotel_id") int hotelId);

    @GET("prowl/rest/v1/formmetadata")
    Observable<FormDataResponse> getFormMetaData();

    @GET("prowl/rest/v1/issues")
    Observable<IssuesListResponse> getIssuesList(@Query("hotel_id") int hotelId);

    @POST("prowl/rest/v1/staff/{staff_id}/")
    Observable<UpdateStaffResponse> updateStaffDetails(@Path("staff_id") int staffId, @Body RequestBody addStaffRequest);

    @GET("/prowl/rest/v1/hotels/{hotel_id}/rooms/")
    Observable<HotelRoomResponse> getHotelRooms(@Path("hotel_id") int hotelId);

    @POST("/prowl/rest/v1/issues/update")
    Observable<UpdateIssueResponse> updateIssue(@Body RequestBody updateIssueBody);

    @POST("/prowl/rest/v1/submitform")
    Observable<SubmitAuditResponse> submitAudit(@Body RequestBody submitForm);

    @GET("/prowl/rest/v1/departments")
    Observable<DepartmentResponse> getAllDepartments(@Query("hotel_id") int hotelId);

    @GET("/prowl/rest/v1/user/{user_id}/hotels")
    Observable<HotelsListResponse> getAllHotels(@Path("user_id") int userId);

    @GET("/prowl/rest/v1/portfolioscores")
    Observable<PortfolioResponse> getAllPortfolio(@Query("user_id") int userId);

    @GET("/prowl/rest/v1/rootcause")
    Observable<RootCauseResponse> getRootCause();

    @GET("/prowl/rest/v1/app/version")
    Observable<AppUpdateResponse> getUpdateDetails(@Query("app_id") int appId);

    @POST("/prowl/rest/v1/submitform")
    Call<SubmitAuditResponse> submitAuditOffline(@Body RequestBody submitForm);

    @GET("/prowl/rest/v1/hotels/{hotel_id}/rooms/")
    Call<HotelRoomResponse> getHotelRoomsOffline(@Path("hotel_id") int hotelId);

    @POST("/prowl/rest/v1/issues/update")
    Call<UpdateIssueResponse> updateIssueOffline(@Body RequestBody updateIssueBody);

    @POST("/prowl/rest/v1/staff/markpaid")
    Observable<MarkIncentiveDone> markIncentivesPaid(@Body RequestBody body);

    @POST("/prowl/rest/v1/device")
    @FormUrlEncoded
    Call<RegisterDeviceIdResponse> registerDeviceId(@Field("registration_id") String deviceId);

    @POST("/prowl/users/logout/")
    Observable<LogoutResponse> logoutAsync(@Body RequestBody logoutRequest);

    @FormUrlEncoded
    @POST("/prowl/users/google/login")
    Observable<LoginResponse> googleLogin(@Field("id_token") String authToken,
                                          @Field("app_version") String version);

    @GET("/prowl/rest/v3/hotel/{hotel_id}/audit/{audit_id}/")
    Observable<AuditAreaResponse> getAuditTypePeriodic(@Path("hotel_id") int hotelId,
                                                       @Path("audit_id") int auditId,
                                                       @Query("version_no") float versionNumber,
                                                       @Query("user_id") int userId);

    @GET("/prowl/rest/v3/hotel/{hotel_id}/audit/{audit_id}/")
    Observable<AuditAreaResponse> getAuditTypePeriodicRemote(@Path("hotel_id") int hotelId,
                                                       @Path("audit_id") int auditId,
                                                       @Query("version_no") float versionNumber,
                                                       @Query("user_id") int userId,
                                                       @Query("audit_submission_type") String auditSubmissionType);


    @GET("/prowl/rest/v3/hotel/{hotel_id}/audit/commonarea/")
    Observable<CommonAreaTypeResponse> getAuditTypeCommon(@Path("hotel_id") int hotelId);

    @GET("/prowl/rest/v3/hotel/{hotel_id}/audit/room/{room_id}")
    Observable<RoomSingleResponse> getAuditTypeRoom(@Path("hotel_id") int hotelId,
                                                    @Path("room_id") int roomId);

    @GET("/prowl/rest/v3/hotel/{hotel_id}/audit/room/")
    Observable<RoomListResponse> getRoomsList(@Path("hotel_id") int hotelId);

    @GET("/prowl/rest/v3/getcheckpoints")
    Call<MasterChecklistResponse> getMasterList();

    @GET("/prowl/rest/v3/hotel")
    Call<HotelLocationResponse> getHotelLocations();

    @GET("/prowl/rest/v3/configurations")
    Call<SystemPropertyResponse> getSystemProperties();

    @GET("/prowl/rest/v3/users/{user_id}/roles")
    Call<UserPermissionsResponse> getUserPermissions(@Path("user_id") int userID);


    @POST("prowl/rest/v3/auditsubmitform")
    @FormUrlEncoded
    Observable<SubmitAuditResponse> submitRoomCommonAudit(@Field("submission_date") String date,
                                                          @Field("user_id") int userId,
                                                          @Field("audit_type_id") int auditTypeId,
                                                          @Field("hotel_id") int hotelId,
                                                          @Field("source") String source,
                                                          @Field("answer") String answer,
                                                          @Field("room_id") int roomId,
                                                          @Field("task_log_id") int taskLogID,
                                                          @Field("start_time") String startTime);

    @POST("prowl/rest/v3/auditsubmitform")
    @FormUrlEncoded
    Observable<SubmitAuditResponse> submitRoomCommonAuditRemote(@Field("submission_date") String date,
                                                          @Field("user_id") int userId,
                                                          @Field("audit_type_id") int auditTypeId,
                                                          @Field("hotel_id") int hotelId,
                                                          @Field("source") String source,
                                                          @Field("answer") String answer,
                                                          @Field("room_id") int roomId,
                                                          @Field("audit_submission_type") String audit_submission_type,
                                                          @Field("start_time") String startTime);

    @POST("prowl/rest/v3/auditsubmitform")
    @FormUrlEncoded
    Observable<SubmitAuditResponse> submitPeriodicAudit(@Field("submission_date") String date,
                                                        @Field("user_id") int userId,
                                                        @Field("audit_type_id") int auditTypeId,
                                                        @Field("hotel_id") int hotelId,
                                                        @Field("source") String source,
                                                        @Field("answer") String answer,
                                                        @Field("task_log_id") int taskLogID,
                                                        @Field("start_time") String startTime);

    @POST("prowl/rest/v3/auditsubmitform")
    @FormUrlEncoded
    Observable<SubmitAuditResponse> submitPeriodicAuditRemote(@Field("submission_date") String date,
                                                        @Field("user_id") int userId,
                                                        @Field("audit_type_id") int auditTypeId,
                                                        @Field("hotel_id") int hotelId,
                                                        @Field("source") String source,
                                                        @Field("answer") String answer,
                                                        @Field("audit_submission_type") String audit_submission_type,
                                                        @Field("start_time") String startTime);

    @POST("prowl/rest/v3/issue-submit-form/")
    @FormUrlEncoded
    Observable<SubmitAuditResponse> createNewIssue(@Field("submission_date") String date,
                                                        @Field("user_id") int userId,
                                                        @Field("audit_type_id") int auditTypeId,
                                                        @Field("hotel_id") int hotelId,
                                                        @Field("source") String source,
                                                        @Field("answer") String answer,
                                                        @Field("room_id") Integer roomID);

    @POST("prowl/rest/v3/hotels/{hotel_id}/frauds")
    @FormUrlEncoded
    Observable<FraudAuditSubmitResponse> submitFraudAudit(@Path("hotel_id") int hotelID,
                                                          @Field("user_id") int userID,
                                                          @Field("task_log_id") int taskLogID,
                                                          @Field("submission_date") String date,
                                                          @Field("answer") String answer);


    @POST("prowl/rest/v3/auditsubmitform")
    @FormUrlEncoded
    Call<SubmitAuditResponse> submitAuditOffline(@Field("answer") String answer,
                                                 @Field("room_id") int roomId,
                                                 @Field("submission_date") String date,
                                                 @Field("user_id") int userId,
                                                 @Field("audit_type_id") int auditTypeId,
                                                 @Field("hotel_id") int hotelId,
                                                 @Field("source") String source);

    @GET("/prowl/rest/v3/hotel/{hotel_id}/issues/")
    Observable<IssueListResponse> getIssueList(@Path("hotel_id") int hotel_id, @Query("type") String type);

    @GET("/prowl/rest/v3/hotels/{hotel_id}/issues/{issue_id}/")
    Observable<IssueDetailResponse> getIssueDetails(@Path("hotel_id") int hotelId,
                                                    @Path("issue_id") int issueId);

    @POST("/prowl/rest/v3/hotels/{hotel_id}/issues/{issue_id}/")
    @FormUrlEncoded
    Observable<BaseResponse> updateIssue(@Path("hotel_id") int hotel_id, @Path("issue_id") int issue_id,
                                         @Field("resolution_date") String resolution_date,
                                         @Field("comment") String comment,
                                         @Field("images") String images,
                                         @Field("status") String status,
                                         @Field("user_id") String userID,
                                         @Field("source") String source,
                                         @Field("task_log_id") int taskLogID);


    @GET("/prowl/rest/v3/qam/{user_id}")
    Observable<PortfolioHotelListResponseV3> getAllHotelsInPortfolio(@Path("user_id") int userID);

    @GET("/prowl/rest/v3/users/{user_id}/hotels/{hotel_id}")
    Observable<PortfolioSingleHotelResponse> getSingleHotelDetails(@Path("user_id") int userID,
                                                                   @Path("hotel_id") int hotelID);

    @GET("/prowl/rest/v3/user/{user_id}/portfolio")
    Observable<PortfolioMetricsResponse> getPortfolioMetrics(@Path("user_id") int userID);

    @GET("/prowl/rest/v3/hotels/{hotel_id}/portfolio")
    Observable<HotelMetricsResponse> getHotelMetrics(@Path("hotel_id") int hotelID);

    @GET("/prowl/rest/v3/hotels/{hotel_id}/user/{user_id}/tasks")
    Observable<HotelTasksResponse> getTaskList(@Path("user_id") int userID,
                                               @Path("hotel_id") int hotelID,
                                               @Query("version_no") float versionNumber);

    @GET("/prowl/rest/v3/hotels/{hotel_id}/user/{user_id}/remote-tasks")
    Observable<HotelTasksResponse> getTaskListRemote(@Path("user_id") int userID,
                                               @Path("hotel_id") int hotelID,
                                               @Query("version_no") float versionNumber);

    @POST("prowl/rest/v3/feedback")
    @FormUrlEncoded
    Observable<BaseResponse> submitFeedback(@Field("feedback") String feedback,
                                            @Field("user_id") int userID);

    @GET("/prowl/rest/v3/history/{user_id}")
    Observable<TaskHistoryResponse> getTaskHistory(@Path("user_id") int userID);

    @PUT("prowl/rest/v3/hotels/{hotel_id}/user/{user_id}/tasks")
    @FormUrlEncoded
    Observable<BaseResponse> taskDone(@Path("hotel_id") int hotelID, @Path("user_id") int userID,
                                      @Field("task_id") int taskID,
                                      @Field("task_log_id") int taskLogID,
                                      @Field("status") String status);

    @POST("prowl/rest/v3/hotels/{hotel_id}/user/{user_id}/remote-tasks/")
    @FormUrlEncoded
    Observable<BaseResponse> taskDoneRemote(@Path("hotel_id") int hotelID, @Path("user_id") int userID,
                                      @Field("task_id") int taskID,
                                      @Field("status") String status);

    @POST("prowl/rest/v3/rooms/{room_id}/mark/notready")
    @FormUrlEncoded
    Observable<BaseResponse> roomNotReady(@Path("room_id") int roomID,
                                          @Field("reason") String reason);

    @GET("/prowl/rest/v3/getauditcategories")
    Call<AuditCategoryResponse> getAuditCategories();

    @GET("prowl/rest/v3/hotels/{hotel_id}/fraudcheckpoints")
    Observable<FraudAuditResponse> getFraudAuditCheckpoints(@Path("hotel_id") int hotelID);

    @POST("/prowl/rest/v3/hotels/error-logs/")
    @FormUrlEncoded
    Call<SubmitAuditResponse> logLocationNotDetected(@Field("lat") double latitude,
                                                     @Field("long") double longitude,
                                                     @Field("user_id") int userID,
                                                     @Field("accuracy") float accuracy,
                                                     @Field("phone_make") String phoneMake,
                                                     @Field("phone_model") String phoneModel,
                                                     @Field("os_version") String osVersion);

    @GET("/prowl/rest/v3/hotels/{hotel_id}/staff-incentives/departments/{department_id}")
    Observable<StaffListResponse> getStaffIncentiveList(@Path("hotel_id") int hotelID,
                                                        @Path("department_id") int departmentID,
                                                        @Query("type") String type,
                                                        @Query("month") int month,
                                                        @Query("year") int year);

    @GET("/prowl/rest/v3/hotels/{hotel_id}/staff-incentives")
    Observable<DepartmentListResponse> getDepartmentList(@Path("hotel_id") int hotelID,
                                                         @Query("type") String type,
                                                         @Query("month") int month,
                                                         @Query("year") int year);

    @GET("/prowl/rest/v3/hotels/{hotel_id}/pending-incentives/")
    Observable<MonthlyIncentiveListResponse> getMonthlyIncentiveList(@Path("hotel_id") int hotelID);

    @POST("/prowl/rest/v3/hotels/{hotel_id}/staff-incentives/departments/{department_id}/staffs/{staff_id}/")
    @FormUrlEncoded
    Observable<BaseResponse> incentivePaid(@Path("hotel_id") int hotelID,
                                           @Path("department_id") int departmentID,
                                           @Path("staff_id") int staffID,
                                           @Field("month") int month,
                                           @Field("year") int year,
                                           @Field("reason") String reason,
                                           @Field("incentive_given") int incentiveGiven);

    @GET("/prowl/rest/v3/contacts")
    Observable<ContactInfoResponse> getContactInformation();

    @GET("/prowl/rest/v3/users/{user_id}/task-completion-report")
    Observable<PendingTasksListResponse> getPendingTasksList(@Path("user_id") int userID);

    @GET("/prowl/rest/v3/hotel-users")
    Observable<EscalationUserListResponse> getEscalationUserList();

    @POST("/prowl/rest/v3/hotel/{hotel_id}/issues/escalation/")
    @FormUrlEncoded
    Observable<BaseResponse> postIssueEscalation(@Path("hotel_id") int hotelID,
                                                 @Field("escalation_user_ids") String idList,
                                                 @Field("issue_id") int issueID,
                                                 @Field("user_id") int userID,
                                                 @Field("comment") String comment);

    @POST("/prowl/rest/v3/staff/{staff_id}/bank-details/")
    @FormUrlEncoded
    Observable<BaseResponse> postStaffBankDetails(@Path("staff_id") int staffID,
                                                  @Field("user_name") String userName,
                                                  @Field("account_number") String accountNumber,
                                                  @Field("ifsc_code") String ifscCode,
                                                  @Field("bank_name") String bankName,
                                                  @Field("branch") String branch,
                                                  @Field("city") String city);

    @GET("/prowl/rest/v3/bank-details")
    Call<BankListResponse> getMasterBankList();

    @GET("/prowl/rest/v3/users/{user_id}/uncaught_issues/")
    Observable<IssueListResponse> getIssueNotCaughtList(@Path("user_id") int userID);

    @GET("/prowl/rest/v3/geography-map/")
    Observable<GeographicalDataResponse> getGeographicalData();

    @GET("/prowl/rest/v3/users/{user_id}/created-tasks/")
    Observable<TaskListResponse> getCreatedTaskList(@Path("user_id") int userID);

    @DELETE("/prowl/rest/v3/users/{user_id}/created-tasks/{task_id}")
    Observable<BaseResponse> deleteCreatedTask(@Path("user_id") int userID,
                                               @Path("task_id") int taskID);

    @Headers("Content-Type: application/json")
    @PUT("/prowl/rest/v3/users/{user_id}/created-tasks/{task_id}")
    Observable<TaskCreationResponse> editCreatedTask(@Path("user_id") int userID,
                                                     @Path("task_id") int taskID,
                                                     @Body CreatedTaskModel taskModel);

    @Headers("Content-Type: application/json")
    @POST("/prowl/rest/v3/users/{user_id}/created-tasks/")
    Observable<TaskCreationResponse> createTask(@Path("user_id") int userID,
                                                @Body CreatedTaskModel taskModel);

    @GET("/prowl/rest/v3/users/{user_id}/hotels/")
    Observable<HotelLocationResponse> getPortfolioHotels(@Path("user_id") int userID);

    @GET("/prowl/rest/v3/users/{user_id}/portfolio-hotel-locations/")
    Observable<HotelLocationResponse> getPortfolioHotelLocations(@Path("user_id") int userId);
}

