package com.treebo.prowlapp.apis;

import android.os.AsyncTask;
import android.util.Log;

import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.net.RestApiModule;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SlackPushErrorAPI<Void> extends AsyncTask<Void, Void, Void> {

    String mRequestUrl;
    Throwable mError;
    String mErrorMessage;

    public SlackPushErrorAPI(String requestUrl, String errorMessage, Throwable error) {
        this.mRequestUrl = requestUrl;
        this.mError = error;
        this.mErrorMessage = errorMessage;

    }

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            JSONObject dataObj = new JSONObject();

            dataObj.put("text", "Android app V" + BuildConfig.VERSION_NAME + " api failed\n" + mRequestUrl + "\n`" + mErrorMessage + "`\n````" + convertStackTraceToString(mError) + "````");

            URL url = new URL(RestApiModule.SLACK_WEBHOOK_URL);
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, dataObj.toString());
            Request request = new Request.Builder().url(url)
                    .addHeader("Content-type", "application/json")
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            Log.i(SlackPushErrorAPI.class.getName(), response.message());
        } catch (JSONException | IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    private static String convertStackTraceToString(Throwable throwable) {
        try (StringWriter sw = new StringWriter();
             PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        } catch (IOException ioe) {
            throw new IllegalStateException(ioe);
        }
    }

}
