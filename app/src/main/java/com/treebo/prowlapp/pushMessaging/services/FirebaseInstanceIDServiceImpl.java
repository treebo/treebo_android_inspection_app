package com.treebo.prowlapp.pushMessaging.services;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

/**
 * Created by aa on 12/08/16.
 */
public class FirebaseInstanceIDServiceImpl extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        //set isDeviceRegistered to false so that new device id can be posted.
        LoginSharedPrefManager.getInstance().setIsDeviceRegistered(false);
        startService(new Intent(this, DeviceRegistrationService.class));
    }
}