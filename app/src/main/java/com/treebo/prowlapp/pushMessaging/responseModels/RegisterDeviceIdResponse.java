package com.treebo.prowlapp.pushMessaging.responseModels;

/**
 * Created by aa on 12/08/16.
 */
public class RegisterDeviceIdResponse {

    public String status;

    public Data data;

    public static class Data {
        String message;
    }
}