package com.treebo.prowlapp.pushMessaging.services;

import android.app.IntentService;
import android.content.Intent;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.Utils.AppUpdateHelperV2;

import javax.inject.Inject;

public class AppUpdateService extends IntentService {

    @Inject
    MixpanelAPI mixpanelAPI;

    public AppUpdateService() {
        super(AppUpdateService.class.getName());
        MainApplication.get().getApplicationComponent().injectAppUpdateService(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        AppUpdateHelperV2.getInstance().updateApp(mixpanelAPI);
    }
}