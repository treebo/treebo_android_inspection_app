package com.treebo.prowlapp.pushMessaging.services;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AppUpdateEvent;
import com.treebo.prowlapp.events.AppUpdatedEvent;
import com.treebo.prowlapp.job.OfflineJobUtils;
import com.treebo.prowlapp.Models.HotelDeactivate;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.response.common.SystemPropertyResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.AppUpdateHelperV2;

import java.util.Map;

import javax.inject.Inject;


/**
 * Created by aa on 12/08/16.
 */
public class FirebaseMessagingServiceImpl extends FirebaseMessagingService {

    @Inject
    public RxBus mRxBus;

    @Inject
    public LoginSharedPrefManager sSharedPrefManager;

    public static final String KEY_UPDATE = "update";

    private static final String KEY_HOTEL_ADD = "hotel_add";
    private static final String KEY_HOTEL_UPDATE = "hotel_update";
    private static final String KEY_HOTEL_DEACTIVATE = "hotel_deactivate";
    private static final String KEY_APP_UPDATE_LINK = "link";
    private static final String KEY_APP_SERVER_VERSION = "version";
    private static final String KEY_CHECKLIST_UPDATE = "checklist_update";
    private static final String KEY_SYSTEM_PROPERTIES = "system_properties";
    private static final String KEY_BANK_LIST_UPDATE = "bank_update";
    private static final String KEY_PERMISSIONS_UPDATE = "roles_update";

    @Override
    public void onCreate() {
        super.onCreate();
        MainApplication.get().getApplicationComponent().injectFirebaseService(this);
    }

    //this method runs on background thread
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage != null && remoteMessage.getData() != null) {
            if (remoteMessage.getData().containsKey(KEY_UPDATE)) {
                handleAppUpdate(this, remoteMessage.getData().get(KEY_UPDATE));
            } else if (remoteMessage.getData().containsKey(KEY_HOTEL_ADD)) {
                handleHotelUpdate(remoteMessage.getData().get(KEY_HOTEL_ADD));
            } else if (remoteMessage.getData().containsKey(KEY_HOTEL_UPDATE)) {
                handleHotelUpdate(remoteMessage.getData().get(KEY_HOTEL_UPDATE));
            } else if (remoteMessage.getData().containsKey(KEY_HOTEL_DEACTIVATE)) {
                handleHotelDeactivate(remoteMessage.getData().get(KEY_HOTEL_DEACTIVATE));
            } else if (remoteMessage.getData().containsKey(KEY_CHECKLIST_UPDATE)) {
                handleUpdateCheckPoint();
            } else if (remoteMessage.getData().containsKey(KEY_SYSTEM_PROPERTIES)) {
                handleSettingsUpdate(remoteMessage.getData().get(KEY_SYSTEM_PROPERTIES));
            } else if (remoteMessage.getData().containsKey(KEY_BANK_LIST_UPDATE)) {
                handleBankList();
            } else if (remoteMessage.getData().containsKey(KEY_PERMISSIONS_UPDATE)) {
                handlePermissionsUpdate();
            }
        }
    }

    private void handleSettingsUpdate(String jsonDataPayload) {
        SystemPropertyResponse.SystemProperty systemProperty = new GsonBuilder().create().fromJson(jsonDataPayload,
                new TypeToken<SystemPropertyResponse.SystemProperty>() {
                }.getType());

        sSharedPrefManager.setLocationAccuracy(systemProperty.getAccuracy());
        sSharedPrefManager.setHouseCountToolTipText(systemProperty.getHouseCountTooltipText());
        sSharedPrefManager.setPriceMismatchToolTipText(systemProperty.getPriceMismatchTooltipText());
        sSharedPrefManager.setIsIssueCreationEnabled(systemProperty.getIsIssueCreationEnabled());
        if (systemProperty.getMasterCheckListVersion() != sSharedPrefManager.getMasterCheckListVersion()) {
            OfflineJobUtils.fetchMasterList();
            sSharedPrefManager.setMasterCheckListVersion(systemProperty.getMasterCheckListVersion());
        }
        sSharedPrefManager.setServerAppVersion(systemProperty.getAppVersion());
        if (systemProperty.getAppVersion() > BuildConfig.VERSION_CODE) {
            mRxBus.postEvent(new AppUpdateEvent());
        } else {
            mRxBus.postEvent(new AppUpdatedEvent());
        }
        Log.d("System Properties Push", "App Version: " + systemProperty.getAppVersion());
        Log.d("firebase", "Settings Update  " + jsonDataPayload);
    }

    private static void handleHotelUpdate(String jsonDataPayload) {
        HotelLocation hotelLocation = new GsonBuilder().create().fromJson(jsonDataPayload, new TypeToken<HotelLocation>() {
        }.getType());
        HotelLocation saveHotel = HotelLocation.getHotelLocationById(hotelLocation.mHotelId);
        if (saveHotel == null) {
            hotelLocation.save();
        } else {
            hotelLocation.update();
        }
        Log.d("firebase", "saved hotel  " + hotelLocation.mHotelName);
    }

    private static void handleHotelDeactivate(String jsonDataPayload) {
        HotelDeactivate hotelDeactivate = new GsonBuilder().create().fromJson(jsonDataPayload, new TypeToken<HotelDeactivate>() {
        }.getType());
        HotelLocation.deleteHotelLocationById(hotelDeactivate.mHotelId);
        Log.d("firebase", "deleted hotel  " + hotelDeactivate.mHotelId);
    }

    private static void handleUpdateCheckPoint() {
        OfflineJobUtils.fetchMasterList();
        OfflineJobUtils.fetchAuditList();
        Log.d("firebase", "update checkpoint  ");
    }

    private static void handleBankList() {
        OfflineJobUtils.fetchBankList();
        Log.d("firebase", "bank list update");
    }

    private static void handlePermissionsUpdate() {
        OfflineJobUtils.fetchUserPermissions();
        Log.d("firebase", "User permissions update");
    }


    public static void handleAppUpdate(Context context, String jsonDataPayload) {

        //parse the data and start service to update
        try {
            Map<String, String> updateInfo = new GsonBuilder().create().fromJson(jsonDataPayload,
                    new TypeToken<Map<String, String>>() {
                    }.getType());

            if (updateInfo.containsKey(KEY_APP_UPDATE_LINK) && updateInfo.containsKey(KEY_APP_SERVER_VERSION)) {
                //get update url
                String appUpdateUrl = updateInfo.get(KEY_APP_UPDATE_LINK);

                //get server app version
                long serverAppVersion = Long.parseLong(updateInfo.get(KEY_APP_SERVER_VERSION));

                //save app update url
                AppUpdateHelperV2.getInstance().setAppUpdateUrl(appUpdateUrl);

                //save server app version
                AppUpdateHelperV2.getInstance().setServerAppVersion(serverAppVersion);

                //start update app service
                context.startService(new Intent(context, AppUpdateService.class));
            }
        } catch (JsonSyntaxException e) {
            Log.e(FirebaseMessagingServiceImpl.class.getName(), e.getMessage());
        }
    }
}
