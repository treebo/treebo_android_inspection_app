package com.treebo.prowlapp.pushMessaging.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.treebo.prowlapp.pushMessaging.services.AppUpdateService;
import com.treebo.prowlapp.Utils.AppUpdateHelperV2;

/**
 * Created by aa on 12/08/16.
 */
public class DownloadCompleteReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(DownloadCompleteReceiver.class.getName(), "Download complete receiver is called");

        //make is update downloaded false for compatibility with AppUpdateHelper class
        //TODO: need to discuss
        AppUpdateHelperV2.getInstance().setUpdateDownloaded(true);

        context.startService(new Intent(context, AppUpdateService.class));
    }
}