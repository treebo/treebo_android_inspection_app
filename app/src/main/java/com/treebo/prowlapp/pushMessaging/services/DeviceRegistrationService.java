package com.treebo.prowlapp.pushMessaging.services;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.pushMessaging.responseModels.RegisterDeviceIdResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.io.IOException;

import retrofit2.Call;

/**
 * Created by aa on 15/08/16.
 */
public class DeviceRegistrationService extends IntentService {

    private static final String TAG = DeviceRegistrationService.class.getName();

    public DeviceRegistrationService() {
        super(DeviceRegistrationService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String token = FirebaseInstanceId.getInstance().getToken();

        if (!TextUtils.isEmpty(token)) {

            Call<RegisterDeviceIdResponse> registerDeviceIdResponseCall = new RestClient().registerDeviceId(token);
            try {
                //execute call
                Log.d("token",token);
                RegisterDeviceIdResponse registerDeviceIdResponse = registerDeviceIdResponseCall.execute().body();

                if (registerDeviceIdResponse != null && "success".equals(registerDeviceIdResponse.status)) {
                    LoginSharedPrefManager.getInstance().setDeviceId(token);
                    LoginSharedPrefManager.getInstance().setIsDeviceRegistered(true);
                } else {
                    Log.d(TAG, " call execute register device id failure");
                }

            } catch (IOException e) {
                Log.d(TAG, " call execute IOException in registering device id");
            }
        } else {
            Log.d(TAG, "Empty Device token");
        }
    }
}
