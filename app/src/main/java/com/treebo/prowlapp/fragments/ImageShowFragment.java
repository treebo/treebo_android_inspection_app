package com.treebo.prowlapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.treebo.prowlapp.R;

import androidx.annotation.Nullable;

/**
 * Created by devesh on 13/05/16.
 */
public class ImageShowFragment extends BaseFragment {
    private static final String IMAGE_POSITION = "image_position" ;
    private static final String TOTAL_IMAGES= "total_images" ;
    public static final String IMAGE_URL_KEY = "key_image_url";

    private int imagePosition = -1;
    private int totalImages =-1;
    private String mImageUrl = null;
    private View mRootView ;
    private ImageView mImageViewSlide;
    private Context mContext;
    private ImageView crossImage;


    public static ImageShowFragment newInstance(int pos , int total, String imageUrl){
        Bundle bundle = new Bundle();
        bundle.putString(IMAGE_URL_KEY, imageUrl);
        bundle.putInt(IMAGE_POSITION, pos);
        bundle.putInt(TOTAL_IMAGES, total);
        ImageShowFragment fragment = new ImageShowFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            if(getArguments().getInt(IMAGE_POSITION)>=0 ) {
                imagePosition = getArguments().getInt(IMAGE_POSITION);
            }
            if(getArguments().getInt(TOTAL_IMAGES)>=0 ) {
                totalImages = getArguments().getInt(TOTAL_IMAGES);
            }
            if(getArguments().getString(IMAGE_URL_KEY)!= null ) {
                mImageUrl = getArguments().getString(IMAGE_URL_KEY);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.image_show, container, false);
        mImageViewSlide =(ImageView) mRootView.findViewById(R.id.image_view_slide);
        crossImage = (ImageView) mRootView.findViewById(R.id.cross);
        crossImage.setOnClickListener(v -> getActivity().onBackPressed());
        mContext = container.getContext();
        Glide.with(mContext).load(mImageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL).crossFade().into(mImageViewSlide);
        return mRootView;
    }
}
