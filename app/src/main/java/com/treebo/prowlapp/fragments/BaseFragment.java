package com.treebo.prowlapp.fragments;


import androidx.fragment.app.Fragment;

/**
 * Created by devesh on 15/03/16.
 */
public class BaseFragment extends Fragment {

    public String TAG;

    public BaseFragment() {
        super();
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

}
