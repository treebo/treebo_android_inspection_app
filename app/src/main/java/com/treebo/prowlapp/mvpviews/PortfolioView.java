package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.response.PortfolioResponse;

/**
 * Created by devesh on 05/05/16.
 */
public interface PortfolioView extends BaseView {
    void onPortfolioSuccess(PortfolioResponse response);
    void onPortfolioFailed(String msg);
}
