package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.response.HotelIncentivesResponse;

/**
 * Created by devesh on 29/04/16.
 */
public interface IncentivesView extends BaseView {
    void onIncentivesLoaded(HotelIncentivesResponse hotelIncentivesResponse);
}
