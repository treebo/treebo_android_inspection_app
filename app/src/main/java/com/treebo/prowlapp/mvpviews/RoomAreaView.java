package com.treebo.prowlapp.mvpviews;

import android.util.Pair;

import com.treebo.prowlapp.Models.RoomTypeList;

import java.util.ArrayList;

/**
 * Created by sumandas on 18/08/2016.
 */
public interface RoomAreaView extends BaseView {
    void onLoadHotelRooms(ArrayList<Pair<String, String>> scoreList, ArrayList<RoomTypeList> rooms);
    void onLoadHotelRoomsFailed(String msg);
}
