package com.treebo.prowlapp.mvpviews;


import com.treebo.prowlapp.response.ScoresResponse;

/**
 * Created by devesh on 30/04/16.
 */
public interface HotelScoresView extends BaseView {

    void onScoresLoaded(ScoresResponse response);
}
