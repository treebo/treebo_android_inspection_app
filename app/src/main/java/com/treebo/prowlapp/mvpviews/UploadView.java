package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.response.UpdateIssueResponse;

/**
 * Created by sumandas on 01/05/2016.
 */
public interface UploadView extends BaseView {
    void onUploadIssuesDone(UpdateIssueResponse response);
    void onUploadAuditDone(UpdateIssueResponse response);
}
