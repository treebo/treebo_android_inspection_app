package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.Models.AppVersionModel;

/**
 * Created by sumandas on 20/05/2016.
 */
public interface UpdateAppView extends BaseView{
     void onAppUpdate(AppVersionModel appVersion);
}
