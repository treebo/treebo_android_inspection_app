package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.usecase.UseCase;

/**
 * Created by devesh on 19/04/16.
 */
public interface BaseView {

    void setUpViewAsPerPermissions();

    void showLoading();

    void hideLoading();

    void showRetry();

    void hideRetry();

    boolean showError(String errorMessage);

    void closeAndNavigateBack(String message);

    BaseActivity getBaseActivity();

    void showEmptyView();

    void hideEmptyView();

    void showServerErrorAlertDialog(UseCase useCase);

}
