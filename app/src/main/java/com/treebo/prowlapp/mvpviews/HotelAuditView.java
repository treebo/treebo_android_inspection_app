package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.response.FormDataResponse;

/**
 * Created by sumandas on 28/04/2016.
 */
public interface HotelAuditView extends RoomAreaView {
    void onLoadHotelAuditAreas(FormDataResponse response);
    void onLoadHotelAuditAreaFailed(String msg);

}
