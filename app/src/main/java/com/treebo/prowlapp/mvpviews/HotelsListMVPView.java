package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.response.HotelsListResponse;
import com.treebo.prowlapp.response.login.LogoutResponse;

/**
 * Created by devesh on 03/05/16.
 */
public interface HotelsListMVPView extends BaseView {

    void onHotelsListSuccess(HotelsListResponse response);

    void onHotelsListFailed(String msg);

    void onLogoutSuccess(LogoutResponse response);

    void onLogoutFailure(LogoutResponse response);

}
