package com.treebo.prowlapp.mvpviews;

/**
 * Created by devesh on 02/05/16.
 */
public interface SendStaffView extends BaseView {

    void onStaffAdded(boolean isError);

    void onStaffAddFailure(String msg);
}
