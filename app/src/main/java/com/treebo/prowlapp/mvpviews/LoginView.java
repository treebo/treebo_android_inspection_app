package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.response.RootCauseResponse;
import com.treebo.prowlapp.response.login.LoginResponse;

/**
 * Created by devesh on 26/04/16.
 */
public interface LoginView extends BaseView {

    void onLoginSuccess(LoginResponse response);

    void onLoginFailure(String msg);

    void onOTPChangeSuccess(String successMsg);

    void onOTPChangeFailure(String message, String phone);

    void onRootCausesFetched(RootCauseResponse rootCause);

}
