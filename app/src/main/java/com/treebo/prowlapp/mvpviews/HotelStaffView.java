package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.Models.StaffModel;

import java.util.List;

/**
 * Created by devesh on 28/04/16.
 */
public interface HotelStaffView extends BaseView {
    void onStaffLoaded(List<StaffModel> staffModelList);
    void onStaffLoadFailure(String msg);
}
