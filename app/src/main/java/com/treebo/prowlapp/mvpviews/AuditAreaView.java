package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.response.audit.SubmitAuditResponse;

/**
 * Created by sumandas on 03/05/2016.
 */
public interface AuditAreaView extends RoomAreaView {
    //void onAuditSubmitted(SubmitAuditResponse response);
    void onAuditSubmitSuccess(SubmitAuditResponse response);
    void onAuditSubmitFailed(SubmitAuditResponse response);
}
