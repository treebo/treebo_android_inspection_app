package com.treebo.prowlapp.mvpviews;

/**
 * Created by devesh on 03/05/16.
 */
public interface UpdateStaffView extends BaseView {
    void onUpdateSuccess();

    void onUpdateFailed();

}
