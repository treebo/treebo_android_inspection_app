package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.data.IndividualEntry;

/**
 * Created by sumandas on 03/07/2016.
 */
public interface MarkIncentivePaidView extends BaseView{
    void onMarkIncentiveDone(IndividualEntry entry);
}
