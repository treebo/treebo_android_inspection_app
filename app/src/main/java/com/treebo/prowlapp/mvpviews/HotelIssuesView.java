package com.treebo.prowlapp.mvpviews;

import com.treebo.prowlapp.Models.IssuesModel;

import java.util.List;

/**
 * Created by sumandas on 30/04/2016.
 */
public interface HotelIssuesView extends BaseView {

    void onIssuesLoaded(List<IssuesModel> issuesList);
}
