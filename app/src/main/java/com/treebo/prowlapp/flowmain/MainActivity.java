package com.treebo.prowlapp.flowmain;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.pushMessaging.services.DeviceRegistrationService;
import com.treebo.prowlapp.pushMessaging.services.FirebaseMessagingServiceImpl;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.MixPanelManager;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    protected static final String TAG = "MainActivity";

    @Inject
    LoginSharedPrefManager mLoginManager;

    @Inject
    MixpanelAPI mMixpanelAPI;

    @Inject
    Navigator mNavigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.activity_splash_screen);

        MainComponent mainComponent = DaggerMainComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();

        mainComponent.injectMainActivity(this);


        //if intent contains extras from push notification then handle that
        //if user clicks a tray notification it opens start activity with data payload
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //handle app update
            if (extras.containsKey(FirebaseMessagingServiceImpl.KEY_UPDATE)) {
                FirebaseMessagingServiceImpl.handleAppUpdate(this, extras.getString(FirebaseMessagingServiceImpl.KEY_UPDATE));
            }
        }


        //if device id is not registered then register device id otherwise check if new app update is available
        if (!mLoginManager.isDeviceRegistered()) {
            startService(new Intent(this, DeviceRegistrationService.class));
        }

        if (mLoginManager.isUserLoggedIn()) {
            String emailId = mLoginManager.getEmailId();
            int userId = mLoginManager.getUserId();
            MixPanelManager.registerUserNameEmail(mMixpanelAPI, userId, emailId);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mLoginManager.getServerAppVersion() > BuildConfig.VERSION_CODE) {
            mNavigator.navigateToBlockerScreen(this);
        } else {
            navigateToLoginOrDashboard(mLoginManager.isUserLoggedIn());
        }
    }

    public void navigateToLoginOrDashboard(boolean isLoggedIn) {
        if (isLoggedIn) {
            mNavigator.navigateToSplashActivity(this);
        } else {
            mNavigator.navigateToLoginActivity(this);
        }
        finish();
    }


}
