package com.treebo.prowlapp.flowmain;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;

import dagger.Component;

/**
 * Created by sumandas on 19/08/2016.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class})
public interface MainComponent {
    void injectMainActivity(MainActivity mainActivity);
}
