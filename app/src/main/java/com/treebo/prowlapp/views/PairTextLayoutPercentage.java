package com.treebo.prowlapp.views;

import android.content.Context;
import android.util.AttributeSet;

import com.treebo.prowlapp.Models.ScoresData;
import com.treebo.prowlapp.Utils.Utils;

/**
 * Created by devesh on 05/05/16.
 */
public class PairTextLayoutPercentage extends PairTextLayout {


    public PairTextLayoutPercentage(Context context) {
        super(context);
    }

    public PairTextLayoutPercentage(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PairTextLayoutPercentage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setScoreData(ScoresData data) {
        this.text1 = String.valueOf((int)data.mValue);
        this.text2 = data.mName;
        textView1.setText(text1 + "%");
        textView1.setTextColor(getResources().getColor(Utils.getFrontOfficeScoreValue((int)data.mValue)));
        textView2.setText(text2);
    }

    public void setTextViewColor(int colorRes){
        textView1.setTextColor(getResources().getColor(colorRes));

    }
}
