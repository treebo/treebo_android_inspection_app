package com.treebo.prowlapp.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;


import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.TypeFaceUtils;

import androidx.appcompat.widget.AppCompatButton;

/**
 * Created by abhisheknair on 17/10/16.
 */

public class TreeboButton extends AppCompatButton {

    public TreeboButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public TreeboButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TreeboTextView);
        String customFont = typedArray.getString(R.styleable.TreeboTextView_custom_font);
        if(!TextUtils.isEmpty(customFont)) {
            try {
                setTypeface(TypeFaceUtils.get(context, customFont));
            } catch (Exception e) {

            }
        }
        typedArray.recycle();
    }
}