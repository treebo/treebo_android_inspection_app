package com.treebo.prowlapp.views;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tokenautocomplete.TokenCompleteTextView;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;

/**
 * Created by abhisheknair on 03/05/17.
 */

public class UserAutoCompleteView extends TokenCompleteTextView<EscalationUserListResponse.EscalationUser> {


    public UserAutoCompleteView(Context context) {
        super(context);
    }

    public UserAutoCompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UserAutoCompleteView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected View getViewForObject(EscalationUserListResponse.EscalationUser object) {
        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        TokenTextView textView = (TokenTextView) l.inflate(R.layout.item_email_chip, (ViewGroup) getParent(), false);
        textView.setText(object.getFirstName() + " " + object.getLastName());
        return textView;
    }

    @Override
    protected EscalationUserListResponse.EscalationUser defaultObject(String completionText) {
        return new EscalationUserListResponse.EscalationUser(completionText);
    }
}
