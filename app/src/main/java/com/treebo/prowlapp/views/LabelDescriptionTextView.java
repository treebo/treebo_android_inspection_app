package com.treebo.prowlapp.views;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.treebo.prowlapp.R;

/**
 * Created by aa on 12/07/16.
 */
public class LabelDescriptionTextView extends TextView {

    private String mLabel = "";
    private String mDescription = "";

    private int mLabelColor = getResources().getColor(R.color.black_212121);
    private int mDescriptionColor = getResources().getColor(R.color.warm_grey_2);

    private float mLabelTextSizeSp = 16.0f;
    private float mDescriptionTextSizeSp = 12.0f;

    public LabelDescriptionTextView(Context context) {
        super(context);
    }

    public LabelDescriptionTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LabelDescriptionTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setLabel(String label) {
        mLabel = label;
        setText();
    }

    public void setDescription(String description) {
        mDescription = description;
        setText();
    }

    public void setLabelAndDescription(String label, String description) {
        mLabel = label;
        mDescription = description;
        setText();
    }

    public void setLabelColor(int labelColor) {
        mLabelColor = labelColor;
        setText();
    }

    public void setDescriptionColor(int descriptionColor) {
        mDescriptionColor = descriptionColor;
        setText();
    }


    public void setLabelTextSizeSp(int labelTextSizeSp) {
        mLabelTextSizeSp = labelTextSizeSp;
        setText();
    }

    public void setDescriptionTextSizeSp(int descriptionTextSizeSp) {
        mDescriptionTextSizeSp = descriptionTextSizeSp;
        setText();
    }

    private void setText() {
        setTextColor(mLabelColor);
        setTextSize(TypedValue.COMPLEX_UNIT_SP, mLabelTextSizeSp);

        if (TextUtils.isEmpty(mDescription)) {
            setText(mLabel);
        } else {
            Spannable spannable = new SpannableString(String.format("%s\n%s", mLabel, mDescription));

            //set color span
            spannable.setSpan(new ForegroundColorSpan(mDescriptionColor),
                    mLabel.length() + 1,
                    mLabel.length() + mDescription.length() + 1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            //set size span
            spannable.setSpan(new RelativeSizeSpan(mDescriptionTextSizeSp / mLabelTextSizeSp),
                    mLabel.length() + 1,
                    mLabel.length() + mDescription.length() + 1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            setText(spannable, TextView.BufferType.SPANNABLE);
        }
    }
}
