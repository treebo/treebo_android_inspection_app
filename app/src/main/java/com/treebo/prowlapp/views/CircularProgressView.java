package com.treebo.prowlapp.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.Utils;

/**
 * Created by devesh on 28/03/16.
 */
public class CircularProgressView extends View {

    // Properties
    private float progress = 0;
    private float strokeWidth = getResources().getDimension(R.dimen.default_stroke_width);
    private float backgroundStrokeWidth = getResources().getDimension(R.dimen.default_background_stroke_width);
    private int color = Color.BLACK;
    private int backgroundColor = Color.GRAY;
    // Object used to draw
    private int startAngle = -90;
    private RectF rectF;
    private Paint backgroundPaint;
    private Paint foregroundPaint;
    private int scorePercentage;
    private int mMargin;
    //private TextPaint mTextPaint;
    //private TextPaint mTextPaint2;
    private int mTextSize = 100;
    private int mRectSize;
    private Context mContext;
    public String subheading;
    private boolean isPercentage = true;

    private boolean showTextView = true;

    private float value_x;
    private float value_y;

/*

    public void setCenteredTextMetrics(){
        Paint.FontMetrics metrics = mTextPaint.getFontMetrics();
        float height = Math.abs(metrics.top - metrics.bottom);
        value_x = getWidth() / 2;
        value_y = (getHeight() / 2) + (height / 2);
    }
*/


    //region Constructor & Init Method
    public CircularProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init(context, attrs);
    }



    public void setIsPercentage(boolean value){
        this.isPercentage = value;
    }

    public void setShowTextViewValue(boolean showText){
        showTextView = showText;
    }

    private void init(Context context, AttributeSet attrs) {
        rectF = new RectF();
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircularProgressView, 0, 0);
        //Reading values from the XML layout
        try {
            // Value
            progress = typedArray.getFloat(R.styleable.CircularProgressView_cpv_progress, progress);
            // StrokeWidth
            strokeWidth = typedArray.getDimension(R.styleable.CircularProgressView_cpv_progressbar_width, strokeWidth);
            backgroundStrokeWidth = typedArray.getDimension(R.styleable.CircularProgressView_cpv_background_progressbar_width, backgroundStrokeWidth);
            // Color
            color = typedArray.getInt(R.styleable.CircularProgressView_cpv_progressbar_color, color);
            backgroundColor = typedArray.getInt(R.styleable.CircularProgressView_cpv_background_progressbar_color, backgroundColor);
            scorePercentage = typedArray.getInt(R.styleable.CircularProgressView_cpv_score_percentage , 0);
        } finally {
            typedArray.recycle();
        }

        // Init Background
        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(backgroundColor);
        backgroundPaint.setStyle(Paint.Style.STROKE);
        backgroundPaint.setStrokeWidth(backgroundStrokeWidth);

        // Init Foreground
        foregroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        foregroundPaint.setColor(color);
        foregroundPaint.setStyle(Paint.Style.STROKE);
        foregroundPaint.setStrokeWidth(strokeWidth);
        mMargin = context.getResources().getDimensionPixelSize(R.dimen.margin_cpv);
/*
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        mTextPaint = new TextPaint(Color.BLACK);
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setTypeface(typeface);
        mTextPaint.setAntiAlias(true);

        //Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        mTextPaint2 = new TextPaint(Color.BLACK);
        mTextPaint2.setTextSize(mTextSize);
        mTextPaint2.setTypeface(typeface);
        mTextPaint2.setAntiAlias(true);*/
    }
    //endregion

    //region Draw Method
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawOval(rectF, backgroundPaint);
        float angle = 360 * progress / 100;
        canvas.drawArc(rectF, startAngle, angle, false, foregroundPaint);
        int textVerticalOffset = mRectSize / 2 + mTextSize/2;
        int textVerticalOffsetNoPercent = mRectSize / 2 + 20;


        int textVerticalOffset2 = mRectSize / 2 - mTextSize/2;

        int textHorizontalOffset = mRectSize / 2 -mTextSize/2;
        int textHorizontalOffsetNoPercent = mRectSize / 2 -40 ;
/*
        if(isPercentage && showTextView){
        canvas.drawText(scorePercentage + "%", textHorizontalOffset, textVerticalOffset, mTextPaint);
        }else {
        canvas.drawText(scorePercentage+ "", textHorizontalOffsetNoPercent, textVerticalOffsetNoPercent, mTextPaint);

        }
        if(!TextUtils.isEmpty(subheading))
        canvas.drawText(subheading, textHorizontalOffset, textVerticalOffset2, mTextPaint2);*/

    }
    //endregion

    //region Mesure Method
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int min = Math.min(width, height);
        mRectSize = min;
        setMeasuredDimension(min, min);
        float highStroke = (strokeWidth > backgroundStrokeWidth) ? strokeWidth : backgroundStrokeWidth;
        rectF.set(0 + highStroke / 2, 0 + highStroke / 2, min - highStroke / 2, min - highStroke / 2);
    }
    //endregion

    //region Method Get/Set
    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = (progress <= 100) ? progress : 100;
        invalidate();
    }

    public void setSubheading(String sub){
        this.subheading = sub;
    }

    public void setScorePercentage(int scorePercentage){
        this.scorePercentage = scorePercentage;
      //  mTextPaint.setColor(Utils.getPercentageColor(mContext , scorePercentage));
      //  mTextPaint2.setColor(getResources().getColor(R.color.black_100));
        foregroundPaint.setColor(Utils.getPercentageColor(mContext , scorePercentage));
        invalidate();
    }
    public float getProgressBarWidth() {
        return strokeWidth;
    }

    public void setProgressBarWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        foregroundPaint.setStrokeWidth(strokeWidth);
        requestLayout();//Because it should recalculate its bounds
        invalidate();
    }

    public float getBackgroundProgressBarWidth() {
        return backgroundStrokeWidth;
    }

    public void setBackgroundProgressBarWidth(float backgroundStrokeWidth) {
        this.backgroundStrokeWidth = backgroundStrokeWidth;
        backgroundPaint.setStrokeWidth(backgroundStrokeWidth);
        requestLayout();//Because it should recalculate its bounds
        invalidate();
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        foregroundPaint.setColor(color);
        setBackgroundColor(color);
        invalidate();
        requestLayout();
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        backgroundPaint.setColor(backgroundColor);
        backgroundPaint.setAlpha(33);
        invalidate();
        requestLayout();
    }
    //endregion

    //region Other Method

    /**
     * Set the progress with an animation.
     * Note that the {@link ObjectAnimator} Class automatically set the progress
     * so don't call the {@link CircularProgressView#setProgress(float)} directly within this method.
     *
     * @param progress The progress it should animate to it.
     */
    public void setProgressWithAnimation(float progress) {
        setProgressWithAnimation(progress, 1500);
    }

    /**
     * Set the progress with an animation.
     * Note that the {@link ObjectAnimator} Class automatically set the progress
     * so don't call the {@link CircularProgressView#setProgress(float)} directly within this method.
     *
     * @param progress The progress it should animate to it.
     * @param duration The length of the animation, in milliseconds.
     */
    public void setProgressWithAnimation(float progress, int duration) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "progress", progress);
        objectAnimator.setDuration(duration);
        objectAnimator.setInterpolator(new DecelerateInterpolator());
        objectAnimator.start();
    }
    //endregion
}

