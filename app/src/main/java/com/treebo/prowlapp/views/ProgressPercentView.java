package com.treebo.prowlapp.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.treebo.prowlapp.R;

/**
 * Created by sumandas on 13/06/2016.
 */
public class ProgressPercentView extends View {
    private Context mContext;
    private Paint mCompletedPaint;
    private Paint mLeftPaint;
    private int mWidth;
    private int mHeight;
    public float mRemainingPercent;

    public ProgressPercentView(Context context) {
        this(context, null);
        mContext = context;
        init();
    }

    public ProgressPercentView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        mContext = context;
        init();
    }

    public ProgressPercentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        mCompletedPaint=new Paint();
        mLeftPaint=new Paint();
        mCompletedPaint.setColor(mContext.getResources().getColor(R.color.color_emerald));
        mLeftPaint.setColor(mContext.getResources().getColor(R.color.grey_400));
        mCompletedPaint.setAntiAlias(true);
        mCompletedPaint.setStyle(Paint.Style.STROKE);
        mLeftPaint.setAntiAlias(true);
        mLeftPaint.setStyle(Paint.Style.STROKE);
    }

    public void setProgressUpdate(float completePercent){
        mRemainingPercent =completePercent;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mHeight= getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        mWidth= getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        setMeasuredDimension(mWidth, mHeight);
        mCompletedPaint.setStrokeWidth(mHeight);
        mLeftPaint.setStrokeWidth(mHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(0, 0, (100-mRemainingPercent) * mWidth / 100.0f, 0, mCompletedPaint);
        canvas.drawLine((100-mRemainingPercent) * mWidth / 100.0f,0,mWidth,0,mLeftPaint);

    }
}
