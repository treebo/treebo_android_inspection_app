package com.treebo.prowlapp.views;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.StringUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sakshamdhawan on 26/04/16.
 */
public class PasscodeDialog extends Dialog implements BaseView {
    Context context;
    OnSendClicked onSendClicked;
    ProgressDialog progress;
    TextView inputTextView;
    TextView errorTextView;


    public PasscodeDialog(Context context, OnSendClicked onSendClicked) {
        super(context);
        this.context = context;
        this.onSendClicked = onSendClicked;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_passcode);
        inputTextView = (TextView) findViewById(R.id.input_text);
        errorTextView = (TextView) findViewById(R.id.error_text);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_send_passcode)
    void sendPasscode() {
        dismissDialogAndChangeLoginView();
    }

    void dismissDialogAndChangeLoginView() {


        if (isInputDataValid()) {
            onSendClicked.sendClicked(inputTextView.getText().toString());
            this.dismiss();
        }



    }

    @Override
    protected void onStart() {
        super.onStart();
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                inputTextView.requestFocus();
            }
        },200);

    }

    @Override
    public boolean onPreparePanel(int featureId, View view, Menu menu) {
        return super.onPreparePanel(featureId, view, menu);
    }

    boolean isInputDataValid() {
        if (TextUtils.isEmpty(inputTextView.getText())) {
            showErrorInDialog(context.getResources().getString(R.string.no_num_entered));
            return false;
        }

        if (!StringUtils.validatePhoneNumber(inputTextView.getText().toString())) {
            showErrorInDialog(context.getString(R.string.invalid_num_entered));
            return false;
        }

        return true;

    }


    public void showErrorInDialog(String errorMessage){

        errorTextView.setText(errorMessage);
        errorTextView.setVisibility(View.VISIBLE);

    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progress = new ProgressDialog(context);
        progress.setTitle("Loading");
        progress.setMessage("Please wait...");
        progress.show();
    }

    @Override
    public void hideLoading() {
        progress.dismiss();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        //None

    }

    public void setPhoneNumber(String phone) {
        inputTextView.setText(phone);

    }


    public interface OnSendClicked {
        void sendClicked(String validPhone);

        void sendInvalidClicked(String invalidPhone);

    }

}
