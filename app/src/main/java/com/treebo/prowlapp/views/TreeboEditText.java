package com.treebo.prowlapp.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;


import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.TypeFaceUtils;

/**
 * Created by abhisheknair on 10/11/16.
 */

public class TreeboEditText extends AppCompatEditText {

    public TreeboEditText(Context context) {
        super(context);
    }

    public TreeboEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
        initAttrs(context, attrs);
    }

    public TreeboEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context, attrs);
        initAttrs(context, attrs);
    }

    private void setCustomFont(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TreeboTextView);
        String customFont = typedArray.getString(R.styleable.TreeboTextView_custom_font);
        if (!TextUtils.isEmpty(customFont)) {
            try {
                setTypeface(TypeFaceUtils.get(context, customFont));
            } catch (Exception e) {

            }
        }
        typedArray.recycle();
    }

    void initAttrs(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray attributeArray = context.obtainStyledAttributes(attrs, R.styleable.TreeboTextView);

            Drawable drawableLeft = null;
            Drawable drawableRight = null;
            Drawable drawableBottom = null;
            Drawable drawableTop = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawableLeft = attributeArray.getDrawable(R.styleable.TreeboTextView_drawableLeftCompat);
                drawableRight = attributeArray.getDrawable(R.styleable.TreeboTextView_drawableRightCompat);
                drawableBottom = attributeArray.getDrawable(R.styleable.TreeboTextView_drawableBottomCompat);
                drawableTop = attributeArray.getDrawable(R.styleable.TreeboTextView_drawableTopCompat);
            } else {
                final int drawableLeftId = attributeArray.getResourceId(R.styleable.TreeboTextView_drawableLeftCompat, -1);
                final int drawableRightId = attributeArray.getResourceId(R.styleable.TreeboTextView_drawableRightCompat, -1);
                final int drawableBottomId = attributeArray.getResourceId(R.styleable.TreeboTextView_drawableBottomCompat, -1);
                final int drawableTopId = attributeArray.getResourceId(R.styleable.TreeboTextView_drawableTopCompat, -1);

                if (drawableLeftId != -1)
                    drawableLeft = ContextCompat.getDrawable(context, drawableLeftId);
                if (drawableRightId != -1)
                    drawableRight = ContextCompat.getDrawable(context, drawableRightId);
                if (drawableBottomId != -1)
                    drawableBottom = ContextCompat.getDrawable(context, drawableBottomId);
                if (drawableTopId != -1)
                    drawableTop = ContextCompat.getDrawable(context, drawableTopId);
            }
            setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);
            attributeArray.recycle();
        }
    }
}
