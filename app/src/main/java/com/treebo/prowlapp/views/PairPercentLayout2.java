package com.treebo.prowlapp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.ScoresData;
import com.treebo.prowlapp.Utils.Utils;

/**
 * Created by sumandas on 12/08/2016.
 */
public class PairPercentLayout2 extends LinearLayout {

    String text1;
    String text2;
    public TextView textView1;
    public TextView textView2;

    public PairPercentLayout2(Context context) {
        this(context,null);
    }

    public PairPercentLayout2(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public PairPercentLayout2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView =inflater.inflate(R.layout.layout_pair_text2, this, true);

        textView1 = (TextView) rootView.findViewById(R.id.score);
        textView2 = (TextView) rootView.findViewById(R.id.name);

        textView1.setTextColor(getResources().getColor(R.color.emerald));
        textView2.setTextColor(getResources().getColor(R.color.black_100));

    }

    public void setScoreData(ScoresData data) {
        this.text1 = String.valueOf((int)data.mValue);
        this.text2 = data.mName;
        textView1.setText(text1 + "%");
        textView1.setTextColor(getResources().getColor(Utils.getFrontOfficeScoreValue((int)data.mValue)));
        textView2.setText(text2);
    }

    public void setTextViewColor(int colorRes){
        textView1.setTextColor(getResources().getColor(colorRes));

    }
}
