package com.treebo.prowlapp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;

/**
 * Created by devesh on 01/05/16.
 */
public class OppPairLayoutStarred extends LinearLayout {
    String text1;
    String text2;
    TextView textView1;
    TextView textView2;
    Context mContext;

    public OppPairLayoutStarred(Context context) {
        this(context, null);
    }

    public OppPairLayoutStarred(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OppPairLayoutStarred(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        mContext = context;
    }

    private void init() {

        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);


        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.layout_opp_pair_star, this, true);

        textView1 = (TextView) rootView.findViewById(R.id.score);
        // textView1.setText(text1);
        textView2 = (TextView) rootView.findViewById(R.id.name);
        //textView2.setText(text2);

        textView1.setTextColor(getResources().getColor(R.color.emerald));
        textView2.setTextColor(getResources().getColor(R.color.black_100));


    }

    public void setData(String text1, String text2) {
        this.text1 =text1;
        this.text2 = text2;
        textView1.setText(text1);
        textView2.setText(text2);


    }

}
