package com.treebo.prowlapp.exception;

/**
 * Created by devesh on 18/02/16.
 */
public class UnknownException extends Exception{

    public UnknownException(String msg) {
        super(msg);
    }
}
