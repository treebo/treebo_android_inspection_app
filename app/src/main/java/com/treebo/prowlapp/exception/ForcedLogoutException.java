package com.treebo.prowlapp.exception;


/**
 * Created by devesh on 18/02/16.
 */
public class ForcedLogoutException extends ServerException {

    public ForcedLogoutException(String detailMessage) {
        super(detailMessage);
    }

}