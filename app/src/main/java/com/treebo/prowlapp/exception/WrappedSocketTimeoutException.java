package com.treebo.prowlapp.exception;

/**
 * Created by devesh on 07/05/16.
 */
public class WrappedSocketTimeoutException extends Exception {
    public WrappedSocketTimeoutException(Throwable t){
        super("Connection Timed out.Check Internet Settings.");
    }
}
