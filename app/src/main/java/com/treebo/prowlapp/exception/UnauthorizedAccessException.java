package com.treebo.prowlapp.exception;

/**
 * Created by devesh on 28/01/16.
 */
public class UnauthorizedAccessException  extends Exception{
    public UnauthorizedAccessException(String message){
        super(message);
    }
}
