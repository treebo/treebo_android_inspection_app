package com.treebo.prowlapp.exception;

/**
 * Created by devesh on 07/05/16.
 */
public class WrappedIOException extends Exception {
    public WrappedIOException(Throwable ex){
        super("No Internet Connection");
    }
}
