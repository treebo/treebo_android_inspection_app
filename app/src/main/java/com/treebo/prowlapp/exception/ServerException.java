package com.treebo.prowlapp.exception;

/**
 * Created by devesh on 18/02/16.
 */


/**
 */
public class ServerException extends Exception{

    public ServerException(String message){
        super(message);
    }

}
