package com.treebo.prowlapp.exception;

/**
 * Created by devesh on 02/02/16.
 */
public class RefreshTokenFailureException extends Exception{

    public RefreshTokenFailureException(String detailMessage) {
        super(detailMessage);
    }
}
