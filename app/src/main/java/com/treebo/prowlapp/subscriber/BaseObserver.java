package com.treebo.prowlapp.subscriber;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.treebo.prowlapp.apis.SlackPushErrorAPI;
import com.treebo.prowlapp.errors.NavigationalErrorCode;
import com.treebo.prowlapp.exception.ForcedLogoutException;
import com.treebo.prowlapp.exception.RefreshTokenFailureException;
import com.treebo.prowlapp.exception.UnauthorizedAccessException;
import com.treebo.prowlapp.exception.WrappedIOException;
import com.treebo.prowlapp.exception.WrappedSocketTimeoutException;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.net.RetrofitException;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.usecase.UseCase;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import rx.Subscriber;

/**
 * Created by devesh on 26/04/16.
 */
public abstract class BaseObserver<T> extends Subscriber<T> {

    protected Subscriber<T> mRetrySubscriber;
    protected BasePresenter mPresenter;
    protected UseCase mUseCase;

    public BaseObserver(BasePresenter presenter, UseCase useCase) {
        mPresenter = presenter;
        mUseCase = useCase;
    }

    public BasePresenter getPresenter() {
        return mPresenter;
    }

    public Subscriber<T> getRetrySubscriber() {
        return mRetrySubscriber;
    }

    @Override
    public void onError(Throwable e) {
        BaseView view = mPresenter.getView();

        if (view == null) {
            Log.e("BaseSubscriber", "Subscriber has a ref to null view");
            return;
        }

        view.hideEmptyView();
        view.hideLoading();
        view.showRetry();

        if (e instanceof UnauthorizedAccessException) {
            RxBus.getInstance().postEvent(new ForcedLogoutException(NavigationalErrorCode.FORCED_LOGOUT.getMessage()));
        } else if (e instanceof RefreshTokenFailureException) {
            RxBus.getInstance().postEvent(new ForcedLogoutException(NavigationalErrorCode.FORCED_LOGOUT.getMessage()));
        } else if (e instanceof ForcedLogoutException) {
            RxBus.getInstance().postEvent(new ForcedLogoutException(NavigationalErrorCode.FORCED_LOGOUT.getMessage()));
        } else if (e instanceof TimeoutException) {
            RxBus.getInstance().postEvent(new WrappedSocketTimeoutException(e));
            view.showServerErrorAlertDialog(mUseCase);
        } else if (e instanceof SocketTimeoutException) {
            RxBus.getInstance().postEvent(new WrappedSocketTimeoutException(e));
            view.showServerErrorAlertDialog(mUseCase);
        } else if (e instanceof IOException) {
            RxBus.getInstance().postEvent(new WrappedIOException(e));
            view.showServerErrorAlertDialog(mUseCase);
        } else if (e instanceof RetrofitException) {
            BaseResponse responseBody = null;
            Type type = new TypeToken<BaseResponse>() {
            }.getType();
            try {
                responseBody =
                        new Gson().fromJson(((RetrofitException) e).getResponse().errorBody().string(), type);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            view.showError(responseBody == null || TextUtils.isEmpty(responseBody.msg)
                    ? e.getMessage() : responseBody.msg);
            new SlackPushErrorAPI(((RetrofitException) e).getUrl(),responseBody == null || TextUtils.isEmpty(responseBody.msg)
                    ? e.getMessage() : responseBody.msg, e).execute();
        } else {
            view.showError(e.getMessage());
        }
    }
}
