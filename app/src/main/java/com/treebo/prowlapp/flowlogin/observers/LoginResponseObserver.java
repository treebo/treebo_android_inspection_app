package com.treebo.prowlapp.flowlogin.observers;

import com.treebo.prowlapp.flowlogin.LoginPresenter;
import com.treebo.prowlapp.flowlogin.usecase.LoginUseCase;
import com.treebo.prowlapp.mvpviews.LoginView;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by sumandas on 15/08/2016.
 */
@Deprecated
public class LoginResponseObserver extends BaseObserver<LoginResponse> {

    LoginUseCase mLoginUseCase;
    LoginPresenter mPresenter;
    LoginView mView;

    public LoginResponseObserver(LoginPresenter presenter, LoginUseCase loginUseCase, LoginView baseView) {
        super(presenter, loginUseCase);
        mPresenter=presenter;
        mLoginUseCase=loginUseCase;
        mView=baseView;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();

    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
    }

    @Override
    public void onNext(LoginResponse loginResponse) {
        if (loginResponse.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)) {
            mView.onLoginSuccess(loginResponse);
            mView.hideLoading();
        } else {
            mView.hideLoading();
            mView.onLoginFailure(loginResponse.msg);
        }
    }
}