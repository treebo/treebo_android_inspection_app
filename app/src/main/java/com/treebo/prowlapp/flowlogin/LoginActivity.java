package com.treebo.prowlapp.flowlogin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.widget.LinearLayout;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowhotelview.HotelsOverViewActivity;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.pushMessaging.services.AppUpdateService;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

@Deprecated
public class LoginActivity extends AppCompatActivity {

    public static final String INTENT_EXTRAS_UNAUTH_ERROR = "unauth_error";
    LinearLayout mLoginRoot;

    @Inject
    Navigator mNavigator;

    @Inject
    LoginSharedPrefManager mLoginSharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLoginComponent();

        if(mLoginSharedPrefManager.isUserLoggedIn()) {
            mNavigator.navigateToHotelsOverviewActivity(LoginActivity.this);
            finish();
            return;
        }
        setContentView(R.layout.activity_login);

        mLoginRoot = (LinearLayout) findViewById(R.id.login_root);

        //if device id is registered then check if new app update is available or not
        if (mLoginSharedPrefManager.isDeviceRegistered()) {
            //start service to check for app update
            startService(new Intent(this, AppUpdateService.class));
        }

        Intent intent = getIntent();
        if (intent != null) {
            boolean showError = intent.getBooleanExtra(INTENT_EXTRAS_UNAUTH_ERROR, false);
            if (showError) {
                SnackbarUtils.show(mLoginRoot, getString(R.string.error_logout));
            }
        }

        if (savedInstanceState == null) {
            addFragment(R.id.fragment_container,  LoginFragment.newInstance());
        }
    }

    public void initLoginComponent(){
        LoginComponent loginComponent= DaggerLoginComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .activityModule(new ActivityModule(this)).build();
        loginComponent.injectActivity(this);
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }


    protected void addFragment(int containerViewId, Fragment fragment) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commit();
    }

    public void startHotelsActivity() {
        Intent intent = new Intent(LoginActivity.this , HotelsOverViewActivity.class );
        startActivity(intent);

    }
}
