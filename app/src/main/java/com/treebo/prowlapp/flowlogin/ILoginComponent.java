package com.treebo.prowlapp.flowlogin;

/**
 * Created by sumandas on 25/08/2016.
 */
@Deprecated
public interface ILoginComponent {
    void injectFragment(LoginFragment loginfragment);
    void injectActivity(LoginActivity loginActivity);
}
