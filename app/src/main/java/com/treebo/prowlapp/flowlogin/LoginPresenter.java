package com.treebo.prowlapp.flowlogin;


import com.treebo.prowlapp.flowlogin.observers.ChangeOTPObserver;
import com.treebo.prowlapp.flowlogin.observers.LoginResponseObserver;
import com.treebo.prowlapp.flowlogin.usecase.ChangeOTPUseCase;
import com.treebo.prowlapp.flowlogin.usecase.LoginUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.LoginView;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by devesh on 26/04/16.
 */
@Deprecated
public class LoginPresenter implements BasePresenter {

    LoginView mBaseView;

    public RestClient mRestClient;

    public LoginPresenter(RestClient restClient){
        mRestClient=restClient;
    }

    @Override
    public void setMvpView(BaseView baseView) {
        this.mBaseView = (LoginView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    public void attemptLogin(LoginUseCase loginUseCase, LoginResponseObserver loginResponseObserver) {
        hideRetry();
        showLoading();
        loginUseCase.execute(loginResponseObserver);
    }

    public void changeOtp(ChangeOTPUseCase changeOTPUseCase, ChangeOTPObserver changeOTPObserver) {
        hideRetry();
        showLoading();
        changeOTPUseCase.execute(changeOTPObserver);
    }

/*    public void callRootCausesApi(RootCauseUseCase rootCauseUseCase, FetchRootCausesObserver fetchRootCausesObserver){
        hideRetry();
        showLoading();
        rootCauseUseCase.execute(fetchRootCausesObserver);
    }*/

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mBaseView;
    }

    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }


}
