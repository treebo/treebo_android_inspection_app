package com.treebo.prowlapp.flowlogin;

import com.treebo.prowlapp.flowlogin.usecase.ChangeOTPUseCase;
import com.treebo.prowlapp.flowlogin.usecase.LoginUseCase;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 08/08/2016.
 */
@Deprecated
@Module
public class LoginModule {

    @Provides
    LoginPresenter providesLoginPresenter(RestClient restClient){
        return new LoginPresenter(restClient);
    }

    @Provides
    RestClient providesRestClient(){
        return new RestClient();
    }

    @Provides
    LoginUseCase providesLoginUseCase(RestClient restClient){
        return new LoginUseCase(restClient);
    }

    @Provides
    ChangeOTPUseCase providesChangeOTPUseCase(RestClient restClient){
        return new ChangeOTPUseCase(restClient);
    }

/*    @Provides
    RootCauseUseCase providesRootCauseUseCase(RestClient restClient){
        return new RootCauseUseCase(restClient);
    }*/


}
