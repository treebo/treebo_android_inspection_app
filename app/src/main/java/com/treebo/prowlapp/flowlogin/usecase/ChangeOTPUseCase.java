package com.treebo.prowlapp.flowlogin.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.ChangeOTPResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by devesh on 27/04/16.
 */
@Deprecated
public class ChangeOTPUseCase extends UseCase {

    private String mPhoneNo;
    private RestClient mRestClient;

    public ChangeOTPUseCase(RestClient restClient) {
        mRestClient=restClient;
    }

    public void setPhone(String phone){
        mPhoneNo=phone;
    }

    @Override
    protected Observable<ChangeOTPResponse> getObservable() {
        return mRestClient.changeOtp(mPhoneNo);
    }
}
