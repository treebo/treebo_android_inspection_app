package com.treebo.prowlapp.flowlogin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowlogin.observers.ChangeOTPObserver;
import com.treebo.prowlapp.flowlogin.observers.LoginResponseObserver;
import com.treebo.prowlapp.flowlogin.usecase.ChangeOTPUseCase;
import com.treebo.prowlapp.flowlogin.usecase.LoginUseCase;
import com.treebo.prowlapp.mvpviews.LoginView;
import com.treebo.prowlapp.response.RootCauseResponse;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.StringUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.PasscodeDialog;

import javax.inject.Inject;


import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Deprecated
public class LoginFragment extends Fragment implements LoginView {

    @Inject
    ProgressDialog progress;

    @BindView(R.id.text_view_forgot_passcode)
    TextView tvFrorgotPasscode;

    @BindView(R.id.email_sign_in_button)
    Button emailSignIn;

    @BindView(R.id.email)
    TextInputEditText mUsernameEditTextView;

    @BindView(R.id.passcode)
    TextInputEditText mPasscodeEditTextView;

    @BindView(R.id.email_text_input_layout)
    TextInputLayout mEmailTextInputLayout;

    @BindView(R.id.passcode_text_input_layout)
    TextInputLayout mPassTextInputLayout;

    String passEntered;

    private View mRootView;

    TextView mToolbarTitle;


    @Inject
    public LoginPresenter mLoginPresenter;

    PasscodeDialog passcodeDialog;

    @Inject
    public MixpanelAPI mMixpanelApi;

    @Inject
    public LoginUseCase mLoginUseCase;

    @Inject
    public ChangeOTPUseCase mChangeOTPUseCase;

/*    @Inject
    public RootCauseUseCase mRootCauseUseCase;*/



    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoginComponent loginComponent= DaggerLoginComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .activityModule(new ActivityModule(getActivity())).build();

        loginComponent.injectFragment(this);
    }

    @Inject
    public void setView(){
        mLoginPresenter.setMvpView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_login_fragment_new, container, false);
        ButterKnife.bind(this, mRootView);
        mToolbarTitle = (TextView) mRootView.findViewById(R.id.toolbar_title);

        mUsernameEditTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    mEmailTextInputLayout.setError(null);
                    mEmailTextInputLayout.setErrorEnabled(false);
                    mPassTextInputLayout.setError(null);
                    mPassTextInputLayout.setErrorEnabled(false);
                }
            }
        });

        mPasscodeEditTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    mPassTextInputLayout.setError(null);
                    mPassTextInputLayout.setErrorEnabled(false);
                    mEmailTextInputLayout.setError(null);
                    mEmailTextInputLayout.setErrorEnabled(false);
                }
            }
        });

        return mRootView;
    }

    /**
     * If editTextUsername is null or empty take the u
     */

    @OnClick(R.id.text_view_forgot_passcode)
    void showResendPasscodeDialog() {
        passcodeDialog = new PasscodeDialog(getActivity(), new PasscodeDialog.OnSendClicked() {
            @Override
            public void sendClicked(String validPhone) {
                mChangeOTPUseCase.setPhone(validPhone);
                ChangeOTPObserver changeOTPObserver=providesChangeOTPObserver(mLoginPresenter,mChangeOTPUseCase);
                changeOTPObserver.setPhone(validPhone);
                mLoginPresenter.changeOtp(mChangeOTPUseCase,changeOTPObserver);

            }

            @Override
            public void sendInvalidClicked(String invalidPhone) {
            }
        });
        passcodeDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        passcodeDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                com.treebo.prowlapp.Utils.Utilities.showKeyboard(getContext(), null);
            }
        });

        passcodeDialog.show();

    }

    @OnClick(R.id.text_view_resend_code)
    void resendCode() {
        showResendPasscodeDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        mToolbarTitle.setText(getResources().getString(R.string.title_activity_login));
        mLoginPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mLoginPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMixpanelApi.flush();
        mLoginPresenter.destroy();
        if (passcodeDialog != null) {
            passcodeDialog.dismiss();
            passcodeDialog = null;
        }

        if (mLoginUseCase != null) {
            mLoginUseCase.unsubscribe();
        }

        if (mChangeOTPUseCase != null) {
            mChangeOTPUseCase.unsubscribe();
        }
/*        if (mRootCauseUseCase != null) {
            mRootCauseUseCase.unsubscribe();
        }*/

    }

    @OnClick(R.id.email_sign_in_button)
    void signIn() {
        if (isDataValid()) {
            Utils.hideSoftKeyboard(mRootView);
            mLoginUseCase.setLoginFields(mUsernameEditTextView.getText().toString(), passEntered);
            LoginResponseObserver  loginResponseObserver=providesLoginResponseObserver(mLoginPresenter,mLoginUseCase);
            mLoginPresenter.attemptLogin(mLoginUseCase,loginResponseObserver);
            MixPanelManager.trackEvent(mMixpanelApi,MixPanelManager.SCREEN_LOGIN,MixPanelManager.EVENT_LOGIN_ATTEMPT);
        }

    }

    private boolean isDataValid() {

        passEntered = mPasscodeEditTextView.getText().toString();

        if (TextUtils.isEmpty(mUsernameEditTextView.getText().toString())) {
            mEmailTextInputLayout.setError(getResources().getString(R.string.no_email_entered));
            return false;
        }

        if (TextUtils.isEmpty(passEntered)) {
            mPassTextInputLayout.setError(getResources().getString(R.string.no_passcode_entered));
            return false;
        }

        if (!StringUtils.validateEmail(mUsernameEditTextView.getText().toString())) {
            mEmailTextInputLayout.setError(getResources().getString(R.string.wrong_email_entered));
            return false;
        }


        if (!StringUtils.validatePin(passEntered)) {
            mPassTextInputLayout.setError(getResources().getString(R.string.incoreect_pin_entered));
            return false;
        }

        return true;

    }


    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progress.setTitle("Loading");
        progress.setMessage("Please wait...");
        progress.setCancelable(true);
        progress.show();
    }

    @Override
    public void hideLoading() {
        if(progress!=null && !getActivity().isFinishing()){
            progress.dismiss();
        }
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        return true;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        if (useCase instanceof LoginUseCase) {
            AlertDialogUtils.showInternetDisconnectedDialog(getActivity(), ((dialog, which) -> {
                        if (useCase instanceof LoginUseCase) {
                            dialog.dismiss();
                            emailSignIn.performClick();
                        }
                    }),
                    (dialog1 -> {
                        dialog1.dismiss();
                    }));
        }

    }


    @Override
    public void onLoginSuccess(LoginResponse response) {
/*        MixPanelManager.registerUserNameEmail(mMixpanelApi,response.getUserId(),response.getEmailId());
        MixPanelManager.trackEvent(mMixpanelApi,MixPanelManager.EVENT_LOGIN_SUCCESS,MixPanelManager.SCREEN_LOGIN);
        Utils.persistAuthToken(response);
        FetchRootCausesObserver fetchRootCausesObserver=providesFetchRootCausesObserver(mLoginPresenter,mRootCauseUseCase);
        mLoginPresenter.callRootCausesApi(mRootCauseUseCase,fetchRootCausesObserver);*/

    }

    @Override
    public void onRootCausesFetched(RootCauseResponse rootCause) {
        AuditPreferenceManager.getInstance().persistRootCauseList(rootCause.data);
        ((LoginActivity) getActivity()).startHotelsActivity();
        getActivity().finish();
    }

    @Override
    public void onLoginFailure(String msg) {
        SnackbarUtils.show(mRootView, msg);
        MixPanelManager.trackEvent(mMixpanelApi,MixPanelManager.EVENT_LOGIN_FAILURE,MixPanelManager.SCREEN_LOGIN);
    }

    @Override
    public void onOTPChangeSuccess(String successMessage) {

        if (passcodeDialog != null && !getActivity().isFinishing()) {
            passcodeDialog.dismiss();
            passcodeDialog = null;
        }
        SnackbarUtils.show(mRootView, successMessage);
        mRootView.findViewById(R.id.linear_layout_passcode_sent).setVisibility(View.VISIBLE);
        mRootView.findViewById(R.id.text_view_forgot_passcode).setVisibility(View.GONE);

    }

    @Override
    public void onOTPChangeFailure(String message, String phone) {
        if (!getActivity().isFinishing()) {
            if ((passcodeDialog != null && !passcodeDialog.isShowing()) || passcodeDialog == null) {
                showErrorInDialog(message, phone);
            }
        }
    }

    public void showErrorInDialog(String message, String phone) {
        passcodeDialog = new PasscodeDialog(getActivity(), new PasscodeDialog.OnSendClicked() {
            @Override
            public void sendClicked(String validPhone) {
                mChangeOTPUseCase.setPhone(validPhone);
                ChangeOTPObserver changeOTPObserver=providesChangeOTPObserver(mLoginPresenter,mChangeOTPUseCase);
                changeOTPObserver.setPhone(validPhone);
                mLoginPresenter.changeOtp(mChangeOTPUseCase,changeOTPObserver);
            }

            @Override
            public void sendInvalidClicked(String invalidPhone) {
            }
        });
        passcodeDialog.show();
        passcodeDialog.setPhoneNumber(phone);
        passcodeDialog.showErrorInDialog(message);
    }



    LoginResponseObserver providesLoginResponseObserver(LoginPresenter loginPresenter,LoginUseCase loginUseCase){
        return new LoginResponseObserver(loginPresenter,loginUseCase,this);
    }


    ChangeOTPObserver providesChangeOTPObserver(LoginPresenter presenter, ChangeOTPUseCase otpUseCase){
        return new ChangeOTPObserver(presenter,otpUseCase,this);
    }


/*   FetchRootCausesObserver providesFetchRootCausesObserver(LoginPresenter presenter, RootCauseUseCase rootCauseUseCase){
        return new FetchRootCausesObserver(presenter,rootCauseUseCase,this);
    }*/

}
