package com.treebo.prowlapp.flowlogin;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;

import dagger.Component;

/**
 * Created by sumandas on 08/08/2016.
 */
@Deprecated
@PerActivity
@Component(dependencies = ApplicationComponent.class,modules
        = {ActivityModule.class,LoginModule.class})
public interface LoginComponent extends ILoginComponent{

}
