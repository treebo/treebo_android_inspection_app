package com.treebo.prowlapp.flowlogin.observers;

import com.treebo.prowlapp.flowOnboarding.OnBoardingContract;
import com.treebo.prowlapp.flowOnboarding.presenter.GoogleLoginPresenter;
import com.treebo.prowlapp.flowOnboarding.usecase.RootCauseUseCase;
import com.treebo.prowlapp.response.RootCauseResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;

/**
 * Created by sumandas on 15/08/2016.
 */
@Deprecated
public class FetchRootCausesObserver extends BaseObserver<RootCauseResponse> {

    RootCauseUseCase mUseCase;
    OnBoardingContract.IGoogleLoginView mView;


    public FetchRootCausesObserver(GoogleLoginPresenter presenter, RootCauseUseCase useCase,
                                   OnBoardingContract.IGoogleLoginView view) {
        super(presenter, useCase);
        mView=view;
        mUseCase=useCase;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
    }

    @Override
    public void onNext(RootCauseResponse rootCauseModel) {
        //mView.onRootCausesFetched(rootCauseModel);
        mView.hideLoading();
    }
}
