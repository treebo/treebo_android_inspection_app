package com.treebo.prowlapp.flowlogin.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by devesh on 27/04/16.
 */
@Deprecated
public class LoginUseCase extends UseCase<LoginResponse> {

    public String mUsername;
    public String mPassword;

    public RestClient mRestClient;

    public LoginUseCase(RestClient restClient) {
        mRestClient=restClient;

    }

    public void setLoginFields(String username, String password){
        mUsername = username;
        mPassword =password;
    }

    @Override
    protected Observable<LoginResponse> getObservable() {
        return mRestClient.attemptLogin(mUsername, mPassword);
    }
}
