package com.treebo.prowlapp.flowlogin.observers;

import com.treebo.prowlapp.flowlogin.LoginPresenter;
import com.treebo.prowlapp.flowlogin.usecase.ChangeOTPUseCase;
import com.treebo.prowlapp.mvpviews.LoginView;
import com.treebo.prowlapp.response.ChangeOTPResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by sumandas on 15/08/2016.
 */
@Deprecated
public class ChangeOTPObserver extends BaseObserver<ChangeOTPResponse> {

    ChangeOTPUseCase mChangeOTPUseCase;
    LoginView mView;
    String mPhone;


    public ChangeOTPObserver(LoginPresenter presenter, ChangeOTPUseCase otpUseCase, LoginView loginView) {
        super(presenter, otpUseCase);
        mChangeOTPUseCase=otpUseCase;
        mView=loginView;

    }

    public void setPhone(String phone){
        mPhone=phone;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
    }

    @Override
    public void onNext(ChangeOTPResponse changeOTPResponse) {
        if (changeOTPResponse.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)) {
            mView.onOTPChangeSuccess(changeOTPResponse.getMessage());
            mView.hideLoading();
        } else {
            mView.hideLoading();
            mView.onOTPChangeFailure(changeOTPResponse.msg,mPhone);
        }
    }
}
