package com.treebo.prowlapp.flowauditanyhotel.Module;

import com.treebo.prowlapp.flowauditanyhotel.model.AuditAnyActivityModel;

import dagger.Module;
import dagger.Provides;

@Module
public class AuditAnyHotelModelModule {
    @Provides
    public AuditAnyActivityModel provideModel() {
        return new AuditAnyActivityModel();
    }
}

