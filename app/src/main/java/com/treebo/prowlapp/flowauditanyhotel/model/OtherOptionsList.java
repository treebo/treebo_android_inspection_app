package com.treebo.prowlapp.flowauditanyhotel.model;

import android.content.Context;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.flowdashboard.periodicstate.BasePeriodicAuditCard;
import com.treebo.prowlapp.flowdashboard.periodicstate.PeriodicStateUtils;

import java.util.ArrayList;
import java.util.List;

public class OtherOptionsList {
    private int hotelId;
    private ArrayList<TaskModel> taskModels;;
    public OtherOptionsList(int hotelId, ArrayList<TaskModel> taskModels) {
        this.hotelId = hotelId;
        this.taskModels = taskModels;
    }

    public int getHotelId() {
        return hotelId;
    }

    public ArrayList<TaskModel> getTaskModels() {
        return taskModels;
    }

    public List<PickerItem> convertToPickerItemList(Context mContext)
    {
        List<PickerItem> lst = new ArrayList<>();
        for(int i=0;i<taskModels.size();i++) {
            if (taskModels.get(i).getTaskTypeString().equals("Periodic Audit")) {
                BasePeriodicAuditCard periodicAuditCard = PeriodicStateUtils.getCard(mContext , taskModels.get(i));
                int resId = periodicAuditCard.getAuditIcon();
                lst.add(new PickerItem(taskModels.get(i).getTaskID(), taskModels.get(i).getHeading(), resId));
            }
        }
        return lst;
    }

}
