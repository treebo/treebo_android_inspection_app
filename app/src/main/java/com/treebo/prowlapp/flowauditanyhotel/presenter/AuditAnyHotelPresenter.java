package com.treebo.prowlapp.flowauditanyhotel.presenter;
import android.content.Context;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.flowauditanyhotel.AuditAnyHotelContract;
import com.treebo.prowlapp.flowauditanyhotel.DaggerAuditAnyHotelModelComponent;
import com.treebo.prowlapp.flowauditanyhotel.model.AuditAnyActivityModel;
import com.treebo.prowlapp.flowauditanyhotel.model.OtherOptionsList;
import com.treebo.prowlapp.flowauditanyhotel.model.PickerItem;
import com.treebo.prowlapp.flowauditanyhotel.model.RoomList;
import com.treebo.prowlapp.flowauditanyhotel.model.Rooms;
import com.treebo.prowlapp.flowdashboard.periodicstate.BasePeriodicAuditCard;
import com.treebo.prowlapp.flowdashboard.periodicstate.PeriodicStateUtils;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.response.dashboard.HotelTasksResponse;
import java.util.ArrayList;
import java.util.List;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;
import rxAndroid.schedulers.AndroidSchedulers;

public class AuditAnyHotelPresenter implements AuditAnyHotelContract.IAuditAnyHotelPresenter {
    private static final String TAG = "AuditAnyHotelPresenter";

    public AuditAnyActivityModel viewModel;

    private AuditAnyHotelContract.IAuditAnyHotelView mView;

    public AuditAnyHotelPresenter(AuditAnyHotelContract.IAuditAnyHotelView mView) {
        viewModel = DaggerAuditAnyHotelModelComponent.builder().build().getModel();
        this.mView = mView;
    }

    @Override
    public void getHotels(){
        try {
            List<PickerItem> itemList= new ArrayList<>();
            List<HotelLocation> hotelList = HotelLocation.getAllHotels();
            for (HotelLocation hotel : hotelList)
                itemList.add(new PickerItem(hotel.mHotelId, hotel.mHotelName));

            mView.onHotelListLoadSuccess(itemList);
        }
        catch(Exception ex)
        {
            mView.onHotelListLoadError();
        }
    }

    @Override
    public void getHotelRooms(){

        PickerItem selectedHotel= viewModel.getSelectedHotel();

        if(selectedHotel==null || selectedHotel.getId()==-1) {
            mView.onRoomListLoadNoHotelSelected();
            return ;
        }
        int hotelId = selectedHotel.getId();

        RoomList roomList = viewModel.getRoomList();
        if(roomList!=null && roomList.getHotelId()==hotelId) {
            mView.onRoomListLoadSuccess(roomList.convertToPickerItemList());
            return ;
        }
        mView.showLoader();
        List<Rooms> lst = new ArrayList<>();
        Observable<RoomListResponse> hotelListObservable = new RestClient().getRoomList(hotelId);
        hotelListObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RoomListResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.hideLoader();
                        RoomList roomList = new RoomList(hotelId,lst);
                        viewModel.setRoomsList(roomList);
                        mView.onRoomListLoadSuccess(roomList.convertToPickerItemList());
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onRoomListLoadError();
                        mView.hideLoader();
                    }

                    @Override
                    public void onNext(RoomListResponse roomListResponse) {

                        ArrayList<RoomV3Object> obj= roomListResponse.getRoomDataObject().getRoomList();
                        for(RoomV3Object o: obj)
                            lst.add(new Rooms(o.id,o.roomNumber,o.roomType));
                    }
                });

    }

    @Override
    public void getOtherOptions(int userId){

        PickerItem selectedHotel= viewModel.getSelectedHotel();

        if(selectedHotel==null || selectedHotel.getId()==-1) {
            mView.onRoomListLoadNoHotelSelected();
            return ;
        }
        int hotelId = selectedHotel.getId();

        OtherOptionsList otherOptionsList = viewModel.getOtherOptionList();
        if(otherOptionsList!=null && otherOptionsList.getHotelId()==hotelId) {
            mView.onOtherListLoaded(otherOptionsList.convertToPickerItemList((Context)mView));
        }

    }

    @Override
    public void getCommonArea(){
        PickerItem selectedHotel= viewModel.getSelectedHotel();

        if(selectedHotel==null || selectedHotel.getId()==-1) {
            mView.onRoomListLoadNoHotelSelected();
            return ;
        }
        int hotelId = selectedHotel.getId();

        mView.showLoader();

        Observable<CommonAreaTypeResponse> commonAreaObservable = new RestClient().getAuditTypeCommon(hotelId);
        commonAreaObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CommonAreaTypeResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.hideLoader();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError("Something went Wrong");
                    }

                    @Override
                    public void onNext(CommonAreaTypeResponse commonAreaTypeResponse) {
                        viewModel.setmCommonAreaResponse(commonAreaTypeResponse);
                        mView.onCommonAreaResponseLoaded(commonAreaTypeResponse);
                    }
                });
    }

    public TaskModel getTaskModelForAudit(){
        ArrayList<TaskModel> taskModels = viewModel.getmMasterTaskList();

        for(int i=0;i<taskModels.size();i++) {
            if (taskModels.get(i).getTaskTypeString().equals("Audit")) {
                BasePeriodicAuditCard periodicAuditCard = PeriodicStateUtils.getCard((Context)mView , taskModels.get(i));
                int resId = periodicAuditCard.getAuditIcon();
                return taskModels.get(i);
            }
        }
        return null;
    }

    @Override
    public void getTaskList(int userId) {
        mView.showLoaderExternal();
        int hotelId = viewModel.getSelectedHotel().getId();
        Observable<HotelTasksResponse> hotelTaskObservable = new RestClient().getTaskListRemote(userId, hotelId);
        hotelTaskObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<HotelTasksResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.hideLoaderExternal();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError("Error Loading Tasks!!! Please Try Again.");
                        mView.hideLoaderExternal();
                    }

                    @Override
                    public void onNext(HotelTasksResponse hotelTasksResponse) {
                        OtherOptionsList otherOptionsList = new OtherOptionsList(hotelId,hotelTasksResponse.data.hotTasks);
                        viewModel.setOtherOptionList(otherOptionsList);
                        ArrayList<TaskModel> mMasterTaskLists = prepareMaterList(hotelTasksResponse.data);
                        viewModel.setmMasterTaskList(mMasterTaskLists);

                    }
                });
    }

    @Override
    public void setCurrentItemPicker(String currentItemPicker) {
        viewModel.setCurrentItemPicker(currentItemPicker);
    }

    @Override
    public String getCurrentItemPicker() {
        return viewModel.getCurrentItemPicker();
    }

    @Override
    public PickerItem getSelectedHotel() {
        return viewModel.getSelectedHotel();
    }

    @Override
    public void setSelectedHotel(PickerItem selectedHotel) {
        viewModel.setSelectedHotel(selectedHotel);
    }

    @Override
    public PickerItem getSelectedArea() {
       return viewModel.getSelectedArea();
    }

    @Override
    public void setSelectedArea(PickerItem selectedArea) {
        viewModel.setSelectedArea(selectedArea);
    }
    @Override
    public PickerItem getSelectedRoom() {
        return viewModel.getSelectedRoom();
    }

    @Override
    public void setSelectedRoom(PickerItem selectedRoom) {
        viewModel.setSelectedRoom(selectedRoom);
    }

    @Override
    public PickerItem getSelectedOtherOption() {
        return viewModel.getSelectedOtherOption();
    }

    @Override
    public void setSelectedOtherOption(PickerItem selectedOtherOption) {
        viewModel.setSelectedOtherOption(selectedOtherOption);
    }

    @Override
    public ArrayList<TaskModel> getAllTaskModels()
    {
        return viewModel.getmMasterTaskList();
    }

    @Override
    public TaskModel getTaskModel(){
        int TaskId = getSelectedOtherOption().getId();
        ArrayList<TaskModel> taskModels= viewModel.getOtherOptionList().getTaskModels();
        for(int i=0;i<taskModels.size();i++){
            if(taskModels.get(i).getTaskID()==TaskId)
                return taskModels.get(i);
        }
        return null;
    }

    public void onTaskDoneRemote(int taskID, int userID, int hotelID) {

        mView.showLoader();
        Observable<BaseResponse> hotelTaskObservable = new RestClient().submitTaskDoneRemote(taskID, userID, hotelID, "Complete");
        hotelTaskObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BaseResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.hideLoader();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideLoader();
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (baseResponse.status.equals(Constants.STATUS_SUCCESS))
                            mView.onTaskDoneSuccess();
                        else
                            mView.onTaskDoneFailure(taskID);

                    }
                });
    }

    public void onTaskStartedRemote(int taskID, int userID, int hotelID) {
        mView.showLoader();
        Observable<BaseResponse> hotelTaskObservable = new RestClient().submitTaskDoneRemote(taskID, userID, hotelID, "Started");
        hotelTaskObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BaseResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.hideLoader();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideLoader();
                    }

                    @Override
                    public void onNext(BaseResponse baseResponse) {
                        if (baseResponse.status.equals(Constants.STATUS_SUCCESS))
                            mView.onTaskDoneSuccess();
                        else
                            mView.onTaskDoneFailure(taskID);

                    }
                });
    }

    //------------------------

    private ArrayList<TaskModel> prepareMaterList(HotelTasksResponse.TaskList taskList) {

        ArrayList<TaskModel> mMasterTaskList = new ArrayList<>();
        mMasterTaskList.clear();
        // Adding Dummy Task at the bottom
        TaskModel dummyTask = new TaskModel();
        dummyTask.setTaskCardType(((Context)mView).getString(R.string.bottom_dummy_task));
        if (taskList.todayTasks != null && taskList.todayTasks.size() > 0) {
            taskList.todayTasks.add(dummyTask);
        } else if (taskList.pendingTasks != null && taskList.pendingTasks.size() > 0) {
            taskList.pendingTasks.add(dummyTask);
        } else if (taskList.hotTasks != null && taskList.hotTasks.size() > 0) {
            taskList.hotTasks.add(dummyTask);
        }
        if (taskList.hotTasks != null && taskList.hotTasks.size() > 0) {
            mMasterTaskList.addAll(taskList.hotTasks);
        }

        if (taskList.pendingTasks != null && taskList.pendingTasks.size() > 0) {
            mMasterTaskList.addAll(taskList.pendingTasks);

        }

        if (taskList.todayTasks != null && taskList.todayTasks.size() > 0) {
            mMasterTaskList.addAll(taskList.todayTasks);
        }
        return mMasterTaskList;
    }
}
