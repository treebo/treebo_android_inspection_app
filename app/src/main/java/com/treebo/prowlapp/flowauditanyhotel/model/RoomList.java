package com.treebo.prowlapp.flowauditanyhotel.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RoomList {

    private int hotelId;
    private List<Rooms> rooms;

    public RoomList(int hotelId, List<Rooms> rooms) {
        this.hotelId = hotelId;
        this.rooms = rooms;
    }

    public int getHotelId() {
        return hotelId;
    }

    public List<PickerItem> convertToPickerItemList() {
        List<PickerItem> lst = new ArrayList<>();
        for (int i = 0; i < rooms.size(); i++)
            lst.add(new PickerItem(rooms.get(i).getRoomId(), rooms.get(i).getRoomName(), rooms.get(i).getRoomType(), null));
        Collections.sort(lst);
        return lst;
    }
}
