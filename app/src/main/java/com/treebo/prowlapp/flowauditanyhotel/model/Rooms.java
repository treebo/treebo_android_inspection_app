package com.treebo.prowlapp.flowauditanyhotel.model;

public class Rooms  {
    private int roomId;
    private String roomName;
    private String roomType;

    public Rooms(int roomId, String roomName,String roomType) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.roomType = roomType;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

}
