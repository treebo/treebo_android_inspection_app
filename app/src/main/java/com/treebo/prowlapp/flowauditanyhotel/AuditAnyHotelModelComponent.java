package com.treebo.prowlapp.flowauditanyhotel;

import com.treebo.prowlapp.flowauditanyhotel.Module.AuditAnyHotelModelModule;
import com.treebo.prowlapp.flowauditanyhotel.model.AuditAnyActivityModel;

import dagger.Component;

@Component(modules = AuditAnyHotelModelModule.class)
public interface AuditAnyHotelModelComponent {
    AuditAnyActivityModel getModel();

}
