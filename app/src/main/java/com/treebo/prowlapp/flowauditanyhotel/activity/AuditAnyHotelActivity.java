package com.treebo.prowlapp.flowauditanyhotel.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.CompleteAudit;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AuditStateChangeEvent;
import com.treebo.prowlapp.events.PeriodicAuditStateChangeEvent;
import com.treebo.prowlapp.flowauditanyhotel.AuditAnyHotelContract;
import com.treebo.prowlapp.flowauditanyhotel.adapter.ItemPickerAdapter;
import com.treebo.prowlapp.flowauditanyhotel.model.Area;
import com.treebo.prowlapp.flowauditanyhotel.model.PickerItem;
import com.treebo.prowlapp.flowauditanyhotel.presenter.AuditAnyHotelPresenter;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

public class AuditAnyHotelActivity extends AppCompatActivity implements AuditAnyHotelContract.IAuditAnyHotelView, ItemPickerAdapter.ItemPickerAdapterListener {

    private static final String TAG = "AuditAnyHotelActivity";

    @Inject
    LoginSharedPrefManager mLoginManager;
    @Inject
    Navigator mNavigator;
    @Inject
    AuditPrefManagerV3 mAuditManager;
    private final CompositeSubscription mSubscription = new CompositeSubscription();
    @Inject
    public RxBus mRxBus;
    private View loader_layout;
    private View loader_layout_external;
    private View mRootView;

    private LinearLayout lv_select_hotel;
    private LinearLayout lv_select_area;
    private LinearLayout lv_select_room;
    private LinearLayout lv_select_other_options;

    private RelativeLayout lv_item_picker;
    RecyclerView rv_item_picker;

    AuditAnyHotelPresenter mPresenter;
    ItemPickerAdapter adapter;

    private void initRecyclerView(){
        rv_item_picker = findViewById(R.id.rv);
        rv_item_picker.setHasFixedSize(true);
        rv_item_picker.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initToolBar(){
        setToolBarColor(R.color.activity_gray_background);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView backBtn =  toolbar.findViewById(R.id.ic_toolbar_audit_any_hotel_back);
        backBtn.setOnClickListener((View v) -> finish());
        TreeboTextView title =  toolbar.findViewById(R.id.toolbar_audit_any_hotel_title);
        title.setText(R.string.title_audit_any_hotel_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
    }

    private void setToolBarColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }
    private void subscribeRx(){
        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof AuditStateChangeEvent) {
                ArrayList<TaskModel> mMasterTaskList = mPresenter.getAllTaskModels();
                int mHotelId = mPresenter.getSelectedHotel().getId();
                switch (((AuditStateChangeEvent) event).auditStateDescription) {
                    case AuditStateChangeEvent.FRAUD_AUDIT_SUBMIT:
                        for (TaskModel taskModel : mMasterTaskList) {
                            if (taskModel.getAuditTaskType() == Constants.FRAUD_AUDIT_TASK_TYPE) {
                                taskModel.isSoftDeleted = true;
                                mPresenter.onTaskDoneRemote(taskModel.getTaskID(), mLoginManager.getUserId(), mHotelId);
                            }
                        }
                        break;

                    case AuditStateChangeEvent.PERIODIC_AUDIT_SUBMIT:
                    case AuditStateChangeEvent.FnB_PERIODIC_AUDIT_SUBMIT:
                        int taskLogId = ((PeriodicAuditStateChangeEvent) event).mTaskLogID;
                        for (TaskModel taskModel : mMasterTaskList) {
                            if (taskModel.getTaskLogID() == taskLogId) {
                                taskModel.isSoftDeleted = true;
                                mPresenter.onTaskDoneRemote(taskModel.getTaskID(), mLoginManager.getUserId(), mHotelId);
                            }
                        }
                        break;

                    case AuditStateChangeEvent.ROOM_AUDIT_SUBMIT:
                    case AuditStateChangeEvent.COMMON_AREA_AUDIT_SUBMIT:
                    case AuditStateChangeEvent.AUDIT_COMPLETE:
                    case AuditStateChangeEvent.UPDATE_AVAILABLE_ROOM_COUNT:
                        int auditTaskID = 0, auditTaskLogID = 0;
                        for (TaskModel taskModel : mMasterTaskList) {
                            if (taskModel.getTaskCardType() == Constants.AUDIT_TASK) {
                                auditTaskID = taskModel.getTaskID();
                                auditTaskLogID = taskModel.getTaskLogID();
                            }
                        }

                        CompleteAudit completeAudit = CompleteAudit.getCompleteAuditByHotelId(mHotelId);
                        if (completeAudit != null) {
                            if (((AuditStateChangeEvent) event).auditStateDescription.equals(AuditStateChangeEvent.ROOM_AUDIT_SUBMIT)) {
                                completeAudit.mAvailableRooms--;
                                completeAudit.update();
                            }
                            if (completeAudit.isDailyAuditCompleted()) {
                                for (TaskModel taskModel : mMasterTaskList) {
                                    if (taskModel.getTaskCardType() == Constants.AUDIT_TASK) {
                                        taskModel.setTaskStatus(getString(R.string.complete));
                                        //mPresenter.onTaskDoneRemote(taskModel.getTaskID(), taskModel.getTaskLogID(), mLoginManager.getUserId(), mHotelId);
                                        mPresenter.onTaskDoneRemote(taskModel.getTaskID(), mLoginManager.getUserId(), mHotelId);
                                    }
                                }
                            } else if (completeAudit.isOneRoomAudited()) {
                               // mPresenter.onTaskStartedRemote(auditTaskID, auditTaskLogID, mLoginManager.getUserId(), mHotelId);
                                mPresenter.onTaskStartedRemote(auditTaskID, mLoginManager.getUserId(), mHotelId);
                            }
                        }
                        break;

                }
            }

        }));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_any_hotel);

        AuditAnyHotelActivityComponent auditAnyHotelActivityComponent = DaggerAuditAnyHotelActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();

        auditAnyHotelActivityComponent.injectAuditAnyHotelActivity(this);
        mPresenter = new AuditAnyHotelPresenter(this);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);
        initViews();
        initToolBar();
        ResetHotel();
        subscribeRx();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Utils.unSubscribe(mSubscription);
    }

    @Override
    public void onBackPressed() {
        if (lv_item_picker.getVisibility()== View.VISIBLE)
            lv_item_picker.setVisibility(View.GONE);
        else
            super.onBackPressed();
    }

    private void initViews(){
        mRootView = findViewById(R.id.audit_any_hotel_root_view);
        loader_layout = findViewById(R.id.loader_layout);
        loader_layout_external = findViewById(R.id.loader_layout_external);
        lv_select_hotel = findViewById(R.id.lv_select_hotel);
        lv_select_hotel.setOnClickListener(View-> onSelectHotelClicked());

        lv_select_area = findViewById(R.id.lv_select_area);
        lv_select_area.setOnClickListener(View-> onSelectAreaClicked());

        lv_select_room = findViewById(R.id.lv_select_room);
        lv_select_room.setVisibility(View.GONE);
        lv_select_room.setOnClickListener(View-> onSelectRoomClicked());

        lv_select_other_options = findViewById(R.id.lv_select_other_options);
        lv_select_other_options.setVisibility(View.GONE);
        lv_select_other_options.setOnClickListener(View-> onSelectOtherOptionClicked());

        lv_item_picker = findViewById(R.id.layout_item_picker_root);
        lv_item_picker.setVisibility(View.GONE);
        View item_picker_back_button = lv_item_picker.findViewById(R.id.ic_back);
        item_picker_back_button.setOnClickListener(view -> hideItemPicker());

        initRecyclerView();

        Button btn_log_issue = findViewById(R.id.btn_log_issue);
        btn_log_issue.setOnClickListener(view -> {
            performLogIssueAction();
        });
    }

    @Override
    public void performLogIssueAction(){
        if(!validateForm())
            return;

        if(mPresenter.getSelectedArea().getName().equals("Rooms"))
        {
            mNavigator.navigateToAuditActivity(this, mPresenter.getSelectedHotel().getId(), mPresenter.getSelectedHotel().getName(), true, mPresenter.getSelectedRoom().getName(),
                    mPresenter.getSelectedRoom().getId(), mPresenter.getTaskModelForAudit(), false,true);
        }
        else if(mPresenter.getSelectedArea().getName().equals("Other"))
        {
            mNavigator.navigateToPeriodicActivity(this, mPresenter.getTaskModel(), mPresenter.getSelectedHotel().getId(), mPresenter.getSelectedHotel().getName(), true,true);
        }
        else
        {
           mPresenter.getCommonArea();
        }

    }
    public void startAudit(HashMap<Integer, ArrayList<Integer>> categoryMap, ArrayList<IssueListModelV3> issues, int auditType, int hotelId) {
        startAudit(categoryMap, issues, auditType, hotelId, "CA", 0); // Right now only Common Area, can add text as per the auditType
    }

    public void startAudit(HashMap<Integer, ArrayList<Integer>> categoryMap, ArrayList<IssueListModelV3> issues,
                          int auditType, int hotelId, String room, int roomId) {
        mAuditManager.cacheCheckpoints(AuditUtilsV3.createSavedCheckPointKey(hotelId, auditType, room), categoryMap);
        mAuditManager.setCurrentRoomId(roomId);
        ArrayList<Category> includedCheckPoints = AuditUtilsV3.checkpointList(categoryMap);
        mNavigator.navigateToCommonAudit(this, includedCheckPoints, issues, auditType, hotelId, mPresenter.getSelectedHotel().getName(),
                false, room, roomId, false, mPresenter.getTaskModelForAudit(),true);
        overridePendingTransition(R.anim.rotate_in, R.anim.rotate_out);
    }
    private Boolean validateForm(){

        if(mPresenter.getSelectedHotel()==null || mPresenter.getSelectedHotel().getId()==-1) {
            showError("Please Select Hotel");
            return false;
        }
        if(mPresenter.getSelectedArea()==null || mPresenter.getSelectedArea().getId()==-1)
        {
            showError("Please Select Area");
            return false;
        }
        if(mPresenter.getSelectedArea().getName().equals("Rooms") && (mPresenter.getSelectedRoom()==null || mPresenter.getSelectedRoom().getId()==-1))
        {
            showError("Please Select Room");
            return false;
        }
        if(mPresenter.getSelectedArea().getName().equals("Other") && (mPresenter.getSelectedOtherOption()==null || mPresenter.getSelectedOtherOption().getId()==-1))
        {
            showError("Please Select Option");
            return false;
        }
        return true;
    }


    @Override
    public void onSelectHotelClicked() {
        showItemPicker("Search Hotel",true,"Hotel");
        TreeboEditText text_search = lv_item_picker.findViewById(R.id.text_search);
        getHotelList();
        text_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //loadHotels(charSequence.toString());
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
    @Override
    public void  getHotelList() {
        mPresenter.getHotels();
    }

    @Override
    public void onHotelListLoadSuccess(List<PickerItem> lst){
        loadHotels(lst);
    }

    @Override
    public void onHotelListLoadError() {
        showError(getString(R.string.error_loading_hotels));
    }
    private void loadHotels(List<PickerItem> lst){
        if(mPresenter.getCurrentItemPicker().equals("Hotel")) {
            adapter = new ItemPickerAdapter(lst, this,"Hotel");
            rv_item_picker.setAdapter(adapter);
        }
    }

    @Override
    public void onItemPickerSuccessHotel(PickerItem item) {
        setHotelText(item);
        mPresenter.getTaskList(mLoginManager.getUserId());
    }

    private void ResetHotel(){
        setHotelText(new PickerItem(-1,getString(R.string.default_spinner_select_hotel)));
    }
    private void setHotelText(PickerItem item){
        TextView text_select_hotel = findViewById(R.id.text_select_hotel);
        mPresenter.setSelectedHotel(item);
        text_select_hotel.setText(item.getName());
        ResetArea();
    }

    @Override
    public void onSelectAreaClicked() {
        if (mPresenter.getSelectedHotel().getId()==-1)
            showError("Please Select Hotel First");
        else {
            showItemPicker(getString(R.string.AreaPickerTitle), false, "Area");
            loadArea();
        }
    }
    private void loadArea(){
        if(mPresenter.getCurrentItemPicker().equals("Area")) {
            List<Area> area = Area.initList();
            List<PickerItem> itemList = Area.convertToItemList(area);
            adapter = new ItemPickerAdapter(itemList, this,"Area");
            rv_item_picker.setAdapter(adapter);
        }
    }

    @Override
    public void onItemPickerSuccessArea(PickerItem item) {
        setAreaText(item);
    }

    private void ResetArea(){
            setAreaText(new PickerItem(-1,"Select Area"));
    }

    private void setAreaText(PickerItem item){
        TextView text_select_area = findViewById(R.id.text_select_area);
        mPresenter.setSelectedArea(item);
        text_select_area.setText(item.getName());
        prepareViewForSelectedArea(mPresenter.getSelectedArea().getName());

    }

    @Override
    public void prepareViewForSelectedArea(String Area) {
        ResetRoom();
        ResetOtherOption();
        lv_select_room.setVisibility(View.GONE);
        lv_select_other_options.setVisibility(View.GONE);

        if (Area.equals("Rooms"))
            lv_select_room.setVisibility(View.VISIBLE);
        else if (Area.equals("Other"))
            lv_select_other_options.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSelectRoomClicked() {
        showItemPicker(getString(R.string.RoomListPickerTitle),false,"Room");
        getRoomList();
    }

    private void  getRoomList() {
        mPresenter.getHotelRooms();
    }
    @Override
    public void onRoomListLoadSuccess(List<PickerItem> lst){
        loadRooms(lst);
    }
    @Override
    public void onRoomListLoadNoHotelSelected() {
        showError(getString(R.string.select_hotel));
    }

    @Override
    public void onRoomListLoadError() {
        showError(getString(R.string.error_loading_rooms));
    }

    private void loadRooms(List<PickerItem> lst){
        if(mPresenter.getCurrentItemPicker().equals("Room")) {
            adapter = new ItemPickerAdapter(lst, this,"Room");
            rv_item_picker.setAdapter(adapter);
        }
    }

    @Override
    public void onItemPickerSuccessRoom(PickerItem item) {
        setRoomText(item);
    }

    private void ResetRoom(){
        setRoomText(new PickerItem(-1,"Select Room"));
    }

    private void setRoomText(PickerItem item)
    {
        TextView text_select_room = findViewById(R.id.text_select_room);
        mPresenter.setSelectedRoom(item);
        text_select_room.setText(item.getName());
    }

    @Override
    public void onSelectOtherOptionClicked() {
        showItemPicker(getString(R.string.OtherPickerTitle),false,"OtherOption");
        getOtherOptions();
    }

    private void  getOtherOptions() {
        mPresenter.getOtherOptions(mLoginManager.getUserId());
    }

    @Override
    public void onOtherListLoaded(List<PickerItem> lst){
        if(mPresenter.getCurrentItemPicker().equals("OtherOption")) {
            loadOthers(lst);
        }
    }

    private void loadOthers(List<PickerItem> lst){

        adapter = new ItemPickerAdapter(lst,this,"Other");
        rv_item_picker.setAdapter(adapter);
    }

    @Override
    public void onItemPickerSuccessOtherOption(PickerItem item) {
        setOtherOptionText(item);

    }

    private void ResetOtherOption()
    {
        setOtherOptionText(new PickerItem(-1,"Select Option"));
    }
    private void setOtherOptionText(PickerItem item)
    {
        TextView text_select_other = findViewById(R.id.text_select_other);
        mPresenter.setSelectedOtherOption(item);
        text_select_other.setText(item.getName());

    }

    @Override
    public void showError(String str) {
        SnackbarUtils.show(mRootView,str);
    }

    @Override
    public void onCommonAreaResponseLoaded(CommonAreaTypeResponse response) {
            startAudit(response.getData().getCommonArea().getmCheckpointList(),
                    response.getData().getCommonArea().getmIssuesList(),
                    response.getData().getCommonArea().getAuditTypeId(), mPresenter.getSelectedHotel().getId());
    }

    @Override
    public void showLoaderExternal() {
        loader_layout_external.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoaderExternal() {
        loader_layout_external.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
       loader_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        loader_layout.setVisibility(View.GONE);
    }

    @Override
    public void hideItemPicker() {
        lv_item_picker.setVisibility(View.GONE);
    }

    @Override
    public void showItemPicker(String Title, Boolean IsSearchEnabled,String CurrentItemPicker) {
        lv_item_picker.setVisibility(View.VISIBLE);
        mPresenter.setCurrentItemPicker(CurrentItemPicker);
        TreeboTextView item_picker_title = lv_item_picker.findViewById(R.id.item_picker_title);
        item_picker_title.setText(Title);
        TreeboEditText text_search = lv_item_picker.findViewById(R.id.text_search);
        if(IsSearchEnabled){
            text_search.setVisibility(View.VISIBLE);
            text_search.setText("");
            text_search.requestFocus();
        }
        else {
            text_search.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemSelected(PickerItem item) {
        hideItemPicker();
        switch (mPresenter.getCurrentItemPicker()) {
            case "Hotel":
                onItemPickerSuccessHotel(item);
                break;
            case "Area":
                onItemPickerSuccessArea(item);
                break;
            case "Room":
                onItemPickerSuccessRoom(item);
                break;
            default:
                onItemPickerSuccessOtherOption(item);
                break;
        }
    }

    @Override
    public void onTaskDoneSuccess() {
            mPresenter.getTaskList(mLoginManager.getUserId());
            ResetArea();
    }

    @Override
    public void onTaskDoneFailure(int taskID) {

    }

}
