package com.treebo.prowlapp.flowauditanyhotel.model;

import com.treebo.prowlapp.R;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class Area {
    public String Name;
    public int ResId;

    @Inject
    public Area(String name, int resId) {
        Name = name;
        ResId = resId;
    }

    public int getResId() {
        return ResId;
    }

    public void setResId(int resId) {
        ResId = resId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
    public static List<Area> initList()
    {
        List<Area> lst = new ArrayList<>();
        lst.add(new Area("Rooms", R.drawable.ic_recommended_rooms));
        lst.add(new Area("General Common Area",R.drawable.ic_common_area));
        lst.add(new Area("Other",R.drawable.group_4));
        return lst;
    }

    public static List<PickerItem> convertToItemList(List<Area> area)
    {
        List<PickerItem> lst = new ArrayList<>();
        for(Area item: area)
            lst.add(new PickerItem(0,item.getName(),item.getResId()));
        return lst;
    }
}
