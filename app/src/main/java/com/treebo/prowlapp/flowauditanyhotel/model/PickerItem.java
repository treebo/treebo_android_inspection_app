package com.treebo.prowlapp.flowauditanyhotel.model;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

public class PickerItem implements Comparable<PickerItem> {

    private int Id;
    private String Name;
    private String SecondaryText;
    private Drawable Icon;
    private int ResId;

    public String getSecondaryText() {
        return SecondaryText;
    }

    public void setSecondaryText(String secondaryText) {
        SecondaryText = secondaryText;
    }

    public int getResId() {
        return ResId;
    }

    public void setResId(int resId) {
        ResId = resId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Inject
    public PickerItem(int id, String name) {
        this.Id = id;
        this.Name = name;

    }

    public PickerItem(int id, String name,String secondaryText,Drawable icon) {
        this.Id = id;
        this.Name = name;
        this.SecondaryText = secondaryText;
        this.Icon = icon;
    }
    public PickerItem(int id, String name,int resId) {
        this.Id = id;
        this.Name = name;

        this.ResId = resId;
    }

    @Override
    public int compareTo(PickerItem another) {
        return getName().compareTo(another.getName());
    }
}
