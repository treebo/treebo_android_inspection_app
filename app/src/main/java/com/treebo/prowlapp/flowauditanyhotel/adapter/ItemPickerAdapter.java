package com.treebo.prowlapp.flowauditanyhotel.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowauditanyhotel.model.PickerItem;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ItemPickerAdapter extends RecyclerView.Adapter implements Filterable {

    private List<PickerItem> listData;
    private String listType;
    private List<PickerItem> filteredListData;
    private ItemPickerAdapterListener listener;


    @Inject
    public AuditPrefManagerV3 mAuditManager;
    public ItemPickerAdapter(List<PickerItem> listData, ItemPickerAdapterListener listener, String listType) {
        this.listData = listData;
        this.listType = listType;
        this.filteredListData = listData;
        this.listener = listener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View listItem;
        if(listType.equals("Area"))
            listItem= layoutInflater.inflate(R.layout.item_area_selection, parent, false);
        else if( listType.equals("Other"))
            listItem= layoutInflater.inflate(R.layout.item_option_selection, parent, false);
        else if(listType.equals("Room"))
            listItem= layoutInflater.inflate(R.layout.item_room_selection, parent, false);
        else
            listItem= layoutInflater.inflate(R.layout.item_item_picker, parent, false);

        return new ViewHolder(listItem,listType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((ViewHolder) holder).layout_root.setOnClickListener(view -> listener.onItemSelected(filteredListData.get(position)));
        ((ViewHolder) holder).text_name.setText(filteredListData.get(position).getName());
         if(listType.equals("Room"))
             ((ViewHolder) holder).text_type.setText(filteredListData.get(position).getSecondaryText());
         else if(listType.equals("Area") || listType.equals("Other"))
            ((ViewHolder) holder).ic_image.setImageResource(filteredListData.get(position).getResId());
         else if(listType.equals("Hotel")) {
             ((ViewHolder) holder).ic_image.setImageResource(R.drawable.ic_hotel_green);
         }
    }

    @Override
    public int getItemCount() {
        return filteredListData.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredListData = listData;
                } else {
                    List<PickerItem> filteredList = new ArrayList<>();
                    for (PickerItem row : listData)
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(row);

                    filteredListData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredListData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredListData = (ArrayList<PickerItem>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ic_image;
        TextView text_name;
        TextView text_type;
        View layout_root;
        public ViewHolder(View itemView, String listType) {
            super(itemView);
            if(listType.equals("Area") || listType.equals("Other")) {
                this.ic_image = itemView.findViewById(R.id.task_card_iv);
                this.text_name = itemView.findViewById(R.id.task_card_header);
                this.layout_root = itemView.findViewById(R.id.layout_root);
            }
            else if(listType.equals("Room")) {
                this.text_name = itemView.findViewById(R.id.room_number_tv);
                this.text_type= itemView.findViewById(R.id.room_type_tv);
                this.layout_root = itemView.findViewById(R.id.layout_root);
            }
            else {
                this.ic_image = itemView.findViewById(R.id.ic_image);
                this.text_name = itemView.findViewById(R.id.text_name);
                this.layout_root = itemView.findViewById(R.id.layout_root);
            }
        }
    }
    public interface ItemPickerAdapterListener {
        void onItemSelected(PickerItem item);
    }
}
