package com.treebo.prowlapp.flowauditanyhotel;

import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.flowauditanyhotel.model.PickerItem;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import java.util.ArrayList;
import java.util.List;

public class AuditAnyHotelContract {

    public interface IAuditAnyHotelView  {

        void prepareViewForSelectedArea(String Area);

        void showLoaderExternal();
        void hideLoaderExternal();

        void showLoader();
        void hideLoader();
        void showError(String str);

        void showItemPicker(String Title, Boolean IsSearchEnabled,String CurrentItemPicker);
        void hideItemPicker();

        void onSelectHotelClicked();
        void onItemPickerSuccessHotel(PickerItem item);

        void onSelectAreaClicked();
        void onItemPickerSuccessArea(PickerItem item);

        void onSelectRoomClicked();
        void onItemPickerSuccessRoom(PickerItem item);

        void onSelectOtherOptionClicked();
        void onItemPickerSuccessOtherOption(PickerItem item);

        void getHotelList();
        void onHotelListLoadSuccess(List<PickerItem> lst);
        void onRoomListLoadNoHotelSelected();
        void onHotelListLoadError();

        void onRoomListLoadSuccess(List<PickerItem> lst);
        void onRoomListLoadError();
        void onOtherListLoaded(List<PickerItem> lst);

        void onCommonAreaResponseLoaded(CommonAreaTypeResponse response);
        void performLogIssueAction();

        void onTaskDoneSuccess();
        void onTaskDoneFailure(int taskID);

    }

    public interface IAuditAnyHotelPresenter
    {
        void getHotels();
        void getTaskList(int UserId);
        void getHotelRooms();
        void getOtherOptions(int userId);
        void getCommonArea();

        TaskModel getTaskModel();
        ArrayList<TaskModel> getAllTaskModels();
        void setCurrentItemPicker(String currentItemPicker);
        String getCurrentItemPicker();
        PickerItem getSelectedHotel();
        void setSelectedHotel(PickerItem selectedHotel);
        PickerItem getSelectedArea();
        void setSelectedArea(PickerItem selectedArea);
        PickerItem getSelectedRoom();
        void setSelectedRoom(PickerItem selectedRoom);
        PickerItem getSelectedOtherOption();
        void setSelectedOtherOption(PickerItem selectedOtherOption);
    }
}
