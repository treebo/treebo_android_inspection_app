package com.treebo.prowlapp.flowauditanyhotel.model;

import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import java.util.ArrayList;
import javax.inject.Inject;

public class AuditAnyActivityModel{
    private String CurrentItemPicker="";
    private PickerItem selectedHotel;
    private PickerItem selectedArea;
    private PickerItem selectedRoom;
    private PickerItem selectedOtherOption;
    private RoomList roomList;
    private CommonAreaTypeResponse mCommonAreaResponse;
    private OtherOptionsList otherOptionsList;
    private ArrayList<TaskModel> mMasterTaskList;
    @Inject
    public AuditAnyActivityModel(){

    }

    public ArrayList<TaskModel> getmMasterTaskList() {
        return mMasterTaskList;
    }

    public void setmMasterTaskList(ArrayList<TaskModel> mMasterTaskList) {
        this.mMasterTaskList = mMasterTaskList;
    }

    public CommonAreaTypeResponse getmCommonAreaResponse() {
        return this.mCommonAreaResponse;
    }

    public void setmCommonAreaResponse(CommonAreaTypeResponse mCommonAreaResponse) {
        this.mCommonAreaResponse = mCommonAreaResponse;
    }

    public RoomList getRoomList(){
        return roomList;
    }
    public void setRoomsList(RoomList lst){
        this.roomList = lst;
    }

    public OtherOptionsList getOtherOptionList(){
        return this.otherOptionsList;
    }
    public void setOtherOptionList(OtherOptionsList lst){
        this.otherOptionsList = lst;
    }

    public String getCurrentItemPicker() {
        return CurrentItemPicker;
    }

    public void setCurrentItemPicker(String currentItemPicker) {
        CurrentItemPicker = currentItemPicker;
    }

    public PickerItem getSelectedHotel() {
        return selectedHotel;
    }

    public void setSelectedHotel(PickerItem selectedHotel) {
        this.selectedHotel = selectedHotel;
    }

    public PickerItem getSelectedArea() {
        return selectedArea;
    }

    public void setSelectedArea(PickerItem selectedArea) {
        this.selectedArea = selectedArea;
    }

    public PickerItem getSelectedRoom() {
        return selectedRoom;
    }

    public void setSelectedRoom(PickerItem selectedRoom) {
        this.selectedRoom = selectedRoom;
    }

    public PickerItem getSelectedOtherOption() {
        return selectedOtherOption;
    }

    public void setSelectedOtherOption(PickerItem selectedOtherOption) {
        this.selectedOtherOption = selectedOtherOption;
    }
}
