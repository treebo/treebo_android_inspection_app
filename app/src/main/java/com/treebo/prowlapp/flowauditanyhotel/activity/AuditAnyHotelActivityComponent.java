package com.treebo.prowlapp.flowauditanyhotel.activity;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class})
public interface AuditAnyHotelActivityComponent {
    void injectAuditAnyHotelActivity(AuditAnyHotelActivity auditAnyHotelActivity);
}
