package com.treebo.prowlapp.flowaudit;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowaudit.activity.HotelAuditActivity;
import com.treebo.prowlapp.flowaudit.activity.PickRoomActivity;
import com.treebo.prowlapp.flowaudit.activity.SectionAreaActivity;

import dagger.Component;

/**
 * Created by sumandas on 08/08/2016.
 */
@Deprecated
@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, AuditModule.class})
public interface AuditComponent {
    void injectAuditActivity(HotelAuditActivity hotelAuditActivity);
    void injectRoomActivity(PickRoomActivity pickRoomActivity);
    void injectQuestionaireActivity(SectionAreaActivity sectionAreaActivity);
}
