package com.treebo.prowlapp.flowaudit.presenter;

import com.treebo.prowlapp.flowaudit.observers.HotelAuditAreaObserver;
import com.treebo.prowlapp.flowaudit.observers.HotelRoomObserver;
import com.treebo.prowlapp.flowaudit.usecase.FormMetaDataUseCase;
import com.treebo.prowlapp.flowaudit.usecase.HotelRoomsUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by sumandas on 28/04/2016.
 */
@Deprecated
public interface HotelAuditPresenter extends BasePresenter {
    void loadRooms(int hotelId, HotelRoomsUseCase hotelRoomsUseCase, HotelRoomObserver hotelRoomObserver);
    void loadAreas(FormMetaDataUseCase formMetaDataUseCase, HotelAuditAreaObserver hotelAuditAreaObserve);

}
