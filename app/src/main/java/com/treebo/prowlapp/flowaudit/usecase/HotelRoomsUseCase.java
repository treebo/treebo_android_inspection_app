package com.treebo.prowlapp.flowaudit.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.HotelRoomResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by sumandas on 29/04/2016.
 */
public class HotelRoomsUseCase extends UseCase<HotelRoomResponse> {

    public void setmHotelId(int mHotelId) {
        this.mHotelId = mHotelId;
    }

    private int mHotelId;

    private RestClient mRestClient;

    public HotelRoomsUseCase(RestClient restClient) {
        mRestClient=restClient;
    }

    @Override
    protected Observable<HotelRoomResponse> getObservable() {
        return mRestClient.getHotelRooms(mHotelId);
    }
}
