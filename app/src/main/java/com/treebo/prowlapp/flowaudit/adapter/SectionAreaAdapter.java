package com.treebo.prowlapp.flowaudit.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowaudit.activity.SectionAreaActivity;
import com.treebo.prowlapp.flowupdateauditOrissue.AddCommentActivity;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.Models.Photo_Table;
import com.treebo.prowlapp.Models.Query;
import com.treebo.prowlapp.Models.Subsection;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;
import com.treebo.prowlapp.Utils.Utils;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 27/04/2016.
 */
@Deprecated
public class SectionAreaAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    public Form mAreaSections;
    public Activity mActivity;
    public int mNumberAuditsDone;
    public OnAuditStateChangedListener mAuditStateListener;

    public AuditPreferenceManager mAuditManager;

    int mTotalCount;
    boolean isAuditDone[];
    int[] mCheckedArray;
    int mHotelId;
    String mRoomNo;
    String mKey;

    public Query mQueryBeingAudited;
    public int mCurrentBadAuditId;
    public int mCurrentBadAuditPosition;

    public SectionAreaAdapter(Activity activity, Form areaSections, OnAuditStateChangedListener listener, int hotelId, String roomNo){
        mAreaSections=areaSections;
        mActivity=activity;
        mAuditStateListener=listener;
        mAuditManager= AuditPreferenceManager.getInstance();
        for(int i=0;i<getSectionCount();i++){
            mTotalCount+=getItemCount(i);
        }
        isAuditDone=new boolean[mTotalCount];
        mCheckedArray=new int[mTotalCount];
        for(int i=0;i<mTotalCount;i++){
            mCheckedArray[i]=-1;
        }
        mRoomNo=roomNo;
        mHotelId=hotelId;
        Form formSaved;
        if(mAreaSections.getSection().equalsIgnoreCase("room") ){
            formSaved=mAuditManager.getPersistedRoomAreaAudit(Integer.toString(hotelId), roomNo);
            mKey=Integer.toString(hotelId)+"_"+roomNo;
        }else{
           formSaved=mAuditManager.getPersistedCommonAreaAudit(Integer.toString(hotelId));
            mKey=Integer.toString(hotelId);
        }
        if(formSaved!=null){
            mAreaSections=formSaved;
            initSavedAudit();
        }
    }

    @Override
    public int getSectionCount() {
        return mAreaSections.getSubsections().size();
    }

    @Override
    public int getItemCount(int section) {
        return mAreaSections.getSubsections().get(section).getQueries().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder  holder, int section) {
        String sectionName = mAreaSections.getSubsections().get(section).getSubsection();
        ((SectionedViewHolder)holder).mSectionTitle.setText(sectionName.toUpperCase());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,final int section,final int relativePosition,final int absolutePosition) {
        List<Query> itemsInSection = mAreaSections.getSubsections().get(section).getQueries();
        String itemName = itemsInSection.get(relativePosition).getQueryName();
        final Query query=itemsInSection.get(relativePosition);
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.queryName.setText(itemName.trim());

        itemViewHolder.divider.setVisibility(relativePosition == getItemCount(section) - 1 ? View.INVISIBLE : View.VISIBLE);
        final int checkedId=mCheckedArray[absolutePosition];

        if(checkedId==-1){
            itemViewHolder.radioGroup.setActivated(false);
        }else{
            itemViewHolder.radioGroup.setActivated(true);
        }
        itemViewHolder.radioGroup.check(checkedId);
        if(query.getSubQuery().isEmpty()){
            itemViewHolder.subQueryName.setVisibility(View.GONE);
        }else{
            itemViewHolder.subQueryName.setVisibility(View.VISIBLE);
            itemViewHolder.subQueryName.setText(query.getSubQuery().trim());
        }


        itemViewHolder.feedbackBad.setOnClickListener((View v) -> {
            mCurrentBadAuditId=v.getId();
            mCurrentBadAuditPosition=absolutePosition;
            mQueryBeingAudited = query;
            itemViewHolder.radioGroup.setActivated(true);
            Intent intent = new Intent(mActivity, AddCommentActivity.class);
            intent.putExtra(Utils.AUDIT_BAD, true);
            intent.putExtra(Utils.AUDIT_BAD_TITLE,query.getQueryName());
            if (mActivity instanceof SectionAreaActivity) {
                intent.putExtra(Utils.QUERY_OBJECT,mQueryBeingAudited);
                intent.putExtra(Utils.PHOTO_KEY,mKey+"_"+mQueryBeingAudited.getQueryId());
                mActivity.startActivityForResult(intent, Utils.START_AUDIT);
            }
            mAuditStateListener.onAuditChanged();
        });

        itemViewHolder.feedbackGood.setOnClickListener((View v) -> {
            itemViewHolder.radioGroup.setActivated(true);
            setAuditedRowState(v.getId(), absolutePosition);
            persistAuditRow(query, "GOOD", "", "");
            mAuditStateListener.onAuditChanged();
        });

        itemViewHolder.feedbackNA.setOnClickListener((View v) -> {
            itemViewHolder.radioGroup.setActivated(true);
            setAuditedRowState(v.getId(), absolutePosition);
            persistAuditRow(query, "NA", "", "");
            mAuditStateListener.onAuditChanged();
        });

    }

    public void setAuditedRowState(int viewId,int absolutePosition){
        mCheckedArray[absolutePosition] = viewId;
        if (viewId > 0 && !isAuditDone[absolutePosition]) {
            mNumberAuditsDone++;
            isAuditDone[absolutePosition] = true;
            if (mNumberAuditsDone == mTotalCount) {
                mAuditStateListener.onAuditStateChanged(true,true);
            }
            if (mNumberAuditsDone == 1) {
                mAuditStateListener.onAuditStateChanged(false, true);
            }
            AuditPreferenceManager.getInstance().persistLastAuditKey(mKey);

        }
    }

    public void persistAuditRow(Query query, String answer, String comments, String imageUrls){
        AuditPreferenceManager.getInstance().setAuditRowUpdated(true);
        query.setImage_url("");
        query.setComment("");
        query.setAnswer(answer);
        if(mAreaSections.getSection().startsWith("Common")|| mAreaSections.getSection().startsWith("COMMON")){
            String key=Integer.toString(mHotelId);
            mAuditManager.removeAndPersistCommonAreaAudit(key,mAreaSections);
            mAuditManager.persistLastAuditKey(key);
        } else {
            if(mRoomNo.isEmpty()) {
                Log.e("SectionAdapter","The room no is empty");
            }else{
                String key =Integer.toString(mHotelId)+"_"+mRoomNo;
                mAuditManager.removeAndPersistRoomAreaAudit(Integer.toString(mHotelId), mRoomNo, mAreaSections);
                mAuditManager.persistLastAuditKey(key);
            }
        }
        mAuditStateListener.onAuditRowUpdated();
    }

    public void persistAuditRow(String comments){
        setAuditedRowState(mCurrentBadAuditId, mCurrentBadAuditPosition);
        AuditPreferenceManager.getInstance().setAuditRowUpdated(true);
        mQueryBeingAudited.setComment(comments);
        mQueryBeingAudited.setAnswer("BAD");
        if(mAreaSections.getSection().startsWith("Common")|| mAreaSections.getSection().startsWith("COMMON")){
            mAuditManager.removeAndPersistCommonAreaAudit(Integer.toString(mHotelId),mAreaSections);
        }else {
            if(mRoomNo==null || mRoomNo.isEmpty()){
                Log.e("SectionAdapter","The room no is empty");
            }else{
                mAuditManager.removeAndPersistRoomAreaAudit(Integer.toString(mHotelId), mRoomNo, mAreaSections);
            }
        }
        mAuditStateListener.onAuditRowUpdated();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch(viewType){
            case VIEW_TYPE_HEADER:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_area_header, parent, false);
                return new SectionedViewHolder(v);
            case VIEW_TYPE_ITEM:
                default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.communal_area_list_row, parent, false);
                return new ItemViewHolder(v);
        }
    }


    public static class SectionedViewHolder extends RecyclerView.ViewHolder {

        public TextView mSectionTitle;

        public SectionedViewHolder(View itemView) {
            super(itemView);
            mSectionTitle=(TextView)itemView.findViewById(R.id.section_header_title);
        }
    }

    public  class ItemViewHolder extends RecyclerView.ViewHolder{

        public TextView queryName;
        public TextView subQueryName;
        public RadioGroup radioGroup;
        public RadioButton feedbackGood;
        public RadioButton feedbackBad;
        public RadioButton feedbackNA;
        public View divider;


        public ItemViewHolder(View itemView) {
            super(itemView);
            radioGroup = (RadioGroup) itemView.findViewById(R.id.radio_group_communal) ;
            queryName = (TextView) itemView.findViewById(R.id.subsection_name);
            feedbackGood =   (RadioButton) itemView.findViewById(R.id.feedback_good);
            feedbackBad = (RadioButton) itemView.findViewById(R.id.feedback_bad);
            feedbackNA = (RadioButton) itemView.findViewById(R.id.feedback_unavailable);
            subQueryName=(TextView) itemView.findViewById(R.id.section_sub_query);
            divider= itemView.findViewById(R.id.audit_divider);

        }

    }

    public void clearAudit(){
        for(int i=0;i<mTotalCount;i++){
            mCheckedArray[i]=-1;
            isAuditDone[i]=false;
        }
        mNumberAuditsDone=0;
        for(Subsection subsection:mAreaSections.getSubsections()){
            for(Query query:subsection.getQueries()){
                query.setAnswer("");
                query.setComment("");
                query.setImage_url("");
            }
        }
        String key;
        if(mAreaSections.getSection().equalsIgnoreCase("room")) {
            key = Integer.toString(mHotelId)+"_"+mRoomNo;
        }else {
            key=  Integer.toString(mHotelId);
        }
        mAuditManager.removeAudit(key);
        mAuditManager.setAuditRowUpdated(true);
        mAuditManager.removeLastAuditKey(key);//remove from paused audit list
        mAuditStateListener.onAuditRowUpdated();
        SQLite.delete().from(Photo.class).where(Photo_Table.mKey.like("%" + key + "%")).async().execute();
        notifyDataSetChanged();
    }

    public void initSavedAudit(){
        int absolutePosition=0;
        for(Subsection subsection:mAreaSections.getSubsections()){
            for(Query query:subsection.getQueries()){
                if(query.getAnswer().equals("GOOD")){
                    mCheckedArray[absolutePosition]=R.id.feedback_good;
                    mNumberAuditsDone++;
                    isAuditDone[absolutePosition] = true;
                }else if(query.getAnswer().equals("BAD")){
                    mNumberAuditsDone++;
                    isAuditDone[absolutePosition] = true;
                    mCheckedArray[absolutePosition]=R.id.feedback_bad;
                }else if(query.getAnswer().equals("NA")){
                    mNumberAuditsDone++;
                    isAuditDone[absolutePosition] = true;
                    mCheckedArray[absolutePosition]=R.id.feedback_unavailable;
                }
                absolutePosition++;
            }
        }
        mAuditStateListener.onAuditRowUpdated();
        if(mNumberAuditsDone>0){
            if(mNumberAuditsDone==mTotalCount){
                mAuditStateListener.onAuditStateChanged(true,true);
            }else{
                mAuditStateListener.onAuditStateChanged(false,true);
            }
        }

    }

    public interface OnAuditStateChangedListener {
        void onAuditRowUpdated();
        void onAuditStateChanged(boolean isSubmitEnabled, boolean isButtonVisible);
        void onAuditChanged();
    }
}
