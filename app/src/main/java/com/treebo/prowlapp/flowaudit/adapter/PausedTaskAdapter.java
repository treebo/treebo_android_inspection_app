package com.treebo.prowlapp.flowaudit.adapter;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.Room;
import com.treebo.prowlapp.Utils.Utils;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 28/04/2016.
 */
@Deprecated
public class PausedTaskAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Pair<Room,String>> mPausedRoomList;
    private Context mContext;
    public OnRoomClickedListener mRoomClickListener;

    public PausedTaskAdapter(Context context, List<Pair<Room, String>> auditScores, OnRoomClickedListener onRoomClickedListener) {
        mPausedRoomList = auditScores;
        mContext = context;
        mRoomClickListener=onRoomClickedListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.inflated_paused_tasks_view, parent, false);
        return new IndividualViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        Pair<Room, String> score = mPausedRoomList.get(position);
        ((IndividualViewHolder) holder).roomNumber.setText((score.first).number);
        ((IndividualViewHolder) holder).roomNumber.setTextColor(mContext.getResources().getColor(Utils.getRoomColor((score.first).color)));
        ((IndividualViewHolder) holder).pausedTasksNumber.setText
                (String.format(mContext.getResources().getString(R.string.audit_paused_task), score.second));
        ((IndividualViewHolder) holder).rootView.setOnClickListener((View v)->
            PausedTaskAdapter.this.mRoomClickListener.onPausedRoomClicked(((IndividualViewHolder) holder).roomNumber.getText().toString()));
        ((IndividualViewHolder) holder).divider.setVisibility(position == getItemCount() - 1 ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mPausedRoomList.size();
    }

    public  class IndividualViewHolder extends RecyclerView.ViewHolder {
        public TextView roomNumber;
        public TextView pausedTasksNumber;
        public View divider;
        public View rootView;

        public IndividualViewHolder(View itemView) {
            super(itemView);
            roomNumber = (TextView) itemView.findViewById(R.id.room_number);
            pausedTasksNumber = (TextView) itemView.findViewById(R.id.percent_completed_tasks);
            divider =  itemView.findViewById(R.id.room_divider);
            rootView=itemView;
        }
    }

    public interface OnRoomClickedListener{
        void onPausedRoomClicked(String roomClicked);
    }

}
