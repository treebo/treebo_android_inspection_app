package com.treebo.prowlapp.flowaudit.presenter;

import com.treebo.prowlapp.flowaudit.observers.HotelAuditAreaObserver;
import com.treebo.prowlapp.flowaudit.observers.HotelRoomObserver;
import com.treebo.prowlapp.flowaudit.usecase.FormMetaDataUseCase;
import com.treebo.prowlapp.flowaudit.usecase.HotelRoomsUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.HotelAuditView;

/**
 * Created by sumandas on 28/04/2016.
 */
@Deprecated
public class HotelAuditPresenterImpl implements HotelAuditPresenter {
    public HotelAuditView mAuditView;

    @Override
    public void loadRooms(int hotelId,HotelRoomsUseCase hotelRoomsUseCase, HotelRoomObserver hotelRoomObserver) {
        showLoading();
        hotelRoomsUseCase.setmHotelId(hotelId);
        hotelRoomsUseCase.execute(hotelRoomObserver);

    }

    @Override
    public void loadAreas(FormMetaDataUseCase formMetaDataUseCase, HotelAuditAreaObserver hotelAuditAreaObserver) {
        showLoading();
        formMetaDataUseCase.execute(hotelAuditAreaObserver);

    }

    @Override
    public void setMvpView(BaseView baseView) {
        mAuditView=(HotelAuditView)baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return null;
    }

    private void showLoading() {
        mAuditView.showLoading();
    }



}
