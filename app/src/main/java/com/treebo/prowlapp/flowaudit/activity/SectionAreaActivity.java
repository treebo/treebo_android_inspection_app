package com.treebo.prowlapp.flowaudit.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.adapter.SlidingTabAdapter;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AuditIssueSubmitEvent;
import com.treebo.prowlapp.events.RoomSubmittedEvent;
import com.treebo.prowlapp.events.UpdateBlindCountEvent;
import com.treebo.prowlapp.flowaudit.AuditComponent;
import com.treebo.prowlapp.flowaudit.DaggerAuditComponent;
import com.treebo.prowlapp.flowaudit.adapter.SectionAreaAdapter;
import com.treebo.prowlapp.flowaudit.observers.AuditSubmitObserver;
import com.treebo.prowlapp.flowaudit.observers.HotelRoomObserver;
import com.treebo.prowlapp.flowaudit.presenter.AuditSubmitPresenter;
import com.treebo.prowlapp.flowaudit.usecase.AuditUseCase;
import com.treebo.prowlapp.flowaudit.usecase.HotelRoomsUseCase;
import com.treebo.prowlapp.job.AuditJob;
import com.treebo.prowlapp.Models.AnswerListWithSectionName;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.Models.Photo_Table;
import com.treebo.prowlapp.Models.RoomTypeList;
import com.treebo.prowlapp.Models.Subsection;
import com.treebo.prowlapp.Models.UpdateQueryModel;
import com.treebo.prowlapp.mvpviews.AuditAreaView;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.AuditUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.MapperUtils;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.NetworkUtil;
import com.treebo.prowlapp.Utils.PhotoUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.SlidingTabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.percentlayout.widget.PercentLayoutHelper;
import androidx.percentlayout.widget.PercentRelativeLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import rx.Observable;
import rx.Observer;

/**
 * Created by sumandas on 27/04/2016.
 */
@Deprecated
public class SectionAreaActivity extends AppCompatActivity implements SectionAreaAdapter.OnAuditStateChangedListener, AuditAreaView, SlidingTabLayout.OnTabSelectedListener {

    private Toolbar mToolBar;
    private SlidingTabLayout mSlidingTabLayout;
    private RecyclerView mRecyclerView;
    private ViewPager mViewPager;
    private TextView mSubmitClear;
    private String mTitle;
    private SectionAreaAdapter mSectionAreaAdapter;
    private Form mAreaList;
    private String mRoomNo;
    private int mHotelId;
    private String mHotelName="";

    @Inject
    public  ProgressDialog progressDialog;

    private View completed;
    private View left;

    private View mRootView;

    private String mPersistedKey;

    @Inject
    public AuditSubmitPresenter mAuditPresenter;

    HashMap<Integer, Integer> mQuestionSectionPositionMap = new HashMap<>();

    //Optimisation mechanism for Position to first Visible;
    HashMap<Integer, Integer> mQuestionSectionFirstPositionCache = new HashMap<>();

    @Inject
    public MixpanelAPI mMixpanelApi;

    @Inject
    public AuditPreferenceManager mAuditPreferenceManager;

    @Inject
    public LoginSharedPrefManager mLoginSharedPrefManager;

    @Inject
    public RxBus mRxBus;

    private boolean isRoomAudit;

    @Inject
    public HotelRoomsUseCase mRoomsUseCase;

    @Inject
    public AuditUseCase mAuditUseCase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_area);

        Intent intent = getIntent();
        mTitle = intent.getStringExtra(Utils.AREA_NAME);
        mAreaList = intent.getParcelableExtra(Utils.FORM_OBJECT);
        mRoomNo = intent.getStringExtra(Utils.ROOM_NAME);
        mHotelId = getIntent().getIntExtra(Utils.HOTEL_ID, 0);
        mHotelName=getIntent().getStringExtra(Utils.HOTEL_NAME);

        if (mAreaList == null) {
            mAreaList = new Form();
        }

        isRoomAudit = mRoomNo != null;

        mRootView=findViewById(R.id.area_root_view);
        mToolBar=(Toolbar)findViewById(R.id.toolbar);
        mRecyclerView =(RecyclerView)findViewById(R.id.section_subsection);
        mSubmitClear =(TextView)findViewById(R.id.submit_clear_inspection);
        completed=findViewById(R.id.audit_progressed);
        left=findViewById(R.id.audit_left);

        mToolBar.setTitleTextColor(getResources().getColor(R.color.white));
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.section_tab_strip);
        mSlidingTabLayout.setTabTextColor(getResources().getColor(R.color.white));
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setOnTabSelectedListener(this);


        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.white));
        ArrayList<String> subSections = new ArrayList();

        for (Subsection section : mAreaList.getSubsections()) {
            subSections.add(section.getSubsection());
        }
        mViewPager = new ViewPager(this);
        mViewPager.setAdapter(new SlidingTabAdapter(this, subSections));
        mSlidingTabLayout.setViewPager(mViewPager);

        mToolBar.setNavigationIcon(R.drawable.ic_back_white);
        mToolBar.setNavigationOnClickListener((View v) -> finish());
        mToolBar.setTitle(mTitle);
        mToolBar.setTitleTextColor(getResources().getColor(R.color.white_opaque_80));

        AuditComponent auditComponent= DaggerAuditComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectQuestionaireActivity(this);

        mSubmitClear.setVisibility(View.GONE);

        mSubmitClear.setOnClickListener((View v) -> {
            if (mSubmitClear.getText().equals(getString(R.string.clear))) {
                AlertDialogUtils.showConfirmDialog(SectionAreaActivity.this, "Clear", "Are you sure you want to clear the Audit?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mSectionAreaAdapter.clearAudit();
                        mSubmitClear.setVisibility(View.GONE);
                        if(isRoomAudit){
                            MixPanelManager.trackAuditEvent(mMixpanelApi,MixPanelManager.SCREEN_ROOM_AUDITS,
                                    MixPanelManager.EVENT_AUDITS_ROOM_CLEAR_TAP,Integer.toString(mHotelId),mHotelName,mRoomNo);
                        }else{
                            MixPanelManager.trackAuditEvent(mMixpanelApi,MixPanelManager.SCREEN_COMMON_AUDITS,
                                    MixPanelManager.EVENT_AUDITS_COMMON_CLEAR_TAP,Integer.toString(mHotelId),mHotelName);
                        }
                        setResult(Utils.AUDIT_CHANGED);
                    }
                });

            } else {
                if (!NetworkUtil.isConnected(SectionAreaActivity.this)) {
                    AlertDialogUtils.showInternetDisconnectedDialog(this,
                            (dialog, which) -> {
                                dialog.dismiss();
                                submitForm();
                                return;
                            }, (dialog) -> {
                                dialog.dismiss();
                                return;
                            }
                    );
                    return;
                }
                submitForm();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mSectionAreaAdapter = new SectionAreaAdapter(this, mAreaList, this, mHotelId, mRoomNo);
        if(savedInstanceState!=null){
            mSectionAreaAdapter.mQueryBeingAudited=savedInstanceState.getParcelable(Constants.CURRENT_BAD_QUERY);
            mSectionAreaAdapter.mCurrentBadAuditId=savedInstanceState.getInt(Constants.CURRENT_BAD_AUDIT_ID);
            mSectionAreaAdapter.mCurrentBadAuditPosition=savedInstanceState.getInt(Constants.CURRENT_BAD_AUDIT_POSITION);
        }

        mRecyclerView.setAdapter(mSectionAreaAdapter);
        if(mAreaList.getSubsections().size()<=1){
            mSlidingTabLayout.setVisibility(View.GONE);
        }else{
            mSlidingTabLayout.setVisibility(View.VISIBLE);
        }
        mSectionAreaAdapter.notifyDataSetChanged();
        mapAreaSectionsToPositions();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                int index = getSectionIndexForFirstVisibleItem(firstVisiblePosition);
                mViewPager.setCurrentItem(index);
            }
        });

        if(isRoomAudit){
            MixPanelManager.trackAuditEvent(mMixpanelApi,MixPanelManager.SCREEN_ROOM_AUDITS,
                    MixPanelManager.EVENT_AUDITS_ROOM_LOAD,Integer.toString(mHotelId),mHotelName,mRoomNo);
        }else{
            MixPanelManager.trackAuditEvent(mMixpanelApi,MixPanelManager.SCREEN_COMMON_AUDITS,
                    MixPanelManager.EVENT_AUDITS_COMMON_LOAD,Integer.toString(mHotelId),mHotelName);
        }

    }

    @Inject
    public void initView() {
        mAuditPresenter.setMvpView(this);
    }



    private void mapAreaSectionsToPositions() {
        mQuestionSectionPositionMap = MapperUtils.getSectionPositionHashMap(mAreaList);
        mQuestionSectionFirstPositionCache = MapperUtils.populateFirstVisibleSectionPositionsMap(mAreaList);
    }

    private int getSectionIndexForFirstVisibleItem(int firstVisiblePosition) {
        return mQuestionSectionPositionMap.get(firstVisiblePosition);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Utils.START_AUDIT:
                if (resultCode == RESULT_OK) {
                    UpdateQueryModel update = data.getParcelableExtra(Utils.AUDIT_BAD);
                    mSectionAreaAdapter.persistAuditRow(update.mComments);
                }
                if (resultCode == RESULT_CANCELED) {
                    mSectionAreaAdapter.notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constants.CURRENT_BAD_QUERY,mSectionAreaAdapter.mQueryBeingAudited);
        outState.putInt(Constants.CURRENT_BAD_AUDIT_ID,mSectionAreaAdapter.mCurrentBadAuditId);
        outState.putInt(Constants.CURRENT_BAD_AUDIT_POSITION,mSectionAreaAdapter.mCurrentBadAuditPosition);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mMixpanelApi.flush();
        super.onDestroy();
        if(mAuditUseCase != null){
            mAuditUseCase.unsubscribe();
        }
        if(mRoomsUseCase !=null){
            mRoomsUseCase.unsubscribe();
        }
    }

    @Override
    public void onAuditChanged() {
        setResult(Utils.AUDIT_CHANGED);
    }

    @Override
    public void onAuditStateChanged(boolean isSubmitEnabled, boolean isVisible) {
        mSubmitClear.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        mSubmitClear.setText(isSubmitEnabled ? getString(R.string.submit) : getString(R.string.clear));

    }


    @Override
    public void onAuditSubmitSuccess(SubmitAuditResponse response) {
        SnackbarUtils.show(mRootView, response.data.status);
        mAuditPreferenceManager.removeAudit(mPersistedKey);
        mAuditPreferenceManager.removeLastAuditKey(mPersistedKey);
        mRxBus.postEvent(new AuditIssueSubmitEvent("audit"));
        mAuditPresenter.loadRooms(mHotelId,mRoomsUseCase,providesHotelRoomObserver());
        SQLite.delete().from(Photo.class).where(Photo_Table.mKey.like("%" + mPersistedKey + "%")).async().execute();
        if (isRoomAudit) {
            MixPanelManager.trackAuditEvent(mMixpanelApi, MixPanelManager.SCREEN_ROOM_AUDITS,
                    MixPanelManager.EVENT_AUDITS_ROOM_SUBMIT_DONE, Integer.toString(mHotelId), mHotelName,mRoomNo);
        } else {
            MixPanelManager.trackAuditEvent(mMixpanelApi, MixPanelManager.SCREEN_COMMON_AUDITS,
                    MixPanelManager.EVENT_AUDITS_COMMON_SUBMIT_DONE, Integer.toString(mHotelId), mHotelName);
        }

    }

    @Override
    public void onAuditSubmitFailed(SubmitAuditResponse response) {
        SnackbarUtils.show(mRootView, response.data.status);
        if (isRoomAudit) {
            MixPanelManager.trackAuditStatusEvent(mMixpanelApi, MixPanelManager.SCREEN_ROOM_AUDITS,
                    MixPanelManager.EVENT_AUDITS_ROOM_SUBMIT_FAIL, Integer.toString(mHotelId), mHotelName,mRoomNo,response.status);
        } else {
            MixPanelManager.trackAuditStatusEvent(mMixpanelApi, MixPanelManager.SCREEN_COMMON_AUDITS,
                    MixPanelManager.EVENT_AUDITS_COMMON_SUBMIT_FAIL, Integer.toString(mHotelId), mHotelName,response.status);
        }
    }

    @Override
    public void onLoadHotelRooms(ArrayList<Pair<String, String>> scoreList, ArrayList<RoomTypeList> rooms) {
        mRxBus.postEvent(new RoomSubmittedEvent(rooms));
        mRxBus.postEvent(new UpdateBlindCountEvent(scoreList, rooms));
        finishWithDelay(1);
    }

    @Override
    public void onLoadHotelRoomsFailed(String msg) {
        SnackbarUtils.show(mRootView, msg);
    }


    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progressDialog.setTitle(getString(R.string.submitting_audit));
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        if (!isFinishing())
            progressDialog.show();
        mSubmitClear.setEnabled(true);
    }

    @Override
    public void hideLoading() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        MixPanelManager.trackStatusEvent(mMixpanelApi,
                isRoomAudit?MixPanelManager.SCREEN_ROOM_AUDITS:MixPanelManager.SCREEN_COMMON_AUDITS,
                MixPanelManager.EVENT_AUDITS_ERROR,errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(this, ((dialog, which) -> {
                    dialog.dismiss();
                    submitForm();
                }),
                (dialog1 -> {
                    dialog1.dismiss();
                }));
    }

    @Override
    public void onAuditRowUpdated() {
        Form savedLastAuditForm= mAuditPreferenceManager.getPersistedAudit(getKey());
        if(savedLastAuditForm!=null){
            float percentage= AuditUtils.getPercentTaskRemainingInAudit(savedLastAuditForm);
            PercentRelativeLayout.LayoutParams params = (PercentRelativeLayout.LayoutParams)completed.getLayoutParams();
            PercentLayoutHelper.PercentLayoutInfo info = params.getPercentLayoutInfo();
            info.widthPercent =((100.0f-percentage)/100.0f);
            completed.requestLayout();

            PercentRelativeLayout.LayoutParams params1 = (PercentRelativeLayout.LayoutParams)left.getLayoutParams();
            PercentLayoutHelper.PercentLayoutInfo info1= params1.getPercentLayoutInfo();
            info1.widthPercent =(percentage)/100.0f;
            left.requestLayout();
        }else{
            PercentRelativeLayout.LayoutParams params = (PercentRelativeLayout.LayoutParams)completed.getLayoutParams();
            PercentLayoutHelper.PercentLayoutInfo info = params.getPercentLayoutInfo();
            info.widthPercent =0.0f;
            completed.requestLayout();

            PercentRelativeLayout.LayoutParams params1 = (PercentRelativeLayout.LayoutParams)left.getLayoutParams();
            PercentLayoutHelper.PercentLayoutInfo info1= params1.getPercentLayoutInfo();
            info1.widthPercent =100.0f;
            left.requestLayout();
        }
    }

    private void submitForm() {
        String sectionName = mAreaList.getSection();
        boolean isRoomAudit= sectionName.equalsIgnoreCase("Room");
        if (sectionName.equalsIgnoreCase("Room")) {
            sectionName = sectionName + " " + mRoomNo;
        }

        Form savedForm;
        if (sectionName.startsWith("Room") || sectionName.startsWith("ROOM")) {
            savedForm = AuditUtils.getPausedRoomAreaAudit(Integer.toString(mHotelId), mRoomNo);
            mPersistedKey = Integer.toString(mHotelId) + "_" + mRoomNo;
        } else {
            savedForm = AuditUtils.getPausedCommonAreaAudit(Integer.toString(mHotelId));
            mPersistedKey = Integer.toString(mHotelId);
        }
        if (savedForm == null) {
            Log.e("Save", "Form not Saved");
            return;
        }

        List<Photo> photoList= SQLite.select().from(Photo.class).where(Photo_Table.mKey.like("%"+mPersistedKey+"%")).queryList();

        if (!NetworkUtil.isConnected(this)) {
            SnackbarUtils.show(mRootView, Constants.OFFLINE_SUBMIT_FORM_NO_INTERNET);
            submitOfflineAuditJob(isRoomAudit,sectionName,Integer.toString(mHotelId));
            if (isRoomAudit) {
                MixPanelManager.trackAuditEvent(mMixpanelApi, MixPanelManager.SCREEN_ROOM_AUDITS,
                        MixPanelManager.EVENT_AUDITS_ROOM_SUBMIT_OFFLINE_TAP, Integer.toString(mHotelId), mHotelName);
            } else {
                MixPanelManager.trackAuditEvent(mMixpanelApi, MixPanelManager.SCREEN_COMMON_AUDITS,
                        MixPanelManager.EVENT_AUDITS_COMMON_SUBMIT_OFFLINE_TAP, Integer.toString(mHotelId), mHotelName);
            }
            finishWithDelay(4);
            return;
        }

        //check for any pending photo uploads. if pending send to job queue
        if(PhotoUtils.isPhotoUploadsPending(photoList)){
            SnackbarUtils.show(mRootView, Constants.OFFLINE_SUBMIT_PHOTOS_UPLOADS_PENDING);
            submitOfflineAuditJob(isRoomAudit,sectionName,Integer.toString(mHotelId));
            if (isRoomAudit) {
                MixPanelManager.trackAuditEvent(mMixpanelApi, MixPanelManager.SCREEN_ROOM_AUDITS,
                        MixPanelManager.EVENT_AUDITS_ROOM_SUBMIT_OFFLINE_TAP, Integer.toString(mHotelId), mHotelName);
            } else {
                MixPanelManager.trackAuditEvent(mMixpanelApi, MixPanelManager.SCREEN_COMMON_AUDITS,
                        MixPanelManager.EVENT_AUDITS_COMMON_SUBMIT_OFFLINE_TAP, Integer.toString(mHotelId), mHotelName);
            }
            finishWithDelay(4);
            return;
        }

        //link photos to form
        PhotoUtils.addPhotoUrlsToForm(photoList,savedForm,mPersistedKey);
        mAuditPreferenceManager.removeAndPersistAudit(mPersistedKey,savedForm);

        AnswerListWithSectionName answerListWithSectionName
                = AuditUtils.convertFormtoSubmit(savedForm, sectionName);
       mAuditPresenter.onAuditSubmit(answerListWithSectionName,
                mHotelId,
                mLoginSharedPrefManager.getFormId(),
                mLoginSharedPrefManager.getUserId(),
                "QA",mAuditUseCase,providesAuditSubmitObserver());
        if (isRoomAudit) {
            MixPanelManager.trackAuditEvent(mMixpanelApi, MixPanelManager.SCREEN_ROOM_AUDITS,
                    MixPanelManager.EVENT_AUDITS_ROOM_SUBMIT_TAP, Integer.toString(mHotelId), mHotelName);
        } else {
            MixPanelManager.trackAuditEvent(mMixpanelApi, MixPanelManager.SCREEN_COMMON_AUDITS,
                    MixPanelManager.EVENT_AUDITS_COMMON_SUBMIT_TAP, Integer.toString(mHotelId), mHotelName);
        }

    }


    public void finishWithDelay(int seconds){
        Observable.timer(seconds, TimeUnit.SECONDS)
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onCompleted() {
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Long number) {

                    }
                });
    }


    @Override
    public void onTabSelected(int newPos) {
        int headerPosition = getHeaderPositionForTab(newPos);
        ((LinearLayoutManager) mRecyclerView.getLayoutManager()).scrollToPositionWithOffset(headerPosition, 0);
    }

    private int getHeaderPositionForTab(int newPos) {
        return mQuestionSectionFirstPositionCache.get(newPos);
    }
    private String getKey(){
        if(mPersistedKey!=null){
            return mPersistedKey;
        }
        String sectionName = mAreaList.getSection();
        if (sectionName.equalsIgnoreCase("room")) {
            mPersistedKey=Integer.toString(mHotelId)+"_"+mRoomNo;
        } else {
            mPersistedKey=Integer.toString(mHotelId);
        }
        return mPersistedKey;
    }

    public void submitOfflineAuditJob(boolean isRoomAudit,String sectionName,String hotelId) {
        PersistableBundleCompat extras = new PersistableBundleCompat();
        extras.putBoolean(AuditJob.IS_ROOM_AUDIT, isRoomAudit);
        extras.putString(AuditJob.SECTION_NAME,sectionName);
        extras.putString(Utils.HOTEL_ID, hotelId);
        extras.putString(Utils.HOTEL_NAME, mHotelName);
        extras.putString(AuditJob.AUDIT_SUBMIT_TIME,new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

        if(isRoomAudit){
            extras.putString(Utils.ROOM_NAME,mRoomNo);
        }

        new JobRequest.Builder(AuditJob.TAG+"_"+mPersistedKey)
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(30_000L, JobRequest.BackoffPolicy.EXPONENTIAL)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setExtras(extras)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public HotelRoomObserver providesHotelRoomObserver(){
        return new HotelRoomObserver(mAuditPresenter,mRoomsUseCase,this);
    }

    public AuditSubmitObserver providesAuditSubmitObserver(){
        return new AuditSubmitObserver(mAuditPresenter,mAuditUseCase,this);
    }

}
