package com.treebo.prowlapp.flowaudit.observers;

import android.util.Pair;

import com.treebo.prowlapp.flowaudit.usecase.HotelRoomsUseCase;
import com.treebo.prowlapp.Models.Audit;
import com.treebo.prowlapp.mvpviews.RoomAreaView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.HotelRoomResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

import java.util.ArrayList;

/**
 * Created by sumandas on 18/08/2016.
 */
@Deprecated
public class HotelRoomObserver extends BaseObserver<HotelRoomResponse> {

    HotelRoomsUseCase mHotelRoomsUseCase;
    RoomAreaView mRoomView;

    public HotelRoomObserver(BasePresenter presenter,
                             HotelRoomsUseCase hotelRoomsUseCase, RoomAreaView view) {
        super(presenter, hotelRoomsUseCase);
        mHotelRoomsUseCase=hotelRoomsUseCase;
        mRoomView=view;

    }

    @Override
    public void onCompleted() {
        mRoomView.hideLoading();
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        mRoomView.showError(e.getMessage());
    }

    @Override
    public void onNext(HotelRoomResponse response) {
        mRoomView.hideLoading();
        if(response.status.equals(Constants.STATUS_SUCCESS)){
            Audit auditData = response.data;
            ArrayList<Pair<String, String>> scoreList = new ArrayList<>();
            scoreList.add(new Pair("Blind Rooms", auditData.blind_rooms_count));
            scoreList.add(new Pair("Completion Ratio", auditData.completion_ratio));
            mRoomView.onLoadHotelRooms(scoreList, auditData.types);
        }else{
            mRoomView.onLoadHotelRoomsFailed(response.msg);
        }


    }
}
