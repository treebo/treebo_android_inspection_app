package com.treebo.prowlapp.flowaudit;

import com.treebo.prowlapp.flowaudit.presenter.AuditSubmitPresenter;
import com.treebo.prowlapp.flowaudit.presenter.AuditSubmitPresenterImpl;
import com.treebo.prowlapp.flowaudit.presenter.HotelAuditPresenter;
import com.treebo.prowlapp.flowaudit.presenter.HotelAuditPresenterImpl;
import com.treebo.prowlapp.flowaudit.usecase.AuditUseCase;
import com.treebo.prowlapp.flowaudit.usecase.FormMetaDataUseCase;
import com.treebo.prowlapp.flowaudit.usecase.HotelRoomsUseCase;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 08/08/2016.
 */
@Deprecated
@Module
public class AuditModule {

    @Provides
    HotelAuditPresenter providesPortFolioPresenter(){
        return new HotelAuditPresenterImpl();
    }

    @Provides
    AuditSubmitPresenter providesAuditSubmitPresenter(){
        return new AuditSubmitPresenterImpl();
    }

    @Provides
    RestClient providesRestClient(){
        return new RestClient();
    }

    @Provides
    HotelRoomsUseCase providesHotelRoomUseCase(RestClient restClient){
        return new HotelRoomsUseCase(restClient);
    }

    @Provides
    AuditUseCase providesAuditUseCase(RestClient restClient){
        return  new AuditUseCase(restClient);
    }

    @Provides
    FormMetaDataUseCase providesFormDataUseCase(RestClient restClient){
        return  new FormMetaDataUseCase(restClient);
    }
}
