package com.treebo.prowlapp.flowaudit.usecase;

import com.treebo.prowlapp.Models.AnswerListWithSectionName;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by sumandas on 03/05/2016.
 */
public class AuditUseCase extends UseCase<SubmitAuditResponse> {

    RestClient mRestClient;

    public AuditUseCase(RestClient restClient){
        mRestClient=restClient;
    }


    public void setFields(AnswerListWithSectionName answer, String hotelId, String formId,
                        String userId, String source) {
        this.answer=answer;
        this.hotelId=hotelId;
        this.userId=userId;
        this.formId=formId;
        this.source=source;
    }

    AnswerListWithSectionName answer;
    String hotelId;
    String userId;
    String formId;
    String source;


    @Override
    protected Observable<SubmitAuditResponse> getObservable() {
        return mRestClient.submitAudit(answer,hotelId,userId,
                 formId,source);
    }

}
