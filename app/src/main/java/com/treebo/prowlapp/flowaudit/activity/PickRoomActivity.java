package com.treebo.prowlapp.flowaudit.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.RoomSubmittedEvent;
import com.treebo.prowlapp.flowaudit.AuditComponent;
import com.treebo.prowlapp.flowaudit.DaggerAuditComponent;
import com.treebo.prowlapp.flowaudit.adapter.PausedTaskAdapter;
import com.treebo.prowlapp.flowaudit.adapter.SectionedRoomAdapter;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.Room;
import com.treebo.prowlapp.Models.RoomTypeList;
import com.treebo.prowlapp.mvpviews.HotelAuditView;
import com.treebo.prowlapp.response.FormDataResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AuditUtils;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.ButterKnife;
import rx.Subscriber;

/**
 * Created by sumandas on 28/04/2016.
 */
@Deprecated
public class PickRoomActivity extends AppCompatActivity implements SectionedRoomAdapter.OnRoomClickedListener,PausedTaskAdapter.OnRoomClickedListener,HotelAuditView {

    private Toolbar mToolBar;
    private SectionedRoomAdapter mSectionRoomAdapter;
    private PausedTaskAdapter mPausedAdapter;
    
    private RecyclerView mPauseList;
    private RecyclerView mRoomList;

    private ArrayList<RoomTypeList> mRooms;

    private Form mAreas;
    private int mHotelId;
    private String mHotelName="";

    private boolean isRoomAuditDone;

    private HashMap<Room,Form> mPausedRoomMap;

    private ArrayList<Pair<Room,String>> mScoreList=new ArrayList<>();

    @Inject
    public ProgressDialog progressDialog;
    private View mRootView;

    @Inject
    public RxBus mBus;

    @Inject
    public MixpanelAPI mixpanelAPI;

    public OnRoomSubmittedSubsciber onRoomSubmittedSubsciber;
    private boolean isInForeground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_room);
        ButterKnife.bind(this);

        mToolBar=(Toolbar)findViewById(R.id.toolbar);
        mPauseList =(RecyclerView)findViewById(R.id.paused_tasks);
        mRoomList =(RecyclerView)findViewById(R.id.room_section_list);

        Intent intent=getIntent();
        mRooms=intent.getParcelableArrayListExtra(Utils.ROOM_OBJECT);
        if(mRooms==null){
            mRooms=new ArrayList<>();
        }
        mAreas=intent.getParcelableExtra(Utils.FORM_OBJECT);
        if(mAreas==null) {
            mAreas = new Form();

        }
        mPausedRoomMap=new HashMap<>();
        mHotelId=intent.getIntExtra(Utils.HOTEL_ID, 0);
        mHotelName=getIntent().getStringExtra(Utils.HOTEL_NAME);

        mToolBar.setTitleTextColor(getResources().getColor(R.color.white));
        mToolBar.setTitle(R.string.pick_a_room);

        updatePausedState();

        mToolBar.setNavigationIcon(R.drawable.ic_back_white);
        mToolBar.setNavigationOnClickListener((View v) -> finish());


        mRootView=findViewById(R.id.audit_room_root_view);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mPauseList.setLayoutManager(layoutManager);
        mPauseList.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);

        mRoomList.setLayoutManager(layoutManager1);
        mRoomList.setItemAnimator(new DefaultItemAnimator());

        mSectionRoomAdapter=new SectionedRoomAdapter(this,mRooms,this);
        mRoomList.setAdapter(mSectionRoomAdapter);

        mSectionRoomAdapter.notifyDataSetChanged();

        mPausedAdapter=new PausedTaskAdapter(this,mScoreList,this);
        mPauseList.setAdapter(mPausedAdapter);
        mPausedAdapter.notifyDataSetChanged();

        AuditComponent auditComponent= DaggerAuditComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectRoomActivity(this);

        onRoomSubmittedSubsciber=new OnRoomSubmittedSubsciber();
        mBus.toObservable().subscribe(onRoomSubmittedSubsciber);

    }


    @Override
    protected void onResume(){
        isInForeground=true;
        super.onResume();
        updatePausedState();
        mPausedAdapter.notifyDataSetChanged();
        if(isRoomAuditDone){
            isRoomAuditDone=false;
            mSectionRoomAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        isInForeground=false;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        onRoomSubmittedSubsciber.unsubscribe();
    }

    @Override
    public void onRoomClicked(Room roomClicked) {
        Intent intent = new Intent(this, SectionAreaActivity.class);
        intent.putExtra(Utils.AREA_NAME, getString(R.string.room_no)+" "+roomClicked.number);
        intent.putExtra(Utils.FORM_OBJECT, mAreas);
        intent.putExtra(Utils.ROOM_OBJECT, roomClicked);
        intent.putExtra(Utils.ROOM_NAME,roomClicked.number);
        intent.putExtra(Utils.HOTEL_ID, mHotelId);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        MixPanelManager.trackAuditEvent(mixpanelAPI,MixPanelManager.SCREEN_ROOM_LIST,
                MixPanelManager.EVENT_AUDITS_ROOM_NUMBER_TAP,Integer.toString(mHotelId),mHotelName,roomClicked.number);
        startActivityForResult(intent, Utils.START_AUDIT);
    }

    @Override
    public void onPausedRoomClicked(String roomClicked) {
        Intent intent = new Intent(this, SectionAreaActivity.class);
        intent.putExtra(Utils.AREA_NAME, getString(R.string.room_no)+" "+roomClicked);
        intent.putExtra(Utils.FORM_OBJECT, mAreas);
        intent.putExtra(Utils.ROOM_NAME, roomClicked);
        intent.putExtra(Utils.HOTEL_ID,mHotelId);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        MixPanelManager.trackAuditEvent(mixpanelAPI,MixPanelManager.SCREEN_ROOM_LIST,
                MixPanelManager.EVENT_AUDITS_ROOM_NUMBER_TAP,Integer.toString(mHotelId),mHotelName,roomClicked);
        startActivityForResult(intent, Utils.START_AUDIT);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case Utils.START_AUDIT:
                if(resultCode==Utils.AUDIT_CHANGED){
                    setResult(Utils.AUDIT_CHANGED);
                }
                break;
        }
    }
    public void updatePausedState(){
        mPausedRoomMap.clear();
        mScoreList.clear();
        String hotelId=Integer.toString(mHotelId);
        for(RoomTypeList roomType:mRooms){
            for(Room room:roomType.rooms){
                Form roomSaved= AuditUtils.getPausedRoomAreaAudit(hotelId,room.number);
                if(roomSaved!=null){
                    mPausedRoomMap.put(room,roomSaved);
                }
            }
        }
        for(Room room:mPausedRoomMap.keySet()){
            Form saved=mPausedRoomMap.get(room);
            mScoreList.add(new Pair(room, AuditUtils.getPercentageTaskRemainingInAudit(saved)));
        }
    }


    @Override
    public void onLoadHotelRooms(ArrayList<Pair<String, String>> scoreList, ArrayList<RoomTypeList> rooms) {
       mRooms=rooms;
    }


    @Override
    public void onLoadHotelRoomsFailed(String msg) {
        SnackbarUtils.show(mRootView, msg);
    }

    @Override
    public void onLoadHotelAuditAreas(FormDataResponse formDataResponse) {
        //not supported
    }

    @Override
    public void onLoadHotelAuditAreaFailed(String msg) {
        //not supported
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {

        progressDialog.setTitle(getString(R.string.loading_rooms));
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        if(!this.isFinishing())
            progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    public class OnRoomSubmittedSubsciber extends Subscriber<Object> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(Object event) {
            if(event instanceof RoomSubmittedEvent){
                mSectionRoomAdapter.mRoomTypeList=((RoomSubmittedEvent)event).mRoomList;
                if(isInForeground){
                    runOnUiThread(() -> {
                        mSectionRoomAdapter.notifyDataSetChanged();
                        updatePausedState();
                        mPausedAdapter.notifyDataSetChanged();
                    });

                }else{
                    isRoomAuditDone=true;
                }
            }
        }

    }
}