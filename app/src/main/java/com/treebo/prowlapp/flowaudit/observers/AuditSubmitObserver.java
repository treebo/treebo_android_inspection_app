package com.treebo.prowlapp.flowaudit.observers;

import com.treebo.prowlapp.flowaudit.usecase.AuditUseCase;
import com.treebo.prowlapp.mvpviews.AuditAreaView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;

/**
 * Created by sumandas on 18/08/2016.
 */
@Deprecated
public class AuditSubmitObserver extends BaseObserver<SubmitAuditResponse> {

    AuditUseCase mAuditUseCase;
    AuditAreaView mView;

    public AuditSubmitObserver(BasePresenter presenter, AuditUseCase auditUseCase, AuditAreaView view) {
        super(presenter, auditUseCase);
        mAuditUseCase = auditUseCase;
        mView = view;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
    }

    @Override
    public void onNext(SubmitAuditResponse response) {
        mView.hideLoading();
        if (response.status.equals("success")) {
            mView.onAuditSubmitSuccess(response);
        } else {
            mView.onAuditSubmitFailed(response);
        }
    }
}