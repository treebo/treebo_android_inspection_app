package com.treebo.prowlapp.flowaudit.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import android.util.Pair;
import android.view.View;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.UpdateBlindCountEvent;
import com.treebo.prowlapp.flowaudit.AuditComponent;
import com.treebo.prowlapp.flowaudit.DaggerAuditComponent;
import com.treebo.prowlapp.flowaudit.adapter.AuditAreaSectionAdapter;
import com.treebo.prowlapp.flowaudit.observers.HotelAuditAreaObserver;
import com.treebo.prowlapp.flowaudit.observers.HotelRoomObserver;
import com.treebo.prowlapp.flowaudit.presenter.HotelAuditPresenter;
import com.treebo.prowlapp.flowaudit.usecase.FormMetaDataUseCase;
import com.treebo.prowlapp.flowaudit.usecase.HotelRoomsUseCase;
import com.treebo.prowlapp.flowscores.adapter.AuditScoresRecyclerAdapter;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.RoomTypeList;
import com.treebo.prowlapp.mvpviews.HotelAuditView;
import com.treebo.prowlapp.response.FormDataResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.Subscriber;


/**
 * Created by sumandas on 26/04/2016.
 */
@Deprecated
public class HotelAuditActivity extends AppCompatActivity implements AuditAreaSectionAdapter.OnAreaClickedListener,HotelAuditView {

    private Toolbar mToolBar;
    private RecyclerView mListScore;
    private RecyclerView mListArea;
    private AuditScoresRecyclerAdapter mAuditScoreAdapter;
    private AuditAreaSectionAdapter mAuditAreaAdapter;

    @Inject
    public HotelAuditPresenter mHotelAuditPresenter;
    private ArrayList<Form> mAreas;
    private ArrayList<RoomTypeList> mRooms=new ArrayList<>();

    @Inject
    public ProgressDialog progressDialog;
    boolean isAuditChanged;


    ArrayList<Pair<String, String>> mScoreList;
    private View mRootView;

    private int mHotelId;
    private String mHotelName="";
    public boolean isBlindCountUpdated;

    private boolean isInForeground;

    @Inject
    public RxBus mBus;

    @Inject
    MixpanelAPI mixpanelAPI;

    @Inject
    FormMetaDataUseCase mFormMetaDataUseCase;

    @Inject
    HotelRoomsUseCase mRoomsUseCase;

    @Inject
    LoginSharedPrefManager mLoginManager;

    public OnRoomSubmittedSubsciber onRoomSubmittedSubsciber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_main);
        mToolBar=(Toolbar)findViewById(R.id.toolbar);
//        mListScore =(RecyclerView)findViewById(R.id.listView_scores);
        mListArea =(RecyclerView)findViewById(R.id.listView_areas);
        mRootView=findViewById(R.id.audit_root_view);


        mHotelId=getIntent().getIntExtra(Utils.HOTEL_ID, 0);
        mHotelName=getIntent().getStringExtra(Utils.HOTEL_NAME);

        mToolBar.setNavigationIcon(R.drawable.ic_back_white);
        mToolBar.setTitleTextColor(getResources().getColor(R.color.white));
        mToolBar.setNavigationOnClickListener((View v) -> finish());
        mToolBar.setTitle(R.string.title_audits);

//        mListScore = (RecyclerView)findViewById(R.id.listView_scores);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mListScore.setLayoutManager(layoutManager);
        mListScore.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        mListArea.setLayoutManager(layoutManager1);
        mListArea.setItemAnimator(new DefaultItemAnimator());

       AuditComponent auditComponent= DaggerAuditComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectAuditActivity(this);

        onRoomSubmittedSubsciber=new OnRoomSubmittedSubsciber();
        mBus.toObservable().subscribe(onRoomSubmittedSubsciber);

        mHotelAuditPresenter.loadRooms(mHotelId,mRoomsUseCase,providesHotelRoomObserver());

        MixPanelManager.trackAuditEvent(mixpanelAPI, MixPanelManager.SCREEN_AUDITS,
                MixPanelManager.EVENT_AUDITS_LOAD, Integer.toString(mHotelId),mHotelName);

    }

    @Inject
    public void initView() {
        mHotelAuditPresenter.setMvpView(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        isInForeground=true;
        if(isBlindCountUpdated ){
            isBlindCountUpdated=false;
            if(mAuditScoreAdapter!=null){
                mAuditScoreAdapter.notifyDataSetChanged();
            }
        }
        if(isAuditChanged){
            isAuditChanged=false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(mAuditAreaAdapter!=null){
                        mAuditAreaAdapter.notifyDataSetChanged();
                    }
                }
            }, 40);
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        isInForeground=false;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        onRoomSubmittedSubsciber.unsubscribe();
        if(mFormMetaDataUseCase != null){
            mFormMetaDataUseCase.unsubscribe();
        }
        if(mRoomsUseCase != null){
            mRoomsUseCase.unsubscribe();
        }
    }


    @Override
    public void onAreaClicked(int position) {
        Intent intent;
        Form formClicked=mAreas.get(position);
        if(formClicked.getSection().equalsIgnoreCase("room")){
            intent = new Intent(this, PickRoomActivity.class);
            intent.putParcelableArrayListExtra(Utils.ROOM_OBJECT, mRooms);
            intent.putExtra(Utils.FORM_OBJECT, mAreas.get(position));
            intent.putExtra(Utils.HOTEL_ID, mHotelId);
            intent.putExtra(Utils.HOTEL_NAME, mHotelName);
            MixPanelManager.trackAuditEvent(mixpanelAPI,MixPanelManager.SCREEN_AUDITS,
                    MixPanelManager.EVENT_AUDITS_ROOM_TAP,Integer.toString(mHotelId),mHotelName);
            startActivityForResult(intent, Utils.START_AUDIT);

        }else{
            intent = new Intent(this, SectionAreaActivity.class);
            intent.putExtra(Utils.AREA_NAME,  mAreas.get(position).getSection());
            intent.putExtra(Utils.FORM_OBJECT, mAreas.get(position));
            intent.putExtra(Utils.HOTEL_ID,mHotelId);
            intent.putExtra(Utils.HOTEL_NAME, mHotelName);
            MixPanelManager.trackAuditEvent(mixpanelAPI,MixPanelManager.SCREEN_AUDITS,
                    MixPanelManager.EVENT_AUDITS_COMMON_TAP,Integer.toString(mHotelId),mHotelName);
            startActivityForResult(intent, Utils.START_AUDIT);
        }
    }

    @Override
    public void onPausedRoomClicked(String hotelID, String roomNo,int position) {
        Intent intent = new Intent(this, SectionAreaActivity.class);
        intent.putExtra(Utils.AREA_NAME, getString(R.string.room_no)+" "+roomNo);
        intent.putExtra(Utils.FORM_OBJECT, mAreas.get(position));
        intent.putExtra(Utils.ROOM_NAME,roomNo);
        intent.putExtra(Utils.HOTEL_ID,mHotelId);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        MixPanelManager.trackAuditEvent(mixpanelAPI,MixPanelManager.SCREEN_AUDITS,
                MixPanelManager.EVENT_AUDITS_ROOM_TAP,hotelID,mHotelName,roomNo);
        startActivityForResult(intent, Utils.START_AUDIT);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case Utils.START_AUDIT:
                if(resultCode==Utils.AUDIT_CHANGED){
                    isAuditChanged=true;
                }
                break;
        }
    }

    @Override
    public void onLoadHotelRooms(ArrayList<Pair<String, String>> scoreList, ArrayList<RoomTypeList> rooms) {
        mScoreList=scoreList;
        mAuditScoreAdapter =new AuditScoresRecyclerAdapter(this,mScoreList);
        mListScore.setAdapter(mAuditScoreAdapter);
        mAuditScoreAdapter.notifyDataSetChanged();
        mRooms=rooms;
        showLoading();
        mHotelAuditPresenter.loadAreas(mFormMetaDataUseCase,providesHotelAuditAreaObserver());
    }

    @Override
    public void onLoadHotelRoomsFailed(String msg) {
        SnackbarUtils.show(mRootView, msg);
    }

    @Override
    public void onLoadHotelAuditAreas(FormDataResponse response) {
        mLoginManager.setFormId(response.data.getId());
        List<Form> form=response.data.getForm();
        mAreas=(ArrayList<Form>)form;
        mAuditAreaAdapter= new AuditAreaSectionAdapter(this,this,form,mRooms,Integer.toString(mHotelId));
        mAuditAreaAdapter.shouldShowHeadersForEmptySections(true);
        mListArea.setAdapter(mAuditAreaAdapter);
        mAuditAreaAdapter.mRooms=mRooms;
        mAuditAreaAdapter.notifyDataSetChanged();

    }

    @Override
    public void onLoadHotelAuditAreaFailed(String msg) {
        SnackbarUtils.show(mRootView, msg);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        if(!isFinishing())
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }
    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, HotelAuditActivity.class);
    }

    public class OnRoomSubmittedSubsciber extends Subscriber<Object> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(Object event) {
            if(event instanceof UpdateBlindCountEvent){
                mAuditScoreAdapter.mAuditScores=((UpdateBlindCountEvent)event).mScorelist;
                mRooms=((UpdateBlindCountEvent)event).mRooms;
                if(isInForeground){
                    runOnUiThread(() -> {
                        if(mAuditScoreAdapter!=null){
                            mAuditScoreAdapter.notifyDataSetChanged();
                        }
                        if(mAuditAreaAdapter!=null){
                            mAuditAreaAdapter.notifyDataSetChanged();
                        }

                    });

                }else{
                    isBlindCountUpdated=true;
                    isAuditChanged=true;
                }

            }
        }

    }

    public HotelAuditAreaObserver providesHotelAuditAreaObserver(){
        return new HotelAuditAreaObserver(mHotelAuditPresenter,mFormMetaDataUseCase,this);

    }

    public HotelRoomObserver providesHotelRoomObserver(){
        return new HotelRoomObserver(mHotelAuditPresenter,mRoomsUseCase,this);
    }
}
