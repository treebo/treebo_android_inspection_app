package com.treebo.prowlapp.flowaudit.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.Room;
import com.treebo.prowlapp.Models.RoomTypeList;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 28/04/2016.
 */
@Deprecated
public class SectionedRoomAdapter  extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    public List<RoomTypeList> mRoomTypeList;
    public OnRoomClickedListener mRoomClickListener;
    public Context mContext;

    public SectionedRoomAdapter(Context context, List<RoomTypeList> areaSections, OnRoomClickedListener listener){
       if(areaSections==null){
            mRoomTypeList=new ArrayList<>();
        }else{
            mRoomTypeList =areaSections;
        }
        mRoomClickListener=listener;
        mContext=context;
    }

    @Override
    public int getSectionCount() {
        return mRoomTypeList.size();
    }

    @Override
    public int getItemCount(int section) {
        return mRoomTypeList.get(section).rooms.size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder  holder, int section) {
        String sectionName = mRoomTypeList.get(section).type;
        ((SectionedViewHolder)holder).mSectionTitle.setText(sectionName);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int relativePosition, int absolutePosition) {
        List<Room> itemsInSection = mRoomTypeList.get(section).rooms;
        String itemName = itemsInSection.get(relativePosition).number;
        String color=itemsInSection.get(relativePosition).color;
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.mRoomNumber.setText(itemName);
        itemViewHolder.divider.setVisibility(relativePosition == getItemCount(section) - 1 ? View.INVISIBLE : View.VISIBLE);
        itemViewHolder.mRoomNumber.setTextColor(mContext.getResources().getColor(Utils.getRoomColor(color)));
        itemViewHolder.mRoomNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SectionedRoomAdapter.this.mRoomClickListener.onRoomClicked(itemsInSection.get(relativePosition));
            }
        });

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch(viewType){
            case VIEW_TYPE_HEADER:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_area_header, parent, false);
                return new SectionedViewHolder(v);
            case VIEW_TYPE_ITEM:
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_room_item, parent, false);
                return new ItemViewHolder(v);
        }

    }

    public static class SectionedViewHolder extends RecyclerView.ViewHolder {

        public TextView mSectionTitle;

        public SectionedViewHolder(View itemView) {
            super(itemView);
            mSectionTitle=(TextView)itemView.findViewById(R.id.section_header_title);
        }
    }

    public  class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mRoomNumber;
        public View divider;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRoomNumber = (TextView) itemView.findViewById(R.id.section_room_name);
            divider=itemView.findViewById(R.id.room_divider);

        }
    }

    public interface OnRoomClickedListener{
        void onRoomClicked(Room roomClicked);
    }
}
