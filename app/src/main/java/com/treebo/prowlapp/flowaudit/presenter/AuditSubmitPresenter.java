package com.treebo.prowlapp.flowaudit.presenter;

import com.treebo.prowlapp.flowaudit.observers.AuditSubmitObserver;
import com.treebo.prowlapp.flowaudit.observers.HotelRoomObserver;
import com.treebo.prowlapp.flowaudit.usecase.AuditUseCase;
import com.treebo.prowlapp.flowaudit.usecase.HotelRoomsUseCase;
import com.treebo.prowlapp.Models.AnswerListWithSectionName;
import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by sumandas on 03/05/2016.
 */
@Deprecated
public interface AuditSubmitPresenter extends BasePresenter {
    void onAuditSubmit(AnswerListWithSectionName answer, int hotelId, int formId,
                       int userId, String source, AuditUseCase auditUseCase,
                       AuditSubmitObserver auditSubmitObserver);

    void loadRooms(int hotelId, HotelRoomsUseCase hotelRoomsUseCase, HotelRoomObserver hotelRoomObserver);

}
