package com.treebo.prowlapp.flowaudit.adapter;

import android.content.Context;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.RoomTypeList;
import com.treebo.prowlapp.Utils.AuditUtils;
import com.treebo.prowlapp.Utils.StringUtils;
import com.treebo.prowlapp.views.ProgressPercentView;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 05/06/2016.
 */
@Deprecated
public class AuditAreaSectionAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {


    private static final String TAG = AuditAreaSectionAdapter.class.getSimpleName();

    public List<Form> mAuditAreas;
    private Context mContext;
    private OnAreaClickedListener mAreaClickedListener;
    private String mHotelId;
    public ArrayList<RoomTypeList> mRooms;
    ArrayList<Pair<String,Form>> mPausedRoomAudits =new ArrayList<>();

    public AuditAreaSectionAdapter(Context context, OnAreaClickedListener areaClickedListener , List<Form> auditScores, ArrayList<RoomTypeList> rooms,String hotelId) {
        mAuditAreas = auditScores;
        mRooms=rooms;
        mContext = context;
        mAreaClickedListener=areaClickedListener;
        mHotelId=hotelId;
    }
    @Override
    public int getSectionCount() {
        return mAuditAreas.size();
    }

    @Override
    public int getItemCount(int section) {
        Form form=mAuditAreas.get(section);
        if((form.getSection().startsWith("Common")|| form.getSection().startsWith("COMMON"))){
            Form commonSaved= AuditUtils.getPausedCommonAreaAudit(mHotelId);
            if(commonSaved==null){
                return 0;
            }else{
                return 1;
            }
        }else if(form.getSection().equalsIgnoreCase("room")){
            mPausedRoomAudits =AuditUtils.getPausedRoomAudits(mHotelId,mRooms);
            return mPausedRoomAudits.size();
        }
        return 0;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int section) {
        final Form form = mAuditAreas.get(section);
        ((SectionedViewHolder) viewHolder).mSectionTitle.setText(StringUtils.capitalizeFirstLetter(form.getSection().toLowerCase()));
        ((SectionedViewHolder) viewHolder).rootHeaderView.setOnClickListener(v -> {
            AuditAreaSectionAdapter.this.mAreaClickedListener.onAreaClicked(section);
        });
        ((SectionedViewHolder) viewHolder).divider.setVisibility((getItemCount(section)>0)?View.INVISIBLE:View.VISIBLE);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,final int section,final int relativePosition,final int absolutePosition) {
        final Form form = mAuditAreas.get(section);

        if((form.getSection().startsWith("Common")|| form.getSection().startsWith("COMMON"))){
            ((IndividualViewHolder) holder).roomNo.setVisibility(View.GONE);
            Form commonSaved= AuditUtils.getPausedCommonAreaAudit(mHotelId);
            if(commonSaved!=null){
                ((IndividualViewHolder) holder).mPAusedTasksInfo.setText((String.format(mContext.getResources().getString(R.string.paused_audit_task),
                        AuditUtils.getPercentageTaskRemainingInAudit(commonSaved))));
                float percentage= AuditUtils.getPercentTaskRemainingInAudit(commonSaved);
                ((IndividualViewHolder)holder).progressBar.setVisibility(View.VISIBLE);
                ((IndividualViewHolder)holder).progressBar.setProgressUpdate(percentage);
            }
            ((IndividualViewHolder) holder).rootView.setOnClickListener((View v)->
                    AuditAreaSectionAdapter.this.mAreaClickedListener.onAreaClicked(section));
        }else if(form.getSection().equalsIgnoreCase("room")){
            ((IndividualViewHolder) holder).roomNo.setVisibility(View.VISIBLE);
            ((IndividualViewHolder) holder).roomNo.setText(mPausedRoomAudits.get(relativePosition).first);
            Form roomSaved= mPausedRoomAudits.get(relativePosition).second;
            ((IndividualViewHolder) holder).mPAusedTasksInfo.setVisibility(View.VISIBLE);
            ((IndividualViewHolder) holder).mPAusedTasksInfo.setText((String.format(mContext.getResources().getString(R.string.paused_audit_task),
                    AuditUtils.getPercentageTaskRemainingInAudit(roomSaved))));

            float percentage= AuditUtils.getPercentTaskRemainingInAudit(roomSaved);
           ((IndividualViewHolder)holder).progressBar.setVisibility(View.VISIBLE);
            ((IndividualViewHolder)holder).progressBar.setProgressUpdate(percentage);
            if(relativePosition==mPausedRoomAudits.size()-1){
                ((IndividualViewHolder)holder).divider.setVisibility(View.VISIBLE);
            }else{
                ((IndividualViewHolder)holder).divider.setVisibility(View.INVISIBLE);
            }
            ((IndividualViewHolder) holder).rootView.setOnClickListener((View v) ->
                    AuditAreaSectionAdapter.this.mAreaClickedListener.onPausedRoomClicked(mHotelId, mPausedRoomAudits.get(relativePosition).first, section));
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch(viewType){
            case VIEW_TYPE_HEADER:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.inflated_audit_area_header, parent, false);
                return new SectionedViewHolder(v);
            case VIEW_TYPE_ITEM:
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.inflated_audit_area_item, parent, false);
                return new IndividualViewHolder(v);
        }
    }


    public interface OnAreaClickedListener{
        void onAreaClicked(int positionClicked);
        void onPausedRoomClicked(String hotelID,String roomNo,int position);
    }

    public static class SectionedViewHolder extends RecyclerView.ViewHolder {

        public TextView mSectionTitle;
        public View rootHeaderView;
        View divider;
        public SectionedViewHolder(View itemView) {
            super(itemView);
            mSectionTitle=(TextView)itemView.findViewById(R.id.text_view_area);
            divider=itemView.findViewById(R.id.area_divider);
            rootHeaderView=itemView;
        }
    }

    public class IndividualViewHolder extends RecyclerView.ViewHolder {
        public View rootView;
        public TextView roomNo;
        public TextView mPAusedTasksInfo;
        ProgressPercentView progressBar;
        View divider;

        public IndividualViewHolder(View itemView) {
            super(itemView);
            mPAusedTasksInfo=(TextView)itemView.findViewById(R.id.area_audit_status);
            roomNo=(TextView)itemView.findViewById(R.id.area_room_no);
            progressBar=(ProgressPercentView) itemView.findViewById(R.id.hotel_percent_complete);
            divider=itemView.findViewById(R.id.area_divider);
            rootView=itemView;
        }
    }

}
