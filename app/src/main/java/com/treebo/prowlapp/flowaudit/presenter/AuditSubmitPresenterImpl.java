package com.treebo.prowlapp.flowaudit.presenter;

import com.treebo.prowlapp.flowaudit.observers.AuditSubmitObserver;
import com.treebo.prowlapp.flowaudit.observers.HotelRoomObserver;
import com.treebo.prowlapp.flowaudit.usecase.AuditUseCase;
import com.treebo.prowlapp.flowaudit.usecase.HotelRoomsUseCase;
import com.treebo.prowlapp.Models.AnswerListWithSectionName;
import com.treebo.prowlapp.mvpviews.AuditAreaView;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by sumandas on 03/05/2016.
 */
@Deprecated
public class AuditSubmitPresenterImpl implements AuditSubmitPresenter {

    public AuditAreaView mAuditAreaView;

    @Override
    public void loadRooms(int hotelId, HotelRoomsUseCase hotelRoomsUseCase, HotelRoomObserver hotelRoomObserver) {
        showLoading();
        hotelRoomsUseCase.setmHotelId(hotelId);
        hotelRoomsUseCase.execute(hotelRoomObserver);

    }


    @Override
    public void onAuditSubmit(AnswerListWithSectionName answer,
                              int hotelId, int formId,
                              int userId, String source,
                              AuditUseCase auditUseCase,
                              AuditSubmitObserver auditSubmitObserver) {
        showLoading();
        auditUseCase.setFields(answer,Integer.toString(hotelId),Integer.toString(formId), Integer.toString(userId),source);
        auditUseCase.execute(auditSubmitObserver);
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mAuditAreaView=(AuditAreaView)baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    private void showLoading() {
        mAuditAreaView.showLoading();
    }


    @Override
    public BaseView getView() {
        return mAuditAreaView;
    }

}
