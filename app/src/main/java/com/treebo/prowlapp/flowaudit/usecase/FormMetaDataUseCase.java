package com.treebo.prowlapp.flowaudit.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.FormDataResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by devesh on 28/04/16.
 */
public class FormMetaDataUseCase extends UseCase<FormDataResponse> {

    RestClient mRestClient;

    public FormMetaDataUseCase(RestClient restClient){
        mRestClient=restClient;
    }

    @Override
    protected Observable<FormDataResponse> getObservable() {
        return mRestClient.getFormMetaData();
    }
}
