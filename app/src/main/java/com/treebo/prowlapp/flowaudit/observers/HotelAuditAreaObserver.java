package com.treebo.prowlapp.flowaudit.observers;

import com.treebo.prowlapp.flowaudit.usecase.FormMetaDataUseCase;
import com.treebo.prowlapp.mvpviews.HotelAuditView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.FormDataResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;

/**
 * Created by sumandas on 18/08/2016.
 */
@Deprecated
public class HotelAuditAreaObserver extends BaseObserver<FormDataResponse> {

    FormMetaDataUseCase mGetMetaDataUsecase;
    HotelAuditView mAuditAreaView;

    public HotelAuditAreaObserver(BasePresenter presenter,
                                  FormMetaDataUseCase metMetaDataUsecase, HotelAuditView auditAreaView){
        super(presenter, metMetaDataUsecase);
        mGetMetaDataUsecase=metMetaDataUsecase;
        mAuditAreaView=auditAreaView;
    }

    @Override
    public void onCompleted() {
        mAuditAreaView.hideLoading();
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        mAuditAreaView.showError(e.getMessage());
    }

    @Override
    public void onNext(FormDataResponse response) {
        mAuditAreaView.hideLoading();
        if(response.status.equals("success")){
            mAuditAreaView.onLoadHotelAuditAreas(response);
        }else{
            mAuditAreaView.onLoadHotelAuditAreaFailed(response.msg);
        }

    }
}
