package com.treebo.prowlapp.flowupdateauditOrissue.presenter;

import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.UploadView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.UpdateIssueResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UploadUseCase;

/**
 * Created by sumandas on 01/05/2016.
 */
public class UploadPresenterImpl implements UploadPresenter {

    public UploadView mUploadView;

    public UploadUseCase uploadUseCase;

    @Override
    public void onUploadIssue(String issueId, String status, String rootCause, String description, String imageUrls) {
        uploadUseCase=new UploadUseCase(issueId,status,rootCause,description,imageUrls);
        uploadUseCase.execute(new UploadIssueObserver(this));
        showLoading();
    }


    @Override
    public void setMvpView(BaseView baseView) {
        mUploadView=(UploadView)baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if(uploadUseCase != null){
            uploadUseCase.unsubscribe();
        }
    }

    @Override
    public BaseView getView() {
        return null;
    }

    private void showLoading() {
        mUploadView.showLoading();
    }

    private void hideLoading() {
        mUploadView.hideLoading();
    }


    public class UploadIssueObserver extends BaseObserver<UpdateIssueResponse> {

        public UploadIssueObserver(BasePresenter presenter) {
            super(presenter, uploadUseCase);
        }

        @Override
        public void onCompleted() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            mUploadView.showError(e.getMessage());
            super.onError(e);
        }

        @Override
        public void onNext(UpdateIssueResponse response) {
            hideLoading();
            mUploadView.onUploadIssuesDone(response);
        }
    }

}
