package com.treebo.prowlapp.flowupdateauditOrissue;

import com.treebo.prowlapp.flowupdateauditOrissue.presenter.UploadPresenter;
import com.treebo.prowlapp.flowupdateauditOrissue.presenter.UploadPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 09/08/2016.
 */
@Module
public class UpdateAuditIssueModule {

    @Provides
    UploadPresenter providesUploadPresenter(){
        return new UploadPresenterImpl();
    }
}
