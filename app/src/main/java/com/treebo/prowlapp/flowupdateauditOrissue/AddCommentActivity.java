package com.treebo.prowlapp.flowupdateauditOrissue;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import com.google.android.material.appbar.AppBarLayout;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AuditIssueSubmitEvent;
import com.treebo.prowlapp.events.ResolveIssueEvent;
import com.treebo.prowlapp.events.UpdateIssueEvent;
import com.treebo.prowlapp.flowupdateauditOrissue.presenter.UploadPresenter;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.fragments.ImageShowFragment;
import com.treebo.prowlapp.job.IssueJob;
import com.treebo.prowlapp.job.PhotoJob;
import com.treebo.prowlapp.Models.IssuesModel;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.Models.Photo_Table;
import com.treebo.prowlapp.Models.Query;
import com.treebo.prowlapp.Models.RootCauseData;
import com.treebo.prowlapp.Models.UpdateIssueModel;
import com.treebo.prowlapp.Models.UpdateQueryModel;
import com.treebo.prowlapp.mvpviews.UploadView;
import com.treebo.prowlapp.response.UpdateIssueResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.AmazonS3UploadTask;
import com.treebo.prowlapp.Utils.CompressImageUtils;
import com.treebo.prowlapp.Utils.Logger;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.NetworkUtil;
import com.treebo.prowlapp.Utils.PermissionHelper;
import com.treebo.prowlapp.Utils.PhotoUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utilities;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.LabelDescriptionTextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.schedulers.Schedulers;
import rxAndroid.schedulers.AndroidSchedulers;

/**
 * Created by sumandas on 29/04/2016.
 */
@Deprecated
public class AddCommentActivity extends AppCompatActivity
        implements UploadView, ImageAdapter.ListInteractor, CausesBottomSheetFragment.CauseSelectedListener {

    private static final String TAG = "AddCommentActivity";

    private ImageView mCrossButton;
    private RelativeLayout mRootView;

    @Inject
    public UploadPresenter mUploadPresenter;

    @Inject
    public RxBus mRxBus;

    private String mStatus;

    private IssuesModel mIssue;

    List<Photo> mPhotoList;

    private EditText mCommentsText;

    private TextView mDone;

    private RecyclerView mImageList;
    private ImageAdapter mImageAdapter;

    private LabelDescriptionTextView mTvRootCause;

    @Inject
    public ProgressDialog progressDialog;

    @Inject
    public MixpanelAPI mMixpanelAPI;

    @Inject
    public AuditPreferenceManager mAuditPreferenceManager;

    private static final Integer PIC_PHOTO = 1;
    public boolean isBadAudit;
    public String mAuditTitle;

    String mFilePathToBeUploaded = "";
    private String mPhotokey;
    Photo mPhotoGettingUploaded;

    private ArrayList<Photo> mPhotosAdded = new ArrayList<>();

    private ArrayList<String> mCauseList;
    private CausesBottomSheetFragment mCausesBottomSheetFragment;
    private String mSelectedCause;

    private File mTempPickerDir = new File(Environment.getExternalStorageDirectory(), File.separator + Utils.PROWL_DIR + File.separator);
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.audit_comment);

        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        mContext = this;

        if (appBarLayout != null) {
            mCrossButton = (ImageView) appBarLayout.findViewById(R.id.image_view_cross);
            mDone = (TextView) appBarLayout.findViewById(R.id.comment_done_btn);
        }

        RelativeLayout photoLayout = (RelativeLayout) findViewById(R.id.layout_add_photo);
        mCommentsText = (EditText) findViewById(R.id.edit_text_signage);
        mImageList = (RecyclerView) findViewById(R.id.image_recyclerView);
        mTvRootCause = (LabelDescriptionTextView) findViewById(R.id.tv_root_cause);


        mStatus = getIntent().getStringExtra(Utils.ISSUES_STATUS);
        mIssue = getIntent().getParcelableExtra(Utils.ISSUES_OBJECT);
        isBadAudit = getIntent().getBooleanExtra(Utils.AUDIT_BAD, false);
        Query auditedQuery = getIntent().getParcelableExtra(Utils.QUERY_OBJECT);
        mAuditTitle = getIntent().getStringExtra(Utils.AUDIT_BAD_TITLE);
        mPhotokey = getIntent().getStringExtra(Utils.PHOTO_KEY);

        if (appBarLayout != null) {
            TextView toolBarTitle = (TextView) appBarLayout.findViewById(R.id.toolbar_title);
            toolBarTitle.setTextColor(getResources().getColor(R.color.white));
            if (isBadAudit) {
                toolBarTitle.setText(mAuditTitle);
            } else {
                if (mStatus.equals("Update")) {
                    toolBarTitle.setText(getString(R.string.update_info));
                } else if (mStatus.equals("Close")) {
                    toolBarTitle.setText(getString(R.string.mark_resolved));
                }
            }
        }

        mImageList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mImageList.setItemAnimator(new DefaultItemAnimator());

        mPhotoList = SQLite.select().from(Photo.class).where(Photo_Table.mKey.is(mPhotokey)).queryList();
        mImageAdapter = new ImageAdapter(this, mPhotoList, true);
        mImageAdapter.setListInteractor(this);
        mImageList.setAdapter(mImageAdapter);
        mImageAdapter.notifyDataSetChanged();

        if (mIssue != null) {
            if (mIssue.lastUpdate != null && !mIssue.lastUpdate.isEmpty()) {
                mCommentsText.setText(mIssue.lastUpdate.get(0).lastUpdate);
                mCommentsText.setSelection(mIssue.lastUpdate.get(0).lastUpdate.length());
            } else {
                String areaCategory = Utils.getAreaCategoryString(this, mIssue);
                mCommentsText.setHint(String.format(getString(R.string.bad_audit_hint), " " + areaCategory));
            }
        } else {
            if (isBadAudit) {
                if (auditedQuery.getComment().isEmpty()) {
                    mCommentsText.setHint(String.format(getString(R.string.bad_audit_hint), " " + mAuditTitle));
                } else {
                    mCommentsText.setText(auditedQuery.getComment());
                    mCommentsText.setSelection(auditedQuery.getComment().length());
                }
            }

        }

        UpdateAuditIssueComponent updateAuditIssueComponent= DaggerUpdateAuditIssueComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        updateAuditIssueComponent.injectAddCommentActivity(this);

        if (!isBadAudit) {

            //make cause selector visible
            mTvRootCause.setVisibility(View.VISIBLE);

            //make separator line visible
            findViewById(R.id.horizontal_line1).setVisibility(View.VISIBLE);

            RootCauseData rootCauses = mAuditPreferenceManager.getRootCauseList();

            boolean isCauseAdded = false;
            if (rootCauses != null) {

                mCauseList = new ArrayList<>(rootCauses.root_causes.size());

                for (int i = 0, size = rootCauses.root_causes.size(); i < size; i++) {
                    mCauseList.add(rootCauses.root_causes.get(i).name);
                    if (rootCauses.root_causes.get(i).name.equals(mIssue.rootCause)) {
                        isCauseAdded = true;
                    }
                }
            }

            if (isCauseAdded) {
                mTvRootCause.setLabelAndDescription(getString(R.string.root_cause_hint), mIssue.rootCause);
            } else {
                mTvRootCause.setLabel(getString(R.string.root_cause_hint));
            }

            mTvRootCause.setOnClickListener((View v) -> {
                mCausesBottomSheetFragment = CausesBottomSheetFragment.newInstance(mCauseList);
                mCausesBottomSheetFragment.setCauseSelectedListener(this);
                mCausesBottomSheetFragment.show(getSupportFragmentManager(), null);
            });
        }

        mCrossButton.setOnClickListener((View v) -> {
            if (isBadAudit) {
                setResult(RESULT_CANCELED);
            }
            finish();
        });

        mDone.setOnClickListener((View v) -> {
            if (!NetworkUtil.isConnected(AddCommentActivity.this)) {
                AlertDialogUtils.showInternetDisconnectedDialog(this,
                        (dialog, which) -> {
                            dialog.dismiss();
                            onRetryButtonClicked();
                        }, (dialog) -> {
                            dialog.dismiss();
                        }
                );
                return;
            }

            if (isBadAudit) {
                onBadAuditDone();
            } else {
                if (TextUtils.isEmpty(mSelectedCause)) {
                    SnackbarUtils.show(mRootView, getString(R.string.please_select_root_cause));
                } else {
                    //check if anything has changed
                    if (!checkIfIssueUpdated()) {
                        SnackbarUtils.show(mRootView, getString(R.string.issue_not_updated));
                        return;
                    }
                    if(mStatus.equals("Update")){
                        MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ISSUES_UPDATE,
                                MixPanelManager.EVENT_UPDATE_INFO_DONE);
                    }else{
                        MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ISSUES_RESOLVED,
                                MixPanelManager.EVENT_MARK_RESOLVED_DONE);
                    }

                    if (!PhotoUtils.isPhotoUploadsPending(mPhotoList)) {
                        String urls = PhotoUtils.getCSVForUrlList(PhotoUtils.getCloudUrlsList(mPhotoList));
                        mUploadPresenter.onUploadIssue(Integer.toString(mIssue.id), mStatus, mSelectedCause, mCommentsText.getText().toString(), urls);
                    } else {
                        List<Photo> pendingUploads = PhotoUtils.getPendingPhotoUploads(mPhotoList);
                        for (Photo photo : pendingUploads) {
                            photo.save();
                            submitPhotoJob(photo);
                        }
                        submitIssueJob();
                        finish();
                    }
                }
            }

        });

        mRootView = (RelativeLayout) findViewById(R.id.parent_layout);

        if (photoLayout != null) {
            photoLayout.setOnClickListener((View v) -> {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (PermissionHelper.hasNoPermissionToCamera(AddCommentActivity.this)
                            || PermissionHelper.hasNoPermissionToExternalStorage(AddCommentActivity.this)) {
                        SnackbarUtils.show(mRootView, getResources().getString(R.string.must_provide_camera));
                        ArrayList<String> permissons = new ArrayList<>();
                        if (PermissionHelper.hasNoPermissionToCamera(AddCommentActivity.this)) {
                            permissons.add(Manifest.permission.CAMERA);
                        }

                        if (PermissionHelper.hasNoPermissionToExternalStorage(AddCommentActivity.this)) {
                            permissons.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                            permissons.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        }
                        String[] items = permissons.toArray(new String[permissons.size()]);
                        this.requestPermissions(items, 1);
                        return;
                    }

                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri());
                startActivityForResult(intent, PIC_PHOTO);
            });
        }

    }


    @Inject
    public void initView() {
        mUploadPresenter.setMvpView(this);
    }

    public boolean checkIfIssueUpdated() {
        boolean isChanged = false;
        if (!mStatus.equals("Update")) {
            return true;
        }

        if (mIssue == null && !mCommentsText.getText().toString().isEmpty()) {
            isChanged = true;
        }

        if (mIssue != null && mIssue.lastUpdate != null && !mIssue.lastUpdate.isEmpty()) {
            if (!mCommentsText.getText().toString().equals(mIssue.lastUpdate.get(0).lastUpdate)) {
                isChanged = true;
            }
        }
        if (mIssue != null && mIssue.lastUpdate != null && mIssue.lastUpdate.isEmpty() && !mCommentsText.getText().toString().isEmpty()) {
            isChanged = true;
        }

        if (mIssue != null && mSelectedCause != null && !mSelectedCause.equals(mIssue.rootCause)) {
            isChanged = true;
        }
        if (!mPhotosAdded.isEmpty()) {
            isChanged = true;
        }

        return isChanged;
    }

    private void onRetryButtonClicked() {
        if (isBadAudit) {
            onBadAuditDone();
        } else {
            //check if anything has changed
            if (!checkIfIssueUpdated()) {
                SnackbarUtils.show(mRootView, getString(R.string.issue_not_updated));
                return;
            }
            if (TextUtils.isEmpty(mSelectedCause) || mSelectedCause.equals(getString(R.string.root_cause))) {
                SnackbarUtils.show(mRootView, getString(R.string.please_select_root_cause));
            } else {
                if (!PhotoUtils.isPhotoUploadsPending(mPhotoList)) {
                    String urls = PhotoUtils.getCSVForUrlList(PhotoUtils.getCloudUrlsList(mPhotoList));
                    mUploadPresenter.onUploadIssue(Integer.toString(mIssue.id), mStatus, mSelectedCause, mCommentsText.getText().toString(), urls);
                } else {
                    List<Photo> pendingUploads = PhotoUtils.getPendingPhotoUploads(mPhotoList);
                    for (Photo photo : pendingUploads) {
                        photo.save();
                        submitPhotoJob(photo);
                    }
                    submitIssueJob();
                    finish();
                }
            }

        }

    }

    public UpdateIssueModel getUpdatedIssueModel() {
        UpdateIssueModel updateModel = new UpdateIssueModel();
        updateModel.mIssueId = mIssue.id;
        updateModel.mRootCause = mSelectedCause;
        updateModel.mComment = mCommentsText.getText().toString();
        updateModel.mImageUrls = (ArrayList) PhotoUtils.getCloudUrlsList(mPhotoList);
        return updateModel;
    }

    public void onBadAuditDone() {
        Intent intent = new Intent();
        UpdateQueryModel update = new UpdateQueryModel();
        update.mComments = mCommentsText.getText().toString();
        intent.putExtra(Utils.AUDIT_BAD, update);
        setResult(RESULT_OK, intent);
        for (Photo photo : mPhotosAdded) {//change to transaction
            photo.save();
            if (!photo.isUploadedToCloud()) {
                submitPhotoJob(photo);
            }
        }
        finish();
    }


    public void addFragment(BaseFragment fragment) {
        Log.v(TAG, "addFragment called ");
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.parent_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    if (!mFilePathToBeUploaded.isEmpty()) {
                        String filePath = mTempPickerDir + File.separator + mFilePathToBeUploaded;
                        File file = new File(filePath);
/*                        RxUtils.build(Observable.just(CompressImageUtils.compressImage(file))).subscribe(file1 -> {
                            persistPicture(file1);
                            if (NetworkUtil.isConnected(AddCommentActivity.this)) {
                                uploadPicture(file1);
                            } else {
                                SnackbarUtils.show(mRootView, getString(R.string.unable_to_upload_photo_no_network));
                            }
                        });
                        */

                        Observable.just(file)
                                .flatMap(filetoUpload -> Observable.just(CompressImageUtils.compressImage(filetoUpload)))
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(new Subscriber<File>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.d("AddCommentActivity","Failed to compress file.");
                                    }

                                    @Override
                                    public void onNext(File file) {
                                        persistPicture(file);
                                        //TODO create job if net not present
                                        if (NetworkUtil.isConnected(AddCommentActivity.this)) {
                                            uploadPicture(file);
                                        } else {
                                            SnackbarUtils.show(mRootView, getString(R.string.unable_to_upload_photo_no_network));
                                        }

                                        Log.d("AddCommentActivity","File compress successfully: %s"+file.getAbsolutePath());
                                    }
                                });
                    }
                } else if (resultCode == AddCommentActivity.RESULT_CANCELED) {
                    Logger.d("IMAGE Canceled", "cancel");
                }
                break;
        }
    }

    private void persistPicture(File file) {
        mPhotoGettingUploaded = new Photo();
        mPhotoGettingUploaded.setmPhotoId(mFilePathToBeUploaded);
        mPhotoGettingUploaded.setmDeviceFilePath(file.getPath());
        mPhotoGettingUploaded.setmKey(mPhotokey);
        mPhotoList.add(0, mPhotoGettingUploaded);
        mPhotosAdded.add(0, mPhotoGettingUploaded);

        if (mImageList.getVisibility() != View.VISIBLE) {
            mImageList.setVisibility(View.VISIBLE);
        }
        mImageAdapter.notifyDataSetChanged();
    }

    private void uploadPicture(File fileToUpload) {
        String filePath = fileToUpload.getPath();
        AmazonS3UploadTask amazonS3UploadTask = new AmazonS3UploadTask(AddCommentActivity.this, filePath, uploadImageCallback);
        amazonS3UploadTask.execute();
    }

    private Uri getPhotoFileUri() {
        mFilePathToBeUploaded = mPhotokey + "_" + new SimpleDateFormat("yyyyMMddhhmmss'.jpg'").format(new Date());
        mFilePathToBeUploaded = mFilePathToBeUploaded.replaceAll(" ", "_").toLowerCase();
        mTempPickerDir.mkdirs();
        File issueFile = new File(mTempPickerDir, mFilePathToBeUploaded);
        return FileProvider.getUriForFile(
                this,
                getApplicationContext()
                        .getPackageName() + ".provider", issueFile);
    }

    AmazonS3UploadTask.CallBackData uploadImageCallback = new AmazonS3UploadTask.CallBackData() {

        @Override
        public void onSuccess(String cloudUrl) {
            mPhotoGettingUploaded.setIsUploadedToCloud(true);
            mPhotoGettingUploaded.setmCloudFilePath(cloudUrl);
            Log.d("AddCommentActivity", cloudUrl);

        }

        public void onError(Object result) {
            if (!((Activity) mContext).isFinishing())
                Utilities.cancelProgressDialog();
            SnackbarUtils.show(mRootView, getString(R.string.unable_to_attach_photo));

        }
    };

    @Override
    public void onCauseSelected(int position) {
        mSelectedCause = mCauseList.get(position);
        mTvRootCause.setDescription(mSelectedCause);
        if (mCausesBottomSheetFragment != null) {
            mCausesBottomSheetFragment.dismiss();
            mCausesBottomSheetFragment = null;
        }
    }

    @Override
    public void onUploadIssuesDone(UpdateIssueResponse response) {
        showMessageAndFinish(response);
    }

    @Override
    public void onUploadAuditDone(UpdateIssueResponse response) {
        showMessageAndFinish(response);
    }

    public void showMessageAndFinish(UpdateIssueResponse response) {
        SnackbarUtils.show(mRootView, response.data);
        Observable.timer(2, TimeUnit.SECONDS)
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onCompleted() {
                        if (!isBadAudit) {
                            if (mStatus.equals("Update")) {
                                UpdateIssueEvent event = new UpdateIssueEvent(getUpdatedIssueModel());
                                mRxBus.postEvent(event);
                            } else {
                                ResolveIssueEvent rEvent = new ResolveIssueEvent(getUpdatedIssueModel());
                                mRxBus.postEvent(rEvent);
                                mRxBus.postEvent(new AuditIssueSubmitEvent("issue"));
                            }
                        }
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Long number) {

                    }
                });
    }


    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mDone.setEnabled(false);
        progressDialog.setTitle(getString(mStatus.equals("Update") ? R.string.updating_issue : R.string.closing_issue));
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
        if(mDone!=null){
            mDone.setEnabled(true);
        }
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void onListEmpty() {
    }

    @Override
    public void imageItemClicked(int pos, int total, String imageUrl) {
        addFragment(ImageShowFragment.newInstance(pos, total, imageUrl));
    }


    public void submitPhotoJob(Photo photo) {
        PersistableBundleCompat extras = new PersistableBundleCompat();
        extras.putString(Utils.PHOTO_ID, photo.getmPhotoId());
        new JobRequest.Builder(PhotoJob.TAG + "_" + photo.getmPhotoId())
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(30_000L, JobRequest.BackoffPolicy.EXPONENTIAL)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setExtras(extras)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public void submitIssueJob() {
        PersistableBundleCompat extras = new PersistableBundleCompat();
        extras.putString("issueId", Integer.toString(mIssue.id));
        extras.putString("status", mStatus);
        extras.putString("rootcause", mSelectedCause);
        extras.putString("description", mCommentsText.getText().toString());

        new JobRequest.Builder(IssueJob.TAG + "_" + mIssue.id)
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(30_000L, JobRequest.BackoffPolicy.EXPONENTIAL)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setExtras(extras)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }
}
