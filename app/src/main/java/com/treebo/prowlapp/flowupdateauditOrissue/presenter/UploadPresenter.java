package com.treebo.prowlapp.flowupdateauditOrissue.presenter;

import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by sumandas on 01/05/2016.
 */
public interface UploadPresenter extends BasePresenter {
    void onUploadIssue(String issueId, String status, String rootCause, String description, String imageUrls);

}
