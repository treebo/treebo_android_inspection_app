package com.treebo.prowlapp.flowupdateauditOrissue;

import android.app.Dialog;
import android.os.Bundle;

import android.view.View;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.treebo.prowlapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CausesBottomSheetFragment extends BottomSheetDialogFragment {

    private CauseSelectedListener mCauseSelectedListener;

    private static final String KEY_CAUSE_LIST = "CAUSE_LIST";

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    public static CausesBottomSheetFragment newInstance(List<String> causeList) {
        CausesBottomSheetFragment fragment = new CausesBottomSheetFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(KEY_CAUSE_LIST, (ArrayList) causeList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View rootView = View.inflate(getContext(), R.layout.bottom_sheet_cause_list, null);

        dialog.setContentView(rootView);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) rootView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        setCauseList(rootView);
    }

    private void setCauseList(View rootView) {

        RecyclerView rvCauseList = (RecyclerView) rootView.findViewById(R.id.rv_cause_list);

        if (rvCauseList != null) {
            rvCauseList.setLayoutManager(new LinearLayoutManager(getContext()));

            rvCauseList.setAdapter(new CausesBottomSheetAdapter(
                    getArguments().getStringArrayList(KEY_CAUSE_LIST),
                    mCauseSelectedListener)
            );
        }
    }

    public void setCauseSelectedListener(CauseSelectedListener causeSelectedListener) {
        mCauseSelectedListener = causeSelectedListener;
    }

    public interface CauseSelectedListener {
        void onCauseSelected(int position);
    }
}
