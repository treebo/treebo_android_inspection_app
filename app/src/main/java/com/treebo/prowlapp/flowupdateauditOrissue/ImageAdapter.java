package com.treebo.prowlapp.flowupdateauditOrissue;

import android.content.Context;
import android.net.Uri;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.Utils.PhotoUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 01/05/2016.
 */
public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ListInteractor mListInteractor;

    public List<Photo> mPhotosList = new ArrayList<>();

    public ImageAdapter(Context context, List<Photo> photoList, boolean canRemove) {
        //mImageUrls=mImageUrls;
        mPhotosList = photoList;
        mContext = context;
    }

    public void setListInteractor(ListInteractor listInteractor) {
        this.mListInteractor = listInteractor;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup rootView;
        rootView = (ViewGroup) inflater.inflate(R.layout.image_view_with_cross, parent, false);
        return new IndividualViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Photo photo = mPhotosList.get(position);
        ((IndividualViewHolder) holder).crossImage.setVisibility(View.VISIBLE);
        ((IndividualViewHolder) holder).crossImage.setOnClickListener((View v) -> {
            Photo photoToBeRemoved = mPhotosList.remove(position);
            photoToBeRemoved.delete();
            notifyDataSetChanged();
            if (mPhotosList.size() == 0) {
                if (mListInteractor != null) {
                    mListInteractor.onListEmpty();
                }
            }
        });

        ((IndividualViewHolder) holder).itemView.setOnClickListener(v -> {
            if (mListInteractor != null) {
                mListInteractor.imageItemClicked(position, mPhotosList.size(), PhotoUtils.getPhotoUrl(mPhotosList.get(position)));
            }

        });
        if (!TextUtils.isEmpty(photo.getmDeviceFilePath())) {
            Glide.with(mContext).load(Uri.fromFile(new File(photo.getmDeviceFilePath())))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).crossFade().into(((IndividualViewHolder) holder).issueImage);
        } else {
            Glide.with(mContext).load(photo.getmCloudFilePath())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE).crossFade().into(((IndividualViewHolder) holder).issueImage);
        }
    }

    @Override
    public int getItemCount() {
        return mPhotosList.size();
    }

    public class IndividualViewHolder extends RecyclerView.ViewHolder {
        public ImageView issueImage;
        public ImageView crossImage;

        public IndividualViewHolder(View itemView) {
            super(itemView);
            issueImage = (ImageView) itemView.findViewById(R.id.issue_image_placeholder);
            crossImage = (ImageView) itemView.findViewById(R.id.cross_item);
        }
    }

    public interface ListInteractor {
        void onListEmpty();

        void imageItemClicked(int pos, int total, String imageUrl);
    }

}
