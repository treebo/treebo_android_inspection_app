package com.treebo.prowlapp.flowupdateauditOrissue;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;

import dagger.Component;

/**
 * Created by abhisheknair on 10/11/16.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, UpdateAuditIssueModule.class})
public interface UpdateAuditIssueComponentNew {

    void injectAddCommentActivity(AddCommentActivityNew addCommentActivity);

}