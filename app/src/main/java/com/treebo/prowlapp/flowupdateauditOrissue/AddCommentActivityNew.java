package com.treebo.prowlapp.flowupdateauditOrissue;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.Utils.AmazonS3UploadTask;
import com.treebo.prowlapp.Utils.CompressImageUtils;
import com.treebo.prowlapp.Utils.Logger;
import com.treebo.prowlapp.Utils.NetworkUtil;
import com.treebo.prowlapp.Utils.PermissionHelper;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utilities;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.inject.Inject;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.Observable;

/**
 * Created by abhisheknair on 10/11/16.
 */
public class AddCommentActivityNew extends AppCompatActivity
        implements ImageAdapter.ListInteractor {

    private static final String TAG = "AddCommentActivity";

    private TreeboEditText mCommentsText;

    @Inject
    public MixpanelAPI mMixpanelAPI;

    @Inject
    public AuditPrefManagerV3 mAuditPreferenceManager;

    private static final Integer PIC_PHOTO = 1;
    public boolean isBadAudit;

    // Image Variables
    private RecyclerView mImageList;
    private ImageAdapter mImageAdapter;
    ArrayList<Photo> mPhotoList = new ArrayList<>();
    ArrayList<String> mPhotoUrlList = new ArrayList<>();
    String mFilePathToBeUploaded = "";
    private String mPhotokey = "prowl";
    Photo mPhotoGettingUploaded;
    private ArrayList<Photo> mPhotosAdded = new ArrayList<>();
    private File mTempPickerDir = new File(Environment.getExternalStorageDirectory(), File.separator + Utils.PROWL_DIR + File.separator);

    private int mPosition;
    private String mIssueText;
    private Context mContext;
    private String mTitleText;
    private String mRoomName;
    private String mReasonText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        mContext = this;

        if (savedInstanceState == null) {
            mTitleText = getIntent().getStringExtra(Utils.AUDIT_BAD_TITLE);
            mRoomName = getIntent().getStringExtra(Utils.ROOM_NAME);
            mReasonText = getIntent().getStringExtra(Utils.FRAUD_REASON_TEXT);
            mPosition = getIntent().getIntExtra(Utils.CHECKPOINT_POSITION, -1);
            mIssueText = getIntent().getStringExtra(Utils.ISSUE_TEXT);
            String photoUrlList = getIntent().getStringExtra(Utils.PHOTO_URL_LIST);
            formArrayListFromCsv(photoUrlList);
        } else {
            mTitleText = savedInstanceState.getString(Utils.AUDIT_BAD_TITLE);
            mRoomName = savedInstanceState.getString(Utils.ROOM_NAME);
            mReasonText = savedInstanceState.getString(Utils.FRAUD_REASON_TEXT);
            mPosition = savedInstanceState.getInt(Utils.CHECKPOINT_POSITION, -1);
            mIssueText = savedInstanceState.getString(Utils.ISSUE_TEXT);
            String photoUrlList = savedInstanceState.getString(Utils.PHOTO_URL_LIST);
            formArrayListFromCsv(photoUrlList);
            mFilePathToBeUploaded = savedInstanceState.getString(Utils.PHOTO_PATH);
            mPhotoGettingUploaded = savedInstanceState.getParcelable(Utils.PHOTO_OBJECT);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_comment);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        TreeboTextView titleTv = (TreeboTextView) toolbar.findViewById(R.id.toolbar_add_comment_title);
        titleTv.setText(mTitleText);

        View crossButton = toolbar.findViewById(R.id.ic_toolbar_add_comment_back);
        crossButton.setOnClickListener((View v) -> {
            Utils.hideSoftKeyboard(findViewById(android.R.id.content));
            setResult(RESULT_CANCELED);
            finish();
        });

        View mDone = findViewById(R.id.comment_done_btn);
        mDone.setOnClickListener((View v) -> {
            Intent data = new Intent();
            data.putExtra(Utils.ISSUES_TEXT, mCommentsText.getText().toString());
            data.putExtra(Utils.CHECKPOINT_POSITION, mPosition);
            data.putExtra(Utils.PHOTO_URL_LIST, formCsvFromArrayList(mPhotoUrlList));
            data.putExtra(Utils.AUDIT_BAD_TITLE, mTitleText);
            data.putExtra(Utils.ROOM_NAME, mRoomName);
            data.putExtra(Utils.FRAUD_REASON_TEXT, mReasonText);
            Utils.hideSoftKeyboard(findViewById(android.R.id.content));
            setResult(RESULT_OK, data);
            finish();
        });

        View photoLayout = findViewById(R.id.add_photo_iv);
        mCommentsText = (TreeboEditText) findViewById(R.id.edit_text_signage);
        if (mIssueText != null || !TextUtils.isEmpty(mIssueText))
            mCommentsText.setText(mIssueText);
        mImageList = (RecyclerView) findViewById(R.id.image_recyclerView);

        mImageList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mImageList.setItemAnimator(new DefaultItemAnimator());


        mImageAdapter = new ImageAdapter(this, mPhotoList, true);
        mImageAdapter.setListInteractor(this);
        mImageList.setAdapter(mImageAdapter);
        mImageAdapter.notifyDataSetChanged();
        if (mPhotoList.size() > 0) {
            if (mImageList.getVisibility() != View.VISIBLE) {
                mImageList.setVisibility(View.VISIBLE);
            }
        }

        UpdateAuditIssueComponentNew updateAuditIssueComponent = DaggerUpdateAuditIssueComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        updateAuditIssueComponent.injectAddCommentActivity(this);

        if (photoLayout != null) {
            photoLayout.setOnClickListener((View v) -> {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (PermissionHelper.hasNoPermissionToCamera(this)
                            || PermissionHelper.hasNoPermissionToExternalStorage(this)) {
                        SnackbarUtils.show(findViewById(android.R.id.content), getResources().getString(R.string.must_provide_camera));
                        ArrayList<String> permissons = new ArrayList<>();
                        if (PermissionHelper.hasNoPermissionToCamera(this)) {
                            permissons.add(Manifest.permission.CAMERA);
                        }

                        if (PermissionHelper.hasNoPermissionToExternalStorage(this)) {
                            permissons.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                            permissons.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        }
                        String[] items = permissons.toArray(new String[permissons.size()]);
                        this.requestPermissions(items, 1);
                        return;
                    }

                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri());
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(intent, PIC_PHOTO);
            });
        }

    }

    private void formArrayListFromCsv(String csv) {
        if (!TextUtils.isEmpty(csv)) {
            String[] list = csv.split(",");
            mPhotoUrlList.addAll(Arrays.asList(list));
            for (String url : mPhotoUrlList) {
                Photo photo = new Photo();
                photo.setmCloudFilePath(url);
                mPhotoList.add(photo);
            }
        }
    }

    private String formCsvFromArrayList(ArrayList<String> list) {
        StringBuilder builder = new StringBuilder();
        for (String object : list) {
            builder.append(object);
            builder.append(",");
        }
        return builder.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    if (!mFilePathToBeUploaded.isEmpty()) {
                        String filePath = mTempPickerDir + File.separator + mFilePathToBeUploaded;
                        File file = new File(filePath);

                        RxUtils.build(Observable.just(CompressImageUtils.compressImage(file))).subscribe(file1 -> {
                            persistPicture(file1);
                            if (NetworkUtil.isConnected(AddCommentActivityNew.this)) {
                                uploadPicture(file1);
                            } else {
                                SnackbarUtils.show(findViewById(android.R.id.content), getString(R.string.unable_to_upload_photo_no_network));
                            }

                            Log.d("AddCommentActivityNew", "File compress successfully: %s" + file1.getAbsolutePath());
                        });
                    }
                    break;
                } else if (resultCode == RESULT_CANCELED) {
                    Logger.d("IMAGE Canceled", "cancel");
                }
        }
    }


    private void uploadPicture(File fileToUpload) {
        String filePath = fileToUpload.getPath();
        AmazonS3UploadTask amazonS3UploadTask = new AmazonS3UploadTask(this, filePath, uploadImageCallback);
        amazonS3UploadTask.execute();
    }

    AmazonS3UploadTask.CallBackData uploadImageCallback = new AmazonS3UploadTask.CallBackData() {

        @Override
        public void onSuccess(String cloudUrl) {
            mPhotoGettingUploaded.setIsUploadedToCloud(true);
            mPhotoGettingUploaded.setmCloudFilePath(cloudUrl);
            mPhotoList.add(0, mPhotoGettingUploaded);
            if (mImageList.getVisibility() != View.VISIBLE) {
                mImageList.setVisibility(View.VISIBLE);
            }
            mImageAdapter.notifyDataSetChanged();
            Log.d("AddCommentActivityNew", cloudUrl);
            mPhotoUrlList.add(cloudUrl);
        }

        public void onError(Object result) {
            if (!((Activity) mContext).isFinishing())
                Utilities.cancelProgressDialog();
            SnackbarUtils.show(findViewById(android.R.id.content), getString(R.string.unable_to_attach_photo));

        }
    };

    private void persistPicture(File file) {
        mPhotoGettingUploaded = new Photo();
        mPhotoGettingUploaded.setmPhotoId(mFilePathToBeUploaded);
        mPhotoGettingUploaded.setmDeviceFilePath(file.getPath());
        mPhotoGettingUploaded.setmKey(mPhotokey);
        mPhotosAdded.add(0, mPhotoGettingUploaded);
    }

    private Uri getPhotoFileUri() {
        mFilePathToBeUploaded = mPhotokey + "_" + new SimpleDateFormat("yyyyMMddhhmmss'.jpg'").format(new Date());
        mFilePathToBeUploaded = mFilePathToBeUploaded.replaceAll(" ", "_").toLowerCase();
        mTempPickerDir.mkdirs();

        File issueFile = new File(mTempPickerDir, mFilePathToBeUploaded);
        return FileProvider.getUriForFile(
                this,
                getApplicationContext()
                        .getPackageName() + ".provider", issueFile);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void onListEmpty() {
        mImageList.setVisibility(View.GONE);
    }

    @Override
    public void imageItemClicked(int pos, int total, String imageUrl) {
        mPhotoList.remove(pos);
        mImageAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Utils.AUDIT_BAD_TITLE, mTitleText);
        outState.putString(Utils.ROOM_NAME, mRoomName);
        outState.putString(Utils.FRAUD_REASON_TEXT, mReasonText);
        outState.putInt(Utils.CHECKPOINT_POSITION, -1);
        outState.putString(Utils.ISSUE_TEXT, mIssueText);
        outState.putString(Utils.PHOTO_URL_LIST, formCsvFromArrayList(mPhotoUrlList));
        outState.putString(Utils.PHOTO_PATH, mFilePathToBeUploaded);
        outState.putParcelable(Utils.PHOTO_OBJECT, mPhotoGettingUploaded);
        super.onSaveInstanceState(outState);
    }
}
