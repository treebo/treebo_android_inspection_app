package com.treebo.prowlapp.flowupdateauditOrissue;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;

import dagger.Component;

/**
 * Created by sumandas on 09/08/2016.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class,UpdateAuditIssueModule.class})
public interface UpdateAuditIssueComponent {

    void injectAddCommentActivity(AddCommentActivity addCommentActivity);

}
