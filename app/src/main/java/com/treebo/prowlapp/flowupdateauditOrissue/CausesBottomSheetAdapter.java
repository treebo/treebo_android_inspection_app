package com.treebo.prowlapp.flowupdateauditOrissue;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;

import java.lang.ref.WeakReference;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by aa on 11/07/16.
 */
public class CausesBottomSheetAdapter extends RecyclerView.Adapter<CausesBottomSheetAdapter.CauseViewHolder> {

    private List<String> mCauseList;
    private CausesBottomSheetFragment.CauseSelectedListener mCauseSelectedListener;

    public CausesBottomSheetAdapter(List<String> causeList,
                                    CausesBottomSheetFragment.CauseSelectedListener causeSelectedListener) {
        mCauseList = causeList;
        mCauseSelectedListener = causeSelectedListener;
    }

    @Override
    public CauseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CauseViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cause_list, parent, false), mCauseSelectedListener);
    }

    @Override
    public void onBindViewHolder(CauseViewHolder causeViewHolder, int position) {
        causeViewHolder.tvCause.setText(mCauseList.get(position));
    }

    @Override
    public int getItemCount() {
        if (mCauseList == null) {
            return 0;
        } else {
            return mCauseList.size();
        }
    }

    public static class CauseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public WeakReference<CausesBottomSheetFragment.CauseSelectedListener> mCauseSelectedListenerWeakReference;
        public TextView tvCause;

        public CauseViewHolder(View itemView,
                               CausesBottomSheetFragment.CauseSelectedListener causeSelectedListener) {
            super(itemView);
            mCauseSelectedListenerWeakReference = new WeakReference<>(causeSelectedListener);
            tvCause = (TextView) itemView.findViewById(R.id.tv_cause);

            tvCause.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_cause:
                    CausesBottomSheetFragment.CauseSelectedListener causeSelectedListener =
                            mCauseSelectedListenerWeakReference.get();
                    if (causeSelectedListener != null) {
                        causeSelectedListener.onCauseSelected(getAdapterPosition());
                    }
                    break;

                default:
                    break;
            }
        }
    }
}
