package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.presenter.TaskListPresenter;
import com.treebo.prowlapp.response.taskcreation.TaskListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class GetCreatedTaskListObserver extends BaseObserver<TaskListResponse> {

    private TaskListPresenter mPresenter;

    public GetCreatedTaskListObserver(TaskListPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(TaskListResponse response) {
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onTaskListFetchSuccess(response);
        } else {
            mPresenter.onTaskListFetchFailure(response.msg);
        }
    }
}
