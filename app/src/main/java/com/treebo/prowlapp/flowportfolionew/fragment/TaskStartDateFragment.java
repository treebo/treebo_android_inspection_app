package com.treebo.prowlapp.flowportfolionew.fragment;


import android.content.Context;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.views.TreeboTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

/**
 * Created by abhisheknair on 21/06/17.
 */

public class TaskStartDateFragment extends Fragment implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener {

    public final static String TAG = "TaskStartDateFragment";

    private boolean isStartDateSet;
    private boolean isStartTimeSet;

    private TreeboTextView mStartDateTv;
    private TreeboTextView mStartTimeTv;
    private Context mContext;

    private String mAppearDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_task_date, container, false);
        mContext = inflater.getContext();
        return layoutView;
    }

    @Override
    public void onViewCreated(View layoutView, Bundle savedInstanceState) {
        super.onViewCreated(layoutView, savedInstanceState);
        mStartDateTv = (TreeboTextView) layoutView.findViewById(R.id.task_start_date_tv);
        mStartDateTv.setOnClickListener(view -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH),
                    getString(R.string.cancel)
            );
            dpd.setMinDate(now);
            dpd.setAccentColor(ContextCompat.getColor(mContext, R.color.emerald));
            mAppearDate = "";
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        });
        mStartTimeTv = (TreeboTextView) layoutView.findViewById(R.id.task_start_time_tv);
        mStartTimeTv.setOnClickListener(view -> {
            Calendar now = Calendar.getInstance();
            TimePickerDialog dpd = TimePickerDialog.newInstance(
                    this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    false
            );
            dpd.setAccentColor(ContextCompat.getColor(mContext, R.color.emerald));
            dpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
        });
    }

    public boolean isDateEntered() {
        return isStartDateSet && isStartTimeSet;
    }

    public String getTaskAppearDate() {
        return mAppearDate;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        isStartDateSet = true;
        monthOfYear += 1;
        mStartDateTv.setText(DateUtils.getDisplayDateFromDatePicker(year, monthOfYear, dayOfMonth));
        mStartDateTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        mAppearDate = year + "-" + monthOfYear + "-" + dayOfMonth;
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        isStartTimeSet = true;
        mStartTimeTv.setText(DateUtils.getDisplayTimeFromTimePicker(hourOfDay, minute));
        mStartTimeTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        if (mAppearDate != null && mAppearDate.contains(":00")) {
            mAppearDate = mAppearDate.substring(0, mAppearDate.indexOf(" "));
        }
        mAppearDate += " " + hourOfDay + ":" + minute + ":00";
    }
}
