package com.treebo.prowlapp.flowportfolionew.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.Models.taskcreationmodel.SectionedTaskModel;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static com.treebo.prowlapp.Utils.Constants.BOTTOM_DUMMY_TASK;
import static com.treebo.prowlapp.Utils.Constants.CREATED_TASK;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class SectionedTaskListAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private ArrayList<SectionedTaskModel> mList;
    private onTaskClickListener mListener;
    private Context mContext;

    public SectionedTaskListAdapter(Context context, ArrayList<SectionedTaskModel> list,
                                    onTaskClickListener listener) {
        this.mContext = context;
        this.mList = list;
        this.mListener = listener;
    }

    public void refreshList(ArrayList<SectionedTaskModel> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getSectionCount() {
        if (mList == null)
            return 0;
        else
            return mList.size();
    }

    @Override
    public int getItemCount(int i) {
        if (mList == null)
            return 0;
        else
            return mList.get(i).getTaskList().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        SectionedTaskModel section = mList.get(i);
        ((TaskSectionViewHolder) viewHolder).sectionTitle.setText(section.getSectionName());
        ((TaskSectionViewHolder) viewHolder).sectionCount.setText(String.valueOf(section.getTaskListSize()));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, int i1, int i2) {
        CreatedTaskModel task = mList.get(i).getTaskList().get(i1);
        task.setSectionHeader(mList.get(i).getSectionName());
        if (task.getTaskType() != BOTTOM_DUMMY_TASK) {
            ((TaskViewHolder) viewHolder).task = task;
            ((TaskViewHolder) viewHolder).taskName.setText(task.getTaskName());
            ((TaskViewHolder) viewHolder).taskDetails.setText(task.getTaskDetails());
            ((TaskViewHolder) viewHolder).startDateTv.setText(
                    DateUtils.getDisplayTimeFromServerTime(task.getAppearDate()));
            ((TaskViewHolder) viewHolder).endDateTv.setText(
                    DateUtils.getDisplayTimeFromServerTime(task.getExpiryDate()));
            ((TaskViewHolder) viewHolder).disabledLayout.setVisibility(
                    mList.get(i).getSectionName().equals(mContext.getString(R.string.completed_tasks))
                            ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        return mList.get(section).getTaskList().get(relativePosition).getTaskType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == CREATED_TASK) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_management,
                    parent, false);
            return new TaskViewHolder(view, mListener);
        } else if (viewType == BOTTOM_DUMMY_TASK) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom_dummy_task,
                    parent, false);
            return new BottomDummyViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_issue_section_header,
                    parent, false);
            return new TaskSectionViewHolder(view);
        }
    }

    private class BottomDummyViewHolder extends RecyclerView.ViewHolder {

        BottomDummyViewHolder(View itemView) {
            super(itemView);
        }
    }

    private class TaskSectionViewHolder extends RecyclerView.ViewHolder {

        TreeboTextView sectionTitle;
        TreeboTextView sectionCount;

        TaskSectionViewHolder(View itemView) {
            super(itemView);
            sectionTitle = (TreeboTextView) itemView.findViewById(R.id.issue_section_header_title);
            sectionCount = (TreeboTextView) itemView.findViewById(R.id.issue_section_item_count);
        }
    }

    public class TaskViewHolder extends RecyclerView.ViewHolder {

        private CreatedTaskModel task;
        private TreeboTextView taskName;
        private TreeboTextView taskDetails;
        private ImageView tooltipIv;
        private TreeboTextView startDateTv;
        private TreeboTextView endDateTv;
        private View disabledLayout;

        public TaskViewHolder(View itemView, onTaskClickListener listener) {
            super(itemView);
            taskName = (TreeboTextView) itemView.findViewById(R.id.task_card_header);
            taskDetails = (TreeboTextView) itemView.findViewById(R.id.task_info_tv);
            tooltipIv = (ImageView) itemView.findViewById(R.id.task_tooltip_iv);
            startDateTv = (TreeboTextView) itemView.findViewById(R.id.task_start_text_tv);
            endDateTv = (TreeboTextView) itemView.findViewById(R.id.task_end_text_tv);
            disabledLayout = itemView.findViewById(R.id.task_card_disabled);
            disabledLayout.setOnClickListener(view -> listener.onTaskClicked(task));
            itemView.setOnClickListener(view -> listener.onTaskClicked(task));
            tooltipIv.setOnClickListener(view -> listener.onTooltipClicked(task));
        }
    }

    public interface onTaskClickListener {
        void onTaskClicked(CreatedTaskModel task);

        void onTooltipClicked(CreatedTaskModel task);
    }
}
