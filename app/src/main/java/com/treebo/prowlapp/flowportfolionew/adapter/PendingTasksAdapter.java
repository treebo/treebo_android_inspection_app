package com.treebo.prowlapp.flowportfolionew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.response.portfolio.PendingTasksListResponse;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 29/04/17.
 */

public class PendingTasksAdapter extends RecyclerView.Adapter<PendingTasksAdapter.PendingTasksViewHolder> {

    private ArrayList<PendingTasksListResponse.PendingTaskObject> mList;
    private Context mContext;
    private taskClickListener mListener;

    public PendingTasksAdapter(ArrayList<PendingTasksListResponse.PendingTaskObject> list,
                               Context context, taskClickListener listener) {
        this.mList = list;
        this.mContext = context;
        this.mListener = listener;
    }

    public void refreshList(ArrayList<PendingTasksListResponse.PendingTaskObject> list) {
        mList = new ArrayList<>();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public PendingTasksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pending_tasks_info,
                parent, false);
        return new PendingTasksViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(PendingTasksViewHolder holder, int position) {
        PendingTasksListResponse.PendingTaskObject object = mList.get(position);
        holder.object = object;
        holder.task.setText(object.getTaskName());
        holder.hotelName.setText(object.getHotelName());
        holder.completionDate.setText(DateUtils.getDateAndMonthInFullWithTimeFromTimeStamp(object.getCompletionDate()));
        if (!object.isCompleted()) {
            holder.statusText.setText(mContext.getString(R.string.incomplete));
            holder.statusText.setTextColor(ContextCompat.getColor(mContext, R.color.warning_red));
        } else {
            holder.statusText.setText(mContext.getString(R.string.completed));
            holder.statusText.setTextColor(ContextCompat.getColor(mContext, R.color.green_color_primary));
        }

    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public class PendingTasksViewHolder extends RecyclerView.ViewHolder {

        private PendingTasksListResponse.PendingTaskObject object;
        private TreeboTextView task;
        private TreeboTextView hotelName;
        private TreeboTextView completionDate;
        private TreeboTextView statusText;

        public PendingTasksViewHolder(View itemView, taskClickListener listener) {
            super(itemView);
            task = (TreeboTextView) itemView.findViewById(R.id.task_text_tv);
            hotelName = (TreeboTextView) itemView.findViewById(R.id.hotel_name_text_tv);
            completionDate = (TreeboTextView) itemView.findViewById(R.id.completion_date_text_tv);
            statusText = (TreeboTextView) itemView.findViewById(R.id.status_text_tv);
            itemView.setOnClickListener(view -> listener.onTaskCardClicked(object));
        }

    }

    public interface taskClickListener {
        void onTaskCardClicked(PendingTasksListResponse.PendingTaskObject object);
    }
}
