package com.treebo.prowlapp.flowportfolionew.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AppUpdateEvent;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.events.PermissionsUpdateEvent;
import com.treebo.prowlapp.flowdashboard.activity.DashboardActivity;
import com.treebo.prowlapp.flowportfolionew.DaggerPortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.PortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.adapter.SectionedHotelListAdapter;
import com.treebo.prowlapp.flowportfolionew.presenter.PortfolioPresenterNew;
import com.treebo.prowlapp.flowportfolionew.usecase.AllHotelListUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.PortfolioMetricUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.SingleHotelDetailsUseCase;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioHotelV3;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioMetrics;
import com.treebo.prowlapp.Models.portfoliomodel.SectionedHotelListModel;
import com.treebo.prowlapp.flowupdateauditOrissue.AddCommentActivityNew;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.response.portfolio.PortfolioHotelListResponseV3;
import com.treebo.prowlapp.response.portfolio.PortfolioMetricsResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class PortfolioActivityNew extends AppCompatActivity implements IPortfolioContract.IPortfolioView,
        SectionedHotelListAdapter.onHotelClickListener, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    public PortfolioPresenterNew mPresenter;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public RestClient mRestClient;

    @Inject
    public AllHotelListUseCase mHotelListUseCase;

    @Inject
    public PortfolioMetricUseCase mMetricUseCase;

    @Inject
    public SingleHotelDetailsUseCase mSingleHotelUseCase;

    @Inject
    public Navigator mNavigator;

    @Inject
    RxBus mRxBus;

    CompositeSubscription mSubscription;

    private TreeboTextView mTaskCompletionPercent;
    private TreeboTextView mCompliancePercent;
    private TreeboTextView mIssuesNotFound;
    private TreeboTextView mDelightPercent;
    private TreeboTextView mDisasterPercent;

    private TreeboTextView mTaskCompletionChange;
    private TreeboTextView mIssuesNotFoundChange;
    private TreeboTextView mComplianceChange;
    private TreeboTextView mDelightChange;
    private TreeboTextView mDisasterChange;

    private ImageView mTaskCompletionArrowIv;
    private ImageView mComplianceArrowIv;
    private ImageView mIssuesNotFoundArrowIv;
    private ImageView mDelightArrowIv;
    private ImageView mDisasterArrowIv;

//    private View mTaskCompletionDivider;
//    private View mIssuesNotFoundDivider;
//    private View mComplianceDivider;
//    private View mDelightDivider;

    private View mTaskCompletionText;
    private View mComplianceText;
    private View mIssuesNotFoundText;
    private View mDelightText;
    private View mDisasterText;

    private TreeboTextView mUserName;

    private RecyclerView mHotelRecyclerView;
    private View mLoadingView;

    private SectionedHotelListAdapter mSectionedListAdapter;

    private ArrayList<SectionedHotelListModel> mSectionedList = new ArrayList<>();
    private ArrayList<PortfolioHotelV3> mHotelList = new ArrayList<>();

    private boolean isInHotelLocation;
    private ArrayList<PortfolioHotelV3> mCurrentHotelList = new ArrayList<>();
    private View mSettingsIcon;
    private FloatingActionButton btnAuditAnyHotel;
    private View mMastheadView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isActivityPaused;
    private PortfolioMetrics mReceivedMetrics;
    private TreeboTextView mPortfolioHeaderTv;

    public static Intent getCallingIntent(Activity activity) {
        return new Intent(activity, PortfolioActivityNew.class);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portfolio_new);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);
        PortfolioComponentNew portfolioComponent = DaggerPortfolioComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        portfolioComponent.injectPortfolioActivity(this);

        if (mLoginManager.getServerAppVersion() > BuildConfig.VERSION_CODE) {
            mNavigator.navigateToBlockerScreen(this);
        }

        mSubscription = new CompositeSubscription();

        if (savedInstanceState == null) {
            ArrayList<HotelLocation> hotelList = getIntent().getParcelableArrayListExtra(Utils.HOTEL_LIST);
            if (hotelList != null) {
                for (HotelLocation hotel : hotelList) {
                    PortfolioHotelV3 hotelObject = new PortfolioHotelV3();
                    hotelObject.setHotelID(hotel.mHotelId);
                    hotelObject.setHotelName(hotel.mHotelName);
                    mCurrentHotelList.add(hotelObject);
                }
                //mCurrentHotelList.addAll(hotelList);
                isInHotelLocation = true;
            }
            PortfolioHotelV3 intentHotel = getIntent().getParcelableExtra(Utils.HOTEL_OBJECT);
            if (intentHotel != null && !isHotelInList(intentHotel.getHotelID())) {
                mCurrentHotelList.add(intentHotel);
                isInHotelLocation = getIntent().getBooleanExtra(Utils.IS_IN_HOTEL, false);
            }

        } else {
            mCurrentHotelList = savedInstanceState.getParcelableArrayList(Utils.HOTEL_LIST);
            isInHotelLocation = savedInstanceState.getBoolean(Utils.IS_IN_HOTEL, false);
            mReceivedMetrics = savedInstanceState.getParcelable(Utils.PORTFOLIO_METRICS);
        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.portfolio_swipe_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mHotelRecyclerView = (RecyclerView) findViewById(R.id.portfolio_hotel_list_rv);
        mLoadingView = findViewById(R.id.loader_layout);
        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));

        mMastheadView = findViewById(R.id.scores_layout);

        mUserName = (TreeboTextView) mMastheadView.findViewById(R.id.portfolio_user_name_tv);
        mUserName.setText(getString(R.string.user_name_title, mLoginManager.getUsername()));
        mPortfolioHeaderTv = (TreeboTextView) mMastheadView.findViewById(R.id.portfolio_header_tv);

        ImageView profilePic = (ImageView) mMastheadView.findViewById(R.id.ic_personal_image);
        Utils.loadRoundedImageUsingGlide(this, mLoginManager.getPrefUserProfilePic(),
                profilePic, R.drawable.ic_person_black);

        mSettingsIcon = mMastheadView.findViewById(R.id.ic_settings);
        mSettingsIcon.setOnClickListener(view -> mNavigator.navigateToSettingsActivity(this));
        mTaskCompletionPercent = (TreeboTextView) mMastheadView.findViewById(R.id.masthead_task_completion_score_tv);
        mIssuesNotFound = (TreeboTextView) mMastheadView.findViewById(R.id.masthead_issue_not_caught_score_tv);
        mCompliancePercent = (TreeboTextView) mMastheadView.findViewById(R.id.masthead_compliance_score_tv);
        mDelightPercent = (TreeboTextView) mMastheadView.findViewById(R.id.masthead_delight_score_tv);
        mDisasterPercent = (TreeboTextView) mMastheadView.findViewById(R.id.masthead_disaster_score_tv);

        mTaskCompletionChange = (TreeboTextView) mMastheadView.findViewById(R.id.task_completion_change_tv);
        mIssuesNotFoundChange = (TreeboTextView) mMastheadView.findViewById(R.id.issue_not_caught_change_tv);
        mComplianceChange = (TreeboTextView) mMastheadView.findViewById(R.id.compliance_change_tv);
        mDelightChange = (TreeboTextView) mMastheadView.findViewById(R.id.delight_change_tv);
        mDisasterChange = (TreeboTextView) mMastheadView.findViewById(R.id.disaster_change_tv);

        mTaskCompletionArrowIv = (ImageView) mMastheadView.findViewById(R.id.task_completion_arrow_iv);
        mIssuesNotFoundArrowIv = (ImageView) mMastheadView.findViewById(R.id.issue_not_caught_arrow_iv);
        mComplianceArrowIv = (ImageView) mMastheadView.findViewById(R.id.compliance_arrow_iv);
        mDelightArrowIv = (ImageView) mMastheadView.findViewById(R.id.delight_arrow_iv);
        mDisasterArrowIv = (ImageView) mMastheadView.findViewById(R.id.disaster_arrow_iv);

//        mTaskCompletionDivider = mMastheadView.findViewById(R.id.masthead_task_completion_divider);
//        mIssuesNotFoundDivider = mMastheadView.findViewById(R.id.masthead_issue_not_caught_divider);
//        mComplianceDivider = mMastheadView.findViewById(R.id.masthead_compliance_divider);
//        mDelightDivider = mMastheadView.findViewById(R.id.masthead_delight_divider);

        mTaskCompletionText = mMastheadView.findViewById(R.id.masthead_task_completion_text_tv);
        mIssuesNotFoundText = mMastheadView.findViewById(R.id.issue_not_caught_text_tv);
        mComplianceText = mMastheadView.findViewById(R.id.compliance_text_tv);
        mDelightText = mMastheadView.findViewById(R.id.delight_text_tv);
        mDisasterText = mMastheadView.findViewById(R.id.disaster_text_tv);

        mPresenter.onCreate();
        mPresenter.updateViewWithPermissions();

        View incView = mMastheadView.findViewById(R.id.inc_view);
        incView.setOnClickListener(view -> onIncClick());
        mIssuesNotFoundText.setOnClickListener(view -> onIncClick());
        mIssuesNotFoundArrowIv.setOnClickListener(view -> onIncClick());
        mIssuesNotFound.setOnClickListener(view -> onIncClick());
        mIssuesNotFoundChange.setOnClickListener(view -> onIncClick());

        btnAuditAnyHotel = findViewById(R.id.btnaudioanyactivity);
        btnAuditAnyHotel.setOnClickListener(view -> mNavigator.navigateToAuditAnyHotelActivity(this));

        mPresenter.loadHotelList(mLoginManager.getUserId());
        mPresenter.loadMetrics(mLoginManager.getUserId());

        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                ArrayList<HotelLocation> hotelLocation = ((HotelChangedEvent) event).mHotelList;

                if (hotelLocation == null || hotelLocation.size() == 0) {
                    isInHotelLocation = false;
                } else {
                    isInHotelLocation = true;
                    mCurrentHotelList.clear();
                    for (HotelLocation hotel : hotelLocation) {
                        PortfolioHotelV3 hotelObject = new PortfolioHotelV3();
                        hotelObject.setHotelID(hotel.mHotelId);
                        hotelObject.setHotelName(hotel.mHotelName);
                        mCurrentHotelList.add(hotelObject);
                    }
                }
                mPresenter.updateHotelList(isInHotelLocation);
            } else if (event instanceof AppUpdateEvent) {
                if (!isActivityPaused)
                    mNavigator.navigateToBlockerScreen(this);
            } else if (event instanceof PermissionsUpdateEvent) {
                mPresenter.updateViewWithPermissions();
            }
        }));

    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mPresenter.loadMetrics(mLoginManager.getUserId());
        /*PortfolioHotelV3 hotel = intent.getParcelableExtra(Utils.HOTEL_OBJECT);
        mRxBus.postEvent(new HotelChangedEvent(currentLocationHotelsList));
        if (hotel != null && !isHotelInList(hotel.getHotelID()))
            mCurrentHotelList.add(hotel);*/
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setHotelListUseCase(mHotelListUseCase);
        mPresenter.setMetricUseCase(mMetricUseCase);
        mPresenter.setSingleHotelUseCase(mSingleHotelUseCase);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }

    @Override
    public void setUpRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mHotelRecyclerView.setLayoutManager(layoutManager);

        mSectionedListAdapter = new SectionedHotelListAdapter(this, mSectionedList, this);
        mHotelRecyclerView.setAdapter(mSectionedListAdapter);
    }

    @Override
    public void setUpScoresData(PortfolioMetrics metrics) {
        mReceivedMetrics = metrics;
        Utils.animateTextView(this, 0, metrics.getIssueNotCaughtCount().getScore(), mIssuesNotFound, false,
                R.dimen.textsize_xlarge);
        Utils.animateTextView(this, 0, metrics.getCompliance().getScore(), mCompliancePercent, true,
                R.dimen.textsize_xlarge);
        Utils.animateTextView(this, 0, metrics.getTaskCompletionRatio().getScore(), mTaskCompletionPercent, true,
                R.dimen.textsize_xlarge);
        Utils.animateTextView(this, 0, metrics.getDelightPercent().getScore(), mDelightPercent, true,
                R.dimen.textsize_xlarge);
        Utils.animateTextView(this, 0, metrics.getDisasterPercent().getScore(), mDisasterPercent, true,
                R.dimen.textsize_xlarge);

        mIssuesNotFound.setTextColor(ContextCompat.getColor(this,
                Utils.getMetricDisplayColor(metrics.getIssueNotCaughtCount().getColor())));
        mCompliancePercent.setTextColor(ContextCompat.getColor(this,
                Utils.getMetricDisplayColor(metrics.getCompliance().getColor())));
        mTaskCompletionPercent.setTextColor(ContextCompat.getColor(this,
                Utils.getMetricDisplayColor(metrics.getTaskCompletionRatio().getColor())));
        mDelightPercent.setTextColor(ContextCompat.getColor(this,
                Utils.getMetricDisplayColor(metrics.getDelightPercent().getColor())));
        mDisasterPercent.setTextColor(ContextCompat.getColor(this,
                Utils.getMetricDisplayColor(metrics.getDisasterPercent().getColor())));

        setUpDifferenceMetrics(mIssuesNotFoundChange, mIssuesNotFoundArrowIv, metrics.getIssueNotCaughtCount().getDifference());
        setUpDifferenceMetrics(mComplianceChange, mComplianceArrowIv, metrics.getCompliance().getDifference());
        setUpDifferenceMetrics(mTaskCompletionChange, mTaskCompletionArrowIv, metrics.getTaskCompletionRatio().getDifference());
        setUpDifferenceMetrics(mDelightChange, mDelightArrowIv, metrics.getDelightPercent().getDifference());
        setUpDifferenceMetrics(mDisasterChange, mDisasterArrowIv, metrics.getDisasterPercent().getDifference());
    }

    // TODO: Remove this for new design
    private void setUpDifferenceMetrics(TreeboTextView textView, ImageView imageView, int diff) {
        imageView.setVisibility((diff == 0) ? View.GONE : View.VISIBLE);
        if (diff < 0) {
            imageView.setImageResource(R.drawable.ic_arrow_downward);
            diff *= -1;
        } else if (diff > 0) {
            imageView.setImageResource(R.drawable.ic_arrow_upward);
        }
        textView.setText(String.valueOf(diff));
    }

    public void onIncClick() {
        if (mReceivedMetrics != null && mReceivedMetrics.getIssueNotCaughtCount().getScore() != 0)
            mNavigator.navigateToIssueListActivity(this, 0, "", Utils.SEVERITY_HIGH_LOW, 0, true);
    }

    @Override
    public void onLoadHotelListSuccess(PortfolioHotelListResponseV3 response) {
        mHotelList.clear();
        mHotelList.addAll(response.hotelList);
        mPresenter.updateHotelList(isInHotelLocation);
    }

    @Override
    public void arrangeHotelsInSections(boolean isHotelInCurrentLocation) {

        mLoadingView.setVisibility(View.GONE);
        mSectionedList.clear();
        if (isHotelInCurrentLocation) {
            ArrayList<PortfolioHotelV3> hotels = new ArrayList<>();
            ArrayList<PortfolioHotelV3> currentHotels = new ArrayList<>();
            for (PortfolioHotelV3 hotelV3 : mHotelList) {
                if (!isHotelInList(hotelV3.getHotelID()))
                    hotels.add(hotelV3);
                else
                    currentHotels.add(hotelV3);
            }
            if (mCurrentHotelList.size() > 0) {
                SectionedHotelListModel section = new SectionedHotelListModel();
                section.setSectionName(getString(R.string.current_location));
                for (PortfolioHotelV3 hotel : mCurrentHotelList) {
                    for (PortfolioHotelV3 object : currentHotels) {
                        if (hotel.getHotelID() == object.getHotelID()) {
                            hotel.setCompliancePercent(object.getCompliancePercent());
                            hotel.setOpenIssueCount(object.getOpenIssueCount());
                            hotel.setPendingTaskCount(object.getPendingTaskCount());
                            currentHotels.remove(object);
                            break;
                        }
                    }
                    hotel.setInCurrentLocation(true);
                    if (hotel.isInfoCallRequired()) {
                        mPresenter.loadSingleHotelDetails(mLoginManager.getUserId(), hotel.getHotelID());
                    }
                    section.setHotelInHotelList(hotel);
                }
                mSectionedList.add(section);
            }
            SectionedHotelListModel section2 = new SectionedHotelListModel();
            section2.setSectionName(getString(R.string.my_properties));
            section2.setHotelList(hotels);
            mSectionedList.add(section2);
        } else {
            SectionedHotelListModel section = new SectionedHotelListModel();
            section.setSectionName(getString(R.string.my_properties));
            section.setHotelList(mHotelList);
            mSectionedList.add(section);
        }
        mSectionedListAdapter.notifyDataSetChanged();


    }

    @Override
    public void setMetricsForSingleHotel(PortfolioHotelV3 hotel) {
        for (int i = 0; i < mCurrentHotelList.size(); i++) {
            PortfolioHotelV3 listHotel = mCurrentHotelList.get(i);
            if (hotel.getHotelID() == listHotel.getHotelID()) {
                mCurrentHotelList.set(i, hotel);
            }
        }
        mPresenter.updateHotelList(true);
    }

    private boolean isHotelInList(int id) {
        try {
            for (PortfolioHotelV3 hotel : mCurrentHotelList) {
                if (hotel.getHotelID() == id)
                    return true;
            }
        } catch (ClassCastException e) {
            Log.d("Portfolio Screen", "ClassCastException in isHotelInList");
        }
        return false;
    }

    @Override
    public void onLoadHotelListFailure(String msg) {
        Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);
    }

    @Override
    public void onLoadHotelListComplete() {
        if (mCurrentHotelList.size() == 0 && mHotelList.size() == 0 && !LoginSharedPrefManager.getInstance().isHotelLocationsFetched()) {
            Snackbar.make(findViewById(R.id.scores_layout), "Loading hotels. Please wait for a moment.", Snackbar.LENGTH_LONG).show();
            mLoadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoadMetricSuccess(PortfolioMetricsResponse response) {
        mPresenter.setScores(response.metrics);
    }

    @Override
    public void onLoadMetricFailure(String msg) {
        Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG);
    }

    @Override
    public void hotelClicked(PortfolioHotelV3 hotel) {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.PROPERTY_SELECT,
                new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, hotel.getHotelName())
                        .putValue(SegmentAnalyticsManager.IS_IN_GEOFENCE, isInHotelLocation));
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra(Utils.HOTEL_NAME, hotel.getHotelName());
        intent.putExtra(Utils.HOTEL_ID, hotel.getHotelID());
        boolean isHotelInList = isHotelInList(hotel.getHotelID());
        intent.putExtra(Utils.IS_IN_HOTEL, isHotelInList);
        if (isHotelInList)
            mLoginManager.setCurrentHotelLocationId(hotel.getHotelID());
        intent.putExtra(Utils.IS_FROM_EXPLORE, !isHotelInList);
        startActivity(intent);
    }


    @Override
    public void setUpViewAsPerPermissions() {
        RolesAndUserPermissions.PermissionList permissions = mLoginManager.getUserPermissionList(0);
        if (permissions != null) {
            mMastheadView.setVisibility(permissions.isPortfolioMastheadEnabled() ? View.VISIBLE : View.GONE);
            mUserName.setVisibility(permissions.isPortfolioMastheadHeaderEnabled() ? View.VISIBLE : View.GONE);
            mPortfolioHeaderTv.setVisibility(permissions.isPortfolioMastheadHeaderEnabled() ? View.VISIBLE : View.GONE);
            mSettingsIcon.setVisibility(permissions.isSettingsEnabled() ? View.VISIBLE : View.GONE);
            // Task Completion
            if (!permissions.isPortfolioTaskCompletionMetricEnabled()) {
                mTaskCompletionArrowIv.setVisibility(View.GONE);
                mTaskCompletionChange.setVisibility(View.GONE);
                mTaskCompletionPercent.setVisibility(View.GONE);
                mTaskCompletionText.setVisibility(View.GONE);
            }

            // Issue not caught
            if (!permissions.isPortfolioIncMetricEnabled()) {
                mIssuesNotFoundChange.setVisibility(View.GONE);
                mIssuesNotFoundArrowIv.setVisibility(View.GONE);
                mIssuesNotFound.setVisibility(View.GONE);
                mIssuesNotFoundText.setVisibility(View.GONE);
            }

            // Compliance
            if (!permissions.isPortfolioComplianceMetricEnabled()) {
                mComplianceArrowIv.setVisibility(View.GONE);
                mComplianceChange.setVisibility(View.GONE);
                mCompliancePercent.setVisibility(View.GONE);
                mComplianceText.setVisibility(View.GONE);
            }

            // Delight
            if (!permissions.isPortfolioDelightMetricEnabled()) {
                mDelightArrowIv.setVisibility(View.GONE);
                mDelightChange.setVisibility(View.GONE);
                mDelightPercent.setVisibility(View.GONE);
                mDelightText.setVisibility(View.GONE);
            }

            // Disaster
            if (!permissions.isPortfolioDisasterMetricEnabled()) {
                mDisasterArrowIv.setVisibility(View.GONE);
                mDisasterChange.setVisibility(View.GONE);
                mDisasterPercent.setVisibility(View.GONE);
                mDisasterText.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
        if (mLoginManager.getServerAppVersion() > BuildConfig.VERSION_CODE) {
            mNavigator.navigateToBlockerScreen(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
        isActivityPaused = true;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        if (Utils.isInternetConnected(this)) {
            mPresenter.loadHotelList(mLoginManager.getUserId());
            mPresenter.loadMetrics(mLoginManager.getUserId());
        } else {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(Utils.HOTEL_LIST, mCurrentHotelList);
        outState.putBoolean(Utils.IS_IN_HOTEL, isInHotelLocation);
        outState.putParcelable(Utils.PORTFOLIO_METRICS, mReceivedMetrics);
        super.onSaveInstanceState(outState);
    }

}
