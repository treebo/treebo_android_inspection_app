package com.treebo.prowlapp.flowportfolionew.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;

import com.google.android.material.snackbar.Snackbar;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.CreatedTaskUpdated;
import com.treebo.prowlapp.events.TaskCreatedEvent;
import com.treebo.prowlapp.flowportfolionew.DaggerPortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.PortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.adapter.SectionedTaskListAdapter;
import com.treebo.prowlapp.flowportfolionew.presenter.TaskListPresenter;
import com.treebo.prowlapp.flowportfolionew.usecase.GetCreatedTaskListUseCase;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.Models.taskcreationmodel.SectionedTaskModel;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.taskcreation.TaskListResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 26/06/17.
 */

public class CreatedTaskListActivity extends AppCompatActivity
        implements IPortfolioContract.ITaskListView, SectionedTaskListAdapter.onTaskClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    @Inject
    protected Navigator mNavigator;

    @Inject
    protected LoginSharedPrefManager mSharedPrefs;

    @Inject
    protected RxBus mRxBus;

    @Inject
    protected TaskListPresenter mPresenter;

    @Inject
    protected GetCreatedTaskListUseCase mUseCase;

    private View mCreateTaskBottomBar;
    private RecyclerView mRecyclerView;
    private View mLoadingView;

    private ArrayList<SectionedTaskModel> mSectionedTaskList = new ArrayList<>();

    private SectionedTaskListAdapter mAdapter;
    private SwipeRefreshLayout mRefreshLayout;

    private CompositeSubscription mSubscription = new CompositeSubscription();
    private TaskListResponse.Tasks mMasterTaskObject;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_created_task_list);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        PortfolioComponentNew portfolioComponent = DaggerPortfolioComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        portfolioComponent.injectTaskListActivity(this);

        View backBtn = findViewById(R.id.toolbar_back_btn);
        backBtn.setOnClickListener(view -> onBackPressed());

        mLoadingView = findViewById(R.id.loader_layout);
        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));

        mCreateTaskBottomBar = findViewById(R.id.floating_bottom_bar);
        mCreateTaskBottomBar.setOnClickListener(view ->
                mNavigator.navigateToTaskCreationActivity(this));
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRefreshLayout.setOnRefreshListener(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager1);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mPresenter.fetchTaskList(mSharedPrefs.getUserId());

        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof CreatedTaskUpdated) {
                boolean isDelete = ((CreatedTaskUpdated) event).isTaskDeleted();
                CreatedTaskModel updatedTask = ((CreatedTaskUpdated) event).getTaskObject();
                boolean isTaskFound = false;
                if (isDelete) {
                    for (CreatedTaskModel model : mMasterTaskObject.getStartedTasks()) {
                        if (model.getTaskID().equals(updatedTask.getTaskID())) {
                            mMasterTaskObject.getStartedTasks().remove(model);
                            isTaskFound = true;
                            break;
                        }
                    }
                    if (!isTaskFound) {
                        for (CreatedTaskModel model : mMasterTaskObject.getYetToStartTasks()) {
                            if (model.getTaskID().equals(updatedTask.getTaskID())) {
                                mMasterTaskObject.getYetToStartTasks().remove(model);
                                break;
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < mMasterTaskObject.getStartedTasks().size(); i++) {
                        CreatedTaskModel model = mMasterTaskObject.getStartedTasks().get(i);
                        if (model.getTaskID().equals(updatedTask.getTaskID())) {
                            isTaskFound = true;
                            mMasterTaskObject.getStartedTasks().set(i, updatedTask);
                            break;
                        }
                    }
                    if (!isTaskFound) {
                        for (int i = 0; i < mMasterTaskObject.getStartedTasks().size(); i++) {
                            CreatedTaskModel model = mMasterTaskObject.getStartedTasks().get(i);
                            if (model.getTaskID().equals(updatedTask.getTaskID())) {
                                mMasterTaskObject.getStartedTasks().set(i, updatedTask);
                                break;
                            }
                        }
                    }
                }
                formSectionedList(mMasterTaskObject);
                mAdapter.refreshList(mSectionedTaskList);
            } else if (event instanceof TaskCreatedEvent) {
                onRefresh();
            }
        }));
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setUseCase(mUseCase);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void setTaskListAdapter(TaskListResponse.Tasks tasks) {
        mMasterTaskObject = tasks;
        formSectionedList(tasks);
        mAdapter = new SectionedTaskListAdapter(this, mSectionedTaskList, this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void formSectionedList(TaskListResponse.Tasks tasks) {
        mSectionedTaskList.clear();
        CreatedTaskModel dummyTask = new CreatedTaskModel();
        dummyTask.setTaskType(getString(R.string.bottom_dummy_task));
        if (tasks.getCompletedTasks().size() > 0) {
            tasks.getCompletedTasks().add(dummyTask);
        } else if (tasks.getYetToStartTasks().size() > 0) {
            tasks.getYetToStartTasks().add(dummyTask);
        } else if (tasks.getStartedTasks().size() > 0) {
            tasks.getStartedTasks().add(dummyTask);
        }
        if (tasks.getStartedTasks().size() > 0) {
            SectionedTaskModel model = new SectionedTaskModel();
            model.setSectionName(getString(R.string.started_tasks));
            model.setTaskList(tasks.getStartedTasks());
            mSectionedTaskList.add(model);
        }
        if (tasks.getYetToStartTasks().size() > 0) {
            SectionedTaskModel model = new SectionedTaskModel();
            model.setSectionName(getString(R.string.yet_to_start_tasks));
            model.setTaskList(tasks.getYetToStartTasks());
            mSectionedTaskList.add(model);
        }
        if (tasks.getCompletedTasks().size() > 0) {
            SectionedTaskModel model = new SectionedTaskModel();
            model.setSectionName(getString(R.string.completed_tasks));
            model.setTaskList(tasks.getCompletedTasks());
            mSectionedTaskList.add(model);
        }
    }

    @Override
    public void onTaskClicked(CreatedTaskModel task) {
        mNavigator.navigateToTaskDetailsActivity(this, task);
    }

    @Override
    public void onTooltipClicked(CreatedTaskModel task) {

    }

    @Override
    public void onRefresh() {
        mRefreshLayout.setRefreshing(false);
        if (Utils.isInternetConnected(this)) {
            mPresenter.fetchTaskList(mSharedPrefs.getUserId());
        } else {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }
}
