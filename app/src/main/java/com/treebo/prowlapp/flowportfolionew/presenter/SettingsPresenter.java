package com.treebo.prowlapp.flowportfolionew.presenter;

import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.observer.LogoutObserver;
import com.treebo.prowlapp.flowportfolionew.usecase.LogoutUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 07/12/16.
 */

public class SettingsPresenter implements IPortfolioContract.ISettingsPresenter {

    private IPortfolioContract.ISettingsView mView;
    private LogoutUseCase mLogoutUseCase;

    @Override
    public void logout(String accessToken) {
        mLogoutUseCase.setAccessToken(accessToken);
        mLogoutUseCase.execute(providesLogoutObserver());
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IPortfolioContract.ISettingsView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    public void setLogoutUseCase(LogoutUseCase useCase) {
        this.mLogoutUseCase = useCase;
    }

    private LogoutObserver providesLogoutObserver() {
        return new LogoutObserver(this, mLogoutUseCase, mView);
    }
}
