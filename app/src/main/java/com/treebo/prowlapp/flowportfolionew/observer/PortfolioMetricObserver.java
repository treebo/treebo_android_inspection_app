package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.usecase.PortfolioMetricUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.portfolio.PortfolioMetricsResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;

/**
 * Created by abhisheknair on 24/11/16.
 */

public class PortfolioMetricObserver extends BaseObserver<PortfolioMetricsResponse> {

    PortfolioMetricUseCase mUseCase;
    IPortfolioContract.IPortfolioView mView;

    public PortfolioMetricObserver(BasePresenter presenter, IPortfolioContract.IPortfolioView view,
                                   PortfolioMetricUseCase useCase) {
        super(presenter, useCase);
        this.mView = view;
        this.mUseCase = useCase;
    }

    @Override
    public void onError(Throwable e) {
        mView.onLoadMetricFailure(e.getMessage());
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(PortfolioMetricsResponse response) {
        mView.hideLoading();
        if (response.status.equals("success")) {
            mView.onLoadMetricSuccess(response);
        } else {
            mView.onLoadMetricFailure("Loading metrics failed");
        }
    }
}
