package com.treebo.prowlapp.flowportfolionew;

import com.treebo.prowlapp.flowportfolionew.presenter.PendingTasksListPresenter;
import com.treebo.prowlapp.flowportfolionew.presenter.PortfolioPresenterNew;
import com.treebo.prowlapp.flowportfolionew.presenter.SettingsPresenter;
import com.treebo.prowlapp.flowportfolionew.presenter.TaskCreationPresenter;
import com.treebo.prowlapp.flowportfolionew.presenter.TaskDetailsPresenter;
import com.treebo.prowlapp.flowportfolionew.presenter.TaskListPresenter;
import com.treebo.prowlapp.flowportfolionew.usecase.AllHotelListUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.CreateTaskUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.DeleteTaskUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.EditTaskUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.GeographicalDataUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.GetCreatedTaskListUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.LogoutUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.PendingTaskListUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.PortfolioMetricUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.SingleHotelDetailsUseCase;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by abhisheknair on 24/11/16.
 */
@Module
public class PortfolioModuleNew {

    @Provides
    RestClient providesRestClient() {
        return new RestClient();
    }

    @Provides
    PortfolioPresenterNew providesPortfolioPresenter() {
        return new PortfolioPresenterNew();
    }

    @Provides
    SettingsPresenter providesSettingsPresenter() {
        return new SettingsPresenter();
    }

    @Provides
    PendingTasksListPresenter providesPendingTaskListPresenter() {
        return new PendingTasksListPresenter();
    }

    @Provides
    TaskCreationPresenter providesTaskCreationPresenter() {
        return new TaskCreationPresenter();
    }

    @Provides
    TaskListPresenter providesTaskListPresenter() {
        return new TaskListPresenter();
    }

    @Provides
    TaskDetailsPresenter providesTaskDetailsPresenter() {
        return new TaskDetailsPresenter();
    }

    @Provides
    AllHotelListUseCase providesAllHotelsUseCase(RestClient restClient) {
        return new AllHotelListUseCase(restClient);
    }

    @Provides
    PortfolioMetricUseCase providesPortfolioMetricsUseCase(RestClient restClient) {
        return new PortfolioMetricUseCase(restClient);
    }

    @Provides
    SingleHotelDetailsUseCase providesSingleHotelDetailsUseCase(RestClient restClient) {
        return new SingleHotelDetailsUseCase(restClient);
    }

    @Provides
    LogoutUseCase providesLogoutUseCase(RestClient restClient) {
        return new LogoutUseCase(restClient);
    }

    @Provides
    PendingTaskListUseCase providesPendingTaskListUseCase(RestClient restClient) {
        return new PendingTaskListUseCase(restClient);
    }

    @Provides
    GeographicalDataUseCase providesGeographicalDatatUseCase(RestClient restClient) {
        return new GeographicalDataUseCase(restClient);
    }

    @Provides
    GetCreatedTaskListUseCase providesCreatedTaskListUseCase(RestClient restClient) {
        return new GetCreatedTaskListUseCase(restClient);
    }

    @Provides
    DeleteTaskUseCase providesDeleteTaskUseCase(RestClient restClient) {
        return new DeleteTaskUseCase(restClient);
    }

    @Provides
    EditTaskUseCase providesEditTaskUseCase(RestClient restClient) {
        return new EditTaskUseCase(restClient);
    }

    @Provides
    CreateTaskUseCase providesCreateTaskUseCase(RestClient restClient) {
        return new CreateTaskUseCase(restClient);
    }
}
