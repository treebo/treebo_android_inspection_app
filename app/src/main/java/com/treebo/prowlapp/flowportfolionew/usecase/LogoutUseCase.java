package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.login.LogoutResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 07/12/16.
 */

public class LogoutUseCase extends UseCase<LogoutResponse> {

    public RestClient mRestClient;

    public void setAccessToken(String mAccessToken) {
        this.mAccessToken = mAccessToken;
    }

    private String mAccessToken;

    public LogoutUseCase(RestClient restClient) {
        mRestClient = restClient;
    }


    @Override
    protected Observable<LogoutResponse> getObservable() {
        return mRestClient.logout(mAccessToken);
    }
}
