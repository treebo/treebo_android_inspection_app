package com.treebo.prowlapp.flowportfolionew.presenter;

import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.observer.CreateTaskObserver;
import com.treebo.prowlapp.flowportfolionew.observer.GeographicalDataObserver;
import com.treebo.prowlapp.flowportfolionew.usecase.CreateTaskUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.GeographicalDataUseCase;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 21/06/17.
 */

public class TaskCreationPresenter implements IPortfolioContract.ITaskCreationPresenter {

    private IPortfolioContract.ITaskCreationView mView;
    private GeographicalDataUseCase mGeographicalDataUseCase;
    private CreateTaskUseCase mCreateTaskUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IPortfolioContract.ITaskCreationView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {
        mView.setUpViewAsPerPermissions();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void onCreate() {
        fetchGeographicalData();
    }

    @Override
    public void fetchGeographicalData() {
        mView.showLoading();
        mGeographicalDataUseCase.execute(providesGeographicalDataObserver());
    }

    @Override
    public void setFetchedData(GeographicalDataResponse regions) {
        mView.hideLoading();
        mView.updateProgressBar();
        mView.saveGeographicalData(regions);
        mView.loadViewPager();
    }

    @Override
    public void dataFetchError(String msg) {
        mView.hideLoading();
        mView.showError(msg);
    }

    @Override
    public void postNewTask(int userID, CreatedTaskModel task) {
        mView.showLoading();
        mCreateTaskUseCase.setCreatedTask(userID, task);
        mCreateTaskUseCase.execute(providesCreateTaskObserver());
    }

    @Override
    public void onTaskCreationSuccess(CreatedTaskModel response) {
        mView.hideLoading();
        mView.onSubmitTaskSuccess(response);
    }

    @Override
    public void onTaskCreationFailure(String msg) {
        mView.hideLoading();
        mView.showError(msg);
    }

    public void setGeographicalDataUseCase(GeographicalDataUseCase useCase) {
        this.mGeographicalDataUseCase = useCase;
    }
    private GeographicalDataObserver providesGeographicalDataObserver() {
        return new GeographicalDataObserver(this, mGeographicalDataUseCase);
    }

    public void setCreateTaksDataUseCase(CreateTaskUseCase useCase) {
        this.mCreateTaskUseCase = useCase;
    }
    private CreateTaskObserver providesCreateTaskObserver() {
        return new CreateTaskObserver(this, mCreateTaskUseCase);
    }
}
