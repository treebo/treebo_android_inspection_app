package com.treebo.prowlapp.flowportfolionew.adapter;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.taskcreationmodel.TextSubtextModel;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 23/06/17.
 */

public class SelectListAdapter extends RecyclerView.Adapter<SelectListAdapter.SelectListViewHolder> {

    private ArrayList<TextSubtextModel> dataList;
    private ArrayList<TextSubtextModel> displayList;
    private onItemClickListener mListener;

    public SelectListAdapter(ArrayList<TextSubtextModel> list, onItemClickListener listener) {
        this.dataList = list;
        this.displayList = list;
        this.mListener = listener;
    }

    public void filter(String searchText) {
        ArrayList<TextSubtextModel> list = new ArrayList<>();
        if (TextUtils.isEmpty(searchText)) {
            list.addAll(dataList);
        } else {
            for (TextSubtextModel model : dataList) {
                if (model.getText().toLowerCase().contains(searchText.toLowerCase())
                        || model.getSubText().toLowerCase().contains(searchText.toLowerCase())) {
                    list.add(model);
                }
            }
        }
        displayList = list;
        notifyDataSetChanged();
    }

    @Override
    public SelectListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_list,
                parent, false);
        return new SelectListViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(SelectListViewHolder holder, int position) {
        TextSubtextModel model = displayList.get(position);
        holder.model = model;
        holder.text.setText(model.getText());
        holder.subText.setText(model.getSubText());
        holder.checkBox.setChecked(model.isSelected());
    }

    @Override
    public int getItemCount() {
        return displayList != null ? displayList.size() : 0;
    }

    public class SelectListViewHolder extends RecyclerView.ViewHolder {

        private TextSubtextModel model;
        private TreeboTextView text;
        private TreeboTextView subText;
        private AppCompatCheckBox checkBox;

        public SelectListViewHolder(View itemView, onItemClickListener listener) {
            super(itemView);
            text = (TreeboTextView) itemView.findViewById(R.id.item_header_tv);
            subText = (TreeboTextView) itemView.findViewById(R.id.item_subheader_tv);
            checkBox = (AppCompatCheckBox) itemView.findViewById(R.id.item_checkbox);
            checkBox.setOnCheckedChangeListener((view, isChecked) -> {
                checkBox.setChecked(isChecked);
                int index = dataList.indexOf(model);
                model.setSelected(isChecked);
                if (index != -1)
                    dataList.set(index, model);
                listener.onListItemClicked(model);
            });
            itemView.setOnClickListener(view -> {
                checkBox.setChecked(!checkBox.isChecked());
                int index = dataList.indexOf(model);
                model.setSelected(checkBox.isChecked());
                if (index != -1)
                    dataList.set(index, model);
                listener.onListItemClicked(model);
            });
        }
    }

    public interface onItemClickListener {
        void onListItemClicked(TextSubtextModel model);
    }
}
