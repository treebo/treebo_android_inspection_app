package com.treebo.prowlapp.flowportfolionew.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.views.TreeboEditText;

import androidx.fragment.app.Fragment;

/**
 * Created by abhisheknair on 21/06/17.
 */

public class TaskTooltipFragment extends Fragment {

    private TreeboEditText mTaskTooltipEt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_tooltip, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTaskTooltipEt = (TreeboEditText) view.findViewById(R.id.task_tooltip_edittext);
    }

    public String getTaskToolTip() {
        return mTaskTooltipEt == null ? "" : mTaskTooltipEt.getText().toString();
    }
}
