package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.taskcreation.TaskListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class GetCreatedTaskListUseCase extends UseCase<TaskListResponse> {

    private RestClient mRestClient;
    private int mUserID;

    public GetCreatedTaskListUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setUserID(int userID) {
        this.mUserID = userID;
    }

    @Override
    protected Observable<TaskListResponse> getObservable() {
        return mRestClient.getCreatedTaskList(mUserID);
    }
}
