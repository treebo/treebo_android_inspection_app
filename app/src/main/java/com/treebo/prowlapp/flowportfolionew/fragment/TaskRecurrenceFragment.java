package com.treebo.prowlapp.flowportfolionew.fragment;


import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

import androidx.fragment.app.Fragment;
import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by abhisheknair on 28/06/17.
 */

public class TaskRecurrenceFragment extends Fragment implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener {

    private boolean isRecurrenceEnabled;
    private CreatedTaskModel.Recurrence mRecurrenceObject = new CreatedTaskModel.Recurrence();

    private AppCompatCheckBox mRepeatCheckbox;
    private AppCompatCheckBox mOnlyOnceCheckbox;

    MaterialSpinner maxOccurrenceSpinner;
    TreeboTextView setEndDateTv;

    private Context mContext;
    private boolean isDialogOnDisplay;
    private boolean isFromOnlyOnce;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = inflater.getContext();
        return inflater.inflate(R.layout.fragment_task_recurrence, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View mRepeatBtn = view.findViewById(R.id.repeat_rl);
        mRepeatCheckbox = (AppCompatCheckBox) mRepeatBtn.findViewById(R.id.repeat_checkbox);
        mRepeatCheckbox.setOnCheckedChangeListener((view1, isChecked) -> {
            if (!isDialogOnDisplay && !isFromOnlyOnce)
                showTaskRecurrenceDialog();
        });
        mRepeatBtn.setOnClickListener(itemView -> {
            if (!isDialogOnDisplay)
                showTaskRecurrenceDialog();
        });
        View mOnlyOnceBtn = view.findViewById(R.id.only_once_rl);
        mOnlyOnceCheckbox = (AppCompatCheckBox) mOnlyOnceBtn.findViewById(R.id.only_once_checkbox);
        mOnlyOnceBtn.setOnClickListener(itemView -> {
            isRecurrenceEnabled = false;
            mOnlyOnceCheckbox.setChecked(!mOnlyOnceCheckbox.isChecked());
        });
        mOnlyOnceCheckbox.setOnCheckedChangeListener((view1, isChecked) -> {
            isRecurrenceEnabled = false;
            if (isChecked && mRepeatCheckbox.isChecked()) {
                isFromOnlyOnce = true;
                mRepeatCheckbox.setChecked(false);
                isFromOnlyOnce = false;
            }
        });
    }

    public CreatedTaskModel.Recurrence getRecurrenceObject() {
        mRecurrenceObject.setValidOccurrence(isRecurrenceEnabled);
        return isRecurrenceEnabled ? mRecurrenceObject : null;
    }

    private void showTaskRecurrenceDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_task_recurrence, null);

        setEndDateTv = (TreeboTextView) dialogView.findViewById(R.id.recurrence_end_date_tv);
        setEndDateTv.setOnClickListener(view ->
                openDatePicker());
        View calendarIcon = dialogView.findViewById(R.id.ic_calender);
        calendarIcon.setOnClickListener(view -> openDatePicker());
        MaterialSpinner repeatFrequency = (MaterialSpinner) dialogView.findViewById(R.id.frequency_spinner);
        String numberList[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        ArrayAdapter<String> repeatAdapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_spinner_item, numberList);
        repeatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        repeatFrequency.setAdapter(repeatAdapter);
        repeatFrequency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1)
                    mRecurrenceObject.setSeparationCount(Integer.valueOf(repeatAdapter.getItem(position)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        maxOccurrenceSpinner = (MaterialSpinner) dialogView.findViewById(R.id.max_occurence_spinner);
        ArrayAdapter<String> maxOccuranceAdapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_spinner_item, numberList);
        maxOccuranceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        maxOccurrenceSpinner.setAdapter(maxOccuranceAdapter);
        maxOccurrenceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    mRecurrenceObject.setMaxOccurrences(Integer.valueOf(maxOccuranceAdapter.getItem(position)));
                    setEndDateTv.setText(getString(R.string.task_end_date_field));
                    mRecurrenceObject.setEndDate(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialogBuilder.setView(dialogView);
        final AlertDialog pauseAuditDialog = dialogBuilder.create();
        pauseAuditDialog.show();
        isDialogOnDisplay = true;

        if (mRecurrenceObject != null && mRecurrenceObject.getSeparationCount() > 0) {
            repeatFrequency.setSelection(mRecurrenceObject.getSeparationCount());
        }
        if (mRecurrenceObject != null && mRecurrenceObject.getMaxOccurrences() > 0) {
            maxOccurrenceSpinner.setSelection(mRecurrenceObject.getMaxOccurrences());
        }
        if (mRecurrenceObject != null && !TextUtils.isEmpty(mRecurrenceObject.getEndDate())) {
            setEndDateTv.setText(mRecurrenceObject.getEndDate());
        }

        TreeboButton doneBtn = (TreeboButton) dialogView.findViewById(R.id.done_btn);
        doneBtn.setOnClickListener(view -> {
            if (mRecurrenceObject.getSeparationCount() == 0) {
                repeatFrequency.setError(getString(R.string.mandatory_field_error));
            } else if (mRecurrenceObject.getMaxOccurrences() == 0
                    && TextUtils.isEmpty(mRecurrenceObject.getEndDate())) {
                maxOccurrenceSpinner.setError(getString(R.string.recurrence_occurrence_error));
            } else {
                isRecurrenceEnabled = true;
                mRepeatCheckbox.setChecked(true);
                mOnlyOnceCheckbox.setChecked(false);
                pauseAuditDialog.dismiss();
            }
        });
        pauseAuditDialog.setOnDismissListener(dialog -> {
            isDialogOnDisplay = false;
        });
        pauseAuditDialog.setOnCancelListener(dialog -> {
            isDialogOnDisplay = false;
        });

    }

    private void openDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH),
                getString(R.string.cancel)
        );
        dpd.setMinDate(now);
        dpd.setAccentColor(ContextCompat.getColor(mContext, R.color.emerald));
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private void openTimePicker() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog dpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        dpd.setAccentColor(ContextCompat.getColor(mContext, R.color.emerald));
        dpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String date = mRecurrenceObject.getEndDate();
        date += " " + hourOfDay + ":" + minute + ":00";
        mRecurrenceObject.setEndDate(date);
        setEndDateTv.setText(DateUtils.getDisplayTimeFromServerTime(date));
        maxOccurrenceSpinner.setSelection(0);
        mRecurrenceObject.setMaxOccurrences(null);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear += 1;
        mRecurrenceObject.setEndDate(year + "-" + monthOfYear + "-" + dayOfMonth);
        openTimePicker();
    }
}
