package com.treebo.prowlapp.flowportfolionew.presenter;

import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.observer.AllHotelListObserver;
import com.treebo.prowlapp.flowportfolionew.observer.PortfolioMetricObserver;
import com.treebo.prowlapp.flowportfolionew.observer.SingleHotelDetailsObserver;
import com.treebo.prowlapp.flowportfolionew.usecase.AllHotelListUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.PortfolioMetricUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.SingleHotelDetailsUseCase;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioHotelV3;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioMetrics;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class PortfolioPresenterNew implements IPortfolioContract.IPortfolioPresenter {

    private IPortfolioContract.IPortfolioView mView;
    private AllHotelListUseCase mHotelListUseCase;
    private PortfolioMetricUseCase mMetricUseCase;
    private SingleHotelDetailsUseCase mSingleHotelUseCase;
    private int mUserID;

    public PortfolioPresenterNew() {
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IPortfolioContract.IPortfolioView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {
        mView.setUpViewAsPerPermissions();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void onCreate() {
        if (mView != null)
            mView.setUpRecyclerView();
    }

    @Override
    public void loadHotelList(int userID) {
        // Make API call
        mView.showLoading();
        mUserID = userID;
        mHotelListUseCase.setUserID(userID);
        mHotelListUseCase.execute(providesHotelListObserver());
    }

    @Override
    public void loadMetrics(int userID) {
        mMetricUseCase.setUserID(userID);
        mMetricUseCase.execute(providesMetricsObserver());
    }

    @Override
    public void loadSingleHotelDetails(int userID, int hotelID) {
        mSingleHotelUseCase.setFields(userID, hotelID);
        mSingleHotelUseCase.execute(providesSingleHotelDetailsObserver());
    }

    @Override
    public void updateHotelList(boolean isHotelInCurrentLocation) {
        if (mView != null)
            mView.arrangeHotelsInSections(isHotelInCurrentLocation);
    }

    @Override
    public void setScores(PortfolioMetrics metrics) {
        if (mView != null)
            mView.setUpScoresData(metrics);
    }

    @Override
    public void updateSingleHotelDetails(PortfolioHotelV3 hotel) {
        mView.setMetricsForSingleHotel(hotel);
    }

    public void setHotelListUseCase(AllHotelListUseCase useCase) {
        this.mHotelListUseCase = useCase;
    }

    public void setMetricUseCase(PortfolioMetricUseCase useCase) {
        this.mMetricUseCase = useCase;
    }

    public void setSingleHotelUseCase(SingleHotelDetailsUseCase useCase) {
        this.mSingleHotelUseCase = useCase;
    }

    private AllHotelListObserver providesHotelListObserver() {
        return new AllHotelListObserver(this, mView, mHotelListUseCase);
    }

    private PortfolioMetricObserver providesMetricsObserver() {
        return new PortfolioMetricObserver(this, mView, mMetricUseCase);
    }

    private SingleHotelDetailsObserver providesSingleHotelDetailsObserver() {
        return new SingleHotelDetailsObserver(this, mSingleHotelUseCase);
    }
}
