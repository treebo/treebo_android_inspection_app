package com.treebo.prowlapp.flowportfolionew.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.TaskAreaSelectedEvent;
import com.treebo.prowlapp.flowportfolionew.DaggerPortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.PortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.adapter.SelectListAdapter;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.Models.taskcreationmodel.TextSubtextModel;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 23/06/17.
 */

public class SelectListActivity extends AppCompatActivity
        implements SelectListAdapter.onItemClickListener, TextWatcher {

    @Inject
    protected LoginSharedPrefManager mSharedPrefs;

    @Inject
    protected RxBus mRxBus;

    private RecyclerView mRecyclerView;
    private SelectListAdapter mAdapter;
    private TreeboEditText mSearchBar;

    private GeographicalDataResponse geographicalData;
    private ArrayList<TextSubtextModel> displayList;
    private boolean isRegion;
    private boolean isCluster;
    private boolean isSearchEnabled;
    private boolean isCity;
    private boolean isProperty;
    private String type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_list);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        PortfolioComponentNew portfolioComponent = DaggerPortfolioComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        portfolioComponent.injectSelectListActivity(this);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            isRegion = extras.getBoolean(Utils.IS_REGION, false);
            isCluster = extras.getBoolean(Utils.IS_CLUSTER, false);
            isSearchEnabled = extras.getBoolean(Utils.IS_SEARCH_ENABLED, false);
            isCity = extras.getBoolean(Utils.IS_CITY, false);
            isProperty = extras.getBoolean(Utils.IS_PROPERTY, false);
            type = extras.getString(Utils.LIST_TYPE, "");
            displayList = extras.getParcelableArrayList(Utils.SELECTED_LIST);
        } else {
            isRegion = savedInstanceState.getBoolean(Utils.IS_REGION);
            isCluster = savedInstanceState.getBoolean(Utils.IS_CLUSTER);
            isSearchEnabled = savedInstanceState.getBoolean(Utils.IS_SEARCH_ENABLED);
            isCity = savedInstanceState.getBoolean(Utils.IS_CITY);
            isProperty = savedInstanceState.getBoolean(Utils.IS_PROPERTY);
            type = savedInstanceState.getString(Utils.LIST_TYPE);
            displayList = savedInstanceState.getParcelableArrayList(Utils.SELECTED_LIST);
        }

        geographicalData = mSharedPrefs.getGeographicalData();

        View toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        TreeboTextView title = (TreeboTextView) toolbar.findViewById(R.id.toolbar_title_text);
        title.setText(getString(R.string.select_title, type));
        View crossBtn = toolbar.findViewById(R.id.room_picker_back);
        crossBtn.setOnClickListener(view -> onBackPressed());

        AppCompatCheckBox checkBox = (AppCompatCheckBox) findViewById(R.id.item_checkbox);
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            setResetAllItems(isChecked);
        });
        TreeboTextView firstElementTitle = (TreeboTextView) findViewById(R.id.item_header_tv);
        firstElementTitle.setText(isRegion ? getString(R.string.all_regions)
                : getString(R.string.all_clusters));
        TreeboTextView firstElementSubTitle = (TreeboTextView) findViewById(R.id.item_subheader_tv);
        firstElementSubTitle.setText(isRegion
                ? getString(R.string.number_clusters, geographicalData.getClusterList().size())
                : getString(R.string.all_clusters));

        mSearchBar = (TreeboEditText) findViewById(R.id.search_edittext);
        mSearchBar.clearFocus();
        mSearchBar.addTextChangedListener(this);

        if (isSearchEnabled) {
            mSearchBar.setVisibility(View.VISIBLE);
            checkBox.setVisibility(View.INVISIBLE);
            firstElementSubTitle.setVisibility(View.GONE);
            firstElementTitle.setVisibility(View.GONE);
        } else {
            mSearchBar.setVisibility(View.INVISIBLE);
            checkBox.setVisibility(View.VISIBLE);
            firstElementSubTitle.setVisibility(View.VISIBLE);
            firstElementTitle.setVisibility(View.VISIBLE);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager1);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        if (displayList == null || displayList.size() == 0)
            formListData();
        mAdapter = new SelectListAdapter(displayList, this);
        mRecyclerView.setAdapter(mAdapter);

        View doneBtn = findViewById(R.id.comment_done_btn);
        doneBtn.setOnClickListener(view -> {
            if (getSelectedIDList().size() == 0)
                type = "empty";
            mRxBus.postEvent(new TaskAreaSelectedEvent(type, getSelectedIDList(),
                    !isSearchEnabled && checkBox.isChecked()));
            onBackPressed();
        });

    }

    private ArrayList<TextSubtextModel> getSelectedIDList() {
        ArrayList<TextSubtextModel> list = new ArrayList<>();
        for (TextSubtextModel model : displayList) {
            if (model.isSelected()) {
                list.add(model);
            }
        }
        return list;
    }

    private void formListData() {
        displayList = new ArrayList<>();
        if (isSearchEnabled) {
            if (isCity) {
                for (GeographicalDataResponse.Region region : geographicalData.getData().getRegionList()) {
                    for (GeographicalDataResponse.Cluster cluster : region.getClusterList()) {
                        for (GeographicalDataResponse.City city : cluster.getCityList()) {
                            for (Integer id : city.getPermittedUserList()) {
                                if (id == mSharedPrefs.getUserId()) {
                                    TextSubtextModel model = new TextSubtextModel();
                                    model.setText(city.getCityName());
                                    model.setSubText(getString(R.string.number_properties, city.getHotelList().size()));
                                    model.setId(city.getId());
                                    displayList.add(model);
                                }
                            }
                        }
                    }
                }
            } else {
                for (GeographicalDataResponse.Region region : geographicalData.getData().getRegionList()) {
                    for (GeographicalDataResponse.Cluster cluster : region.getClusterList()) {
                        for (GeographicalDataResponse.City city : cluster.getCityList()) {
                            for (GeographicalDataResponse.Hotel hotel : city.getHotelList()) {
                                TextSubtextModel model = new TextSubtextModel();
                                model.setText(hotel.getName());
                                model.setSubText(hotel.getCity());
                                model.setId(hotel.getId());
                                displayList.add(model);
                            }
                        }
                    }
                }
            }
        } else {
            if (isRegion) {
                for (GeographicalDataResponse.Region region : geographicalData.getData().getRegionList()) {
                    for (Integer id : region.getPermittedUserList()) {
                        if (id == mSharedPrefs.getUserId()) {
                            TextSubtextModel model = new TextSubtextModel();
                            model.setText(region.getRegionName());
                            model.setId(region.getId());
                            StringBuilder subText = new StringBuilder();
                            for (int i = 0; i < region.getClusterList().size(); i++) {
                                if (i < 4) {
                                    GeographicalDataResponse.Cluster cluster = region.getClusterList().get(i);
                                    subText.append(cluster.getClusterName());
                                    subText.append(" ,");
                                } else {
                                    subText.append("...");
                                    break;
                                }
                            }
                            model.setSubText(subText.toString());
                            displayList.add(model);
                        }
                    }
                }
            } else if (isCluster) {
                for (GeographicalDataResponse.Region region : geographicalData.getData().getRegionList()) {
                    for (GeographicalDataResponse.Cluster cluster : region.getClusterList()) {
                        for (Integer id : cluster.getPermittedUserList()) {
                            if (id == mSharedPrefs.getUserId()) {
                                TextSubtextModel model = new TextSubtextModel();
                                model.setText(cluster.getClusterName());
                                model.setId(cluster.getId());
                                StringBuilder subText = new StringBuilder();
                                for (int i = 0; i < cluster.getCityList().size(); i++) {
                                    if (i < 4) {
                                        GeographicalDataResponse.City city = cluster.getCityList().get(i);
                                        subText.append(city.getCityName());
                                        subText.append(" ,");
                                    } else {
                                        subText.append("...");
                                        break;
                                    }
                                }
                                model.setSubText(subText.toString());
                                displayList.add(model);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onListItemClicked(TextSubtextModel model) {
        for (int i = 0; i < displayList.size(); i++) {
            if (displayList.get(i).getText().equals(model.getText()))
                displayList.get(i).setSelected(model.isSelected());
        }
    }

    private void setResetAllItems(boolean set) {
        for (TextSubtextModel model : displayList) {
            model.setSelected(set);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Utils.GEOGRAPHICAL_DATA, geographicalData);
        outState.putBoolean(Utils.IS_REGION, isRegion);
        outState.putBoolean(Utils.IS_CLUSTER, isCluster);
        outState.putBoolean(Utils.IS_SEARCH_ENABLED, isSearchEnabled);
        outState.putBoolean(Utils.IS_PROPERTY, isCity);
        outState.putBoolean(Utils.IS_CITY, isProperty);
        outState.putString(Utils.LIST_TYPE, type);
        outState.putParcelableArrayList(Utils.SELECTED_LIST, displayList);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        mAdapter.filter(s.toString());
    }
}
