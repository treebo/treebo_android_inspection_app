package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class DeleteTaskUseCase extends UseCase<BaseResponse> {

    private RestClient mRestClient;
    private int userID;
    private int taskID;

    public DeleteTaskUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setField(int userID, int taskID) {
        this.userID = userID;
        this.taskID = taskID;
    }

    @Override
    protected Observable<BaseResponse> getObservable() {
        return mRestClient.deleteCreatedTask(userID, taskID);
    }
}
