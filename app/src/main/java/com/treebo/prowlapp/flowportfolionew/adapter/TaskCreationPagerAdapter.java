package com.treebo.prowlapp.flowportfolionew.adapter;



import android.content.Context;
import android.os.Bundle;

import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskCreateForFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskDetailsFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskEndDateFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskGeofenceFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskLinkFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskNameFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskRecurrenceFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskStartDateFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskTooltipFragment;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Created by abhisheknair on 21/06/17.
 */

public class TaskCreationPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<String> mTaskFieldsList;
    private int mUserID;
    private GeographicalDataResponse geographicalData;
    private TaskCreateForFragment.onAreaClicked mAreaClickedListener;

    public TaskCreationPagerAdapter(FragmentManager fm, Context context,
                                    GeographicalDataResponse data, int userID,
                                    TaskCreateForFragment.onAreaClicked listener) {
        super(fm);
        this.mTaskFieldsList =
                new ArrayList<>(Arrays.asList(context.getResources()
                        .getStringArray(R.array.task_creation_fields)));
        this.mUserID = userID;
        this.geographicalData = data;
        this.mAreaClickedListener = listener;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position % getCount()) {
            case 0:
                fragment = new TaskNameFragment();
                break;

            case 1:
                fragment = new TaskDetailsFragment();
                break;

            case 2:
                fragment = new TaskStartDateFragment();
                break;

            case 3:
                fragment = new TaskEndDateFragment();
                break;

            case 4:
                fragment = new TaskCreateForFragment();
                Bundle arguments = new Bundle();
                arguments.putParcelable(Utils.GEOGRAPHICAL_DATA, geographicalData);
                arguments.putInt(Utils.USER_ID, mUserID);
                fragment.setArguments(arguments);
                ((TaskCreateForFragment) fragment).setAreaClickedListener(mAreaClickedListener);
                break;

            case 5:
                fragment = new TaskRecurrenceFragment();
                break;

            case 6:
                fragment = new TaskTooltipFragment();
                break;

            case 7:
                fragment = new TaskLinkFragment();
                break;

            case 8:
                fragment = new TaskGeofenceFragment();
                break;

            default:
                fragment = new TaskNameFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return mTaskFieldsList != null ? mTaskFieldsList.size() + 1 : 0;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
//        super.destroyItem(container, position, object);
    }
}
