package com.treebo.prowlapp.flowportfolionew.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.TaskAreaSelectedEvent;
import com.treebo.prowlapp.events.TaskCreatedEvent;
import com.treebo.prowlapp.flowportfolionew.DaggerPortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.PortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.adapter.TaskCreationPagerAdapter;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskCreateForFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskDetailsFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskEndDateFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskGeofenceFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskLinkFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskNameFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskRecurrenceFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskStartDateFragment;
import com.treebo.prowlapp.flowportfolionew.fragment.TaskTooltipFragment;
import com.treebo.prowlapp.flowportfolionew.presenter.TaskCreationPresenter;
import com.treebo.prowlapp.flowportfolionew.usecase.CreateTaskUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.GeographicalDataUseCase;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.Models.taskcreationmodel.TextSubtextModel;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;
import com.treebo.prowlapp.views.VerticalSwipeDisabledViewPager;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 20/06/17.
 */

public class TaskCreationActivity extends AppCompatActivity
        implements IPortfolioContract.ITaskCreationView, TaskCreateForFragment.onAreaClicked {


    @Inject
    protected LoginSharedPrefManager mSharedPrefs;

    @Inject
    protected TaskCreationPresenter mPresenter;

    @Inject
    protected GeographicalDataUseCase mGeographicalDataUseCase;

    @Inject
    protected CreateTaskUseCase mCreateTaskUseCase;

    @Inject
    protected RxBus mRxBus;

    private VerticalSwipeDisabledViewPager mViewPager;
    private ImageButton mScrollUp;
    private ImageButton mScrollDown;

    private TaskCreationPagerAdapter mPagerAdapter;

    private View mLoadingView;
    private View mSuccessView;
    private TreeboTextView mFieldNameTv;

    private ProgressBar mAuditProgressBar;
    private TreeboTextView mAuditProgressStatus;

    private ArrayList<String> mTaskFieldsList;
    private GeographicalDataResponse mGeographicalData;

    private CreatedTaskModel mCreatedTask = new CreatedTaskModel();

    private CompositeSubscription mSubscription = new CompositeSubscription();
    private ArrayList<TextSubtextModel> mSelectedList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_creation);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        PortfolioComponentNew portfolioComponent = DaggerPortfolioComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        portfolioComponent.injectTaskCreationActivity(this);

        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof TaskAreaSelectedEvent) {
                String type = ((TaskAreaSelectedEvent) event).getType();
                mSelectedList = ((TaskAreaSelectedEvent) event).getList();
                int[] idList = new int[mSelectedList.size()];
                for (int i = 0; i < idList.length; i++) {
                    idList[i] = mSelectedList.get(i).getId();
                }
                switch (type) {
                    case "region":
                        mCreatedTask.setRegionIDList(idList);
                        break;

                    case "cluster":
                        mCreatedTask.setClusterIDList(idList);
                        break;

                    case "property":
                        mCreatedTask.setHotelIDList(idList);
                        break;

                    case "city":
                        mCreatedTask.setCityIDList(idList);
                        break;
                }
                String displayText = ((TaskAreaSelectedEvent) event).getDisplayText();
                ((TaskCreateForFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).resetFragmentView(type, displayText);
            }
        }));

        View backBtn = findViewById(R.id.toolbar_audit_back_btn);
        backBtn.setOnClickListener(view -> onBackPressed());

        mTaskFieldsList = new ArrayList<>(Arrays.asList(getResources()
                .getStringArray(R.array.task_creation_fields)));

        mLoadingView = findViewById(R.id.loader_layout);
        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));

        mSuccessView = findViewById(R.id.audit_success_layout);
        TreeboTextView successText =
                (TreeboTextView) mSuccessView.findViewById(R.id.audit_successfully_submitted);
        successText.setText(getString(R.string.task_created_successfully));

        mAuditProgressBar = (ProgressBar) findViewById(R.id.toolbar_audit_progress_bar);
        mAuditProgressBar.setMax(mTaskFieldsList.size());
        mAuditProgressStatus = (TreeboTextView) findViewById(R.id.toolbar_audit_progress_tv);

        mViewPager = (VerticalSwipeDisabledViewPager) findViewById(R.id.task_viewpager);
        mScrollUp = (ImageButton) findViewById(R.id.scroll_up_btn);
        mScrollDown = (ImageButton) findViewById(R.id.scroll_down_btn);

        mFieldNameTv = (TreeboTextView) findViewById(R.id.task_field_name_tv);
        mFieldNameTv.setText(mTaskFieldsList.get(0));

        mScrollDown.setOnClickListener(view -> {
            int currentItem = mViewPager.getCurrentItem();
            if (currentItem == 7)
                mScrollDown.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_done_white));
            else
                mScrollDown.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand_more_white));
            if (currentItem != 8 && validatePageDetails()) {
                mViewPager.setCurrentItem(currentItem + 1);
                mFieldNameTv.setText(mTaskFieldsList.get(currentItem));
                Utils.hideSoftKeyboard(findViewById(android.R.id.content));
            } else {
                if (validatePageDetails()) {
                    mPresenter.postNewTask(mSharedPrefs.getUserId(), mCreatedTask);
                }
            }
            updateProgressBar();
        });

        mScrollUp.setOnClickListener(view -> {
            int currentItem = mViewPager.getCurrentItem();
            if (currentItem == 8)
                mScrollDown.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand_more_white));
            if (currentItem != 0) {
                mViewPager.setCurrentItem(currentItem - 1);
                mFieldNameTv.setText(mTaskFieldsList.get(currentItem));
                Utils.hideSoftKeyboard(findViewById(android.R.id.content));
            }
            updateProgressBar();
        });

        mPresenter.onCreate();

    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(mCreatedTask.getTaskName())) {
            showAreYouSureDialog();
        } else {
            super.onBackPressed();
        }
    }

    private void showAreYouSureDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_update_back, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog stopUpdateDialog = dialogBuilder.create();
        stopUpdateDialog.show();

        TreeboTextView cancelBtn = (TreeboTextView) dialogView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(view -> stopUpdateDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            stopUpdateDialog.dismiss();
            finish();
            overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        });
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setGeographicalDataUseCase(mGeographicalDataUseCase);
        mPresenter.setCreateTaksDataUseCase(mCreateTaskUseCase);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void loadViewPager() {
        if (mPagerAdapter == null) {
            mPagerAdapter = new TaskCreationPagerAdapter(getSupportFragmentManager(), this,
                    mGeographicalData, mSharedPrefs.getUserId(), this);
            mViewPager.setPagingEnabled(false);
            mViewPager.setAdapter(mPagerAdapter);
        }
    }

    @Override
    public void saveGeographicalData(GeographicalDataResponse regions) {
        mGeographicalData = regions;
        mSharedPrefs.setGeographicalData(regions);
    }

    @Override
    public void submitTask() {

    }

    @Override
    public void onSubmitTaskSuccess(CreatedTaskModel createdTask) {
        mSuccessView.setVisibility(View.VISIBLE);
        mRxBus.postEvent(new TaskCreatedEvent());
        new Handler().postDelayed(() ->
                finish(), 2000);
    }

    private boolean validatePageDetails() {
        boolean isValid;
        switch (mViewPager.getCurrentItem()) {
            case 0:
                String taskName = ((TaskNameFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).getTaskName();
                isValid = !TextUtils.isEmpty(taskName);
                mCreatedTask.setTaskName(taskName);
                break;

            case 1:
                String taskDetails = ((TaskDetailsFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).getTaskDetails();
                isValid = true;
                mCreatedTask.setTaskDetails(taskDetails);
                break;

            case 2:
                String appearDate = ((TaskStartDateFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).getTaskAppearDate();
                isValid = ((TaskStartDateFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).isDateEntered();
                mCreatedTask.setAppearDate(appearDate);
                break;

            case 3:
                String completionDate = ((TaskEndDateFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).getTaskCompletionDate();
                isValid = ((TaskEndDateFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).isDateEntered();
                mCreatedTask.setCompletionDate(completionDate);
                break;

            case 4:
                isValid = (mCreatedTask.getCityIDList().length > 0
                        || mCreatedTask.getHotelIDList().length > 0
                        || mCreatedTask.getRegionIDList().length > 0
                        || mCreatedTask.getClusterIDList().length > 0);
                // set cluster, region, hotel ids
                break;

            case 5:
                isValid = true;
                CreatedTaskModel.Recurrence recurrence =
                        ((TaskRecurrenceFragment) mPagerAdapter
                                .instantiateItem(mViewPager, mViewPager.getCurrentItem())).getRecurrenceObject();
                mCreatedTask.setRecurrence(recurrence);
                // set recurrence object
                break;

            case 6:
                String taskTooltip = ((TaskTooltipFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).getTaskToolTip();
                isValid = true;
                mCreatedTask.setTooltipText(taskTooltip);
                // set tooltip object
                break;

            case 7:
                String taskLink = ((TaskLinkFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).getTaskLink();
                isValid = true;
                mCreatedTask.setContentLink(taskLink);
                // set content link
                break;

            case 8:
                boolean isGeofenced = ((TaskGeofenceFragment) mPagerAdapter
                        .instantiateItem(mViewPager, mViewPager.getCurrentItem())).isGeofenceEnabled();
                isValid = true;
                mCreatedTask.setGeofenceEnabled(isGeofenced);
                break;

            default:
                isValid = true;
                break;

        }
        if (!isValid)
            SnackbarUtils.show(findViewById(android.R.id.content), getString(R.string.mandatory_field_error));
        return isValid;
    }

    @Override
    public void updateProgressBar() {
        int count = mViewPager.getCurrentItem() + 1;
        mAuditProgressBar.setProgress(count);
        mAuditProgressStatus.setText(count +
                "/" + mTaskFieldsList.size());
    }

    @Override
    public void onRegionClicked() {
        Intent intent = new Intent(this, SelectListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Utils.IS_REGION, true);
        bundle.putString(Utils.LIST_TYPE, "region");
        if (mSelectedList != null && mSelectedList.size() > 0) {
            bundle.putParcelableArrayList(Utils.SELECTED_LIST, mSelectedList);
        }
        intent.putExtras(bundle);
        startActivityForResult(intent, 134);
    }

    @Override
    public void onClusterClicked() {
        Intent intent = new Intent(this, SelectListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Utils.IS_CLUSTER, true);
        bundle.putString(Utils.LIST_TYPE, "cluster");
        if (mSelectedList != null && mSelectedList.size() > 0) {
            bundle.putParcelableArrayList(Utils.SELECTED_LIST, mSelectedList);
        }
        intent.putExtras(bundle);
        startActivityForResult(intent, 1334);
    }

    @Override
    public void onCityClicked() {
        Intent intent = new Intent(this, SelectListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Utils.IS_CITY, true);
        bundle.putBoolean(Utils.IS_SEARCH_ENABLED, true);
        bundle.putString(Utils.LIST_TYPE, "city");
        if (mSelectedList != null && mSelectedList.size() > 0) {
            bundle.putParcelableArrayList(Utils.SELECTED_LIST, mSelectedList);
        }
        intent.putExtras(bundle);
        startActivityForResult(intent, 135);
    }

    @Override
    public void onPropertyClicked() {
        Intent intent = new Intent(this, SelectListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Utils.IS_PROPERTY, true);
        bundle.putBoolean(Utils.IS_SEARCH_ENABLED, true);
        bundle.putString(Utils.LIST_TYPE, "property");
        if (mSelectedList != null && mSelectedList.size() > 0) {
            bundle.putParcelableArrayList(Utils.SELECTED_LIST, mSelectedList);
        }
        intent.putExtras(bundle);
        startActivityForResult(intent, 136);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }
}
