package com.treebo.prowlapp.flowportfolionew.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.View;
import android.view.WindowManager;

import com.google.android.material.snackbar.Snackbar;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowdashboard.activity.DashboardActivity;
import com.treebo.prowlapp.flowportfolionew.DaggerPortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.PortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.adapter.PendingTasksAdapter;
import com.treebo.prowlapp.flowportfolionew.presenter.PendingTasksListPresenter;
import com.treebo.prowlapp.flowportfolionew.usecase.PendingTaskListUseCase;
import com.treebo.prowlapp.response.portfolio.PendingTasksListResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by abhisheknair on 29/04/17.
 */

public class PendingTasksListActivity extends AppCompatActivity
        implements IPortfolioContract.IPendingTaskListView, PendingTasksAdapter.taskClickListener, SwipeRefreshLayout.OnRefreshListener {


    @Inject
    public PendingTasksListPresenter mPresenter;

    @Inject
    public PendingTaskListUseCase mUseCase;

    @Inject
    public LoginSharedPrefManager mSharedPrefs;

    private ArrayList<PendingTasksListResponse.PendingTaskObject> mList;
    private RecyclerView mContactInfoRecyclerView;
    private PendingTasksAdapter mAdapter;

    private View mLoadingView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.activity_gray_background));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        PortfolioComponentNew portfolioComponent = DaggerPortfolioComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        portfolioComponent.injectPendingTasksListActivity(this);

        mLoadingView = findViewById(R.id.loader_layout);

        TreeboTextView toolbarTitle = (TreeboTextView) findViewById(R.id.toolbar_title);
        toolbarTitle.setText(getString(R.string.task_completion_report));

        View backBtn = findViewById(R.id.history_back_iv);
        backBtn.setOnClickListener(view -> onBackPressed());

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mContactInfoRecyclerView = (RecyclerView) findViewById(R.id.task_history_rv);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mContactInfoRecyclerView.setLayoutManager(layoutManager1);
        mContactInfoRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new PendingTasksAdapter(mList, this, this);
        mContactInfoRecyclerView.setAdapter(mAdapter);

        mPresenter.fetchPendingTaskList();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down);
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mUseCase.setUserID(mSharedPrefs.getUserId());
        mPresenter.setPendingTaskListUseCase(mUseCase);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void setUpData(PendingTasksListResponse response) {
        mList = response.getPendingTaskList();
        mAdapter.refreshList(response.getPendingTaskList());
    }

    @Override
    public void onTaskCardClicked(PendingTasksListResponse.PendingTaskObject object) {
        // Open Property DashBoard
        if (object.getHotelID() != 0) {
            Intent intent = new Intent(this, DashboardActivity.class);
            intent.putExtra(Utils.HOTEL_NAME, object.getHotelName());
            intent.putExtra(Utils.HOTEL_ID, object.getHotelID());
            intent.putExtra(Utils.IS_IN_HOTEL, false);
            intent.putExtra(Utils.IS_FROM_EXPLORE, true);
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        if (Utils.isInternetConnected(this)) {
            mPresenter.fetchPendingTaskList();
        } else {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
        }
        mPresenter.fetchPendingTaskList();
    }
}
