package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.presenter.TaskDetailsPresenter;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class DeleteTaskObserver extends BaseObserver<BaseResponse> {

    private TaskDetailsPresenter mPresenter;

    public DeleteTaskObserver(TaskDetailsPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(BaseResponse baseResponse) {
        if (baseResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onSuccessfulDelete();
        } else {
            mPresenter.onDeleteFailure(baseResponse.msg);
        }
    }
}
