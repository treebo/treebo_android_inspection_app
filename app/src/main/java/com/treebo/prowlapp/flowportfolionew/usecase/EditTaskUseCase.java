package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.taskcreation.TaskCreationResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class EditTaskUseCase extends UseCase<TaskCreationResponse> {

    private RestClient mRestClient;
    int mUserID;
    CreatedTaskModel taskModel;

    public EditTaskUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setFields(int userID, CreatedTaskModel task) {
        this.mUserID = userID;
        this.taskModel = task;
    }

    @Override
    protected Observable<TaskCreationResponse> getObservable() {
        return mRestClient.editCreatedTask(mUserID, taskModel);
    }
}
