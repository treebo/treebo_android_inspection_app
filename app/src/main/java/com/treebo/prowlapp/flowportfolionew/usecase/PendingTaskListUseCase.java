package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.portfolio.PendingTasksListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 29/04/17.
 */

public class PendingTaskListUseCase extends UseCase<PendingTasksListResponse> {

    RestClient mRestClient;

    int mUserID;

    public PendingTaskListUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setUserID(int userID) {
        this.mUserID = userID;
    }

    @Override
    protected Observable<PendingTasksListResponse> getObservable() {
        return mRestClient.getPendingTasksInfo(mUserID);
    }
}
