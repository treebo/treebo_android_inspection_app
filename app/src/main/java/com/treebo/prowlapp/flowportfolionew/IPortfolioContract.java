package com.treebo.prowlapp.flowportfolionew;

import com.treebo.prowlapp.Models.portfoliomodel.PortfolioHotelV3;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioMetrics;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.portfolio.PendingTasksListResponse;
import com.treebo.prowlapp.response.portfolio.PortfolioHotelListResponseV3;
import com.treebo.prowlapp.response.portfolio.PortfolioMetricsResponse;
import com.treebo.prowlapp.response.taskcreation.TaskListResponse;

/**
 * Created by abhisheknair on 23/11/16.
 */

public interface IPortfolioContract {

    interface IPortfolioView extends BaseView {
        void setUpRecyclerView();

        void setUpScoresData(PortfolioMetrics metrics);

        void onLoadHotelListSuccess(PortfolioHotelListResponseV3 response);

        void onLoadHotelListFailure(String msg);

        void onLoadHotelListComplete();

        void onLoadMetricSuccess(PortfolioMetricsResponse response);

        void onLoadMetricFailure(String msg);

        void arrangeHotelsInSections(boolean isHotelInCurrentLocation);

        void setMetricsForSingleHotel(PortfolioHotelV3 hotel);
    }

    interface IPortfolioPresenter extends BasePresenter {
        void onCreate();

        void loadHotelList(int userID);

        void loadMetrics(int userID);

        void loadSingleHotelDetails(int userID, int hotelID);

        void updateHotelList(boolean isHotelInCurrentLocation);

        void setScores(PortfolioMetrics metrics);

        void updateSingleHotelDetails(PortfolioHotelV3 hotel);
    }

    interface ISettingsView extends BaseView {
        void onLogoutSuccess();

        void onLogoutFailure(String msg);
    }

    interface ISettingsPresenter extends BasePresenter {
        void logout(String accessToken);
    }

    interface IPendingTaskListView extends BaseView {
        void setUpData(PendingTasksListResponse response);
    }

    interface IPendingTaskListPresenter extends BasePresenter {
        void fetchPendingTaskList();

        void fetchSuccess(PendingTasksListResponse response);

        void fetchFailure(String message);
    }

    interface ITaskCreationView extends BaseView {
        void loadViewPager();

        void updateProgressBar();

        void saveGeographicalData(GeographicalDataResponse regions);

        void submitTask();

        void onSubmitTaskSuccess(CreatedTaskModel createdTask);
    }

    interface ITaskCreationPresenter extends BasePresenter {
        void onCreate();

        void fetchGeographicalData();

        void setFetchedData(GeographicalDataResponse regions);

        void dataFetchError(String msg);

        void postNewTask(int userID, CreatedTaskModel task);

        void onTaskCreationSuccess(CreatedTaskModel response);

        void onTaskCreationFailure(String msg);
    }

    interface ITaskListView extends BaseView {

        void setTaskListAdapter(TaskListResponse.Tasks tasks);
    }

    interface ITaskListPresenter extends BasePresenter {

        void fetchTaskList(int userID);

        void onTaskListFetchFailure(String errorMsg);

        void onTaskListFetchSuccess(TaskListResponse response);
    }

    interface ITaskDetailsView extends BaseView {

        void changeLayoutMode(boolean isEdit);

        void updateDetailsAfterEdit(CreatedTaskModel taskModel);

        void showSuccessScreen(boolean isDelete);
    }

    interface ITaskDetailsPresenter extends BasePresenter {

        void editTaskBtnClick();

        void postEditedTask(int userID, CreatedTaskModel task);

        void deleteTask(int userID, int taskID);

        void cancelEditBtnClick();

        void onSuccessfulEdit(CreatedTaskModel editTask);

        void onEditFailure(String errorMsg);

        void onSuccessfulDelete();

        void onDeleteFailure(String errorMsg);
    }
}
