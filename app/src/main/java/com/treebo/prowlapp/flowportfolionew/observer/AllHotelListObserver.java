package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.usecase.AllHotelListUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.portfolio.PortfolioHotelListResponseV3;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 24/11/16.
 */

public class AllHotelListObserver extends BaseObserver<PortfolioHotelListResponseV3> {

    private IPortfolioContract.IPortfolioView mView;
    private AllHotelListUseCase mUseCase;

    public AllHotelListObserver(BasePresenter presenter, IPortfolioContract.IPortfolioView view, AllHotelListUseCase useCase) {
        super(presenter, useCase);
        mView = view;
        mUseCase = useCase;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
        mView.onLoadHotelListComplete();
    }

    @Override
    public void onNext(PortfolioHotelListResponseV3 response) {
        mView.hideLoading();
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mView.onLoadHotelListSuccess(response);
        } else {
            mView.onLoadHotelListFailure("Loading hotel list failed");
        }
    }
}
