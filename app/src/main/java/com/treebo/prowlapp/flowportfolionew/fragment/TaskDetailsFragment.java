package com.treebo.prowlapp.flowportfolionew.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.views.TreeboEditText;

import androidx.fragment.app.Fragment;

/**
 * Created by abhisheknair on 21/06/17.
 */

public class TaskDetailsFragment extends Fragment {

    private TreeboEditText mTaskDetailsEt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTaskDetailsEt = (TreeboEditText) view.findViewById(R.id.task_details_edittext);
    }

    public String getTaskDetails() {
        return mTaskDetailsEt == null ? "" : mTaskDetailsEt.getText().toString();
    }
}
