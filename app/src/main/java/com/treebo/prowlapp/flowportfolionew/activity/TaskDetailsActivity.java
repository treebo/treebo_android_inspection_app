package com.treebo.prowlapp.flowportfolionew.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ScrollView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.CreatedTaskUpdated;
import com.treebo.prowlapp.events.TaskAreaSelectedEvent;
import com.treebo.prowlapp.flowportfolionew.DaggerPortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.PortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.adapter.TaskStatusAdapter;
import com.treebo.prowlapp.flowportfolionew.presenter.TaskDetailsPresenter;
import com.treebo.prowlapp.flowportfolionew.usecase.DeleteTaskUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.EditTaskUseCase;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.Models.taskcreationmodel.TextSubtextModel;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import fr.ganfra.materialspinner.MaterialSpinner;
import rx.subscriptions.CompositeSubscription;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class TaskDetailsActivity extends AppCompatActivity
        implements IPortfolioContract.ITaskDetailsView,
        TaskStatusAdapter.onTaskCheckChangeClickListener,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, TextWatcher {

    @Inject
    protected TaskDetailsPresenter mPresenter;

    @Inject
    protected RxBus mRxBus;

    @Inject
    protected DeleteTaskUseCase mDeleteUseCase;

    @Inject
    protected EditTaskUseCase mEditUseCase;

    @Inject
    protected LoginSharedPrefManager mSharedPrefs;

    private CreatedTaskModel mTask;

    private boolean isInEditMode;

    private TreeboTextView mTaskNameText;
    private TreeboTextView mTaskDetailsText;
    private TreeboTextView mTaskLinkText;
    private TreeboTextView mTaskTooltipText;
    private TreeboTextView mTaskStartDateHeaderText;
    private TreeboTextView mTasksEndDateHeaderText;
    private TreeboTextView mTaskRecurrenceHeaderText;
    private TreeboTextView mTaskGeofenceHeaderText;

    private TreeboEditText mTaskNameEditText;
    private TreeboEditText mTaskDetailsEditText;
    private TreeboEditText mTaskTooltipEditText;
    private TreeboEditText mTaskLinkEditText;
    private TreeboTextView mStartDateTv;
    private TreeboTextView mEndDateTv;
    private TreeboTextView mRecurrenceTv;
    private TreeboTextView mGeofenceTv;

    private TreeboTextView mTaskStatusHeaderText;
    private RecyclerView mTaskStatusRecyclerView;

    private TaskStatusAdapter mAdapter;

    private TreeboButton mDeleteBtn;
    private TreeboButton mEditBtn;

    private View mLoadingView;
    private View mSuccessView;
    private TreeboTextView mSuccessTv;

    private boolean isStartDatePickerOpen;
    private boolean isEndDatePickerOpen;
    private String datePickerString;
    private ScrollView mScrollView;

    private boolean isRecurrenceDatePickerOpen;

    private View mAddPropertyRl;

    MaterialSpinner maxOccurrenceSpinner;
    TreeboTextView setEndDateTv;

    private CompositeSubscription mSubscription = new CompositeSubscription();
    private String dateTimeString;
    private CreatedTaskModel.Recurrence mRecurrenceObject = new CreatedTaskModel.Recurrence();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        setStatusBarColor(R.color.white);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        PortfolioComponentNew portfolioComponent = DaggerPortfolioComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        portfolioComponent.injectTaskDetailsActivity(this);


        if (savedInstanceState == null) {
            Intent intent = getIntent();
            if (intent != null && intent.getExtras() != null) {
                mTask = intent.getExtras().getParcelable(Utils.CREATED_TASK);
            }
        } else {
            mTask = savedInstanceState.getParcelable(Utils.CREATED_TASK);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });

        mLoadingView = findViewById(R.id.layout_progress);
        mSuccessView = findViewById(R.id.audit_success_layout);
        mSuccessTv = (TreeboTextView) findViewById(R.id.audit_successfully_submitted);

        mScrollView = (ScrollView) findViewById(R.id.scrollview);

        mTaskNameEditText = (TreeboEditText) findViewById(R.id.task_name_tv);
        mTaskDetailsEditText = (TreeboEditText) findViewById(R.id.task_details_tv);
        mTaskTooltipEditText = (TreeboEditText) findViewById(R.id.task_tooltip_tv);
        mTaskLinkEditText = (TreeboEditText) findViewById(R.id.task_link_tv);

        mTaskNameText = (TreeboTextView) findViewById(R.id.task_name_header);
        mTaskDetailsText = (TreeboTextView) findViewById(R.id.task_details_header);
        mTaskLinkText = (TreeboTextView) findViewById(R.id.task_link_header);
        mTaskTooltipText = (TreeboTextView) findViewById(R.id.task_tooltip_header);
        mTaskStartDateHeaderText = (TreeboTextView) findViewById(R.id.task_start_date_text);
        mTaskStartDateHeaderText.setOnClickListener(view -> {
            if (isInEditMode && !mTask.getSectionHeader().equals(getString(R.string.started_tasks))) {
                isStartDatePickerOpen = true;
                openDatePicker();
            }
        });
        mTasksEndDateHeaderText = (TreeboTextView) findViewById(R.id.task_end_date_text);
        mTasksEndDateHeaderText.setOnClickListener(view -> {
            if (isInEditMode && !mTask.getSectionHeader().equals(getString(R.string.started_tasks))) {
                isEndDatePickerOpen = true;
                openDatePicker();
            }
        });
        mTaskRecurrenceHeaderText = (TreeboTextView) findViewById(R.id.task_recurrence_text);
        mTaskRecurrenceHeaderText.setOnClickListener(view -> {
            if (isInEditMode) {
                showTaskRecurrenceDialog();
            }
        });
        mTaskGeofenceHeaderText = (TreeboTextView) findViewById(R.id.task_geofence_text);

        mStartDateTv = (TreeboTextView) findViewById(R.id.task_start_date_tv);
        mStartDateTv.setOnClickListener(view -> {
            if (isInEditMode && !mTask.getSectionHeader().equals(getString(R.string.started_tasks))) {
                isStartDatePickerOpen = true;
                openDatePicker();
            }
        });
        mEndDateTv = (TreeboTextView) findViewById(R.id.task_end_date_tv);
        mEndDateTv.setOnClickListener(view -> {
            if (isInEditMode && !mTask.getSectionHeader().equals(getString(R.string.started_tasks))) {
                isEndDatePickerOpen = true;
                openDatePicker();
            }
        });
        mRecurrenceTv = (TreeboTextView) findViewById(R.id.task_recurrence_tv);
        mRecurrenceTv.setOnClickListener(view -> {
            if (isInEditMode) {
                showTaskRecurrenceDialog();
            }
        });
        mGeofenceTv = (TreeboTextView) findViewById(R.id.task_geofence_tv);

        View startIcon = findViewById(R.id.ic_start_date);
        startIcon.setOnClickListener(view -> {
            if (isInEditMode && !mTask.getSectionHeader().equals(getString(R.string.started_tasks))) {
                isStartDatePickerOpen = true;
                openDatePicker();
            }
        });
        View endIcon = findViewById(R.id.ic_end_date);
        endIcon.setOnClickListener(view -> {
            if (isInEditMode && !mTask.getSectionHeader().equals(getString(R.string.started_tasks))) {
                isEndDatePickerOpen = true;
                openDatePicker();
            }
        });

        mTaskStatusHeaderText = (TreeboTextView) findViewById(R.id.task_status_header);
        mTaskStatusRecyclerView = (RecyclerView) findViewById(R.id.task_log_recyclerview);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mTaskStatusRecyclerView.setLayoutManager(layoutManager1);
        mTaskStatusRecyclerView.setItemAnimator(new DefaultItemAnimator());

        TreeboEditText mHotelSearchTv = (TreeboEditText) findViewById(R.id.hotel_search_et);
        mHotelSearchTv.addTextChangedListener(this);

        mAddPropertyRl = findViewById(R.id.add_property_view);
        View addPropertyBtn = mAddPropertyRl.findViewById(R.id.add_property_btn);
        addPropertyBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, SelectListActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean(Utils.IS_PROPERTY, true);
            bundle.putBoolean(Utils.IS_SEARCH_ENABLED, true);
            bundle.putString(Utils.LIST_TYPE, "property");
            intent.putExtras(bundle);
            startActivity(intent);
        });

        mDeleteBtn = (TreeboButton) findViewById(R.id.delete_btn);
        mEditBtn = (TreeboButton) findViewById(R.id.edit_btn);

        mDeleteBtn.setOnClickListener(view -> {
            if (mDeleteBtn.getText().toString().toLowerCase()
                    .equals(getString(R.string.delete).toLowerCase()))
                showAreYouSureDialog();
            else
                mPresenter.cancelEditBtnClick();
        });

        mEditBtn.setOnClickListener(view -> {
            if (mEditBtn.getText().toString().toLowerCase()
                    .equals(getString(R.string.edit).toLowerCase())) {
                mPresenter.editTaskBtnClick();
            } else
                saveFieldsAndPost();
        });


        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof TaskAreaSelectedEvent) {
                ArrayList<TextSubtextModel> selectedList = ((TaskAreaSelectedEvent) event).getList();
                int[] idList = new int[selectedList.size()];
                for (int i = 0; i < idList.length; i++) {
                    idList[i] = selectedList.get(i).getId();
                }
                mTask.setHotelIDList(idList);
            }
        }));

        fillFieldsWithData();
    }

    @Override
    public void onBackPressed() {
        if (isInEditMode) {
            changeLayoutMode(false);
        } else {
            super.onBackPressed();
        }
    }

    private void showAreYouSureDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_update_back, null);

        TreeboTextView title = (TreeboTextView) dialogView.findViewById(R.id.stop_update_text);
        title.setText(getString(R.string.delete_confirmation));

        dialogBuilder.setView(dialogView);
        final AlertDialog stopUpdateDialog = dialogBuilder.create();
        stopUpdateDialog.show();

        TreeboTextView cancelBtn = (TreeboTextView) dialogView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(view -> stopUpdateDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            stopUpdateDialog.dismiss();
            mPresenter.deleteTask(mSharedPrefs.getUserId(), mTask.getTaskID());
        });
    }

    private void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

    private void saveFieldsAndPost() {
        mTask.setTaskName(mTaskNameEditText.getText().toString());
        mTask.setTaskDetails(mTaskDetailsEditText.getText().toString());
        mTask.setTooltipText(mTaskTooltipEditText.getText().toString());
        mTask.setContentLink(mTaskLinkEditText.getText().toString());
        mTaskNameEditText.clearFocus();
        mTaskDetailsEditText.clearFocus();
        mTaskLinkEditText.clearFocus();
        mTaskTooltipEditText.clearFocus();
        if (mTask.getRecurrence() != null && !TextUtils.isEmpty(mTask.getRecurrence().getEndDate())) {
            mTask.getRecurrence().setMaxOccurrences(null);
        }
        Utils.hideSoftKeyboard(findViewById(android.R.id.content));
        mPresenter.postEditedTask(mSharedPrefs.getUserId(), mTask);
    }

    private void fillFieldsWithData() {
        mTaskNameEditText.setText(mTask.getTaskName());
        mTaskNameEditText.setFocusable(false);

        mTaskDetailsEditText.setText(mTask.getTaskDetails());
        mTaskDetailsEditText.setFocusable(false);

        mTaskTooltipEditText.setText(mTask.getTooltipText());
        mTaskTooltipEditText.setFocusable(false);

        mTaskLinkEditText.setText(mTask.getContentLink());
        mTaskLinkEditText.setFocusable(false);

        mStartDateTv.setText(DateUtils.getDisplayTimeFromServerTime(mTask.getAppearDate()));
        mEndDateTv.setText(DateUtils.getDisplayTimeFromServerTime(mTask.getCompletionDate()));

        if (DateUtils.isTimeExpired(mTask.getCompletionDate())
                || mTask.getStartedAndCompletedTaskLogCount() > 0) {
            View deleteEditLayout = findViewById(R.id.delete_edit_layout);
            deleteEditLayout.setVisibility(GONE);
        } else if (mTask.getSectionHeader().equals(getString(R.string.started_tasks))) {
            mDeleteBtn.setVisibility(GONE);
        }

        mGeofenceTv.setText(mTask.isGeofenceEnabled() ? "Yes" : "No");
        mRecurrenceTv.setText(mTask.isRecurrenceEnabled() ? "Yes" : "No");

        int completeCount = 0;
        for (CreatedTaskModel.TaskLog taskLog : mTask.getTaskLogs()) {
            if (taskLog.getTaskStatus().toLowerCase().equals(getString(R.string.complete).toLowerCase()))
                completeCount++;
        }
        mTaskStatusHeaderText.setText(getString(R.string.task_log_status, mTask.getSectionHeader(),
                completeCount, mTask.getTaskLogs().size()));
        mAdapter = new TaskStatusAdapter(this, mTask.getTaskLogs(), this);
        mTaskStatusRecyclerView.setAdapter(mAdapter);
        mScrollView.smoothScrollBy(0, 0);
    }

    private void openDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH),
                getString(R.string.cancel)
        );
        dpd.setMinDate(now);
        dpd.setAccentColor(ContextCompat.getColor(this, R.color.emerald));
        dpd.show(getFragmentManager(), "Datepickerdialog");
        dpd.setOnCancelListener(dialog -> {
            isStartDatePickerOpen = false;
            isEndDatePickerOpen = false;
        });
    }

    private void openTimePicker() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setAccentColor(ContextCompat.getColor(this, R.color.emerald));
        tpd.show(getFragmentManager(), "Timepickerdialog");
        tpd.setOnCancelListener(dialog -> {
            isStartDatePickerOpen = false;
            isEndDatePickerOpen = false;
        });
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setDeleteUseCase(mDeleteUseCase);
        mPresenter.setEditUseCase(mEditUseCase);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void changeLayoutMode(boolean isEdit) {
        isInEditMode = isEdit;

        mTaskLinkEditText.setFocusableInTouchMode(isEdit);
        mTaskTooltipEditText.setFocusableInTouchMode(isEdit);
        mTaskDetailsEditText.setFocusableInTouchMode(isEdit);

        if (mTask.getSectionHeader().equals(getString(R.string.yet_to_start_tasks))) {
            mTaskNameEditText.setFocusableInTouchMode(isEdit);
            mTaskNameText.setTextColor(ContextCompat.getColor(this,
                    isEdit ? R.color.green_color_primary : R.color.text_color_secondary));
            mTaskStartDateHeaderText.setTextColor(ContextCompat.getColor(this,
                    isEdit ? R.color.green_color_primary : R.color.text_color_secondary));
            mTasksEndDateHeaderText.setTextColor(ContextCompat.getColor(this,
                    isEdit ? R.color.green_color_primary : R.color.text_color_secondary));

            mAdapter.setRecyclerViewMode(isEdit);
            mAdapter.notifyDataSetChanged();

            mAddPropertyRl.setVisibility(isEdit ? VISIBLE : GONE);
        }
        mTaskDetailsText.setTextColor(ContextCompat.getColor(this,
                isEdit ? R.color.green_color_primary : R.color.text_color_secondary));
        mTaskLinkText.setTextColor(ContextCompat.getColor(this,
                isEdit ? R.color.green_color_primary : R.color.text_color_secondary));
        mTaskTooltipText.setTextColor(ContextCompat.getColor(this,
                isEdit ? R.color.green_color_primary : R.color.text_color_secondary));
        mTaskGeofenceHeaderText.setTextColor(ContextCompat.getColor(this,
                isEdit ? R.color.green_color_primary : R.color.text_color_secondary));
        mTaskRecurrenceHeaderText.setTextColor(ContextCompat.getColor(this,
                isEdit ? R.color.green_color_primary : R.color.text_color_secondary));

        mDeleteBtn.setText(isEdit ? getString(R.string.cancel) : getString(R.string.delete));
        mEditBtn.setText(isEdit ? getString(R.string.save) : getString(R.string.edit));
        if (isEdit) {
            mDeleteBtn.setVisibility(VISIBLE);
        } else if (mTask.getSectionHeader().equals(getString(R.string.started_tasks))) {
            mDeleteBtn.setVisibility(GONE);
        }

    }

    @Override
    public void updateDetailsAfterEdit(CreatedTaskModel taskModel) {
        taskModel.setSectionHeader(mTask.getSectionHeader());
        mTask = taskModel;
        fillFieldsWithData();
    }

    @Override
    public void showSuccessScreen(boolean isDelete) {
        mRxBus.postEvent(new CreatedTaskUpdated(isDelete, mTask));
        setStatusBarColor(R.color.green_color_primary);
        if (isDelete) {
            mSuccessView.setVisibility(VISIBLE);
            mSuccessTv.setText(getString(R.string.task_successfully_deleted));
            new Handler().postDelayed(this::finish, 1000);
        } else {
            mSuccessView.setVisibility(VISIBLE);
            mSuccessTv.setText(getString(R.string.task_successfully_edited));
            new Handler().postDelayed(() -> {
                setStatusBarColor(R.color.white);
                mSuccessView.setVisibility(View.GONE);
            }, 1000);
        }
    }

    @Override
    public void taskCheckChanged(CreatedTaskModel.TaskLog taskLog, boolean isChecked) {
        for (CreatedTaskModel.TaskLog task : mTask.getTaskLogs()) {
            if (task.getId() == taskLog.getId()) {
                task.setSelected(isChecked);
            }
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (!isRecurrenceDatePickerOpen) {
            datePickerString = DateUtils.getDisplayDateFromDatePicker(year, monthOfYear, dayOfMonth);
            dateTimeString = year + "-" + monthOfYear + "-" + dayOfMonth;
            openTimePicker();
        } else {
            monthOfYear += 1;
            mRecurrenceObject.setEndDate(year + "-" + monthOfYear + "-" + dayOfMonth);
            openTimePicker();
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        if (!isRecurrenceDatePickerOpen) {
            datePickerString += DateUtils.getDisplayTimeFromTimePicker(hourOfDay, minute);
            dateTimeString += " " + hourOfDay + ":" + minute + ":00";
            if (isStartDatePickerOpen) {
                mStartDateTv.setText(datePickerString);
                mTask.setAppearDate(dateTimeString);
            } else if (isEndDatePickerOpen) {
                mEndDateTv.setText(datePickerString);
                mTask.setCompletionDate(dateTimeString);
            }
            datePickerString = "";
            dateTimeString = "";
            isStartDatePickerOpen = false;
            isEndDatePickerOpen = false;
        } else {
            String date = mRecurrenceObject.getEndDate();
            date += " " + hourOfDay + ":" + minute + ":00";
            mRecurrenceObject.setEndDate(date);
            mRecurrenceObject.setMaxOccurrences(null);
            setEndDateTv.setText(DateUtils.getDisplayTimeFromServerTime(date));
            maxOccurrenceSpinner.setEnabled(false);
            maxOccurrenceSpinner.setClickable(false);
            isRecurrenceDatePickerOpen = false;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        mAdapter.filter(s.toString());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }

    private void showTaskRecurrenceDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_task_recurrence, null);
        setEndDateTv = (TreeboTextView) dialogView.findViewById(R.id.recurrence_end_date_tv);
        setEndDateTv.setOnClickListener(view ->
                openDatePicker());
        MaterialSpinner repeatFrequency = (MaterialSpinner) dialogView.findViewById(R.id.frequency_spinner);
        String numberList[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        ArrayAdapter<String> repeatAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, numberList);
        repeatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        repeatFrequency.setAdapter(repeatAdapter);
        repeatFrequency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1)
                    mRecurrenceObject.setSeparationCount(Integer.valueOf(repeatAdapter.getItem(position)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        maxOccurrenceSpinner = (MaterialSpinner) dialogView.findViewById(R.id.max_occurence_spinner);
        ArrayAdapter<String> maxOccuranceAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, numberList);
        maxOccuranceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        maxOccurrenceSpinner.setAdapter(maxOccuranceAdapter);
        maxOccurrenceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    mRecurrenceObject.setMaxOccurrences(Integer.valueOf(maxOccuranceAdapter.getItem(position)));
                    setEndDateTv.setText(getString(R.string.task_end_date_field));
                    mRecurrenceObject.setEndDate(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialogBuilder.setView(dialogView);
        final AlertDialog pauseAuditDialog = dialogBuilder.create();
        pauseAuditDialog.show();
        isRecurrenceDatePickerOpen = true;
        if (mTask != null && mTask.getRecurrence() != null
                && mTask.getRecurrence().getSeparationCount() > 0) {
            repeatFrequency.setSelection(mTask.getRecurrence().getSeparationCount() - 1);
        }
        if (mTask != null && mTask.getRecurrence() != null
                && mTask.getRecurrence().getMaxOccurrences() > 0) {
            maxOccurrenceSpinner.setSelection(mTask.getRecurrence().getMaxOccurrences() - 1);
        }
        if (mTask != null && mTask.getRecurrence() != null
                && !TextUtils.isEmpty(mTask.getRecurrence().getEndDate())) {
            setEndDateTv.setText(mTask.getRecurrence().getEndDate());
        }

        TreeboButton doneBtn = (TreeboButton) dialogView.findViewById(R.id.done_btn);
        doneBtn.setOnClickListener(view -> {
            if (mRecurrenceObject.getSeparationCount() == 0) {
                repeatFrequency.setError(getString(R.string.mandatory_field_error));
            } else if (mRecurrenceObject.getMaxOccurrences() == 0
                    && TextUtils.isEmpty(mRecurrenceObject.getEndDate())) {
                maxOccurrenceSpinner.setError(getString(R.string.recurrence_occurrence_error));
            } else {
                mTask.setRecurrence(mRecurrenceObject);
                pauseAuditDialog.dismiss();
            }
        });

    }
}
