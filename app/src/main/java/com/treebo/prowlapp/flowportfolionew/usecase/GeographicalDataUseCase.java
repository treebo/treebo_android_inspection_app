package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 23/06/17.
 */

public class GeographicalDataUseCase extends UseCase<GeographicalDataResponse> {

    RestClient mRestClient;

    public GeographicalDataUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    @Override
    protected Observable<GeographicalDataResponse> getObservable() {
        return mRestClient.getGeographicalData();
    }
}
