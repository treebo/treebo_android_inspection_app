package com.treebo.prowlapp.flowportfolionew.fragment;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.Fragment;

/**
 * Created by abhisheknair on 05/07/17.
 */

public class TaskGeofenceFragment extends Fragment {

    private AppCompatCheckBox mGeofenceCheckbox;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_geofence, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View yesTv = view.findViewById(R.id.yes_tv);
        yesTv.setOnClickListener(view1 ->
                mGeofenceCheckbox.setChecked(!mGeofenceCheckbox.isChecked()));
        mGeofenceCheckbox = (AppCompatCheckBox) view.findViewById(R.id.yes_checkbox);
    }

    public boolean isGeofenceEnabled() {
        return mGeofenceCheckbox != null && mGeofenceCheckbox.isChecked();
    }
}
