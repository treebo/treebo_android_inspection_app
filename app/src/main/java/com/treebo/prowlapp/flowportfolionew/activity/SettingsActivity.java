package com.treebo.prowlapp.flowportfolionew.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.PermissionsUpdateEvent;
import com.treebo.prowlapp.flowOnboarding.activity.OnBoardingActivity;
import com.treebo.prowlapp.flowportfolionew.DaggerPortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.PortfolioComponentNew;
import com.treebo.prowlapp.flowportfolionew.adapter.SettingsAdapter;
import com.treebo.prowlapp.flowportfolionew.presenter.SettingsPresenter;
import com.treebo.prowlapp.flowportfolionew.usecase.LogoutUseCase;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 29/11/16.
 */

public class SettingsActivity extends AppCompatActivity implements IPortfolioContract.ISettingsView,
        SettingsAdapter.onSettingsItemClickListener {

    @Inject
    public Navigator mNavigator;

    @Inject
    public SettingsPresenter mPresenter;

    @Inject
    public LogoutUseCase mLogoutUseCase;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    private RolesAndUserPermissions.PermissionList mUserPermissions;

    private final CompositeSubscription mSubscription = new CompositeSubscription();
    private View mSignOutBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.grey_toolbar));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        PortfolioComponentNew portfolioComponent = DaggerPortfolioComponentNew.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        portfolioComponent.injectSettingsActivity(this);
        mUserPermissions = mLoginManager.getUserPermissionList(0);

        View backBtn = findViewById(R.id.settings_back_iv);
        backBtn.setOnClickListener(view -> onBackPressed());
        TreeboTextView versionTxt = (TreeboTextView) findViewById(R.id.version_text);
        if (BuildConfig.DEBUG)
            versionTxt.setText(TextUtils.concat(getString(R.string.version_string, BuildConfig.VERSION_NAME), " - Dev"));
        else
            versionTxt.setText(getString(R.string.version_string, BuildConfig.VERSION_NAME));
        versionTxt.setVisibility(mUserPermissions.isSettingsVersionDetailsEnabled() ? View.VISIBLE
                : View.INVISIBLE);

        TreeboTextView userName = (TreeboTextView) findViewById(R.id.portfolio_user_name_tv);
        userName.setText(mLoginManager.getUsername());
        TreeboTextView userTitleTv = (TreeboTextView) findViewById(R.id.portfolio_header_tv);
        userTitleTv.setText(mLoginManager.getUserRolesString(0, true));

        ImageView profilePic = (ImageView) findViewById(R.id.ic_personal_image);
        Utils.loadRoundedImageUsingGlide(this, mLoginManager.getPrefUserProfilePic(),
                profilePic, R.drawable.ic_person_black);

        mSignOutBtn = findViewById(R.id.sign_out_btn);
        mSignOutBtn.setOnClickListener(view -> showSignOutDialog());


        String[] quotes = getResources().getStringArray(R.array.settings_quotes);
        String[] quoteSources = getResources().getStringArray(R.array.settings_quotes_sources);

        TreeboTextView quotesTv = (TreeboTextView) findViewById(R.id.quote_tv);
        TreeboTextView quoteSourceTv = (TreeboTextView) findViewById(R.id.quote_source_tv);
        int randomNumber = randInt(0, 5);
        quotesTv.setText("\"" + quotes[randomNumber] + "\"");
        quoteSourceTv.setText(quoteSources[randomNumber]);


        RecyclerView mSettingsRecyclerView = (RecyclerView) findViewById(R.id.settings_rv);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mSettingsRecyclerView.setLayoutManager(layoutManager1);
        mSettingsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        ArrayList<String> mOptionsList = setUpDisplayListAsPerUserPermissions();
        SettingsAdapter adapter = new SettingsAdapter(this, mOptionsList, this);
        mSettingsRecyclerView.setAdapter(adapter);

        mSubscription.add(RxUtils.build(RxBus.getInstance().toObservable()).subscribe(event -> {
            if (event instanceof PermissionsUpdateEvent) {
                ArrayList<String> list = setUpDisplayListAsPerUserPermissions();
                SettingsAdapter exploreAdapter = new SettingsAdapter(this, list, this);
                mSettingsRecyclerView.setAdapter(exploreAdapter);
            }
        }));
    }

    private ArrayList<String> setUpDisplayListAsPerUserPermissions() {
        String[] optionsArray = getResources().getStringArray(R.array.settings_options);
        ArrayList<String> displayList =
                new ArrayList<>(Arrays.asList(optionsArray));
        if (mUserPermissions != null) {
            if (!mUserPermissions.isSettingsTaskManagementEnabled()) {
                displayList.remove(optionsArray[0]);
            }
            if (!mUserPermissions.isSettingsContactInfoEnabled()) {
                displayList.remove(optionsArray[1]);
            }
            if (!mUserPermissions.isSettingsTaskCompletionEnabled()) {
                displayList.remove(optionsArray[2]);
            }
            if (!mUserPermissions.isSettingsFeedbackEnabled()) {
                displayList.remove(optionsArray[3]);
            }
            if (!mUserPermissions.isSettingsSignoutEnabled()) {
                mSignOutBtn.setVisibility(View.GONE);
            } else {
                mSignOutBtn.setVisibility(View.VISIBLE);
            }
        }
        return displayList;
    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
        mPresenter.setLogoutUseCase(mLogoutUseCase);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down);
    }

    @Override
    public void onItemClicked(String position) {
        switch (position) {
            case "Send feedback":
                mNavigator.navigateToFeedbackActivity(this);
                break;

            case "Contact directory":
                mNavigator.navigateToContactInfoActivity(this);
                break;

            case "Task completion report":
                mNavigator.navigateToPendingTasksListActivity(this);
                break;

            case "Task Management":
                mNavigator.navigateToCreatedTaskListActivity(this);
                break;
        }
    }

    private void showSignOutDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_update_back, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog stopUpdateDialog = dialogBuilder.create();
        stopUpdateDialog.show();

        TreeboTextView text = (TreeboTextView) dialogView.findViewById(R.id.stop_update_text);
        text.setText(getString(R.string.you_can_login_anytime));

        TreeboTextView cancelBtn = (TreeboTextView) dialogView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(view -> stopUpdateDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setText(getString(R.string.sign_out_label));
        okayBtn.setOnClickListener(view -> {
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SIGNOUT_CLICK, new Properties());
            if (Utils.isInternetConnected(this)) {
                mPresenter.logout(mLoginManager.getAccessToken());
                stopUpdateDialog.dismiss();
            } else
                Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
        });
    }

    private int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    @Override
    public void onLogoutSuccess() {
        Intent intent = new Intent(this, OnBoardingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        mLoginManager.clearUserData();
        finish();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down);
    }

    @Override
    public void onLogoutFailure(String msg) {
        Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }
}
