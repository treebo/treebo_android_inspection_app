package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.presenter.TaskCreationPresenter;
import com.treebo.prowlapp.response.taskcreation.TaskCreationResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 28/06/17.
 */

public class CreateTaskObserver extends BaseObserver<TaskCreationResponse> {

    private TaskCreationPresenter mPresenter;

    public CreateTaskObserver(TaskCreationPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(TaskCreationResponse taskCreationResponse) {
        if (taskCreationResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onTaskCreationSuccess(taskCreationResponse.getTask());
        } else {
            mPresenter.onTaskCreationFailure(taskCreationResponse.msg);
        }
    }
}
