package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.presenter.TaskDetailsPresenter;
import com.treebo.prowlapp.response.taskcreation.TaskCreationResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class EditTaskObserver extends BaseObserver<TaskCreationResponse> {

    private TaskDetailsPresenter mPresenter;

    public EditTaskObserver(TaskDetailsPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(TaskCreationResponse baseResponse) {
        if (baseResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onSuccessfulEdit(baseResponse.getTask());
        } else {
            mPresenter.onEditFailure(baseResponse.msg);
        }
    }
}
