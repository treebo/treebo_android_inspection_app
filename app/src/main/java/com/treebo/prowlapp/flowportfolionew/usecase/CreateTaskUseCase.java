package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.taskcreation.TaskCreationResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 28/06/17.
 */

public class CreateTaskUseCase extends UseCase<TaskCreationResponse> {

    private RestClient mRestClient;
    private CreatedTaskModel createdTask;
    private int mUserID;

    public CreateTaskUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setCreatedTask(int userID, CreatedTaskModel model) {
        this.createdTask = model;
        this.mUserID = userID;
    }


    @Override
    protected Observable<TaskCreationResponse> getObservable() {
        return mRestClient.createTask(mUserID, createdTask);
    }
}
