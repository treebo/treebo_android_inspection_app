package com.treebo.prowlapp.flowportfolionew.presenter;

import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.observer.DeleteTaskObserver;
import com.treebo.prowlapp.flowportfolionew.observer.EditTaskObserver;
import com.treebo.prowlapp.flowportfolionew.usecase.DeleteTaskUseCase;
import com.treebo.prowlapp.flowportfolionew.usecase.EditTaskUseCase;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class TaskDetailsPresenter implements IPortfolioContract.ITaskDetailsPresenter {

    private IPortfolioContract.ITaskDetailsView mView;
    private DeleteTaskUseCase mDeleteUseCase;
    private EditTaskUseCase mEditUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IPortfolioContract.ITaskDetailsView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {
        mView.setUpViewAsPerPermissions();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void editTaskBtnClick() {
        mView.changeLayoutMode(true);
    }

    @Override
    public void postEditedTask(int userID, CreatedTaskModel task) {
        mView.showLoading();
        mEditUseCase.setFields(userID, task); //Set fields
        mEditUseCase.execute(providesEditObserver());
    }

    @Override
    public void deleteTask(int userID, int taskID) {
        mView.showLoading();
        mDeleteUseCase.setField(userID, taskID);
        mDeleteUseCase.execute(providesDeleteObserver());
    }

    @Override
    public void cancelEditBtnClick() {
        mView.changeLayoutMode(false);
    }

    @Override
    public void onSuccessfulEdit(CreatedTaskModel task) {
        mView.hideLoading();
        mView.updateDetailsAfterEdit(task);
        mView.changeLayoutMode(false);
        mView.showSuccessScreen(false);
    }

    @Override
    public void onEditFailure(String errorMsg) {
        mView.hideLoading();
        mView.showError(errorMsg);
    }

    @Override
    public void onSuccessfulDelete() {
        mView.hideLoading();
        mView.showSuccessScreen(true);
    }

    @Override
    public void onDeleteFailure(String errorMsg) {
        mView.hideLoading();
        mView.showError(errorMsg);
    }

    public void setDeleteUseCase(DeleteTaskUseCase useCase) {
        this.mDeleteUseCase = useCase;
    }

    private DeleteTaskObserver providesDeleteObserver() {
        return new DeleteTaskObserver(this, mDeleteUseCase);
    }

    public void setEditUseCase(EditTaskUseCase useCase) {
        this.mEditUseCase = useCase;
    }

    private EditTaskObserver providesEditObserver() {
        return new EditTaskObserver(this, mEditUseCase);
    }
}
