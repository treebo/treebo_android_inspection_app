package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.presenter.PortfolioPresenterNew;
import com.treebo.prowlapp.response.portfolio.PortfolioSingleHotelResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 07/03/17.
 */

public class SingleHotelDetailsObserver extends BaseObserver<PortfolioSingleHotelResponse> {

    private PortfolioPresenterNew mPresenter;

    public SingleHotelDetailsObserver(PortfolioPresenterNew presenter, UseCase useCase) {
        super(presenter, useCase);
        mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(PortfolioSingleHotelResponse portfolioSingleHotelResponse) {
        if (portfolioSingleHotelResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.updateSingleHotelDetails(portfolioSingleHotelResponse.getHotelDetails());
        }
    }
}
