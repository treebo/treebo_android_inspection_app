package com.treebo.prowlapp.flowportfolionew.adapter;

import android.content.Context;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.taskcreationmodel.CreatedTaskModel;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class TaskStatusAdapter extends RecyclerView.Adapter<TaskStatusAdapter.TaskStatusViewHolder> {

    private ArrayList<CreatedTaskModel.TaskLog> mTaskList;
    private ArrayList<CreatedTaskModel.TaskLog> mDisplayList;
    private Context mContext;
    private boolean isInEditMode;
    private onTaskCheckChangeClickListener mListener;

    public TaskStatusAdapter(Context context, ArrayList<CreatedTaskModel.TaskLog> list,
                             onTaskCheckChangeClickListener listener) {
        this.mContext = context;
        this.mTaskList = list;
        this.mDisplayList = list;
        this.mListener = listener;
    }

    public void setRecyclerViewMode(boolean isEdit) {
        isInEditMode = isEdit;
    }

    public void filter(String text) {
        ArrayList<CreatedTaskModel.TaskLog> list = new ArrayList<>();
        if (TextUtils.isEmpty(text)) {
            list.addAll(mTaskList);
        } else {
            for (CreatedTaskModel.TaskLog taskLog : mTaskList) {
                if (taskLog.getHotelName().toLowerCase().contains(text.toLowerCase())) {
                    list.add(taskLog);
                }
            }
        }
        mDisplayList = list;
        notifyDataSetChanged();
    }

    @Override
    public TaskStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_status,
                parent, false);
        return new TaskStatusViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(TaskStatusViewHolder holder, int position) {
        CreatedTaskModel.TaskLog taskLog = mDisplayList.get(position);
        holder.taskLog = taskLog;
        holder.hotelName.setText(taskLog.getHotelName());
        holder.city.setText(taskLog.getCity());
        switch (taskLog.getTaskStatus().toLowerCase()) {
            case "complete":
                holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.green_color_primary));
                holder.status.setText(mContext.getString(R.string.completed));
                break;

            case "started":
                holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.warning_yellow));
                holder.status.setText(mContext.getString(R.string.in_progress));
                break;

            default:
                holder.status.setTextColor(ContextCompat.getColor(mContext, R.color.warning_red));
                holder.status.setText(mContext.getString(R.string.not_started));
                break;

        }
        if (isInEditMode) {
            holder.checkBox.setChecked(true);
            holder.checkBox.setVisibility(View.VISIBLE);
        } else {
            holder.checkBox.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mDisplayList != null ? mDisplayList.size() : 0;
    }

    public class TaskStatusViewHolder extends RecyclerView.ViewHolder {

        private CreatedTaskModel.TaskLog taskLog;
        private AppCompatCheckBox checkBox;
        private TreeboTextView hotelName;
        private TreeboTextView city;
        private TreeboTextView status;

        public TaskStatusViewHolder(View itemView, onTaskCheckChangeClickListener listener) {
            super(itemView);
            checkBox = (AppCompatCheckBox) itemView.findViewById(R.id.item_checkbox);
            hotelName = (TreeboTextView) itemView.findViewById(R.id.item_header_tv);
            city = (TreeboTextView) itemView.findViewById(R.id.item_subheader_tv);
            status = (TreeboTextView) itemView.findViewById(R.id.item_task_status);
            checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                listener.taskCheckChanged(taskLog, isChecked);
            });
        }
    }

    public interface onTaskCheckChangeClickListener {

        void taskCheckChanged(CreatedTaskModel.TaskLog taskLog, boolean isChecked);
    }
}
