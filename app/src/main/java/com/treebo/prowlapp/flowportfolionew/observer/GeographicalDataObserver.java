package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.presenter.TaskCreationPresenter;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 23/06/17.
 */

public class GeographicalDataObserver extends BaseObserver<GeographicalDataResponse> {

    private TaskCreationPresenter mPresenter;

    public GeographicalDataObserver(TaskCreationPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(GeographicalDataResponse geographicalDataResponse) {
        if (geographicalDataResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.setFetchedData(geographicalDataResponse);
        }
    }
}
