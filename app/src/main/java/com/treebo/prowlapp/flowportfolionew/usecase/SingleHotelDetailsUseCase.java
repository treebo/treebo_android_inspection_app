package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.portfolio.PortfolioSingleHotelResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 07/03/17.
 */

public class SingleHotelDetailsUseCase extends UseCase<PortfolioSingleHotelResponse> {

    private int userID;
    private int hotelID;
    private RestClient mRestClient;

    public SingleHotelDetailsUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setFields(int userID, int hotelID) {
        this.userID = userID;
        this.hotelID = hotelID;
    }

    @Override
    protected Observable<PortfolioSingleHotelResponse> getObservable() {
        return mRestClient.getSingleHotelDetails(userID, hotelID);
    }
}
