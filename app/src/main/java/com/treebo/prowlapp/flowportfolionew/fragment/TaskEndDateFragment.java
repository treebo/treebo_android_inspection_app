package com.treebo.prowlapp.flowportfolionew.fragment;


import android.content.Context;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.views.TreeboTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

/**
 * Created by abhisheknair on 28/06/17.
 */

public class TaskEndDateFragment extends Fragment implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener {

    public final static String TAG = "TaskStartDateFragment";

    private boolean isEndDateSet;
    private boolean isEndTimeSet;

    private TreeboTextView mEndDateTv;
    private TreeboTextView mEndTimeTv;
    private Context mContext;

    private String mCompletionDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_end_date, container, false);
        mContext = inflater.getContext();
        mEndDateTv = (TreeboTextView) layoutView.findViewById(R.id.task_end_date_tv);
        mEndDateTv.setOnClickListener(view -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH),
                    getString(R.string.cancel)
            );
            dpd.setMinDate(now);
            dpd.setAccentColor(ContextCompat.getColor(inflater.getContext(), R.color.emerald));
            mCompletionDate = "";
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        });
        mEndTimeTv = (TreeboTextView) layoutView.findViewById(R.id.task_end_time_tv);
        mEndTimeTv.setOnClickListener(view -> {
            Calendar now = Calendar.getInstance();
            TimePickerDialog dpd = TimePickerDialog.newInstance(
                    this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    false
            );
            dpd.setAccentColor(ContextCompat.getColor(inflater.getContext(), R.color.emerald));
            dpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
        });
        return layoutView;
    }

    public boolean isDateEntered() {
        return isEndDateSet && isEndTimeSet;
    }

    public String getTaskCompletionDate() {
        return mCompletionDate;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        isEndDateSet = true;
        monthOfYear += 1;
        mEndDateTv.setText(DateUtils.getDisplayDateFromDatePicker(year, monthOfYear, dayOfMonth));
        mEndDateTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        mCompletionDate = year + "-" + monthOfYear + "-" + dayOfMonth;
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        isEndTimeSet = true;
        mEndTimeTv.setText(DateUtils.getDisplayTimeFromTimePicker(hourOfDay, minute));
        mEndTimeTv.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        if (mCompletionDate != null && mCompletionDate.contains(":00")) {
            mCompletionDate = mCompletionDate.substring(0, mCompletionDate.indexOf(" "));
        }
        mCompletionDate += " " + hourOfDay + ":" + minute + ":00";
    }
}

