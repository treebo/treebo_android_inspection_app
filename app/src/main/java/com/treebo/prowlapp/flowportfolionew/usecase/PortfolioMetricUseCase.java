package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.portfolio.PortfolioMetricsResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 24/11/16.
 */

public class PortfolioMetricUseCase extends UseCase<PortfolioMetricsResponse> {

    RestClient mRestClient;

    int mUserID;

    public PortfolioMetricUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setUserID(int userID) {
        this.mUserID = userID;
    }

    @Override
    protected Observable<PortfolioMetricsResponse> getObservable() {
        return mRestClient.getPortfolioMetrics(mUserID);
    }
}
