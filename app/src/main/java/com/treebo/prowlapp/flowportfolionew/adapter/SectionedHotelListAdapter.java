package com.treebo.prowlapp.flowportfolionew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.portfoliomodel.PortfolioHotelV3;
import com.treebo.prowlapp.Models.portfoliomodel.SectionedHotelListModel;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 24/11/16.
 */

public class SectionedHotelListAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private ArrayList<SectionedHotelListModel> mList;
    private onHotelClickListener mListener;
    private Context mContext;

    public SectionedHotelListAdapter(Context context, ArrayList<SectionedHotelListModel> list, onHotelClickListener listener) {
        mContext = context;
        mList = list;
        mListener = listener;
    }

    @Override
    public int getSectionCount() {
       if (mList == null)
            return 0;
        else
            return mList.size();
    }

    @Override
    public int getItemCount(int i) {
        if (mList == null)
            return 0;
        else
            return mList.get(i).getHotelList().size();
    }


    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        SectionedHotelListModel section = mList.get(i);
        if (TextUtils.isEmpty(section.getSectionName()))
            ((SectionViewHolder) viewHolder).sectionTitle.setVisibility(View.GONE);
        else {
            ((SectionViewHolder) viewHolder).sectionTitle.setText(section.getSectionName());
            ((SectionViewHolder) viewHolder).sectionTitle.setVisibility(View.VISIBLE);
        }
        ((SectionViewHolder) viewHolder).sectionCount.setVisibility(View.GONE);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int section, int i1, int i2) {
        PortfolioHotelV3 hotel = mList.get(section).getHotelList().get(i1);
        if (hotel.isInCurrentLocation()) {
            ((HotelViewHolder) viewHolder).cardRl.setBackground(ContextCompat.getDrawable(mContext,
                    R.drawable.bg_green_border_rectangle));
            ((HotelViewHolder) viewHolder).icon.setImageResource(R.drawable.ic_location_crosshair);
        } else {
            ((HotelViewHolder) viewHolder).cardRl.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            ((HotelViewHolder) viewHolder).icon.setImageResource(R.drawable.ic_hotel);
        }
        ((HotelViewHolder) viewHolder).hotel = hotel;
        ((HotelViewHolder) viewHolder).hotelName.setText(hotel.getHotelName());
        ((HotelViewHolder) viewHolder).openIssues.setText(String.valueOf(hotel.getOpenIssueCount()));
        ((HotelViewHolder) viewHolder).complianceNumber.setText(String.valueOf(hotel.getCompliancePercent()));
        ((HotelViewHolder) viewHolder).pendingTasks.setText(String.valueOf(hotel.getPendingTaskCount()));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_portfolio_hotel_card,
                    parent, false);
            return new HotelViewHolder(view, mListener);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_issue_section_header,
                    parent, false);
            return new SectionViewHolder(view);
        }
    }

    private class SectionViewHolder extends RecyclerView.ViewHolder {

        TreeboTextView sectionTitle;
        TreeboTextView sectionCount;

        SectionViewHolder(View itemView) {
            super(itemView);
            sectionTitle = (TreeboTextView) itemView.findViewById(R.id.issue_section_header_title);
            sectionCount = (TreeboTextView) itemView.findViewById(R.id.issue_section_item_count);
        }
    }

    private class HotelViewHolder extends RecyclerView.ViewHolder {

        PortfolioHotelV3 hotel;
        RelativeLayout cardRl;
        ImageView icon;
        TreeboTextView hotelName;
        TreeboTextView complianceNumber;
        TreeboTextView openIssues;
        TreeboTextView pendingTasks;

        HotelViewHolder(View itemView, onHotelClickListener listener) {
            super(itemView);
            cardRl = (RelativeLayout) itemView.findViewById(R.id.hotel_card_rl);
            icon = (ImageView) itemView.findViewById(R.id.ic_hotel_card);
            hotelName = (TreeboTextView) itemView.findViewById(R.id.portfolio_hotel_name_tv);
            complianceNumber = (TreeboTextView) itemView.findViewById(R.id.hotel_compliance_score_tv);
            openIssues = (TreeboTextView) itemView.findViewById(R.id.hotel_issue_score_tv);
            pendingTasks = (TreeboTextView) itemView.findViewById(R.id.hotel_pending_task_score_tv);
            itemView.setOnClickListener(view -> {
                if (hotel != null)
                    listener.hotelClicked(hotel);
            });
        }

    }

    public interface onHotelClickListener {
        void hotelClicked(PortfolioHotelV3 hotel);
    }
}