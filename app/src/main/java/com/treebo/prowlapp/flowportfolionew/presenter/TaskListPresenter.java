package com.treebo.prowlapp.flowportfolionew.presenter;

import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.observer.GetCreatedTaskListObserver;
import com.treebo.prowlapp.flowportfolionew.usecase.GetCreatedTaskListUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.taskcreation.TaskListResponse;

/**
 * Created by abhisheknair on 27/06/17.
 */

public class TaskListPresenter implements IPortfolioContract.ITaskListPresenter {

    private IPortfolioContract.ITaskListView mView;
    private GetCreatedTaskListUseCase mUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IPortfolioContract.ITaskListView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {
        mView.setUpViewAsPerPermissions();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void fetchTaskList(int userID) {
        mView.showLoading();
        mUseCase.setUserID(userID);
        mUseCase.execute(providesCreatedTaskListObserver());
    }

    @Override
    public void onTaskListFetchFailure(String msg) {
        mView.hideLoading();
        mView.showError(msg);
    }

    @Override
    public void onTaskListFetchSuccess(TaskListResponse response) {
        mView.hideLoading();
        mView.setTaskListAdapter(response.getTaskList());
    }

    public void setUseCase(GetCreatedTaskListUseCase useCase) {
        this.mUseCase = useCase;
    }

    private GetCreatedTaskListObserver providesCreatedTaskListObserver() {
        return new GetCreatedTaskListObserver(this, mUseCase);
    }
}
