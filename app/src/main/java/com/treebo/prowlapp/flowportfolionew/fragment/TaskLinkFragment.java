package com.treebo.prowlapp.flowportfolionew.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.views.TreeboEditText;

import androidx.fragment.app.Fragment;

/**
 * Created by abhisheknair on 22/06/17.
 */

public class TaskLinkFragment extends Fragment {

    private TreeboEditText mTaskLinkEt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_link, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTaskLinkEt = (TreeboEditText) view.findViewById(R.id.task_link_edittext);
    }

    public String getTaskLink() {
        return mTaskLinkEt == null ? "" : mTaskLinkEt.getText().toString();
    }
}
