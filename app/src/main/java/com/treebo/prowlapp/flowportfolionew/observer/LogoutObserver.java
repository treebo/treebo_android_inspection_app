package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.usecase.LogoutUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.login.LogoutResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 07/12/16.
 */

public class LogoutObserver extends BaseObserver<LogoutResponse> {

    IPortfolioContract.ISettingsView mView;
    LogoutUseCase mLogoutUseCase;


    public LogoutObserver(BasePresenter presenter, LogoutUseCase useCase, IPortfolioContract.ISettingsView view) {
        super(presenter, useCase);
        mLogoutUseCase = useCase;
        mView = view;
    }

    @Override
    public void onError(Throwable e) {
        mView.onLogoutFailure(e.getMessage());
        super.onError(e);
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(LogoutResponse response) {
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mView.onLogoutSuccess();
        } else {
            mView.onLogoutFailure(response.msg);
        }
    }
}