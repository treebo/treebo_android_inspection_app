package com.treebo.prowlapp.flowportfolionew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.portfolio.PortfolioHotelListResponseV3;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 24/11/16.
 */

public class AllHotelListUseCase extends UseCase<PortfolioHotelListResponseV3> {

    RestClient mRestClient;

    private int mUserID;

    public AllHotelListUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setUserID(int userID) {
        mUserID = userID;
    }

    @Override
    protected Observable<PortfolioHotelListResponseV3> getObservable() {
        return mRestClient.getAllHotelsInPortfolio(mUserID);
    }
}
