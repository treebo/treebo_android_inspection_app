package com.treebo.prowlapp.flowportfolionew.observer;

import com.treebo.prowlapp.flowportfolionew.presenter.PendingTasksListPresenter;
import com.treebo.prowlapp.response.portfolio.PendingTasksListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 29/04/17.
 */

public class PendingTasksListObserver extends BaseObserver<PendingTasksListResponse> {

    private PendingTasksListPresenter mPresenter;

    public PendingTasksListObserver(PendingTasksListPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(PendingTasksListResponse response) {
        if (response.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.fetchSuccess(response);
        } else {
            mPresenter.fetchFailure(response.msg);
        }
    }
}