package com.treebo.prowlapp.flowportfolionew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 20/06/17.
 */

public class SettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private onSettingsItemClickListener mListener;
    private ArrayList<String> mSettingsOptionsList;
    private Context mContext;

    public SettingsAdapter(Context context, ArrayList<String> optionList,
                           onSettingsItemClickListener listener) {
        mContext = context;
        mSettingsOptionsList = optionList;
        mListener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_settings,
                parent, false);
        return new ItemViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).text.setText(mSettingsOptionsList.get(holder.getAdapterPosition()));
        switch (mSettingsOptionsList.get(holder.getAdapterPosition())) {
            case "Send feedback":
                ((ItemViewHolder) holder).image
                        .setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_feedback_black));
                break;

            case "Contact directory":
                ((ItemViewHolder) holder).image
                        .setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_contact_directory));
                break;

            case "Task completion report":
                ((ItemViewHolder) holder).image
                        .setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_task_management));
                break;

            case "Task Management":
                ((ItemViewHolder) holder).image
                        .setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_task_management));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mSettingsOptionsList.size();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        private TreeboTextView text;
        private ImageView image;

        public ItemViewHolder(View itemView, onSettingsItemClickListener listener) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.ic_option);
            text = (TreeboTextView) itemView.findViewById(R.id.item_explore_tv);
            itemView.setOnClickListener(view -> listener.onItemClicked(text.getText().toString()));
        }
    }

    public interface onSettingsItemClickListener {
        void onItemClicked(String position);
    }
}
