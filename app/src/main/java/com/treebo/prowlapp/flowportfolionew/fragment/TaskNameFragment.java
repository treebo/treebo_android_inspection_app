package com.treebo.prowlapp.flowportfolionew.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.views.TreeboEditText;

import androidx.fragment.app.Fragment;

/**
 * Created by abhisheknair on 21/06/17.
 */

public class TaskNameFragment extends Fragment {

    public final static String TAG = "TaskStartDateFragment";

    private TreeboEditText mTaskNameEt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_name, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTaskNameEt = (TreeboEditText) view.findViewById(R.id.task_name_edittext);
    }

    public String getTaskName() {
        return mTaskNameEt == null ? "" : mTaskNameEt.getText().toString();
    }
}