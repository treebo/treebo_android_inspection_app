package com.treebo.prowlapp.flowportfolionew.fragment;

import android.app.Activity;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

/**
 * Created by abhisheknair on 22/06/17.
 */

public class TaskCreateForFragment extends Fragment {

    public final static String TAG = "TaskCreateForFragment";
    private GeographicalDataResponse regionListData;
    private int userID;

    private onAreaClicked mListener;

    private View mRegionView;
    private View mClusterView;
    private View mCityView;
    private View mPropertyView;

    private TreeboTextView mPropertySubtitle;
    private TreeboTextView mPropertyTitle;
    private TreeboTextView mClusterSubtitle;
    private TreeboTextView mClusterTitle;
    private TreeboTextView mRegionSubtitle;
    private TreeboTextView mRegionTitle;
    private TreeboTextView mCitySubtitle;
    private TreeboTextView mCityTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setAreaClickedListener(onAreaClicked listener) {
        this.mListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View currentView = inflater.inflate(R.layout.fragment_task_create_for, container, false);
        mRegionView = currentView.findViewById(R.id.task_region_rl);
        mClusterView = currentView.findViewById(R.id.task_cluster_rl);
        mCityView = currentView.findViewById(R.id.task_city_rl);
        mPropertyView = currentView.findViewById(R.id.task_property_rl);
        mPropertySubtitle = (TreeboTextView)
                mPropertyView.findViewById(R.id.task_property_subtitle);
        mPropertyTitle = (TreeboTextView)
                mPropertyView.findViewById(R.id.task_property_tv);
        mRegionSubtitle = (TreeboTextView)
                mRegionView.findViewById(R.id.task_region_subtitle);
        mRegionTitle = (TreeboTextView)
                mRegionView.findViewById(R.id.task_region_tv);
        mClusterSubtitle = (TreeboTextView)
                mClusterView.findViewById(R.id.task_cluster_subtitle);
        mClusterTitle = (TreeboTextView)
                mClusterView.findViewById(R.id.task_cluster_tv);
        mCitySubtitle = (TreeboTextView)
                mCityView.findViewById(R.id.task_city_subtitle);
        mCityTitle = (TreeboTextView)
                mCityView.findViewById(R.id.task_city_tv);
        return currentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        regionListData = getArguments().getParcelable(Utils.GEOGRAPHICAL_DATA);
        userID = getArguments().getInt(Utils.USER_ID);
        if (regionListData != null) {
            mRegionView.setVisibility(regionListData.isRegionEnabledForUser(userID) ? View.VISIBLE
                    : View.GONE);
            mClusterView.setVisibility(regionListData.isClusterEnabledForUser(userID) ? View.VISIBLE
                    : View.GONE);
            mCityView.setVisibility(regionListData.isCityEnabledForUser(userID) ? View.VISIBLE
                    : View.GONE);
        }
        mRegionView.setOnClickListener(view -> {
            mListener.onRegionClicked();
        });

        mClusterView.setOnClickListener(view -> {
            mListener.onClusterClicked();
        });
        mCityView.setOnClickListener(view ->
                mListener.onCityClicked());
        mPropertyView.setOnClickListener(view ->
                mListener.onPropertyClicked());
    }

    public void resetFragmentView(String type, String displaySubtext) {
        Activity activity = getActivity();
        switch (type) {
            case "region":
                mClusterView.setClickable(false);
                mCityView.setClickable(false);
                mPropertyView.setClickable(false);
                mPropertyTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                mCityTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                mClusterTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                toggleTitleSubtitleViews(mRegionTitle, mRegionSubtitle, displaySubtext);
                break;

            case "cluster":
                mRegionView.setClickable(false);
                mCityView.setClickable(false);
                mPropertyView.setClickable(false);
                mPropertyTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                mCityTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                mRegionTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                toggleTitleSubtitleViews(mClusterTitle, mClusterSubtitle, displaySubtext);
                break;

            case "property":
                mRegionView.setClickable(false);
                mCityView.setClickable(false);
                mClusterView.setClickable(false);
                mClusterTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                mCityTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                mRegionTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                toggleTitleSubtitleViews(mPropertyTitle, mPropertySubtitle, displaySubtext);
                break;

            case "city":
                mRegionView.setClickable(false);
                mPropertyView.setClickable(false);
                mClusterView.setClickable(false);
                mPropertyTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                mClusterTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                mRegionTitle.setTextColor(ContextCompat.getColor(activity, R.color.white_50_opacity));
                toggleTitleSubtitleViews(mCityTitle, mCitySubtitle, displaySubtext);
                break;

            default:
                mRegionView.setClickable(true);
                mPropertyView.setClickable(true);
                mClusterView.setClickable(true);
                mCityView.setClickable(true);
                resetTitleSubtitleViews(mPropertyTitle, mPropertySubtitle, "");
                resetTitleSubtitleViews(mCityTitle, mCitySubtitle, "");
                resetTitleSubtitleViews(mClusterTitle, mClusterSubtitle, getString(R.string.task_cluster_subtext));
                resetTitleSubtitleViews(mRegionTitle, mRegionSubtitle, getString(R.string.task_region_subtext));
                break;

        }
    }

    private void toggleTitleSubtitleViews(TreeboTextView title, TreeboTextView subtitle,
                                          String subtitleText) {
        subtitle.setText(subtitleText);
        subtitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_16));
        subtitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        subtitle.setVisibility(View.VISIBLE);

        title.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_12sp));
        title.setTextColor(ContextCompat.getColor(getActivity(), R.color.white_50_opacity));
    }

    private void resetTitleSubtitleViews(TreeboTextView title, TreeboTextView subtitle, String subtitleText) {
        subtitle.setText(subtitleText);
        subtitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_12sp));
        subtitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.white_50_opacity));
        subtitle.setVisibility(TextUtils.isEmpty(subtitleText) ? View.GONE : View.VISIBLE);

        title.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_16));
        title.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
    }

    public interface onAreaClicked {
        void onRegionClicked();

        void onClusterClicked();

        void onCityClicked();

        void onPropertyClicked();
    }
}