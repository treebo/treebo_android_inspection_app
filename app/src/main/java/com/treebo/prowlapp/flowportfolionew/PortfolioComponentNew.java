package com.treebo.prowlapp.flowportfolionew;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowportfolionew.activity.CreatedTaskListActivity;
import com.treebo.prowlapp.flowportfolionew.activity.PendingTasksListActivity;
import com.treebo.prowlapp.flowportfolionew.activity.PortfolioActivityNew;
import com.treebo.prowlapp.flowportfolionew.activity.SelectListActivity;
import com.treebo.prowlapp.flowportfolionew.activity.SettingsActivity;
import com.treebo.prowlapp.flowportfolionew.activity.TaskCreationActivity;
import com.treebo.prowlapp.flowportfolionew.activity.TaskDetailsActivity;

import dagger.Component;

/**
 * Created by abhisheknair on 24/11/16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, PortfolioModuleNew.class})
public interface PortfolioComponentNew {
    void injectPortfolioActivity(PortfolioActivityNew portfolioActivity);

    void injectSettingsActivity(SettingsActivity settingsActivity);

    void injectPendingTasksListActivity(PendingTasksListActivity activity);

    void injectTaskCreationActivity(TaskCreationActivity activity);

    void injectSelectListActivity(SelectListActivity activity);

    void injectTaskListActivity(CreatedTaskListActivity activity);

    void injectTaskDetailsActivity(TaskDetailsActivity activity);
}
