package com.treebo.prowlapp.flowportfolionew.presenter;

import com.treebo.prowlapp.flowportfolionew.IPortfolioContract;
import com.treebo.prowlapp.flowportfolionew.observer.PendingTasksListObserver;
import com.treebo.prowlapp.flowportfolionew.usecase.PendingTaskListUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.portfolio.PendingTasksListResponse;

/**
 * Created by abhisheknair on 29/04/17.
 */

public class PendingTasksListPresenter implements IPortfolioContract.IPendingTaskListPresenter {

    private IPortfolioContract.IPendingTaskListView mView;

    private PendingTaskListUseCase mUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (IPortfolioContract.IPendingTaskListView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void fetchPendingTaskList() {
        mView.showLoading();
        mUseCase.execute(providesPendingTaskListObserver());
    }

    @Override
    public void fetchSuccess(PendingTasksListResponse response) {
        mView.hideLoading();
        mView.setUpData(response);
    }

    @Override
    public void fetchFailure(String message) {
        mView.hideLoading();
        mView.showError(message);
    }

    public void setPendingTaskListUseCase(PendingTaskListUseCase useCase) {
        this.mUseCase = useCase;
    }

    private PendingTasksListObserver providesPendingTaskListObserver() {
        return new PendingTasksListObserver(this, mUseCase);
    }
}
