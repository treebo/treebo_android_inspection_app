package com.treebo.prowlapp.flowauditnew.presenter;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.observers.CommonAreaObserver;
import com.treebo.prowlapp.flowauditnew.observers.RoomListObserver;
import com.treebo.prowlapp.flowauditnew.observers.RoomNotReadyObserver;
import com.treebo.prowlapp.flowauditnew.usecase.CommonUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomListUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomNotReadyUseCase;
import com.treebo.prowlapp.Models.auditmodel.CompleteAudit;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by sumandas on 10/11/2016.
 */

public class DailyAuditPresenter implements AuditContract.IDailyAuditPresenter {

    private AuditContract.IDailyAuditView mView;

    private CommonUseCase mCommonUseCase;

    private RoomListUseCase mRoomListUseCase;

    private RoomNotReadyUseCase mRoomNotReadyUseCase;

    @Override
    public void makeNetworkRequests(int hotelId) {
        if (hotelId != -1) {
            loadCommonAudit(hotelId);
            loadRoomList(hotelId);
        }
    }

    @Override
    public void loadCommonAudit(int hotelID) {
        showLoading();
        mCommonUseCase.setHotelId(hotelID);
        mCommonUseCase.execute(providesCommonAreaObserver());
    }

    @Override
    public void loadRoomList(int hotelId) {
        showLoading();
        mRoomListUseCase.setHotelId(hotelId);
        mRoomListUseCase.execute(providesRoomListObserver());
    }

    @Override
    public void updateAvailableRoomCount(int hotelId) {
        CompleteAudit completeAudit = CompleteAudit.getCompleteAuditByHotelId(hotelId);
        if (completeAudit!=null && completeAudit.mAvailableRooms != 0) {
            completeAudit.mAvailableRooms--;
        }
        completeAudit.save();
    }

    @Override
    public void markRoomNotReady(int roomID, String reason) {
        mRoomNotReadyUseCase.setRoomIDAndReason(roomID, reason);
        mRoomNotReadyUseCase.execute(providesRoomNotReadyObserver());
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (AuditContract.IDailyAuditView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    private void showLoading() {
        mView.showLoading();
    }

    @Override
    public BaseView getView() {
        return mView;
    }


    public void setmRoomListUseCase(RoomListUseCase mRoomListUseCase) {
        this.mRoomListUseCase = mRoomListUseCase;
    }

    public void setmCommonUseCase(CommonUseCase mCommonUseCase) {
        this.mCommonUseCase = mCommonUseCase;
    }

    public void setRoomNotReadyUseCase(RoomNotReadyUseCase useCase) {
        this.mRoomNotReadyUseCase = useCase;
    }

    private RoomNotReadyObserver providesRoomNotReadyObserver() {
        return new RoomNotReadyObserver(this, mRoomNotReadyUseCase, mView);
    }

    public CommonAreaObserver providesCommonAreaObserver() {
        return new CommonAreaObserver(this, mCommonUseCase, mView);
    }

    public RoomListObserver providesRoomListObserver() {
        return new RoomListObserver(this, mRoomListUseCase, mView);
    }
}
