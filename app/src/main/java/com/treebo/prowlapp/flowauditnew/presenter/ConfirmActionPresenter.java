package com.treebo.prowlapp.flowauditnew.presenter;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.observers.CreateIssueObserver;
import com.treebo.prowlapp.flowauditnew.observers.FraudAuditSubmitObserver;
import com.treebo.prowlapp.flowauditnew.observers.SubmitAuditObserver;
import com.treebo.prowlapp.flowauditnew.usecase.CreateIssueUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitAuditV3UseCase;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitFraudAuditUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitPeriodicAuditUseCase;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.auditmodel.AuditTypeMap;
import com.treebo.prowlapp.Models.auditmodel.CompleteAudit;
import com.treebo.prowlapp.Models.auditmodel.FraudAuditSubmitObject;
import com.treebo.prowlapp.mvpviews.BaseView;

import java.util.ArrayList;

/**
 * Created by sumandas on 11/11/2016.
 */

public class ConfirmActionPresenter implements AuditContract.IConfirmActionActionPresenter {

    private SubmitAuditV3UseCase mSubmitUseCase;

    private SubmitFraudAuditUseCase mFraudSubmitUseCase;

    private SubmitPeriodicAuditUseCase mPeriodicAuditSubmitUseCase;

    private CreateIssueUseCase mCreateIssueUseCase;

    private AuditContract.IConfirmAuditActionView mView;


    @Override
    public void createIssue(ArrayList<AnswerV3> issueList, int roomID, int userId, int auditType,
                            int hotelId, String source) {
        mView.showLoading();
        mCreateIssueUseCase.setFields(issueList, roomID,
                userId, auditType, hotelId, source);
        mCreateIssueUseCase.execute(providesCreateIssueObserver());
    }

    @Override
    public void submitAudit(ArrayList<AnswerV3> issueList, int roomID, int userId,
                            int auditType, int hotelId, String source, int taskLogID,String currentDate) {
        mView.showLoading();
        mSubmitUseCase.setFields(issueList, roomID,
                userId, auditType, hotelId, source, taskLogID, currentDate);
        mSubmitUseCase.execute(providesSubmitObserver(auditType));
    }
    @Override
    public void submitAuditRemote(ArrayList<AnswerV3> issueList, int roomID, int userId,
                            int auditType, int hotelId, String source, int taskLogID,String currentDate,String audit_submission_type) {
        mView.showLoading();
        mSubmitUseCase.setFields(issueList, roomID,
                userId, auditType, hotelId, source, taskLogID, currentDate,audit_submission_type);
        mSubmitUseCase.execute(providesSubmitObserver(auditType));
    }

    @Override
    public void submitFraudAudit(ArrayList<FraudAuditSubmitObject> fraudList, int userID, int hotelID, int taskLogID) {
        mView.showLoading();
        mFraudSubmitUseCase.setFields(hotelID, userID, taskLogID, fraudList);
        mFraudSubmitUseCase.execute(providesFraudSubmitObserver());
    }

    @Override
    public void submitPeriodicAudit(ArrayList<AnswerV3> issueList, int userId,
                                    int auditType, int hotelId, String source, int taskLogID,String currentDate) {
        mView.showLoading();
        mPeriodicAuditSubmitUseCase.setFields(issueList, userId, auditType,
                hotelId, source, taskLogID,currentDate);
        mPeriodicAuditSubmitUseCase.execute(providesSubmitObserver(auditType));
    }
    @Override
    public void submitPeriodicAuditRemote(ArrayList<AnswerV3> issueList, int userId,
                                    int auditType, int hotelId, String source, int taskLogID,String currentDate,String audit_submission_type) {
        mView.showLoading();
        mPeriodicAuditSubmitUseCase.setFields(issueList, userId, auditType,
                hotelId, source, taskLogID,currentDate,audit_submission_type);
        mPeriodicAuditSubmitUseCase.execute(providesSubmitObserver(auditType));
    }

    @Override
    public void submitAuditOffline(ArrayList<AnswerV3> issueList, int roomId, int userId, int auditType, int hotelId, String source) {

    }

    @Override
    public void updateCompleteAudit(int hotelId, int auditId, String roomNumber) {
        CompleteAudit completeAudit = CompleteAudit.getCompleteAuditByHotelId(hotelId);
        if (completeAudit == null) {
            completeAudit = new CompleteAudit(hotelId);
        }
        if (AuditTypeMap.getAuditTypeFromId(auditId).equals(AuditTypeMap.COMMON_AUDIT)) {
            completeAudit.isCommonAudited = true;
        } else {
            completeAudit.addAuditedRoomNumber(roomNumber);
        }
        completeAudit.save();
    }

    @Override
    public void onIssueCreationSuccess() {
        mView.hideLoading();
        mView.showSuccessScreen();
    }

    @Override
    public void onAPIFailure(String errorMessage) {
        mView.hideLoading();
        mView.showError(errorMessage);
    }


    public void setmSubmitUseCase(SubmitAuditV3UseCase mSubmitUseCase) {
        this.mSubmitUseCase = mSubmitUseCase;
    }

    public void setFraudSubmitUseCase(SubmitFraudAuditUseCase fraudSubmitUseCase) {
        this.mFraudSubmitUseCase = fraudSubmitUseCase;
    }

    public void setPeriodicAuditSubmitUseCase(SubmitPeriodicAuditUseCase periodicAuditSubmitUseCase) {
        this.mPeriodicAuditSubmitUseCase = periodicAuditSubmitUseCase;
    }

    public void setmCreateIssueUseCase(CreateIssueUseCase createIssueUseCase) {
        this.mCreateIssueUseCase = createIssueUseCase;
    }


    @Override
    public void setMvpView(BaseView baseView) {
        mView = (AuditContract.IConfirmAuditActionView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }


    public SubmitAuditObserver providesSubmitObserver(int auditType) {
        return new SubmitAuditObserver(this, mSubmitUseCase, mView, auditType);
    }

    private FraudAuditSubmitObserver providesFraudSubmitObserver() {
        return new FraudAuditSubmitObserver(this, mFraudSubmitUseCase, mView);
    }

    private CreateIssueObserver providesCreateIssueObserver() {
        return new CreateIssueObserver(this, mCreateIssueUseCase);
    }
}
