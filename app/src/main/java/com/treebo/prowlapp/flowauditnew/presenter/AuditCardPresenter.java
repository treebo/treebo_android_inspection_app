package com.treebo.prowlapp.flowauditnew.presenter;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.observers.RoomSingleObserver;
import com.treebo.prowlapp.flowauditnew.usecase.RoomSingleUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 02/11/16.
 */

public class AuditCardPresenter implements AuditContract.IAuditCardPresenter {

    private AuditContract.IAuditCardView mView;

    private RoomSingleUseCase mRoomSingleUse;

    @Override
    public void onCreate() {
        if (mView != null) {
            mView.viewLoadPager();
            mView.setToolbarInfo();
        }
    }

    @Override
    public void onEverythingIsOkayBtnClick() {
        if (mView != null) {
            mView.setToolbarInfo();
        }
    }

    @Override
    public void onBackPressed() {
        if (mView != null) {
            mView.backPressClicked();
        }
    }

    @Override
    public void onSelectAreaClick() {
        mView.toggleSelectAreasVisibility();
    }

    @Override
    public void loadRoomAudit(int hotelId, int roomId) {
        mView.showLoading();
        mRoomSingleUse.setFields(hotelId,roomId);
        mRoomSingleUse.execute(providesRoomSingleObserver());
    }


    @Override
    public void setMvpView(BaseView baseView) {
        mView = (AuditContract.IAuditCardView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    public void setmRoomSingleUse(RoomSingleUseCase mRoomSingleUse) {
        this.mRoomSingleUse = mRoomSingleUse;
    }

    public RoomSingleObserver providesRoomSingleObserver() {
        return new RoomSingleObserver(this, mRoomSingleUse, mView);
    }
}
