package com.treebo.prowlapp.flowauditnew;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowauditnew.activity.AuditCardActivity;
import com.treebo.prowlapp.flowauditnew.activity.AuditCardType2Activity;
import com.treebo.prowlapp.flowauditnew.activity.AuditPeriodicActivity;
import com.treebo.prowlapp.flowauditnew.activity.ConfirmActionActivity;
import com.treebo.prowlapp.flowauditnew.activity.DailyAuditActivity;
import com.treebo.prowlapp.flowauditnew.activity.FraudAuditActivity;
import com.treebo.prowlapp.flowauditnew.activity.PriceMismatchInfoActivity;
import com.treebo.prowlapp.flowauditnew.activity.ResolvableIssuesActivity;
import com.treebo.prowlapp.flowauditnew.activity.RoomPickerActivity;

import dagger.Component;

/**
 * Created by sumandas on 10/11/2016.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, AuditNewModule.class})
public interface AuditNewComponent {
    void injectDailyActivity(DailyAuditActivity dailyAuditActivity);

    void injectAuditCardActivity(AuditCardActivity auditCardActivity);

    void injectRoomPickerActivity(RoomPickerActivity roomPickerActivity);

    void injectConfirmActionActivity(ConfirmActionActivity confirmActionActivity);

    void injectResolvableIssuesActivity(ResolvableIssuesActivity resolvableIssuesActivity);

    void injectAuditTypeAcitivity(AuditCardType2Activity auditType2Activity);
    void injectAuditPeriodicAcitivity(AuditPeriodicActivity auditPeriodicActivity);

    void injectFraudAuditActivity(FraudAuditActivity activity);

    void injectPriceMismatchActivity(PriceMismatchInfoActivity activity);

}
