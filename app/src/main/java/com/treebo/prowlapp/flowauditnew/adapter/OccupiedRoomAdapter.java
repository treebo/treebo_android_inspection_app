package com.treebo.prowlapp.flowauditnew.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 03/11/2016.
 */

public class OccupiedRoomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<RoomV3Object> mAllRoomList = new ArrayList<>();
    private onOccupiedRoomClickListener mAuditRoomListener;

    public OccupiedRoomAdapter(onOccupiedRoomClickListener roomClickListener,
                               @NonNull ArrayList<RoomV3Object> rooms) {
        mAllRoomList = rooms;
        mAuditRoomListener = roomClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_all_room,
                parent, false);
        return new OccupiedRoomViewHolder(view, mAuditRoomListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // Set fields as per model
        RoomV3Object room = mAllRoomList.get(position);
        ((OccupiedRoomViewHolder) holder).room = room;
        ((OccupiedRoomViewHolder) holder).mRoomNumber.setText(room.getRoomNumber());
        ((OccupiedRoomViewHolder) holder).mRoomType.setText(room.getRoomType());
    }

    @Override
    public int getItemCount() {
        return mAllRoomList.size();
    }

    private class OccupiedRoomViewHolder extends RecyclerView.ViewHolder {

        RoomV3Object room;
        TreeboTextView mRoomNumber;
        TreeboTextView mRoomType;

        OccupiedRoomViewHolder(View itemView, onOccupiedRoomClickListener listener) {
            super(itemView);
            mRoomNumber = (TreeboTextView) itemView.findViewById(R.id.room_number_tv);
            mRoomType = (TreeboTextView) itemView.findViewById(R.id.room_type_tv);
            itemView.setOnClickListener(view -> listener.onRoomClicked(room));
        }
    }

    public interface onOccupiedRoomClickListener {
        void onRoomClicked(RoomV3Object room);
    }
}
