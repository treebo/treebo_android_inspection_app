package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowdashboard.periodicstate.PeriodicStateUtils;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by sumandas on 22/01/2017.
 */

public class AuditPeriodicAdapter extends RecyclerView.Adapter<AuditPeriodicAdapter.AuditPeriodicHolder> {

    private ArrayList<AuditAreaResponse.AuditArea> mAuditList;

    private Context mContext;

    private boolean isDone;

    private OnSubAuditClickedListener mListener;

    public AuditPeriodicAdapter(Context context, OnSubAuditClickedListener listener,
                                ArrayList<AuditAreaResponse.AuditArea> auditList, boolean done) {
        mContext = context;
        mAuditList = auditList;
        isDone = done;
        mListener = listener;
    }

    @Override
    public AuditPeriodicHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_periodic_audit_level_0, null);
        return new AuditPeriodicAdapter.AuditPeriodicHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(AuditPeriodicHolder holder, int position) {
        AuditAreaResponse.AuditArea auditDetails = mAuditList.get(position);
        holder.mAuditArea = auditDetails;
        holder.mIconAudit.setImageDrawable(ContextCompat.getDrawable(mContext,
                PeriodicStateUtils.getSubAuditIcon(auditDetails.mAuditCategoryId)));
        holder.mAuditDesc.setText(auditDetails.mName);
    }

    @Override
    public int getItemCount() {
        return mAuditList.size();
    }

    class AuditPeriodicHolder extends RecyclerView.ViewHolder {

        TreeboTextView mAuditDesc;
        ImageView mIconAudit;
        AuditAreaResponse.AuditArea mAuditArea;

        AuditPeriodicHolder(View itemView, OnSubAuditClickedListener listener) {
            super(itemView);
            if (isDone) {
                ((CardView) itemView).setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.divider_grey_opacity50));
            }
            mAuditDesc = (TreeboTextView) itemView.findViewById(R.id.task_card_header);
            mIconAudit = (ImageView) itemView.findViewById(R.id.task_card_iv);
            itemView.setOnClickListener(view -> {
                if (!isDone) {
                    listener.onSubAuditClicked(mAuditArea);
                }
            });
        }
    }

    public interface OnSubAuditClickedListener {
        void onSubAuditClicked(AuditAreaResponse.AuditArea auditArea);
    }
}
