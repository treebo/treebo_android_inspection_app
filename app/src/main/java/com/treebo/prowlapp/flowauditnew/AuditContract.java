package com.treebo.prowlapp.flowauditnew;

import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.auditmodel.FraudAuditSubmitObject;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import com.treebo.prowlapp.response.audit.FraudAuditResponse;
import com.treebo.prowlapp.response.audit.FraudAuditSubmitResponse;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 02/11/16.
 */

public interface AuditContract {

    interface IAuditCardView extends BaseView {
        void viewLoadPager();

        void setToolbarInfo();

        void toggleSelectAreasVisibility();

        void backPressClicked();

        void onLoadRoomAudit(RoomSingleResponse response);

        void onLoadRoomAuditFailure(String message);

    }

    interface IAuditCardPresenter extends BasePresenter {
        void onCreate();

        void onEverythingIsOkayBtnClick();

        void onBackPressed();

        void onSelectAreaClick();

        void loadRoomAudit(int hotelId, int roomId);
    }


    interface IAuditPeriodicView extends BaseView {

        void onLoadAuditByCategory(AuditAreaResponse response);

        void onLoadAuditFailure(String message);
    }

    interface IAuditPeriodicPresenter extends BasePresenter {
        void loadAuditCategory(int hotelId, int roomId, int userId);
        void loadAuditCategoryRemote(int hotelId, int roomId, int userId);
    }


    interface IAuditType2CardView extends BaseView {
        void viewLoadPager();

        void setToolbarInfo();

        void toggleSelectAreasVisibility();

        void backPressClicked();

    }

    interface IAuditType2Presenter extends BasePresenter {
        void onCreate();

        void onEverythingIsOkayBtnClick();

        void onBackPressed();

        void onSelectAreaClick();

    }

    interface IDailyAuditView extends BaseView {
        void onLoadCommonAudit(CommonAreaTypeResponse response);

        void onLoadRoomList(RoomListResponse response);

        void onLoadCommonFailure(String message);

        void onLoadRoomListFailure(String message);

        void onRoomNotReadyFailure(int roomID);
    }

    interface IDailyAuditPresenter extends BasePresenter {
        void makeNetworkRequests(int hotelId);

        void loadCommonAudit(int hotelID);

        void loadRoomList(int hotelId);

        void updateAvailableRoomCount(int hotelId);//set the complete audit params in db

        void markRoomNotReady(int roomID, String reason);
    }

    interface IRoomListView extends BaseView {
        void onLoadRoomList(RoomListResponse response);

        void onLoadRoomListFailure(String message);
    }

    interface IRoomListPresenter extends BasePresenter {
        void loadRoomAudit(int hotelId);
    }

    interface IConfirmAuditActionView extends BaseView {

        void showSuccessScreen();

        void onAuditSubmitSuccess(SubmitAuditResponse response);

        void onAuditSubmitFailure(String message);

        void onFraudAuditSubmitSuccess(FraudAuditSubmitResponse response);

        void onPeriodicAuditSubmitSuccess(SubmitAuditResponse response);
    }

    interface IConfirmActionActionPresenter extends BasePresenter {

        void createIssue(ArrayList<AnswerV3> issueList, int roomId, int userId,
                         int auditType, int hotelId, String source);

        void submitAudit(ArrayList<AnswerV3> issueList, int roomId, int userId,
                         int auditType, int hotelId, String source, int taskLogID,String currentDate);

        void submitAuditRemote(ArrayList<AnswerV3> issueList, int roomId, int userId,
                         int auditType, int hotelId, String source, int taskLogID,String currentDate,String audit_submission_type);

        void submitPeriodicAudit(ArrayList<AnswerV3> issueList, int userId,
                                 int auditType, int hotelId, String source, int taskLogID,String currentDate);

        void submitPeriodicAuditRemote(ArrayList<AnswerV3> issueList, int userId,
                                 int auditType, int hotelId, String source, int taskLogID,String currentDate,String audit_submission_type);


        void submitFraudAudit(ArrayList<FraudAuditSubmitObject> fraudList, int useID, int hotelID, int taskLogID);

        void submitAuditOffline(ArrayList<AnswerV3> issueList, int roomId, int userId, int auditType,
                                int hotelId, String source);

        void updateCompleteAudit(int hotelId, int auditType, String roomNumber);

        void onIssueCreationSuccess();

        void onAPIFailure(String errorMessage);
    }

    interface IFraudAuditView extends BaseView {
        void viewLoadPager();

        void setToolbarInfo();

        void toggleSelectAreasVisibility();

        void backPressClicked();

        void displayApiError(String message);

        void loadDataInCards(FraudAuditResponse response);

    }

    interface IFraudAuditPresenter extends BasePresenter {
        void setUpPageItems();

        void onDoneBtnClick();

        void onBackPressed();

        void onSelectAreaClick();

        void loadFraudAudit(int hotelId);

        void onLoadFraudAuditSuccess(FraudAuditResponse response);

        void onLoadFraudAuditFailure(String message);

        void stopLoadingScreen();
    }
}
