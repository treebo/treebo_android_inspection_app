package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 18/01/17.
 */

public class StringListAdapter extends RecyclerView.Adapter<StringListAdapter.StringViewHolder> {

    private ArrayList<String> mList;
    private boolean isHouseCount;
    private reasonClickListener mListener;
    private Context mContext;
    private int selectedPosition = -1;
    private String mRoomNumber;

    public StringListAdapter(boolean isHouseCount, ArrayList<String> list,
                             reasonClickListener listener, Context context, String roomNumber) {
        this.isHouseCount = isHouseCount;
        this.mList = list;
        this.mListener = listener;
        this.mContext = context;
        this.mRoomNumber = roomNumber;
    }

    @Override
    public StringViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_subcheckpoint,
                parent, false);
        return new StringViewHolder(view, mListener);
    }

    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
    }

    @Override
    public void onBindViewHolder(StringViewHolder holder, int position) {
        String reason = mList.get(position);
        holder.roomNumber = mRoomNumber;
        holder.reason = reason;
        holder.text.setText(reason);
        boolean isIssue = (reason.equals(mContext.getResources().getStringArray(R.array.house_count_reason_list)[0])
                || reason.equals(mContext.getResources().getStringArray(R.array.price_mismatch_list)[0]));
        if (position == selectedPosition) {
            int resId = isIssue ? R.drawable.ic_red_cross_filled : R.drawable.ic_tick_solid;
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(mContext,
                    resId));
        } else {
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(mContext,
                    R.drawable.ic_hollow_circle_green));
        }
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    class StringViewHolder extends RecyclerView.ViewHolder {

        String roomNumber;
        String reason;
        TreeboTextView text;
        ImageView checkBox;

        public StringViewHolder(View itemView, reasonClickListener listener) {
            super(itemView);
            if (isHouseCount)
                itemView.setOnClickListener(view -> {
                    selectedPosition = getAdapterPosition();
                    listener.houseCountReasonClicked(reason, roomNumber);
                });
            else
                itemView.setOnClickListener(view -> {
                    selectedPosition = getAdapterPosition();
                    listener.priceMismatchReasonClicked(reason, roomNumber);
                });
            checkBox = (ImageView) itemView.findViewById(R.id.audit_subheckpoint_checkbox);
            text = (TreeboTextView) itemView.findViewById(R.id.audit_subcheckpoint_title);
        }
    }

    public interface reasonClickListener {
        void houseCountReasonClicked(String reason, String roomNumber);

        void priceMismatchReasonClicked(String reason, String roomNumber);
    }
}
