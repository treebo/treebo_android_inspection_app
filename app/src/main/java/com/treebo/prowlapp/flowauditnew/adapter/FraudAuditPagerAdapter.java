package com.treebo.prowlapp.flowauditnew.adapter;

import android.os.Handler;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.jakewharton.rxbinding.view.RxView;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.FraudAudit;
import com.treebo.prowlapp.Models.auditmodel.FraudRoomObject;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.PagerAdapter;
import rx.subscriptions.CompositeSubscription;


import static android.view.View.GONE;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE;

/**
 * Created by abhisheknair on 17/01/17.
 */

public class FraudAuditPagerAdapter extends PagerAdapter {

    public static final String CARD_TAG = "category_card_";
    private static final int VISIBLE_CARD_NUMBER = 7;

    private FraudAuditSubCheckpointAdapter.onFraudSubCheckpointClickListener mCheckpointClickListener;
    private FraudAuditPagerAdapter.onDoneButtonClickListener mDoneBtnClickListener;
    private FraudRoomListAdapter.onRoomClickListener mRoomListClickListener;
    private extraInfoClickListener mExtraInfoClickListener;
    private onRefreshListener mRefreshListener;
    private ArrayList<FraudAudit.FraudCheckpoint> mDisplayCheckList;
    private ArrayList<FraudAudit.FraudCheckpoint> mMasterCheckList;
    private View mCurrentView;
    private int mThresholdValue;
    private ArrayList<FraudRoomObject> mHouseCountAuditRoomList;
    private ArrayList<FraudRoomObject> mPriceMismatchAuditRoomList;
    private CompositeSubscription mSubscription;

    public FraudAuditPagerAdapter(ArrayList<FraudAudit.FraudCheckpoint> auditList,
                                  FraudAuditSubCheckpointAdapter.onFraudSubCheckpointClickListener checkPointClickListener,
                                  FraudAuditPagerAdapter.onDoneButtonClickListener doneButtonClickListener,
                                  FraudAuditPagerAdapter.extraInfoClickListener extraInfoClickListener,
                                  FraudRoomListAdapter.onRoomClickListener roomListClickListener,
                                  onRefreshListener refreshListener,
                                  CompositeSubscription compositeSubscription) {
        this.mCheckpointClickListener = checkPointClickListener;
        this.mDoneBtnClickListener = doneButtonClickListener;
        this.mRoomListClickListener = roomListClickListener;
        this.mExtraInfoClickListener = extraInfoClickListener;
        this.mMasterCheckList = auditList;
        this.mRefreshListener = refreshListener;
        this.mDisplayCheckList = getNonAuditDoneCheckpoints();
        this.mSubscription = compositeSubscription;
    }

    private ArrayList<FraudAudit.FraudCheckpoint> getNonAuditDoneCheckpoints() {
        ArrayList<FraudAudit.FraudCheckpoint> categories = new ArrayList<>();
        for (FraudAudit.FraudCheckpoint category : mMasterCheckList) {
            if (!category.isAuditDone())
                categories.add(category);
        }
        return categories;
    }

    public void dataChanged(ArrayList<FraudAudit.FraudCheckpoint> list) {
        this.mMasterCheckList = list;
        this.mDisplayCheckList = getNonAuditDoneCheckpoints();
        notifyDataSetChanged();
    }

    public void refreshRoomList(SwipeRefreshLayout layout, ArrayList<FraudRoomObject> list) {
        if (layout != null) {
            layout.setRefreshing(false);
            RecyclerView recyclerView = (RecyclerView) layout.findViewById(R.id.audit_card_room_list);
            FraudRoomListAdapter adapter = (FraudRoomListAdapter) recyclerView.getAdapter();
            adapter.resetData(list);
        }
    }

    public void setSpecialAuditValues(int value, ArrayList<FraudRoomObject> houseCountRooms, ArrayList<FraudRoomObject> priceMismatchRooms) {
        this.mThresholdValue = value;
        this.mHouseCountAuditRoomList = houseCountRooms != null ? houseCountRooms : new ArrayList<>();
        this.mPriceMismatchAuditRoomList = priceMismatchRooms != null ? priceMismatchRooms : new ArrayList<>();
    }

    public void addCard(FraudAudit.FraudCheckpoint category) {
        category.setAuditDone(false);
        for (int i = 0; i < mMasterCheckList.size(); i++) {
            FraudAudit.FraudCheckpoint card = mMasterCheckList.get(i);
            if (card.getId() == category.getId())
                mMasterCheckList.set(i, category);
        }
        mDisplayCheckList = getNonAuditDoneCheckpoints();
        notifyDataSetChanged();
    }

    public void updateSubcheckPointInList(FraudAudit.FraudSubCheckpoint subCheckpoint, String checkpointName) {
        int index = getFraudCheckpointIndexFromListByValue(checkpointName);
        if (index != -1) {
            for (int i = 0; i < mMasterCheckList.get(index).getSubCheckpointArrayList().size(); i++) {
                if (subCheckpoint.getId() == mMasterCheckList.get(index).getSubCheckpointArrayList().get(i).getId())
                    mMasterCheckList.get(index).getSubCheckpointArrayList().set(i, subCheckpoint);
            }
        }
    }

    @Override
    public Object instantiateItem(ViewGroup parent, int position) {
        FraudAudit.FraudCheckpoint fraudCheckpoint = mDisplayCheckList.get(position);
        if (fraudCheckpoint.getDescription().equals(Constants.ROOM_COUNT_AUDIT) || fraudCheckpoint.getDescription().equals(Constants.PRICE_MISMATCH_AUDIT)) {
            boolean isHouseCountAudit = fraudCheckpoint.getDescription().equals(Constants.ROOM_COUNT_AUDIT);
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_roomcount_viewpager,
                    parent, false);
            TreeboTextView title = (TreeboTextView) view.findViewById(R.id.roomcount_title);
            title.setText(fraudCheckpoint.getDescription());

            ImageView extraInfo = (ImageView) view.findViewById(R.id.roomcount_extrainfo_iv);
            extraInfo.setOnClickListener(view1 -> mExtraInfoClickListener.onExtraInfoImageViewClicked(isHouseCountAudit));

            SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.room_list_swipe_layout);
            swipeRefreshLayout.setOnRefreshListener(new CustomSwipeRefreshLayout(isHouseCountAudit, swipeRefreshLayout, mRefreshListener));

            RecyclerView checkPointList = (RecyclerView) view.findViewById(R.id.audit_card_room_list);
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(parent.getContext());
            layoutManager1.setOrientation(RecyclerView.VERTICAL);
            checkPointList.setLayoutManager(layoutManager1);
            checkPointList.setItemAnimator(new DefaultItemAnimator());

            FraudRoomListAdapter adapter = new FraudRoomListAdapter(parent.getContext(),
                    (isHouseCountAudit ? mHouseCountAuditRoomList : mPriceMismatchAuditRoomList), isHouseCountAudit, mRoomListClickListener);
            checkPointList.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            TreeboButton treeboBtn = (TreeboButton) view.findViewById(R.id.audit_done_btn);
            if (adapter.getFraudAuditDoneRoomCount() >= mThresholdValue
                    || adapter.getFraudAuditDoneRoomCount() >= adapter.getItemCount()) {
                treeboBtn.setEnabled(true);
                treeboBtn.setTextColor(ContextCompat.getColor(parent.getContext(), R.color.white));
                treeboBtn.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.btn_green_rectangle));
            } else {
                treeboBtn.setEnabled(false);
                treeboBtn.setTextColor(ContextCompat.getColor(parent.getContext(), R.color.white_20_opacity));
                treeboBtn.setBackground(ContextCompat.getDrawable(parent.getContext(), R.drawable.bg_rect_green_button_disabled));
            }
            mSubscription.add(RxView.clicks(treeboBtn).throttleFirst(1, TimeUnit.SECONDS).subscribe(s -> {
                AnimationSet animationSet = (AnimationSet) new AnimationSet(false);
                Animation translateUp = AnimationUtils.loadAnimation(parent.getContext(), R.anim.translate_up);
                Animation fadeOut = AnimationUtils.loadAnimation(parent.getContext(), R.anim.fade_out);
                animationSet.addAnimation(translateUp);
                animationSet.addAnimation(fadeOut);
                view.startAnimation(animationSet);
                animationSet.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        view.setVisibility(GONE);
                        new Handler().postDelayed(() -> {
                            try {
                                mDisplayCheckList.remove(position);
                            } catch (IndexOutOfBoundsException e) {
                                SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.EXCEPTION_CAUGHT_EVENTS,
                                        new Properties().putValue(SegmentAnalyticsManager.ACTIVITY_NAME, CARD_TAG + "fraud")
                                                .putValue(SegmentAnalyticsManager.EXCEPTION_REASON, e.getMessage()));
                            }
                            notifyDataSetChanged();
                            mDoneBtnClickListener.updateCategoryFine(fraudCheckpoint, true);
                            mDoneBtnClickListener.updateProgressBar(fraudCheckpoint, position);
                        }, 50);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }));
            view.setTag(CARD_TAG + (isHouseCountAudit ? Constants.ROOM_COUNT_AUDIT : Constants.PRICE_MISMATCH_AUDIT));
            parent.addView(view);

            return view;
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_card,
                    parent, false);
            TreeboButton doneBtn = (TreeboButton) view.findViewById(R.id.audit_done_btn);
            if (fraudCheckpoint.issueRaised()) {
                doneBtn.setText(parent.getContext().getString(R.string.everything_else_is_fine));
            }
            TreeboTextView title = (TreeboTextView) view.findViewById(R.id.audit_card_title);
            title.setText(fraudCheckpoint.getDescription());

            RecyclerView checkPointList = (RecyclerView) view.findViewById(R.id.audit_card_checkbox_list);
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(parent.getContext());
            layoutManager1.setOrientation(RecyclerView.VERTICAL);
            checkPointList.setLayoutManager(layoutManager1);
            checkPointList.setItemAnimator(new DefaultItemAnimator());

            FraudAuditSubCheckpointAdapter adapter = new FraudAuditSubCheckpointAdapter(fraudCheckpoint.getSubCheckpointArrayList(),
                    parent.getContext(), mCheckpointClickListener);
            checkPointList.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            mSubscription.add(RxView.clicks(doneBtn).throttleFirst(1, TimeUnit.SECONDS).subscribe(s -> {
                AnimationSet animationSet = (AnimationSet) new AnimationSet(false);
                Animation translateUp = AnimationUtils.loadAnimation(parent.getContext(), R.anim.translate_up);
                Animation fadeOut = AnimationUtils.loadAnimation(parent.getContext(), R.anim.fade_out);
                animationSet.addAnimation(translateUp);
                animationSet.addAnimation(fadeOut);
                adapter.setAnimationPlaying(true);
                adapter.notifyDataSetChanged();
                new Handler().postDelayed(() -> {
                    view.startAnimation(animationSet);
                }, 100);
                animationSet.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        view.setVisibility(GONE);
                        new Handler().postDelayed(() -> {
                            try {
                                mDisplayCheckList.remove(position);
                            } catch (IndexOutOfBoundsException e) {
                                SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.EXCEPTION_CAUGHT_EVENTS,
                                        new Properties().putValue(SegmentAnalyticsManager.ACTIVITY_NAME, CARD_TAG + "fraud")
                                                .putValue(SegmentAnalyticsManager.EXCEPTION_REASON, e.getMessage()));
                            }
                            notifyDataSetChanged();
                            mDoneBtnClickListener.updateCategoryFine(fraudCheckpoint, true);
                            mDoneBtnClickListener.updateProgressBar(fraudCheckpoint, position);
                        }, 50);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }));

            View showMore = view.findViewById(R.id.ic_show_more);
            View showLess = view.findViewById(R.id.ic_show_less);
            int numberOfCheckpoints = fraudCheckpoint.getSubCheckpointArrayList().size();
            if (numberOfCheckpoints >= VISIBLE_CARD_NUMBER) {
                checkPointList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (newState == SCROLL_STATE_IDLE) {
                            int visibleItemCount = layoutManager1.getChildCount();
                            int totalItemCount = layoutManager1.getItemCount();
                            int pastVisibleItems = layoutManager1.findFirstVisibleItemPosition();
                            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                showLess.setVisibility(View.VISIBLE);
                                showMore.setVisibility(View.GONE);
                            } else if (layoutManager1.findFirstCompletelyVisibleItemPosition() == 0) {
                                showMore.setVisibility(View.VISIBLE);
                                showLess.setVisibility(View.GONE);
                            }
                        }
                    }
                });
                showMore.setVisibility(View.VISIBLE);
                showLess.setOnClickListener(view1 -> {
                    checkPointList.smoothScrollToPosition(0);
                    showMore.setVisibility(View.VISIBLE);
                    showLess.setVisibility(View.GONE);
                });
                showMore.setOnClickListener(view1 -> {
                    checkPointList.smoothScrollToPosition(adapter.getItemCount());
                    showMore.setVisibility(View.GONE);
                    showLess.setVisibility(View.VISIBLE);
                });
            }

            view.setTag(CARD_TAG + mMasterCheckList.get(position).getId());
            parent.addView(view);

            return view;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mDisplayCheckList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public float getPageWidth(int position) {
        return 0.93f;
    }


    public interface onDoneButtonClickListener {
        void updateProgressBar(FraudAudit.FraudCheckpoint category, int position);

        void updateCategoryFine(FraudAudit.FraudCheckpoint category, boolean isAuditDone);
    }

    public interface extraInfoClickListener {
        void onExtraInfoImageViewClicked(boolean isHouseCountAudit);
    }

    public interface onRefreshListener {
        void layoutRefreshed(boolean isHouseCount, SwipeRefreshLayout swipeRefreshLayout);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        mCurrentView = (View) object;
    }

    public View getCurrentView() {
        return mCurrentView;
    }

    private int getFraudCheckpointIndexFromListByValue(String description) {
        for (int i = 0; i < mMasterCheckList.size(); i++) {
            FraudAudit.FraudCheckpoint checkpoint = mMasterCheckList.get(i);
            if (checkpoint.getDescription().equals(description))
                return i;
        }
        return -1;
    }

    private class CustomSwipeRefreshLayout implements SwipeRefreshLayout.OnRefreshListener {

        private boolean isHouseCount;
        private SwipeRefreshLayout layout;
        private onRefreshListener refreshListener;

        public CustomSwipeRefreshLayout(boolean houseCount, SwipeRefreshLayout layout, onRefreshListener listener) {
            this.isHouseCount = houseCount;
            this.layout = layout;
            this.refreshListener = listener;
        }

        @Override
        public void onRefresh() {
            refreshListener.layoutRefreshed(isHouseCount, layout);
        }
    }
}
