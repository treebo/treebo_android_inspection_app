package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by sumandas on 10/11/2016.
 */

public class RoomListUseCase extends UseCase<RoomListResponse> {

    RestClient mRestClient;

    private int mHotelId;

    public RoomListUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setHotelId(int hotelId) {
        mHotelId = hotelId;
    }

    @Override
    protected Observable<RoomListResponse> getObservable() {
        return mRestClient.getRoomList(mHotelId);
    }
}
