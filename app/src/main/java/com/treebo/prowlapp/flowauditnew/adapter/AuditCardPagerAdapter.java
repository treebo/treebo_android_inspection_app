package com.treebo.prowlapp.flowauditnew.adapter;

import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;

import com.jakewharton.rxbinding.view.RxView;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import rx.subscriptions.CompositeSubscription;


import static android.view.View.GONE;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE;

/**
 * Created by abhisheknair on 02/11/16.
 */

public class AuditCardPagerAdapter extends PagerAdapter {

    public static final String CARD_TAG = "category_card_";
    private static final int VISIBLE_CARD_NUMBER = 7;

    private ArrayList<Category> mMasterCheckList;
    private AuditCheckpointAdapter.onCheckPointClickListener mCheckpointClickListener;
    private onDoneButtonClickListener mDoneBtnClickListener;
    private ArrayList<Category> mDisplayCheckList;
    private View mCurrentView;
    private CompositeSubscription mSubscription;

    public AuditCardPagerAdapter(ArrayList<Category> auditList,
                                 AuditCheckpointAdapter.onCheckPointClickListener checkPointClickListener,
                                 onDoneButtonClickListener doneButtonClickListener,
                                 CompositeSubscription subscription) {
        mMasterCheckList = auditList;
        mCheckpointClickListener = checkPointClickListener;
        mDoneBtnClickListener = doneButtonClickListener;
        mDisplayCheckList = getNonAuditDoneCategories();
        mSubscription = subscription;
    }

    public ArrayList<Category> getNonAuditDoneCategories() {
        ArrayList<Category> categories = new ArrayList<>();
        for (Category category : mMasterCheckList) {
            if (!category.isAuditDone())
                categories.add(category);
        }
        return categories;
    }

    public void addCard(Category category) {
        category.setAuditDone(false);
        for (int i = 0; i < mMasterCheckList.size(); i++) {
            Category card = mMasterCheckList.get(i);
            if (card.mCategoryId == category.mCategoryId)
                mMasterCheckList.set(i, category);
        }
        mDisplayCheckList = getNonAuditDoneCategories();
        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_card,
                parent, false);
        Category category = mDisplayCheckList.get(position);
        TreeboButton doneBtn = (TreeboButton) view.findViewById(R.id.audit_done_btn);
        if (category.isAuditDone() || category.issueRaised()) {
            doneBtn.setText(parent.getContext().getString(R.string.everything_else_is_fine));
        }
        TreeboTextView title = (TreeboTextView) view.findViewById(R.id.audit_card_title);
        title.setText(category.mDescription);

        RecyclerView checkPointList = (RecyclerView) view.findViewById(R.id.audit_card_checkbox_list);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(parent.getContext());
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        checkPointList.setLayoutManager(layoutManager1);
        checkPointList.setHasFixedSize(true);
        checkPointList.setItemAnimator(new DefaultItemAnimator());

        AuditCheckpointAdapter adapter = new AuditCheckpointAdapter(parent.getContext(),
                category.getmCheckPoints(), mCheckpointClickListener);
        checkPointList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        mSubscription.add(RxView.clicks(doneBtn).throttleFirst(1, TimeUnit.SECONDS).subscribe(s -> {
            AnimationSet animationSet = (AnimationSet) new AnimationSet(false);
            Animation translateUp = AnimationUtils.loadAnimation(parent.getContext(), R.anim.translate_up);
            Animation fadeOut = AnimationUtils.loadAnimation(parent.getContext(), R.anim.fade_out);
            animationSet.addAnimation(translateUp);
            animationSet.addAnimation(fadeOut);
            adapter.setAnimationPlaying(true);
            adapter.notifyDataSetChanged();
            new Handler().postDelayed(() -> {
                view.startAnimation(animationSet);
            }, 100);
            animationSet.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(GONE);
                    new Handler().postDelayed(() -> {
                        try {
                            mDisplayCheckList.remove(position);
                        } catch (IndexOutOfBoundsException e) {
                            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.EXCEPTION_CAUGHT_EVENTS,
                                    new Properties().putValue(SegmentAnalyticsManager.ACTIVITY_NAME, CARD_TAG + "audit")
                                            .putValue(SegmentAnalyticsManager.EXCEPTION_REASON, e.getMessage()));
                        }
                        notifyDataSetChanged();
                        mDoneBtnClickListener.updateCategoryFine(category, true);
                        mDoneBtnClickListener.updateProgressBar(category);
                    }, 50);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

        }));

        View showMore = view.findViewById(R.id.ic_show_more);
        View showLess = view.findViewById(R.id.ic_show_less);
        int numberOfCheckpoints = category.getmCheckPoints().size();
        if (numberOfCheckpoints >= VISIBLE_CARD_NUMBER) {
            checkPointList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == SCROLL_STATE_IDLE) {
                        int visibleItemCount = layoutManager1.getChildCount();
                        int totalItemCount = layoutManager1.getItemCount();
                        int pastVisibleItems = layoutManager1.findFirstVisibleItemPosition();
                        if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                            showLess.setVisibility(View.VISIBLE);
                            showMore.setVisibility(View.GONE);
                        } else if (layoutManager1.findFirstCompletelyVisibleItemPosition() == 0) {
                            showMore.setVisibility(View.VISIBLE);
                            showLess.setVisibility(View.GONE);
                        }
                    }
                }
            });
            showMore.setVisibility(View.VISIBLE);
            showLess.setOnClickListener(view1 -> {
                checkPointList.smoothScrollToPosition(0);
                showMore.setVisibility(View.VISIBLE);
                showLess.setVisibility(View.GONE);
            });
            showMore.setOnClickListener(view1 -> {
                checkPointList.smoothScrollToPosition(adapter.getItemCount());
                showMore.setVisibility(View.GONE);
                showLess.setVisibility(View.VISIBLE);
            });
        }

        view.setTag(CARD_TAG + mMasterCheckList.get(position).mCategoryId);
        parent.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mDisplayCheckList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public float getPageWidth(int position) {
        return 0.93f;
    }


    public interface onDoneButtonClickListener {
        void updateProgressBar(Category category);

        void updateCategoryFine(Category category, boolean isAuditDone);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        mCurrentView = (View) object;
    }

    public View getCurrentView() {
        return mCurrentView;
    }
}
