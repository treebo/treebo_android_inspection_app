package com.treebo.prowlapp.flowauditnew.presenter;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.observers.PeriodicAuditObserver;
import com.treebo.prowlapp.flowauditnew.usecase.AuditCategoryUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by sumandas on 22/01/2017.
 */

public class AuditPeriodicPresenter implements AuditContract.IAuditPeriodicPresenter {

    private AuditCategoryUseCase mAuditCategoryUseCase;

    private AuditContract.IAuditPeriodicView mView;

    @Override
    public void setMvpView(BaseView baseView) {
        mView= (AuditContract.IAuditPeriodicView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void loadAuditCategory(int hotelId, int auditId, int userId) {
        mView.showLoading();
        mAuditCategoryUseCase.setFields(hotelId,auditId,userId);
        mAuditCategoryUseCase.execute(providesAuditObserver());
    }
    @Override
    public void loadAuditCategoryRemote(int hotelId, int auditId, int userId) {
        mView.showLoading();
        mAuditCategoryUseCase.setFields(hotelId,auditId,userId,"Remote");
        mAuditCategoryUseCase.execute(providesAuditObserver());
    }


    public void setmAuditCategoryUseCase(AuditCategoryUseCase mAuditCategoryUseCase) {
        this.mAuditCategoryUseCase = mAuditCategoryUseCase;
    }

    public PeriodicAuditObserver providesAuditObserver() {
        return new PeriodicAuditObserver(this, mAuditCategoryUseCase, mView);
    }
}
