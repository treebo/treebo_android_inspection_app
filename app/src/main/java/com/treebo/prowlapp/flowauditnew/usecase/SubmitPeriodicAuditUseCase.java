package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.usecase.UseCase;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by abhisheknair on 24/01/17.
 */

public class SubmitPeriodicAuditUseCase extends UseCase<SubmitAuditResponse> {

    private RestClient mRestClient;

    private ArrayList<AnswerV3> mIssueList;
    private int mUserID;
    private int mAuditType;
    private int mHotelID;
    private int mTaskLogID;
    private String mcurrentDate;
    private String mSource;
    private String maudit_submission_type="";

    public SubmitPeriodicAuditUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setFields(ArrayList<AnswerV3> issueList, int userId, int auditType,
                          int hotelId, String source, int taskLogID,String currentDate) {
        mIssueList = issueList;
        mUserID = userId;
        mAuditType = auditType;
        mUserID = userId;
        mHotelID = hotelId;
        mSource = source;
        mTaskLogID = taskLogID;
        mcurrentDate=currentDate;
    }
    public void setFields(ArrayList<AnswerV3> issueList, int userId, int auditType,
                          int hotelId, String source, int taskLogID,String currentDate, String audit_submission_type) {
        mIssueList = issueList;
        mUserID = userId;
        mAuditType = auditType;
        mUserID = userId;
        mHotelID = hotelId;
        mSource = source;
        mTaskLogID = taskLogID;
        mcurrentDate=currentDate;
        maudit_submission_type=audit_submission_type;
    }

    @Override
    protected Observable<SubmitAuditResponse> getObservable() {
        return mRestClient.submitPeriodicAuditV3(mIssueList, mUserID, mAuditType, mHotelID, mSource, mTaskLogID,mcurrentDate,maudit_submission_type);
    }
}
