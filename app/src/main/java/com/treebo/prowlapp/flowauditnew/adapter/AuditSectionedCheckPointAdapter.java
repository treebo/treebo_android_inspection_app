package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumandas on 21/01/2017.
 */

public class AuditSectionedCheckPointAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {


    private List<Checkpoint> mCheckPointList;
    private AuditSectionedCheckPointAdapter.onSubCheckPointClickListener mListener;
    private Context mContext;
    private boolean isAnimationPlaying;

    public AuditSectionedCheckPointAdapter(Context context, List<Checkpoint> list, AuditSectionedCheckPointAdapter.onSubCheckPointClickListener listener) {
        mContext = context;
        if (list == null) {
            mCheckPointList = new ArrayList<>();
        } else {
            mCheckPointList = new ArrayList<>(list.size());
        }

        for (Checkpoint checkpoint : list) {
            if (checkpoint.isIncluded()) {
                mCheckPointList.add(checkpoint);
            }
        }
        mListener = listener;
    }

    public void setAnimationPlaying(boolean value) {
        this.isAnimationPlaying = value;
    }

    @Override
    public int getSectionCount() {
        return mCheckPointList.size();
    }

    @Override
    public int getItemCount(int section) {
        return mCheckPointList.get(section).getmSubCheckpoints().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int section) {
        ((SectionedViewHolder) viewHolder).mSectionTitle.setText(mCheckPointList.get(section).mDescription);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int section, final int relativePosition, final int absolutePosition) {
        SubCheckpoint subcheckPoint = mCheckPointList.get(section).getSubCheckpoints().get(relativePosition);
        ((AuditSubCheckpointViewHolder) holder).subCheckpoint = subcheckPoint;
        ((AuditSubCheckpointViewHolder) holder).checkpoint = mCheckPointList.get(section);
        ((AuditSubCheckpointViewHolder) holder).text.setText(subcheckPoint.getmDescription());
        if (subcheckPoint.issue()) {
            ((AuditSubCheckpointViewHolder) holder).checkBox.setImageDrawable(ContextCompat.getDrawable(mContext,
                    R.drawable.ic_red_cross_filled));
        } else {
            int resId = isAnimationPlaying ? R.drawable.ic_tick_solid : R.drawable.ic_hollow_circle_green;
            ((AuditSubCheckpointViewHolder) holder).checkBox.setImageDrawable(ContextCompat.getDrawable(mContext,
                    resId));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                View v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_audit_type2_header, parent, false);
                return new AuditSectionedCheckPointAdapter.SectionedViewHolder(v);
            case VIEW_TYPE_ITEM:
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_audit_subcheckpoint, parent, false);
                return new AuditSectionedCheckPointAdapter.AuditSubCheckpointViewHolder(v, mListener);
        }
    }

    public static class SectionedViewHolder extends RecyclerView.ViewHolder {

        public TextView mSectionTitle;

        public SectionedViewHolder(View itemView) {
            super(itemView);
            mSectionTitle = (TextView) itemView.findViewById(R.id.type_2_header);
        }
    }


    public static class AuditSubCheckpointViewHolder extends RecyclerView.ViewHolder {

        private SubCheckpoint subCheckpoint;
        private Checkpoint checkpoint;
        private TreeboTextView text;
        private ImageView checkBox;

        public AuditSubCheckpointViewHolder(View itemView, onSubCheckPointClickListener listener) {
            super(itemView);
            text = (TreeboTextView) itemView.findViewById(R.id.audit_subcheckpoint_title);
            checkBox = (ImageView) itemView.findViewById(R.id.audit_subheckpoint_checkbox);
            checkBox.setOnClickListener(view -> listener.subCheckPointClicked(subCheckpoint, checkpoint));
            itemView.setOnClickListener(view -> {
                listener.subCheckPointClicked(subCheckpoint, checkpoint);
            });
            itemView.setOnLongClickListener(view -> {
                listener.subCheckPointLongPress(subCheckpoint, checkpoint);
                return false;
            });
        }
    }

    public void refreshItem(Checkpoint checkpoint, SubCheckpoint subCheckpoint) {
        for (int index = 0; index < mCheckPointList.size(); index++) {
            if (checkpoint.mCheckpointId == mCheckPointList.get(index).mCheckpointId) {
                for (int subIndex = 0; subIndex < checkpoint.getmSubCheckpoints().size(); subIndex++)
                    if (subCheckpoint.getmSubCheckpointId() == checkpoint.getmSubCheckpoints().get(subIndex).getmSubCheckpointId()) {
                        checkpoint.getmSubCheckpoints().set(subIndex, subCheckpoint);
                        mCheckPointList.set(index, checkpoint);
                        notifyItemChanged(getAbsoluteIndex(index, subIndex));
                        break;
                    }
                break;
            }
        }
    }

    private int getAbsoluteIndex(int index, int subIndex) {
        int absoluteCount = 0;
        for (int i = 0; i < index; i++) {
            absoluteCount++;
            absoluteCount += mCheckPointList.get(i).getSubCheckpoints().size();
        }
        absoluteCount++;
        absoluteCount += subIndex;
        return absoluteCount;
    }

    public interface onSubCheckPointClickListener {
        void subCheckPointClicked(SubCheckpoint subCheckpoint, Checkpoint checkpoint);

        void subCheckPointLongPress(SubCheckpoint subCheckpoint, Checkpoint checkpoint);
    }
}
