package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.FraudAudit;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 17/01/17.
 */

public class FraudAuditSubCheckpointAdapter extends RecyclerView.Adapter<FraudAuditSubCheckpointAdapter.FraudAuditSubCheckpointViewHolder> {

    private ArrayList<FraudAudit.FraudSubCheckpoint> mSubCheckpointArrayList;
    private Context mContext;
    private onFraudSubCheckpointClickListener mListener;
    private boolean isAnimationPlaying;

    public FraudAuditSubCheckpointAdapter(ArrayList<FraudAudit.FraudSubCheckpoint> list,
                                          Context context, onFraudSubCheckpointClickListener listener) {
        mSubCheckpointArrayList = list;
        mContext = context;
        mListener = listener;
    }

    @Override
    public FraudAuditSubCheckpointViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_checkpoint,
                parent, false);
        return new FraudAuditSubCheckpointViewHolder(view, mListener);
    }

    public void refreshItem(FraudAudit.FraudSubCheckpoint subCheckpoint) {
        int index = getCheckpointPositionInList(subCheckpoint);
        if (index != -1) {
            mSubCheckpointArrayList.set(index, subCheckpoint);
            notifyItemChanged(index);
        }
    }

    public void setAnimationPlaying(boolean value) {
        this.isAnimationPlaying = value;
    }

    @Override
    public void onBindViewHolder(FraudAuditSubCheckpointViewHolder holder, int position) {
        FraudAudit.FraudSubCheckpoint subCheckpoint = mSubCheckpointArrayList.get(position);
        holder.subCheckpoint = subCheckpoint;
        holder.text.setText(subCheckpoint.getDescription());
        if (subCheckpoint.isIssue()) {
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_red_cross_filled));
        } else {
            int resId = isAnimationPlaying ? R.drawable.ic_tick_solid : R.drawable.ic_hollow_circle_green;
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(mContext, resId));
        }
    }

    @Override
    public int getItemCount() {
        return mSubCheckpointArrayList == null ? 0 : mSubCheckpointArrayList.size();
    }

    class FraudAuditSubCheckpointViewHolder extends RecyclerView.ViewHolder {

        private FraudAudit.FraudSubCheckpoint subCheckpoint;
        private TreeboTextView text;
        private ImageView checkBox;

        public FraudAuditSubCheckpointViewHolder(View itemView, onFraudSubCheckpointClickListener listener) {
            super(itemView);
            text = (TreeboTextView) itemView.findViewById(R.id.audit_checkpoint_title);
            checkBox = (ImageView) itemView.findViewById(R.id.audit_checkpoint_checkbox);
            checkBox.setOnClickListener(view -> listener.subCheckpointClicked(subCheckpoint));
            itemView.setOnClickListener(view -> {
                listener.subCheckpointClicked(subCheckpoint);
            });
        }
    }

    public interface onFraudSubCheckpointClickListener {
        void subCheckpointClicked(FraudAudit.FraudSubCheckpoint subCheckpoint);
    }

    private int getCheckpointPositionInList(FraudAudit.FraudSubCheckpoint subCheckpoint) {
        for (int i = 0; i < mSubCheckpointArrayList.size(); i++) {
            if (subCheckpoint.getId() == mSubCheckpointArrayList.get(i).getId())
                return i;
        }
        return -1;
    }
}
