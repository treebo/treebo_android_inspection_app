package com.treebo.prowlapp.flowauditnew.presenter;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.observers.FraudAuditObserver;
import com.treebo.prowlapp.flowauditnew.usecase.FraudAuditUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.audit.FraudAuditResponse;

/**
 * Created by abhisheknair on 17/01/17.
 */

public class FraudAuditPresenter implements AuditContract.IFraudAuditPresenter {

    private AuditContract.IFraudAuditView mView;

    private FraudAuditUseCase mUseCase;

    @Override
    public void setUpPageItems() {
        mView.viewLoadPager();
        mView.setToolbarInfo();
    }

    @Override
    public void onDoneBtnClick() {

    }

    @Override
    public void onBackPressed() {
        mView.backPressClicked();
    }

    @Override
    public void onSelectAreaClick() {
        mView.toggleSelectAreasVisibility();
    }

    @Override
    public void loadFraudAudit(int hotelId) {
        mView.showLoading();
        mUseCase.setFields(hotelId);
        mUseCase.execute(providesFraudAuditObserver());
    }

    @Override
    public void onLoadFraudAuditSuccess(FraudAuditResponse response) {
        mView.loadDataInCards(response);
    }

    @Override
    public void onLoadFraudAuditFailure(String message) {
        mView.displayApiError(message);
    }

    @Override
    public void stopLoadingScreen() {
        mView.hideLoading();
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (AuditContract.IFraudAuditView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    public void setFraudAuditUseCase(FraudAuditUseCase useCase) {
        this.mUseCase = useCase;
    }

    public FraudAuditObserver providesFraudAuditObserver() {
        return new FraudAuditObserver(this, mUseCase);
    }
}
