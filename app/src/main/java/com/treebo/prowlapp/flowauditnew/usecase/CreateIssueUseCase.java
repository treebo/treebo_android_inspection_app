package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.usecase.UseCase;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by abhisheknair on 29/06/17.
 */

public class CreateIssueUseCase extends UseCase<SubmitAuditResponse> {

    private RestClient mRestClient;

    private ArrayList<AnswerV3> mIssueList;
    private Integer mRoomID;
    private int mUserID;
    private int mAuditType;
    private int mHotelID;
    private String mSource;


    public CreateIssueUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setFields(ArrayList<AnswerV3> issueList, Integer roomId, int userId, int auditType,
                          int hotelId, String source) {
        mIssueList = issueList;
        mRoomID = roomId;
        mUserID = userId;
        mAuditType = auditType;
        mUserID = userId;
        mHotelID = hotelId;
        mSource = source;
    }

    @Override
    protected Observable<SubmitAuditResponse> getObservable() {
        return mRestClient.createNewIssue(mIssueList, mRoomID, mUserID, mAuditType, mHotelID, mSource);
    }
}
