package com.treebo.prowlapp.flowauditnew;

import android.text.TextUtils;

import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.auditmodel.AuditSaveObject;
import com.treebo.prowlapp.Models.auditmodel.AuditTypeMap;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.Category_Table;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint_Table;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sumandas on 10/11/2016.
 */

public class AuditUtilsV3 {

    public static final String AUDIT_TYPE = "audit_type";
    public static final int FRAUD_AUDIT_TYPE = 101;

    public static final String SAVED_CHECKPOINT_PREFIX = "saved_checkpoint_";


    public static ArrayList<Category> checkpointList(HashMap<Integer, ArrayList<Integer>> checkPointList) {
        if (checkPointList != null) {
            ArrayList<Integer> categoryList = new ArrayList<>(checkPointList.keySet().size());
            for (Integer includedCategories : checkPointList.keySet()) {
                categoryList.add(includedCategories);
            }

            ArrayList<Category> includedCategories = new ArrayList<>();
            if (categoryList.size() > 0) {
                Condition.In categoryIn = Condition.column(Category_Table.mCategoryId.getNameAlias()).in(categoryList.get(0));
                for (int i = 1; i < categoryList.size(); i++) {
                    categoryIn.and(categoryList.get(i));
                }
                //get the list of included checkpoints
                includedCategories = (ArrayList<Category>) SQLite.select().from(Category.class).where(categoryIn).queryList();

                //check if checkpoint is excluded and set isIncluded flag
                for (Category category : includedCategories) {
                    for (Checkpoint checkpoint : category.getCheckpoints()) {
                        ArrayList<Integer> excludedCheckPoints = checkPointList.get(category.mCategoryId);
                        checkpoint.setmSubCheckpoints((ArrayList<SubCheckpoint>) SQLite.select()
                                .from(SubCheckpoint.class).where(SubCheckpoint_Table.mCheckpointId_mCheckpointId.eq(checkpoint.mCheckpointId)).queryList());
                        if (excludedCheckPoints != null && excludedCheckPoints.contains(checkpoint.mCheckpointId)) {
                            checkpoint.setIncluded(false);
                        } else {
                            checkpoint.setIncluded(true);
                        }
                    }
                }
            }
            return includedCategories;
        } else
            return new ArrayList<>();
    }


    public static void prefillSavedIssues(ArrayList<Category> categories, AuditSaveObject auditSaveObject) {
        for (AnswerV3 answerV3 : auditSaveObject.mAnswerList) {
            for (Category category : categories) {
                if (category.mCategoryId == answerV3.getCategory_id()) {
                    if (answerV3.isAuditDone) {
                        category.setAuditDone(true);
                    } else {
                        category.setIssueRaised(true);
                        for (Checkpoint checkpoint : category.getmCheckPoints()) {
                            if (checkpoint.mCheckpointId == answerV3.getCheckpoint_id()) {
                                checkpoint.setIssue(true);
                                for (SubCheckpoint subCheckpoint : checkpoint.getmSubCheckpoints()) {
                                    if (subCheckpoint.getmSubCheckpointId() == answerV3.getSubcheckpoint_id()) {
                                        subCheckpoint.setIssue(true);
                                        subCheckpoint.setIssueText(answerV3.getComment());
                                        subCheckpoint.setPhotoUrls(answerV3.getImage_url());
                                        checkpoint.setIssueText(subCheckpoint.getmDescription());
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    public static ArrayList<Category> preMarkIssuesOnCheckpoints(ArrayList<Category> list, ArrayList<IssueListModelV3> issueList) {
        if (issueList != null) {
            for (IssueListModelV3 issue : issueList) {
                for (int j = 0; j < list.size(); j++) {
                    Category category = list.get(j);
                    for (int k = 0; k < category.getmCheckPoints().size(); k++) {
                        Checkpoint checkpoint = category.getmCheckPoints().get(k);
                        if (checkpoint.mCheckpointId == issue.getCheckpointID()) {
                            checkpoint.setPendingIssue(true);
                            checkpoint.setIssueText(issue.getSubCheckpointText());
                            category.getmCheckPoints().set(k, checkpoint);
                            category.setPendingIssue(true);
                            list.set(j, category);
                            break;
                        }
                    }
                }
            }
        }
        return list;
    }

    public static ArrayList<SubCheckpoint> preMarkIssuesOnSubCheckpoints(ArrayList<SubCheckpoint> list, ArrayList<IssueListModelV3> issueList) {
        if (issueList != null) {
            for (IssueListModelV3 issue : issueList) {
                for (int j = 0; j < list.size(); j++) {
                    SubCheckpoint subCheckpoint = list.get(j);
                    if (subCheckpoint.getmSubCheckpointId() == issue.getSubCheckpointID()) {
                        subCheckpoint.setPendingIssue(true);
                        subCheckpoint.setPendingIssueDetails(issue);
                        list.set(j, subCheckpoint);
                        break;
                    }
                }
            }
        }
        return list;
    }

    public static ArrayList<IssueListModelV3> removeResolvedIssueFromIssueList(SubCheckpoint subCheckpoint, ArrayList<IssueListModelV3> issueList) {
        int index = -1;
        if (issueList != null) {
            for (int i = 0; i < issueList.size(); i++) {
                IssueListModelV3 issue = issueList.get(i);
                if (subCheckpoint.getmSubCheckpointId() == issue.getSubCheckpointID()) {
                    index = i;
                    break;
                }
            }
        }
        if (index != -1)
            issueList.remove(index);
        return issueList;
    }



    public static String createSavedCheckPointKey(int hotelId, int auditType, String room) {
        return SAVED_CHECKPOINT_PREFIX + createAuditKey(hotelId, auditType, room);
    }


    public static String createAuditKey(int hotelId, int auditType, String room) {
        if (room == null) {
            return hotelId + "_" + auditType;
        } else {
            return hotelId + "_" + auditType + "_" + room;
        }
    }


    public static String getAuditIdFromKey(String key) {
        if (!TextUtils.isEmpty(key)) {
            String[] keyComponents = key.split("_");
            return keyComponents[1];
        }
        return "";
    }


    public static String getAuditTypeFromKey(String key) {
        String id = getAuditIdFromKey(key);
        return AuditTypeMap.getAuditTypeFromId(Integer.parseInt(id));
    }


    public static String getRoomFromKey(String key) {
        if (key != null) {
            String[] keyComponents = key.split("_");
            if (keyComponents.length == 3) {
                return keyComponents[2];
            }
        }
        return "";
    }

    public static int getAuditedCategoryCount(ArrayList<Category> categories) {
        int audited = 0;
        for (Category category : categories) {
            if (category.isAuditDone()) {
                audited++;
            }
        }
        return audited;
    }

    public static int getIndexOfCheckpointInCategory(int checkpointId, List<Checkpoint> checkpoints) {
        for (int i = 0; i < checkpoints.size(); i++) {
            Checkpoint card = checkpoints.get(i);
            if (checkpointId == card.mCheckpointId)
                return i;
        }
        return -1;
    }

    public static int getIndexOfCardCategory(int categoryId, ArrayList<Category> categoryList) {
        for (int i = 0; i < categoryList.size(); i++) {
            Category card = categoryList.get(i);
            if (categoryId == card.mCategoryId)
                return i;
        }
        return -1;
    }


    public static ArrayList<Category> getNonAuditDoneCategories(ArrayList<Category> categoryList) {
        ArrayList<Category> categories = new ArrayList<>();
        for (Category category : categoryList) {
            if (!category.isAuditDone())
                categories.add(category);
        }
        return categories;
    }

}
