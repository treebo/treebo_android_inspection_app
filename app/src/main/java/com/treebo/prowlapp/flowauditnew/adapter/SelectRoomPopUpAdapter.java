package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import android.content.res.ColorStateList;

import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 01/12/16.
 */

public class SelectRoomPopUpAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<RoomV3Object> mRoomList = new ArrayList<>();
    private optionClickListener mListener;
    private Context mContext;

    public SelectRoomPopUpAdapter(Context context, ArrayList<RoomV3Object> list, optionClickListener listener) {
        this.mRoomList = list;
        this.mListener = listener;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog_select_room,
                parent, false);
        return new RoomViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RoomV3Object room = mRoomList.get(position);
        ((RoomViewHolder) holder).room = room;
        ((RoomViewHolder) holder).roomName.setText(room.getRoomNumber());
    }

    @Override
    public int getItemCount() {
        if (mRoomList != null)
            return mRoomList.size();
        else
            return 0;
    }

    private class RoomViewHolder extends RecyclerView.ViewHolder {

        private RoomV3Object room;
        private TreeboTextView roomName;
        private RadioGroup optionList;

        public RoomViewHolder(View itemView, optionClickListener listener) {
            super(itemView);
            roomName = (TreeboTextView) itemView.findViewById(R.id.room_name_tv);
            optionList = (RadioGroup) itemView.findViewById(R.id.radio_group_select_room);
            optionList.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int checkedID) {
                    int selectedID = 0;
                    if (checkedID == R.id.radio_btn_not_ready)
                        selectedID = Constants.ROOM_NOT_READY;
                    else if (checkedID == R.id.radio_btn_occupied)
                        selectedID = Constants.ROOM_OCCUPIED;
                    else if (checkedID == R.id.radio_btn_others)
                        selectedID = Constants.OTHERS;
                    listener.onOptionsSelected(selectedID, room);
                }
            });
            ColorStateList colorStateList = new ColorStateList(
                    new int[][]{
                            new int[]{-android.R.attr.state_checked},
                            new int[]{android.R.attr.state_checked}
                    },
                    new int[]{
                            ContextCompat.getColor(mContext, R.color.text_color_secondary)
                            , ContextCompat.getColor(mContext, R.color.green_color_primary),
                    }
            );
            AppCompatRadioButton notReadyBtn = (AppCompatRadioButton) optionList.findViewById(R.id.radio_btn_not_ready);
            AppCompatRadioButton occupiedBtn = (AppCompatRadioButton) optionList.findViewById(R.id.radio_btn_occupied);
            AppCompatRadioButton othersBtn = (AppCompatRadioButton) optionList.findViewById(R.id.radio_btn_others);
            notReadyBtn.setSupportButtonTintList(colorStateList);
            occupiedBtn.setSupportButtonTintList(colorStateList);
            othersBtn.setSupportButtonTintList(colorStateList);
        }
    }

    public interface optionClickListener {
        void onOptionsSelected(int optionNumber, RoomV3Object roomID);
    }
}
