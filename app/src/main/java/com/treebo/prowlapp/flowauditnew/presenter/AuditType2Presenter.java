package com.treebo.prowlapp.flowauditnew.presenter;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by sumandas on 20/01/2017.
 */

public class AuditType2Presenter implements AuditContract.IAuditType2Presenter {

    private AuditContract.IAuditType2CardView mView;



    @Override
    public void setMvpView(BaseView baseView) {
        mView = (AuditContract.IAuditType2CardView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    @Override
    public void onCreate() {
        if (mView != null) {
            mView.viewLoadPager();
            mView.setToolbarInfo();
        }
    }

    @Override
    public void onEverythingIsOkayBtnClick() {
        if (mView != null) {
            mView.setToolbarInfo();
        }
    }

    @Override
    public void onBackPressed() {
        if (mView != null) {
            mView.backPressClicked();
        }
    }

    @Override
    public void onSelectAreaClick() {
        mView.toggleSelectAreasVisibility();
    }



}
