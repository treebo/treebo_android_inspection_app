package com.treebo.prowlapp.flowauditnew.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AuditStateChangeEvent;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.AuditNewComponent;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.DaggerAuditNewComponent;
import com.treebo.prowlapp.flowauditnew.adapter.AuditCardPagerAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.AuditCheckpointAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.AuditSubcheckpointAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.SelectAreasAdapter;
import com.treebo.prowlapp.flowauditnew.presenter.AuditCardPresenter;
import com.treebo.prowlapp.flowauditnew.usecase.RoomSingleUseCase;
import com.treebo.prowlapp.flowdashboard.activity.DashboardActivity;
import com.treebo.prowlapp.flowupdateauditOrissue.AddCommentActivityNew;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Models.auditmodel.AuditSaveObject;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint_Table;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.DatabaseUtils;
import com.treebo.prowlapp.Utils.IssueFormatUtils;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.TypeFaceUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;
import static android.view.View.VISIBLE;


/**
 * Created by abhisheknair on 02/11/16.
 */

public class AuditCardActivity extends AppCompatActivity implements AuditContract.IAuditCardView,
        AuditCheckpointAdapter.onCheckPointClickListener, AuditCardPagerAdapter.onDoneButtonClickListener,
        AuditSubcheckpointAdapter.onSubcheckPointClickListener, SelectAreasAdapter.onSelectAreasClickListener {

    private static final int REQUEST_ADD_COMMENT = 100;
    private static final int REQUEST_RESOLVABLE_ISSUES = 102;
    private static final int REQUEST_CONFIRM_ACTIVITY = 104;
    private static final int REQUEST_PRE_MARKED_ISSUE = 105;
    private static final int VISIBLE_CARD_NUMBER = 7;

    private Boolean mIsFromAuditAnyHotel;
    private String mRoomName;
    private int mHotelId;
    private String mHotelName;
    private int mAuditType;

    private TreeboTextView mRoomNumberTitle;
    private ImageView mPeriodicAuditToolbarIcon;
    private ProgressBar mAuditProgressBar;
    private TreeboTextView mAuditProgressStatus;
    private ViewPager mViewpager;

    private View mCardLevel2View;
    private View mSelectAreaView;

    private TreeboTextView mCardLevel2Title;
    private RecyclerView mCardLevel2RecyclerView;

    private RecyclerView mSelectAreasRecyclerView;

    private View mRootView;

    @Inject
    public AuditCardPresenter mAuditPresenter;

    @Inject
    RoomSingleUseCase mRoomSingleUseCase;

    @Inject
    public RxBus mRxBus;

    @Inject
    AuditPrefManagerV3 mAuditPrefManagerV3;

    @Inject
    LoginSharedPrefManager mLoginManager;

    @Inject
    public Navigator mNavigator;

    private AuditCardPagerAdapter mCardPagerAdapter;

    private Checkpoint mCheckpointClicked;
    private SubCheckpoint mSubCheckpointSelected;

    private ArrayList<Category> mCategoryList = new ArrayList<>();
    private SelectAreasAdapter mSelectAreasAdapter;

    private AuditSaveObject mAuditSavedObject;
    private int mRoomId;
    private View mLoadingView;
    private boolean makeEndpointCall;
    private View mErrorView;
    private boolean fromConfirmActivity;
    private String mCurrentDate;
    private String mCurrentDateTime;
    private boolean isRestart;
    private boolean isInHotelLocation;
    CompositeSubscription mSubsription = new CompositeSubscription();
    private int mRoomTypeID;
    private int mTaskLogID;
    private TaskModel mTaskModel;
    private ArrayList<IssueListModelV3> mPreExistingIssuesInRoom;
    private boolean isExtraRoomAudit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_card);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_progress);
        setSupportActionBar(toolbar);
        mRootView = findViewById(R.id.audit_card_root_view);
        mLoadingView = findViewById(R.id.loader_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mCategoryList = intent.getParcelableArrayListExtra(Category.INCLUDED_CATEGORY_LIST);
            mRoomName = intent.getStringExtra(Utils.ROOM_NAME);
            //isCommonArea = "CA".equals(mRoomName);
            isInHotelLocation = intent.getBooleanExtra(Utils.IS_IN_HOTEL, true);
            mHotelName = intent.getStringExtra(Utils.HOTEL_NAME);
            mRoomId = intent.getIntExtra(Constants.ROOM_ID, -1);
            mAuditType = intent.getIntExtra(AuditUtilsV3.AUDIT_TYPE, -1);
            mRoomTypeID = intent.getIntExtra(Utils.ROOM_TYPE_ID, -1);
            mTaskModel = intent.getParcelableExtra(Constants.TASK_MODEL);
            mTaskLogID = mTaskModel == null ? 0 : mTaskModel.getTaskLogID();
            isRestart = intent.getBooleanExtra(Constants.IS_RESTART_AUDIT, false);
            mPreExistingIssuesInRoom = intent.getParcelableArrayListExtra(Utils.PRE_EXISTING_ISSUES);
            mHotelId = intent.getIntExtra(Utils.HOTEL_ID, -1);
            isExtraRoomAudit = intent.getBooleanExtra(Utils.IS_EXTRA_ROOM_AUDIT, false);
            mIsFromAuditAnyHotel = intent.getBooleanExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL,false);
        } else {
            mCategoryList = savedInstanceState.getParcelableArrayList(Category.INCLUDED_CATEGORY_LIST);
            mRoomName = savedInstanceState.getString(Utils.ROOM_NAME);
            isInHotelLocation = savedInstanceState.getBoolean(Utils.IS_IN_HOTEL, true);
            mHotelName = savedInstanceState.getString(Utils.HOTEL_NAME);
            mRoomId = savedInstanceState.getInt(Constants.ROOM_ID, -1);
            mAuditType = savedInstanceState.getInt(AuditUtilsV3.AUDIT_TYPE, -1);
            mRoomTypeID = savedInstanceState.getInt(Utils.ROOM_TYPE_ID, -1);
            mTaskModel = savedInstanceState.getParcelable(Constants.TASK_MODEL);
            mTaskLogID = mTaskModel == null ? 0 : mTaskModel.getTaskLogID();
            isRestart = savedInstanceState.getBoolean(Constants.IS_RESTART_AUDIT, false);
            mPreExistingIssuesInRoom = savedInstanceState.getParcelableArrayList(Utils.PRE_EXISTING_ISSUES);
            mHotelId = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            mCheckpointClicked = savedInstanceState.getParcelable(Utils.CLICKED_CHECKPOINT);
            mSubCheckpointSelected = savedInstanceState.getParcelable(Utils.CLICKED_SUBCHECKPOINT);
            isExtraRoomAudit = savedInstanceState.getBoolean(Utils.IS_EXTRA_ROOM_AUDIT);
            mIsFromAuditAnyHotel = savedInstanceState.getBoolean(Utils.IS_FROM_AUDIT_ANY_HOTEL,false);
        }

        AuditCategory auditCategory = AuditCategory.getAuditCategoryById(mAuditType);
        if (auditCategory != null && auditCategory.mConstantAuditName.equals(Constants.ROOM_AUDIT_STRING) && !isRestart) {
            makeEndpointCall = true;
        }
        AuditNewComponent auditComponent = DaggerAuditNewComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectAuditCardActivity(this);

        SegmentAnalyticsManager.setmLoginManager(mLoginManager);

        mAuditSavedObject = mAuditPrefManagerV3.getSavedAudit(AuditUtilsV3.createAuditKey(mHotelId, mAuditType, mRoomName));
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        mCurrentDate = sdf.format(calendar.getTime());
        SimpleDateFormat sdf_with_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale.getDefault());
        sdf_with_time.setTimeZone(TimeZone.getTimeZone("UTC"));
        mCurrentDateTime = sdf_with_time.format(calendar.getTime());

        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));

        mRoomNumberTitle = (TreeboTextView) findViewById(R.id.toolbar_room_number_tv);
        mAuditProgressBar = (ProgressBar) findViewById(R.id.toolbar_audit_progress_bar);
        mPeriodicAuditToolbarIcon = (ImageView) findViewById(R.id.toolbar_room_number_iv);
        mAuditProgressBar.setMax(mCategoryList.size());
        mAuditProgressStatus = (TreeboTextView) findViewById(R.id.toolbar_audit_progress_tv);

        View statusAreaText = findViewById(R.id.select_areas_tv);
        statusAreaText.setOnClickListener(view -> {
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SELECT_AREA_CLICK,
                    new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                            .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mRoomName)
                            .putValue(SegmentAnalyticsManager.COMPLETION_RATIO, String.valueOf(AuditUtilsV3.getAuditedCategoryCount(mCategoryList)) + "/" + mCategoryList.size()));
            mAuditPresenter.onSelectAreaClick();
        });
        View statusAreaDrawable = findViewById(R.id.ic_select_area);
        statusAreaDrawable.setOnClickListener(view -> mAuditPresenter.onSelectAreaClick());
        mViewpager = (ViewPager) findViewById(R.id.audit_card_viewpager);

        mCardLevel2View = findViewById(R.id.audit_card_level2_layout);
        mSelectAreaView = findViewById(R.id.select_areas_layout);

        if (mCardLevel2View != null) {
            mCardLevel2Title = (TreeboTextView) mCardLevel2View.findViewById(R.id.audit_card_level2_title);
            View cardLevel2crossIv = mCardLevel2View.findViewById(R.id.ic_audit_card_level2_cancel);
            mCardLevel2RecyclerView = (RecyclerView) mCardLevel2View.findViewById(R.id.audit_card_level2_checkbox_list);
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
            layoutManager1.setOrientation(RecyclerView.VERTICAL);
            mCardLevel2RecyclerView.setLayoutManager(layoutManager1);
            mCardLevel2RecyclerView.setItemAnimator(new DefaultItemAnimator());
            cardLevel2crossIv.setOnClickListener(view -> mCardLevel2View.setVisibility(View.GONE));
            mCardLevel2View.setOnClickListener(view -> mCardLevel2View.setVisibility(View.GONE));
        }

        if (mSelectAreaView != null) {
            View closeSelectAreasCross = mSelectAreaView.findViewById(R.id.ic_cross_select_areas);
            closeSelectAreasCross.setOnClickListener(view -> {
                mAuditPresenter.onBackPressed();
            });
            mSelectAreasRecyclerView = (RecyclerView) mSelectAreaView.findViewById(R.id.select_areas_rv);
            mSelectAreasRecyclerView.setItemAnimator(new SlideInLeftAnimator());
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
            layoutManager1.setOrientation(RecyclerView.VERTICAL);
            mSelectAreasRecyclerView.setLayoutManager(layoutManager1);
            mSelectAreasRecyclerView.setItemAnimator(new DefaultItemAnimator());
        }

        View backBtn = findViewById(R.id.toolbar_audit_back_btn);
        if (backBtn != null)
            backBtn.setOnClickListener((View v) -> mAuditPresenter.onBackPressed());


        mSubsription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                ArrayList<HotelLocation> hotelList = ((HotelChangedEvent) event).mHotelList;
                if (hotelList.size() == 0) {
                    finish();
                }
            }
        }));

        if (makeEndpointCall) {
            mAuditPresenter.loadRoomAudit(mHotelId, mRoomId);
        } else {
            mAuditSavedObject = mAuditPrefManagerV3.getSavedAudit
                    (AuditUtilsV3.createAuditKey(mHotelId, mAuditType, mRoomName));
            if (mAuditSavedObject != null &&
                    mAuditSavedObject.getPreMarkedIssues() != null)
                mPreExistingIssuesInRoom = mAuditSavedObject.getPreMarkedIssues();
            mCategoryList = AuditUtilsV3.preMarkIssuesOnCheckpoints(mCategoryList, mPreExistingIssuesInRoom);
            if (mAuditSavedObject != null) {
                prefillIssues(mCategoryList, mAuditSavedObject);
            } else {
                mAuditPresenter.onCreate();
            }
            mAuditProgressBar.setProgress(AuditUtilsV3.getAuditedCategoryCount(mCategoryList));
        }
    }


    @Inject
    public void setUp() {
        mAuditPresenter.setMvpView(this);
        mAuditPresenter.setmRoomSingleUse(mRoomSingleUseCase);
    }

    public static Intent getCallingIntent(Activity activity) {
        return new Intent(activity, AuditCardActivity.class);
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    private void prefillIssues(ArrayList<Category> categoryList, AuditSaveObject auditSaveObject) {
        Observable observable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                AuditUtilsV3.prefillSavedIssues(categoryList, auditSaveObject);
                subscriber.onCompleted();
            }
        });
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer() {
                    @Override
                    public void onCompleted() {
                        mAuditPresenter.onCreate();
                        prefillProgressBar();
                        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_RESUMED,
                                new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                                        .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mRoomName)
                                        .putValue(SegmentAnalyticsManager.COMPLETION_RATIO, String.valueOf(AuditUtilsV3.getAuditedCategoryCount(mCategoryList)) + "/" + mCategoryList.size()));

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {

                    }
                });
    }

    @Override
    public void viewLoadPager() {
        mViewpager.setClipToPadding(false);
        mViewpager.setPageMargin(50);
        mCardPagerAdapter = new AuditCardPagerAdapter(mCategoryList, this, this, mSubsription);
        mViewpager.setAdapter(mCardPagerAdapter);
        mCardPagerAdapter.notifyDataSetChanged();

        mSelectAreasAdapter = new SelectAreasAdapter(mCategoryList, this, this);
        mSelectAreasRecyclerView.setAdapter(mSelectAreasAdapter);
        mSelectAreasAdapter.notifyItemRangeChanged(0, mCategoryList.size());

        if (AuditUtilsV3.getNonAuditDoneCategories(mCategoryList).size() == 0)
            mSelectAreaView.setVisibility(VISIBLE);
        else
            mSelectAreaView.setVisibility(View.GONE);
    }


    @Override
    public void setToolbarInfo() {
        if (!TextUtils.isEmpty(mRoomName) && !"CA".equals(mRoomName)) {//check if rooom audit
            mRoomNumberTitle.setText(mRoomName);
        } else {//check which area audit this is
            mRoomNumberTitle.setVisibility(View.GONE);
            mPeriodicAuditToolbarIcon.setImageDrawable(IssueFormatUtils.getTintedIconForAuditType(this, mAuditType, R.color.warm_grey_2));
            mPeriodicAuditToolbarIcon.setVisibility(VISIBLE);
        }
        mAuditProgressBar.setProgress(0);
        mAuditProgressStatus.setText("0/" + mCategoryList.size());
    }

    @Override
    public void toggleSelectAreasVisibility() {
        if (mSelectAreaView != null) {
            if (mSelectAreaView.getVisibility() == VISIBLE) {
                mSelectAreaView.setVisibility(View.GONE);
            } else {
                mSelectAreaView.setVisibility(VISIBLE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        mAuditPresenter.onBackPressed();
    }

    @Override
    public void backPressClicked() {
        int auditDone = AuditUtilsV3.getAuditedCategoryCount(mCategoryList);
        if (mSelectAreaView.getVisibility() == VISIBLE && mCategoryList.size() > 0) {
            if (mCategoryList.size() - auditDone == 0) {
                ArrayList<AnswerV3> resolvableIssueList =
                        getResolvableIssueList(mAuditPrefManagerV3.getAllIssuesRaisedInARoom(mAuditType, mHotelId, mRoomName));
                if (resolvableIssueList.size() > 0)
                    startResolvableIssuesActivity(resolvableIssueList);
                else
                    startConfirmActivity();
            }
            mSelectAreaView.setVisibility(View.GONE);
        } else if (mCardLevel2View.getVisibility() == VISIBLE)
            mCardLevel2View.setVisibility(View.GONE);
        else {
            String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditType, mRoomName);
            mAuditPrefManagerV3.savePreMarkedIssues(key, mPreExistingIssuesInRoom);
            if (auditDone > 0)
                showPauseAuditDialog();
            else
                finish();
        }
    }

    @Override
    public void onLoadRoomAudit(RoomSingleResponse response) {
        mCategoryList = AuditUtilsV3.checkpointList(response.getRoomSingleData().getCheckpointList());
        mPreExistingIssuesInRoom = response.getRoomSingleData().getIssuesList();
        mCategoryList = AuditUtilsV3.preMarkIssuesOnCheckpoints(mCategoryList, response.getRoomSingleData().getIssuesList());
        mAuditProgressBar.setMax(mCategoryList.size());
        mRoomId = response.getRoomSingleData().getRoomId();
        mAuditSavedObject = mAuditPrefManagerV3.getSavedAudit(AuditUtilsV3.createAuditKey(mHotelId, mAuditType, mRoomName));
        mAuditPrefManagerV3.cacheCheckpoints(AuditUtilsV3.createSavedCheckPointKey(mHotelId, mAuditType, mRoomName),
                response.getRoomSingleData().getCheckpointList());
        mAuditPrefManagerV3.setCurrentRoomId(mRoomId);
        if (mAuditSavedObject != null) {
            prefillIssues(mCategoryList, mAuditSavedObject);
        } else {
            mAuditPresenter.onCreate();
        }

    }

    @Override
    public void onLoadRoomAuditFailure(String message) {
        mErrorView.setVisibility(VISIBLE);
    }

    private void showPauseAuditDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_pause_audit, null);
        TreeboTextView dialogText = (TreeboTextView) dialogView.findViewById(R.id.pause_dialog_text);
        dialogText.setText(getString(R.string.pause_dialog_text, mRoomNumberTitle.getText()));
        dialogBuilder.setView(dialogView);
        final AlertDialog pauseAuditDialog = dialogBuilder.create();
        pauseAuditDialog.show();

        TreeboTextView notNowBtn = (TreeboTextView) dialogView.findViewById(R.id.not_now_btn);
        notNowBtn.setOnClickListener(view -> pauseAuditDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            pauseAuditDialog.dismiss();
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_PAUSED,
                    new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                            .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mRoomName)
                            .putValue(SegmentAnalyticsManager.COMPLETION_RATIO, String.valueOf(AuditUtilsV3.getAuditedCategoryCount(mCategoryList)) + "/" + mCategoryList.size()));
            Intent intent = new Intent(this, DashboardActivity.class);
            intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
            intent.putExtra(Utils.HOTEL_ID, mHotelId);
            intent.putExtra(Utils.HOTEL_NAME, mHotelName);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        });

    }

    private void showUnmarkIssueDialog(boolean isPremarkedIssue, Checkpoint checkpoint) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_unmark_issues, null);
        TreeboTextView titleText = (TreeboTextView) dialogView.findViewById(R.id.checkpoint_name_tv);
        titleText.setText(checkpoint.getmDescription());
        TreeboTextView subtitleText = (TreeboTextView) dialogView.findViewById(R.id.unmark_text_tv);
        subtitleText.setText(isPremarkedIssue ? getString(R.string.cant_unmark_this_issue) : getString(R.string.unmark_issue));
        if (isPremarkedIssue) {
            subtitleText.setTypeface(TypeFaceUtils.get(this, "Roboto-Italic.ttf"));
        }

        dialogBuilder.setView(dialogView);
        final AlertDialog unmarkIssueDialog = dialogBuilder.create();
        unmarkIssueDialog.show();

        View closeBtn = dialogView.findViewById(R.id.close_iv);
        closeBtn.setOnClickListener(view -> unmarkIssueDialog.dismiss());
        subtitleText.setOnClickListener(view -> {
            if (!isPremarkedIssue) {
                String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditType, mRoomName);
                mAuditPrefManagerV3.removeCheckPointFromSavedList(key, checkpoint);
                updateCategoryList(false);
                mCheckpointClicked.setIssue(false);
                if (mAuditPrefManagerV3.getAllIssuesRaisedInARoom(mAuditType, mHotelId, mRoomName).size() == 0)
                    mAuditPrefManagerV3.clearCurrentAuditKey();
                View cardView = ((AuditCardPagerAdapter) mViewpager.getAdapter()).getCurrentView();
                if (cardView != null) {
                    RecyclerView checkpointRv = (RecyclerView) cardView.findViewById(R.id.audit_card_checkbox_list);
                    ((AuditCheckpointAdapter) checkpointRv.getAdapter()).refreshItem(mCheckpointClicked);
                    TreeboButton treeboBtn = (TreeboButton) cardView.findViewById(R.id.audit_done_btn);
                    if (checkpointRv.getAdapter().getItemCount() >= VISIBLE_CARD_NUMBER)
                        cardView.findViewById(R.id.ic_show_more).setVisibility(VISIBLE);
                    treeboBtn.setText(getString(R.string.everything_else_is_fine));
                }
            }
            unmarkIssueDialog.dismiss();
        });

    }

    @Override
    public void selectAreasClicked(Category category) {
        int index = AuditUtilsV3.getIndexOfCardCategory(category.getCategoryId(), mCategoryList);
        if (category.isAuditDone()) {
            category.setAuditDone(false);
            mCardPagerAdapter.addCard(mCategoryList.get(index));
            mCategoryList.set(index, category);
            mSelectAreasAdapter.updateItemInList(category);
            updateCategoryFine(category, false);
            prefillProgressBar();
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SELECT_AREA_SELECTED,
                    new Properties().putValue(SegmentAnalyticsManager.CATEGORY_SELECTED, category.getDescription()));
        }
        mViewpager.setCurrentItem(index - AuditUtilsV3.getAuditedCategoryCount(mCategoryList));
        mSelectAreaView.setVisibility(View.GONE);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }


    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }


    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void checkPointClicked(Checkpoint checkpoint) {
        mCheckpointClicked = checkpoint;
        setUpLevel2CardData(checkpoint);
        mCardLevel2Title.setText(checkpoint.getmDescription());
        mCardLevel2View.setVisibility(VISIBLE);
    }

    @Override
    public void checkPointLongPress(Checkpoint checkpoint) {
        if (checkpoint.issue()) {
            mCheckpointClicked = checkpoint;
            showUnmarkIssueDialog(checkpoint.isPendingIssue(), checkpoint);
        }
    }

    @Override
    public void updateProgressBar(Category category) {
        int index = AuditUtilsV3.getIndexOfCardCategory(category.getCategoryId(), mCategoryList);
        category.setAuditDone(true);
        mCategoryList.set(index, category);
        int cardsDone = AuditUtilsV3.getAuditedCategoryCount(mCategoryList);
        mAuditProgressBar.setProgress(cardsDone);
        mAuditProgressStatus.setText(String.valueOf(cardsDone) +
                "/" + mCategoryList.size());

        mSelectAreasAdapter.updateItemInList(category);
        if (mCategoryList.size() - cardsDone == 0) {
            ArrayList<AnswerV3> resolvableIssueList =
                    getResolvableIssueList(mAuditPrefManagerV3.getAllIssuesRaisedInARoom(mAuditType, mHotelId, mRoomName));
            if (resolvableIssueList.size() > 0)
                startResolvableIssuesActivity(resolvableIssueList);
            else
                startConfirmActivity();
        }
    }

    public void prefillProgressBar() {
        int cardsDone = AuditUtilsV3.getAuditedCategoryCount(mCategoryList);
        mAuditProgressBar.setProgress(cardsDone);
        mAuditProgressStatus.setText(String.valueOf(cardsDone) +
                "/" + mCategoryList.size());
    }

    @Override
    public void updateCategoryFine(Category category, boolean isAuditDone) {
        String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditType, mRoomName);

        mAuditPrefManagerV3.setCurrentAuditKey(key);
        if (isAuditDone) {
            AnswerV3 allFineCategory = new AnswerV3(category.getCategoryId(), true);
            mAuditPrefManagerV3.saveAuditCheckPoint(key, allFineCategory);
        } else {
            AnswerV3 allFineCategory = new AnswerV3(category.getCategoryId(), true);
            mAuditPrefManagerV3.removeAuditDoneCheckPoint(key, allFineCategory);
        }
        mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.CATEGORY_DONE));
    }

    private ArrayList<AnswerV3> getResolvableIssueList(ArrayList<AnswerV3> allIssues) {
        ArrayList<AnswerV3> resolvableIssues = new ArrayList<>();
        for (AnswerV3 issue : allIssues)
            if (!issue.getIs_instantly_resolved() && issue.getTat() <= Constants.RESOLVABLE_TAT) {
                resolvableIssues.add(issue);
            }
        return resolvableIssues;
    }

    private void setUpLevel2CardData(Checkpoint checkpoint) {
        ArrayList<SubCheckpoint> subCheckpointList = getSubCheckpointListFromDB(checkpoint.getmCheckpointId());
        if (checkpoint.issue()) {
            for (int i = 0; i < subCheckpointList.size(); i++) {
                SubCheckpoint sb = subCheckpointList.get(i);
                AuditSaveObject object = mAuditPrefManagerV3.getSavedAudit(AuditUtilsV3.createAuditKey(mHotelId, mAuditType, mRoomName));
                if (object != null && object.mAnswerList != null) {
                    for (AnswerV3 issue : object.mAnswerList) {
                        if (issue.getSubcheckpoint_id() != null && issue.getSubcheckpoint_id() == sb.getmSubCheckpointId() && !issue.isAuditDone) {
                            subCheckpointList.get(i).setIssue(true);
                            subCheckpointList.get(i).setIssueText(issue.getComment());
                            subCheckpointList.get(i).setPhotoUrls(issue.getImage_url());
                        }
                    }
                }
            }
        }
        subCheckpointList = AuditUtilsV3.preMarkIssuesOnSubCheckpoints(subCheckpointList, mPreExistingIssuesInRoom);
        AuditSubcheckpointAdapter adapter = new AuditSubcheckpointAdapter(this, subCheckpointList, this);
        mCardLevel2RecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void subcheckPointClicked(SubCheckpoint subCheckpoint, int position) {
        mSubCheckpointSelected = subCheckpoint;
        if (subCheckpoint.isPendingIssue()) {
            IssueListModelV3 issue = subCheckpoint.getPendingIssueDetails();
            mNavigator.navigateToIssueDetailsActivity(this, issue, mHotelId,
                    REQUEST_PRE_MARKED_ISSUE, mTaskLogID, true);
        } else {
            if (subCheckpoint.isCommentRequired()) {
                Intent intent = new Intent(this, AddCommentActivityNew.class);
                intent.putExtra(Utils.AUDIT_BAD_TITLE, subCheckpoint.getmDescription());
                intent.putExtra(Utils.CHECKPOINT_POSITION, position);
                intent.putExtra(Utils.ISSUE_TEXT, subCheckpoint.getIssueText());
                intent.putExtra(Utils.PHOTO_URL_LIST, subCheckpoint.getPhotoUrls());
                startActivityForResult(intent, REQUEST_ADD_COMMENT);
            } else {
                markIssueAndCreateIssueObject(position, "", "");
            }
        }
    }

    private void markIssueAndCreateIssueObject(int position, String issueText, String imageUrlList) {
        updateCategoryList(true);
        mSubCheckpointSelected.setIssue(true);
        mCheckpointClicked.setIssue(true);
        mCheckpointClicked.setIssueText(mSubCheckpointSelected.getmDescription());
        int categoryId = mCheckpointClicked.getmCategoryId();
        View cardView = ((AuditCardPagerAdapter) mViewpager.getAdapter()).getCurrentView();
        if (cardView != null) {
            RecyclerView checkpointRv = (RecyclerView) cardView.findViewById(R.id.audit_card_checkbox_list);
            ((AuditCheckpointAdapter) checkpointRv.getAdapter()).refreshItem(mCheckpointClicked);
            TreeboButton treeboBtn = (TreeboButton) cardView.findViewById(R.id.audit_done_btn);
            if (checkpointRv.getAdapter().getItemCount() >= VISIBLE_CARD_NUMBER)
                cardView.findViewById(R.id.ic_show_more).setVisibility(VISIBLE);
            treeboBtn.setText(getString(R.string.everything_else_is_fine));
        }
        AnswerV3 issueRaisedObject = new AnswerV3(categoryId, mCheckpointClicked.getmCheckpointId(),
                mSubCheckpointSelected.getmSubCheckpointId(), issueText, imageUrlList, false);
        issueRaisedObject.setCheckpointText(mCheckpointClicked.getmDescription());
        issueRaisedObject.setSubcheckpointText(mSubCheckpointSelected.getmDescription());
        issueRaisedObject.setCategoryText(DatabaseUtils.getCategoryText(mCheckpointClicked.getmCategoryId()));
        issueRaisedObject.setCreatedAt(mCurrentDate);
        issueRaisedObject.setTat(mSubCheckpointSelected.getmTurnaroundTime());
        String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditType, mRoomName);
        mAuditPrefManagerV3.setCurrentAuditKey(key);
        mAuditPrefManagerV3.saveAuditCheckPoint(key, issueRaisedObject);
        mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.ISSUE_CREATED));
        mCardLevel2RecyclerView.getAdapter().notifyItemChanged(position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ADD_COMMENT:
                if (resultCode == RESULT_OK) {
                    // Update Checkpoint UI
                    String issueText = data.getStringExtra(Utils.ISSUES_TEXT);
                    String imageUrlList = data.getStringExtra(Utils.PHOTO_URL_LIST);
                    int position = data.getIntExtra(Utils.CHECKPOINT_POSITION, -1);
                    if (position != -1) {
                        markIssueAndCreateIssueObject(position, issueText, imageUrlList);
                        mCardLevel2View.setVisibility(View.GONE);
                    }
                }
                break;

            case REQUEST_RESOLVABLE_ISSUES:
                if (resultCode == RESULT_OK) {
                    startConfirmActivity();
                } else {
                    mSelectAreaView.setVisibility(VISIBLE);
                }
                break;

            case REQUEST_CONFIRM_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    if(!mIsFromAuditAnyHotel){
                        if (AuditCategory.isPeriodicAudit(mAuditType)) {
                            Intent intent = new Intent(this, DashboardActivity.class);
                            intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
                            intent.putExtra(Utils.HOTEL_ID, mHotelId);
                            intent.putExtra(Utils.HOTEL_NAME, mHotelName);
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                        } else if (isExtraRoomAudit) {
                            Intent intent2 = new Intent(this, RoomPickerActivity.class);
                            intent2.putExtra(Utils.ROOM_NAME, mRoomName);
                            intent2.putExtra(Constants.AUDIT_TYPE, mAuditType);
                            intent2.putExtra(Utils.HOTEL_NAME, mHotelName);
                            intent2.putExtra(Utils.HOTEL_ID, mHotelId);
                            intent2.putExtra(Constants.TASK_MODEL, mTaskModel);
                            intent2.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
                            intent2.setFlags(FLAG_ACTIVITY_SINGLE_TOP | FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent2);
                        } else {
                            Intent intent1 = new Intent(this, DailyAuditActivity.class);
                            intent1.putExtra(Utils.ROOM_NAME, mRoomName);
                            intent1.putExtra(Constants.AUDIT_TYPE, mAuditType);
                            intent1.putExtra(Utils.HOTEL_NAME, mHotelName);
                            intent1.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
                            intent1.putExtra(Constants.TASK_MODEL, mTaskModel);
                            intent1.setFlags(FLAG_ACTIVITY_SINGLE_TOP | FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent1);
                        }
                    }
                    finish();
                } else {
                    fromConfirmActivity = true;
                    mSelectAreaView.setVisibility(VISIBLE);
                }
                break;

            case REQUEST_PRE_MARKED_ISSUE:
                if (resultCode == Utils.RESULT_RESOLVE_ISSUE) {
                    updateCategoryList(false);
                    mSubCheckpointSelected.setIssue(false);
                    mSubCheckpointSelected.setPendingIssue(false);
                    mPreExistingIssuesInRoom = AuditUtilsV3.removeResolvedIssueFromIssueList(mSubCheckpointSelected, mPreExistingIssuesInRoom);
                    if (isCheckpointFreeOfPreExistingIssues(mCheckpointClicked)) {
                        mCheckpointClicked.setPendingIssue(false);
                    }
                    View cardView = ((AuditCardPagerAdapter) mViewpager.getAdapter()).getCurrentView();
                    if (cardView != null) {
                        RecyclerView checkpointRv = (RecyclerView) cardView.findViewById(R.id.audit_card_checkbox_list);
                        ((AuditCheckpointAdapter) checkpointRv.getAdapter()).refreshItem(mCheckpointClicked);
                        TreeboButton treeboBtn = (TreeboButton) cardView.findViewById(R.id.audit_done_btn);
                        if (checkpointRv.getAdapter().getItemCount() >= VISIBLE_CARD_NUMBER)
                            cardView.findViewById(R.id.ic_show_more).setVisibility(VISIBLE);
                        treeboBtn.setText(getString(R.string.everything_else_is_fine));
                    }
                    setUpLevel2CardData(mCheckpointClicked);
                }
                break;

        }
    }

    private boolean isCheckpointFreeOfPreExistingIssues(Checkpoint checkpoint) {
        if (mPreExistingIssuesInRoom != null) {
            for (IssueListModelV3 issue : mPreExistingIssuesInRoom) {
                if (issue.getCheckpointID() == checkpoint.mCheckpointId)
                    return false;
            }
        }
        return true;
    }

    private void updateCategoryList(boolean set) {
        if (mCheckpointClicked != null) {
            int cardIndex = AuditUtilsV3.getIndexOfCardCategory(mCheckpointClicked.getmCategoryId(), mCategoryList);
            int checkPointIndexInCategory = AuditUtilsV3.getIndexOfCheckpointInCategory(mCheckpointClicked.getmCheckpointId(),
                    mCategoryList.get(cardIndex).getCheckpoints());
            mCategoryList.get(cardIndex).setIssueRaised(set);
            mCategoryList.get(cardIndex).getmCheckPoints().get(checkPointIndexInCategory).setIssue(set);
            mSelectAreasAdapter.updateItemInList(mCategoryList.get(cardIndex));
        }
    }

    private ArrayList<SubCheckpoint> getSubCheckpointListFromDB(int checkpointId) {
        return new ArrayList<>(SQLite.select().from(SubCheckpoint.class).where(SubCheckpoint_Table.mCheckpointId_mCheckpointId.eq(checkpointId)).queryList());
    }


    private void startResolvableIssuesActivity(ArrayList<AnswerV3> issueList) {
        Intent intent = new Intent(this, ResolvableIssuesActivity.class);
        intent.putExtra(Constants.ROOM_ID, mRoomName);
        intent.putParcelableArrayListExtra(Utils.ISSUES_LIST, issueList);
        intent.putExtra(Constants.AUDIT_TYPE, mAuditType);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        intent.putExtra(Utils.HOTEL_ID, mHotelId);
        startActivityForResult(intent, REQUEST_RESOLVABLE_ISSUES);
    }

    private void startConfirmActivity() {
        Intent intent = new Intent(this, ConfirmActionActivity.class);
        intent.putExtra(Constants.AUDIT_SUBMISSION, true);
        intent.putParcelableArrayListExtra(Constants.ISSUE_LIST,
                mAuditPrefManagerV3.getAllIssuesRaisedInARoom(mAuditType, mHotelId, mRoomName));
        intent.putExtra(Constants.ROOM_ID, mRoomId);
        intent.putExtra(Utils.ROOM_NAME, mRoomName);
        intent.putExtra(Utils.HOTEL_ID, mHotelId);
        intent.putExtra(Constants.TASK_MODEL, mTaskModel);
        intent.putExtra(Utils.ROOM_TYPE_ID, mRoomTypeID);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        intent.putExtra(Constants.AUDIT_TYPE, mAuditType);
        intent.putExtra(Utils.START_TIME,mCurrentDateTime);
        intent.putExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL,mIsFromAuditAnyHotel);
        startActivityForResult(intent, REQUEST_CONFIRM_ACTIVITY);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubsription);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(Category.INCLUDED_CATEGORY_LIST, mCategoryList);
        outState.putString(Utils.ROOM_NAME, mRoomName);
        outState.putBoolean(Utils.IS_IN_HOTEL, isInHotelLocation);
        outState.putString(Utils.HOTEL_NAME, mHotelName);
        outState.putInt(Constants.ROOM_ID, mRoomId);
        outState.putInt(AuditUtilsV3.AUDIT_TYPE, mAuditType);
        outState.putInt(Utils.ROOM_TYPE_ID, mRoomTypeID);
        outState.putParcelable(Constants.TASK_MODEL, mTaskModel);
        outState.putBoolean(Constants.IS_RESTART_AUDIT, isRestart);
        outState.putParcelableArrayList(Utils.PRE_EXISTING_ISSUES, mPreExistingIssuesInRoom);
        outState.putInt(Utils.HOTEL_ID, mHotelId);
        outState.putParcelable(Utils.CLICKED_CHECKPOINT, mCheckpointClicked);
        outState.putParcelable(Utils.CLICKED_SUBCHECKPOINT, mSubCheckpointSelected);
        super.onSaveInstanceState(outState);
    }

}

