package com.treebo.prowlapp.flowauditnew.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 28/10/16.
 */

public class BaseRoomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<RoomV3Object> mRoomList = new ArrayList<>();
    private onRoomClickListener mAuditRoomListener;

    private boolean isFromSelectRoom;

    public BaseRoomAdapter(onRoomClickListener roomClickListener,
                           @NonNull ArrayList<RoomV3Object> rooms,
                           boolean fromSelectRoom) {
        mRoomList = rooms;
        mAuditRoomListener = roomClickListener;
        isFromSelectRoom = fromSelectRoom;
    }

    public void setData(boolean reset, ArrayList<RoomV3Object> newRooms) {
        if (reset) {
            mRoomList.clear();
        }
        mRoomList.addAll(newRooms);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_room,
                parent, false);
        return new BaseRoomHolder(view, mAuditRoomListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // Set fields as per model
        RoomV3Object room = mRoomList.get(position);
        ((BaseRoomHolder) holder).room = room;
        ((BaseRoomHolder) holder).mRoomNumber.setText(room.getRoomNumber());
        ((BaseRoomHolder) holder).mRoomType.setText(room.getRoomType());
        ((BaseRoomHolder) holder).mRoomStatus.setText(room.getDescription());
    }

    @Override
    public int getItemCount() {
        return mRoomList.size();
    }

    //show overflow button based on if recommended or available rooms
    private class BaseRoomHolder extends RecyclerView.ViewHolder {

        RoomV3Object room;
        TreeboTextView mRoomNumber;
        TreeboTextView mRoomType;
        TreeboTextView mRoomStatus;

        BaseRoomHolder(View itemView, onRoomClickListener listener) {
            super(itemView);
            mRoomNumber = (TreeboTextView) itemView.findViewById(R.id.room_number_tv);
            mRoomType = (TreeboTextView) itemView.findViewById(R.id.room_type_tv);
            mRoomStatus = (TreeboTextView) itemView.findViewById(R.id.room_status_tv);
            itemView.setOnClickListener(view -> listener.onAuditRoomClicked(room));
        }
    }

    public interface onRoomClickListener {
        void onAuditRoomClicked(RoomV3Object room);
    }

}
