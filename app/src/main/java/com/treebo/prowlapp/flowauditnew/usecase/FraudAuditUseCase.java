package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.FraudAuditResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 17/01/17.
 */

public class FraudAuditUseCase extends UseCase<FraudAuditResponse> {

    private RestClient mRestClient;
    private int mHotelId;

    public FraudAuditUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setFields(int hotelId) {
        mHotelId = hotelId;
    }

    @Override
    protected Observable<FraudAuditResponse> getObservable() {
        return mRestClient.getFraudAuditCheckpoints(mHotelId);
    }
}
