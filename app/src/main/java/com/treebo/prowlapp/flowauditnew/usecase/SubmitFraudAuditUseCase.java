package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.Models.auditmodel.FraudAuditSubmitObject;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.FraudAuditSubmitResponse;
import com.treebo.prowlapp.usecase.UseCase;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by abhisheknair on 20/01/17.
 */

public class SubmitFraudAuditUseCase extends UseCase<FraudAuditSubmitResponse> {

    private RestClient mRestClient;

    private int mHotelID;
    private int mUserID;
    private int mTaskLogID;
    private ArrayList<FraudAuditSubmitObject> mFraudList;

    public SubmitFraudAuditUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setFields(int hotelID, int userID, int taskLogID, ArrayList<FraudAuditSubmitObject> list) {
        this.mHotelID = hotelID;
        this.mUserID = userID;
        this.mTaskLogID = taskLogID;
        this.mFraudList = list;
    }


    @Override
    protected Observable<FraudAuditSubmitResponse> getObservable() {
        return mRestClient.submitFraudAudit(mFraudList, mHotelID, mUserID, mTaskLogID);
    }
}
