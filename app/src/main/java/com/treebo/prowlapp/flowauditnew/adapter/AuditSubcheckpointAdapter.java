package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.List;

/**
 * Created by abhisheknair on 08/11/16.
 */

public class AuditSubcheckpointAdapter extends RecyclerView.Adapter<AuditSubcheckpointAdapter.AuditSubcheckpointViewHolder> {

    private List<SubCheckpoint> mCheckPointList;
    private onSubcheckPointClickListener mListener;
    private Context mContext;

    public AuditSubcheckpointAdapter(Context context, List<SubCheckpoint> list, onSubcheckPointClickListener listener) {
        mContext = context;
        mCheckPointList = list;
        mListener = listener;
    }

    @Override
    public AuditSubcheckpointViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_subcheckpoint,
                parent, false);
        return new AuditSubcheckpointViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(AuditSubcheckpointViewHolder holder, int position) {
        SubCheckpoint checkPoint = mCheckPointList.get(position);
        holder.subCheckpoint = checkPoint;
        holder.text.setText(checkPoint.getmDescription());
        holder.pendingFlag.setVisibility(checkPoint.isPendingIssue() ? View.VISIBLE : View.GONE);
        if (checkPoint.issue() || checkPoint.isPendingIssue()) {
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(mContext,
                    R.drawable.ic_red_cross_filled));
        } else
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(mContext,
                    R.drawable.ic_hollow_circle_green));
    }

    @Override
    public int getItemCount() {
        return mCheckPointList.size();
    }

    static class AuditSubcheckpointViewHolder extends RecyclerView.ViewHolder {

        private SubCheckpoint subCheckpoint;
        private TreeboTextView text;
        private ImageView checkBox;
        private TreeboTextView pendingFlag;

        AuditSubcheckpointViewHolder(View itemView, onSubcheckPointClickListener listener) {
            super(itemView);
            text = (TreeboTextView) itemView.findViewById(R.id.audit_subcheckpoint_title);
            checkBox = (ImageView) itemView.findViewById(R.id.audit_subheckpoint_checkbox);
            checkBox.setOnClickListener(view -> listener.subcheckPointClicked(subCheckpoint, getAdapterPosition()));
            pendingFlag = (TreeboTextView) itemView.findViewById(R.id.pending_issue_flag);
            itemView.setOnClickListener(view -> {
                listener.subcheckPointClicked(subCheckpoint, getAdapterPosition());
            });
        }
    }

    public interface onSubcheckPointClickListener {
        void subcheckPointClicked(SubCheckpoint subcheckPointId, int position);
    }
}
