package com.treebo.prowlapp.flowauditnew;

import android.content.Context;

import com.treebo.prowlapp.R;

/**
 * Created by sumandas on 06/11/2016.
 */

public class StartAuditActionStrategy implements ActionStrategy {

    private Context mContext;

    public StartAuditActionStrategy(Context context){
        mContext=context;
    }

    @Override
    public String showTitle() {
        return mContext.getString(R.string.confirm_start_audit);
    }

    @Override
    public void onTakeAction() {

    }

    @Override
    public void onCancelAction() {

    }
}
