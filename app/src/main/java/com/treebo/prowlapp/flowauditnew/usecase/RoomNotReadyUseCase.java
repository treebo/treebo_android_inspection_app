package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by abhisheknair on 07/12/16.
 */

public class RoomNotReadyUseCase extends UseCase<BaseResponse> {

    RestClient mRestClient;

    private String mReason;
    private int mRoomID;

    public RoomNotReadyUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setRoomIDAndReason(int roomID, String reason) {
        this.mRoomID = roomID;
        this.mReason = reason;
    }

    public int getRoomID() {
        return this.mRoomID;
    }

    @Override
    protected Observable<BaseResponse> getObservable() {
        return mRestClient.postRoomNotReady(mRoomID, mReason);
    }
}
