package com.treebo.prowlapp.flowauditnew.observers;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.usecase.RoomListUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;

/**
 * Created by sumandas on 10/11/2016.
 */

public class RoomListObserver extends BaseObserver<RoomListResponse> {

    private AuditContract.IDailyAuditView mDailyAuditView;
    private AuditContract.IRoomListView mRoomListView;
    private boolean isAuditActivity;

    public RoomListObserver(BasePresenter presenter, RoomListUseCase roomListUseCase, AuditContract.IDailyAuditView view) {
        super(presenter, roomListUseCase);
        mDailyAuditView = view;
        isAuditActivity = true;
    }

    public RoomListObserver(BasePresenter presenter, RoomListUseCase roomListUseCase, AuditContract.IRoomListView view) {
        super(presenter, roomListUseCase);
        mRoomListView = view;
    }

    @Override
    public void onCompleted() {
        if (isAuditActivity) {
            mDailyAuditView.hideLoading();
        } else {
            mRoomListView.hideLoading();
        }
    }

    @Override
    public void onNext(RoomListResponse roomListResponse) {
        if (roomListResponse.status.equals("success")) {
            if (isAuditActivity) {
                mDailyAuditView.hideLoading();
                mDailyAuditView.onLoadRoomList(roomListResponse);
            } else {
                mRoomListView.hideLoading();
                mRoomListView.onLoadRoomList(roomListResponse);
            }
        } else {
            if (isAuditActivity) {
                mDailyAuditView.hideLoading();
                mDailyAuditView.onLoadRoomListFailure("Loading room checklist failed");
            } else {
                mRoomListView.hideLoading();
                mRoomListView.onLoadRoomListFailure("Loading room checklist failed");
            }
        }
    }

}
