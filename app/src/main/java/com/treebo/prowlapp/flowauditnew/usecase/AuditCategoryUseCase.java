package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by sumandas on 20/01/2017.
 */

public class AuditCategoryUseCase extends UseCase<AuditAreaResponse> {

    RestClient mRestClient;

    private int mHotelId;

    private int mAuditId;

    private int mUserId;
    private String mauditSubmissionType="";

    public AuditCategoryUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setFields(int hotelId,int auditId, int userId) {
        mHotelId = hotelId;
        mAuditId = auditId;
        mUserId = userId;
    }

    public void setFields(int hotelId,int auditId, int userId,String auditSubmissionType) {
        mHotelId = hotelId;
        mAuditId = auditId;
        mUserId = userId;
        mauditSubmissionType=auditSubmissionType;
    }

    public void setFields(int hotelId,int auditId) {
        mHotelId = hotelId;
        mAuditId = auditId;
        mUserId = -1;
    }

    @Override
    protected Observable<AuditAreaResponse> getObservable() {
        if(mauditSubmissionType.equals("Remote"))
            return mRestClient.getAuditTypePeriodicRemote(mHotelId,mAuditId,mUserId,mauditSubmissionType);
        else
            return mRestClient.getAuditTypePeriodic(mHotelId,mAuditId,mUserId);
    }
}

