package com.treebo.prowlapp.flowauditnew.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.flowauditnew.AuditNewComponent;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.DaggerAuditNewComponent;
import com.treebo.prowlapp.flowauditnew.ResolvableIssuesActivityContract;
import com.treebo.prowlapp.flowauditnew.adapter.ResolvableIssuesAdapter;
import com.treebo.prowlapp.flowauditnew.presenter.ResolvableIssuesPresenter;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.issuemodel.SectionedResolvableIssuesModel;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.DateUtils;
import com.treebo.prowlapp.Utils.IssueFormatUtils;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.subscriptions.CompositeSubscription;

import static android.view.View.VISIBLE;

/**
 * Created by abhisheknair on 08/11/16.
 */

public class ResolvableIssuesActivity extends AppCompatActivity
        implements ResolvableIssuesActivityContract.IResolvableIssuesView, ResolvableIssuesAdapter.onResolveBtnClickListener {

    private RecyclerView mResolvableIssuesRecyclerView;


    @Inject
    public ResolvableIssuesPresenter mResolvableIssuesPresenter;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public AuditPrefManagerV3 mAuditManager;

    @Inject
    public RxBus mRxBus;

    CompositeSubscription mSubsription = new CompositeSubscription();

    private static final int REQUEST_CONFIRM_ACTIVITY = 120;

    private String mRoomNumber = "";
    private ArrayList<AnswerV3> mMasterIssueList;
    private ArrayList<AnswerV3> mDisplayIssueList;
    private ArrayList<SectionedResolvableIssuesModel> mSectionedList;
    private TreeboTextView mNoIssuesView;
    private int mAuditType;
    private int hotelId;
    private String mHotelName;
    private ResolvableIssuesAdapter mAdapter;
    private boolean isUndoClicked;
    private TreeboButton mSkipBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resolvable_issues);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }

        AuditNewComponent auditComponent = DaggerAuditNewComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectResolvableIssuesActivity(this);

        mRoomNumber = getIntent().getStringExtra(Constants.ROOM_ID);
        mMasterIssueList = getIntent().getParcelableArrayListExtra(Utils.ISSUES_LIST);
        mDisplayIssueList = mMasterIssueList;
        mAuditType = getIntent().getIntExtra(Constants.AUDIT_TYPE, -1);
        mHotelName = getIntent().getStringExtra(Utils.HOTEL_NAME);
        hotelId = getIntent().getIntExtra(Utils.HOTEL_ID, -1);

        getSectionedList();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        TreeboTextView roomTitle = (TreeboTextView) findViewById(R.id.toolbar_room_number_tv);
        ImageView toolbarIcon = (ImageView) findViewById(R.id.toolbar_room_number_iv);

        if (!TextUtils.isEmpty(mRoomNumber) && !"CA".equals(mRoomNumber)) {//check if rooom audit
            roomTitle.setText(mRoomNumber);
        } else {//check which area audit this is
            roomTitle.setVisibility(View.GONE);
            toolbarIcon.setImageDrawable(IssueFormatUtils.getTintedIconForAuditType(this, mAuditType, R.color.warm_grey_2));
            toolbarIcon.setVisibility(VISIBLE);
        }


        mNoIssuesView = (TreeboTextView) findViewById(R.id.no_resolvable_issues_raised_tv);
        mResolvableIssuesRecyclerView = (RecyclerView) findViewById(R.id.resolvable_issues_rv);
        setUpIssuesRecyclerView();

        mSkipBtn = (TreeboButton) findViewById(R.id.resolvable_issues_skip_btn);
        mSkipBtn.setOnClickListener(view -> mResolvableIssuesPresenter.onSkipClick());
        if (mMasterIssueList != null && mMasterIssueList.size() == 0) {
            mNoIssuesView.setVisibility(View.VISIBLE);
            mResolvableIssuesRecyclerView.setVisibility(View.GONE);
            (mSkipBtn).setText(getString(R.string.continue_string));
        }

        mSubsription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                ArrayList<HotelLocation> hotelList = ((HotelChangedEvent) event).mHotelList;
                if (hotelList.size() == 0) {
                    finish();
                }
            }
        }));

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubsription);
    }

    @Inject
    public void setUp() {
        mResolvableIssuesPresenter.setMvpView(this);
    }

    @Override
    public void setUpIssuesRecyclerView() {
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mResolvableIssuesRecyclerView.setLayoutManager(layoutManager1);
        mResolvableIssuesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        initSwipe();

        mAdapter = new ResolvableIssuesAdapter(this, mSectionedList, this);
        mResolvableIssuesRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishActivity(boolean isSkip) {
        if (!isSkip) {
            setResult(RESULT_CANCELED);
            finish();
        } else {
            Intent data = new Intent();
            data.putParcelableArrayListExtra(Utils.ISSUES_LIST, mMasterIssueList);
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONFIRM_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    finish();
                }
        }
    }

    @Override
    public void resolveBtnClicked(AnswerV3 issue) {
        int index = getIndexOfIssueInList(issue);
        issue.setIs_instantly_resolved(Boolean.TRUE);
        mAuditManager.updateAuditCheckPoint(AuditUtilsV3.createAuditKey(hotelId, mAuditType, mRoomNumber),
                issue);
        issue.isPendingDelete = true;
        mMasterIssueList.set(index, issue);
        getSectionedList();
        mAdapter.notifyDataSetChanged();
        new Handler().postDelayed(() -> {
            if (!isUndoClicked) {
                issue.isSoftDeleted = true;
                SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.ISSUE_RESOLVE,
                        new Properties().putValue(SegmentAnalyticsManager.SOURCE_SCREEN, SegmentAnalyticsManager.RESOLVABLE_ISSUES_SCREEN));
            } else {
                issue.isPendingDelete = false;
                issue.isSoftDeleted = false;
            }
            getSectionedList();
            mAdapter.notifyDataSetChanged();
            isUndoClicked = false;
        }, 2000);
    }

    @Override
    public void undoBtnClicked() {
        isUndoClicked = true;
    }

    private int getIndexOfIssueInList(AnswerV3 issue) {
        for (int i = 0; i < mMasterIssueList.size(); i++) {
            AnswerV3 issueRaised = mMasterIssueList.get(i);
            if (issueRaised.getSubcheckpoint_id().equals(issue.getSubcheckpoint_id()))
                return i;
        }
        return -1;
    }

    private void getSectionedList() {
        if (mSectionedList != null)
            mSectionedList.clear();
        else
            mSectionedList = new ArrayList<>();
        Set<String> sectionNames = getUniqueValues(mMasterIssueList);
        for (int i = 0; i < sectionNames.size(); i++) {
            ArrayList<AnswerV3> list = new ArrayList<>();
            SectionedResolvableIssuesModel model = new SectionedResolvableIssuesModel();
            String date = String.valueOf(sectionNames.toArray()[i]);
            model.setSectionName(DateUtils.getCreatedAtDateComparedToToday(date));
            for (AnswerV3 issue : mMasterIssueList) {
                if (issue.getCreatedAt().equals(date) && !issue.isSoftDeleted)
                    list.add(issue);
            }
            model.setIssueList(list);
            mSectionedList.add(model);
        }
        mDisplayIssueList.clear();
        for (SectionedResolvableIssuesModel model : mSectionedList) {
            mDisplayIssueList.addAll(model.getIssueList());
        }
        if (mDisplayIssueList.size() == 0) {
            mNoIssuesView.setText(getString(R.string.all_issues_resolved));
            mNoIssuesView.setVisibility(View.VISIBLE);
            mResolvableIssuesRecyclerView.setVisibility(View.GONE);
            mSkipBtn.setVisibility(View.INVISIBLE);
            new Handler().postDelayed(() -> {
                mResolvableIssuesPresenter.onSkipClick();
            }, 500);
        }
    }

    public static Set<String> getUniqueValues(ArrayList<AnswerV3> issueList) {
        Set<String> uniqueValues = new HashSet<String>();
        for (AnswerV3 issue : issueList) {
            uniqueValues.add(issue.getCreatedAt());
        }
        return uniqueValues;
    }

    private void initSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                AnswerV3 issue = mDisplayIssueList.get(viewHolder.getAdapterPosition() - 1);
                resolveBtnClicked(issue);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE && viewHolder.getItemViewType() == -1) {

                    Bitmap icon;
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    Paint p = new Paint();

                    if (dX < 0) {
                        p.setColor(ContextCompat.getColor(recyclerView.getContext(), R.color.green_color_primary));
                        c.drawRect((float) itemView.getRight() + dX, (float) itemView.getTop(),
                                (float) itemView.getRight(), (float) itemView.getBottom(), p);
                        // TODO Complete the logic
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mResolvableIssuesRecyclerView);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }
}
