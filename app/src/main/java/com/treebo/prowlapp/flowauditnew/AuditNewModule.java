package com.treebo.prowlapp.flowauditnew;

import com.treebo.prowlapp.flowauditnew.presenter.AuditCardPresenter;
import com.treebo.prowlapp.flowauditnew.presenter.AuditPeriodicPresenter;
import com.treebo.prowlapp.flowauditnew.presenter.AuditType2Presenter;
import com.treebo.prowlapp.flowauditnew.presenter.ConfirmActionPresenter;
import com.treebo.prowlapp.flowauditnew.presenter.DailyAuditPresenter;
import com.treebo.prowlapp.flowauditnew.presenter.FraudAuditPresenter;
import com.treebo.prowlapp.flowauditnew.presenter.ResolvableIssuesPresenter;
import com.treebo.prowlapp.flowauditnew.presenter.RoomListPresenter;
import com.treebo.prowlapp.flowauditnew.usecase.AuditCategoryUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.CommonUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.CreateIssueUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.FraudAuditUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomListUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomNotReadyUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomSingleUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitAuditV3UseCase;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitFraudAuditUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitPeriodicAuditUseCase;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 10/11/2016.
 */
@Module
public class AuditNewModule {

    @Provides
    RestClient providesRestClient() {
        return new RestClient();
    }

    @Provides
    AuditCardPresenter providesAuditCardPresenter() {
        return new AuditCardPresenter();
    }


    @Provides
    AuditType2Presenter providesAuditCardType2Presenter() {
        return new AuditType2Presenter();
    }

    @Provides
    DailyAuditPresenter providesDailyAuditPresenter() {
        return new DailyAuditPresenter();
    }

    @Provides
    RoomListPresenter providesRoomLisPresenter() {
        return new RoomListPresenter();
    }

    @Provides
    ConfirmActionPresenter providesConfirmActionPresenter() {
        return new ConfirmActionPresenter();
    }

    @Provides
    FraudAuditPresenter providesFraudAuditPresenter() {
        return new FraudAuditPresenter();
    }

    @Provides
    ResolvableIssuesPresenter providesResolvableIssuesPresenter() {
        return new ResolvableIssuesPresenter();
    }

    @Provides
    CommonUseCase providesCommonUseCase(RestClient restClient) {
        return new CommonUseCase(restClient);
    }

    @Provides
    RoomSingleUseCase providesRoomSingleUseCase(RestClient restClient) {
        return new RoomSingleUseCase(restClient);
    }

    @Provides
    RoomListUseCase providesRoomListUseCase(RestClient restClient) {
        return new RoomListUseCase(restClient);
    }

    @Provides
    RoomNotReadyUseCase providesRoomNotReadyUseCase(RestClient restClient) {
        return new RoomNotReadyUseCase(restClient);
    }

    @Provides
    SubmitAuditV3UseCase providesSubmitAuditUseCase(RestClient restClient) {
        return new SubmitAuditV3UseCase(restClient);
    }

    @Provides
    AuditCategoryUseCase providesAuditcategoryUseCase(RestClient restClient){
        return new AuditCategoryUseCase(restClient);
    }

    @Provides
    AuditPeriodicPresenter providesAuditPeriodicPresenter() {
        return new AuditPeriodicPresenter();
    }
    @Provides
    FraudAuditUseCase providesFraudAuditUseCase(RestClient restClient) {
        return new FraudAuditUseCase(restClient);
    }

    @Provides
    SubmitFraudAuditUseCase providesSubmitFraudUseCase(RestClient restClient) {
        return new SubmitFraudAuditUseCase(restClient);
    }

    @Provides
    SubmitPeriodicAuditUseCase providesSubmitPeriodicAuditUseCase(RestClient restClient) {
        return new SubmitPeriodicAuditUseCase(restClient);
    }

    @Provides
    CreateIssueUseCase providesCreateIssueUseCase(RestClient restClient) {
        return new CreateIssueUseCase(restClient);
    }

}
