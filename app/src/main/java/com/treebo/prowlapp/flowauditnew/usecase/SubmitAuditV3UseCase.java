package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.usecase.UseCase;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by sumandas on 11/11/2016.
 */

public class SubmitAuditV3UseCase extends UseCase<SubmitAuditResponse> {

    private RestClient mRestClient;

    private ArrayList<AnswerV3> mIssueList;
    private int mRoomID;
    private int mUserID;
    private int mAuditType;
    private int mHotelID;
    private int mTaskLogID;
    private String mCurrentDate;
    private String mSource;
    private String maudit_submission_type="";


    public SubmitAuditV3UseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setFields(ArrayList<AnswerV3> issueList, int roomId, int userId, int auditType,
                          int hotelId, String source, int taskLogID,String currentDate) {
        mIssueList = issueList;
        mRoomID = roomId;
        mUserID = userId;
        mAuditType = auditType;
        mUserID = userId;
        mHotelID = hotelId;
        mSource = source;
        mTaskLogID = taskLogID;
        mCurrentDate=currentDate;
    }

    public void setFields(ArrayList<AnswerV3> issueList, int roomId, int userId, int auditType,
                          int hotelId, String source, int taskLogID,String currentDate, String audit_submission_type) {
        mIssueList = issueList;
        mRoomID = roomId;
        mUserID = userId;
        mAuditType = auditType;
        mUserID = userId;
        mHotelID = hotelId;
        mSource = source;
        mTaskLogID = taskLogID;
        mCurrentDate=currentDate;
        maudit_submission_type = audit_submission_type;
    }

    @Override
    protected Observable<SubmitAuditResponse> getObservable() {
        return mRestClient.submitAuditV3(mIssueList, mRoomID, mUserID, mAuditType, mHotelID, mSource, mTaskLogID,mCurrentDate,maudit_submission_type);
    }
}
