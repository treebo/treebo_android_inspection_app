package com.treebo.prowlapp.flowauditnew.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.jakewharton.rxbinding.view.RxView;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AuditStateChangeEvent;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.AuditNewComponent;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.DaggerAuditNewComponent;
import com.treebo.prowlapp.flowauditnew.adapter.AuditDoneAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.BaseRoomAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.SelectRoomPopUpAdapter;
import com.treebo.prowlapp.flowauditnew.presenter.DailyAuditPresenter;
import com.treebo.prowlapp.flowauditnew.usecase.CommonUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomListUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.RoomNotReadyUseCase;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.AuditTypeMap;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.CompleteAudit;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 28/10/16.
 */

public class DailyAuditActivity extends AppCompatActivity implements AuditContract.IDailyAuditView,
        BaseRoomAdapter.onRoomClickListener, SelectRoomPopUpAdapter.optionClickListener {

    private static final int COMMON_ROOM_AUDIT_TYPE = 5;

    private int listRoomCount = 3;
    private int displayRoomCount = 3;
    private int mRoomsOnDisplayCount;

    private String mHotelName;
    private BaseRoomAdapter mAuditRoomAdapter;
    private AuditDoneAdapter mDoneAuditRoomAdapter;

    private RecyclerView mRoomsRecyclerView;
    private View mRootView;
    private View mExtraInformationView;

    private RecyclerView mDoneRoomsRecyclerView;
    private View mCommonAreaDoneCard;
    private View mCompletedText;
    private TreeboTextView mCompletedCountTv;

    private View mCommonAreaCard;

    private View mLoaderView;

    private ArrayList<RoomV3Object> mTotalRoomList;
    private ArrayList<RoomV3Object> mDisplayRoomList;

    private ArrayList<RoomV3Object> mDoneRoomList = new ArrayList<>();

    @Inject
    Navigator mNavigator;

    @Inject
    DailyAuditPresenter mDailyAuditPresenter;

    @Inject
    CommonUseCase mCommonUseCase;

    @Inject
    RoomListUseCase mRoomListUseCase;

    @Inject
    RoomNotReadyUseCase mRoomNotReadyUseCase;

    @Inject
    LoginSharedPrefManager mLoginManager;

    @Inject
    AuditPrefManagerV3 mAuditManager;

    @Inject
    RxBus mRxBus;

    CommonAreaTypeResponse mCommonAreaResponse;

    CompositeSubscription mSubsription = new CompositeSubscription();

    int mHotelId;
    private String mClickedRoomNumber;
    private TreeboTextView mSelectRoomView;
    private ArrayList<RoomV3Object> mCantAuditRoomList = new ArrayList<>();
    private ArrayList<RoomV3Object> mRecommendedRoomList = new ArrayList<>();
    private boolean isInHotelLocation;
    private View mRecommendedRoomsCard;
    private int mTaskLogID;
    private TaskModel mTaskModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_new);

        setToolBarColor(R.color.activity_gray_background);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);

        mRootView = findViewById(R.id.audit_root_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView backBtn = (ImageView) toolbar.findViewById(R.id.ic_toolbar_audit_back);
        backBtn.setOnClickListener((View v) -> finish());
        TreeboTextView title = (TreeboTextView) toolbar.findViewById(R.id.toolbar_audit_title);
        title.setText(R.string.title_audits);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        setUpExtraInformationLayout();

        mLoaderView = findViewById(R.id.loader_layout);

        if (savedInstanceState == null) {
            mHotelName = getIntent().getStringExtra(Utils.HOTEL_NAME);
            mHotelId = getIntent().getIntExtra(Utils.HOTEL_ID,-1);
            mTaskModel = getIntent().getParcelableExtra(Constants.TASK_MODEL);
            mTaskLogID = mTaskModel == null ? 0 : mTaskModel.getTaskLogID();
            isInHotelLocation = getIntent().getBooleanExtra(Utils.IS_IN_HOTEL, false);
        } else {
            mHotelName = savedInstanceState.getString(Utils.HOTEL_NAME);
            mHotelId = savedInstanceState.getInt(Utils.HOTEL_ID);
            mTaskModel = savedInstanceState.getParcelable(Constants.TASK_MODEL);
            mTaskLogID = mTaskModel == null ? 0 : mTaskModel.getTaskLogID();
            isInHotelLocation = savedInstanceState.getBoolean(Utils.IS_IN_HOTEL, false);
        }


        AuditNewComponent auditComponent = DaggerAuditNewComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectDailyActivity(this);

        // HotelId received from Dashboard Activity
         mHotelId = mLoginManager.getCurrentHotelLocationId();

        if (mHotelId == -1) {
            finish();
        }

        ImageView questionMarkIv = (ImageView) findViewById(R.id.question_mark_iv);
        questionMarkIv.setOnClickListener((View v) -> toggleMoreInformationLayout(true));

        mCommonAreaCard = findViewById(R.id.common_area_card);
        mCommonAreaCard.setOnClickListener(view -> {
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_START,
                    new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                            .putValue(SegmentAnalyticsManager.ROOM_NUMBER, SegmentAnalyticsManager.COMMON_AREA));
            startAudit(mCommonAreaResponse.getData().getCommonArea().getmCheckpointList(),
                    mCommonAreaResponse.getData().getCommonArea().getmIssuesList(),
                    mCommonAreaResponse.getData().getCommonArea().getAuditTypeId(), mHotelId);
        });
        mCommonAreaCard.setVisibility(View.GONE);

        mRecommendedRoomsCard = findViewById(R.id.recommended_rooms_card);

        mSelectRoomView = (TreeboTextView) findViewById(R.id.select_a_room_tv);
        mSubsription.add(RxView.clicks(mSelectRoomView).throttleFirst(1, TimeUnit.SECONDS).subscribe(s -> {
            if (mSelectRoomView.isClickable() && mSelectRoomView.getText().equals(getString(R.string.new_recommended_rooms)))
                showSelectReasonDialog();
            else
                onSelectRoomClick();
        }));


        mCommonAreaDoneCard = findViewById(R.id.common_area_done_card);
        mCompletedText = findViewById(R.id.completed_audit_tv);
        mCompletedCountTv = (TreeboTextView) findViewById(R.id.completed_count_tv);

        mRoomsRecyclerView = (RecyclerView) findViewById(R.id.room_recycler_view);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mRoomsRecyclerView.setLayoutManager(layoutManager1);
        mRoomsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mDoneRoomsRecyclerView = (RecyclerView) findViewById(R.id.audit_done_rv);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this);
        layoutManager2.setOrientation(RecyclerView.VERTICAL);
        mDoneRoomsRecyclerView.setLayoutManager(layoutManager2);

        initAdapters();


        mSubsription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                ArrayList<HotelLocation> hotelList = ((HotelChangedEvent) event).mHotelList;
                if (hotelList.size() == 0) {
                    finish();
                }
            }
        }));

        mDailyAuditPresenter.makeNetworkRequests(mLoginManager.getCurrentHotelLocationId());
//        mDailyAuditPresenter.makeNetworkRequests(mHotelId);

    }

    @Override
    public void onBackPressed() {
        if (mExtraInformationView.getVisibility() == View.VISIBLE) {
            toggleMoreInformationLayout(false);
        } else
            super.onBackPressed();
    }

    @Inject
    public void setUp() {
        mDailyAuditPresenter.setMvpView(this);
        mDailyAuditPresenter.setmCommonUseCase(mCommonUseCase);
        mDailyAuditPresenter.setmRoomListUseCase(mRoomListUseCase);
        mDailyAuditPresenter.setRoomNotReadyUseCase(mRoomNotReadyUseCase);

    }

    private void initAdapters() {
        mDoneAuditRoomAdapter = new AuditDoneAdapter(mDoneRoomList);
        mDoneRoomsRecyclerView.setAdapter(mDoneAuditRoomAdapter);
        mDoneAuditRoomAdapter.notifyDataSetChanged();
    }

    private void onSelectRoomClick() {
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SELECT_ANOTHER_ROOM, new Properties());
        Intent intent = new Intent(this, RoomPickerActivity.class);
        intent.putParcelableArrayListExtra(Utils.ROOM_LIST, mTotalRoomList);
        intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        intent.putExtra(Constants.TASK_MODEL, mTaskModel);
        startActivity(intent);
    }

    private void setUpDisplayRoomList() {
        for (int i = 0; i < mRoomsOnDisplayCount; i++) {
            RoomV3Object room = mRecommendedRoomList.get(i);
            if (!room.isAudited)
                mDisplayRoomList.add(room);
            else
                mDoneRoomList.add(room);
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String roomNumber = intent.getStringExtra(Utils.ROOM_NAME);
        int auditType = intent.getIntExtra(Constants.AUDIT_TYPE, -1);
        RoomV3Object room = getRoomFromList(roomNumber);
        if ((AuditTypeMap.getAuditTypeFromId(auditType).equals(AuditTypeMap.COMMON_AUDIT))) {
            mCommonAreaCard.setVisibility(View.GONE);
            mCompletedText.setVisibility(View.VISIBLE);
            mCommonAreaDoneCard.setVisibility(View.VISIBLE);
            if (room.isPendingAudit) {
                View errorIcon = mCommonAreaCard.findViewById(R.id.ic_submit_error);
                errorIcon.setVisibility(View.VISIBLE);
            }

        }
        if (roomNumber != null) {
            if (room.getId() != 0) {
                mDisplayRoomList.remove(room);
                mDoneRoomList.add(room);
                updateRecyclerViews();
            }
        }
    }


    public static Intent getCallingIntent(Activity activity) {
        Intent intent = new Intent(activity, DailyAuditActivity.class);
        return intent;
    }


    public void setUpExtraInformationLayout() {
        mExtraInformationView = findViewById(R.id.extra_information_layout);
        TreeboTextView title = (TreeboTextView) mExtraInformationView.findViewById(R.id.extra_info_title);
        title.setText(getString(R.string.recommended_rooms_extra_info_title));
        TreeboTextView text = (TreeboTextView) mExtraInformationView.findViewById(R.id.extra_info_text);
        text.setText(getString(R.string.recommended_rooms_extra_info_text));
        View closeView = mExtraInformationView.findViewById(R.id.ic_extra_info_close);
        closeView.setOnClickListener(view -> toggleMoreInformationLayout(false));
        title.setOnClickListener(view -> toggleMoreInformationLayout(false));
        text.setOnClickListener(view -> toggleMoreInformationLayout(false));
        mExtraInformationView.setOnClickListener(view -> toggleMoreInformationLayout(false));
    }

    public void toggleMoreInformationLayout(boolean show) {
        if (show) {
            mExtraInformationView.setVisibility(View.VISIBLE);
            setToolBarColor(R.color.extra_information_layout_bg);
        } else {
            mExtraInformationView.setVisibility(View.GONE);
            setToolBarColor(R.color.activity_gray_background);
        }
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoaderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoaderView.setVisibility(View.GONE);
    }


    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        return false;
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void onAuditRoomClicked(RoomV3Object room) {
//        int hotelID = mHotelId; //mLoginManager.getCurrentHotelLocationId();
        int hotelID = mLoginManager.getCurrentHotelLocationId();
        mClickedRoomNumber = room.getRoomNumber();
        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_START,
                new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                        .putValue(SegmentAnalyticsManager.ROOM_NUMBER, room.getRoomNumber())
                        .putValue(SegmentAnalyticsManager.IS_ROOM_AVAILABLE, room.isAvailable())
                        .putValue(SegmentAnalyticsManager.IS_ROOM_RECOMMENDED, room.isRecommended()));
        mNavigator.navigateToAuditActivity(this, hotelID, mHotelName, isInHotelLocation, room.getRoomNumber(),
                room.getId(), mTaskModel, false);

    }

    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }


    public void onNotReadySelected(RoomV3Object model, boolean isReady) {
        RoomV3Object notReadyRoom = getRoomFromList(model.getRoomNumber());
        if (!isReady) {
            mDailyAuditPresenter.updateAvailableRoomCount(mLoginManager.getCurrentHotelLocationId());
//          mDailyAuditPresenter.updateAvailableRoomCount(mHotelId);
            mDisplayRoomList.remove(notReadyRoom);
            if (listRoomCount < mTotalRoomList.size()) {
                mDisplayRoomList.add(mTotalRoomList.get(listRoomCount));
                listRoomCount += 1;
            }
        } else if (!mDisplayRoomList.contains(model)) {
            if (mDisplayRoomList.size() == 3)
                mDisplayRoomList.remove(2);
            mDisplayRoomList.add(model);
        }
        mAuditRoomAdapter.notifyDataSetChanged();
        // TODO: make API call to update room info
    }

    private RoomV3Object getRoomFromList(String paramRoomNumber) {
        RoomV3Object notReadyRoom = new RoomV3Object();
        if (mRecommendedRoomList != null) {
            for (RoomV3Object room : mRecommendedRoomList) {
                if (room.getRoomNumber().equals(paramRoomNumber))
                    notReadyRoom = room;
            }
        }
        return notReadyRoom;
    }

    private void updateRecyclerViews() {
        mAuditRoomAdapter.notifyDataSetChanged();
        if (mDoneRoomList.size() > 0) {
            mCompletedText.setVisibility(View.VISIBLE);
            mCompletedCountTv.setText(String.valueOf(mDoneRoomList.size()));
            mCompletedCountTv.setVisibility(View.VISIBLE);
            mDoneRoomsRecyclerView.setVisibility(View.VISIBLE);
            mDoneAuditRoomAdapter.notifyDataSetChanged();
        }
        CompleteAudit completeAudit = CompleteAudit.getCompleteAuditByHotelId(mHotelId);
        if ((mRecommendedRoomList.size() > 0 && mDisplayRoomList.size() == 0)
//                || (completeAudit != null && completeAudit.isDailyAuditCompleted())
                ) {
            mRecommendedRoomsCard.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLoadCommonAudit(CommonAreaTypeResponse response) {
        if (!response.getData().getCommonArea().isAudited())
            mCommonAreaCard.setVisibility(View.VISIBLE);
        else {
            mCompletedText.setVisibility(View.VISIBLE);
            mCommonAreaDoneCard.setVisibility(View.VISIBLE);
            CompleteAudit.updateCommonAreaAuditStatusForHotel(mHotelId);
        }
        mCommonAreaResponse = response;
    }

    public void startAudit(HashMap<Integer, ArrayList<Integer>> categoryMap, ArrayList<IssueListModelV3> issues, int auditType, int hotelId) {
        startAudit(categoryMap, issues, auditType, hotelId, "CA", 0); // Right now only Common Area, can add text as per the auditType
    }

    public void startAudit(HashMap<Integer, ArrayList<Integer>> categoryMap, ArrayList<IssueListModelV3> issues,
                           int auditType, int hotelId, String room, int roomId) {
        mAuditManager.cacheCheckpoints(AuditUtilsV3.createSavedCheckPointKey(hotelId, auditType, room), categoryMap);
        mAuditManager.setCurrentRoomId(roomId);
        ArrayList<Category> includedCheckPoints = AuditUtilsV3.checkpointList(categoryMap);
        mNavigator.navigateToCommonAudit(this, includedCheckPoints, issues, auditType, hotelId, mHotelName,
                isInHotelLocation, room, roomId, false, mTaskModel);
        overridePendingTransition(R.anim.rotate_in, R.anim.rotate_out);
    }


    @Override
    public void onLoadRoomList(RoomListResponse response) {
        mTotalRoomList = response.getRoomDataObject().getRoomList();
        displayRoomCount = response.getRoomDataObject().getDisplayRoomCount();
        listRoomCount = displayRoomCount;
        setUpRecommendedRoomList();
        if (listRoomCount > 0)
            setUpDisplayRoomList();
        mAuditRoomAdapter = new BaseRoomAdapter(this, mDisplayRoomList, false);
        mRoomsRecyclerView.setAdapter(mAuditRoomAdapter);
        updateRecyclerViews();
    }

    private void setUpRecommendedRoomList() {
        int auditDoneCount = 0;
        for (RoomV3Object room : mTotalRoomList) {
            if (room.isReady()) {
                if (room.isAudited) {
                    mDoneRoomList.add(room);
                    auditDoneCount++;
                } else if (room.isRecommended && room.isAvailable) {
                    mRecommendedRoomList.add(room);
                }
            }
        }
        if (auditDoneCount >= displayRoomCount) {
            mRecommendedRoomsCard.setVisibility(View.GONE);
            mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.AUDIT_COMPLETE));
            mDisplayRoomList = new ArrayList<>();
            listRoomCount = 0;
            mRoomsOnDisplayCount = 0;
        } else {
            mRoomsOnDisplayCount = Math.min(displayRoomCount - auditDoneCount, mRecommendedRoomList.size());
            mDisplayRoomList = new ArrayList<>(mRoomsOnDisplayCount);
            listRoomCount = mRoomsOnDisplayCount;
        }
        CompleteAudit.updateAvailableRoomCountForAudit(mHotelId, mRoomsOnDisplayCount);
        if (mRecommendedRoomList.size() == 0) {
            mSelectRoomView.setEnabled(false);
            mSelectRoomView.setClickable(false);
            mSelectRoomView.setTextColor(ContextCompat.getColor(this, R.color.text_color_secondary));
//            mSelectRoomView.setText(getString(R.string.select_a_different_room));
        }
    }

    @Override
    public void onLoadCommonFailure(String message) {
        SnackbarUtils.show(mRootView, message);
    }

    @Override
    public void onLoadRoomListFailure(String message) {
        SnackbarUtils.show(mRootView, message);
    }

    @Override
    public void onRoomNotReadyFailure(int roomID) {
        //TODO: Handle Error case
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubsription);
    }

    @Override
    public void onOptionsSelected(int optionNumber, RoomV3Object room) {
        room.cantAuditReason = Utils.getRoomNotReadyReason(optionNumber);
        mCantAuditRoomList.add(room);
    }

    private void showSelectReasonDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_select_room, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog stopUpdateDialog = dialogBuilder.create();
        stopUpdateDialog.show();

        RecyclerView roomListRv = (RecyclerView) dialogView.findViewById(R.id.dialog_select_room_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        roomListRv.setLayoutManager(layoutManager);
        roomListRv.setItemAnimator(new DefaultItemAnimator());

        int height = mRootView.getMeasuredHeight();
        ViewGroup.LayoutParams params = roomListRv.getLayoutParams();
        params.height = (height / 3) * 2;
        roomListRv.setLayoutParams(params);

        SelectRoomPopUpAdapter adapter = new SelectRoomPopUpAdapter(this, mDisplayRoomList, this);
        roomListRv.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        TreeboTextView cancelBtn = (TreeboTextView) dialogView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(view -> {
            mCantAuditRoomList.clear();
            stopUpdateDialog.dismiss();
        });

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            refreshDisplayList();
            stopUpdateDialog.dismiss();
            mCantAuditRoomList.clear();
        });
    }

    private void refreshDisplayList() {
        for (RoomV3Object room : mCantAuditRoomList) {
            RoomV3Object notReadyRoom = getRoomFromList(room.getRoomNumber());
            mDisplayRoomList.remove(notReadyRoom);
            if (listRoomCount < mRecommendedRoomList.size() && mDisplayRoomList.size() < mRoomsOnDisplayCount) {
                mDisplayRoomList.add(mRecommendedRoomList.get(listRoomCount));
                listRoomCount += 1;
            }
            mDailyAuditPresenter.markRoomNotReady(room.getId(), room.cantAuditReason);
        }
        if (mDisplayRoomList != null && mDisplayRoomList.size() < mRoomsOnDisplayCount) {
            CompleteAudit.updateAvailableRoomCountForAudit(mHotelId, mDisplayRoomList.size());
            if (mDisplayRoomList.size() == 0) {
                mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.AUDIT_COMPLETE));
                mSelectRoomView.setEnabled(false);
                mSelectRoomView.setClickable(false);
                mSelectRoomView.setTextColor(ContextCompat.getColor(this, R.color.text_color_secondary));
            } else
                mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.UPDATE_AVAILABLE_ROOM_COUNT));
        }
        if (mAuditRoomAdapter != null)
            mAuditRoomAdapter.notifyDataSetChanged();
    }

    private void markRoomsNotOccupied() {
        for (RoomV3Object room : mCantAuditRoomList) {
            int index = mTotalRoomList.indexOf(room);
            if (room.cantAuditReason.equals("Occupied")) {
                room.isAvailable = false;
                mTotalRoomList.set(index, room);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Utils.HOTEL_NAME, mHotelName);
        outState.putInt(Utils.HOTEL_ID, mHotelId);
        outState.putParcelable(Constants.TASK_MODEL, mTaskModel);
        outState.putBoolean(Utils.IS_IN_HOTEL, isInHotelLocation);
        super.onSaveInstanceState(outState);
    }
}
