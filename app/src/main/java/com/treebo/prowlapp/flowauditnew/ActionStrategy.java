package com.treebo.prowlapp.flowauditnew;

/**
 * Created by sumandas on 06/11/2016.
 */

public interface ActionStrategy {

    String showTitle();
    void onTakeAction();
    void onCancelAction();
}
