package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.FraudRoomObject;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 17/01/17.
 */

public class FraudRoomListAdapter extends RecyclerView.Adapter<FraudRoomListAdapter.RoomViewHolder> {

    private ArrayList<FraudRoomObject> mRoomList = new ArrayList<>();
    private boolean isHouseCountAudit;
    private onRoomClickListener mListener;
    private Context mContext;

    public FraudRoomListAdapter(Context context, ArrayList<FraudRoomObject> rooms, boolean isHouseCountAudit, onRoomClickListener listener) {
        this.mRoomList = rooms;
        this.mContext = context;
        this.isHouseCountAudit = isHouseCountAudit;
        this.mListener = listener;
    }

    public void resetData(ArrayList<FraudRoomObject> list) {
        this.mRoomList = list;
        notifyDataSetChanged();
    }

    public boolean isAdapterForHouseCountAudit() {
        return this.isHouseCountAudit;
    }

    public void refreshItem(String roomName, String fraudReason) {
        int index = getCheckpointPositionInList(roomName);
        if (index != -1) {
            FraudRoomObject roomObject = mRoomList.get(index);
            roomObject.setFraudReason(fraudReason);
            mRoomList.set(index, roomObject);
            notifyItemChanged(index);
        }
    }

    public int getFraudAuditDoneRoomCount() {
        int count = 0;
        for (FraudRoomObject roomObject : mRoomList) {
            if (!TextUtils.isEmpty(roomObject.getFraudReason()))
                count++;
        }
        return count;
    }

    @Override
    public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_roomcount,
                parent, false);
        return new RoomViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(RoomViewHolder holder, int position) {
        FraudRoomObject room = mRoomList.get(position);
        holder.room = room;
        holder.roomNumberTv.setText(room.getRoomName());
        if (!TextUtils.isEmpty(room.getFraudReason())) {
            holder.fraudReasonTv.setText(room.getFraudReason());
            holder.fraudReasonTv.setTextColor(ContextCompat.getColor(mContext, room.getTextColor()));
            holder.fraudReasonTv.setVisibility(View.VISIBLE);
            holder.arrow.setVisibility(View.GONE);
        } else {
            holder.fraudReasonTv.setVisibility(View.GONE);
            holder.arrow.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mRoomList == null ? 0 : mRoomList.size();
    }

    class RoomViewHolder extends RecyclerView.ViewHolder {

        private FraudRoomObject room;
        private TreeboTextView roomNumberTv;
        private TreeboTextView fraudReasonTv;
        private ImageView arrow;

        public RoomViewHolder(View itemView, onRoomClickListener listener) {
            super(itemView);
            if (isHouseCountAudit)
                itemView.setOnClickListener(view -> listener.onHouseCountAuditRoomClicked(room));
            else
                itemView.setOnClickListener(view -> listener.onPriceMismatchAuditRoomClicked(room));
            roomNumberTv = (TreeboTextView) itemView.findViewById(R.id.room_number_tv);
            fraudReasonTv = (TreeboTextView) itemView.findViewById(R.id.room_fraud_reason_tv);
            arrow = (ImageView) itemView.findViewById(R.id.ic_side_arrow);
        }
    }

    public interface onRoomClickListener {
        void onHouseCountAuditRoomClicked(FraudRoomObject roomNumber);

        void onPriceMismatchAuditRoomClicked(FraudRoomObject roomNumber);
    }

    private int getCheckpointPositionInList(String subCheckpoint) {
        for (int i = 0; i < mRoomList.size(); i++) {
            if (subCheckpoint.equals(mRoomList.get(i).getRoomName()))
                return i;
        }
        return -1;
    }
}
