package com.treebo.prowlapp.flowauditnew.presenter;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.observers.RoomListObserver;
import com.treebo.prowlapp.flowauditnew.usecase.RoomListUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by sumandas on 10/11/2016.
 */

public class RoomListPresenter implements AuditContract.IRoomListPresenter {


    private RoomListUseCase mRoomSingleUse;

    private AuditContract.IRoomListView mView;

    @Override
    public void loadRoomAudit(int hotelId) {
        showLoading();
        mRoomSingleUse.setHotelId(hotelId);
        mRoomSingleUse.execute(providesRoomPickObserver());
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView=(AuditContract.IRoomListView)baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }

    private void showLoading() {
        mView.showLoading();
    }

    public void setRoomListUseCase(RoomListUseCase mRoomSingleUse) {
        this.mRoomSingleUse = mRoomSingleUse;
    }

    public RoomListObserver providesRoomPickObserver(){
        return new RoomListObserver(this,mRoomSingleUse,mView);
    }
}
