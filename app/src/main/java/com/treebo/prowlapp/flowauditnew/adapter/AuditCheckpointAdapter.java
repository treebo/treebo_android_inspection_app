package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhisheknair on 02/11/16.
 */

public class AuditCheckpointAdapter extends RecyclerView.Adapter<AuditCheckpointAdapter.AuditCheckpointViewHolder> {

    private List<Checkpoint> mCheckPointList;
    private onCheckPointClickListener mListener;
    private Context mContext;
    private boolean isAnimationPlaying;

    public AuditCheckpointAdapter(Context context, List<Checkpoint> list,
                                  onCheckPointClickListener listener) {
        mContext = context;
        mCheckPointList = new ArrayList<>(list.size());
        for (Checkpoint checkpoint : list) {
            if (checkpoint.isIncluded()) {
                mCheckPointList.add(checkpoint);
            }
        }
        mListener = listener;
    }

    public void setAnimationPlaying(boolean value) {
        this.isAnimationPlaying = value;
    }

    @Override
    public AuditCheckpointViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_checkpoint,
                parent, false);
        return new AuditCheckpointViewHolder(view, mListener);
    }

    public void refreshItem(Checkpoint checkpoint) {
        int index = getCheckpointPositionInList(checkpoint);
        if (index != -1) {
            mCheckPointList.set(index, checkpoint);
            notifyItemChanged(index);
        }
    }

    private int getCheckpointPositionInList(Checkpoint checkpoint) {
        for (int i = 0; i < mCheckPointList.size(); i++) {
            if (checkpoint.mCheckpointId == mCheckPointList.get(i).mCheckpointId)
                return i;
        }
        return -1;
    }

    @Override
    public void onBindViewHolder(AuditCheckpointViewHolder holder, int position) {
        Checkpoint checkPoint = mCheckPointList.get(position);
        holder.checkpoint = checkPoint;
        holder.text.setText(checkPoint.mDescription);
        holder.pendingFlag.setVisibility(checkPoint.isPendingIssue() ? View.VISIBLE : View.GONE);
        if (checkPoint.issue() || checkPoint.isPendingIssue()) {
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_red_cross_filled));
            holder.subText.setText(checkPoint.getIssueText());
            int visibility = !TextUtils.isEmpty(checkPoint.getIssueText()) ? View.VISIBLE : View.GONE;
            holder.subText.setVisibility(visibility);
        } else {
            int resId = isAnimationPlaying ? R.drawable.ic_tick_solid : R.drawable.ic_hollow_circle_green;
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(mContext, resId));
            holder.subText.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mCheckPointList.size();
    }

    static class AuditCheckpointViewHolder extends RecyclerView.ViewHolder {

        private Checkpoint checkpoint;
        private TreeboTextView text;
        private ImageView checkBox;
        private TreeboTextView subText;
        private TreeboTextView pendingFlag;

        public AuditCheckpointViewHolder(View itemView, onCheckPointClickListener listener) {
            super(itemView);
            text = (TreeboTextView) itemView.findViewById(R.id.audit_checkpoint_title);
            checkBox = (ImageView) itemView.findViewById(R.id.audit_checkpoint_checkbox);
            checkBox.setOnClickListener(view -> listener.checkPointClicked(checkpoint));
            subText = (TreeboTextView) itemView.findViewById(R.id.audit_checkpoint_subtitle);
            pendingFlag = (TreeboTextView) itemView.findViewById(R.id.pending_issue_flag);
            itemView.setOnClickListener(view -> {
                listener.checkPointClicked(checkpoint);
            });
            itemView.setOnLongClickListener(view -> {
                listener.checkPointLongPress(checkpoint);
                return false;
            });
        }
    }

    public interface onCheckPointClickListener {
        void checkPointClicked(Checkpoint checkpoint);

        void checkPointLongPress(Checkpoint checkpoint);
    }
}
