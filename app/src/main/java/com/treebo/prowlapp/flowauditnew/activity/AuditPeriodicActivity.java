package com.treebo.prowlapp.flowauditnew.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AuditStateChangeEvent;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.events.PeriodicAuditStateChangeEvent;
import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.AuditNewComponent;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.DaggerAuditNewComponent;
import com.treebo.prowlapp.flowauditnew.adapter.AuditPeriodicAdapter;
import com.treebo.prowlapp.flowauditnew.presenter.AuditPeriodicPresenter;
import com.treebo.prowlapp.flowauditnew.usecase.AuditCategoryUseCase;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.PeriodicAuditComplete;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.DividerItemDecoration;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by sumandas on 22/01/2017.
 */

public class AuditPeriodicActivity extends AppCompatActivity implements AuditContract.IAuditPeriodicView,
        AuditPeriodicAdapter.OnSubAuditClickedListener {

    @Inject
    public AuditCategoryUseCase mAuditCategoryUseCase;

    @Inject
    public AuditPeriodicPresenter mAuditPresenter;

    @Inject
    public AuditPrefManagerV3 mAuditManager;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public RxBus mRxBus;

    @Inject
    public Navigator mNavigator;

    private RecyclerView mNotDoneRecyclerView;

    private RecyclerView mDoneRecyclerView;

    private AuditPeriodicAdapter mNotDoneAdapter;
    private AuditPeriodicAdapter mDoneAdapter;


    private View mLoaderView;

    private TaskModel mTaskModel;

    private Boolean mIsFromAuditAnyHotel;
    private int mHotelId;
    private String mHotelName;
    private int mAuditType;
    private boolean isInHotelLocation;

    private TreeboTextView mCompletedText;
    private TreeboTextView mCompletedCount;

    TreeboTextView mTitleBar;

    ImageView mBackBtn;

    private ArrayList<AuditAreaResponse.AuditArea> mNotDoneAudits = new ArrayList<>();

    private ArrayList<AuditAreaResponse.AuditArea> mDoneAudits = new ArrayList<>();

    private ArrayList<AuditAreaResponse.AuditArea> mAuditList = new ArrayList<>();


    private View mRootView;

    CompositeSubscription mSubsription = new CompositeSubscription();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_periodic_audit);

        setToolBarColor(R.color.activity_gray_background);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);

        AuditNewComponent auditComponent = DaggerAuditNewComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectAuditPeriodicAcitivity(this);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mTaskModel = intent.getParcelableExtra(Constants.TASK_MODEL);
            isInHotelLocation = intent.getBooleanExtra(Utils.IS_IN_HOTEL, true);
            mHotelName = intent.getStringExtra(Utils.HOTEL_NAME);
            mHotelId = intent.getIntExtra(Utils.HOTEL_ID, -1);
            mIsFromAuditAnyHotel = intent.getBooleanExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL,false);
        } else {
            mTaskModel = savedInstanceState.getParcelable(Constants.TASK_MODEL);
            isInHotelLocation = savedInstanceState.getBoolean(Utils.IS_IN_HOTEL, true);
            mHotelName = savedInstanceState.getString(Utils.HOTEL_NAME);
            mHotelId = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            mIsFromAuditAnyHotel = savedInstanceState.getBoolean(Utils.IS_FROM_AUDIT_ANY_HOTEL,false);
        }
        mAuditType = mTaskModel.getAuditTaskType();

        mDoneRecyclerView = (RecyclerView) findViewById(R.id.audit_done_rv);
        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.transparent_divider);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        mDoneRecyclerView.setLayoutManager(layoutManager1);
        mDoneRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mDoneRecyclerView.addItemDecoration(new DividerItemDecoration(dividerDrawable));

        mNotDoneRecyclerView = (RecyclerView) findViewById(R.id.audit_not_done_rv);

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this);
        layoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        mNotDoneRecyclerView.setLayoutManager(layoutManager2);
        mNotDoneRecyclerView.addItemDecoration(new DividerItemDecoration(dividerDrawable));
//        mNotDoneRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLoaderView = findViewById(R.id.loader_layout);
        mRootView = findViewById(R.id.audit_root_view);

        mCompletedText = (TreeboTextView) findViewById(R.id.completed_audit_tv);
        mCompletedCount = (TreeboTextView) findViewById(R.id.completed_count_tv);

        Toolbar toolbar = (Toolbar) findViewById(R.id.periodic_audit_toolbar);
        mBackBtn = (ImageView) toolbar.findViewById(R.id.ic_toolbar_audit_back);

        mTitleBar = (TreeboTextView) toolbar.findViewById(R.id.toolbar_audit_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);


        if (savedInstanceState != null) {
            setUpToolBar();
        }

        if(mIsFromAuditAnyHotel)
            mAuditPresenter.loadAuditCategoryRemote(mHotelId, mAuditType,mLoginManager.getUserId());
        else
            mAuditPresenter.loadAuditCategory(mHotelId, mAuditType,mLoginManager.getUserId());

        mSubsription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                ArrayList<HotelLocation> hotelList = ((HotelChangedEvent) event).mHotelList;
                if (hotelList.size() == 0) {
                    finish();
                }
            }
        }));

    }

    public void setUpToolBar() {
        mTitleBar.setText(mTaskModel.getHeading());
        mBackBtn.setVisibility(View.VISIBLE);
        mBackBtn.setOnClickListener((View v) -> finish());
    }

    public static Intent getCallingIntent(Activity activity) {
        return new Intent(activity, AuditPeriodicActivity.class);
    }

    @Inject
    public void setUp() {
        mAuditPresenter.setMvpView(this);
        mAuditPresenter.setmAuditCategoryUseCase(mAuditCategoryUseCase);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int auditCategoryId = intent.getIntExtra(Constants.AUDIT_CATEGORY_ID, -1);
        if (auditCategoryId != -1) {
            AuditAreaResponse.AuditArea auditedArea = getAuditAreaFromList(auditCategoryId);
            if (auditedArea != null) {
                mDoneAudits.add(auditedArea);
                mNotDoneAudits.remove(auditedArea);
                if (mDoneAudits.size() > 0) {
                    mCompletedText.setVisibility(View.VISIBLE);
                    mCompletedCount.setText(String.valueOf(mDoneAudits.size()));
                    mCompletedCount.setVisibility(View.VISIBLE);
                    mDoneRecyclerView.setVisibility(View.VISIBLE);
                    mDoneAdapter.notifyDataSetChanged();
                }
                if (mNotDoneAudits.size() == 0) {
                    mNotDoneRecyclerView.setVisibility(View.GONE);
                    PeriodicAuditComplete.deleteSubmittedPeriodicAudit(mTaskModel.getTaskLogID());
                    mRxBus.postEvent(new PeriodicAuditStateChangeEvent(AuditStateChangeEvent.FnB_PERIODIC_AUDIT_SUBMIT, mTaskModel.getTaskLogID()));
                }
                mNotDoneAdapter.notifyDataSetChanged();
            }
        }
    }

    private AuditAreaResponse.AuditArea getAuditAreaFromList(int auditCategoryID) {
        for (AuditAreaResponse.AuditArea auditArea : mAuditList) {
            if (auditArea.mAuditCategoryId == auditCategoryID)
                return auditArea;
        }
        return null;
    }

    @Override
    public void onLoadAuditByCategory(AuditAreaResponse response) {
        mAuditList = response.mAuditAreaObject.mAuditAreas;
        if (mAuditList == null) {
            mAuditList = new ArrayList<>();
        }
        if (mAuditList.size() > 1) {
            hideLoading();
            setUpToolBar();
            for (AuditAreaResponse.AuditArea auditArea : mAuditList) {
                if (auditArea.mDetails.get(0).isAuditDone) {
                    if (!PeriodicAuditComplete.isSubAuditInList(mTaskModel.getTaskLogID(), auditArea.mAuditCategoryId)) {
                        PeriodicAuditComplete auditComplete = new PeriodicAuditComplete(mHotelId,
                                auditArea.mAuditCategoryId, mTaskModel.getTaskLogID(), mTaskModel.getExpiryDate());
                        auditComplete.save();
                    }
                    mDoneAudits.add(auditArea);
                } else {
                    mNotDoneAudits.add(auditArea);
                }
            }
            if (mNotDoneAudits.size() == 0) {
                mNotDoneRecyclerView.setVisibility(View.GONE);
                PeriodicAuditComplete.deleteSubmittedPeriodicAudit(mTaskModel.getTaskLogID());
                mRxBus.postEvent(new PeriodicAuditStateChangeEvent(AuditStateChangeEvent.FnB_PERIODIC_AUDIT_SUBMIT, mTaskModel.getTaskLogID()));
            }
            mNotDoneAdapter = new AuditPeriodicAdapter(this, this, mNotDoneAudits, false);
            mDoneAdapter = new AuditPeriodicAdapter(this, this, mDoneAudits, true);
            mDoneRecyclerView.setAdapter(mDoneAdapter);
            mDoneAdapter.notifyDataSetChanged();
            mNotDoneRecyclerView.setAdapter(mNotDoneAdapter);
            mNotDoneAdapter.notifyDataSetChanged();
            if (mDoneAudits.size() > 0) {
                mCompletedText.setVisibility(View.VISIBLE);
                mCompletedCount.setVisibility(View.VISIBLE);
                mCompletedCount.setText(String.valueOf(mDoneAudits.size()));
            } else {
                mCompletedText.setVisibility(View.GONE);
                mCompletedCount.setVisibility(View.GONE);
            }
        } else {//audit has only one sub type so start it
            if (!mAuditList.get(0).mDetails.get(0).isAuditDone) {
                if (mTaskModel.getVisualRepresentationType() == Constants.VISUAL_REPRESENTATION_TYPE_1) {
                    startType1Audit(mAuditList.get(0));
                } else {
                    startType2Audit(mAuditList.get(0));
                }
            } else {
                mRxBus.postEvent(new PeriodicAuditStateChangeEvent(AuditStateChangeEvent.PERIODIC_AUDIT_SUBMIT, mTaskModel.getTaskLogID()));
            }
            finish();
        }
    }

    @Override
    public void onLoadAuditFailure(String message) {
        SnackbarUtils.show(mRootView, message);
    }

    public void startType2Audit(AuditAreaResponse.AuditArea auditArea) {
        mAuditManager.cacheCheckpoints(AuditUtilsV3.createSavedCheckPointKey(mHotelId,
                auditArea.mAuditCategoryId, null), auditArea.mDetails.get(0).mCheckpointList);
        ArrayList<Category> includedCheckPoints =
                AuditUtilsV3.checkpointList(auditArea.mDetails.get(0).mCheckpointList);
        mNavigator.navigateToAuditType2Activity(this, mTaskModel, auditArea.mAuditCategoryId,
                includedCheckPoints, mHotelId, mHotelName, isInHotelLocation, true,mIsFromAuditAnyHotel);

    }

    public void startType1Audit(AuditAreaResponse.AuditArea auditArea) {
        mAuditManager.cacheCheckpoints(AuditUtilsV3.createSavedCheckPointKey(mHotelId,
                auditArea.mAuditCategoryId, null), auditArea.mDetails.get(0).mCheckpointList);
        ArrayList<Category> includedCheckPoints =
                AuditUtilsV3.checkpointList(auditArea.mDetails.get(0).mCheckpointList);
        mNavigator.navigateToAuditActivity(this, includedCheckPoints, auditArea.mAuditCategoryId,
                mHotelId, mHotelName, isInHotelLocation, null, -1, false, mTaskModel,mIsFromAuditAnyHotel);

    }


    @Override
    public void setUpViewAsPerPermissions() {
        
    }

    @Override
    public void showLoading() {
        mLoaderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoaderView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    public void onSubAuditClicked(AuditAreaResponse.AuditArea auditArea) {
        startType2Audit(auditArea);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubsription);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constants.TASK_MODEL, mTaskModel);
        outState.putBoolean(Utils.IS_IN_HOTEL, isInHotelLocation);
        outState.putString(Utils.HOTEL_NAME, mHotelName);
        outState.putInt(Utils.HOTEL_ID, mHotelId);
        super.onSaveInstanceState(outState);
    }
}
