package com.treebo.prowlapp.flowauditnew.observers;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.usecase.CommonUseCase;
import com.treebo.prowlapp.Models.auditmodel.AuditTypeMap;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;

/**
 * Created by sumandas on 10/11/2016.
 */

public class CommonAreaObserver extends BaseObserver<CommonAreaTypeResponse> {

    private AuditContract.IDailyAuditView mView;
    private CommonUseCase mUseCase;

    public CommonAreaObserver(BasePresenter presenter, CommonUseCase commonUseCase, AuditContract.IDailyAuditView view) {
        super(presenter, commonUseCase);
        mUseCase = commonUseCase;
        mView = view;
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public void onNext(CommonAreaTypeResponse commonAreaTypeResponse) {
        if (commonAreaTypeResponse.status.equals("success")) {
            AuditTypeMap.saveAuditType(commonAreaTypeResponse.getData().getCommonArea().getAuditTypeId(), AuditTypeMap.COMMON_AUDIT);
            mView.onLoadCommonAudit(commonAreaTypeResponse);
        } else {
            mView.onLoadCommonFailure("Loading common area checklist failed");
        }
    }

}
