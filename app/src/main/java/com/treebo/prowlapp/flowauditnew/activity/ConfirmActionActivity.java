package com.treebo.prowlapp.flowauditnew.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.view.WindowManager;

import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding.view.RxView;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AuditStateChangeEvent;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.events.PeriodicAuditStateChangeEvent;
import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.AuditNewComponent;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.DaggerAuditNewComponent;
import com.treebo.prowlapp.flowauditnew.presenter.ConfirmActionPresenter;
import com.treebo.prowlapp.flowauditnew.usecase.CreateIssueUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitAuditV3UseCase;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitFraudAuditUseCase;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitPeriodicAuditUseCase;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Models.auditmodel.AuditTypeMap;
import com.treebo.prowlapp.Models.auditmodel.FraudAuditSubmitObject;
import com.treebo.prowlapp.Models.auditmodel.PeriodicAuditComplete;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.response.audit.FraudAuditSubmitResponse;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by sumandas on 04/11/2016.
 */

public class ConfirmActionActivity extends AppCompatActivity implements AuditContract.IConfirmAuditActionView {

    private TreeboButton mConfirmAction;
    private TreeboButton mCancelAction;
    private TreeboTextView mTitle;
    private TreeboTextView mAuditWarning;

    private View mRootView;

    private View mSuccessView;

    @Inject
    public ConfirmActionPresenter mConfirmActionPresenter;

    @Inject
    public SubmitAuditV3UseCase mRoomAuditSubmitUseCase;

    @Inject
    public SubmitPeriodicAuditUseCase mPeriodicAuditSubmitUseCase;

    @Inject
    public SubmitFraudAuditUseCase mFraudAuditSubmitUseCase;

    @Inject
    protected CreateIssueUseCase mCreateIssueUseCase;

    @Inject
    LoginSharedPrefManager mLoginManager;

    @Inject
    AuditPrefManagerV3 mAuditManager;

    @Inject
    RxBus mRxBus;

    CompositeSubscription mSubscription = new CompositeSubscription();

    private Boolean mIsFromAuditAnyHotel;
    public boolean isAuditSubmit;
    private ArrayList<AnswerV3> mIssueList;
    private int mRoomId;
    private String mRoomNumber;
    private int mAuditCategoryID;
    private int mHotelId;
    private int mTaskLogID;
    private TaskModel mTaskModel;
    private View mLoadingView;
    private ArrayList<FraudAuditSubmitObject> mFraudIssueList;
    private boolean disableBackPress;
    private String mHotelName;
    private boolean isIssueSubmit;
    private String mCurrentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_action);
        setToolBarColor(R.color.card_activity_background);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);


        AuditNewComponent auditComponent = DaggerAuditNewComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectConfirmActionActivity(this);

        Intent intent = getIntent();
        if (savedInstanceState == null) {
            isAuditSubmit = intent.getBooleanExtra(Constants.AUDIT_SUBMISSION, false);
            mIssueList = intent.getParcelableArrayListExtra(Constants.ISSUE_LIST);
            mFraudIssueList = intent.getParcelableArrayListExtra(Constants.FRAUD_ISSUE_LIST);
            mTaskModel = intent.getParcelableExtra(Constants.TASK_MODEL);
            mRoomId = intent.getIntExtra(Constants.ROOM_ID, -1);
            mRoomNumber = intent.getStringExtra(Utils.ROOM_NAME);
            mAuditCategoryID = intent.getIntExtra(Constants.AUDIT_TYPE, -1);
            mHotelName = intent.getStringExtra(Utils.HOTEL_NAME);
            mHotelId = intent.getIntExtra(Utils.HOTEL_ID, -1);
            isIssueSubmit = intent.getBooleanExtra(Utils.IS_FROM_CREATE_ISSUE, false);
            mCurrentDate = intent.getStringExtra(Utils.START_TIME);
            mIsFromAuditAnyHotel = intent.getBooleanExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL,false);
        } else {
            isAuditSubmit = savedInstanceState.getBoolean(Constants.AUDIT_SUBMISSION, false);
            mIssueList = savedInstanceState.getParcelableArrayList(Constants.ISSUE_LIST);
            mFraudIssueList = savedInstanceState.getParcelableArrayList(Constants.FRAUD_ISSUE_LIST);
            mTaskModel = savedInstanceState.getParcelable(Constants.TASK_MODEL);
            mRoomId = savedInstanceState.getInt(Constants.ROOM_ID, -1);
            mRoomNumber = savedInstanceState.getString(Utils.ROOM_NAME);
            mAuditCategoryID = savedInstanceState.getInt(Constants.AUDIT_TYPE, -1);
            mHotelName = savedInstanceState.getString(Utils.HOTEL_NAME);
            mHotelId = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            isIssueSubmit = savedInstanceState.getBoolean(Utils.IS_FROM_CREATE_ISSUE, false);
            mCurrentDate = savedInstanceState.getString(Utils.START_TIME);
            mIsFromAuditAnyHotel = savedInstanceState.getBoolean(Utils.IS_FROM_AUDIT_ANY_HOTEL,false);
        }

        mRootView = findViewById(R.id.root_confirm_view);
        mTitle = (TreeboTextView) findViewById(R.id.confirm_area_text);
        mAuditWarning = (TreeboTextView) findViewById(R.id.confirm_area_warning);
        mConfirmAction = (TreeboButton) findViewById(R.id.confirm_action);
        mCancelAction = (TreeboButton) findViewById(R.id.cancel_action);
        mLoadingView = findViewById(R.id.loader_layout);
        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));

        mSuccessView = findViewById(R.id.audit_success_layout);

        mTaskLogID = mTaskModel == null ? 0 : mTaskModel.getTaskLogID();

        String auditArea = intent.getStringExtra(Constants.AUDIT_AREA);
        String auditType = AuditCategory.getAuditCategoryConstantByID(mAuditCategoryID);
        if (isAuditSubmit) {
            mTitle.setText(getString(R.string.submit_audit));
            mAuditWarning.setVisibility(View.VISIBLE);
            mConfirmAction.setText(getString(R.string.submit_audit_now));
            mCancelAction.setText(getString(R.string.have_changes));
            mCancelAction.setTextColor(getResources().getColor(R.color.green_color_primary));
            mCancelAction.setBackgroundResource(R.drawable.border_button_green);
        } else if (isIssueSubmit) {
            mTitle.setText(getString(R.string.submit_issue));
            mAuditWarning.setVisibility(View.VISIBLE);
            mConfirmAction.setText(getString(R.string.create_issue_now));
            mCancelAction.setText(getString(R.string.add_another_issue));
            TreeboTextView treeboTextView = (TreeboTextView) mSuccessView.findViewById(R.id.audit_successfully_submitted);
            treeboTextView.setText(getString(R.string.issue_creation_success));
            mCancelAction.setTextColor(getResources().getColor(R.color.green_color_primary));
            mCancelAction.setBackgroundResource(R.drawable.border_button_green);
        } else {
            mTitle.setText(String.format(getString(R.string.confirm_start_audit), auditArea));
        }

        mCancelAction.setOnClickListener(view -> {
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.CHANGES_BUTTON_SUBMIT,
                    new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                            .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mRoomNumber)
                            .putValue(SegmentAnalyticsManager.AUDIT_TYPE, auditType));
            onBackPressed();
        });


        mSubscription.add(RxView.clicks(mConfirmAction)
                .throttleFirst(3, TimeUnit.SECONDS)
                .subscribe(s -> {
                    SegmentAnalyticsManager.trackEventOnSegment(isIssueSubmit ?
                                    SegmentAnalyticsManager.AUDIT_SUBMIT : SegmentAnalyticsManager.CREATE_NEW_ISSUE,
                            new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                                    .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mRoomNumber)
                                    .putValue(SegmentAnalyticsManager.AUDIT_TYPE, auditType));
                    onConfirmClick();
                }));

        mSubscription.add(RxUtils.build(mRxBus.toObservable())
                .subscribe(event -> {
                    if (event instanceof HotelChangedEvent) {
                        ArrayList<HotelLocation> hotelList = ((HotelChangedEvent) event).mHotelList;
                        if (hotelList.size() == 0) {
                            finish();
                        }
                    }
                }));
    }

    @Inject
    public void setUp() {
        mConfirmActionPresenter.setMvpView(this);
        mConfirmActionPresenter.setmSubmitUseCase(mRoomAuditSubmitUseCase);
        mConfirmActionPresenter.setFraudSubmitUseCase(mFraudAuditSubmitUseCase);
        mConfirmActionPresenter.setPeriodicAuditSubmitUseCase(mPeriodicAuditSubmitUseCase);
        mConfirmActionPresenter.setmCreateIssueUseCase(mCreateIssueUseCase);
    }

    @Override
    public void onBackPressed() {
        if (!disableBackPress)
            super.onBackPressed();
    }

    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

    private void onConfirmClick() {
        if (isIssueSubmit) {
            String userRoles = mLoginManager.getUserRolesString(mHotelId, false);
            String source = userRoles.toLowerCase().contains(Constants.SOURCE_QAM)
                    ? Constants.SOURCE_QAM : userRoles;
            mConfirmActionPresenter.createIssue(mIssueList, mRoomId, mLoginManager.getUserId(),
                    mAuditCategoryID, mHotelId, source);
        } else if (isAuditSubmit) {
            if (Utils.isInternetConnected(this)) {
                AuditCategory category = AuditCategory.getAuditCategoryById(mAuditCategoryID);
                if (category != null) {
                    disableBackPress = true;
                    switch (category.mConstantAuditName) {
                        case Constants.COMMON_AREA_AUDIT_STRING:
                        case Constants.ROOM_AUDIT_STRING:
                            if(mIsFromAuditAnyHotel)
                                mConfirmActionPresenter.submitAuditRemote(mIssueList, mRoomId, mLoginManager.getUserId(),
                                        mAuditCategoryID,
                                        mHotelId, Constants.SOURCE_QAM, mTaskLogID,mCurrentDate,"Remote");
                            else
                                mConfirmActionPresenter.submitAudit(mIssueList, mRoomId, mLoginManager.getUserId(),
                                        mAuditCategoryID,
                                        mHotelId, Constants.SOURCE_QAM, mTaskLogID,mCurrentDate);
                            break;

                        case Constants.FRAUD_AUDIT_STRING:
                            mConfirmActionPresenter.submitFraudAudit(mFraudIssueList, mLoginManager.getUserId(),
                                    mHotelId, mTaskLogID);
                            break;

                        case Constants.BREAKFAST_AUDIT_STRING:
                        case Constants.FnB_AUDIT_STRING:
                        default:
                            if(mIsFromAuditAnyHotel)
                                mConfirmActionPresenter.submitPeriodicAuditRemote(mIssueList, mLoginManager.getUserId(),
                                        mAuditCategoryID, mHotelId, Constants.SOURCE_QAM, mTaskLogID,mCurrentDate,"Remote");
                            else
                                mConfirmActionPresenter.submitPeriodicAudit(mIssueList, mLoginManager.getUserId(),
                                        mAuditCategoryID, mHotelId, Constants.SOURCE_QAM, mTaskLogID,mCurrentDate);
                            break;
                    }
                }
            } else {
                Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
            }
        } else {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onAuditSubmitSuccess(SubmitAuditResponse response) {
        String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryID, mRoomNumber);
        mAuditManager.removeAudit(key);
        mAuditManager.clearCurrentAuditKey();
        mAuditManager.removeCachedCheckPoints(AuditUtilsV3.createSavedCheckPointKey(mHotelId, mAuditCategoryID, mRoomNumber));
        mAuditManager.setCurrentRoomId(-1);
        mConfirmActionPresenter.updateCompleteAudit(mHotelId, mAuditCategoryID, mRoomNumber);
        if ((AuditTypeMap.getAuditTypeFromId(mAuditCategoryID).equals(AuditTypeMap.COMMON_AUDIT))) {
            mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.COMMON_AREA_AUDIT_SUBMIT));
        } else {
            mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.ROOM_AUDIT_SUBMIT));
        }
        showSuccessScreen();
    }

    @Override
    public void onAuditSubmitFailure(String message) {
        disableBackPress = false;
        mCancelAction.setVisibility(View.VISIBLE);
        mConfirmAction.setVisibility(View.VISIBLE);
        if(message != null) {
            checkIfAuditAlreadyDone(message);
        }else{
            SnackbarUtils.show(mRootView, getString(R.string.generic_error));
        }
    }

    @Override
    public void onFraudAuditSubmitSuccess(FraudAuditSubmitResponse response) {
        showSuccessScreen();
        mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.FRAUD_AUDIT_SUBMIT));
    }

    @Override
    public void onPeriodicAuditSubmitSuccess(SubmitAuditResponse response) {
        String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryID, mRoomNumber);
        mAuditManager.removeAudit(key);
        mAuditManager.removeCachedCheckPoints(AuditUtilsV3.createSavedCheckPointKey(mHotelId, mAuditCategoryID, null));
        if(!mIsFromAuditAnyHotel){
            PeriodicAuditComplete auditComplete = new PeriodicAuditComplete(mHotelId, mAuditCategoryID, mTaskLogID, mTaskModel.getExpiryDate());
            auditComplete.save();
        }
        List<AuditCategory> subAuditList = AuditCategory.getSubAudits(mTaskModel.getAuditTaskType());
        if (subAuditList.size() == 1)
            mRxBus.postEvent(new PeriodicAuditStateChangeEvent(AuditStateChangeEvent.PERIODIC_AUDIT_SUBMIT, mTaskLogID));
        showSuccessScreen();
    }

    @Override
    public void showSuccessScreen() {
        mSuccessView.setVisibility(View.VISIBLE);
        setToolBarColor(R.color.green_color_primary);
        mCancelAction.setVisibility(View.GONE);
        mConfirmAction.setVisibility(View.GONE);
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            setResult(RESULT_OK);
            finish();
        }, 2000);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
        mCancelAction.setVisibility(View.GONE);
        mConfirmAction.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }


    @Override
    public boolean showError(String errorMessage) {
        disableBackPress = false;
        mCancelAction.setVisibility(View.VISIBLE);
        mConfirmAction.setVisibility(View.VISIBLE);
        if (errorMessage != null) {
            checkIfAuditAlreadyDone(errorMessage);
        } else {
            SnackbarUtils.show(mRootView, getString(R.string.generic_error));
        }
        return false;
    }

    /**
     * Handing Audit Already Done Error Message specially
     * Ad-hoc fix before it is handled at the backend
     *
     * @param message
     */
    private void checkIfAuditAlreadyDone(String message) {
        if (message.equals(getString(R.string.audit_already_done))) {
            AuditCategory category = AuditCategory.getAuditCategoryById(mAuditCategoryID);
            if (category != null) {
                switch (category.mConstantAuditName) {
                    case Constants.COMMON_AREA_AUDIT_STRING:
                    case Constants.ROOM_AUDIT_STRING:
                        onAuditSubmitSuccess(new SubmitAuditResponse());
                        break;

                    case Constants.FRAUD_AUDIT_STRING:
                        onFraudAuditSubmitSuccess(new FraudAuditSubmitResponse());
                        break;

                    default:
                        onPeriodicAuditSubmitSuccess(new SubmitAuditResponse());
                        break;
                }
            }
        } else {
            SnackbarUtils.show(mRootView, message);
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {
        disableBackPress = false;
        mCancelAction.setVisibility(View.VISIBLE);
        mConfirmAction.setVisibility(View.VISIBLE);
    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

}
