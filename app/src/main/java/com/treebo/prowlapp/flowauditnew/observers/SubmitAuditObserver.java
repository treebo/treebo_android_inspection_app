package com.treebo.prowlapp.flowauditnew.observers;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitAuditV3UseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by sumandas on 11/11/2016.
 */

public class SubmitAuditObserver extends BaseObserver<SubmitAuditResponse> {

    private AuditContract.IConfirmAuditActionView mView;
    private SubmitAuditV3UseCase mUseCase;
    private int mAuditType;

    public SubmitAuditObserver(BasePresenter presenter, SubmitAuditV3UseCase useCase,
                               AuditContract.IConfirmAuditActionView view, int auditType) {
        super(presenter, useCase);
        mUseCase = useCase;
        mView = view;
        mAuditType = auditType;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(SubmitAuditResponse submitAuditResponse) {
        mView.hideLoading();
        if (submitAuditResponse.data.status.equals(Constants.STATUS_SUCCESS)) {
            if (mAuditType != Constants.COMMON_AREA_AUDIT_TYPE_ID && mAuditType != Constants.ROOM_AUDIT_TYPE_ID)
                mView.onPeriodicAuditSubmitSuccess(submitAuditResponse);
            else
                mView.onAuditSubmitSuccess(submitAuditResponse);
        } else {
            mView.onAuditSubmitFailure(submitAuditResponse.msg);
        }
    }
}
