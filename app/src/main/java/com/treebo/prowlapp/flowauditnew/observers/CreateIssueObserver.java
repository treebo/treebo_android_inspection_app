package com.treebo.prowlapp.flowauditnew.observers;

import com.treebo.prowlapp.flowauditnew.presenter.ConfirmActionPresenter;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 29/06/17.
 */

public class CreateIssueObserver extends BaseObserver<SubmitAuditResponse> {

    private ConfirmActionPresenter mPresenter;

    public CreateIssueObserver(ConfirmActionPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        this.mPresenter = presenter;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(SubmitAuditResponse submitAuditResponse) {
        if (submitAuditResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onIssueCreationSuccess();
        } else {
            mPresenter.onAPIFailure(submitAuditResponse.msg);
        }
    }
}
