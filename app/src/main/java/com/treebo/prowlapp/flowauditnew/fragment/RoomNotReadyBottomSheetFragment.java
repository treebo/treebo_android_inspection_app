package com.treebo.prowlapp.flowauditnew.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboTextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

/**
 * Created by abhisheknair on 09/11/16.
 */

public class RoomNotReadyBottomSheetFragment extends BottomSheetDialogFragment {

    private RoomV3Object mRoom;
    private RoomBottomSheetClickListener mListener;

    public static RoomNotReadyBottomSheetFragment newInstance(RoomV3Object roomModel) {

        RoomNotReadyBottomSheetFragment fragment = new RoomNotReadyBottomSheetFragment();
        Bundle args = new Bundle();
        args.putParcelable(Utils.ROOM_OBJECT, roomModel);
        fragment.setArguments(args);
        return fragment;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View rootView = View.inflate(getContext(), R.layout.layout_bottomsheet_room_not_available, null);
        dialog.setContentView(rootView);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) rootView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        mRoom = getArguments().getParcelable(Utils.ROOM_OBJECT);
        TreeboTextView bottomSheetTitle = (TreeboTextView) rootView.findViewById(R.id.room_name_type_bottomsheet_tv);
        bottomSheetTitle.setText(getString(R.string.room_name_bottomsheet, mRoom.getRoomNumber(), mRoom.getRoomType()));

        View closeBottomSheet = rootView.findViewById(R.id.ic_bottom_sheet_cross_room_na);
        closeBottomSheet.setOnClickListener(view -> dismiss());

        SwitchCompat switchCompatBtn = (SwitchCompat) rootView.findViewById(R.id.room_not_ready_switch);
        switchCompatBtn.setOnClickListener(view -> {
//            mRoom.setReady(!switchCompatBtn.isChecked());
            mListener.onNotReadySelected(mRoom, !switchCompatBtn.isChecked());
        });
    }

    public void setRoomBottomSheetClickListener(RoomBottomSheetClickListener listener) {
        this.mListener = listener;
    }

    public interface RoomBottomSheetClickListener {
        void onNotReadySelected(RoomV3Object model, boolean isReady);
    }
}
