package com.treebo.prowlapp.flowauditnew.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by abhisheknair on 09/11/16.
 */

public class AuditDoneAdapter extends RecyclerView.Adapter<AuditDoneAdapter.AuditDoneRoomHolder> {

    private ArrayList<RoomV3Object> mRoomList = new ArrayList<>();

    public AuditDoneAdapter(ArrayList<RoomV3Object> rooms) {
        this.mRoomList = rooms;
    }


    @Override
    public AuditDoneRoomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_audit_room_done, null);
        return new AuditDoneRoomHolder(view);
    }

    @Override
    public void onBindViewHolder(AuditDoneRoomHolder holder, int position) {
        RoomV3Object room = mRoomList.get(position);
        holder.mRoomNumber.setText(room.getRoomNumber());
        holder.mRoomType.setText(room.getRoomType());
        if (room.isPendingAudit)
            holder.submitErrorIcon.setVisibility(VISIBLE);
        else
            holder.submitErrorIcon.setVisibility(GONE);
    }

    @Override
    public int getItemCount() {
        return mRoomList.size();
    }

    class AuditDoneRoomHolder extends RecyclerView.ViewHolder {

        TreeboTextView mRoomNumber;
        TreeboTextView mRoomType;
        ImageView submitErrorIcon;

        AuditDoneRoomHolder(View itemView) {
            super(itemView);
            mRoomNumber = (TreeboTextView) itemView.findViewById(R.id.room_number_tv);
            mRoomType = (TreeboTextView) itemView.findViewById(R.id.room_type_tv);
            submitErrorIcon = (ImageView) itemView.findViewById(R.id.ic_submit_error);

        }
    }
}
