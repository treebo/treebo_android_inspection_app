package com.treebo.prowlapp.flowauditnew.observers;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.usecase.RoomNotReadyUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 07/12/16.
 */

public class RoomNotReadyObserver extends BaseObserver<BaseResponse> {

    private AuditContract.IDailyAuditView mView;
    private RoomNotReadyUseCase mUseCase;

    public RoomNotReadyObserver(BasePresenter presenter, RoomNotReadyUseCase useCase,
                                AuditContract.IDailyAuditView view) {
        super(presenter, useCase);
        this.mUseCase = useCase;
        this.mView = view;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onNext(BaseResponse baseResponse) {
        if (!baseResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mView.onRoomNotReadyFailure(mUseCase.getRoomID());
        }
    }
}
