package com.treebo.prowlapp.flowauditnew.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AuditStateChangeEvent;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.AuditNewComponent;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.DaggerAuditNewComponent;
import com.treebo.prowlapp.flowauditnew.adapter.AuditCardType2PagerAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.AuditSectionedCheckPointAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.SelectAreasAdapter;
import com.treebo.prowlapp.flowauditnew.presenter.AuditType2Presenter;
import com.treebo.prowlapp.flowdashboard.activity.DashboardActivity;
import com.treebo.prowlapp.flowdashboard.periodicstate.BasePeriodicAuditCard;
import com.treebo.prowlapp.flowdashboard.periodicstate.PeriodicStateUtils;
import com.treebo.prowlapp.flowupdateauditOrissue.AddCommentActivityNew;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.auditmodel.AuditSaveObject;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.DatabaseUtils;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.TypeFaceUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

import static android.view.View.VISIBLE;

/**
 * Created by sumandas on 19/01/2017.
 */

public class AuditCardType2Activity extends AppCompatActivity implements AuditContract.IAuditType2CardView
        , SelectAreasAdapter.onSelectAreasClickListener, AuditCardType2PagerAdapter.onDoneButtonClickListener, AuditSectionedCheckPointAdapter.onSubCheckPointClickListener {

    private static final int REQUEST_ADD_COMMENT = 100;
    private static final int REQUEST_RESOLVABLE_ISSUES = 102;
    private static final int REQUEST_CONFIRM_ACTIVITY = 104;
    private static final int VISIBLE_CARD_NUMBER = 7;

    private TaskModel mTaskModel;
    private Boolean mIsFromAuditAnyHotel;
    private int mHotelId;
    private String mHotelName;
    private int mAuditCategoryId;

    private ImageView mAuditIcon;
    private ProgressBar mAuditProgressBar;
    private TreeboTextView mAuditProgressStatus;
    private ViewPager mViewpager;

    private View mSelectAreaView;

    private RecyclerView mSelectAreasRecyclerView;

    private AuditCardType2PagerAdapter mCardPagerAdapter;

    private SelectAreasAdapter mSelectAreasAdapter;


    @Inject
    public AuditType2Presenter mAuditPresenter;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public AuditPrefManagerV3 mAuditManager;

    @Inject
    public RxBus mRxBus;

    @Inject
    public Navigator mNavigator;

    private View mLoadingView;

    boolean isInHotelLocation;

    boolean isFromIntermediateScreen;

    AuditSaveObject mAuditSavedObject;
    private boolean isFromCreateIssue;
    private boolean isRoom;
    private int mRoomID;

    private BasePeriodicAuditCard mBaseCard;

    private String mCurrentDate;
    private String mCurrentDateTime;

    private Checkpoint mCheckpointClicked;
    private SubCheckpoint mSubCheckpointSelected;

    private ArrayList<Category> mCategoryList = new ArrayList<>();
    private ArrayList<AnswerV3> mIssueList = new ArrayList<>();

    CompositeSubscription mSubsription = new CompositeSubscription();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_type2_card);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_progress);
        setSupportActionBar(toolbar);
        mLoadingView = findViewById(R.id.loader_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.card_activity_background));
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);


        AuditNewComponent auditComponent = DaggerAuditNewComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectAuditTypeAcitivity(this);

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            mTaskModel = intent.getParcelableExtra(Constants.TASK_MODEL);
            mCategoryList = intent.getParcelableArrayListExtra(Category.INCLUDED_CATEGORY_LIST);
            mAuditCategoryId = intent.getIntExtra(Constants.AUDIT_CATEGORY_ID, -1);
            isInHotelLocation = intent.getBooleanExtra(Utils.IS_IN_HOTEL, true);
            mHotelName = intent.getStringExtra(Utils.HOTEL_NAME);
            isFromIntermediateScreen = intent.getBooleanExtra(Utils.IS_FROM_INTERMEDIATE_SCREEN, false);
            mHotelId = intent.getIntExtra(Utils.HOTEL_ID, -1);
            isFromCreateIssue = intent.getBooleanExtra(Utils.IS_FROM_CREATE_ISSUE, false);
            mRoomID = intent.getIntExtra(Constants.ROOM_ID, -1);
            mIsFromAuditAnyHotel = intent.getBooleanExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL,false);
        } else {
            mTaskModel = savedInstanceState.getParcelable(Constants.TASK_MODEL);
            mCategoryList = savedInstanceState.getParcelableArrayList(Category.INCLUDED_CATEGORY_LIST);
            mAuditCategoryId = savedInstanceState.getInt(Constants.AUDIT_CATEGORY_ID, -1);
            isInHotelLocation = savedInstanceState.getBoolean(Utils.IS_IN_HOTEL, true);
            mHotelName = savedInstanceState.getString(Utils.HOTEL_NAME);
            isFromIntermediateScreen = savedInstanceState.getBoolean(Utils.IS_FROM_INTERMEDIATE_SCREEN, false);
            mHotelId = savedInstanceState.getInt(Utils.HOTEL_ID, -1);
            mCheckpointClicked = savedInstanceState.getParcelable(Utils.CLICKED_CHECKPOINT);
            mSubCheckpointSelected = savedInstanceState.getParcelable(Utils.CLICKED_SUBCHECKPOINT);
            isFromCreateIssue = savedInstanceState.getBoolean(Utils.IS_FROM_CREATE_ISSUE, false);
            mRoomID = savedInstanceState.getInt(Constants.ROOM_ID, -1);
            mIsFromAuditAnyHotel = savedInstanceState.getBoolean(Utils.IS_FROM_AUDIT_ANY_HOTEL,false);
        }

        isRoom = !(mRoomID == -1);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        mCurrentDate = sdf.format(calendar.getTime());
        SimpleDateFormat sdf_with_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale.getDefault());
        sdf_with_time.setTimeZone(TimeZone.getTimeZone("UTC"));
        mCurrentDateTime = sdf_with_time.format(calendar.getTime());

        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));

        mAuditIcon = (ImageView) findViewById(R.id.toolbar_room_number_tv);
        mAuditProgressBar = (ProgressBar) findViewById(R.id.toolbar_audit_progress_bar);
        mAuditProgressBar.setMax(mCategoryList.size());
        mAuditProgressStatus = (TreeboTextView) findViewById(R.id.toolbar_audit_progress_tv);

        View statusAreaText = findViewById(R.id.select_areas_tv);
        statusAreaText.setOnClickListener(view -> {
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SELECT_AREA_CLICK,
                    new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                            .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mTaskModel.getHeading())
                            .putValue(SegmentAnalyticsManager.COMPLETION_RATIO, String.valueOf(AuditUtilsV3.getAuditedCategoryCount(mCategoryList)) + "/" + mCategoryList.size()));
            mAuditPresenter.onSelectAreaClick();
        });
        View statusAreaDrawable = findViewById(R.id.ic_select_area);
        statusAreaDrawable.setOnClickListener(view -> mAuditPresenter.onSelectAreaClick());
        mViewpager = (ViewPager) findViewById(R.id.audit_card_viewpager);
        mSelectAreaView = findViewById(R.id.select_areas_layout);


        if (mSelectAreaView != null) {
            View closeSelectAreasCross = mSelectAreaView.findViewById(R.id.ic_cross_select_areas);
            closeSelectAreasCross.setOnClickListener(view -> {
                mAuditPresenter.onBackPressed();
            });
            mSelectAreasRecyclerView = (RecyclerView) mSelectAreaView.findViewById(R.id.select_areas_rv);
            mSelectAreasRecyclerView.setItemAnimator(new SlideInLeftAnimator());
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
            layoutManager1.setOrientation(RecyclerView.VERTICAL);
            mSelectAreasRecyclerView.setLayoutManager(layoutManager1);
            mSelectAreasRecyclerView.setItemAnimator(new DefaultItemAnimator());
        }

        View backBtn = findViewById(R.id.toolbar_audit_back_btn);
        if (backBtn != null)
            backBtn.setOnClickListener((View v) -> mAuditPresenter.onBackPressed());


        mSubsription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                ArrayList<HotelLocation> hotelList = ((HotelChangedEvent) event).mHotelList;
                if (hotelList.size() == 0) {
                    finish();
                }
            }
        }));

        mAuditProgressBar.setMax(mCategoryList.size());
        mAuditSavedObject = mAuditManager.getSavedAudit(AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryId, null));
        if (mAuditSavedObject != null && !isFromCreateIssue) {
            prefillIssues(mCategoryList, mAuditSavedObject);
        } else {
            mAuditPresenter.onCreate();
        }

    }

    @Inject
    public void setUp() {
        mAuditPresenter.setMvpView(this);
    }

    public static Intent getCallingIntent(Activity activity) {
        return new Intent(activity, AuditCardType2Activity.class);
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    private void prefillIssues(ArrayList<Category> categoryList, AuditSaveObject auditSaveObject) {
        Observable observable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                AuditUtilsV3.prefillSavedIssues(categoryList, auditSaveObject);
                subscriber.onCompleted();
            }
        });
        RxUtils.build(observable)
                .subscribe(new Observer() {
                    @Override
                    public void onCompleted() {
                        mAuditPresenter.onCreate();
                        prefillProgressBar();
                        SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_RESUMED,
                                new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                                        .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mTaskModel.getHeading())
                                        .putValue(SegmentAnalyticsManager.COMPLETION_RATIO, String.valueOf(AuditUtilsV3.getAuditedCategoryCount(mCategoryList)) + "/" + mCategoryList.size()));

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {

                    }
                });
    }


    @Override
    public void viewLoadPager() {
        mViewpager.setClipToPadding(false);
        mViewpager.setPageMargin(50);
        mCardPagerAdapter = new AuditCardType2PagerAdapter(mCategoryList, this, this, mSubsription);
        mViewpager.setAdapter(mCardPagerAdapter);
        mCardPagerAdapter.notifyDataSetChanged();

        mSelectAreasAdapter = new SelectAreasAdapter(mCategoryList, this, this);
        mSelectAreasRecyclerView.setAdapter(mSelectAreasAdapter);
        mSelectAreasAdapter.notifyItemRangeChanged(0, mCategoryList.size());

        if (AuditUtilsV3.getNonAuditDoneCategories(mCategoryList).size() == 0)
            mSelectAreaView.setVisibility(VISIBLE);
        else
            mSelectAreaView.setVisibility(View.GONE);
    }


    @Override
    public void setToolbarInfo() {
        mBaseCard = PeriodicStateUtils.getCard(this, mTaskModel);
        mAuditIcon.setImageDrawable(ContextCompat.getDrawable(this, PeriodicStateUtils.getSubAuditIcon(mAuditCategoryId)));
        mAuditProgressBar.setProgress(0);
        mAuditProgressStatus.setText("0/" + mCategoryList.size());
    }

    @Override
    public void toggleSelectAreasVisibility() {
        if (mSelectAreaView != null) {
            if (mSelectAreaView.getVisibility() == VISIBLE) {
                mSelectAreaView.setVisibility(View.GONE);
            } else {
                mSelectAreaView.setVisibility(VISIBLE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        mAuditPresenter.onBackPressed();
    }

    @Override
    public void backPressClicked() {
        int auditDone = AuditUtilsV3.getAuditedCategoryCount(mCategoryList);
        if (isFromCreateIssue) {
            if (mIssueList.size() > 0) {
                showPauseAuditDialog();
            } else {
                finish();
            }
        } else {
            if (mSelectAreaView.getVisibility() == VISIBLE && mCategoryList.size() > 0) {
                if (mCategoryList.size() - auditDone == 0) {
                    ArrayList<AnswerV3> resolvableIssueList =
                            getResolvableIssueList(mAuditManager.getAllIssuesRaisedInARoom(mAuditCategoryId, mHotelId, null));
                    if (resolvableIssueList.size() > 0)
                        startResolvableIssuesActivity(resolvableIssueList);
                    else
                        startConfirmActivity();
                }
                mSelectAreaView.setVisibility(View.GONE);
            } else {
                if (auditDone > 0)
                    showPauseAuditDialog();
                else
                    finish();
            }
        }
    }


    private void showPauseAuditDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_pause_audit, null);
        TreeboTextView dialogText = (TreeboTextView) dialogView.findViewById(R.id.pause_dialog_text);
        dialogText.setText(isFromCreateIssue ? getString(R.string.pause_issue_creation)
                : getString(R.string.pause_dialog_text, mBaseCard.getAuditText()));
        dialogBuilder.setView(dialogView);
        final AlertDialog pauseAuditDialog = dialogBuilder.create();
        pauseAuditDialog.show();

        TreeboTextView notNowBtn = (TreeboTextView) dialogView.findViewById(R.id.not_now_btn);
        notNowBtn.setOnClickListener(view -> pauseAuditDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            pauseAuditDialog.dismiss();
            if (!isFromCreateIssue) {
                SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_PAUSED,
                        new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                                .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mTaskModel.getHeading())
                                .putValue(SegmentAnalyticsManager.COMPLETION_RATIO, String.valueOf(AuditUtilsV3.getAuditedCategoryCount(mCategoryList)) + "/" + mCategoryList.size()));
                Intent intent = new Intent(this, DashboardActivity.class);
                intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
                intent.putExtra(Utils.HOTEL_ID, mHotelId);
                intent.putExtra(Utils.HOTEL_NAME, mHotelName);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
            finish();
        });

    }


    @Override
    public void selectAreasClicked(Category category) {
        int index = AuditUtilsV3.getIndexOfCardCategory(category.getCategoryId(), mCategoryList);
        if (category.isAuditDone()) {
            category.setAuditDone(false);
            mCardPagerAdapter.addCard(mCategoryList.get(index));
            mCategoryList.set(index, category);
            mSelectAreasAdapter.updateItemInList(category);
            updateCategoryFine(category, false);
            prefillProgressBar();
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SELECT_AREA_SELECTED,
                    new Properties().putValue(SegmentAnalyticsManager.CATEGORY_SELECTED, category.getDescription()));
        }
        mViewpager.setCurrentItem(index - AuditUtilsV3.getAuditedCategoryCount(mCategoryList));
        mSelectAreaView.setVisibility(View.GONE);
    }


    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }


    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }


    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }


    @Override
    public void updateProgressBar(Category category) {
        int index = AuditUtilsV3.getIndexOfCardCategory(category.getCategoryId(), mCategoryList);
        category.setAuditDone(true);
        mCategoryList.set(index, category);
        int cardsDone = AuditUtilsV3.getAuditedCategoryCount(mCategoryList);
        mAuditProgressBar.setProgress(cardsDone);
        mAuditProgressStatus.setText(String.valueOf(cardsDone) +
                "/" + mCategoryList.size());

        mSelectAreasAdapter.updateItemInList(category);
        if (mCategoryList.size() - cardsDone == 0) {
            ArrayList<AnswerV3> resolvableIssueList =
                    getResolvableIssueList(mAuditManager.getAllIssuesRaisedInARoom(mAuditCategoryId, mHotelId, null));
            if (resolvableIssueList.size() > 0 && !isFromCreateIssue)
                startResolvableIssuesActivity(resolvableIssueList);
            else
                startConfirmActivity();
        }
    }

    public void prefillProgressBar() {
        int cardsDone = AuditUtilsV3.getAuditedCategoryCount(mCategoryList);
        mAuditProgressBar.setProgress(cardsDone);
        mAuditProgressStatus.setText(String.valueOf(cardsDone) +
                "/" + mCategoryList.size());
    }

    @Override
    public void updateCategoryFine(Category category, boolean isAuditDone) {
        if (!isFromCreateIssue) {
            String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryId, null);

            if (isAuditDone) {
                AnswerV3 allFineCategory = new AnswerV3(category.getCategoryId(), true);
                mAuditManager.saveAuditCheckPoint(key, allFineCategory);
            } else {
                AnswerV3 allFineCategory = new AnswerV3(category.getCategoryId(), true);
                mAuditManager.removeAuditDoneCheckPoint(key, allFineCategory);
            }
            mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.CATEGORY_DONE));
        }
    }

    private ArrayList<AnswerV3> getResolvableIssueList(ArrayList<AnswerV3> allIssues) {
        ArrayList<AnswerV3> resolvableIssues = new ArrayList<>();
        for (AnswerV3 issue : allIssues)
            if (!issue.getIs_instantly_resolved() && issue.getTat() <= Constants.RESOLVABLE_TAT) {
                resolvableIssues.add(issue);
            }
        return resolvableIssues;
    }

    @Override
    public void subCheckPointClicked(SubCheckpoint subCheckpoint, Checkpoint checkpoint) {
        mSubCheckpointSelected = subCheckpoint;
        mCheckpointClicked = checkpoint;
        if (subCheckpoint.isCommentRequired()) {
            Intent intent = new Intent(this, AddCommentActivityNew.class);
            intent.putExtra(Utils.AUDIT_BAD_TITLE, subCheckpoint.getmDescription());
            intent.putExtra(Utils.ISSUE_TEXT, subCheckpoint.getIssueText());
            intent.putExtra(Utils.PHOTO_URL_LIST, subCheckpoint.getPhotoUrls());
            startActivityForResult(intent, REQUEST_ADD_COMMENT);
        } else {
            markIssueAndCreateIssueObject("", "");
        }
    }

    @Override
    public void subCheckPointLongPress(SubCheckpoint subCheckpoint, Checkpoint checkpoint) {
        if (subCheckpoint.issue()) {
            mCheckpointClicked = checkpoint;
            mSubCheckpointSelected = subCheckpoint;
            showUnmarkIssueDialog(checkpoint.isPendingIssue(), checkpoint, subCheckpoint);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ADD_COMMENT:
                if (resultCode == RESULT_OK) {
                    // Update Checkpoint UI
                    String issueText = data.getStringExtra(Utils.ISSUES_TEXT);
                    String imageUrlList = data.getStringExtra(Utils.PHOTO_URL_LIST);
                    markIssueAndCreateIssueObject(issueText, imageUrlList);

                }
                break;

            case REQUEST_RESOLVABLE_ISSUES:
                if (resultCode == RESULT_OK) {
                    startConfirmActivity();
                } else {
                    mSelectAreaView.setVisibility(VISIBLE);
                }
                break;

            case REQUEST_CONFIRM_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    if (isFromIntermediateScreen) {
                        Intent intent = AuditPeriodicActivity.getCallingIntent(this);
                        intent.putExtra(Constants.TASK_MODEL, mTaskModel);
                        intent.putExtra(Utils.HOTEL_ID, mHotelId);
                        intent.putExtra(Constants.AUDIT_CATEGORY_ID, mAuditCategoryId);
                        intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
                        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL, mIsFromAuditAnyHotel);
                        startActivity(intent);
                    }
                    finish();
                } else {
                    mSelectAreaView.setVisibility(VISIBLE);
                }
                break;

        }
    }

    private void showUnmarkIssueDialog(boolean isPremarkedIssue, Checkpoint checkpoint, SubCheckpoint subCheckpoint) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_unmark_issues, null);
        TreeboTextView titleText = (TreeboTextView) dialogView.findViewById(R.id.checkpoint_name_tv);
        titleText.setText(subCheckpoint.getmDescription());
        TreeboTextView subtitleText = (TreeboTextView) dialogView.findViewById(R.id.unmark_text_tv);
        subtitleText.setText(isPremarkedIssue ? getString(R.string.cant_unmark_this_issue) : getString(R.string.unmark_issue));
        if (isPremarkedIssue) {
            subtitleText.setTypeface(TypeFaceUtils.get(this, "Roboto-Italic.ttf"));
        }

        dialogBuilder.setView(dialogView);
        final AlertDialog unmarkIssueDialog = dialogBuilder.create();
        unmarkIssueDialog.show();

        View closeBtn = dialogView.findViewById(R.id.close_iv);
        closeBtn.setOnClickListener(view -> unmarkIssueDialog.dismiss());
        subtitleText.setOnClickListener(view -> {
            if (!isPremarkedIssue) {
                String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryId, null);
                if (!isFromCreateIssue) {
                    mAuditManager.removeCheckPointFromSavedList(key, checkpoint);
                } else {
                    for (AnswerV3 answerV3 : mIssueList) {
                        if (checkpoint.getmDescription().equals(answerV3.getCheckpointText())) {
                            mIssueList.remove(answerV3);
                            break;
                        }
                    }
                }
                updateCategoryList(false);
                mCheckpointClicked.setIssue(false);
                mSubCheckpointSelected.setIssue(false);
                View cardView = ((AuditCardType2PagerAdapter) mViewpager.getAdapter()).getCurrentView();
                if (cardView != null) {
                    RecyclerView checkpointRv = (RecyclerView) cardView.findViewById(R.id.audit_card_checkbox_list);
                    ((AuditSectionedCheckPointAdapter) checkpointRv.getAdapter()).refreshItem(mCheckpointClicked, mSubCheckpointSelected);
                    TreeboButton treeboBtn = (TreeboButton) cardView.findViewById(R.id.audit_done_btn);
                    if (mAuditManager.getSavedAudit(key) != null
                            || mAuditManager.getSavedAudit(key).mAnswerList == null
                            || mAuditManager.getSavedAudit(key).mAnswerList.size() == 0)
                        treeboBtn.setText(getString(R.string.everything_is_fine));
                    else
                        treeboBtn.setText(getString(R.string.everything_else_is_fine));
                }
            }
            unmarkIssueDialog.dismiss();
        });

    }

    private void markIssueAndCreateIssueObject(String issueText, String imageUrlList) {
        updateCategoryList(true);
        mSubCheckpointSelected.setIssue(true);
        mSubCheckpointSelected.setIssueText(issueText);
        mSubCheckpointSelected.setPhotoUrls(imageUrlList);
        mCheckpointClicked.setIssue(true);
        mCheckpointClicked.setIssueText(mSubCheckpointSelected.getmDescription());
        String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryId, null);
        int categoryId = mCheckpointClicked.getmCategoryId();
        AnswerV3 issueRaisedObject = new AnswerV3(categoryId, mCheckpointClicked.getmCheckpointId(),
                mSubCheckpointSelected.getmSubCheckpointId(), issueText, imageUrlList, false);
        issueRaisedObject.setCheckpointText(mCheckpointClicked.getmDescription());
        issueRaisedObject.setSubcheckpointText(mSubCheckpointSelected.getmDescription());
        issueRaisedObject.setCategoryText(DatabaseUtils.getCategoryText(mCheckpointClicked.getmCategoryId()));
        issueRaisedObject.setCreatedAt(mCurrentDate);
        issueRaisedObject.setTat(mSubCheckpointSelected.getmTurnaroundTime());
        if (!isFromCreateIssue) {
            mAuditManager.saveAuditCheckPoint(key, issueRaisedObject);
        } else {
            mIssueList.add(issueRaisedObject);
        }
        View cardView = ((AuditCardType2PagerAdapter) mViewpager.getAdapter()).getCurrentView();
        if (cardView != null) {
            RecyclerView checkpointRv = (RecyclerView) cardView.findViewById(R.id.audit_card_checkbox_list);
            ((AuditSectionedCheckPointAdapter) checkpointRv.getAdapter()).refreshItem(mCheckpointClicked, mSubCheckpointSelected);
            TreeboButton treeboBtn = (TreeboButton) cardView.findViewById(R.id.audit_done_btn);
            if (isFromCreateIssue) {
                treeboBtn.setText(getString(R.string.everything_else_is_fine));
            } else if (mAuditManager.getSavedAudit(key).mAnswerList == null
                    || mAuditManager.getSavedAudit(key).mAnswerList.size() == 0) {
                treeboBtn.setText(getString(R.string.everything_is_fine));
            } else {
                treeboBtn.setText(getString(R.string.everything_else_is_fine));
            }
        }
        mRxBus.postEvent(new AuditStateChangeEvent(AuditStateChangeEvent.ISSUE_CREATED));
    }

    private void updateCategoryList(boolean set) {
        int cardIndex = AuditUtilsV3.getIndexOfCardCategory(mCheckpointClicked.getmCategoryId(), mCategoryList);
        int checkPointIndexInCategory = AuditUtilsV3.getIndexOfCheckpointInCategory(mCheckpointClicked.getmCheckpointId(),
                mCategoryList.get(cardIndex).getCheckpoints());
        mCategoryList.get(cardIndex).setIssueRaised(set);
        mCategoryList.get(cardIndex).getmCheckPoints().get(checkPointIndexInCategory).setIssue(set);
        mSelectAreasAdapter.updateItemInList(mCategoryList.get(cardIndex));
    }

    private void startResolvableIssuesActivity(ArrayList<AnswerV3> issueList) {
        Intent intent = new Intent(this, ResolvableIssuesActivity.class);
        intent.putParcelableArrayListExtra(Utils.ISSUES_LIST, issueList);
        intent.putExtra(Constants.AUDIT_TYPE, mAuditCategoryId);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        intent.putExtra(Utils.HOTEL_ID, mHotelId);
        startActivityForResult(intent, REQUEST_RESOLVABLE_ISSUES);
    }

    private void startConfirmActivity() {
        Intent intent = new Intent(this, ConfirmActionActivity.class);
        intent.putExtra(Constants.AUDIT_SUBMISSION, !isFromCreateIssue);
        intent.putParcelableArrayListExtra(Constants.ISSUE_LIST,
                isFromCreateIssue ? mIssueList
                        : mAuditManager.getAllIssuesRaisedInARoom(mAuditCategoryId, mHotelId, null));
        intent.putExtra(Utils.HOTEL_ID, mHotelId);
        intent.putExtra(Constants.TASK_MODEL, mTaskModel);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        intent.putExtra(Utils.IS_FROM_CREATE_ISSUE, isFromCreateIssue);
        intent.putExtra(Constants.AUDIT_TYPE, mAuditCategoryId);
        intent.putExtra(Utils.START_TIME,mCurrentDateTime);
        intent.putExtra(Utils.IS_FROM_AUDIT_ANY_HOTEL,mIsFromAuditAnyHotel);
        if (isRoom)
            intent.putExtra(Constants.ROOM_ID, mRoomID);
        startActivityForResult(intent, REQUEST_CONFIRM_ACTIVITY);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubsription);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(Category.INCLUDED_CATEGORY_LIST, mCategoryList);
        outState.putBoolean(Utils.IS_IN_HOTEL, isInHotelLocation);
        outState.putString(Utils.HOTEL_NAME, mHotelName);
        outState.putInt(Constants.AUDIT_CATEGORY_ID, mAuditCategoryId);
        outState.putParcelable(Constants.TASK_MODEL, mTaskModel);
        outState.putBoolean(Utils.IS_FROM_INTERMEDIATE_SCREEN, isFromIntermediateScreen);
        outState.putInt(Utils.HOTEL_ID, mHotelId);
        outState.putParcelable(Utils.CLICKED_CHECKPOINT, mCheckpointClicked);
        outState.putParcelable(Utils.CLICKED_SUBCHECKPOINT, mSubCheckpointSelected);
        super.onSaveInstanceState(outState);
    }
}
