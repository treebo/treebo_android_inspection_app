package com.treebo.prowlapp.flowauditnew.presenter;

import com.treebo.prowlapp.flowauditnew.ResolvableIssuesActivityContract;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 08/11/16.
 */

public class ResolvableIssuesPresenter implements ResolvableIssuesActivityContract.IResolvableIssuesPresenter {

    private ResolvableIssuesActivityContract.IResolvableIssuesView mView;

    @Override
    public void onCreate() {
        if (mView != null) {
            mView.setUpIssuesRecyclerView();
        }
    }

    @Override
    public void onSkipClick() {
        if (mView != null) {
            mView.finishActivity(true);
        }
    }

    @Override
    public void onCloseActivityClick() {
        if (mView != null) {
            mView.finishActivity(false);
        }
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView = (ResolvableIssuesActivityContract.IResolvableIssuesView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mView;
    }
}
