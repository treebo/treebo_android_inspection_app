package com.treebo.prowlapp.flowauditnew.observers;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.usecase.SubmitFraudAuditUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.audit.FraudAuditSubmitResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 20/01/17.
 */

public class FraudAuditSubmitObserver extends BaseObserver<FraudAuditSubmitResponse> {
    private AuditContract.IConfirmAuditActionView mView;

    public FraudAuditSubmitObserver(BasePresenter presenter,
                                    SubmitFraudAuditUseCase useCase, AuditContract.IConfirmAuditActionView view) {
        super(presenter, useCase);
        mView = view;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(FraudAuditSubmitResponse submitAuditResponse) {
        mView.hideLoading();
        if (submitAuditResponse.data.status.equals(Constants.STATUS_SUCCESS)) {
            mView.onFraudAuditSubmitSuccess(submitAuditResponse);
        } else {
            mView.onAuditSubmitFailure(submitAuditResponse.msg);
        }
    }
}
