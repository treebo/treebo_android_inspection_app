package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by sumandas on 10/11/2016.
 */
public class RoomSingleUseCase extends UseCase<RoomSingleResponse> {

    private RestClient mRestClient;
    private int mHotelId;
    private int mRoomId;

    public RoomSingleUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setFields(int hotelId, int roomId) {
        mHotelId = hotelId;
        mRoomId = roomId;
    }

    @Override
    protected Observable<RoomSingleResponse> getObservable() {
        return mRestClient.getAuditTypeRoom(mHotelId, mRoomId);
    }
}
