package com.treebo.prowlapp.flowauditnew.observers;

import com.treebo.prowlapp.flowauditnew.presenter.FraudAuditPresenter;
import com.treebo.prowlapp.response.audit.FraudAuditResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by abhisheknair on 17/01/17.
 */

public class FraudAuditObserver extends BaseObserver<FraudAuditResponse> {

    private FraudAuditPresenter mPresenter;

    public FraudAuditObserver(FraudAuditPresenter presenter, UseCase useCase) {
        super(presenter, useCase);
        mPresenter = presenter;
    }

    @Override
    public void onCompleted() {
        mPresenter.stopLoadingScreen();
    }

    @Override
    public void onNext(FraudAuditResponse fraudAuditResponse) {
        mPresenter.stopLoadingScreen();
        if (fraudAuditResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mPresenter.onLoadFraudAuditSuccess(fraudAuditResponse);
        } else {
            mPresenter.onLoadFraudAuditFailure(fraudAuditResponse.msg);
        }
    }
}
