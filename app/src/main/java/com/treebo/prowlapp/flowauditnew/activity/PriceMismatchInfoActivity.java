package com.treebo.prowlapp.flowauditnew.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowauditnew.AuditNewComponent;
import com.treebo.prowlapp.flowauditnew.DaggerAuditNewComponent;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboEditText;
import com.treebo.prowlapp.views.TreeboTextView;

/**
 * Created by abhisheknair on 21/02/17.
 */

public class PriceMismatchInfoActivity extends AppCompatActivity {

    private TreeboEditText mHotelPriceInputTv;
    private TreeboTextView mHotelPriceTagTv;
    private TreeboEditText mTreeboPriceInputTv;
    private TreeboTextView mTreeboPriceTagTv;
    private View mErrorView;

    private String mRoomNumber;
    private String mReason;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_mismatch_details);
        setToolBarColor(R.color.white);

        AuditNewComponent auditComponent = DaggerAuditNewComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectPriceMismatchActivity(this);

        int priceInHotel, priceInTreebo;
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            priceInHotel = intent.getIntExtra(Utils.PRICE_IN_HOTEL, 0);
            priceInTreebo = intent.getIntExtra(Utils.PRICE_IN_TREEBO, 0);
            mRoomNumber = intent.getStringExtra(Utils.ROOM_NAME);
            mReason = intent.getStringExtra(Utils.FRAUD_REASON_TEXT);
        } else {
            priceInHotel = savedInstanceState.getInt(Utils.PRICE_IN_HOTEL, 0);
            priceInTreebo = savedInstanceState.getInt(Utils.PRICE_IN_TREEBO, 0);
            mRoomNumber = savedInstanceState.getString(Utils.ROOM_NAME);
            mReason = savedInstanceState.getString(Utils.FRAUD_REASON_TEXT);
        }

        View toolbar = findViewById(R.id.toolbar_update_issue);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        TreeboTextView title = (TreeboTextView) toolbar.findViewById(R.id.toolbar_title_text);
        title.setText(Constants.PRICE_MISMATCH_AUDIT);
        View crossBtn = toolbar.findViewById(R.id.room_picker_back);
        crossBtn.setOnClickListener(view -> onBackPressed());


        mHotelPriceInputTv = (TreeboEditText) findViewById(R.id.price_in_hotel_editext);
        mHotelPriceTagTv = (TreeboTextView) findViewById(R.id.price_in_hotel_title);

        mTreeboPriceInputTv = (TreeboEditText) findViewById(R.id.price_in_treebo_edittext);
        mTreeboPriceTagTv = (TreeboTextView) findViewById(R.id.price_in_treebo_title_text);
        mErrorView = findViewById(R.id.price_in_treebo_error_tv);

        mHotelPriceInputTv.addTextChangedListener(new HotelPriceTextWatcher());
        mTreeboPriceInputTv.addTextChangedListener(new TreeboPriceTextWatcher());

        TreeboButton doneBtn = (TreeboButton) findViewById(R.id.done_btn);
        doneBtn.setOnClickListener(view -> {
            if (TextUtils.isEmpty(mTreeboPriceInputTv.getText()) || TextUtils.isEmpty(mHotelPriceInputTv.getText())
                    || mTreeboPriceInputTv.getText().toString().equals(mHotelPriceInputTv.getText().toString())) {
                setError(true);
            } else {
                Intent intent = new Intent();
                intent.putExtra(Utils.PRICE_IN_HOTEL, Integer.valueOf(mHotelPriceInputTv.getText().toString()));
                intent.putExtra(Utils.PRICE_IN_TREEBO, Integer.valueOf(mTreeboPriceInputTv.getText().toString()));
                intent.putExtra(Utils.ROOM_NAME, mRoomNumber);
                intent.putExtra(Utils.FRAUD_REASON_TEXT, mReason);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        if (priceInHotel > 0)
            mHotelPriceInputTv.setText(String.valueOf(priceInHotel));
        if (priceInTreebo > 0)
            mTreeboPriceInputTv.setText(String.valueOf(priceInTreebo));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    private void setError(boolean isError) {
        mErrorView.setVisibility(isError ? View.VISIBLE : View.GONE);
        int resID = isError ? R.drawable.bg_rectangle_border_red : R.drawable.bg_edittext_selector;
        mTreeboPriceInputTv.setBackground(ContextCompat.getDrawable(this, resID));
    }

    private void setEditTextBackGround(boolean enabled, TreeboEditText textView, TreeboTextView title) {
        title.setVisibility(!enabled ? View.GONE : View.VISIBLE);
        int resID = !enabled ? R.drawable.bg_rectangle_border_gray : R.drawable.bg_rectangle_border_green;
        textView.setBackground(ContextCompat.getDrawable(this, resID));
    }

    private class TreeboPriceTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() == 0) {
                mTreeboPriceInputTv.setHint(getString(R.string.price_in_treebo_pms));
                setEditTextBackGround(false, mTreeboPriceInputTv, mTreeboPriceTagTv);
            } else if (editable.length() > 0 && editable.length() < 2) {
                setError(false);
                setEditTextBackGround(true, mTreeboPriceInputTv, mTreeboPriceTagTv);
            } else {
                setError(false);
                setEditTextBackGround(true, mTreeboPriceInputTv, mTreeboPriceTagTv);
            }
        }
    }

    private class HotelPriceTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() == 0) {
                mHotelPriceInputTv.setHint(getString(R.string.price_in_hotel));
                setEditTextBackGround(false, mHotelPriceInputTv, mHotelPriceTagTv);
            } else if (editable.length() > 0 && editable.length() < 2) {
                setError(false);
                setEditTextBackGround(true, mHotelPriceInputTv, mHotelPriceTagTv);
            } else {
                setError(false);
                setEditTextBackGround(true, mHotelPriceInputTv, mHotelPriceTagTv);
            }
        }
    }
}
