package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.issuemodel.SectionedResolvableIssuesModel;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by abhisheknair on 08/11/16.
 */

public class ResolvableIssuesAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private onResolveBtnClickListener mListener;
    private ArrayList<SectionedResolvableIssuesModel> mTimeSections;
    private Context mContext;

    public ResolvableIssuesAdapter(Context context, ArrayList<SectionedResolvableIssuesModel> issueList, onResolveBtnClickListener listener) {
        mContext = context;
        mTimeSections = issueList;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_resolvable_issues,
                    parent, false);
            return new ResolvableIssuesViewHolder(view, mListener);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_header,
                    parent, false);
            return new SectionViewHolder(view);
        }
    }

    @Override
    public int getSectionCount() {
        if (mTimeSections == null)
            return 0;
        else
            return mTimeSections.size();
    }

    @Override
    public int getItemCount(int i) {
        if (mTimeSections == null)
            return 0;
        else
            return mTimeSections.get(i).getIssueList().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        String sectionName = mTimeSections.get(i).getSectionName();
        ((SectionViewHolder) viewHolder).sectionTitle.setText(sectionName);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int i1, int i2) {
        AnswerV3 issue = mTimeSections.get(section).getIssueList().get(i1);
        if (issue.isPendingDelete) {
            ((ResolvableIssuesViewHolder) holder).pendingDelete.setVisibility(View.VISIBLE);
            ((ResolvableIssuesViewHolder) holder).mainLayout.setVisibility(View.INVISIBLE);
        } else {
            ((ResolvableIssuesViewHolder) holder).pendingDelete.setVisibility(View.GONE);
            ((ResolvableIssuesViewHolder) holder).mainLayout.setVisibility(View.VISIBLE);
            ((ResolvableIssuesViewHolder) holder).issue = issue;
            ((ResolvableIssuesViewHolder) holder).issueCategory.setText(mContext.getString(R.string.issue_heading,
                    issue.getCategoryText(), issue.getCheckpointText()));
            ((ResolvableIssuesViewHolder) holder).issueCause.setText(issue.getSubcheckpointText());
        }
    }

    private class SectionViewHolder extends RecyclerView.ViewHolder {

        TreeboTextView sectionTitle;

        SectionViewHolder(View itemView) {
            super(itemView);
            sectionTitle = (TreeboTextView) itemView.findViewById(R.id.item_section_header_tv);
        }
    }

    private class ResolvableIssuesViewHolder extends RecyclerView.ViewHolder {

        AnswerV3 issue;
        TreeboTextView issueCategory;
        TreeboTextView issueCause;
        TreeboTextView resolveBtn;
        View pendingDelete;
        View undoBtn;
        View mainLayout;

        ResolvableIssuesViewHolder(View itemView, onResolveBtnClickListener listener) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.main_layout);
            issueCategory = (TreeboTextView) itemView.findViewById(R.id.resolvable_issues_item_title);
            issueCause = (TreeboTextView) itemView.findViewById(R.id.resolvable_issues_item_text);
            resolveBtn = (TreeboTextView) itemView.findViewById(R.id.resolvable_issues_item_btn);
            pendingDelete = itemView.findViewById(R.id.resolved_layout);
            undoBtn = itemView.findViewById(R.id.undo_btn);
            undoBtn.setOnClickListener(view -> listener.undoBtnClicked());
            resolveBtn.setOnClickListener(view -> listener.resolveBtnClicked(issue));
        }
    }

    public interface onResolveBtnClickListener {
        void resolveBtnClicked(AnswerV3 issue);

        void undoBtnClicked();
    }
}
