package com.treebo.prowlapp.flowauditnew;

import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by abhisheknair on 08/11/16.
 */

public interface ResolvableIssuesActivityContract {

    interface IResolvableIssuesView extends BaseView {
        void setUpIssuesRecyclerView();

        void finishActivity(boolean isSkip);
    }

    interface IResolvableIssuesPresenter extends BasePresenter {
        void onCreate();

        void onSkipClick();

        void onCloseActivityClick();
    }
}
