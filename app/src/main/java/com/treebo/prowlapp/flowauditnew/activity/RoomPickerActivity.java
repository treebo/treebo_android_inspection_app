package com.treebo.prowlapp.flowauditnew.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.AuditNewComponent;
import com.treebo.prowlapp.flowauditnew.DaggerAuditNewComponent;
import com.treebo.prowlapp.flowauditnew.adapter.BaseRoomAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.OccupiedRoomAdapter;
import com.treebo.prowlapp.flowauditnew.presenter.RoomListPresenter;
import com.treebo.prowlapp.flowauditnew.usecase.RoomListUseCase;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.subscriptions.CompositeSubscription;

import static com.treebo.prowlapp.Utils.Constants.AUDIT_SUBMISSION;

/**
 * Created by sumandas on 27/10/2016.
 */

public class RoomPickerActivity extends AppCompatActivity implements BaseRoomAdapter.onRoomClickListener,
        OccupiedRoomAdapter.onOccupiedRoomClickListener, AuditContract.IRoomListView {

    private static final int REQUEST_AUDIT_ROOM = 130;

    private RecyclerView mAvailableRecyclerView;
    private BaseRoomAdapter mAvailableRoomAdapter;

    private RecyclerView mOccupiedRoomsRecyclerView;
    private OccupiedRoomAdapter mOccupiedRoomAdapter;

    private View mRootView;

    @Inject
    Navigator mNavigator;

    @Inject
    RoomListPresenter mRoomListPresenter;

    @Inject
    LoginSharedPrefManager mLoginSharedPrefManager;

    @Inject
    RoomListUseCase mRoomListUseCase;

    @Inject
    LoginSharedPrefManager mLoginManager;

    @Inject
    RxBus mRxBus;

    private ArrayList<RoomV3Object> mAvailableRoomList = new ArrayList<>();
    private ArrayList<RoomV3Object> mOccupiedRoomList = new ArrayList<>();

    private String mClickedRoomNumber;

    private RoomV3Object mRoom;
    private String mHotelName;
    private boolean isInHotelLocation;

    CompositeSubscription mSubsription = new CompositeSubscription();
    private int mHotelID;
    private TaskModel mTaskModel;
    private View mLoaderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_picker);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.grey_toolbar));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_select_room);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        mHotelName = getIntent().getStringExtra(Utils.HOTEL_NAME);
        mHotelID = getIntent().getIntExtra(Utils.HOTEL_ID, -1);
        mTaskModel = getIntent().getParcelableExtra(Constants.TASK_MODEL);
        isInHotelLocation = getIntent().getBooleanExtra(Utils.IS_IN_HOTEL, false);

        mRootView = findViewById(R.id.activity_room_picker_root);
        mLoaderView = findViewById(R.id.loader_layout);
        AuditNewComponent auditComponent = DaggerAuditNewComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectRoomPickerActivity(this);

        ImageView backButton = (ImageView) findViewById(R.id.room_picker_back);
        backButton.setOnClickListener(view -> finish());

        mAvailableRecyclerView = (RecyclerView) findViewById(R.id.available_list);
        mOccupiedRoomsRecyclerView = (RecyclerView) findViewById(R.id.occupied_list);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mAvailableRecyclerView.setLayoutManager(layoutManager);
        mAvailableRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAvailableRoomAdapter = new BaseRoomAdapter(this, mAvailableRoomList, true);
        mAvailableRecyclerView.setAdapter(mAvailableRoomAdapter);
        mAvailableRoomAdapter.notifyDataSetChanged();

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        layoutManager1.setOrientation(RecyclerView.VERTICAL);
        mOccupiedRoomsRecyclerView.setLayoutManager(layoutManager1);
        mOccupiedRoomsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mOccupiedRoomAdapter = new OccupiedRoomAdapter(this, mOccupiedRoomList);
        mOccupiedRoomsRecyclerView.setAdapter(mOccupiedRoomAdapter);
        mOccupiedRoomAdapter.notifyDataSetChanged();


        mSubsription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                ArrayList<HotelLocation> hotelList = ((HotelChangedEvent) event).mHotelList;
                if (hotelList.size() == 0) {
                    finish();
                }
            }
        }));
        mRoomListPresenter.loadRoomAudit(mHotelID);

    }

    @Inject
    public void setUp() {
        mRoomListPresenter.setMvpView(this);
        mRoomListPresenter.setRoomListUseCase(mRoomListUseCase);

    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String roomNumber = intent.getStringExtra(Utils.ROOM_NAME);
        RoomV3Object room = getRoomFromList(roomNumber);
        if (roomNumber != null) {
            if (room.getId() != 0) {
                mAvailableRoomList.remove(room);
                mAvailableRoomAdapter.notifyDataSetChanged();
            }
        }
    }

    private RoomV3Object getRoomFromList(String paramRoomNumber) {
        RoomV3Object auditedRoom = new RoomV3Object();
        if (mAvailableRoomList != null) {
            for (RoomV3Object room : mAvailableRoomList) {
                if (room.getRoomNumber().equals(paramRoomNumber))
                    auditedRoom = room;
            }
        }
        return auditedRoom;
    }

    private void populateAvailableAndOccupiedRoomList(ArrayList<RoomV3Object> totalRoomList) {
        for (RoomV3Object room : totalRoomList) {
            if (!room.isAudited) {
                if (room.isAvailable())
                    mAvailableRoomList.add(room);
                else
                    mOccupiedRoomList.add(room);
            }
        }
    }


    public static Intent getCallingIntent(Context context) {
        return new Intent(context, RoomPickerActivity.class);
    }


    @Override
    public void onAuditRoomClicked(RoomV3Object room) {
        mRoom = room;
        mClickedRoomNumber = room.getRoomNumber();
        startConfirmActivity(mClickedRoomNumber);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_AUDIT_ROOM:
                if (resultCode == RESULT_OK) {
                    mNavigator.navigateToAuditActivity(this, mHotelID, mHotelName,
                            isInHotelLocation, mRoom.getRoomNumber(), mRoom.getId(), mTaskModel, true);
                    overridePendingTransition(R.anim.rotate_in, R.anim.rotate_out);
                }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubsription);
    }

    @Override
    public void onRoomClicked(RoomV3Object room) {
        mRoom = room;
        mClickedRoomNumber = room.getRoomNumber();
        startConfirmActivity(mClickedRoomNumber);

    }

    private void startConfirmActivity(String roomNumber) {
        Intent intent = new Intent(this, ConfirmActionActivity.class);
        intent.putExtra(AUDIT_SUBMISSION, false);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        intent.putExtra(Constants.AUDIT_AREA, roomNumber);
        mClickedRoomNumber = roomNumber;
        startActivityForResult(intent, REQUEST_AUDIT_ROOM);
    }

    @Override
    public void onLoadRoomList(RoomListResponse response) {
        populateAvailableAndOccupiedRoomList(response.getRoomDataObject().getRoomList());
        mAvailableRoomAdapter.notifyDataSetChanged();
        mOccupiedRoomAdapter.notifyDataSetChanged();
    }


    @Override
    public void onLoadRoomListFailure(String message) {
        SnackbarUtils.show(mRootView, message);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mLoaderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoaderView.setVisibility(View.GONE);
    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        return false;
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }


    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }
}