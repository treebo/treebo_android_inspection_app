package com.treebo.prowlapp.flowauditnew.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.AuditNewComponent;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.flowauditnew.DaggerAuditNewComponent;
import com.treebo.prowlapp.flowauditnew.adapter.FraudAuditPagerAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.FraudAuditSubCheckpointAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.FraudRoomListAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.FraudSelectAreasAdapter;
import com.treebo.prowlapp.flowauditnew.adapter.StringListAdapter;
import com.treebo.prowlapp.flowauditnew.presenter.FraudAuditPresenter;
import com.treebo.prowlapp.flowauditnew.usecase.FraudAuditUseCase;
import com.treebo.prowlapp.flowdashboard.activity.DashboardActivity;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Models.auditmodel.FraudAudit;
import com.treebo.prowlapp.Models.auditmodel.FraudAuditSaveObject;
import com.treebo.prowlapp.Models.auditmodel.FraudAuditSubmitObject;
import com.treebo.prowlapp.Models.auditmodel.FraudRoomObject;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.response.audit.FraudAuditResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import rx.subscriptions.CompositeSubscription;

import static android.view.View.VISIBLE;

/**
 * Created by abhisheknair on 16/01/17.
 */

public class FraudAuditActivity extends AppCompatActivity
        implements AuditContract.IFraudAuditView,
        FraudAuditPagerAdapter.onDoneButtonClickListener, FraudAuditSubCheckpointAdapter.onFraudSubCheckpointClickListener,
        FraudRoomListAdapter.onRoomClickListener, FraudAuditPagerAdapter.extraInfoClickListener,
        StringListAdapter.reasonClickListener, FraudSelectAreasAdapter.onFraudSelectAreasClickListener,
        FraudAuditPagerAdapter.onRefreshListener {

    private static final int REQUEST_PRICE_MISMATCH = 100;
    private static final int REQUEST_CONFIRM_ACTIVITY = 104;

    private int mHotelId;
    private String mHotelName;
    private boolean isInHotelLocation;

    private ImageView mAuditIcon;
    private ProgressBar mAuditProgressBar;
    private TreeboTextView mAuditProgressStatus;
    private ViewPager mViewpager;

    private View mCardLevel2View;
    private View mSelectAreaView;

    private TreeboTextView mCardLevel2Title;
    private RecyclerView mCardLevel2RecyclerView;

    private RecyclerView mSelectAreasRecyclerView;

    private View mExtraInformationView;
    private TreeboTextView mExtraInformationViewText;
    private TreeboTextView mExtraInformationViewTitle;


    @Inject
    public FraudAuditPresenter mAuditPresenter;

    @Inject
    FraudAuditUseCase mFraudAuditUseCase;

    @Inject
    public RxBus mRxBus;

    @Inject
    AuditPrefManagerV3 mAuditPrefManagerV3;

    @Inject
    LoginSharedPrefManager mLoginManager;

    private FraudAuditPagerAdapter mCardPagerAdapter;

    private ArrayList<FraudAudit.FraudCheckpoint> mCategoryList = new ArrayList<>();
    private FraudSelectAreasAdapter mSelectAreasAdapter;

    private int mRoomId;
    private View mLoadingView;
    private View mErrorView;
    CompositeSubscription mSubsription = new CompositeSubscription();
    private TreeboTextView mCardLevel2SubTitle;
    private int mThreshold;
    private int mAuditCategoryID;
    private TaskModel mTaskModel;
    private ArrayList<FraudRoomObject> mHouseCountRoomList = new ArrayList<>();
    private ArrayList<FraudRoomObject> mPriceMismatchRoomList = new ArrayList<>();
    private FraudAuditSaveObject mFraudAuditSavedObject;
    private boolean isRefreshListRequested;
    private SwipeRefreshLayout mCurrentSwipeRefreshLayout;
    private boolean isRefreshForHouseCount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fraud_audit);
        setToolBarColor(R.color.card_activity_background);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_progress);
        setSupportActionBar(toolbar);
        mLoadingView = findViewById(R.id.loader_layout);

        Intent intent = getIntent();
        mRoomId = intent.getIntExtra(Constants.ROOM_ID, -1);
        mHotelId = intent.getIntExtra(Utils.HOTEL_ID, -1);
        mHotelName = intent.getStringExtra(Utils.HOTEL_NAME);
        isInHotelLocation = intent.getBooleanExtra(Utils.IS_IN_HOTEL, false);
        mTaskModel = intent.getParcelableExtra(Constants.TASK_MODEL);

        AuditNewComponent auditComponent = DaggerAuditNewComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        auditComponent.injectFraudAuditActivity(this);

        mLoadingView.setBackground(ContextCompat.getDrawable(this, R.drawable.confirm_action_gradient));

        mAuditIcon = (ImageView) findViewById(R.id.toolbar_room_number_tv);
        mAuditProgressBar = (ProgressBar) findViewById(R.id.toolbar_audit_progress_bar);
        mAuditProgressStatus = (TreeboTextView) findViewById(R.id.toolbar_audit_progress_tv);
        mAuditCategoryID = AuditCategory.getAuditCategoryIDFromConstant(Constants.FRAUD_AUDIT_STRING);

        View statusAreaText = findViewById(R.id.select_areas_tv);
        statusAreaText.setOnClickListener(view -> {
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SELECT_AREA_CLICK,
                    new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                            .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mTaskModel.getHeading())
                            .putValue(SegmentAnalyticsManager.COMPLETION_RATIO, String.valueOf(getAuditedFraudCheckpointCount()) + "/" + mCategoryList.size()));
            mAuditPresenter.onSelectAreaClick();
        });
        View statusAreaDrawable = findViewById(R.id.ic_select_area);
        statusAreaDrawable.setOnClickListener(view -> mAuditPresenter.onSelectAreaClick());
        mViewpager = (ViewPager) findViewById(R.id.audit_card_viewpager);
        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                checkIfRoomCountOrPriceMismatch(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        mCardLevel2View = findViewById(R.id.audit_card_level2_layout);
        mSelectAreaView = findViewById(R.id.select_areas_layout);

        mFraudAuditSavedObject =
                mAuditPrefManagerV3.getSavedFraudAudit(AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryID, null));

        if (mCardLevel2View != null) {
            mCardLevel2Title = (TreeboTextView) mCardLevel2View.findViewById(R.id.audit_card_level2_title);
            mCardLevel2SubTitle = (TreeboTextView) mCardLevel2View.findViewById(R.id.level2_tv);
            View cardLevel2crossIv = mCardLevel2View.findViewById(R.id.ic_audit_card_level2_cancel);
            mCardLevel2RecyclerView = (RecyclerView) mCardLevel2View.findViewById(R.id.audit_card_level2_checkbox_list);
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
            layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
            mCardLevel2RecyclerView.setLayoutManager(layoutManager1);
            mCardLevel2RecyclerView.setItemAnimator(new DefaultItemAnimator());
            cardLevel2crossIv.setOnClickListener(view -> mCardLevel2View.setVisibility(View.GONE));
            mCardLevel2View.setOnClickListener(view -> mCardLevel2View.setVisibility(View.GONE));
        }

        if (mSelectAreaView != null) {
            View closeSelectAreasCross = mSelectAreaView.findViewById(R.id.ic_cross_select_areas);
            closeSelectAreasCross.setOnClickListener(view -> {
                mAuditPresenter.onBackPressed();
            });
            mSelectAreasRecyclerView = (RecyclerView) mSelectAreaView.findViewById(R.id.select_areas_rv);
            mSelectAreasRecyclerView.setItemAnimator(new SlideInLeftAnimator());
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
            layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
            mSelectAreasRecyclerView.setLayoutManager(layoutManager1);
            mSelectAreasRecyclerView.setItemAnimator(new DefaultItemAnimator());
        }

        setUpTooltipLayout();

        View backBtn = findViewById(R.id.toolbar_audit_back_btn);
        if (backBtn != null)
            backBtn.setOnClickListener((View v) -> mAuditPresenter.onBackPressed());


        mSubsription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                ArrayList<HotelLocation> hotelList = ((HotelChangedEvent) event).mHotelList;
                if (hotelList.size() == 0) {
                    finish();
                }
            }
        }));

        if (mFraudAuditSavedObject != null && mFraudAuditSavedObject.getCategoryList() != null && mFraudAuditSavedObject.getCategoryList().size() > 0) {
            mCategoryList = mFraudAuditSavedObject.getCategoryList();
            mHouseCountRoomList = mFraudAuditSavedObject.getHouseCountList();
            mPriceMismatchRoomList = mFraudAuditSavedObject.getPriceMismatchList();
            mThreshold = mFraudAuditSavedObject.getThreshold();
            mAuditCategoryID = mFraudAuditSavedObject.getAuditCategoryID();
            mAuditPresenter.setUpPageItems();
            mAuditProgressBar.setMax(mCategoryList.size());
            mAuditProgressBar.setProgress(getAuditedFraudCheckpointCount());
            mCardPagerAdapter.setSpecialAuditValues(mFraudAuditSavedObject.getThreshold(),
                    mHouseCountRoomList, mPriceMismatchRoomList);
            mCardPagerAdapter.dataChanged(mCategoryList);
            mSelectAreasAdapter.dataChanged(mCategoryList);
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_RESUMED,
                    new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                            .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mTaskModel.getHeading())
                            .putValue(SegmentAnalyticsManager.COMPLETION_RATIO, String.valueOf(getAuditedFraudCheckpointCount()) + "/" + mCategoryList.size()));

        } else {
            mAuditPresenter.setUpPageItems();
            mAuditPresenter.loadFraudAudit(mHotelId);
        }
    }

    @Inject
    public void setUp() {
        mAuditPresenter.setMvpView(this);
        mAuditPresenter.setFraudAuditUseCase(mFraudAuditUseCase);
    }

    @Override
    public void viewLoadPager() {
        mViewpager.setClipToPadding(false);
        mViewpager.setPageMargin(50);
        mCardPagerAdapter = new FraudAuditPagerAdapter(mCategoryList, this, this, this, this, this, mSubsription);
        mViewpager.setAdapter(mCardPagerAdapter);

        mSelectAreasAdapter = new FraudSelectAreasAdapter(mCategoryList, this, this);
        mSelectAreasRecyclerView.setAdapter(mSelectAreasAdapter);
    }

    @Override
    public void setToolbarInfo() {
        mAuditIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fraud_room));
        mAuditProgressBar.setProgress(0);
        mAuditProgressStatus.setText("0/" + mCategoryList.size());
    }

    @Override
    public void toggleSelectAreasVisibility() {
        if (mSelectAreaView != null) {
            if (mSelectAreaView.getVisibility() == VISIBLE) {
                mSelectAreaView.setVisibility(View.GONE);
            } else {
                mSelectAreaView.setVisibility(VISIBLE);
            }
        }
    }

    private void setUpTooltipLayout() {
        mExtraInformationView = findViewById(R.id.extra_information_layout);
        mExtraInformationViewTitle = (TreeboTextView) mExtraInformationView.findViewById(R.id.extra_info_title);
        mExtraInformationViewText = (TreeboTextView) mExtraInformationView.findViewById(R.id.extra_info_text);
        View closeView = mExtraInformationView.findViewById(R.id.ic_extra_info_close);
        closeView.setOnClickListener(view -> toggleTooltipLayout(false));
        mExtraInformationViewTitle.setOnClickListener(view -> toggleTooltipLayout(false));
        mExtraInformationViewTitle.setOnClickListener(view -> toggleTooltipLayout(false));
        mExtraInformationView.setOnClickListener(view -> toggleTooltipLayout(false));
    }


    private void toggleTooltipLayout(boolean show) {
        mExtraInformationView.setVisibility(show ? View.VISIBLE : View.GONE);
        setToolBarColor(show ? R.color.extra_information_layout_bg : R.color.card_activity_background);
    }

    private void setToolBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, color));
        }
    }

    @Override
    public void onBackPressed() {
        mAuditPresenter.onBackPressed();
    }

    @Override
    public void backPressClicked() {
        int auditDoneCount = getAuditedFraudCheckpointCount();
        if (mExtraInformationView.getVisibility() == View.VISIBLE) {
            mExtraInformationView.setVisibility(View.GONE);
        } else if (mCardLevel2View.getVisibility() == View.VISIBLE) {
            mCardLevel2View.setVisibility(View.GONE);
        } else if (mSelectAreaView.getVisibility() == VISIBLE && mCategoryList.size() > 0) {
            mSelectAreaView.setVisibility(View.GONE);
            if (mCategoryList.size() - auditDoneCount == 0)
                startConfirmActivity();
        } else {
            if (auditDoneCount > 0 || mAuditPrefManagerV3.getAllFraudIssuesRaisedInARoom(mHotelId, mAuditCategoryID).size() > 0)
                showPauseAuditDialog();
            else
                finish();
        }
    }

    @Override
    public void displayApiError(String message) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void loadDataInCards(FraudAuditResponse response) {
        if (!isRefreshListRequested) {
            mCategoryList.addAll(setUpFraudAuditList(response.getFraudAuditData().getFraudCheckpointArrayList(),
                    response.getFraudAuditData().getHouseCountRoomList().size() == 0, response.getFraudAuditData().getPriceMismatchRoomList().size() == 0));
            setToolbarInfo();
            mAuditCategoryID = response.getFraudAuditData().getAuditCategoryID();
            mAuditProgressBar.setMax(mCategoryList.size());
            mThreshold = response.getFraudAuditData().getThreshold();
            formRoomCountAndPriceMismatchList(response);
            mCardPagerAdapter.setSpecialAuditValues(response.getFraudAuditData().getThreshold(),
                    mHouseCountRoomList, mPriceMismatchRoomList);
            mCardPagerAdapter.dataChanged(mCategoryList);
            mSelectAreasAdapter.dataChanged(mCategoryList);
        } else {
            formRoomCountAndPriceMismatchList(response);
            mCardPagerAdapter.refreshRoomList(mCurrentSwipeRefreshLayout,
                    isRefreshForHouseCount ? mHouseCountRoomList : mPriceMismatchRoomList);
        }
    }

    private void formRoomCountAndPriceMismatchList(FraudAuditResponse response) {
        for (String room : response.getFraudAuditData().getHouseCountRoomList()) {
            if (!isRoomInList(mHouseCountRoomList, room)) {
                FraudRoomObject fraudRoomObject = new FraudRoomObject();
                fraudRoomObject.setRoomName(room);
                fraudRoomObject.setFraudReason("");
                mHouseCountRoomList.add(fraudRoomObject);
            }
        }

        for (String room : response.getFraudAuditData().getPriceMismatchRoomList()) {
            if (!isRoomInList(mPriceMismatchRoomList, room)) {
                FraudRoomObject fraudRoomObject = new FraudRoomObject();
                fraudRoomObject.setRoomName(room);
                fraudRoomObject.setFraudReason("");
                mPriceMismatchRoomList.add(fraudRoomObject);
            }
        }
    }

    private boolean isRoomInList(ArrayList<FraudRoomObject> list, String room) {
        for (FraudRoomObject object: list) {
            if (object.getRoomName().equals(room))
                return true;
        }
        return false;
    }


    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        if (!isRefreshListRequested)
            mLoadingView.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Override
    public void updateProgressBar(FraudAudit.FraudCheckpoint fraudCheckpoint, int position) {
        int index = getFraudCheckpointIndexFromList(fraudCheckpoint.getId());
        fraudCheckpoint.setAuditDone(true);
        mCategoryList.set(index, fraudCheckpoint);
        int cardsDone = getAuditedFraudCheckpointCount();
        mAuditProgressBar.setProgress(cardsDone);
        mAuditProgressStatus.setText(String.valueOf(cardsDone) +
                "/" + mCategoryList.size());
        mSelectAreasAdapter.updateItemInList(fraudCheckpoint);

        checkIfRoomCountOrPriceMismatch(position);
        int auditDoneCount = getAuditedFraudCheckpointCount();
        if (mCategoryList.size() - auditDoneCount == 0)
            startConfirmActivity();
    }

    @Override
    public void updateCategoryFine(FraudAudit.FraudCheckpoint category, boolean isAuditDone) {

    }

    @Override
    public void subCheckpointClicked(FraudAudit.FraudSubCheckpoint subCheckpoint) {
        subCheckpoint.setIssue(!subCheckpoint.isIssue());
        View cardView = ((FraudAuditPagerAdapter) mViewpager.getAdapter()).getCurrentView();
        if (cardView != null) {
            TreeboTextView title = (TreeboTextView) cardView.findViewById(R.id.audit_card_title);
            ((FraudAuditPagerAdapter) mViewpager.getAdapter()).updateSubcheckPointInList(subCheckpoint, title.getText().toString());
            updateCategoryList(title.getText().toString(), subCheckpoint.isIssue());
            RecyclerView checkpointRv = (RecyclerView) cardView.findViewById(R.id.audit_card_checkbox_list);
            ((FraudAuditSubCheckpointAdapter) checkpointRv.getAdapter()).refreshItem(subCheckpoint);
            TreeboButton treeboBtn = (TreeboButton) cardView.findViewById(R.id.audit_done_btn);
            treeboBtn.setText(getString(R.string.everything_else_is_fine));
        }
        FraudAuditSubmitObject fraudObject = new FraudAuditSubmitObject(Constants.FRAUD_CHECKPOINT_AUDIT, subCheckpoint.getId(), "", "");
        String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryID, null);
        mAuditPrefManagerV3.saveFraudAuditCheckpoint(key, fraudObject);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_PRICE_MISMATCH:
                if (resultCode == RESULT_OK) {
                    int priceInHotel = data.getIntExtra(Utils.PRICE_IN_HOTEL, 0);
                    int priceInTreebo = data.getIntExtra(Utils.PRICE_IN_TREEBO, 0);
                    String reason = data.getStringExtra(Utils.FRAUD_REASON_TEXT);
                    String roomNumber = data.getStringExtra(Utils.ROOM_NAME);
                    FraudAuditSubmitObject issueRaisedObject =
                            new FraudAuditSubmitObject(Constants.PRICE_MISMATCH_AUDIT, roomNumber, getString(R.string.true_string), priceInTreebo, priceInHotel);
                    String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryID, null);
                    mAuditPrefManagerV3.saveFraudAuditCheckpoint(key, issueRaisedObject);
                    updateFraudRoomCard(reason, roomNumber);
                    mCardLevel2View.setVisibility(View.GONE);
                }
                break;

            case REQUEST_CONFIRM_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    finish();
                } else {
                    mSelectAreaView.setVisibility(VISIBLE);
                }
                break;

            default:
                break;
        }
    }

    private void startPriceMismatchInfoActivity(String title, String reason, String roomNumber) {
        Intent intent = new Intent(this, PriceMismatchInfoActivity.class);
        intent.putExtra(Utils.AUDIT_BAD_TITLE, title);
        intent.putExtra(Utils.ROOM_NAME, roomNumber);
        intent.putExtra(Utils.FRAUD_REASON_TEXT, reason);
        startActivityForResult(intent, REQUEST_PRICE_MISMATCH);
    }

    private void startConfirmActivity() {
        Intent intent = new Intent(this, ConfirmActionActivity.class);
        intent.putExtra(Constants.AUDIT_SUBMISSION, true);
        intent.putParcelableArrayListExtra(Constants.FRAUD_ISSUE_LIST,
                mAuditPrefManagerV3.getAllFraudIssuesRaisedInARoom(mHotelId, mAuditCategoryID));
        intent.putExtra(Constants.TASK_MODEL, mTaskModel);
        intent.putExtra(Utils.HOTEL_ID, mHotelId);
        intent.putExtra(Utils.HOTEL_NAME, mHotelName);
        intent.putExtra(Constants.AUDIT_TYPE, mAuditCategoryID);
        startActivityForResult(intent, REQUEST_CONFIRM_ACTIVITY);
    }

    private void showPauseAuditDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_pause_audit, null);
        TreeboTextView dialogText = (TreeboTextView) dialogView.findViewById(R.id.pause_dialog_text);
        dialogText.setText(getString(R.string.fraud_pause_dialog_text));
        dialogBuilder.setView(dialogView);
        final AlertDialog pauseAuditDialog = dialogBuilder.create();
        pauseAuditDialog.show();

        TreeboTextView notNowBtn = (TreeboTextView) dialogView.findViewById(R.id.not_now_btn);
        notNowBtn.setOnClickListener(view -> pauseAuditDialog.dismiss());

        TreeboTextView okayBtn = (TreeboTextView) dialogView.findViewById(R.id.okay_btn);
        okayBtn.setOnClickListener(view -> {
            pauseAuditDialog.dismiss();
            if (mFraudAuditSavedObject == null) {
                mFraudAuditSavedObject = new FraudAuditSaveObject();
            }
            mFraudAuditSavedObject.setCategoryList(mCategoryList);
            mFraudAuditSavedObject.setPriceMismatchList(mPriceMismatchRoomList);
            mFraudAuditSavedObject.setHouseCountList(mHouseCountRoomList);
            mFraudAuditSavedObject.setThreshold(mThreshold);
            mFraudAuditSavedObject.setAuditCategoryID(mAuditCategoryID);
            mAuditPrefManagerV3.saveFraudAuditObject(AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryID, null), mFraudAuditSavedObject);
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.AUDIT_PAUSED,
                    new Properties().putValue(SegmentAnalyticsManager.HOTEL_NAME, mHotelName)
                            .putValue(SegmentAnalyticsManager.ROOM_NUMBER, mTaskModel.getHeading())
                            .putValue(SegmentAnalyticsManager.COMPLETION_RATIO, String.valueOf(getAuditedFraudCheckpointCount()) + "/" + mCategoryList.size()));

            Intent intent = new Intent(this, DashboardActivity.class);
            intent.putExtra(Utils.IS_IN_HOTEL, isInHotelLocation);
            intent.putExtra(Utils.HOTEL_ID, mHotelId);
            intent.putExtra(Utils.HOTEL_NAME, mHotelName);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        });
    }

    private ArrayList<FraudAudit.FraudCheckpoint> setUpFraudAuditList(ArrayList<FraudAudit.FraudCheckpoint> auditList, boolean isHouseCountListEmpty, boolean isPriceMismatchListEmpty) {
        if (!isHouseCountListEmpty) {
            FraudAudit.FraudCheckpoint houseCount = new FraudAudit.FraudCheckpoint();
            houseCount.setDescription(Constants.ROOM_COUNT_AUDIT);
            houseCount.setId(Constants.ROOM_COUNT_ID);
            auditList.add(houseCount);
        }

        if (!isPriceMismatchListEmpty) {
            FraudAudit.FraudCheckpoint priceMismatch = new FraudAudit.FraudCheckpoint();
            priceMismatch.setDescription(Constants.PRICE_MISMATCH_AUDIT);
            priceMismatch.setId(Constants.PRICE_MISMATCH_ID);
            auditList.add(priceMismatch);
        }

        return auditList;
    }

    private int getFraudCheckpointIndexFromList(int listID) {
        for (int i = 0; i < mCategoryList.size(); i++) {
            FraudAudit.FraudCheckpoint checkpoint = mCategoryList.get(i);
            if (checkpoint.getId() == listID)
                return i;
        }
        return -1;
    }

    private int getFraudCheckpointIndexFromListByValue(String description) {
        for (int i = 0; i < mCategoryList.size(); i++) {
            FraudAudit.FraudCheckpoint checkpoint = mCategoryList.get(i);
            if (checkpoint.getDescription().equals(description))
                return i;
        }
        return -1;
    }

    private int getAuditedFraudCheckpointCount() {
        int count = 0;
        for (int i = 0; i < mCategoryList.size(); i++) {
            FraudAudit.FraudCheckpoint checkpoint = mCategoryList.get(i);
            if (checkpoint.isAuditDone())
                count++;
        }
        return count;
    }

    @Override
    public void onHouseCountAuditRoomClicked(FraudRoomObject roomObject) {
        setUpLevel2Data(true, roomObject.getRoomName(), getSelectedFraudReasonPosition(roomObject.getFraudReason()));
    }

    @Override
    public void onPriceMismatchAuditRoomClicked(FraudRoomObject roomObject) {
        setUpLevel2Data(false, roomObject.getRoomName(), getSelectedFraudReasonPosition(roomObject.getFraudReason()));
    }

    private int getSelectedFraudReasonPosition(String fraudReason) {
        if (TextUtils.isEmpty(fraudReason))
            return -1;
        else
            return (fraudReason.equals(getResources().getStringArray(R.array.house_count_reason_list)[0])
                    || fraudReason.equals(getResources().getStringArray(R.array.price_mismatch_list)[0])) ? 0 : 1;
    }

    @Override
    public void onExtraInfoImageViewClicked(boolean isHouseCountAudit) {
        mExtraInformationViewTitle.setText(isHouseCountAudit ? Constants.ROOM_COUNT_AUDIT : Constants.PRICE_MISMATCH_AUDIT);
        String text = isHouseCountAudit ? mLoginManager.getHouseCountToolTipText() : mLoginManager.getPriceMismatchToolTipText();
        if (TextUtils.isEmpty(text))
            text = getString(R.string.fraud_audit_tooltip_default, String.valueOf(mThreshold));
        mExtraInformationViewText.setText(text);
        toggleTooltipLayout(true);
    }

    private void setUpLevel2Data(boolean isHouseCount, String room, int selectedPosition) {
        mCardLevel2SubTitle.setText(getString(R.string.level2_subtitle));
        mCardLevel2Title.setText(room);
        ArrayList<String> reasonsList;
        if (isHouseCount)
            reasonsList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.house_count_reason_list)));
        else
            reasonsList = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.price_mismatch_list)));
        StringListAdapter adapter = new StringListAdapter(isHouseCount, reasonsList, this, this, room);
        mCardLevel2RecyclerView.setAdapter(adapter);
        if (selectedPosition != -1) {
            adapter.setSelectedPosition(selectedPosition);
            adapter.notifyDataSetChanged();
        }
        mCardLevel2View.setVisibility(VISIBLE);
    }

    @Override
    public void houseCountReasonClicked(String reason, String roomNumber) {
        mCardLevel2RecyclerView.getAdapter().notifyDataSetChanged();
        String isTrue = (reason.equals(getResources().getStringArray(R.array.house_count_reason_list)[0]))
                ? getString(R.string.true_string) : getString(R.string.false_string);
        FraudAuditSubmitObject issueRaisedObject = new FraudAuditSubmitObject(Constants.HOUSE_COUNT_AUDIT_TEXT, roomNumber, "", "", isTrue);
        String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryID, null);
        mAuditPrefManagerV3.saveFraudAuditCheckpoint(key, issueRaisedObject);
        updateFraudRoomCard(reason, roomNumber);
        new Handler().postDelayed(() -> {
            if (mCardLevel2View.getVisibility() == VISIBLE)
                mCardLevel2View.setVisibility(View.GONE);
        }, 1000);
    }

    @Override
    public void priceMismatchReasonClicked(String reason, String roomNumber) {
        mCardLevel2RecyclerView.getAdapter().notifyDataSetChanged();
        String isTrue = (reason.equals(getResources().getStringArray(R.array.price_mismatch_list)[0]))
                ? getString(R.string.true_string) : getString(R.string.false_string);
        if (isTrue.equals(getString(R.string.false_string))) {
            FraudAuditSubmitObject issueRaisedObject = new FraudAuditSubmitObject(Constants.PRICE_MISMATCH_AUDIT, roomNumber, "", "", isTrue);
            String key = AuditUtilsV3.createAuditKey(mHotelId, mAuditCategoryID, null);
            mAuditPrefManagerV3.saveFraudAuditCheckpoint(key, issueRaisedObject);
            updateFraudRoomCard(reason, roomNumber);
            new Handler().postDelayed(() -> {
                if (mCardLevel2View.getVisibility() == VISIBLE)
                    mCardLevel2View.setVisibility(View.GONE);
            }, 1000);
        } else {
            startPriceMismatchInfoActivity(Constants.PRICE_MISMATCH_AUDIT, reason, roomNumber);
        }
    }

    private void updateFraudRoomCard(String reason, String roomNumber) {
        View cardView = ((FraudAuditPagerAdapter) mViewpager.getAdapter()).getCurrentView();
        if (cardView != null) {
            TreeboTextView title = (TreeboTextView) cardView.findViewById(R.id.roomcount_title);
            updateCategoryList(title.getText().toString(), true);
            RecyclerView checkpointRv = (RecyclerView) cardView.findViewById(R.id.audit_card_room_list);
            FraudRoomListAdapter adapter = (FraudRoomListAdapter) checkpointRv.getAdapter();
            adapter.refreshItem(roomNumber, reason);
            updateArrayWithFraudReason(roomNumber, reason, adapter.isAdapterForHouseCountAudit());
            int count = adapter.getFraudAuditDoneRoomCount();
            TreeboButton treeboBtn = (TreeboButton) cardView.findViewById(R.id.audit_done_btn);
            if (count >= mThreshold || count >= adapter.getItemCount()) {
                treeboBtn.setEnabled(true);
                treeboBtn.setTextColor(ContextCompat.getColor(this, R.color.white));
                treeboBtn.setBackground(ContextCompat.getDrawable(this, R.drawable.btn_green_rectangle));
            } else {
                treeboBtn.setEnabled(false);
                treeboBtn.setTextColor(ContextCompat.getColor(this, R.color.white_20_opacity));
                treeboBtn.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_rect_green_button_disabled));
            }
        }
    }

    private void updateArrayWithFraudReason(String roomNumber, String reason, boolean isHouseCountAudit) {
        if (isHouseCountAudit) {
            for (int i = 0; i < mHouseCountRoomList.size(); i++) {
                FraudRoomObject object = mHouseCountRoomList.get(i);
                if (object.getRoomName().equals(roomNumber)) {
                    object.setFraudReason(reason);
                    mHouseCountRoomList.set(i, object);
                }
            }
        } else {
            for (int i = 0; i < mPriceMismatchRoomList.size(); i++) {
                FraudRoomObject object = mPriceMismatchRoomList.get(i);
                if (object.getRoomName().equals(roomNumber)) {
                    object.setFraudReason(reason);
                    mPriceMismatchRoomList.set(i, object);
                }
            }
        }
    }

    @Override
    public void selectAreasClicked(FraudAudit.FraudCheckpoint fraudCheckpoint) {
        int index = getFraudCheckpointIndexFromList(fraudCheckpoint.getId());
        if (fraudCheckpoint.isAuditDone()) {
            fraudCheckpoint.setAuditDone(false);
            mCardPagerAdapter.addCard(mCategoryList.get(index));
            mCategoryList.set(index, fraudCheckpoint);
            mSelectAreasAdapter.updateItemInList(fraudCheckpoint);
            updateCategoryFine(fraudCheckpoint, false);
            updateProgressBar();
            SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.SELECT_AREA_SELECTED,
                    new Properties().putValue(SegmentAnalyticsManager.CATEGORY_SELECTED, fraudCheckpoint.getDescription()));
        }
        mViewpager.setCurrentItem(index - getAuditedFraudCheckpointCount());
        mSelectAreaView.setVisibility(View.GONE);
    }

    private void updateCategoryList(String description, boolean isIssue) {
        int cardIndex = getFraudCheckpointIndexFromListByValue(description);
        if (cardIndex != -1) {
            FraudAudit.FraudCheckpoint checkpoint = mCategoryList.get(cardIndex);
            checkpoint.setIssueRaised(isIssue);
            mSelectAreasAdapter.updateItemInList(checkpoint);
        }
    }

    private void updateProgressBar() {
        int cardsDone = getAuditedFraudCheckpointCount();
        mAuditProgressBar.setProgress(cardsDone);
        mAuditProgressStatus.setText(String.valueOf(cardsDone) +
                "/" + mCategoryList.size());
    }

    private void checkIfRoomCountOrPriceMismatch(int position) {
        boolean isPriceMisMatchCardVisible = mPriceMismatchRoomList.size() > 0;
        boolean isHouseCountCardVisible = mHouseCountRoomList.size() > 0;
        int cardsVisible = mCategoryList.size() - getAuditedFraudCheckpointCount();
        if (position == cardsVisible - 1) {
            if (isPriceMisMatchCardVisible) {
                onExtraInfoImageViewClicked(false);
            } else {
                onExtraInfoImageViewClicked(true);
            }
        } else if (position == cardsVisible - 2) {
            if (isPriceMisMatchCardVisible && isHouseCountCardVisible)
                onExtraInfoImageViewClicked(true);
        } else {
            mExtraInformationView.setVisibility(View.GONE);
        }
    }

    @Override
    public void layoutRefreshed(boolean isHouseCount, SwipeRefreshLayout swipeRefreshLayout) {
        if (Utils.isInternetConnected(this)) {
            isRefreshListRequested = true;
            mCurrentSwipeRefreshLayout = swipeRefreshLayout;
            isRefreshForHouseCount = isHouseCount;
            mAuditPresenter.loadFraudAudit(mHotelId);
        } else {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_internet_error), Snackbar.LENGTH_LONG).show();
        }
    }
}

