package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 09/11/16.
 */

public class SelectAreasAdapter extends RecyclerView.Adapter<SelectAreasAdapter.SelectAreasViewHolder> {

    private ArrayList<Category> mCategoryList = new ArrayList<>();
    private onSelectAreasClickListener mListener;
    private Context mContext;

    public SelectAreasAdapter(ArrayList<Category> list, onSelectAreasClickListener listener, Context context) {
        arrangeList(list);
        mListener = listener;
        mContext = context;
    }

    public void updateItemInList(Category category) {
        for (int i = 0; i < mCategoryList.size(); i++) {
            Category cat = mCategoryList.get(i);
            if (cat.mCategoryId == category.mCategoryId) {
                mCategoryList.set(i, category);
            }
        }
        arrangeList(mCategoryList);
        notifyDataSetChanged();
    }

    @Override
    public SelectAreasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_areas,
                parent, false);
        return new SelectAreasViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(SelectAreasViewHolder holder, int position) {
        Category category = mCategoryList.get(position);
        holder.category = category;
        holder.selectAreasTitle.setText(category.mDescription);
        if (category.issueRaised() || category.isPendingIssue())
            holder.issuesDot.setVisibility(View.VISIBLE);
        else
            holder.issuesDot.setVisibility(View.INVISIBLE);
        if (category.isAuditDone()) {
            holder.selectAreasDoneIv.setVisibility(View.VISIBLE);
            holder.selectAreasTitle.setTextColor(ContextCompat.getColor(mContext, R.color.text_faded));
        } else {
            holder.selectAreasDoneIv.setVisibility(View.GONE);
            holder.selectAreasTitle.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        if (mCategoryList == null)
            return 0;
        else
            return mCategoryList.size();
    }

    public class SelectAreasViewHolder extends RecyclerView.ViewHolder {

        private Category category;
        private TreeboTextView selectAreasTitle;
        private View issuesDot;
        private View selectAreasDoneIv;

        public SelectAreasViewHolder(View itemView, onSelectAreasClickListener listener) {
            super(itemView);
            selectAreasDoneIv = itemView.findViewById(R.id.audit_done_iv_tick);
            issuesDot = itemView.findViewById(R.id.issue_marked_dot);
            selectAreasTitle = (TreeboTextView) itemView.findViewById(R.id.select_areas_title);
            itemView.setOnClickListener(view -> listener.selectAreasClicked(category));
        }
    }

    public interface onSelectAreasClickListener {
        void selectAreasClicked(Category category);
    }

    private void arrangeList(ArrayList<Category> list) {
        ArrayList<Category> arrangedList = new ArrayList<>();
        ArrayList<Category> auditDoneList = new ArrayList<>();
        for (Category category : list) {
            if (!category.isAuditDone())
                arrangedList.add(category);
            else
                auditDoneList.add(category);
        }
        arrangedList.addAll(auditDoneList);
        mCategoryList.clear();
        mCategoryList.addAll(arrangedList);
    }
}
