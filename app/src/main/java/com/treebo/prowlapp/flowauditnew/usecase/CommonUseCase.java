package com.treebo.prowlapp.flowauditnew.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by sumandas on 10/11/2016.
 */

public class CommonUseCase extends UseCase<CommonAreaTypeResponse> {

    RestClient mRestClient;

    private int mHotelId;

    public CommonUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setHotelId(int hotelId) {
        mHotelId = hotelId;
    }


    @Override
    protected Observable<CommonAreaTypeResponse> getObservable() {
        return mRestClient.getAuditTypeCommon(mHotelId);
    }
}
