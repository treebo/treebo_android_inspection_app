package com.treebo.prowlapp.flowauditnew.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.auditmodel.FraudAudit;
import com.treebo.prowlapp.views.TreeboTextView;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 19/01/17.
 */

public class FraudSelectAreasAdapter
        extends RecyclerView.Adapter<FraudSelectAreasAdapter.FraudSelectAreasViewHolder> {

    private ArrayList<FraudAudit.FraudCheckpoint> mCheckpointList = new ArrayList<>();
    private onFraudSelectAreasClickListener mListener;
    private Context mContext;

    public FraudSelectAreasAdapter(ArrayList<FraudAudit.FraudCheckpoint> list,
                                   onFraudSelectAreasClickListener listener, Context context) {
        arrangeList(list);
        mListener = listener;
        mContext = context;
    }

    public void updateItemInList(FraudAudit.FraudCheckpoint inputFraudCheckpoint) {
        for (int i = 0; i < mCheckpointList.size(); i++) {
            FraudAudit.FraudCheckpoint checkpoint = mCheckpointList.get(i);
            if (checkpoint.getId() == inputFraudCheckpoint.getId()) {
                mCheckpointList.set(i, inputFraudCheckpoint);
            }
        }
        arrangeList(mCheckpointList);
        notifyDataSetChanged();
    }

    public void dataChanged(ArrayList<FraudAudit.FraudCheckpoint> list) {
        arrangeList(list);
        notifyDataSetChanged();
    }


    @Override
    public FraudSelectAreasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_areas,
                parent, false);
        return new FraudSelectAreasViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(FraudSelectAreasViewHolder holder, int position) {
        FraudAudit.FraudCheckpoint fraudCheckpoint = mCheckpointList.get(position);
        holder.checkpoint = fraudCheckpoint;
        holder.selectAreasTitle.setText(fraudCheckpoint.getDescription());
        if (fraudCheckpoint.issueRaised())
            holder.issuesDot.setVisibility(View.VISIBLE);
        else
            holder.issuesDot.setVisibility(View.INVISIBLE);
        if (fraudCheckpoint.isAuditDone()) {
            holder.selectAreasDoneIv.setVisibility(View.VISIBLE);
            holder.selectAreasTitle.setTextColor(ContextCompat.getColor(mContext, R.color.text_faded));
        } else {
            holder.selectAreasDoneIv.setVisibility(View.GONE);
            holder.selectAreasTitle.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return mCheckpointList == null ? 0 : mCheckpointList.size();
    }

    public class FraudSelectAreasViewHolder extends RecyclerView.ViewHolder {

        private FraudAudit.FraudCheckpoint checkpoint;
        private TreeboTextView selectAreasTitle;
        private View issuesDot;
        private View selectAreasDoneIv;

        public FraudSelectAreasViewHolder(View itemView, onFraudSelectAreasClickListener listener) {
            super(itemView);
            selectAreasDoneIv = itemView.findViewById(R.id.audit_done_iv_tick);
            issuesDot = itemView.findViewById(R.id.issue_marked_dot);
            selectAreasTitle = (TreeboTextView) itemView.findViewById(R.id.select_areas_title);
            itemView.setOnClickListener(view -> listener.selectAreasClicked(checkpoint));
        }
    }

    public interface onFraudSelectAreasClickListener {
        void selectAreasClicked(FraudAudit.FraudCheckpoint fraudCheckpoint);
    }

    private void arrangeList(ArrayList<FraudAudit.FraudCheckpoint> list) {
        ArrayList<FraudAudit.FraudCheckpoint> arrangedList = new ArrayList<>();
        ArrayList<FraudAudit.FraudCheckpoint> auditDoneList = new ArrayList<>();
        for (FraudAudit.FraudCheckpoint fraudCheckpoint : list) {
            if (!fraudCheckpoint.isAuditDone())
                arrangedList.add(fraudCheckpoint);
            else
                auditDoneList.add(fraudCheckpoint);
        }
        arrangedList.addAll(auditDoneList);
        mCheckpointList.clear();
        mCheckpointList.addAll(arrangedList);
    }
}
