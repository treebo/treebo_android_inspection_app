package com.treebo.prowlapp.flowauditnew.observers;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.usecase.RoomSingleUseCase;
import com.treebo.prowlapp.Models.auditmodel.AuditTypeMap;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by sumandas on 10/11/2016.
 */

public class RoomSingleObserver extends BaseObserver<RoomSingleResponse> {

    private AuditContract.IAuditCardView mView;
    private RoomSingleUseCase mUseCase;

    public RoomSingleObserver(BasePresenter presenter, RoomSingleUseCase roomSingleUseCase, AuditContract.IAuditCardView view) {
        super(presenter, roomSingleUseCase);
        mUseCase = roomSingleUseCase;
        mView = view;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(RoomSingleResponse roomSingleResponse) {
        mView.hideLoading();
        if (roomSingleResponse.status.equals("success")) {
            AuditTypeMap.saveAuditType(Constants.ROOM_AUDIT_TYPE_ID,AuditTypeMap.ROOM_AUDIT);
            mView.onLoadRoomAudit(roomSingleResponse);
        } else {
            mView.onLoadRoomAuditFailure("Loading room audit failed");
        }
    }

}
