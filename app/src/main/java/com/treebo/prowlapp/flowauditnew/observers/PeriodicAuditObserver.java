package com.treebo.prowlapp.flowauditnew.observers;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.usecase.AuditCategoryUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.audit.AuditAreaResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by sumandas on 20/01/2017.
 */

public class PeriodicAuditObserver extends BaseObserver<AuditAreaResponse> {

    private AuditContract.IAuditPeriodicView mView;

    public PeriodicAuditObserver(BasePresenter presenter, AuditCategoryUseCase useCase, AuditContract.IAuditPeriodicView view) {
        super(presenter, useCase);
        mView = view;
    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onNext(AuditAreaResponse auditCategoryResponse) {
        if (auditCategoryResponse.status.equals(Constants.STATUS_SUCCESS)) {
            mView.onLoadAuditByCategory(auditCategoryResponse);
        } else {
            mView.hideLoading();
            mView.onLoadAuditFailure(auditCategoryResponse.msg);
        }
    }
}
