package com.treebo.prowlapp.flowissues.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 25/05/2016.
 */
@Deprecated
public class LeaveHistoryAdapter extends  RecyclerView.Adapter<LeaveHistoryAdapter.LeaveListHolder> {

    public Context mContext;
    public ArrayList<String> mLeavesList;

    public LeaveHistoryAdapter(Context context,ArrayList<String> leaveList) {
        mContext=context;
        mLeavesList=leaveList;
    }

    @Override
    public LeaveListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leave_history_row, parent, false);
        return new LeaveListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LeaveListHolder holder, int position) {
        String leaveDates=mLeavesList.get(position);
        holder.mLeaveHistoryDate.setText(leaveDates);
    }

    @Override
    public int getItemCount() {
        return mLeavesList.size();
    }

    public class LeaveListHolder extends RecyclerView.ViewHolder {

        public TextView mLeaveHistoryDate;

        public LeaveListHolder(View itemView) {
            super(itemView);
            mLeaveHistoryDate = (TextView) itemView.findViewById(R.id.leave_history_dates);
        }
    }
}
