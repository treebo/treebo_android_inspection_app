package com.treebo.prowlapp.flowissues.adapter;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.IssuesModel;
import com.treebo.prowlapp.Utils.StringUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.RoundedBackgroundSpan;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 30/04/2016.
 */
@Deprecated
public class IssuesAdapter extends RecyclerView.Adapter<IssuesAdapter.IssuesViewHolder> {

    private Context mContext;
    private List<IssuesModel> mIssuesList;
    private OnShowIssueDetail mIssueDetailListener;

    public IssuesAdapter(Context context, List<IssuesModel> issuesList, OnShowIssueDetail issueDetailListener) {
        mIssuesList = issuesList;
        mIssueDetailListener=issueDetailListener;
        mContext=context;
    }

    @Override
    public IssuesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.issues_row, parent, false);
        return new IssuesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final IssuesViewHolder holder, int position) {
        final IssuesModel issue =mIssuesList.get(position);

        ArrayList<String> areaCategoryList=Utils.getAreaCategoryRooms(mContext,issue);

        SpannableStringBuilder span=new SpannableStringBuilder();
        for(String areaCategory:areaCategoryList){
            areaCategory= StringUtils.capitalizeFirstLetter(areaCategory.toLowerCase());
            areaCategory=" "+areaCategory+" ";
            span.append(areaCategory);
            span.setSpan(new RoundedBackgroundSpan(mContext), span.length()-areaCategory.length(), span.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            span.append("  ");
        }
        holder.mIssuesArea.setText(span);

        holder.mIssuesCheckpoint.setText(issue.checkpoint);
        if(issue.scoring_guideline.isEmpty()){
            holder.mIssuesScoringGuidelines.setVisibility(View.GONE);
        }else{
            holder.mIssuesScoringGuidelines.setVisibility(View.VISIBLE);
            holder.mIssuesScoringGuidelines.setText(issue.scoring_guideline);
        }

        holder.mIssuesCheckpoint.setTextColor(mContext.getResources().getColor(Utils.getIssueColor(issue.severity)));
        holder.itemView.setOnClickListener((View v)->mIssueDetailListener.onShowIssueDetails(issue));
    }

    @Override
    public int getItemCount() {
        return mIssuesList.size();
    }


    public class IssuesViewHolder extends RecyclerView.ViewHolder {

        public TextView mIssuesCheckpoint;
        public TextView mIssuesScoringGuidelines;
        public TextView mIssuesArea;
        public ImageView mExpandIssueIcon;

        public IssuesViewHolder(View itemView) {
            super(itemView);
            mIssuesCheckpoint = (TextView) itemView.findViewById(R.id.issues_comment);
            mIssuesArea = (TextView) itemView.findViewById(R.id.issues_area_reported);
            mExpandIssueIcon =(ImageView) itemView.findViewById(R.id.expand_issue_icon);
            mIssuesScoringGuidelines=(TextView) itemView.findViewById(R.id.issues_area_scoring_guidelines);
        }
    }

    public interface OnShowIssueDetail{
        void onShowIssueDetails(IssuesModel issue);
    }

}
