package com.treebo.prowlapp.flowissues.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import android.view.View;
import android.widget.RelativeLayout;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.ResolveIssueEvent;
import com.treebo.prowlapp.events.UpdateIssueEvent;
import com.treebo.prowlapp.flowissues.DaggerIssuesComponent;
import com.treebo.prowlapp.flowissues.IssuesComponent;
import com.treebo.prowlapp.flowissues.adapter.IssuesAdapter;
import com.treebo.prowlapp.flowissues.presenter.HotelIssuesPresenter;
import com.treebo.prowlapp.Models.IssuesModel;
import com.treebo.prowlapp.Models.LastUpdateModel;
import com.treebo.prowlapp.Models.UpdateIssueModel;
import com.treebo.prowlapp.mvpviews.HotelIssuesView;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;

/**
 * Created by sumandas on 30/04/2016.
 */
@Deprecated
public class IssuesActivity extends AppCompatActivity implements IssuesAdapter.OnShowIssueDetail,HotelIssuesView {

    private Toolbar mToolBar;
    private RecyclerView mListIssues;

    private IssuesAdapter mIssuesAdapter;

    @Inject
    public HotelIssuesPresenter mIssuesPresenter;

    public RelativeLayout mRootView;

    private int mHotelId;

    @Inject
    ProgressDialog progressDialog;

    List<IssuesModel> mIssuesList=new ArrayList<>();

    public OnIssueChangedSubsciber onIssueChangedSubsciber;

    @Inject
    public RxBus mIssueBus;

    @Inject
    MixpanelAPI mMixpanelAPI;

    @BindView(R.id.retry_view)
    RelativeLayout retryView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issues_main);
        ButterKnife.bind(this);
        mToolBar=(Toolbar)findViewById(R.id.toolbar);
        mListIssues =(RecyclerView)findViewById(R.id.listView_list);
        mToolBar.setTitleTextColor(getResources().getColor(R.color.white));

        mRootView=(RelativeLayout)findViewById(R.id.root_issues_layout);
        mToolBar.setNavigationIcon(R.drawable.ic_back_white);
        mToolBar.setNavigationOnClickListener((View v)->finish());

        mToolBar.setTitle(R.string.title_issues);

        mHotelId=getIntent().getIntExtra(Utils.HOTEL_ID,0);

        IssuesComponent issuesComponent= DaggerIssuesComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        issuesComponent.injectIssuesActivity(this);


        //mIssueBus= RxBus.getInstance();
        onIssueChangedSubsciber=new OnIssueChangedSubsciber();
        mIssueBus.toObservable().subscribe(onIssueChangedSubsciber);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mListIssues.setLayoutManager(layoutManager);
        mListIssues.setItemAnimator(new DefaultItemAnimator());
    /*    mIssuesPresenter=new HotelIssuePresenterImpl();
        mIssuesPresenter.setMvpView(this);*/
        mIssuesPresenter.loadIssues(mHotelId);
    }

    @Inject
    public void initView() {
        mIssuesPresenter.setMvpView(this);
    }

    @Override
    public void onShowIssueDetails(IssuesModel issue) {
        Intent intent=new Intent(this,ExpandedIssuesActivity.class);
        intent.putExtra(Utils.ISSUES_OBJECT, issue);
        MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ISSUES_LIST,
                MixPanelManager.EVENT_ISSUES_SELECT_TAP);
        startActivityForResult(intent, Utils.OPEN_ISSUE);
    }

    @Override
    public void onIssuesLoaded(List<IssuesModel> issuesList) {
        mIssuesList=issuesList;
       if(issuesList.size()==0){
            View v = findViewById(R.id.listView_list);
            SnackbarUtils.show (v, Constants.NO_ISSUES);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 2000);
        }
        else {
            mIssuesAdapter = new IssuesAdapter(this, issuesList, this);
            mListIssues.setAdapter(mIssuesAdapter);
            mIssuesAdapter.notifyDataSetChanged();
        }
        MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ISSUES_LIST,
                MixPanelManager.EVENT_ISSUES_LOAD);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(mIssuesAdapter!=null){
            mIssuesAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        onIssueChangedSubsciber.unsubscribe();
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        //progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        if(!isFinishing())
            progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    @Override
    public void showRetry() {
        retryView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        retryView.setVisibility(View.GONE);
    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(mRootView, errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(this, ((dialog, which) -> {
                    dialog.dismiss();
                    mIssuesPresenter.loadIssues(mHotelId);
                }),
                (dialog1 -> {
                    dialog1.dismiss();
                }));

    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, IssuesActivity.class);
    }

    public class OnIssueChangedSubsciber extends Subscriber<Object>{

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(Object event) {
            if(event instanceof UpdateIssueEvent){
                UpdateIssueModel updatedModel=((UpdateIssueEvent)event).mUpdateIssueModel;
                for(IssuesModel issuesModel:mIssuesList){
                    if(issuesModel.id==updatedModel.mIssueId){
                        issuesModel.rootCause=updatedModel.mRootCause;
                        LastUpdateModel updateModel=new LastUpdateModel();
                        updateModel.lastUpdate=updatedModel.mComment;
                        updateModel.time= Utils.getCurrentTime();
                        if(updateModel.lastUpdate!=null && !updateModel.lastUpdate.isEmpty()){
                            issuesModel.lastUpdate.add(0,updateModel);
                        }
                        if(updatedModel.mImageUrls!=null && !updatedModel.mImageUrls.isEmpty()){
                            for (String url : updatedModel.mImageUrls){
                                issuesModel.imageUrl.add(url);
                            }
                        }
                        return;
                    }
                }
            }else if(event instanceof ResolveIssueEvent){
                //AuditPreferenceManager.getInstance().addAuditOrIssueSubmitted();
                UpdateIssueModel closedModel=((ResolveIssueEvent)event).mUpdateIssueModel;
                for(IssuesModel issuesModel:mIssuesList){
                    if(issuesModel.id==closedModel.mIssueId){
                        mIssuesList.remove(issuesModel);
                        return;
                    }
                }
            }

        }
    }

}
