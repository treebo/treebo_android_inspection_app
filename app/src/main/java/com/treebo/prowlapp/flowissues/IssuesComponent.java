package com.treebo.prowlapp.flowissues;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowissues.activity.ExpandedIssuesActivity;
import com.treebo.prowlapp.flowissues.activity.IssuesActivity;

import dagger.Component;

/**
 * Created by sumandas on 09/08/2016.
 */
@Deprecated
@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class,IssuesModule.class})
public interface IssuesComponent {
    void injectIssuesActivity(IssuesActivity issuesActivity);
    void injectExpandedIssuesActivity(ExpandedIssuesActivity expandedIssuesActivity);
}
