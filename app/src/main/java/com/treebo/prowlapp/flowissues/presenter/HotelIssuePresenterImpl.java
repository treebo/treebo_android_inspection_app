package com.treebo.prowlapp.flowissues.presenter;

import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.HotelIssuesView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.IssuesListResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.HotelIssuesUseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by sumandas on 30/04/2016.
 */
@Deprecated
public class HotelIssuePresenterImpl implements HotelIssuesPresenter {

    public HotelIssuesUseCase mIssuesUseCase;

    public HotelIssuesView mBaseView;

    @Override
    public void loadIssues(int hotelId) {
        hideRetry();
        showLoading();
        mIssuesUseCase=new HotelIssuesUseCase(hotelId);
        mIssuesUseCase.execute(new HotelIssuesObserver(this));

    }

    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView=(HotelIssuesView)baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if(mIssuesUseCase != null){
            mIssuesUseCase.unsubscribe();
        }

    }


    @Override
    public BaseView getView() {
        return mBaseView;
    }


    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideLoading() {
        mBaseView.hideLoading();
    }

    private void showRetry() {
        mBaseView.showRetry();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }




    public class HotelIssuesObserver extends BaseObserver<IssuesListResponse> {

        public HotelIssuesObserver(BasePresenter presenter) {
            super(presenter, mIssuesUseCase);
        }

        @Override
        public void onCompleted() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            super.onError(e);
        }

        @Override
        public void onNext(IssuesListResponse response) {
            hideLoading();
            if(response.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)){
                mBaseView.onIssuesLoaded(response.data.issues);
            }else {
                mBaseView.showError(response.msg);
            }
        }
    }

}
