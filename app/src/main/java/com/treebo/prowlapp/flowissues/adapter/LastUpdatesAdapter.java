package com.treebo.prowlapp.flowissues.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.LastUpdateModel;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 10/05/2016.
 */
@Deprecated
public class LastUpdatesAdapter extends RecyclerView.Adapter<LastUpdatesAdapter.LastUpdatesHolder> {

    public Context mContext;
    public ArrayList<LastUpdateModel> mUpdatedList;

    public LastUpdatesAdapter(Context context,ArrayList<LastUpdateModel> updatedList) {
        mContext=context;
        mUpdatedList=updatedList;
    }

    @Override
    public LastUpdatesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_last_update_row, parent, false);
        return new LastUpdatesHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LastUpdatesHolder holder, int position) {
        LastUpdateModel lastUpdateModel=mUpdatedList.get(position);
        holder.mLastUpdateComment.setText(lastUpdateModel.lastUpdate);
        holder.mLastUpdateTime.setText(lastUpdateModel.time);
    }

    @Override
    public int getItemCount() {
        return mUpdatedList.size();
    }

    public class LastUpdatesHolder extends RecyclerView.ViewHolder {

        public TextView mLastUpdateComment;
        public TextView mLastUpdateTime;

        public LastUpdatesHolder(View itemView) {
            super(itemView);
            mLastUpdateComment = (TextView) itemView.findViewById(R.id.last_update_comment);
            mLastUpdateTime =(TextView) itemView.findViewById(R.id.last_update_time);
        }
    }

    }


