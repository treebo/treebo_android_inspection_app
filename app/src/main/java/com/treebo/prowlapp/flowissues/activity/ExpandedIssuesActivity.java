package com.treebo.prowlapp.flowissues.activity;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.ResolveIssueEvent;
import com.treebo.prowlapp.events.UpdateIssueEvent;
import com.treebo.prowlapp.flowissuenew.adapter.ImageUrlAdapter;
import com.treebo.prowlapp.flowissues.DaggerIssuesComponent;
import com.treebo.prowlapp.flowissues.IssuesComponent;
import com.treebo.prowlapp.flowupdateauditOrissue.AddCommentActivity;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.fragments.ImageShowFragment;
import com.treebo.prowlapp.Models.IssuesModel;
import com.treebo.prowlapp.Models.LastUpdateModel;
import com.treebo.prowlapp.Models.UpdateIssueModel;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import rx.Subscriber;

/**
 * Created by sumandas on 01/05/2016.
 */
@Deprecated
public class ExpandedIssuesActivity extends AppCompatActivity implements ImageUrlAdapter.ListInteractor {

    private static final String TAG = "ExpandedIssuesActivity";
    public ImageView mFloatingCross;
    public RelativeLayout mFloatingIssue;

    public RelativeLayout mLayout1;
    public RelativeLayout mLayout2;
    public RelativeLayout mLayout3;
    public RelativeLayout mLayout4;
    public RelativeLayout mLayout5;
    public RelativeLayout mLayout6;
    public RelativeLayout mLayout7;

    public RelativeLayout mRootView;

    public Button mUpdateInfo;
    public Button mMarkResolved;

    public IssuesModel mIssue;

    public RecyclerView mPhotosList;

    public ImageUrlAdapter mImageAdapter;

    public ScrollView mScrollView;
    public TextView mHeaderText;

    public ArrayList<String> imageUrls=new ArrayList<>();

    @Inject
    public RxBus mIssueBus;

    @Inject
    public MixpanelAPI mMixpanelAPI;

    public OnIssueChangedSubsciber onIssueChangedSubsciber;

    public boolean isImagesAdded=false;
    private boolean isInForeground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIssue=getIntent().getParcelableExtra(Utils.ISSUES_OBJECT);
        setContentView(R.layout.issues_expanded);
        mRootView = (RelativeLayout) findViewById(R.id.root_view);
        mFloatingIssue=(RelativeLayout)findViewById(R.id.floating_text_issue);
        mPhotosList=(RecyclerView)findViewById(R.id.hotels_list_recycler_view);
        mScrollView=(ScrollView)findViewById(R.id.issues_expanded_scroll_view);
        mHeaderText=(TextView) findViewById(R.id.issues_expanded_heading_text);
        mUpdateInfo=(Button)findViewById(R.id.button_update_info);
        mMarkResolved=(Button)findViewById(R.id.marked_resolved);
        mFloatingCross=(ImageView)findViewById(R.id.image_view_cross);

        mLayout1=(RelativeLayout)findViewById(R.id.layout1);
        mLayout2=(RelativeLayout)findViewById(R.id.layout2);
        mLayout3=(RelativeLayout)findViewById(R.id.layout3);
        mLayout4=(RelativeLayout)findViewById(R.id.layout4);
        mLayout5=(RelativeLayout)findViewById(R.id.layout5);
        mLayout6=(RelativeLayout)findViewById(R.id.layout6);
        mLayout7=(RelativeLayout)findViewById(R.id.layout7);

        mHeaderText.setText(mIssue.checkpoint);
        mHeaderText.setTextColor(getResources().getColor(Utils.getIssueColor(mIssue.severity)));


        IssuesComponent issuesComponent= DaggerIssuesComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .build();
        issuesComponent.injectExpandedIssuesActivity(this);


        mFloatingCross.setOnClickListener((View v) -> finish());

        mUpdateInfo.setOnClickListener((View v) -> {
            Intent intent = new Intent(ExpandedIssuesActivity.this, AddCommentActivity.class);
            intent.putExtra(Utils.ISSUES_OBJECT, mIssue);
            intent.putExtra(Utils.ISSUES_STATUS, "Update");
            intent.putExtra(Utils.PHOTO_KEY,Integer.toString(mIssue.id));
            MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ISSUES_DETAILS,
                    MixPanelManager.EVENT_UPDATE_INFO_TAP);
            startActivityForResult(intent, Utils.ISSUE_UPDATED);
        });

        mMarkResolved.setOnClickListener((View v) -> {
            Intent intent = new Intent(ExpandedIssuesActivity.this, AddCommentActivity.class);
            intent.putExtra(Utils.ISSUES_OBJECT, mIssue);
            intent.putExtra(Utils.ISSUES_STATUS, "Close");
            intent.putExtra(Utils.PHOTO_KEY, Integer.toString(mIssue.id));
            MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_ISSUES_DETAILS,
                    MixPanelManager.EVENT_MARK_RESOLVED_TAP);
            startActivityForResult(intent, Utils.ISSUE_RESOLVED);
        });

        mScrollView.smoothScrollTo(0, 0);

        onIssueChangedSubsciber=new OnIssueChangedSubsciber();
        mIssueBus.toObservable().subscribe(onIssueChangedSubsciber);

        if(mIssue.imageUrl!=null && !mIssue.imageUrl.isEmpty()){
            imageUrls.clear();
            for(String url:mIssue.imageUrl){
                if(!url.isEmpty()){
                    imageUrls.add(url);
                }
            }
            if(mImageAdapter==null){
                mImageAdapter=new ImageUrlAdapter(this,imageUrls,false);
                mPhotosList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
                mPhotosList.setItemAnimator(new DefaultItemAnimator());
                mImageAdapter.setListInteractor(this);
                mPhotosList.setAdapter(mImageAdapter);
            }
            mImageAdapter.notifyDataSetChanged();
        }
    }

    private void setLayout(RelativeLayout layout,String title,String value){
        TextView issueTitle =(TextView)layout.findViewById(R.id.issues_expanded_row_heading);
        TextView issueValue =(TextView)layout.findViewById(R.id.issues_source_value);

        issueTitle.setText(title);
        issueValue.setText(value);
    }

    private void setLayoutLatestUpdate(RelativeLayout layout,String title,String value){
        TextView issueTitle =(TextView)layout.findViewById(R.id.issues_expanded_row_heading);
        TextView issueValue =(TextView)layout.findViewById(R.id.issues_source_value);
        TextView lastUpdateButton=(TextView)layout.findViewById(R.id.expanded_item_click);
        lastUpdateButton.setVisibility(View.VISIBLE);
        lastUpdateButton.setOnClickListener((View v) -> {
            Intent intent = new Intent(ExpandedIssuesActivity.this, LastUpdatesActivity.class);
            intent.putExtra(Utils.LAST_UPDATE_LIST,mIssue.lastUpdate);
            startActivity(intent);

        });
        issueTitle.setText(title);
        issueValue.setText(value);
    }

    @Override
    public void onPause() {
        isInForeground=false;
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
        isInForeground=true;
        setLayout(mLayout1, getString(R.string.issues_source), mIssue.source);
        setLayout(mLayout2, getString(R.string.area), Utils.getAreaCategoryString(this, mIssue));
        setLayout(mLayout3,getString(R.string.repeat),mIssue.isRepeat?getString(R.string.Yes):getString(R.string.No));
        setLayout(mLayout4,getString(R.string.open_since),mIssue.opensince);
        setLayout(mLayout5,getString(R.string.root_cause),mIssue.rootCause);
        if(mIssue.lastUpdate!=null && !mIssue.lastUpdate.isEmpty()){
            setLayoutLatestUpdate(mLayout6, getString(R.string.last_update),mIssue.lastUpdate.get(0).lastUpdate);
        }else{
            setLayout(mLayout6,getString(R.string.last_update),"");
        }
        setLayout(mLayout7, getString(R.string.additional_details), mIssue.comment);
        if( isImagesAdded && mIssue.imageUrl!=null && !mIssue.imageUrl.isEmpty()){
            imageUrls.clear();
            for(String url:mIssue.imageUrl){
                if(!url.isEmpty()){
                    imageUrls.add(url);
                }
            }
            if(mImageAdapter==null){
                mImageAdapter=new ImageUrlAdapter(this,imageUrls,false);
                mPhotosList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                mPhotosList.setItemAnimator(new DefaultItemAnimator());
                mImageAdapter.setListInteractor(this);
                mPhotosList.setAdapter(mImageAdapter);
            }
            mImageAdapter.notifyDataSetChanged();
            isImagesAdded=false;
        }
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        onIssueChangedSubsciber.unsubscribe();
    }

    @Override
    public void onListEmpty() {

    }

    @Override
    public void imageItemClicked(int pos, int total, String imageUrl) {
        addFragment(ImageShowFragment.newInstance(pos, total, imageUrl));
    }


    public class OnIssueChangedSubsciber extends Subscriber<Object> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(Object event) {
            if(event instanceof UpdateIssueEvent){
                UpdateIssueModel updatedValues=((UpdateIssueEvent)event).mUpdateIssueModel;
                mIssue.rootCause=updatedValues.mRootCause;
                LastUpdateModel updateModel=new LastUpdateModel();
                updateModel.lastUpdate=updatedValues.mComment;
                updateModel.time= Utils.getCurrentTime();
                if(updateModel.lastUpdate!=null && !updateModel.lastUpdate.isEmpty()){
                    mIssue.lastUpdate.add(0,updateModel);
                }
                //mIssue.imageUrl.clear();
                if(updatedValues.mImageUrls!=null && !updatedValues.mImageUrls.isEmpty()){
                    for (String url : updatedValues.mImageUrls){
                        mIssue.imageUrl.add(url);
                    }
                }
                if(isInForeground){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(mIssue.imageUrl!=null && !mIssue.imageUrl.isEmpty()){
                                imageUrls.clear();
                                for(String url:mIssue.imageUrl){
                                    if(!url.isEmpty()){
                                        imageUrls.add(url);
                                    }
                                }
                                if(mImageAdapter==null){
                                    mImageAdapter=new ImageUrlAdapter(ExpandedIssuesActivity.this,imageUrls,false);
                                    mPhotosList.setLayoutManager(new LinearLayoutManager(ExpandedIssuesActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    mPhotosList.setItemAnimator(new DefaultItemAnimator());
                                    mImageAdapter.setListInteractor(ExpandedIssuesActivity.this);
                                    mPhotosList.setAdapter(mImageAdapter);
                                }
                                mImageAdapter.notifyDataSetChanged();
                            }
                        }
                    });
                }else{
                    isImagesAdded=true;
                }

            }else if(event instanceof ResolveIssueEvent){
                finish();
            }

        }
    }
    public void addFragment(BaseFragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.root_view, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
