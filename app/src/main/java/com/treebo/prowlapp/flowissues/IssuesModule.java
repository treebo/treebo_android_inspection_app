package com.treebo.prowlapp.flowissues;

import com.treebo.prowlapp.flowissues.presenter.HotelIssuePresenterImpl;
import com.treebo.prowlapp.flowissues.presenter.HotelIssuesPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 09/08/2016.
 */
@Deprecated
@Module
public class IssuesModule {

    @Provides
    HotelIssuesPresenter providesHotelIssuesPresenter(){
        return new HotelIssuePresenterImpl();
    }

}
