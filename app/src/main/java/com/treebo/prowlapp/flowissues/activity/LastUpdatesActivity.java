package com.treebo.prowlapp.flowissues.activity;

import android.os.Bundle;
import android.view.View;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowissues.adapter.LastUpdatesAdapter;
import com.treebo.prowlapp.Models.LastUpdateModel;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 10/05/2016.
 */
@Deprecated
public class LastUpdatesActivity extends AppCompatActivity {

    public Toolbar mToolBar;
    public RecyclerView mListLastUpdates;
    public ArrayList<LastUpdateModel> mLastUpdates;
    LastUpdatesAdapter mLastUpdatesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_update);

        mToolBar=(Toolbar)findViewById(R.id.toolbar);
        mListLastUpdates =(RecyclerView)findViewById(R.id.last_update_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mListLastUpdates.setLayoutManager(layoutManager);
        mListLastUpdates.setItemAnimator(new DefaultItemAnimator());

        mLastUpdates=getIntent().getParcelableArrayListExtra(Utils.LAST_UPDATE_LIST);

        mLastUpdatesAdapter = new LastUpdatesAdapter(this, mLastUpdates);
        mListLastUpdates.setAdapter(mLastUpdatesAdapter);
        mLastUpdatesAdapter.notifyDataSetChanged();

        mToolBar.setNavigationIcon(R.drawable.ic_back_white);
        mToolBar.setNavigationOnClickListener((View v) -> finish());
        mToolBar.setTitle(R.string.last_activity_title);

    }

}
