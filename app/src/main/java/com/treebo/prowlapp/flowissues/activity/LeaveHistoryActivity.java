package com.treebo.prowlapp.flowissues.activity;

import android.os.Bundle;

import android.view.View;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowissues.adapter.LeaveHistoryAdapter;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sumandas on 25/05/2016.
 */
@Deprecated
public class LeaveHistoryActivity extends AppCompatActivity {

    public Toolbar mToolBar;
    public RecyclerView mListLeaveHistory;
    public ArrayList<String> mLeaveHistory;
    LeaveHistoryAdapter mLeaveHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_update);

        mToolBar=(Toolbar)findViewById(R.id.toolbar);
        mListLeaveHistory =(RecyclerView)findViewById(R.id.last_update_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mListLeaveHistory.setLayoutManager(layoutManager);
        mListLeaveHistory.setItemAnimator(new DefaultItemAnimator());

        mLeaveHistory=getIntent().getStringArrayListExtra(Utils.LEAVE_HISTORY_LIST);

        mLeaveHistoryAdapter = new LeaveHistoryAdapter(this, mLeaveHistory);
        mListLeaveHistory.setAdapter(mLeaveHistoryAdapter);
        mLeaveHistoryAdapter.notifyDataSetChanged();

        mToolBar.setNavigationIcon(R.drawable.ic_back_white);
        mToolBar.setNavigationOnClickListener((View v) -> finish());
        mToolBar.setTitle(R.string.leave_history);

    }

}
