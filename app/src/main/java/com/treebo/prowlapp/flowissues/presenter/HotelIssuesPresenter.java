package com.treebo.prowlapp.flowissues.presenter;

import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by sumandas on 30/04/2016.
 */
@Deprecated
public interface HotelIssuesPresenter extends BasePresenter {
   void loadIssues(int hotelId);
}
