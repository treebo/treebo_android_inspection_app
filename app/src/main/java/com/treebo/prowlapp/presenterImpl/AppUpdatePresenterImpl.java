package com.treebo.prowlapp.presenterImpl;


import com.treebo.prowlapp.Models.AppUpdateResponse;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.UpdateAppView;
import com.treebo.prowlapp.presenter.AppUpdatePresenter;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.AppUpdateUseCase;

/**
 * Created by sumandas on 20/05/2016.
 */
@Deprecated
public class AppUpdatePresenterImpl implements AppUpdatePresenter {

    private AppUpdateUseCase mAppUpdateUseCase;
    public UpdateAppView mUpdateView;

    @Override
    public void getAppUpdate() {
        mAppUpdateUseCase=new AppUpdateUseCase();
        mAppUpdateUseCase.execute(new UpdateObserver(this));

    }

    @Override
    public void setMvpView(BaseView baseView) {
        mUpdateView=(UpdateAppView)baseView;

    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if(mAppUpdateUseCase != null){
            mAppUpdateUseCase.unsubscribe();
        }

    }

    @Override
    public BaseView getView() {
        return null;
    }


    public class UpdateObserver extends BaseObserver<AppUpdateResponse> {

        public UpdateObserver(BasePresenter presenter) {
            super(presenter, mAppUpdateUseCase);
        }

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onNext(AppUpdateResponse response) {
            if(response.data!=null && !response.data.isEmpty()){
                mUpdateView.onAppUpdate(response.data.get(0));
            }

        }
    }

}
