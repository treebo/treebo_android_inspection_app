package com.treebo.prowlapp.application;

import android.app.ProgressDialog;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 01/08/2016.
 */
@Module
public class ActivityModule {

    public Context mContext;

    public ActivityModule(Context context){
        mContext=context;
    }

    @Provides
    Context providesContext(){
        return mContext;
    }

    @Provides
    ProgressDialog providesProgressBar(){
        return new ProgressDialog(mContext);
    }
}
