package com.treebo.prowlapp.application;

import android.content.Context;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.segment.analytics.Analytics;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 08/08/2016.
 */
@Module
public class ApplicationModule {

    private Context mContext;

    public ApplicationModule(Context context) {
        mContext = context;
    }

    @Singleton
    @Provides
    RxBus providesRxBus() {
        return RxBus.getInstance();
    }

    @Singleton
    @Provides
    LoginSharedPrefManager providesLoginManager() {
        return LoginSharedPrefManager.getInstance();
    }

    @Singleton
    @Provides
    AuditPreferenceManager providesAuditManager() {
        return AuditPreferenceManager.getInstance();
    }

    @Singleton
    @Provides
    AuditPrefManagerV3 providesAuditV3Manager() {
        return AuditPrefManagerV3.getInstance();
    }

    @Singleton
    @Provides
    MixpanelAPI providesMixPanelApi() {
        if (BuildConfig.BUILD_TYPE.equals("release")) {
            return MixpanelAPI.getInstance(mContext, MainApplication.MIXPANEL_TOKEN_RELEASE);
        } else {
            return MixpanelAPI.getInstance(mContext, MainApplication.MIXPANEL_TOKEN_DEBUG);
        }
    }

    @Singleton
    @Provides
    Analytics providesSegmentsAnalytics() {
        String writeKey;
        if (BuildConfig.BUILD_TYPE.equals("release"))
            writeKey = MainApplication.SEGMENT_RELEASE_WRITE_KEY;
        else
            writeKey = MainApplication.SEGMENT_DEBUG_WRITE_KEY;
        Analytics analytics = new Analytics.Builder(mContext, writeKey)
                .trackApplicationLifecycleEvents() // Enable this to record certain application events automatically!
                .recordScreenViews() // Enable this to record screen views automatically!
                .build();

        //        Set the initialized instance as a globally accessible instance.
        Analytics.setSingletonInstance(analytics);
        return analytics;
    }

    @Singleton
    @Provides
    Navigator providesNavigator() {
        return new Navigator();
    }

}
