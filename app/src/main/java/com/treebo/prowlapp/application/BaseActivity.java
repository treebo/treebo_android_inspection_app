package com.treebo.prowlapp.application;

import android.text.TextUtils;

import com.treebo.prowlapp.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


/**
 * Created by devesh on 15/03/16.
 */
public class BaseActivity extends AppCompatActivity {
    protected Toolbar mToolbar;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

    }

    protected boolean hasActionBar() {
        return true;
    }


    public void setActionBarTitle(String title) {
        if (TextUtils.isEmpty(title)) {
            title = getString(R.string.app_name);
        }
        if (hasActionBar() && mToolbar != null) {
            mToolbar.setTitle(title);
        }
    }

}
