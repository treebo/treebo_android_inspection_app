package com.treebo.prowlapp.application;

import android.content.Context;
import android.content.Intent;

import com.crashlytics.android.Crashlytics;
import com.evernote.android.job.JobManager;
import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.segment.analytics.Analytics;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.exception.ForcedLogoutException;
import com.treebo.prowlapp.flowOnboarding.activity.OnBoardingActivity;
import com.treebo.prowlapp.flowlogin.LoginActivity;
import com.treebo.prowlapp.job.OfflineJobCreator;
import com.treebo.prowlapp.net.DaggerRestApiComponent;
import com.treebo.prowlapp.net.RestApiComponent;
import com.treebo.prowlapp.net.RestApiModule;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import javax.inject.Inject;

import androidx.multidex.MultiDexApplication;
import io.fabric.sdk.android.Fabric;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rxAndroid.schedulers.AndroidSchedulers;


public class MainApplication extends MultiDexApplication {

    private static Context context;

    @Inject
    RxBus rxBus;

    @Inject
    LoginSharedPrefManager mLoginManager;

    @Inject
    Analytics mSegmentAnalytics;

    public static String MIXPANEL_TOKEN_DEBUG = "fe37411e4894e8fad6c75c0e6ecd52c8";

    public static String MIXPANEL_TOKEN_RELEASE = "d470a983116626ffc6173741c97a796e";

    public static String SEGMENT_DEBUG_WRITE_KEY = "EkYSKjspKLiOQdLSWOQVcX7dc5xgjIxl";

    public static String SEGMENT_RELEASE_WRITE_KEY = "j4C48dQdbxHqqNKfhyOl8Z248V4edUtV";

    protected RestApiComponent mRestApiComponent;

    protected ApplicationComponent mApplicationComponent;

    private static boolean activityVisible;

    @Override
    public void onCreate() {
//        if (BuildConfig.DEBUG) {
//            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                    .detectDiskReads()
//                    .detectDiskWrites()
//                    .detectNetwork()   // or .detectAll() for all detectable problems
//                    .penaltyLog()
//                    .build());
//            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                    .detectLeakedSqlLiteObjects()
//                    .detectLeakedClosableObjects()
//                    .penaltyLog()
//                    .penaltyDeath()
//                    .build());
//        }
        super.onCreate();
        MainApplication.context = getApplicationContext();
        if (BuildConfig.BUILD_TYPE.equals("release")) {
            Fabric.with(context, new Crashlytics());
        }

        // Create an InitializerBuilder
        Stetho.InitializerBuilder initializerBuilder =
                Stetho.newInitializerBuilder(this);

        // Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this)
        );

        // Enable command line interface
        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(context)
        );

        // Use the InitializerBuilder to generate an Initializer
        Stetho.Initializer initializer = initializerBuilder.build();

        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer);

        initJobManager();
        initDbFlow();

        initializeRestClient();
        initializeAppComponent();

        handleForceLogoutEvents();
    }

    public static Context getAppContext() {
        return MainApplication.context;
    }


    protected void initializeRestClient() {
        mRestApiComponent = DaggerRestApiComponent.builder()
                .restApiModule(new RestApiModule(LoginSharedPrefManager.getInstance()))
                .build();
    }

    protected void initializeAppComponent() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mApplicationComponent.injectApplication(this);
    }

    public static MainApplication get() {
        return (MainApplication) MainApplication.getAppContext();
    }

    public static void initJobManager() {
        JobManager.create(context).addJobCreator(new OfflineJobCreator());
    }

    public static void initDbFlow() {
        FlowManager.init(new FlowConfig.Builder(context)
                .openDatabasesOnInit(true).build());
    }

    public RestApiComponent getRestComponent() {
        return mRestApiComponent;
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }


    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    /**
     * Subscribes to the {@link RxBus} to handle all {@link ForcedLogoutException} from any observer
     */
    private void handleForceLogoutEvents() {
        rxBus.toObservable()
                .flatMap((Func1<Object, Observable<?>>) o -> {
                    if (o instanceof ForcedLogoutException) {
                        return Observable.just((ForcedLogoutException) o);
                    }
                    return Observable.never();
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        mLoginManager.clearUserData();

                        if (!mLoginManager.isUserLoggedIn() && !this.getClass().getSimpleName().equalsIgnoreCase(LoginActivity.class.getSimpleName())) {
                            Intent loginIntent = new Intent(getApplicationContext(), OnBoardingActivity.class);
                            loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            loginIntent.putExtra(OnBoardingActivity.INTENT_EXTRAS_UNAUTH_ERROR, true);
                            startActivity(loginIntent);
                        }
                    }
                });
    }

}
