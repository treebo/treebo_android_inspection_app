package com.treebo.prowlapp.application;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.segment.analytics.Analytics;
import com.treebo.prowlapp.flowdashboard.auditstate.StateRoomPaused;
import com.treebo.prowlapp.geofence.GeofenceTransitionsIntentService;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.pushMessaging.services.AppUpdateService;
import com.treebo.prowlapp.pushMessaging.services.FirebaseMessagingServiceImpl;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.service.LocationService;
import com.treebo.prowlapp.service.UpdateHotelLocationService;
import com.treebo.prowlapp.sharedprefs.AuditPrefManagerV3;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

/**
 * Created by sumandas on 25/08/2016.
 */
public interface IApplicationComponent {

    RxBus getRxBus();
    LoginSharedPrefManager getLoginManager();
    AuditPreferenceManager getAuditManager();
    AuditPrefManagerV3 getAuditManagerV3();
    MixpanelAPI getMixpaneApi();
    Navigator getNavigator();
    Analytics getSegmentsAnalytics();

    void injectAppUpdateService(AppUpdateService appUpdateService);
    void injectUpdateService(UpdateHotelLocationService updateHotelLocationService);
    void injectLocationService(LocationService locationService);
    void injectApplication(MainApplication application);

    void injectGoefenceService(GeofenceTransitionsIntentService geofenceService);

    void injectRoomState(StateRoomPaused stateRoomPaused);

    void injectFirebaseService(FirebaseMessagingServiceImpl firebaseMessagingService);
}
