package com.treebo.prowlapp.application;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by sumandas on 08/08/2016.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent extends IApplicationComponent{

}
