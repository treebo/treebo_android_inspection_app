package com.treebo.prowlapp.adapter;

import android.content.Context;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;

import java.util.List;

import androidx.viewpager.widget.PagerAdapter;

/**
 * Created by sumandas on 27/04/2016.
 */
public class SlidingTabAdapter extends PagerAdapter {

    List<String> mTabList;
    Context mContext;

    public SlidingTabAdapter(Context context, List<String> auditFrequency) {
        mContext = context;
        mTabList = auditFrequency;
    }

    @Override
    public int getCount() {
        return mTabList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TextView textView=new TextView(mContext);
        textView.setText(mTabList.get(position));
        textView.setBackgroundColor(mContext.getResources().getColor(R.color.color_emerald));
        textView.setTextColor(mContext.getResources().getColor(R.color.white));
        container.addView(textView);
        return textView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabList.get(position);
    }

}
