package com.treebo.prowlapp.flowOnboarding;

import android.content.Context;

import com.treebo.prowlapp.flowOnboarding.presenter.GoogleLoginPresenter;
import com.treebo.prowlapp.flowOnboarding.presenter.OnBoardingPresenter;
import com.treebo.prowlapp.flowOnboarding.usecase.GoogleLoginUseCase;
import com.treebo.prowlapp.flowOnboarding.usecase.RootCauseUseCase;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 18/10/2016.
 */
@Module
public class OnBoardingModule {

    private Context mContext;

    public OnBoardingModule(Context context){
        mContext=context;
    }

    @Provides
    RestClient providesRestClient(){
        return new RestClient();
    }

    @Provides
    OnBoardingPresenter providesOnBoardingPresenter(){
        return new OnBoardingPresenter(mContext);
    }

    @Provides
    GoogleLoginPresenter providesGoogleLoginPresenter(){
        return new GoogleLoginPresenter(mContext);
    }

    @Provides
    GoogleLoginUseCase providesGoogleUseCase(RestClient restClient){
        return new GoogleLoginUseCase(restClient);
    }

    @Provides
    RootCauseUseCase providesRootCauseUseCase(RestClient restClient){
        return new RootCauseUseCase(restClient);
    }
}
