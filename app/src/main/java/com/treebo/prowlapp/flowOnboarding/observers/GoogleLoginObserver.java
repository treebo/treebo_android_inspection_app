package com.treebo.prowlapp.flowOnboarding.observers;

import android.util.Log;

import com.treebo.prowlapp.errors.BaseErrorResponse;
import com.treebo.prowlapp.flowOnboarding.OnBoardingContract;
import com.treebo.prowlapp.flowOnboarding.usecase.GoogleLoginUseCase;
import com.treebo.prowlapp.net.RetrofitException;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Created by sumandas on 18/10/2016.
 */

public class GoogleLoginObserver extends BaseObserver<LoginResponse> {

    public OnBoardingContract.IGoogleLoginView mView;

    public GoogleLoginObserver(BasePresenter basePresenter, GoogleLoginUseCase googleLoginUseCase,
                               OnBoardingContract.IGoogleLoginView view){
        super(basePresenter,googleLoginUseCase);
        mView=view;

    }

    @Override
    public void onCompleted() {
        mView.hideLoading();
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        mView.hideLoading();
        if (e instanceof RetrofitException) {
            RetrofitException retrofitException = (RetrofitException) e;
            if(retrofitException.getKind()==RetrofitException.NETWORK){
                mView.showSlowInternetScreen();
                return;
            }
            if(retrofitException.getKind()==RetrofitException.UNEXPECTED){
                return;
            }
            if(HttpURLConnection.HTTP_BAD_REQUEST == retrofitException.getResponse().code()) {
                BaseErrorResponse errorResponse = null;
                try {
                    errorResponse = ((RetrofitException) e).getErrorBodyAs(BaseErrorResponse.class);
                } catch (IOException exception) {
                    Log.e("Google Login","exception occured while getting error body");
                }
                if(errorResponse != null) {
                    mView.onLoginFailure(errorResponse.getMsg());
                    return;
                }
            }
        }
        if(e.getMessage()!=null){
            mView.onLoginFailure(e.getMessage());
        }
    }

    @Override
    public void onNext(LoginResponse response) {
        mView.hideLoading();
        Log.i(GoogleLoginObserver.class.getName(),response.status);
        if (response.status.equals("success")) {
            mView.onLoginSuccess(response);
        } else {
            mView.onLoginFailure(response.getMessage());
        }
    }
}
