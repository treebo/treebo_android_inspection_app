package com.treebo.prowlapp.flowOnboarding.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;

/**
 * Created by sumandas on 04/05/2016.
 */
public class RootCauseUseCase extends UseCase {

    RestClient mRestClient;

    public RootCauseUseCase(RestClient restClient){
        mRestClient=restClient;
    }
    @Override
    protected Observable getObservable() {
        return mRestClient.getRootCause();
    }
}
