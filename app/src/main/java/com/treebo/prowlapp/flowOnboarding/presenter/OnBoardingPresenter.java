package com.treebo.prowlapp.flowOnboarding.presenter;


import android.content.Context;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowOnboarding.OnBoardingContract;
import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by abhisheknair on 17/10/16.
 */

public class OnBoardingPresenter implements OnBoardingContract.IOnBoardingActivityPresenter {

    private OnBoardingContract.IOnBoardingActivityView mIOnBoardingActivityView;
    private Context mContext;

    public OnBoardingPresenter(Context context){
        mContext=context;
    }

    @Override
    public void onCreate() {
        if(mIOnBoardingActivityView != null) {
            mIOnBoardingActivityView.loadViewPager();
        }
    }

    @Override
    public void onViewPagerLoaded() {
        if(mIOnBoardingActivityView != null) {
            mIOnBoardingActivityView.loadViewPagerDots();
        }
    }

    @Override
    public void onResume(int currentPosition, int adapterCount) {
        onPageSelected(currentPosition, adapterCount);
    }

    @Override
    public void onPageSelected(int position, int adapterCount) {
        if(mIOnBoardingActivityView != null) {
            mIOnBoardingActivityView.changeViewPagerCurrentDots();
        }
    }

    @Override
    public void onNoAccessButtonClick(String btnText) {
        if (btnText.toLowerCase().equals(mContext.getString(R.string.retry_text).toLowerCase())){
            //mIOnBoardingActivityView.gotoLoginActivity();
        }
       // else  Request Access flow

    }

    @Override
    public void setMvpView(BaseView baseView) {
        mIOnBoardingActivityView = (OnBoardingContract.IOnBoardingActivityView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return mIOnBoardingActivityView;
    }
}
