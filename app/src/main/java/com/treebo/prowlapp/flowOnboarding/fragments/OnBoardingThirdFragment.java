package com.treebo.prowlapp.flowOnboarding.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.views.TreeboTextView;

/**
 * Created by abhisheknair on 17/10/16.
 */

public class OnBoardingThirdFragment extends com.treebo.prowlapp.fragments.BaseFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutInflater = inflater.inflate(R.layout.fragment_on_boarding_third, container, false);
        TreeboTextView title = (TreeboTextView) layoutInflater.findViewById(R.id.treeboTextView2);
        title.setText(BuildConfig.VERSION_NAME);
        return layoutInflater;
    }
}
