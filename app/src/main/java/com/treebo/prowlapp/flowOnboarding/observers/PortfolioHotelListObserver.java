package com.treebo.prowlapp.flowOnboarding.observers;

import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.flowOnboarding.OnBoardingContract;
import com.treebo.prowlapp.flowOnboarding.usecase.PortfolioHotelsListUseCase;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.common.HotelLocationResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.UseCase;

public class PortfolioHotelListObserver extends BaseObserver<HotelLocationResponse> {

    public OnBoardingContract.IPortfolioHotelsListView mView;

    public PortfolioHotelListObserver(BasePresenter presenter, PortfolioHotelsListUseCase useCase, OnBoardingContract.IPortfolioHotelsListView view) {
        super(presenter, useCase);
        mView = view;
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        mView.onHotelListLoadError(e.getMessage());
    }

    @Override
    public void onCompleted() {
        // mView.onHotelListLoadSuccess();
    }

    @Override
    public void onNext(HotelLocationResponse hotelLocationResponse) {
        for (HotelLocation hotelLocation : hotelLocationResponse.data.hotels) {
            hotelLocation.save();
        }

        mView.onHotelListLoadSuccess();
    }
}
