package com.treebo.prowlapp.flowOnboarding.presenter;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowOnboarding.OnBoardingContract;
import com.treebo.prowlapp.flowOnboarding.observers.GoogleLoginObserver;
import com.treebo.prowlapp.flowOnboarding.usecase.GoogleLoginUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.Utils.Utils;

/**
 * Created by sumandas on 18/10/2016.
 */
public class GoogleLoginPresenter implements OnBoardingContract.IGoogleLoginPresenter {

    public Context mContext;
    private OnBoardingContract.IGoogleLoginView mGoogleView;
    public int mPlayServiceAvailabilityStatus;

    public GoogleLoginPresenter(Context context) {
        mContext = context;
    }

    private GoogleLoginUseCase mGoogleLoginUseCase;


    @Override
    public void onCreate() {
        mPlayServiceAvailabilityStatus = mGoogleView.checkForPlayServices();
        if (mPlayServiceAvailabilityStatus != ConnectionResult.SUCCESS
                && Utils.isPlayServicesErrorUserRecoverable(mPlayServiceAvailabilityStatus)) {
            mGoogleView.showPlayServicesErrorDialog(mPlayServiceAvailabilityStatus);
        }
    }

    @Override
    public void googleSignInButtonClicked() {
        if (mPlayServiceAvailabilityStatus != ConnectionResult.SUCCESS) {
            if (Utils.isPlayServicesErrorUserRecoverable(mPlayServiceAvailabilityStatus)) {
                mGoogleView.showPlayServicesErrorDialog(mPlayServiceAvailabilityStatus);
            }
        } else {
            if (Utils.isInternetConnected(mContext)) {
                mGoogleView.startGoogleSignIn();
            } else {
                mGoogleView.showNoInternetScreen();
            }

        }
    }


    public void performGoogleLogin(GoogleSignInAccount googleSignInAccount, GoogleLoginUseCase googleLoginUseCase,
                                   GoogleLoginObserver googleLoginObserver) {
        mGoogleLoginUseCase = googleLoginUseCase;
        if (googleSignInAccount!=null) {
            sendGoogleIdToken(googleSignInAccount.getIdToken(), googleLoginUseCase, googleLoginObserver);
        }else {
                mGoogleView.onLoginFailure(mContext.getString(R.string.google_login_failed));
            }

    }


    @Override
    public void performGoogleLogin(GoogleSignInResult googleSignInResult, GoogleLoginUseCase googleLoginUseCase,
                                   GoogleLoginObserver googleLoginObserver) {
        mGoogleLoginUseCase = googleLoginUseCase;
        if (googleSignInResult.isSuccess()) {
            GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();
            if (googleSignInAccount != null) {
                sendGoogleIdToken(googleSignInAccount.getIdToken(), googleLoginUseCase, googleLoginObserver);
            } else {
                mGoogleView.onLoginFailure(mContext.getString(R.string.google_login_failed));
            }
        } else {
            mGoogleView.onLoginFailure(mContext.getString(R.string.google_login_failed));
        }
    }

    public void sendGoogleIdToken(String idToken, GoogleLoginUseCase googleLoginUseCase,
                                  GoogleLoginObserver googleLoginObserver) {
        if (TextUtils.isEmpty(idToken)) {
            mGoogleView.onLoginFailure(mContext.getString(R.string.google_login_failed));
        } else {
            mGoogleView.showLoading();
            googleLoginUseCase.setLoginFields(idToken);
            googleLoginUseCase.execute(googleLoginObserver);
        }
    }

    @Override
    public void persistLoginTokens(LoginResponse loginResponse) {
        Utils.persistAuthToken(loginResponse);
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mGoogleView = (OnBoardingContract.IGoogleLoginView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if (mGoogleLoginUseCase != null) {
            mGoogleLoginUseCase.unsubscribe();
        }
    }

    @Override
    public BaseView getView() {
        return null;
    }
}
