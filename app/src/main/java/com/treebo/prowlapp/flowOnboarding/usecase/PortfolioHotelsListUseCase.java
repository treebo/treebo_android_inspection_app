package com.treebo.prowlapp.flowOnboarding.usecase;

import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.common.HotelLocationResponse;
import com.treebo.prowlapp.usecase.UseCase;

import rx.Observable;
import rx.Subscriber;

public class PortfolioHotelsListUseCase extends UseCase<HotelLocationResponse> {

    public RestClient mRestClient;
    public int mUserId;


    public PortfolioHotelsListUseCase(RestClient restClient) {
        this.mRestClient = restClient;
    }

    public void setUserId(int userId) {
        this.mUserId = userId;
    }

    @Override
    protected Observable<HotelLocationResponse> getObservable() {
        return mRestClient.getPortfolioHotelsLocation(mUserId);
    }


}
