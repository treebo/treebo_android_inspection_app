package com.treebo.prowlapp.flowOnboarding.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.view.Window;
import android.view.WindowManager;

import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AppUpdatedEvent;
import com.treebo.prowlapp.flowOnboarding.DaggerOnBoardingComponent;
import com.treebo.prowlapp.flowOnboarding.OnBoardingComponent;
import com.treebo.prowlapp.flowOnboarding.OnBoardingModule;
import com.treebo.prowlapp.job.OfflineJobUtils;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 08/03/17.
 */

public class BlockerActivity extends AppCompatActivity {

    @Inject
    public RxBus mRxBus;

    @Inject
    public Navigator mNavigator;

    @Inject
    public LoginSharedPrefManager mSharedPrefManager;

    public CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocker_screen);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        OnBoardingComponent onBoardingComponent = DaggerOnBoardingComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .onBoardingModule(new OnBoardingModule(this))
                .build();
        onBoardingComponent.injectBlockerActivity(this);

        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof AppUpdatedEvent) {
                navigateOutOfBlockerScreen();
            }
        }));

        if (mSharedPrefManager.getServerAppVersion() > BuildConfig.VERSION_CODE) {
            TreeboButton updateBtn = (TreeboButton) findViewById(R.id.update_app_button);
            updateBtn.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.treebo.prowl"));
                startActivity(intent);
                finish();
            });
        } else {
            navigateOutOfBlockerScreen();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSharedPrefManager.getServerAppVersion() > BuildConfig.VERSION_CODE) {
            OfflineJobUtils.fetchSystemProperties();
        } else {
            navigateOutOfBlockerScreen();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubscription);
    }

    private void navigateOutOfBlockerScreen() {
        if (mSharedPrefManager.isUserLoggedIn()) {
            mNavigator.navigateToSplashActivity(this);
        } else {
            mNavigator.navigateToLoginActivity(this);
        }
        finish();
    }
}
