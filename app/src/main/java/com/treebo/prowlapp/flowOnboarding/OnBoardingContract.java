package com.treebo.prowlapp.flowOnboarding;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.flowOnboarding.observers.GoogleLoginObserver;
import com.treebo.prowlapp.flowOnboarding.usecase.GoogleLoginUseCase;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.login.LoginResponse;

/**
 * Created by abhisheknair on 17/10/16.
 */

public interface OnBoardingContract {

    interface IOnBoardingActivityView extends BaseView {
        void loadViewPager();
        void loadViewPagerDots();
        void changeViewPagerCurrentDots();
        void gotoLocationSplashScreen();
        void gotoEnableLocationScreen(String userName);
        void showNoAccessErrorScreen(String message);
    }

    interface IOnBoardingActivityPresenter extends BasePresenter {
        void onCreate();
        void onViewPagerLoaded();
        void onResume(int currentPosition, int adapterCount);
        void onPageSelected(int position, int adapterCount);
        void onNoAccessButtonClick(String btnText);
    }

    interface IGoogleLoginView extends BaseView{
        int checkForPlayServices();
        void showPlayServicesErrorDialog(int status);
        void startGoogleSignIn();
        void onLoginSuccess(LoginResponse response);
        void onLoginFailure(String message);
        //void onRootCausesFetched(RootCauseResponse rootCause);
        void showNoInternetScreen();
        void showSlowInternetScreen();

    }

    interface IPortfolioHotelsListView extends BaseView{
        void onHotelListLoadSuccess();
        void onHotelListLoadError(String error);
    }

    interface IGoogleLoginPresenter extends BasePresenter {
        void onCreate();
        void googleSignInButtonClicked();
        void performGoogleLogin(GoogleSignInResult googleSignInResult, GoogleLoginUseCase googleLoginUseCase,
                                GoogleLoginObserver googleLoginObserver);
        void performGoogleLogin(GoogleSignInAccount account, GoogleLoginUseCase googleLoginUseCase,
                                GoogleLoginObserver googleLoginObserver);
/*        void callRootCausesApi(RootCauseUseCase rootCauseUseCase,
                               FetchRootCausesObserver fetchRootCausesObserver);*/
        void persistLoginTokens(LoginResponse loginResponse);
    }

}
