package com.treebo.prowlapp.flowOnboarding;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowOnboarding.activity.BlockerActivity;
import com.treebo.prowlapp.flowOnboarding.activity.OnBoardingActivity;

import dagger.Component;

/**
 * Created by sumandas on 18/10/2016.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,modules
        = {ActivityModule.class,OnBoardingModule.class})
public interface OnBoardingComponent {
    void injectOnboardingActvity(OnBoardingActivity onBoardingActivity);

    void injectBlockerActivity(BlockerActivity blockerActivity);
}
