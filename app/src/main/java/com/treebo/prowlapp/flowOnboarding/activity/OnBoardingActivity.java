package com.treebo.prowlapp.flowOnboarding.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AppUpdateEvent;
import com.treebo.prowlapp.flowOnboarding.DaggerOnBoardingComponent;
import com.treebo.prowlapp.flowOnboarding.OnBoardingComponent;
import com.treebo.prowlapp.flowOnboarding.OnBoardingContract;
import com.treebo.prowlapp.flowOnboarding.OnBoardingModule;
import com.treebo.prowlapp.flowOnboarding.adapter.OnBoardingPagerAdapter;
import com.treebo.prowlapp.flowOnboarding.observers.GoogleLoginObserver;
import com.treebo.prowlapp.flowOnboarding.presenter.GoogleLoginPresenter;
import com.treebo.prowlapp.flowOnboarding.presenter.OnBoardingPresenter;
import com.treebo.prowlapp.flowOnboarding.usecase.GoogleLoginUseCase;
import com.treebo.prowlapp.flowOnboarding.usecase.RootCauseUseCase;
import com.treebo.prowlapp.flowlocation.activity.EnableLocationActivity;
import com.treebo.prowlapp.flowlocation.activity.LocationSplashActivity;
import com.treebo.prowlapp.job.OfflineJobUtils;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.LocationUtils;
import com.treebo.prowlapp.Utils.MixPanelManager;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import javax.inject.Inject;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 17/10/16.
 */
public class OnBoardingActivity extends AppCompatActivity implements OnBoardingContract.IOnBoardingActivityView,
        OnBoardingContract.IGoogleLoginView, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    private Button mLoginButton;
    private LinearLayout mViewPagerDotesContainer;

    public static final String INTENT_EXTRAS_UNAUTH_ERROR = "unauth_error";

    @Inject
    ProgressDialog progress;

    @Inject
    public OnBoardingPresenter mOnBoardingActivityPresenter;

    @Inject
    public GoogleLoginPresenter mGoogleLoginPresenter;

    @Inject
    public GoogleLoginUseCase mGoogleLoginUseCase;


    @Inject
    public RootCauseUseCase mRootCauseUseCase;

    @Inject
    public MixpanelAPI mMixpanelApi;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public RxBus mRxBus;

    @Inject
    public Navigator mNavigator;

    private GoogleApiClient mGoogleApiClient;


    private View mRootview;

    private static final int RC_SIGN_IN = 10133;

    private static final int ACCOUNT_PICKER_REQUEST_CODE = 10198;

    private View mNoAccessView;
    private ImageView mImageView;
    private TreeboTextView mErrorTextView;
    private TreeboTextView mAccessErrorTypeView;
    private TreeboButton mRetry;

    private GoogleSignInOptions mGoogleSignInOption;

    private GoogleSignInClient mGoogleSigninClient;

    private String mSelectedAccount;

    private CompositeSubscription mSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_on_boarding);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initOnBoardingComponent();

        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mLoginButton = (Button) findViewById(R.id.loginButton);
        mRootview = findViewById(R.id.rootView);

        mViewPagerDotesContainer = (LinearLayout) findViewById(R.id.viewPagerDotesContainer);

        //no access layout elements
        mNoAccessView = findViewById(R.id.no_access_layout);
        mErrorTextView = (TreeboTextView) findViewById(R.id.error_description_tv);
        mAccessErrorTypeView = (TreeboTextView) findViewById(R.id.no_access_tv);
        mRetry = (TreeboButton) findViewById(R.id.retry_button);
        mImageView = (ImageView) findViewById(R.id.no_access_iv);

        mSubscription = new CompositeSubscription();

        mSubscription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof AppUpdateEvent) {
                mNavigator.navigateToBlockerScreen(this);
            }
        }));

        mLoginButton.setOnClickListener(view -> {
            mGoogleLoginPresenter.googleSignInButtonClicked();
        });


        mRetry.setOnClickListener(view -> {
            mNoAccessView.setVisibility(View.GONE);
            mGoogleLoginPresenter.googleSignInButtonClicked();
        });

        mOnBoardingActivityPresenter.onCreate();
        mGoogleLoginPresenter.onCreate();

        Intent intent = getIntent();
        if (intent != null) {
            boolean showError = intent.getBooleanExtra(INTENT_EXTRAS_UNAUTH_ERROR, false);
            if (showError) {
                SnackbarUtils.show(mRootview, getString(R.string.error_logout));
            }
        }
    }

    public void initOnBoardingComponent() {
        OnBoardingComponent onBoardingComponent = DaggerOnBoardingComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .onBoardingModule(new OnBoardingModule(this))
                .build();
        onBoardingComponent.injectOnboardingActvity(this);
    }

    @Inject
    public void setUp() {
        mOnBoardingActivityPresenter.setMvpView(this);
        mGoogleLoginPresenter.setMvpView(this);
    }

    @Override
    public void loadViewPager() {
        if (mPagerAdapter == null) {
            mPagerAdapter = new OnBoardingPagerAdapter(getSupportFragmentManager());
            mViewPager.setAdapter(mPagerAdapter);
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    mOnBoardingActivityPresenter.onPageSelected(position,
                            mViewPager.getAdapter().getCount());
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
            mOnBoardingActivityPresenter.onViewPagerLoaded();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case RESULT_OK:
                if (requestCode == RC_SIGN_IN) {
                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    handleSignInResult(task);
                    Log.i("Google Sign in ", "Received ok result");
                    /*GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                    if (googleSignInResult != null && googleSignInResult.getSignInAccount() != null)
                        mSelectedAccount = googleSignInResult.getSignInAccount().getEmail();
                    mGoogleLoginPresenter.performGoogleLogin(googleSignInResult,
                            mGoogleLoginUseCase, providesGoogleLoginObserver(mGoogleLoginPresenter, mGoogleLoginUseCase, this));*/

                }
                break;
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> accountTask) {
        try {
            GoogleSignInAccount account = accountTask.getResult(ApiException.class);
            mSelectedAccount = account.getEmail();
            mGoogleLoginPresenter.performGoogleLogin(account,
                    mGoogleLoginUseCase, providesGoogleLoginObserver(mGoogleLoginPresenter, mGoogleLoginUseCase, this));
        } catch (ApiException e) {
            showError(e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mOnBoardingActivityPresenter.onResume(mViewPager.getCurrentItem(),
                mViewPager.getAdapter().getCount());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleLoginPresenter.destroy();
        Utils.unSubscribe(mSubscription);
    }

    @Override
    public void loadViewPagerDots() {
        mViewPagerDotesContainer.removeAllViews();

        int numberOfDots = 0;
        PagerAdapter pagerAdapter = mViewPager.getAdapter();
        if (pagerAdapter != null) {
            numberOfDots = pagerAdapter.getCount();
        }

        int dotSize = (int) getResources().getDimension(R.dimen.on_boarding_dot_size);
        for (int i = 0; i < numberOfDots; ++i) {
            View view = getLayoutInflater().inflate(R.layout.view_on_boarding_dots,
                    mViewPagerDotesContainer, false);
            view.setLayoutParams(new RelativeLayout.LayoutParams(dotSize, dotSize));
            view.setBackgroundResource(R.drawable.bg_on_boarding_dot_unselected);
            mViewPagerDotesContainer.addView(view);
        }
    }

    @Override
    public void onBackPressed() {
        if (mNoAccessView.getVisibility() == View.VISIBLE)
            mNoAccessView.setVisibility(View.GONE);
        else
            super.onBackPressed();
    }

    @Override
    public void changeViewPagerCurrentDots() {
        int currentPosition = mViewPager.getCurrentItem();
        Object tag = mViewPagerDotesContainer.getTag();
        if (tag != null) {
            int selectedPosition = (int) mViewPagerDotesContainer.getTag();
            mViewPagerDotesContainer.getChildAt(selectedPosition).setBackgroundResource(R.drawable.bg_on_boarding_dot_unselected);
        }
        mViewPagerDotesContainer.getChildAt(currentPosition).setBackgroundResource(R.drawable.bg_on_boarding_dot_selected);
        mViewPagerDotesContainer.setTag(currentPosition);
    }

    @Override
    public void gotoLocationSplashScreen() {
        Intent intent = LocationSplashActivity.getCallingIntent(this);
        startActivity(intent);
    }

    @Override
    public void gotoEnableLocationScreen(String userName) {
        Intent intent = EnableLocationActivity.getCallingIntent(this);
        intent.putExtra(Utils.USER_NAME, userName);
        startActivity(intent);
    }

    @Override
    public void showNoAccessErrorScreen(String message) {
        mNoAccessView.setVisibility(View.VISIBLE);
        mAccessErrorTypeView.setText(getString(R.string.no_access));
        mImageView.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_no_access));
        if (message.equals("User doesnt have access to prowl")) {
            mErrorTextView.setText(String.format(getString(R.string.email_has_no_access),
                    mSelectedAccount != null ? mSelectedAccount : "User"));
        } else {
            mErrorTextView.setText(message.equals("Invalid email") ?
                    getString(R.string.use_treebo_email) :
                    message);
        }
    }

    @Override
    public void showNoInternetScreen() {
        mNoAccessView.setVisibility(View.VISIBLE);
        mAccessErrorTypeView.setText(getString(R.string.mobile_data_disabled));
        mErrorTextView.setText(getString(R.string.switch_on_internet));
        mImageView.setBackground(ContextCompat.getDrawable(this, R.drawable.group_2));

    }

    @Override
    public void showSlowInternetScreen() {
        mNoAccessView.setVisibility(View.VISIBLE);
        mAccessErrorTypeView.setText(getString(R.string.slow_connectivity));
        mErrorTextView.setText("");
        mImageView.setBackgroundResource(R.drawable.group_2);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        progress.setTitle("Login in progress");
        progress.setMessage("Please wait...");
        progress.setCancelable(true);
        progress.show();
    }


    @Override
    public void hideLoading() {
        if (progress != null && !this.isFinishing()) {
            progress.dismiss();
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, OnBoardingActivity.class);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (!mLoginManager.isUserLoggedIn()) {
            mGoogleApiClient.clearDefaultAccountAndReconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        SnackbarUtils.show(mRootview, Constants.MSG_CONNECTION_TO_GOOGLE_CLIENT_FAILED);
    }

    @Override
    public int checkForPlayServices() {
        return Utils.isGooglePlayServicesAvailable(this);
    }

    @Override
    public void showPlayServicesErrorDialog(int status) {
        Utils.showPlayServicesErrorDialog(this, status);
    }

    @Override
    public void startGoogleSignIn() {
        mGoogleSignInOption = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_server_client_id))
                .requestProfile()
                .requestEmail()
                .build();

        mGoogleSigninClient = GoogleSignIn.getClient(this, mGoogleSignInOption);
        Intent signInIntent = mGoogleSigninClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);

        /*if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, mGoogleSignInOption)
                    .build();
        }
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        startActivityForResult(signInIntent, RC_SIGN_IN);*/

    }

    public void addLogMessage(String message) {
        Log.i(OnBoardingActivity.class.getName(), message);
    }

    @Override
    public void onLoginSuccess(LoginResponse loginResponse) {
        addLogMessage("Mixpanel call start");
        if(loginResponse == null){
            addLogMessage("Null Login response");
        }
        MixPanelManager.registerUserNameEmail(mMixpanelApi, loginResponse.getUserId(), loginResponse.getEmailId());
        MixPanelManager.trackEvent(mMixpanelApi, MixPanelManager.EVENT_LOGIN_SUCCESS, MixPanelManager.SCREEN_LOGIN);
        addLogMessage("Before persisting tokens");
        mGoogleLoginPresenter.persistLoginTokens(loginResponse);
        addLogMessage("After persisting tokens");
        if (LocationUtils.isLocationEnabled(this)) {
            gotoLocationSplashScreen();
        } else {
            String name = !(TextUtils.isEmpty(mLoginManager.getUsername())) ? mLoginManager.getUsername() : mLoginManager.getEmailId();
            gotoEnableLocationScreen(name);
        }
        SnackbarUtils.show(mRootview, "Login Success");

        OfflineJobUtils.fetchMasterList();

        OfflineJobUtils.fetchAuditList();

        OfflineJobUtils.fetchHotelLocations();

        OfflineJobUtils.fetchSystemProperties();

        OfflineJobUtils.fetchBankList();

        if (!mLoginManager.isClearCompleteAuditJobStarted()) {
            OfflineJobUtils.clearCompleteAuditsPeriodic();
            mLoginManager.setCompleteAuditJobStarted(true);
        }

        finish();

    }

    @Override
    public void onLoginFailure(String message) {
        SnackbarUtils.show(mRootview, message);
        showNoAccessErrorScreen(message);
        mGoogleSigninClient.signOut();
    }

    public GoogleLoginObserver providesGoogleLoginObserver(BasePresenter basePresenter, GoogleLoginUseCase googleLoginUseCase,
                                                           OnBoardingContract.IGoogleLoginView view) {
        return new GoogleLoginObserver(basePresenter, googleLoginUseCase, view);
    }


}

