package com.treebo.prowlapp.flowOnboarding.usecase;

import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

import rx.Observable;

/**
 * Created by sumandas on 18/10/2016.
 */

public class GoogleLoginUseCase extends UseCase<LoginResponse> {

    public String mIdToken;
    public RestClient mRestClient;

    public GoogleLoginUseCase(RestClient restClient) {
        mRestClient = restClient;
    }

    public void setLoginFields(String idToken) {
        mIdToken = idToken;
    }

    @Override
    protected Observable<LoginResponse> getObservable() {
        return mRestClient.googleLogin(mIdToken, Constants.VERSION_V3);
    }
}