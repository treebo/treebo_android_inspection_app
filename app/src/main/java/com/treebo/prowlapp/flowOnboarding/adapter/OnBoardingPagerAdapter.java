package com.treebo.prowlapp.flowOnboarding.adapter;



import com.treebo.prowlapp.flowOnboarding.fragments.OnBoardingFirstFragment;
import com.treebo.prowlapp.flowOnboarding.fragments.OnBoardingSecondFragment;
import com.treebo.prowlapp.flowOnboarding.fragments.OnBoardingThirdFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Created by abhisheknair on 17/10/16.
 */

public class OnBoardingPagerAdapter extends FragmentPagerAdapter {

    private static final int NUM_PAGES = 3;

    public OnBoardingPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position % getCount()) {
            case 0:
                fragment = new OnBoardingFirstFragment();
                break;

            case 1:
                fragment = new OnBoardingSecondFragment();
                break;

            case 2:
                fragment = new OnBoardingThirdFragment();
                break;

            default:
                fragment = new OnBoardingFirstFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
