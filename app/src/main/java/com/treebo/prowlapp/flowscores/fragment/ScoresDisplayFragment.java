package com.treebo.prowlapp.flowscores.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowscores.HotelScoresActivity;
import com.treebo.prowlapp.flowscores.adapter.FOTScoresViewPagerAdapter;
import com.treebo.prowlapp.flowscores.adapter.GuestFeedbackScoresViewPagerAdapter;
import com.treebo.prowlapp.flowscores.adapter.HotelScoresViewPagerAdapter;
import com.treebo.prowlapp.flowscores.adapter.QAScoresViewPagerAdapter;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.GroupedScoresModel;
import com.treebo.prowlapp.Utils.MapperUtils;
import com.treebo.prowlapp.views.SlidingTabLayout;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by devesh on 01/05/16.
 */
@Deprecated
public class ScoresDisplayFragment extends BaseFragment {

    private static final String SCORES_MODEL = "scores_model";
    GroupedScoresModel scoresModel;
    View mRootView;
    SlidingTabLayout mSlidingTabLayout;
    ViewPager mViewPager;
    @BindView(R.id.toolbar_title)
    TextView title;

    public static ScoresDisplayFragment newInstance(GroupedScoresModel model) {
        ScoresDisplayFragment fragment = new ScoresDisplayFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SCORES_MODEL, model);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            scoresModel = getArguments().getParcelable(SCORES_MODEL);
        }
    }

    private void setActionBarTitle() {
        if(scoresModel.heading.contains("QA") || scoresModel.heading.contains("FOT")){
            ((HotelScoresActivity) getActivity()).setActionBarTitle(scoresModel.heading + "Score");

        }else{
        ((HotelScoresActivity) getActivity()).setActionBarTitle(scoresModel.heading);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_scores_display, container, false);
        ButterKnife.bind(this,mRootView);
        title.setText(scoresModel.heading);
        mSlidingTabLayout = (SlidingTabLayout) mRootView.findViewById(R.id.tabs);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setSelectedIndicatorColors(0xFFFFFFFF);
        mSlidingTabLayout.setTabTextColor(getResources().getColor(R.color.white));
        setActionBarTitle();

        mViewPager = (ViewPager) mRootView.findViewById(R.id.scores_pager);
        if (scoresModel.heading.contains("QA")) {
            mViewPager.setAdapter(new QAScoresViewPagerAdapter(getActivity(),scoresModel.scores.overall,  MapperUtils.mapHeadingToDaywiseScore(scoresModel.scores.subHeading)));
        } else if (scoresModel.heading.contains("FOT")) {
            mViewPager.setAdapter(new FOTScoresViewPagerAdapter(getActivity(), MapperUtils.mapHeadingToDaywiseScore(scoresModel.scores.subHeading)));
        } else if (scoresModel.heading.contains("Net Promoter Score")) {
            mViewPager.setAdapter(new GuestFeedbackScoresViewPagerAdapter(getActivity(), MapperUtils.mapHeadingToDaywiseScore(scoresModel.scores.subHeading)));
        }
        else {
            mViewPager.setAdapter(new HotelScoresViewPagerAdapter(getActivity(), MapperUtils.mapHeadingToDaywiseScore(scoresModel.scores.subHeading)));
        }
        mSlidingTabLayout.setViewPager(mViewPager);
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setActionBarTitle();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            setActionBarTitle();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }
    @OnClick(R.id.image_view_back)
    void click(){
        getActivity().onBackPressed();
        //getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }


}
