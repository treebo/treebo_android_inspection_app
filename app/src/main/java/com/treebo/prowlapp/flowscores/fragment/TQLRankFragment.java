package com.treebo.prowlapp.flowscores.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.GroupedScoresModel;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by devesh on 26/05/16.
 */
@Deprecated
public class TQLRankFragment extends BaseFragment {

    GroupedScoresModel tqlModel;

    private View mRootView;

    private static final String TQL_RANK = "tql_rank";


    @BindView(R.id.text_tql_month)
    TextView mTqlMonthTextView;

    @BindView(R.id.text_tql)
    TextView mTqlScore;

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;

    @BindView(R.id.image_view_back)
    ImageView backButton;


    public TQLRankFragment(){
        // do nothing in the default constructor
    }


    public static TQLRankFragment newInstance(GroupedScoresModel model){
        TQLRankFragment fragment = new TQLRankFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(TQL_RANK, model);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tqlModel = getArguments().getParcelable(TQL_RANK);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.tql_rank_layout, container , false);
        ButterKnife.bind(this,mRootView);
        Log.d("TQLRankFragment", tqlModel.toString());
        int tqlRank = (int)tqlModel.scores.overall;
        if(tqlRank == 0){
            mTqlScore.setText("-");
        }else {
            mTqlScore.setText(""+(int)tqlModel.scores.overall);
            mTqlMonthTextView.setText(String.format("in %s", tqlModel.scores.tqlMonth));
        }
        mToolbarTitle.setText("TQL Rank");
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return mRootView;
    }




}
