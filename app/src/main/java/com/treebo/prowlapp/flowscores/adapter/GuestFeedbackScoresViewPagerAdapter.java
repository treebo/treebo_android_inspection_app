package com.treebo.prowlapp.flowscores.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.ScoresData;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.CircularProgressView;
import com.treebo.prowlapp.views.PairPercentLayout2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.viewpager.widget.PagerAdapter;


/**
 * Created by
 * devesh on 05/05/16.
 */
@Deprecated
public class GuestFeedbackScoresViewPagerAdapter extends PagerAdapter {
    public static String SEVEN_DAYS ="7 Days";
    public static String THIRTY_DAYS ="30 Days";
    public Double overall;
    HashMap<String, List<ScoresData>> scoresDataMap;



    Context mContext;

    public GuestFeedbackScoresViewPagerAdapter(Context context ,HashMap<String, List<ScoresData>> scoresDataMap){
        this.scoresDataMap = scoresDataMap;
        mContext = context;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.scores_subheading_nps, container, false);
        CircularProgressView progressView =(CircularProgressView) layout.findViewById(R.id.progress_view_current);
        progressView.setIsPercentage(false);
        LinearLayout linearLayoutfillRate = (LinearLayout) layout.findViewById(R.id.layout_text_fill_rate);
        LinearLayout linearLayoutRemaining = (LinearLayout) layout.findViewById(R.id.layout_text_remaining);

        TextView scoreHeadingTextView = (TextView) layout.findViewById(R.id.score_text);
        scoreHeadingTextView.setVisibility(View.VISIBLE);

        TextView subheading = (TextView) layout.findViewById(R.id.subheading);
        subheading.setVisibility(View.VISIBLE);
        switch(position){
            case 0 : addTextLayouts(SEVEN_DAYS,linearLayoutfillRate,linearLayoutRemaining, progressView,scoreHeadingTextView );
                break;
            case 1 : addTextLayouts(THIRTY_DAYS,  linearLayoutfillRate,linearLayoutRemaining, progressView, scoreHeadingTextView);
                break;
        }
        container.addView(layout);
        return layout;
    }

    private void addTextLayouts(String value, LinearLayout linearLayoutfillRate,LinearLayout linearLayoutRemaining, CircularProgressView progressView, TextView scoreHeadingTextView) {
        linearLayoutfillRate.removeAllViews();
        linearLayoutRemaining.removeAllViews();

        List<ScoresData> scoresDataList = scoresDataMap.get(value);
        List<ScoresData> scoresDataListToBeAdded = new ArrayList<>();
        ScoresData netPromoterScoresData =null;
        for(ScoresData scoresData : scoresDataList){
            Log.v(this.getClass().getSimpleName(), ""+scoresDataList);
            if(scoresData.mName.equalsIgnoreCase("Net Promoter Score")){
                netPromoterScoresData = scoresData;
            }else{
                scoresDataListToBeAdded.add(scoresData);
            }
        }
        if(netPromoterScoresData != null) {
            progressView.setColor(Utils.getNPSColor((int) netPromoterScoresData.mValue) );
            scoreHeadingTextView.setText("" + (int) netPromoterScoresData.mValue);
            scoreHeadingTextView.setTextColor(mContext.getResources().getColor(Utils.getNPSColor((int) netPromoterScoresData.mValue)));
            progressView.setProgress((float)netPromoterScoresData.mValue);
            progressView.setColor(mContext.getResources().getColor(Utils.getNPSColor((int) netPromoterScoresData.mValue)));

        }

        for(ScoresData scoresData : scoresDataListToBeAdded){
            if(scoresData.mName.equalsIgnoreCase("Fill Rate")){
                PairPercentLayout2 pairTextLayout = new PairPercentLayout2(mContext);
                pairTextLayout.setScoreData(scoresData);
                pairTextLayout.setTextViewColor(Utils.getFillRateColor(scoresData.mValue));
                linearLayoutfillRate.addView(pairTextLayout);

            }else {
                PairPercentLayout2 pairTextLayout = new PairPercentLayout2(mContext);
                pairTextLayout.setTextViewColor(Utils.getFillRateColor(scoresData.mValue));
                pairTextLayout.setScoreData(scoresData);
                linearLayoutRemaining.addView(pairTextLayout);
            }
        }

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position){
            case 0: return SEVEN_DAYS;
            case 1: return THIRTY_DAYS;
        }
        return "";
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}