package com.treebo.prowlapp.flowscores;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.flowscores.fragment.ScoresListFragment;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.HotelDataResponse;

import androidx.fragment.app.FragmentManager;
import butterknife.ButterKnife;

/**
 * Created by devesh on 30/04/16.
 */
@Deprecated
public class HotelScoresActivity extends BaseActivity {

    public static final String SCORES_HOTEL_DATA = "hotel_data_scores";
    private static final String TAG = "HotelScoresActivity";

    HotelDataResponse mHotelData;

    FrameLayout mParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores_main);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getParcelable(SCORES_HOTEL_DATA) != null) {
            mHotelData = bundle.getParcelable(SCORES_HOTEL_DATA);
        }
        mParent = (FrameLayout) findViewById(R.id.container);
        addFragment(ScoresListFragment.newInstance(mHotelData));

    }

    public static Intent getCallingIntent(Context context, HotelDataResponse hotelData) {
        Intent startIntent = new Intent(context, HotelScoresActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(SCORES_HOTEL_DATA, hotelData);
        startIntent.putExtras(bundle);
        return startIntent;
    }


    public void addFragment(BaseFragment fragment) {
        Log.v(TAG, "addFragment called ");
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

}
