package com.treebo.prowlapp.flowscores.fragment;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.flowscores.HotelScoresActivity;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.GroupedScoresModel;
import com.treebo.prowlapp.Models.SubHeadingScoresModel;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.MapperUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.util.HashMap;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by devesh on 01/05/16.
 */
@Deprecated
public class GuestReviewFragment extends BaseFragment {

    private static final String GUEST_RV_KEY = "guest_rv";
    private View mRootView;
    GroupedScoresModel scoresModel;
    TextView displayedRatingsTextView;
    LinearLayout displayedRatingsLayoutCurrent;
    LinearLayout displayedRatingsLayoutWeek;
    LinearLayout displayedRatingsLayoutMonth;
    TextView newReviewScoreTextView;
    LinearLayout newReviewScoreLayoutCurrent;
    LinearLayout newReviewScoreLayoutWeek;
    LinearLayout newReviewScoreLayoutMonth;
    TextView numberOfNewReviewsTextView;
    LinearLayout numberOfNewReviewsCurrent;
    LinearLayout numberOfNewReviewsWeek;
    LinearLayout numberOfNewReviewsMonth;
    LinearLayout numberOfNewReviewsLayout;
    @BindView(R.id.toolbar_title) TextView title;


    Double value = 1d;
    HashMap<String, SubHeadingScoresModel> subHeadingScoresModelHashMap = new HashMap<>();

    public static GuestReviewFragment newInstance(GroupedScoresModel model) {
        GuestReviewFragment fragment = new GuestReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(GUEST_RV_KEY, model);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            scoresModel = getArguments().getParcelable(GUEST_RV_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_guest_layout, container, false);
        ButterKnife.bind(this,mRootView);
        title.setText(scoresModel.heading);
        bindViews();


        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setActionBarTitle();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            setActionBarTitle();
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void setActionBarTitle() {
        ((HotelScoresActivity) getActivity()).setActionBarTitle(scoresModel.heading);
    }

    private void bindViews() {
        subHeadingScoresModelHashMap = MapperUtils.mapSubHeadeingScoresListToHashMap(scoresModel.scores.subHeading);
        //TODO Remove this when values start coming from server
        testScores();






        displayedRatingsTextView = (TextView) mRootView.findViewById(R.id.diplayed_ratings_category_name);
        displayedRatingsLayoutCurrent = (LinearLayout) mRootView.findViewById(R.id.diplayed_ratings_current);
        displayedRatingsLayoutWeek = (LinearLayout) mRootView.findViewById(R.id.diplayed_ratings_week);
        displayedRatingsLayoutMonth = (LinearLayout) mRootView.findViewById(R.id.diplayed_ratings_month);
        displayedRatingsTextView.setText("Displayed Rating");
        //setDisplayedAuditViews(displayedRatingsLayout);
        setUpDisplayedCurrent(displayedRatingsLayoutCurrent);
        setUpDisplayedWeek(displayedRatingsLayoutWeek);
        setUpDiplayedMonth(displayedRatingsLayoutMonth);

        // newReviewScoreTextView = (TextView) mRootView.findViewById(R.id.diplayed_ratings_current);
        // newReviewScoreLayoutCurrent = (LinearLayout)mRootView.findViewById(R.id.diplayed_ratings_current);
        newReviewScoreTextView = (TextView) mRootView.findViewById(R.id.new_reviews_category_name);
        newReviewScoreLayoutCurrent = (LinearLayout) mRootView.findViewById(R.id.new_reviews_category_current);
        newReviewScoreLayoutWeek = (LinearLayout) mRootView.findViewById(R.id.new_reviews_category_week);
        newReviewScoreLayoutMonth = (LinearLayout) mRootView.findViewById(R.id.new_reviews_category_month);
        newReviewScoreTextView.setText("New Reviews Score");


        //setNewRatingsViews(newReviewScoreLayout);
        setUpNewReviewCurrent(newReviewScoreLayoutCurrent);
        setUpNewReviewWeek(newReviewScoreLayoutWeek);
        setUpNewReviewMonth(newReviewScoreLayoutMonth);

        numberOfNewReviewsTextView = (TextView) mRootView.findViewById(R.id.number_category_name);
        numberOfNewReviewsCurrent = (LinearLayout) mRootView.findViewById(R.id.number_current);
        numberOfNewReviewsWeek = (LinearLayout) mRootView.findViewById(R.id.number_week);
        numberOfNewReviewsMonth = (LinearLayout) mRootView.findViewById(R.id.number_month);
        numberOfNewReviewsTextView.setText("Number of New Reviews");

        //setNumbersViews(numberOfNewReviewsLayout);
        setUpNewReviewNumberCurrent(numberOfNewReviewsCurrent);
        setUpNewReviewNumberWeek(numberOfNewReviewsWeek);
        setUpNewReviewNumberMonth(numberOfNewReviewsMonth);

    }

    private void testScores() {

    }


    private void setUpNewReviewNumberMonth(LinearLayout newReviewNumberLayout) {
        TextView displayedRatingsNameTextView = (TextView) newReviewNumberLayout.findViewById(R.id.name);
        displayedRatingsNameTextView.setText("30 Days");
        Double value = subHeadingScoresModelHashMap.get(Constants.NUMBER_OF_NEW_REVIEWS).last_thirty_days;
        TextView displayedRatingsScoreTextView = (TextView) newReviewNumberLayout.findViewById(R.id.score);
        displayedRatingsScoreTextView.setText(value.intValue() + "");
       // displayedRatingsScoreTextView.setTextColor(getResources().getColor(R.color.emerald));
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(Utils.getNewReviewsColor(value)));

    }

    private void setUpNewReviewNumberWeek(LinearLayout newReviewNumberLayout) {
        TextView displayedRatingsNameTextView = (TextView) newReviewNumberLayout.findViewById(R.id.name);
        displayedRatingsNameTextView.setText("7 Days");
        Double value = subHeadingScoresModelHashMap.get(Constants.NUMBER_OF_NEW_REVIEWS).last_seven_days;

        TextView displayedRatingsScoreTextView = (TextView) newReviewNumberLayout.findViewById(R.id.score);
        displayedRatingsScoreTextView.setText(value.intValue()+"");
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(R.color.emerald));
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(Utils.getNewReviewsColor(value)));


    }

    private void setUpNewReviewNumberCurrent(LinearLayout newReviewNumberLayout) {
        newReviewNumberLayout.setVisibility(View.INVISIBLE);
        TextView displayedRatingsNameTextView = (TextView) newReviewNumberLayout.findViewById(R.id.name);
        displayedRatingsNameTextView.setText("Current");
        Double value = subHeadingScoresModelHashMap.get(Constants.NUMBER_OF_NEW_REVIEWS).current;
        TextView displayedRatingsScoreTextView = (TextView) newReviewNumberLayout.findViewById(R.id.score);
        displayedRatingsScoreTextView.setText(value.intValue()+"");
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(R.color.emerald));
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(Utils.getNewReviewsColor(value)));



    }

    private void setUpNewReviewMonth(LinearLayout newReviewScoreLayout) {
        TextView displayedRatingsNameTextView = (TextView) newReviewScoreLayout.findViewById(R.id.name);
        displayedRatingsNameTextView.setText("30 Days");
        TextView displayedRatingsScoreTextView = (TextView) newReviewScoreLayout.findViewById(R.id.score);
        Double value = subHeadingScoresModelHashMap.get(Constants.NEW_REVIEW_SCORE).last_thirty_days;
        ImageView star = (ImageView)newReviewScoreLayout.findViewById(R.id.image_view_star);
        star.setImageDrawable(getResources().getDrawable(Utils.getStarResourceForValue(value)));


        displayedRatingsScoreTextView.setText(value+"");
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(R.color.emerald));
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(Utils.getStarRatingValue(value)));


    }

    private void setUpNewReviewWeek(LinearLayout newReviewScoreLayout) {
        TextView displayedRatingsNameTextView = (TextView) newReviewScoreLayout.findViewById(R.id.name);
        displayedRatingsNameTextView.setText("7 Days");
        TextView displayedRatingsScoreTextView = (TextView) newReviewScoreLayout.findViewById(R.id.score);
        Double value = subHeadingScoresModelHashMap.get(Constants.NEW_REVIEW_SCORE).last_seven_days;
        ImageView star = (ImageView)newReviewScoreLayout.findViewById(R.id.image_view_star);
        star.setImageDrawable(getResources().getDrawable(Utils.getStarResourceForValue(value)));



        displayedRatingsScoreTextView.setText(value+"");
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(R.color.emerald));
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(Utils.getStarRatingValue(value)));



    }

    private void setUpNewReviewCurrent(LinearLayout newReviewScoreLayout) {
        newReviewScoreLayout.setVisibility(View.INVISIBLE);
        TextView displayedRatingsNameTextView = (TextView) newReviewScoreLayout.findViewById(R.id.name);
        displayedRatingsNameTextView.setText("Current");
        Double value = subHeadingScoresModelHashMap.get(Constants.NEW_REVIEW_SCORE).current;
        ImageView star = (ImageView)newReviewScoreLayout.findViewById(R.id.image_view_star);
        star.setImageDrawable(getResources().getDrawable(Utils.getStarResourceForValue(value)));
        TextView displayedRatingsScoreTextView = (TextView) newReviewScoreLayout.findViewById(R.id.score);
        displayedRatingsScoreTextView.setText(value+"");
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(R.color.emerald));
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(Utils.getStarRatingValue(value)));


    }

    private void setUpDisplayedCurrent(LinearLayout displayedRatingsLayout) {
        TextView displayedRatingsNameTextView = (TextView) displayedRatingsLayout.findViewById(R.id.name);
        displayedRatingsNameTextView.setText("Current");
        Double value = subHeadingScoresModelHashMap.get(Constants.DISPLAYED_RATINGS).current;
        TextView displayedRatingsScoreTextView = (TextView) displayedRatingsLayout.findViewById(R.id.score);
        ImageView star = (ImageView)displayedRatingsLayout.findViewById(R.id.image_view_star);
        star.setImageDrawable(getResources().getDrawable(Utils.getStarResourceForValue(value)));
        displayedRatingsScoreTextView.setText(value+"");
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(R.color.emerald));
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(Utils.getStarRatingValue(value)));


    }

    private void setUpDisplayedWeek(LinearLayout displayedRatingsLayout) {
        TextView displayedRatingsNameTextView = (TextView) displayedRatingsLayout.findViewById(R.id.name);
        displayedRatingsNameTextView.setText("Last Week");
        Double value = subHeadingScoresModelHashMap.get(Constants.DISPLAYED_RATINGS).last_seven_days;
        TextView displayedRatingsScoreTextView = (TextView) displayedRatingsLayout.findViewById(R.id.score);
        ImageView star = (ImageView)displayedRatingsLayout.findViewById(R.id.image_view_star);
        star.setImageDrawable(getResources().getDrawable(Utils.getStarResourceForValue(value)));
        displayedRatingsScoreTextView.setText(value+"");
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(R.color.emerald));
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(Utils.getStarRatingValue(value)));

    }

    private void setUpDiplayedMonth(LinearLayout displayedRatingsLayout) {
        TextView displayedRatingsNameTextView = (TextView) displayedRatingsLayout.findViewById(R.id.name);
        displayedRatingsNameTextView.setText("4 Wks. Back");
        Double value = subHeadingScoresModelHashMap.get(Constants.DISPLAYED_RATINGS).last_thirty_days;
        TextView displayedRatingsScoreTextView = (TextView) displayedRatingsLayout.findViewById(R.id.score);
        ImageView star = (ImageView)displayedRatingsLayout.findViewById(R.id.image_view_star);
        star.setImageDrawable(getResources().getDrawable(Utils.getStarResourceForValue(value)));
        displayedRatingsScoreTextView.setText(value+"");
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(R.color.emerald));
        displayedRatingsScoreTextView.setTextColor(getResources().getColor(Utils.getStarRatingValue(value)));

    }

    @OnClick(R.id.image_view_back)
    void click(){
        getActivity().onBackPressed();
        //getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

}
