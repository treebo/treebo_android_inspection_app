package com.treebo.prowlapp.flowscores.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowscores.DaggerScoresComponent;
import com.treebo.prowlapp.flowscores.HotelScoresActivity;
import com.treebo.prowlapp.flowscores.ScoresComponent;
import com.treebo.prowlapp.flowscores.adapter.GroupedScoresAdapter;
import com.treebo.prowlapp.flowscores.presenter.HotelScoresPresenter;
import com.treebo.prowlapp.fragments.BaseFragment;
import com.treebo.prowlapp.Models.GroupedScoresModel;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.mvpviews.HotelScoresView;
import com.treebo.prowlapp.response.ScoresResponse;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.AlertDialogUtils;
import com.treebo.prowlapp.Utils.MapperUtils;
import com.treebo.prowlapp.Utils.MixPanelManager;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by devesh on 01/05/16.
 */
@Deprecated
public class ScoresListFragment extends BaseFragment implements GroupedScoresAdapter.ItemClickedListener,HotelScoresView {

    private static final String HOTEL_DATA = "hotel_data";

    ArrayList<GroupedScoresModel> mGroupedScoresModels = new ArrayList<>();

    HotelDataResponse mHotelData;

    @Inject
    HotelScoresPresenter mPresenter;

    @Inject
    ProgressDialog mProgress;

    @Inject
    MixpanelAPI mMixpanelAPI;


    private View mRootView;

    RecyclerView mRecyclerView;

    GroupedScoresAdapter mGroupedScoresAdapter;

    int guestReviewsPosition = -1;

    int tqlRankPosition = -1;



    @BindView(R.id.retry_view)
    RelativeLayout retryView;




    public static ScoresListFragment newInstance(HotelDataResponse hotelData) {
        ScoresListFragment fragment = new ScoresListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(HOTEL_DATA, hotelData);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mHotelData = getArguments().getParcelable(HOTEL_DATA);

        }
        ScoresComponent scoresComponent= DaggerScoresComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent()).
                        activityModule(new ActivityModule(getActivity())).build();
        scoresComponent.injectScoreListFragment(this);
        MixPanelManager.trackEvent(mMixpanelAPI,MixPanelManager.SCREEN_SCORES,MixPanelManager.EVENT_SCORE_LOAD);
    }

    private void findGuestReviewsPosition() {
        for (GroupedScoresModel model : mGroupedScoresModels) {
            if (model.heading.equalsIgnoreCase("Guest Reviews")) {
                guestReviewsPosition = mGroupedScoresModels.indexOf(model);
            }
            if (model.heading.equalsIgnoreCase("TQL Rank")) {
                tqlRankPosition = mGroupedScoresModels.indexOf(model);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_scores, container, false);
        ButterKnife.bind(this,mRootView);
        getAllScores(mHotelData.getHotelId());
        return mRootView;
    }



    @Inject
    public void initView() {
        mPresenter.setMvpView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.pause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.destroy();

    }


    void getAllScores(int hotel_id) {
        mPresenter.getAllScores(hotel_id);
    }


    @Override
    public void hideLoading() {
        if (mProgress != null)
            mProgress.dismiss();
    }

    @Override
    public void showRetry() {
        retryView.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideRetry() {
        retryView.setVisibility(View.GONE);

    }

    @Override
    public boolean showError(String errorMessage) {
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {
        AlertDialogUtils.showInternetDisconnectedDialog(getActivity(), ((dialog, which) -> {
                    dialog.dismiss();
                    getAllScores(mHotelData.getHotelId());
                }), (dialog1) -> {
                    dialog1.dismiss();
                }
        );


    }


    @OnClick(R.id.retry_button)
    void onRetryButtonClicked(){
        getAllScores(mHotelData.getHotelId());
    }


    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mProgress.setCancelable(false);
        mProgress.setTitle("Loading");
        mProgress.setMessage("Please wait..");
        if(!getActivity().isFinishing())
            mProgress.show();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setActionBarTitle();
        findViews();
    }

    private void setActionBarTitle() {
        ((HotelScoresActivity) getActivity()).setActionBarTitle("Scores");
    }

    private void findViews() {
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mGroupedScoresAdapter = new GroupedScoresAdapter(mGroupedScoresModels, guestReviewsPosition);
        mGroupedScoresAdapter.setOnItemClickedListener(this);
        mRecyclerView.setAdapter(mGroupedScoresAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        setActionBarTitle();
        mPresenter.resume();

    }

    @Override
    public void onItemClicked(int position) {
        if ((guestReviewsPosition == -1 || position != guestReviewsPosition) && position != tqlRankPosition) {
            ((HotelScoresActivity) getActivity()).addFragment(ScoresDisplayFragment.newInstance(mGroupedScoresModels.get(position)));
        } else if (position == guestReviewsPosition) {
           ((HotelScoresActivity) getActivity()).addFragment(GuestReviewFragment.newInstance(mGroupedScoresModels.get(position)));
        }else if(position == tqlRankPosition){
            ((HotelScoresActivity) getActivity()).addFragment(TQLRankFragment.newInstance(mGroupedScoresModels.get(position)));

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser){
            setActionBarTitle();
        }
    }

    @OnClick (R.id.image_view_back)
    void click(){
        getActivity().finish();
    }


    @Override
    public void onScoresLoaded(ScoresResponse response) {
        mGroupedScoresModels = MapperUtils.mapGroupedScoresModel(response);
        findGuestReviewsPosition();
        findViews();
    }
}
