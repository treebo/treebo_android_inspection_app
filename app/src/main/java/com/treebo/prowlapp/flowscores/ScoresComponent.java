package com.treebo.prowlapp.flowscores;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowscores.fragment.ScoresListFragment;

import dagger.Component;

/**
 * Created by sumandas on 09/08/2016.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,modules
        = {ActivityModule.class,ScoresModule.class})
public interface ScoresComponent {
    void injectScoreListFragment(ScoresListFragment scoresListFragment);

}

