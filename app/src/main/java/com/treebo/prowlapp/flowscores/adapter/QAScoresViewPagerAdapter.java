package com.treebo.prowlapp.flowscores.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.ScoresData;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.CircularProgressView;
import com.treebo.prowlapp.views.PairTextLayoutPercentage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.viewpager.widget.PagerAdapter;


/**
 * Created by
 * devesh on 05/05/16.
 */
@Deprecated
public class    QAScoresViewPagerAdapter extends PagerAdapter {

    HashMap<String, List<ScoresData>> mSubHeadingScoresModels;
    public static String CURRENT ="Last";
    public static String SEVEN_DAYS ="7 Days";
    public static String THIRTY_DAYS ="30 Days";
    public Double overall;

    Context mContext;

    public QAScoresViewPagerAdapter(Context context ,Double overall,  HashMap<String, List<ScoresData>> subHeadingScoresModels){
        mSubHeadingScoresModels = subHeadingScoresModels;
        this.overall = overall;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mSubHeadingScoresModels.keySet().size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.scores_subheading_view, container , false);
        CircularProgressView progressView =(CircularProgressView) layout.findViewById(R.id.progress_view_current);
        TextView scoreTextView = (TextView) layout.findViewById(R.id.score_text);
        scoreTextView.setVisibility(View.VISIBLE);
        TextView subheading = (TextView) layout.findViewById(R.id.subheading);
        subheading.setVisibility(View.GONE);
        LinearLayout linearLayoutBottom = (LinearLayout) layout.findViewById(R.id.layout_text);
        switch(position){
            case 0 : addTextLayouts(CURRENT,linearLayoutBottom, progressView,scoreTextView);
                break;
            case 1 : addTextLayouts(SEVEN_DAYS,  linearLayoutBottom, progressView,scoreTextView);
                break;
            case 2 : addTextLayouts(THIRTY_DAYS, linearLayoutBottom, progressView,scoreTextView);
                break;
        }
        container.addView(layout);
        return layout;
    }

    private void addTextLayouts(String value, LinearLayout linearLayout, CircularProgressView progressView, TextView scoreTextView) {
        linearLayout.removeAllViews();
        List<ScoresData> scoresDataList = mSubHeadingScoresModels.get(value);
        ScoresData qamData = null;
        List<ScoresData> scoresDataListToBeAdded = new ArrayList<>();

        for (ScoresData scoresData : scoresDataList) {
            if (scoresData.mName.contains("QAM Audit")) {
                qamData = scoresData;
            } else {
                scoresDataListToBeAdded.add(scoresData);
            }
        }

        if (qamData != null) {
            progressView.setColor(Utils.getFOtColor((int) qamData.mValue));
            scoreTextView.setText((int) qamData.mValue + "%");
            scoreTextView.setTextColor(mContext.getResources().getColor(Utils.getQAMOverallColor((int) qamData.mValue)));
            progressView.setScorePercentage((int) qamData.mValue);
            progressView.setProgress((float) qamData.mValue);
            progressView.setColor(mContext.getResources().getColor(Utils.getQAMOverallColor((int) qamData.mValue)));
        }

        for(ScoresData scoresData : scoresDataListToBeAdded){
            PairTextLayoutPercentage pairTextLayout = new PairTextLayoutPercentage(mContext);
            pairTextLayout.setScoreData(scoresData);
            if(scoresData.mName.equalsIgnoreCase("House Keeping")){
                pairTextLayout.setTextViewColor(Utils.getColorForHouseKeeping(scoresData.mValue));
            }else if(scoresData.mName.equalsIgnoreCase("Food & Beverage")){
                pairTextLayout.setTextViewColor(Utils.getColorForFnB(scoresData.mValue));
            }else if(scoresData.mName.equalsIgnoreCase("Front Reception")){
                pairTextLayout.setTextViewColor(Utils.getColorForFrontReception(scoresData.mValue));
            }else if(scoresData.mName.equalsIgnoreCase("Owner / Manager")){
                pairTextLayout.setTextViewColor(Utils.getColorForFrontReception(scoresData.mValue));
            }

            linearLayout.addView(pairTextLayout);
        }

       /*


        for(ScoresData scoresData : scoresDataList){
            PairTextLayoutPercentage pairTextLayout = new PairTextLayoutPercentage(mContext);
            pairTextLayout.setScoreData(scoresData);
            linearLayout.addView(pairTextLayout);
        }
       // progressView.setSubheading(value);
        scoreTextView.setText(overall.intValue() + "%");
        scoreTextView.setTextColor(mContext.getResources().getColor(Utils.getQAMOverallColor((int) overall.intValue())));
        progressView.setScorePercentage(overall.intValue());
        progressView.setColor(mContext.getResources().getColor(Utils.getQAMOverallColor((int) overall.intValue())));
        progressView.setProgress(overall.floatValue());*/
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position){
            case 0: return CURRENT;
            case 1: return SEVEN_DAYS;
            case 2: return THIRTY_DAYS;
        }
        return "";
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}