package com.treebo.prowlapp.flowscores.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Utils.Utils;

import java.util.List;

/**
 * Created by devesh on 13/04/16.
 */
@Deprecated
public class AuditScoresRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = AuditScoresRecyclerAdapter.class.getSimpleName();

    public List<Pair<String, String>> mAuditScores;
    private Context mContext;

    public AuditScoresRecyclerAdapter(Context context, List<Pair<String, String>> auditScores) {
        mAuditScores = auditScores;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.inflated_audit_score_view, parent, false);
        return new IndividualViewHolder(rootView);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Pair<String, String> score = mAuditScores.get(position);
        ((IndividualViewHolder) holder).scoreName.setText(score.first.toUpperCase());
        ((IndividualViewHolder) holder).scoreValue.setText(score.second);
        int colorScheme = Utils.getColorSchemeForScore(mContext, score.first, score.second);
        GradientDrawable bgShape = (GradientDrawable)((IndividualViewHolder) holder).scoreValue.getBackground();
        bgShape.setColor(ContextCompat.getColor(mContext,colorScheme));

    }

    @Override
    public int getItemCount() {
        return mAuditScores.size();
    }

    public class IndividualViewHolder extends RecyclerView.ViewHolder {
        public TextView scoreName;
        public TextView scoreValue;

        public IndividualViewHolder(View itemView) {
            super(itemView);
            scoreName = (TextView) itemView.findViewById(R.id.score_name);
            scoreValue = (TextView) itemView.findViewById(R.id.score_value);

        }
    }

}