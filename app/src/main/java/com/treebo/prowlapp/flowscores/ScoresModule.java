package com.treebo.prowlapp.flowscores;

import com.treebo.prowlapp.flowscores.presenter.HotelScoresPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 09/08/2016.
 */
@Module
public class ScoresModule {

    @Provides
    HotelScoresPresenter providesScoresPresenter(){
        return new HotelScoresPresenter();
    }
}
