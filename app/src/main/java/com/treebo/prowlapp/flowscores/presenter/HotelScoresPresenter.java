package com.treebo.prowlapp.flowscores.presenter;

import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.mvpviews.HotelScoresView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.response.ScoresResponse;
import com.treebo.prowlapp.subscriber.BaseObserver;
import com.treebo.prowlapp.usecase.GetAllScoresUseCase;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.Constants;

/**
 * Created by devesh on 30/04/16.
 *
 */
@Deprecated
public class HotelScoresPresenter implements BasePresenter {

    HotelScoresView mBaseView;

    UseCase getAllScoresUseCase;

    @Override
    public void setMvpView(BaseView baseView) {
        mBaseView = (HotelScoresView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    public void getAllScores(int hotel_id){
        hideRetry();
        showLoading();
        getAllScoresUseCase = new GetAllScoresUseCase(hotel_id);
        getAllScoresUseCase.execute(new HotelScoresSubscriber(HotelScoresPresenter.this));
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        if(getAllScoresUseCase != null){
            getAllScoresUseCase.unsubscribe();
        }
    }

    @Override
    public BaseView getView() {
        return mBaseView;
    }


    private void showLoading() {
        mBaseView.showLoading();
    }

    private void hideLoading() {
        mBaseView.hideLoading();
    }

    private void showRetry() {
        mBaseView.showRetry();
    }

    private void hideRetry() {
        mBaseView.hideRetry();
    }

    public class HotelScoresSubscriber extends BaseObserver<ScoresResponse> {

        public HotelScoresSubscriber(BasePresenter basePresenter){
            super(basePresenter, getAllScoresUseCase);
        }


        @Override
        public void onError(Throwable e) {
            super.onError(e);
            hideLoading();
            mBaseView.showError(e.getMessage());
        }

        @Override
        public void onCompleted() {
            hideLoading();
        }

        @Override
        public void onNext(ScoresResponse scoresResponse) {
            if(scoresResponse.status.equalsIgnoreCase(Constants.STATUS_SUCCESS)){
                mBaseView.onScoresLoaded(scoresResponse);
            }else {
                mBaseView.showError(scoresResponse.msg);
            }
        }
    }



}
