package com.treebo.prowlapp.flowscores.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.ScoresData;
import com.treebo.prowlapp.Utils.Utils;
import com.treebo.prowlapp.views.CircularProgressView;
import com.treebo.prowlapp.views.PairTextLayoutPercentage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.viewpager.widget.PagerAdapter;

/**
 * Created
 * by devesh on 05/05/16.
 */
@Deprecated
public class FOTScoresViewPagerAdapter extends PagerAdapter {
    public static String CURRENT ="Last";

    public static String SEVEN_DAYS = "7 Days";
    public static String THIRTY_DAYS = "30 Days";
    public Double overall;
    HashMap<String, List<ScoresData>> scoresDataMap;
    Context mContext;

    public FOTScoresViewPagerAdapter(Context context, HashMap<String, List<ScoresData>> scoresDataMap) {
        this.scoresDataMap = scoresDataMap;
        mContext = context;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.scores_subheading_view, container, false);
        TextView scoreHeadingTextView = (TextView) layout.findViewById(R.id.score_text);
        scoreHeadingTextView.setVisibility(View.VISIBLE);
        CircularProgressView progressView = (CircularProgressView) layout.findViewById(R.id.progress_view_current);
        LinearLayout linearLayoutBottom = (LinearLayout) layout.findViewById(R.id.layout_text);
        switch (position) {
            case 0:
                addTextLayouts(CURRENT, linearLayoutBottom, progressView, scoreHeadingTextView);
                break;
            case 1:
                addTextLayouts(SEVEN_DAYS, linearLayoutBottom, progressView, scoreHeadingTextView);
                break;
            case 2:
                addTextLayouts(THIRTY_DAYS, linearLayoutBottom, progressView, scoreHeadingTextView);
                break;
        }
        container.addView(layout);
        return layout;
    }

    private void addTextLayouts(String value, LinearLayout linearLayout, CircularProgressView progressView, TextView scoreText) {
        linearLayout.removeAllViews();
        List<ScoresData> scoresDataList = scoresDataMap.get(value);

        List<ScoresData> scoresDataListToBeAdded = new ArrayList<>();
        ScoresData fotData = null;
        for (ScoresData scoresData : scoresDataList) {
            if (scoresData.mName.contains("FOT Audit")) {
                fotData = scoresData;
            } else {
                scoresDataListToBeAdded.add(scoresData);
            }
        }
        if (fotData != null) {
            scoreText.setText("" + (int) fotData.mValue);
            scoreText.setTextColor(mContext.getResources().getColor(Utils.getFOtColor((int) fotData.mValue)));
            scoreText.setText((int) fotData.mValue + "%");
            //progressView.setScorePercentage((int) fotData.mValue);
            progressView.setProgress((float) fotData.mValue);
            progressView.setColor(mContext.getResources().getColor(Utils.getFOtColor((int)fotData.mValue)));
        }

        for (ScoresData scoresData : scoresDataListToBeAdded) {
            PairTextLayoutPercentage pairTextLayout = new PairTextLayoutPercentage(mContext);
            pairTextLayout.setScoreData(scoresData);
            linearLayout.addView(pairTextLayout);
        }
        // progressView.setSubheading(value);

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return CURRENT + " Audit";
            case 1:
                return SEVEN_DAYS;
            case 2:
                return THIRTY_DAYS;
        }
        return "";
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
