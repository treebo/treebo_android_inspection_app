package com.treebo.prowlapp.flowscores.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.ScoresData;
import com.treebo.prowlapp.views.CircularProgressView;
import com.treebo.prowlapp.views.PairTextLayout;

import java.util.HashMap;
import java.util.List;

import androidx.viewpager.widget.PagerAdapter;

/**
 * Created by
 * devesh on 01/05/16.
 */
@Deprecated
public class HotelScoresViewPagerAdapter extends PagerAdapter {

    HashMap<String, List<ScoresData>> mSubHeadingScoresModels;
    public static String CURRENT ="Last";
    public static String SEVEN_DAYS ="7 Days";
    public static String THIRTY_DAYS ="30 Days";

    Context mContext;

    public HotelScoresViewPagerAdapter(Context context , HashMap<String, List<ScoresData>> subHeadingScoresModels){
        mSubHeadingScoresModels = subHeadingScoresModels;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mSubHeadingScoresModels.keySet().size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.scores_subheading_view, container , false);
        CircularProgressView progressView =(CircularProgressView) layout.findViewById(R.id.progress_view_current);
        LinearLayout linearLayoutBottom = (LinearLayout) layout.findViewById(R.id.layout_text);
        switch(position){
            case 0 : addTextLayouts(CURRENT,linearLayoutBottom, progressView);
                break;
            case 1 : addTextLayouts(SEVEN_DAYS,  linearLayoutBottom, progressView);
                break;
            case 2 : addTextLayouts(THIRTY_DAYS, linearLayoutBottom, progressView);
                break;
        }
        container.addView(layout);
        return layout;
    }

    private void addTextLayouts(String value, LinearLayout linearLayout, CircularProgressView progressView) {
        linearLayout.removeAllViews();
        List<ScoresData> scoresDataList = mSubHeadingScoresModels.get(value);
        for(ScoresData scoresData : scoresDataList){
            PairTextLayout pairTextLayout = new PairTextLayout(mContext);
            pairTextLayout.setScoreData(scoresData);
            linearLayout.addView(pairTextLayout);
        }
        progressView.setSubheading(value);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch(position){
            case 0: return CURRENT;
            case 1: return SEVEN_DAYS;
            case 2: return THIRTY_DAYS;
        }
        return "";
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
