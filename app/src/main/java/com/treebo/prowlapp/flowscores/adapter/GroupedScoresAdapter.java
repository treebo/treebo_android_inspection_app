package com.treebo.prowlapp.flowscores.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.GroupedScoresModel;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;

/**
 * Created by
 * devesh on 01/05/16.
 */
@Deprecated
public class GroupedScoresAdapter extends RecyclerView.Adapter<GroupedScoresAdapter.GroupedScoresViewHolder> {
    private ArrayList<GroupedScoresModel> scoresModelArrayList;
    private ItemClickedListener mItemClickedListener;
    int mGuestReviewsPosition;
    Context mContext;

    public GroupedScoresAdapter(ArrayList<GroupedScoresModel> groupedScoresModels, int guestReviewsPosition) {
        scoresModelArrayList = groupedScoresModels;
        mGuestReviewsPosition = guestReviewsPosition;
    }

    public void setOnItemClickedListener(ItemClickedListener listener) {
         mItemClickedListener = listener;
    }

    @Override
    public GroupedScoresViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewGroup rootView;
        mContext = parent.getContext();
        switch (viewType) {
            case 0:
                rootView = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.scores_row, parent, false);
                return new GroupedScoresViewHolder(rootView);
            case 1:
                rootView = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.scores_row_with_star, parent, false);
                return new StarredGroupedScoresViewHolder(rootView);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(GroupedScoresViewHolder holder, final int position) {
        holder.categoryNameTextView.setText(scoresModelArrayList.get(position).heading);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickedListener.onItemClicked(position);
            }
        });

        if(holder instanceof  StarredGroupedScoresViewHolder){
            Double value1 = scoresModelArrayList.get(position).scores.overall;
            int colorScheme = Utils.getBackgroundColorForGuestReviews(value1);
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));

            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                ((StarredGroupedScoresViewHolder)holder).starredImageView.setVisibility(View.GONE);
                return;
            }else{
                ((StarredGroupedScoresViewHolder)holder).starredImageView.setVisibility(View.VISIBLE);

            }
            holder.categoryValueTextView.setText(value1 + "");
            return;
        }
        else if (scoresModelArrayList.get(position).heading.trim().equalsIgnoreCase("TQL Rank")) {
            Double value1 = scoresModelArrayList.get(position).scores.overall;
            int colorScheme = Utils.getBackgroundColorForTQLRank(value1);
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));
            // fallback
            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                return;
            }else if(value1.intValue()>0 && value1.intValue() < 10){
                holder.categoryValueTextView.setText("0"+value1.intValue());
                return;
            }else{
                holder.categoryValueTextView.setText(""+value1.intValue());
                return;
            }
        }
        else if (scoresModelArrayList.get(position).heading.trim().equalsIgnoreCase("QAM Audit")) {
            Double value1 = scoresModelArrayList.get(position).scores.overall;
            int colorScheme = Utils.getBackgroundColorForQAMAudit(value1);
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));
            if(value1.intValue() == 0){
                    holder.categoryValueTextView.setText("-");
                return;
            }
            if (Utils.checkIfFloatHasDecimal(scoresModelArrayList.get(position).scores.overall)) {
                holder.categoryValueTextView.setText(value1 + "%");
            } else {
                holder.categoryValueTextView.setText(value1.intValue() + "%");
            }
            return;
        }
        else if (scoresModelArrayList.get(position).heading.trim().equalsIgnoreCase("FOT Audit")) {
            Double value1 = scoresModelArrayList.get(position).scores.overall;
            int colorScheme = Utils.getBackgroundColorForFOTAudit(value1.intValue());
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));
            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                return;
            }
            if (Utils.checkIfFloatHasDecimal(scoresModelArrayList.get(position).scores.overall)) {
                holder.categoryValueTextView.setText(value1 + "%");
            } else {
                holder.categoryValueTextView.setText(value1.intValue() + "%");
            }
            return;

        }
        else if (scoresModelArrayList.get(position).heading.contains("Net Promoter Score")) {
            Double value1 = scoresModelArrayList.get(position).scores.overall;
            int colorScheme = Utils.getBackgroundColorForGuestFeedback(value1.intValue());
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));

            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                return;
            }
            holder.categoryValueTextView.setText(value1 + "");
            return;

        }

       else if (scoresModelArrayList.get(position).heading.contains("Guest Feedback")) {
            Double value1 = scoresModelArrayList.get(position).scores.overall;
            int colorScheme = Utils.getBackgroundColorForGuestFeedback(value1.intValue());
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));

            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                return;
            }
            holder.categoryValueTextView.setText(value1 + "");
            return;

        } else {
            Double value1 = scoresModelArrayList.get(position).scores.overall;
            int colorScheme = Utils.getBackgroundColorForOthers(value1.intValue());
            GradientDrawable bgShape = (GradientDrawable) holder.coloredContainer.getBackground();
            bgShape.setColor(ContextCompat.getColor(mContext, colorScheme));
            if(value1.intValue() == 0){
                holder.categoryValueTextView.setText("-");
                return;
            }
            holder.categoryValueTextView.setText(value1 + "");
        }
        return;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mGuestReviewsPosition) {
            return 1;
        } else
            return 0;
    }

    @Override
    public int getItemCount() {
        if (scoresModelArrayList == null) {
            return 0;
        }
        return scoresModelArrayList.size();
    }

    public static class StarredGroupedScoresViewHolder extends GroupedScoresViewHolder{
        public ImageView starredImageView;
        StarredGroupedScoresViewHolder(View itemView){
            super(itemView);
            starredImageView = (ImageView) itemView.findViewById(R.id.image_view_star);
        }

    }

    public static class GroupedScoresViewHolder extends RecyclerView.ViewHolder {

        public TextView categoryNameTextView;
        public TextView categoryValueTextView;
        public RelativeLayout coloredContainer;

        public GroupedScoresViewHolder(View itemView) {
            super(itemView);
            categoryNameTextView = (TextView) itemView.findViewById(R.id.category_name);
            categoryValueTextView = (TextView) itemView.findViewById(R.id.category_value);
            coloredContainer = (RelativeLayout) itemView.findViewById(R.id.value_star_container);
        }
    }

    public interface ItemClickedListener {
        void onItemClicked(int position);
    }
}
