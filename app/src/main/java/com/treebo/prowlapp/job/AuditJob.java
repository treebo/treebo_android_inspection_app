package com.treebo.prowlapp.job;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.util.Pair;

import com.evernote.android.job.Job;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.events.AuditIssueSubmitEvent;
import com.treebo.prowlapp.events.RoomSubmittedEvent;
import com.treebo.prowlapp.events.UpdateBlindCountEvent;
import com.treebo.prowlapp.Models.AnswerListWithSectionName;
import com.treebo.prowlapp.Models.Audit;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.Models.Photo_Table;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.HotelRoomResponse;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.AuditUtils;
import com.treebo.prowlapp.Utils.PhotoUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by sumandas on 09/06/2016.
 */
@Deprecated
public class AuditJob extends Job{

    public static String TAG="audit_job";
    public static String IS_ROOM_AUDIT="is_room_audit";
    public static String SECTION_NAME="section_name";
    public static String AUDIT_SUBMIT_TIME="audit_submit_time";

    public String mJobTag;
    public String mAuditKey;
    public String mSectionName;
    public String mHotelId;
    public String mHotelName;
    public String mRoomNo;
    public boolean isRoomAudit;
    public String mAuditTime;
    Form mSavedForm;
    NotificationManager mNotificationManager;

    public AuditJob(String tag){
        mJobTag=tag;
    }

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        if(!isCanceled()) {
            PersistableBundleCompat extras = params.getExtras();
            mAuditKey= mJobTag.substring(TAG.length()+1,mJobTag.length());
            isRoomAudit = extras.getBoolean(IS_ROOM_AUDIT, false);
            Log.d(TAG,"starting audit job ");
            mSectionName=extras.getString(SECTION_NAME, "");
            mHotelId=extras.getString(Utils.HOTEL_ID, "");
            mHotelName=extras.getString(Utils.HOTEL_NAME, "");
            mRoomNo=extras.getString(Utils.ROOM_NAME,"");
            mAuditTime=extras.getString(AUDIT_SUBMIT_TIME,"");

            if(isRoomAudit){
                mSavedForm= AuditUtils.getPausedRoomAreaAudit(mHotelId, mRoomNo);
            }else{
                mSavedForm= AuditUtils.getPausedCommonAreaAudit(mAuditKey);
            }
            if (mSavedForm == null) {
                return Result.FAILURE;
            }


            List<Photo> photoList= SQLite.select().from(Photo.class).where(Photo_Table.mKey.like("%"+mAuditKey+"%")).queryList();
            if(PhotoUtils.isPhotoUploadsPending(photoList)){
                Log.d(TAG,"pending photo uploads ... rescheduling audit job");
                return Result.RESCHEDULE;
            }

            //link photos to form
            PhotoUtils.addPhotoUrlsToForm(photoList,mSavedForm,mAuditKey);
            AuditPreferenceManager.getInstance().removeAndPersistAudit(mAuditKey,mSavedForm);

            AnswerListWithSectionName answerListWithSectionName
                    = AuditUtils.convertFormtoSubmit(mSavedForm, mSectionName);
            Call<SubmitAuditResponse> call=new RestClient().submitAuditOffline(answerListWithSectionName,
                    mHotelId,
                    Integer.toString(LoginSharedPrefManager.getInstance().getFormId()),
                    Integer.toString(LoginSharedPrefManager.getInstance().getUserId()),
                    "QA",
                    mAuditTime);
            try {
                Log.d(TAG,"starting audit call execute ");
                SubmitAuditResponse response=call.execute().body();
                mNotificationManager =
                        (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                int notifyID;
                if(isRoomAudit){
                    notifyID = Integer.parseInt(mHotelId+mRoomNo);
                }else{
                    notifyID = Integer.parseInt(mHotelId);
                }
                String contentText;
                if(response.status.equalsIgnoreCase("success")){
                    Log.d(TAG, "call audit execute success ");
                    AuditPreferenceManager.getInstance().removeAudit(mAuditKey);
                    AuditPreferenceManager.getInstance().removeLastAuditKey(mAuditKey);
                    //AuditPreferenceManager.getInstance().addAuditOrIssueSubmitted();
                    RxBus.getInstance().postEvent(new AuditIssueSubmitEvent("audit"));
                    if(isRoomAudit){
                        contentText=String.format(getContext().getString(R.string.audit_room_successfully_submitted),mRoomNo,mHotelName);
                    }else{
                        contentText=String.format(getContext().getString(R.string.audit_common_successfully_submitted),mHotelName);
                    }

                            NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(getContext())
                                    .setContentTitle("Prowl Audit")
                                    .setContentText(contentText)
                                    .setLargeIcon(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_launcher))
                                    .setSmallIcon(R.drawable.ic_launcher)
                                    .setStyle(new NotificationCompat.BigTextStyle()
                                            .bigText(contentText));
                    mNotificationManager.notify(
                            notifyID,
                            mNotifyBuilder.build());
                    getRoomsAndUpdateUI(Integer.parseInt(mHotelId));
                    SQLite.delete().from(Photo.class).where(Photo_Table.mKey.like("%"+mAuditKey+"%")).async().execute();
                    return Result.SUCCESS;
                }else{
                    Log.e(TAG, "call execute failure ");
                    if(isRoomAudit){
                        contentText=String.format(getContext().getString(R.string.audit_room_failed),mRoomNo,mHotelName);
                    }else{
                        contentText=String.format(getContext().getString(R.string.audit_common_failed),mHotelName);
                    }

                    NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(getContext())
                            .setContentTitle("Prowl Audit")
                            .setContentText(contentText)
                            .setLargeIcon(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_launcher))
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText(contentText));
                    mNotificationManager.notify(
                            notifyID,
                            mNotifyBuilder.build());
                    return Result.FAILURE;
                }
            } catch (IOException e) {
                Log.e(TAG," call execute IOException in submitting audit");
                return Result.RESCHEDULE;
            }
        }
        return  Result.FAILURE;
    }

    public void getRoomsAndUpdateUI(int hotelId){
        Call<HotelRoomResponse> hotelRoomResponseCall =new RestClient().getHotelRoomsOffline(hotelId);
        try {
            HotelRoomResponse response=hotelRoomResponseCall.execute().body();
            Audit auditData = response.data;
            if(response.status.equals("success")){
                Log.d(TAG," call execute fetching rooms success");
                ArrayList<Pair<String, String>> scoreList = new ArrayList<>();
                scoreList.add(new Pair("Blind Rooms", auditData.blind_rooms_count));
                scoreList.add(new Pair("Completion Ratio", auditData.completion_ratio));
                RxBus.getInstance().postEvent(new RoomSubmittedEvent(auditData.types));
                RxBus.getInstance().postEvent(new UpdateBlindCountEvent(scoreList, auditData.types));
            }else{
                Log.d(TAG," call execute fetching rooms failure");
            }

        }catch (IOException e){
            Log.d(TAG," call execute IOException in fetching rooms");
        }
    }

}
