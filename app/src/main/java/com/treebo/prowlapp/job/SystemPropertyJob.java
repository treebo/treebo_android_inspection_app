package com.treebo.prowlapp.job;


import android.util.Log;

import com.evernote.android.job.Job;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.events.AppUpdateEvent;
import com.treebo.prowlapp.events.AppUpdatedEvent;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.common.SystemPropertyResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.io.IOException;

import androidx.annotation.NonNull;
import retrofit2.Call;

/**
 * Created by abhisheknair on 07/02/17.
 */

public class SystemPropertyJob extends Job {

    public static String TAG = "system_property_job";


    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        Call<SystemPropertyResponse> call = new RestClient().getSystemProperties();
        try {
            SystemPropertyResponse response = call.execute().body();
            if (response != null) {
                Log.d("System Properties", "Response received: " + response.getData().getSystemProperty().toString());
                LoginSharedPrefManager prefManager = LoginSharedPrefManager.getInstance();
                prefManager.setLocationAccuracy(response.getData().getSystemProperty().getAccuracy());
                prefManager.setHouseCountToolTipText(response.getData().getSystemProperty().getHouseCountTooltipText());
                if (response.getData().getSystemProperty().getMasterCheckListVersion() != prefManager.getMasterCheckListVersion()) {
                    OfflineJobUtils.fetchMasterList();
                    prefManager.setMasterCheckListVersion(response.getData().getSystemProperty().getMasterCheckListVersion());
                }
                prefManager.setServerAppVersion(response.getData().getSystemProperty().getAppVersion());
                prefManager.setIsIssueCreationEnabled(response.getData().getSystemProperty().getIsIssueCreationEnabled());
                RxBus rxBus = RxBus.getInstance();
                if (response.getData().getSystemProperty().getAppVersion() > BuildConfig.VERSION_CODE) {
                    rxBus.postEvent(new AppUpdateEvent());
                } else {
                    rxBus.postEvent(new AppUpdatedEvent());
                }
                Log.d("System Properties Call", "App Version: " + response.getData().getSystemProperty().getAppVersion());
                return Result.SUCCESS;
            } else {
                return Result.RESCHEDULE;
            }

        } catch (IOException e) {
            return Result.RESCHEDULE;

        }
    }
}
