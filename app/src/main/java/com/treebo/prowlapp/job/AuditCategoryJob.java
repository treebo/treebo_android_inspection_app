package com.treebo.prowlapp.job;


import android.util.Log;

import com.evernote.android.job.Job;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.AuditCategoryResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.io.IOException;

import androidx.annotation.NonNull;
import retrofit2.Call;

/**
 * Created by sumandas on 19/01/2017.
 */

public class AuditCategoryJob extends Job {

    public static String TAG = "audit_category_job";

    @NonNull
    @Override
    protected Result onRunJob(Params params) {

        LoginSharedPrefManager loginSharedPrefManager = LoginSharedPrefManager.getInstance();
        Call<AuditCategoryResponse> call = new RestClient().getAuditList();

        Log.d("Audit Category Response", "Call started");
        try {
            AuditCategoryResponse response = call.execute().body();
            for (AuditCategory category : response.auditCategories) {
                AuditCategory category1 = AuditCategory.getAuditCategoryById(category.mAuditCategoryId);
                if (category1 == null) {
                    category.save();
                } else {
                    category.update();
                }
            }
            loginSharedPrefManager.setAuditListFetched(true);
            return Result.SUCCESS;

        } catch (IOException e) {
            return Result.FAILURE;
        }
    }
}
