package com.treebo.prowlapp.job;


import android.util.Log;

import com.evernote.android.job.Job;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BankListResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.io.IOException;

import androidx.annotation.NonNull;
import retrofit2.Call;

/**
 * Created by abhisheknair on 16/05/17.
 */

public class BankListJob extends Job {
    public static String TAG = "Bank List Job:";

    private LoginSharedPrefManager sharedPrefManager = LoginSharedPrefManager.getInstance();

    @NonNull
    @Override
    protected Job.Result onRunJob(Job.Params params) {
        Call<BankListResponse> call = new RestClient().getMasterBankList();
        try {
            BankListResponse response = call.execute().body();
            if (response != null) {
                Log.d(TAG, "Bank list fetched successfully");
                sharedPrefManager.setBankList(response.getBankList());
                return Job.Result.SUCCESS;
            } else {
                return Job.Result.RESCHEDULE;
            }
        } catch (IOException e) {
            return Job.Result.RESCHEDULE;

        }
    }
}
