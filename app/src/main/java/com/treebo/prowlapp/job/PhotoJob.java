package com.treebo.prowlapp.job;


import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.evernote.android.job.Job;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.Models.Photo_Table;
import com.treebo.prowlapp.Utils.AmazonS3ClientCredentials;
import com.treebo.prowlapp.Utils.Utilities;
import com.treebo.prowlapp.Utils.Utils;

import java.net.URL;
import java.util.Date;

import androidx.annotation.NonNull;

/**
 * Created by sumandas on 19/06/2016.
 */
public class PhotoJob extends Job {

    public static String TAG = "photo_job";

    public String mJobTag;
    public String mPhotoId;
    public String mFilePath;

    public PhotoJob(String tag) {
        mJobTag = tag;
    }


    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        Photo photo;
        PersistableBundleCompat extras = params.getExtras();
        mPhotoId=extras.getString(Utils.PHOTO_ID,"");
        photo = SQLite.select().from(Photo.class).where(Photo_Table.mPhotoId.eq(mPhotoId)).querySingle();
        if(photo==null){
            return Result.FAILURE;
        }
        if (!isCanceled()) {
            mFilePath = photo.getmDeviceFilePath();
            AmazonS3Client s3Client = AmazonS3ClientCredentials.getAmazonImageUploadInstance();
            String randomNumber = Utilities.createRandomNumber();
            Object result;
            try {
                PutObjectRequest por = new PutObjectRequest(BuildConfig.AWS_BUCKET_NAME, randomNumber + ".jpeg", new java.io.File(mFilePath));
                result = s3Client.putObject(por);

                if (result != null) {
                    System.out.println(result);
                    ResponseHeaderOverrides override = new ResponseHeaderOverrides();
                    override.setContentType("image/jpeg");
                    GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(BuildConfig.AWS_BUCKET_NAME, randomNumber + ".jpeg");
                    urlRequest.setExpiration(new Date(System.currentTimeMillis() + 3600000 * 24 * 30 * 12 * 100));  // Added an 100 years worth of milliseconds to the current time.

                    urlRequest.setResponseHeaders(override);
                    URL url = s3Client.generatePresignedUrl(urlRequest);
                    photo.setIsUploadedToCloud(true);
                    photo.setmCloudFilePath(url.toString());
                    photo.update();
                    Log.e(TAG,"photo uploaded successfully amazon url is "+url.toString());
                    return Result.SUCCESS;
                } else {
                    return Result.RESCHEDULE;
                }
            } catch (AmazonClientException e) {
                return Result.RESCHEDULE;
            }
        }
        photo.delete();
        return Result.FAILURE;
    }

}
