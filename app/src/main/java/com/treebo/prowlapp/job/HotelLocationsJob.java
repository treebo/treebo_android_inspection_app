package com.treebo.prowlapp.job;


import com.evernote.android.job.Job;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.common.HotelLocationResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import retrofit2.Call;

/**
 * Created by sumandas on 10/11/2016.
 */

public class HotelLocationsJob  extends Job {

    public static String TAG = "hotel_location_job";

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        Call<HotelLocationResponse> call = new RestClient().getHotelLocations();
        try {
            HotelLocationResponse response = call.execute().body();
            for (HotelLocation hotelLocation : response.data.hotels) {
                hotelLocation.save();
            }
            RxBus rxBus = RxBus.getInstance();
            ArrayList<HotelLocation> hotelList = HotelLocation.getHotelForLocation(LoginSharedPrefManager.getInstance().getLatitude(), LoginSharedPrefManager.getInstance().getLongitude());
            rxBus.postEvent(new HotelChangedEvent(hotelList));
            LoginSharedPrefManager.getInstance().setPrefIsPortfolioListDownloaded(true);
            LoginSharedPrefManager.getInstance().setHotelLocationsFetched(true);
            return Result.SUCCESS;

        } catch (IOException e) {
            return Result.RESCHEDULE;

        }
    }
}

