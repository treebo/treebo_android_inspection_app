package com.treebo.prowlapp.job;


import android.util.Log;

import com.evernote.android.job.Job;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.io.IOException;

import androidx.annotation.NonNull;
import retrofit2.Call;

/**
 * Created by abhisheknair on 16/02/17.
 */

public class LocationNotFoundLoggingJob extends Job {

    public static String TAG = "location_error_log_job";

    private LoginSharedPrefManager sharedPrefManager = LoginSharedPrefManager.getInstance();

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        Call<SubmitAuditResponse> call = new RestClient()
                .logLocationErrorDetails(sharedPrefManager.getLatitude(), sharedPrefManager.getLongitude(),
                        sharedPrefManager.getUserId(), sharedPrefManager.getHotelLocationNotFoundAccuracy());
        try {
            SubmitAuditResponse response = call.execute().body();
            if (response != null) {
                Log.d("Location Error Log:", "Details logged successfully");
                return Result.SUCCESS;
            } else {
                return Result.RESCHEDULE;
            }
        } catch (IOException e) {
            return Result.RESCHEDULE;

        }
    }
}
