package com.treebo.prowlapp.job;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by sumandas on 09/06/2016.
 */
public class OfflineJobCreator implements JobCreator {
    @Override
    public Job create(String tag) {
        if (tag.startsWith(AuditJob.TAG)) {
            return new AuditJob(tag);
        } else if (tag.startsWith(PhotoJob.TAG)) {
            return new PhotoJob(tag);
        } else if (tag.startsWith(IssueJob.TAG)) {
            return new IssueJob(tag);
        } else if (tag.startsWith(MasterListJob.TAG)) {
            return new MasterListJob();
        } else if (tag.startsWith(AuditCategoryJob.TAG)) {
            return new AuditCategoryJob();
        } else if (tag.startsWith(HotelLocationsJob.TAG)) {
            return new HotelLocationsJob();
        } else if (tag.startsWith(ClearCompleteAuditJob.TAG)) {
            return new ClearCompleteAuditJob();
        } else if (tag.startsWith(SystemPropertyJob.TAG)) {
            return new SystemPropertyJob();
        } else if (tag.startsWith(LocationNotFoundLoggingJob.TAG)) {
            return new LocationNotFoundLoggingJob();
        } else if (tag.startsWith(BankListJob.TAG)) {
            return new BankListJob();
        } else if (tag.startsWith(UserPermissionsJob.TAG)) {
            return new UserPermissionsJob();
        } else
            return null;
    }
}
