package com.treebo.prowlapp.job;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.events.AuditIssueSubmitEvent;
import com.treebo.prowlapp.events.ResolveIssueEvent;
import com.treebo.prowlapp.events.UpdateIssueEvent;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.Models.Photo_Table;
import com.treebo.prowlapp.Models.UpdateIssueModel;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.UpdateIssueResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.Utils.PhotoUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by sumandas on 21/06/2016.
 */
public class IssueJob extends Job {

    public static String TAG="issue_job";

    public String mJobTag;
    String mIssueId;
    String mStatus;
    String mRootCause;
    String mDescription;
    String mImageUrls;
    List<Photo> mPhotoList;
    NotificationManager mNotificationManager;

    public IssueJob(String tag){
        mJobTag=tag;
    }

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        if(!isCanceled()) {
            PersistableBundleCompat extras = params.getExtras();
            mIssueId=extras.getString("issueId", "");
            mStatus=extras.getString("status", "");
            mRootCause=extras.getString("rootcause", "");
            mDescription=extras.getString("description", "");

            mPhotoList= SQLite.select().from(Photo.class).where(Photo_Table.mKey.eq(mIssueId)).queryList();
            if(PhotoUtils.isPhotoUploadsPending(mPhotoList)){
                Log.e(TAG, "pending photo uploads ... rescheduling issue job");
                return Result.RESCHEDULE;
            }
            mImageUrls=PhotoUtils.getCSVForUrlList(PhotoUtils.getCloudUrlsList(mPhotoList));
            Call<UpdateIssueResponse> call=new RestClient().updateIssueOffline(mIssueId, mStatus, mRootCause,
                    mDescription, mImageUrls);
            Log.d(TAG,"starting issue call execute ");
            try {
                UpdateIssueResponse response=call.execute().body();
                mNotificationManager =
                        (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                int notifyID = Integer.parseInt(mIssueId);
                String contentText;
                if(response.status.equalsIgnoreCase("success")) {
                    Log.d(TAG, "call Issue execute success ");
                    //AuditPreferenceManager.getInstance().addAuditOrIssueSubmitted();

                    if (mStatus.equals("Update")) {
                        contentText=getContext().getString(R.string.issue_successfully_updated);
                        UpdateIssueEvent event = new UpdateIssueEvent(getUpdatedIssueModel());
                        RxBus.getInstance().postEvent(event);
                    } else {
                        contentText=getContext().getString(R.string.issue_successfully_closed);
                        ResolveIssueEvent rEvent = new ResolveIssueEvent(getUpdatedIssueModel());
                        RxBus.getInstance().postEvent(rEvent);
                        RxBus.getInstance().postEvent(new AuditIssueSubmitEvent("issue"));
                    }

                    NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(getContext())
                            .setContentTitle("Prowl Audit")
                            .setContentText(contentText)
                            .setLargeIcon(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_launcher))
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText(contentText));
                    mNotificationManager.notify(
                            notifyID,
                            mNotifyBuilder.build());
                    SQLite.delete().from(Photo.class).where(Photo_Table.mKey.eq(mIssueId)).async().execute();
                    return Result.SUCCESS;
                }else{
                    return Result.FAILURE;
                }

            } catch (IOException e) {
                Log.e(TAG, " call execute IOException in submitting issue");
                return Result.RESCHEDULE;
            }

        }
        return Result.FAILURE;
    }


    public UpdateIssueModel getUpdatedIssueModel(){
        UpdateIssueModel updateModel = new UpdateIssueModel();
        updateModel.mIssueId = Integer.parseInt(mIssueId);
        updateModel.mRootCause = mRootCause;
        updateModel.mComment = mDescription;
        updateModel.mImageUrls = (ArrayList)PhotoUtils.getCloudUrlsList(mPhotoList);
        return updateModel;
    }
}
