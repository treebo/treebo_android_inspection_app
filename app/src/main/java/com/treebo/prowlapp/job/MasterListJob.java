package com.treebo.prowlapp.job;


import android.util.Log;

import com.evernote.android.job.Job;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.common.MasterChecklistResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.DateUtils;

import java.io.IOException;

import androidx.annotation.NonNull;
import retrofit2.Call;

/**
 * Created by sumandas on 09/11/2016.
 */

public class MasterListJob extends Job {


    public static String TAG = "master_job";

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        LoginSharedPrefManager loginSharedPrefManager = LoginSharedPrefManager.getInstance();
        Call<MasterChecklistResponse> call = new RestClient().getMasterList();
        Log.d("MasterCheckpoint", "Call started");
        try {
            MasterChecklistResponse response = call.execute().body();
            loginSharedPrefManager.setLastMasterListFetchTime(DateUtils.getDateTimeNow());
            for (Category category : response.data.master_list) {
                for (Checkpoint checkpoint : category.getmCheckPoints()) {
                    for (SubCheckpoint subCheckpoint : checkpoint.getmSubCheckpoints()) {
                        subCheckpoint.mCheckpointId = checkpoint.mCheckpointId;
                    }
                    checkpoint.mCategoryId = category.mCategoryId;
                }
                Category category1 = Category.getCategoryById(category.mCategoryId);
                if (category1 == null) {
                    category.save();
                } else {
                    category.update();
                }


            }
            loginSharedPrefManager.setMasterListFetched(true);
            return Result.SUCCESS;

        } catch (IOException e) {
            return Result.FAILURE;
        }
    }
}
