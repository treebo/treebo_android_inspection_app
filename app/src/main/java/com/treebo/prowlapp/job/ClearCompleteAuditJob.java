package com.treebo.prowlapp.job;


import android.util.Log;

import com.evernote.android.job.Job;
import com.treebo.prowlapp.Models.auditmodel.CompleteAudit;

import androidx.annotation.NonNull;

/**
 * Created by sumandas on 15/11/2016.
 */

public class ClearCompleteAuditJob extends Job {

    public static String TAG = "delete_complete_audit_job";

    @NonNull
    @Override
    protected Job.Result onRunJob(Job.Params params) {
        Log.d("Complete Audit", "delete started");
        CompleteAudit.deletePreviousCompleteAudits();
        return Result.SUCCESS;
    }
}
