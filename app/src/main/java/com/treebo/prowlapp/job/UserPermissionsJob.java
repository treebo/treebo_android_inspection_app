package com.treebo.prowlapp.job;

import com.evernote.android.job.Job;
import com.treebo.prowlapp.events.PermissionsUpdateEvent;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.common.UserPermissionsResponse;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.io.IOException;

import androidx.annotation.NonNull;
import retrofit2.Call;

/**
 * Created by abhisheknair on 25/05/17.
 */

public class UserPermissionsJob extends Job {

    public static String TAG = "user_permissions_job";

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        LoginSharedPrefManager sharedPrefManager = LoginSharedPrefManager.getInstance();
        Call<UserPermissionsResponse> call = new RestClient()
                .getUserPermissions(sharedPrefManager.getUserId());
        try {
            UserPermissionsResponse response = call.execute().body();
            sharedPrefManager.setUserPermissions(response.getRolesAndUserPermissions());
            RxBus.getInstance().postEvent(new PermissionsUpdateEvent());
            return Result.SUCCESS;

        } catch (IOException e) {
            return Result.RESCHEDULE;
        }
    }
}
