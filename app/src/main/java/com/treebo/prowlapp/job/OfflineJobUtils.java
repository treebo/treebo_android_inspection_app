package com.treebo.prowlapp.job;

import android.util.Log;

import com.evernote.android.job.JobRequest;

import java.util.concurrent.TimeUnit;

/**
 * Created by sumandas on 12/11/2016.
 */

public class OfflineJobUtils {

    public static void fetchMasterList() {
        Log.d("MasterCheckpoint", "Request started");
        new JobRequest.Builder(MasterListJob.TAG)
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(TimeUnit.MINUTES.toMillis(2), JobRequest.BackoffPolicy.LINEAR)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public static void fetchAuditList() {
        Log.d("AuditList", "Request started");
        new JobRequest.Builder(AuditCategoryJob.TAG)
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(TimeUnit.MINUTES.toMillis(2), JobRequest.BackoffPolicy.LINEAR)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public static void fetchHotelLocations() {
        Log.d("HotelLatLong", "Request started");
        new JobRequest.Builder(HotelLocationsJob.TAG)
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(TimeUnit.MINUTES.toMillis(2), JobRequest.BackoffPolicy.LINEAR)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public static void clearCompleteAuditsPeriodic(){
        new JobRequest.Builder(ClearCompleteAuditJob.TAG)
                .setPeriodic(TimeUnit.DAYS.toMillis(1))
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public static void fetchSystemProperties() {
        Log.d("System Properties", "Request started");
        new JobRequest.Builder(SystemPropertyJob.TAG)
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(TimeUnit.MINUTES.toMillis(2), JobRequest.BackoffPolicy.LINEAR)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public static void logLocationNotFoundError() {
        Log.d("Location Error Log:", "Request started");
        new JobRequest.Builder(LocationNotFoundLoggingJob.TAG)
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(TimeUnit.MINUTES.toMillis(2), JobRequest.BackoffPolicy.LINEAR)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public static void fetchBankList() {
        Log.d("Bank List Job:", "Request started");
        new JobRequest.Builder(BankListJob.TAG)
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(TimeUnit.MINUTES.toMillis(2), JobRequest.BackoffPolicy.LINEAR)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

    public static void fetchUserPermissions() {
        Log.d("User Permissions Job:", "Reload Request started");
        new JobRequest.Builder(UserPermissionsJob.TAG)
                .setExecutionWindow(3_00L, 5_00L)
                .setBackoffCriteria(TimeUnit.MINUTES.toMillis(2), JobRequest.BackoffPolicy.LINEAR)
                .setRequiresCharging(false)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

}

