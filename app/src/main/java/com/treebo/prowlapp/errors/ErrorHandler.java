package com.treebo.prowlapp.errors;

import android.text.TextUtils;
import android.util.Log;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.exception.RefreshTokenFailureException;
import com.treebo.prowlapp.exception.ServerException;
import com.treebo.prowlapp.exception.UnauthorizedAccessException;
import com.treebo.prowlapp.exception.UnknownException;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.Utils.Constants;

import rx.Observable;

/**
 * Created by devesh on 26/04/16.
 */
public class ErrorHandler {

    public static Observable filterServerResponseErrors(BaseResponse baseResponse) {

        String status = baseResponse.status;
        String message = baseResponse.msg;
        int code = baseResponse.code;
        
        if (TextUtils.equals(status.toLowerCase(), Constants.STATUS_SUCCESS.toLowerCase())) {
            return Observable.just(baseResponse);
        } else if (code == NavigationalErrorCode.UNAUTHORIZED.code) {
            Log.e("ErrorHandler","UnauthorizedAccessException");
            return Observable.error(new UnauthorizedAccessException(!TextUtils.isEmpty(message) ? message : "Authorization Failed"));
        }
        else if (code == NavigationalErrorCode.UNAUTHORIZED_ACCESS.code) {
            Log.e("ErrorHandler","UnauthorizedAccessException");
            return Observable.error(new UnauthorizedAccessException(!TextUtils.isEmpty(message) ? message : "Authorization Failed"));
        }
        else if (code == NavigationalErrorCode.UNAUTHORIZED_ACCESS_SERVER.code) {
            Log.e("ErrorHandler","UnauthorizedAccessException");
            return Observable.error(new UnauthorizedAccessException(!TextUtils.isEmpty(message) ? message : "Authorization Failed"));
        }
        else if (code == Constants.REFRESH_TOKEN_FAILED) {
            Log.e("ErrorHandler","RefreshTokenFailureException");
            return Observable.error(new RefreshTokenFailureException(!TextUtils.isEmpty(message) ? message : "Refresh Token Failed"));
        } else if (!TextUtils.equals(status.toLowerCase(), Constants.STATUS_SUCCESS.toLowerCase())) {

            NavigationalErrorCode errorCode = new NavigationalErrorCode(baseResponse.code, baseResponse.msg , -1);
            //107
                return Observable.error(new ServerException(baseResponse.msg));
        }

        if (code < 1000) {
            message = MainApplication.getAppContext().getString(R.string.generic_error);
        }

        // Unidentified error
        return Observable.error(new UnknownException(!TextUtils.isEmpty(message) ? message : "Unknown error"));
    }
}
