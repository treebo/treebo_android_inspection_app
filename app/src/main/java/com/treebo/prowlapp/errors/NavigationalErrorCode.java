package com.treebo.prowlapp.errors;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.MainApplication;

/**
 * Created by devesh on 26/04/16.
 */
public class NavigationalErrorCode {

    public static final NavigationalErrorCode GENERIC_ERROR = new NavigationalErrorCode(-1 , MainApplication.getAppContext().getString(R.string.generic_error), ErrorConstants.UNKNOWN_NAVIGATION_DO_NOTHING);
    public static final NavigationalErrorCode FORCED_LOGOUT = new NavigationalErrorCode(1 , MainApplication.getAppContext().getString(R.string.forced_logout), ErrorConstants.FORCE_LOGOUT_THE_USER);
    public static final NavigationalErrorCode UNAUTHORIZED = new NavigationalErrorCode(2 , MainApplication.getAppContext().getString(R.string.forced_logout), ErrorConstants.FORCE_LOGOUT_THE_USER);
    public static final NavigationalErrorCode USER_ALREADY_REGISTERED = new NavigationalErrorCode(107 , MainApplication.getAppContext().getString(R.string.user_aready_registered), ErrorConstants.FORCE_LOGOUT_THE_USER);
    public static final NavigationalErrorCode UNAUTHORIZED_ACCESS = new NavigationalErrorCode(137 , MainApplication.getAppContext().getString(R.string.unauthorized_access), ErrorConstants.FORCE_LOGOUT_THE_USER);
    public static final NavigationalErrorCode UNAUTHORIZED_ACCESS_SERVER = new NavigationalErrorCode(401 , MainApplication.getAppContext().getString(R.string.unauthorized_access), ErrorConstants.FORCE_LOGOUT_THE_USER);

    public final int code;
    public final String message;
    public final int step;

    public NavigationalErrorCode(int code, String message, int step) {
        this.code = code;
        this.message = message;
        this.step = step;
    }

    public static class ErrorConstants {
        public static final int UNKNOWN_NAVIGATION_DO_NOTHING = 0;
        public static final int CURRENT_SCREEN_REFRESH = 1;
        public static final int FORCE_NAVIGATE_TO_LOGIN = 2;
        public static final int AUTO_RETRY = 8;
        public static final int FORCE_LOGOUT_THE_USER = 9;
    }


    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public int getStep() {
        return step;
    }



    @Override
    public String toString() {
        return "NavigationalErrorCode{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", step=" + step +
                '}';
    }


}
