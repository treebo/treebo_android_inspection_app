package com.treebo.prowlapp.errors;

/**
 * Created by sumandas on 19/10/2016.
 */

public class BaseErrorResponse {

    private String msg;
    private String code;

    public String getMsg() {
        return msg;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "msg: " + msg + ", code: " + code;
    }
}
