package com.treebo.prowlapp.presenter;

import com.treebo.prowlapp.mvpviews.BaseView;

/**
 * Created by devesh on 19/04/16.
 */
public interface BasePresenter {

    void setMvpView(BaseView baseView);

    void updateViewWithPermissions();

    void pause();

    void resume();

    void destroy();

    BaseView getView();
}
