package com.treebo.prowlapp.presenter;

/**
 * Created by sumandas on 20/05/2016.
 */
public interface AppUpdatePresenter extends BasePresenter{
     void getAppUpdate();
}
