package com.treebo.prowlapp.usecase;

import com.treebo.prowlapp.data.IndividualEntry;
import com.treebo.prowlapp.net.RestClient;

import rx.Observable;

/**
 * Created by sumandas on 03/07/2016.
 */
public class StaffIncentivePaidUseCase extends UseCase {
    private IndividualEntry mIndividualEntry;

    public StaffIncentivePaidUseCase(IndividualEntry entry){
        mIndividualEntry=entry;
    }

    @Override
    protected Observable getObservable() {
        return new RestClient().markIncentivesPaid(mIndividualEntry.staff_id);
    }
}
