package com.treebo.prowlapp.usecase;



import com.treebo.prowlapp.net.RestClient;

import rx.Observable;

/**
 * Created by sumandas on 20/05/2016.
 */
public class AppUpdateUseCase extends UseCase {

    @Override
    public Observable getObservable() {
        return new RestClient().getUpdateDetails(2);
    }
}
