package com.treebo.prowlapp.usecase;

import android.util.Log;

import com.treebo.prowlapp.net.RestClient;

import java.util.ArrayList;
import java.util.Date;

import rx.Observable;

/**
 * Created by devesh on 03/05/16.
 */
public class UpdateStaffUseCase extends UseCase {
    int staff_id;
    String first_name;
    String last_name;
    String phone;
    String fathers_name;
    String staff_url;
    String address_line_1;
    String address_line_2;
    String state;
    String city;
    String pin_code;
    int department;
    int hotel;
    int salary;
    Date date_of_joining;
    Date date_of_leaving;
    Date leave_start_date;
    Date leave_end_date;
    Date date_of_birth;
    ArrayList<String> languagesRead;
    ArrayList<String> languagesSpeak;

    public UpdateStaffUseCase(int staff_id, String first_name, String last_name, String phone, String fathers_name, String staff_url,
                              String address_line_1, String address_line_2, String state, String city, String pin_code,
                              int department, int hotel, int salary,
                              Date date_of_joining, Date date_of_leaving, Date leave_start_date, Date leave_end_date, Date date_of_birth,
                              ArrayList<String> languagesRead, ArrayList<String> languagesSpeak) {

        this.staff_id = staff_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone = phone;
        this.fathers_name = fathers_name;
        this.staff_url = staff_url;
        this.address_line_1 = address_line_1;
        this.address_line_2 = address_line_2;
        this.state = state;
        this.city = city;
        this.pin_code = pin_code;
        this.department = department;
        this.hotel = hotel;
        this.salary = salary;
        this.date_of_joining = date_of_joining;
        this.date_of_leaving = date_of_leaving;
        this.leave_start_date = leave_start_date;
        this.leave_end_date = leave_end_date;
        this.date_of_birth = date_of_birth;
        this.languagesRead = languagesRead;
        this.languagesSpeak = languagesSpeak;
    }


    @Override
    protected Observable getObservable() {

        Log.d("RestClientUpdateStaff", this.toString());

        return new RestClient().updateStaffDetails(staff_id, first_name, last_name, phone, fathers_name, staff_url,
                address_line_1, address_line_2, state, city, pin_code,
                department, hotel, salary,
                date_of_joining, date_of_leaving, leave_start_date, leave_end_date, date_of_birth,
                languagesRead, languagesSpeak);
    }

    @Override
    public String toString() {
        return "UpdateStaffUseCase{" +
                "staff_id=" + staff_id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", phone='" + phone + '\'' +
                ", fathers_name='" + fathers_name + '\'' +
                ", staff_url='" + staff_url + '\'' +
                ", address_line_1='" + address_line_1 + '\'' +
                ", address_line_2='" + address_line_2 + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", pin_code='" + pin_code + '\'' +
                ", department=" + department +
                ", hotel=" + hotel +
                ", salary=" + salary +
                ", date_of_joining=" + date_of_joining +
                ", date_of_leaving=" + date_of_leaving +
                ", leave_start_date=" + leave_start_date +
                ", leave_end_date=" + leave_end_date +
                ", date_of_birth=" + date_of_birth +
                ", languagesRead=" + languagesRead +
                ", languagesSpeak=" + languagesSpeak +
                '}';
    }
}
