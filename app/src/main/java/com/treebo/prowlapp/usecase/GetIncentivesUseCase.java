package com.treebo.prowlapp.usecase;

import com.treebo.prowlapp.net.RestClient;

import rx.Observable;

/**
 * Created by devesh on 28/04/16.
 */
public class GetIncentivesUseCase extends UseCase {
    public GetIncentivesUseCase(int hotelId) {
        this.hotelId = hotelId;
    }

    public int hotelId;

    @Override
    protected Observable getObservable() {
        return new RestClient().getAllIncentives(hotelId);
    }
}
