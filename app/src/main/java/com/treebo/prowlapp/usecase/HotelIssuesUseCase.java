package com.treebo.prowlapp.usecase;

import com.treebo.prowlapp.net.RestClient;

import rx.Observable;

/**
 * Created by sumandas on 30/04/2016.
 */
public class HotelIssuesUseCase extends UseCase {

    public HotelIssuesUseCase(int hotelId) {
        this.hotelId = hotelId;
    }

    public int hotelId;

    @Override
    protected Observable getObservable() {
        return new RestClient().getIssuesList(hotelId);
    }
}
