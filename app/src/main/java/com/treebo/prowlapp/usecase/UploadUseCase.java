package com.treebo.prowlapp.usecase;

import com.treebo.prowlapp.net.RestClient;

import rx.Observable;

/**
 * Created by sumandas on 01/05/2016.
 */
public class UploadUseCase extends UseCase {

    String issueId;
    String status;
    String rootCause;
    String description;
    String imageUrls;

    public UploadUseCase(String issueId,String status,String rootCause,String description,String imageUrls){
        this.issueId=issueId;
        this.status=status;
        this.rootCause=rootCause;
        this.description=description;
        this.imageUrls=imageUrls;

    }

    @Override
    protected Observable getObservable() {
        return new RestClient().updateIssue(issueId,status,rootCause, description,imageUrls);
    }
}
