package com.treebo.prowlapp.usecase;

import com.treebo.prowlapp.net.RestClient;

import rx.Observable;

/**
 * Created by devesh on 02/05/16.
 */
public class GetDepartmentsUseCase extends UseCase {
    int hotelId;

    public GetDepartmentsUseCase(int hotelId){
        this.hotelId = hotelId;
    }

    @Override
    protected Observable getObservable() {
        return new RestClient().getAllDepartments(hotelId);
    }
}
