package com.treebo.prowlapp.usecase;

import com.treebo.prowlapp.net.RestClient;

import rx.Observable;

/**
 * Created by devesh on 30/04/16.
 */
public class GetAllScoresUseCase extends UseCase {

    private int hotelId;

    public GetAllScoresUseCase(int hotel_id){
        hotelId = hotel_id;
    }

    @Override
    protected Observable getObservable() {
        return new RestClient().getAllScores(hotelId);
    }

}
