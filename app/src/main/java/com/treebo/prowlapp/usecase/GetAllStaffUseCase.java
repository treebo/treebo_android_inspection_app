package com.treebo.prowlapp.usecase;

import com.treebo.prowlapp.net.RestClient;

import rx.Observable;

/**
 * Created by devesh on 27/04/16.
 */
public class GetAllStaffUseCase extends UseCase {

    private int hotelId;


    public GetAllStaffUseCase(int hotelId) {
        this.hotelId = hotelId;
    }


    @Override
    protected Observable getObservable() {
        return new RestClient().getAllStaffUsers(hotelId);
    }
}
