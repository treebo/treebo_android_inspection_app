package com.treebo.prowlapp.usecase;



import com.treebo.prowlapp.apis.CommonApi;
import com.treebo.prowlapp.Utils.Constants;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by devesh on 01/02/16.
 */
public abstract class UseCase<M> {

    private Subscription subscription;

    public Subscriber<M> getSubscriber() {
        return mSubscriber;
    }

    private  Subscriber<M> mSubscriber;

    protected abstract Observable<M> getObservable();
    //added cache observable temporarily
    protected Observable<M> buildObservable(){
        return getObservable().subscribeOn(Schedulers.io())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .cache()
                .retry(Constants.NUM_RETRIES)
                .onErrorResumeNext(CommonApi.refreshTokenAndRetry(getObservable()))
                //.materialize()
                .observeOn(AndroidSchedulers.mainThread());
                //.<M>dematerialize();
    }

    public void execute(Subscriber<M> subscriber){
        mSubscriber = subscriber;
        subscription = buildObservable().subscribe(subscriber);
    }

    public void unsubscribe(){
        if(subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }
    }

}
