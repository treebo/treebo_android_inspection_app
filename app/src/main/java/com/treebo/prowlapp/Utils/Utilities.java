package com.treebo.prowlapp.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.treebo.prowlapp.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by kuliza123 on 4/8/15.
 */
@Deprecated
public class Utilities {
    private static ProgressDialog mProgressDialog = null;

    // show progressing dialog
    public static void displayProgressDialog(Context context, String message, Boolean backButtonCancelable) {
        if (mProgressDialog == null && context != null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage(message);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setIndeterminateDrawable(ContextCompat.getDrawable(context, R.drawable.circular_progress_green));
            mProgressDialog.show();
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(backButtonCancelable);
        }
    }

    // hide progressing dialog
    public static void cancelProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog.cancel();
            mProgressDialog = null;
        }
    }

    public static void showKeyboard(Context ctx, EditText editText) {
        if (ctx != null && ctx.getSystemService(Activity.INPUT_METHOD_SERVICE) != null) {
            try {
                InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY);
            } catch (Exception e) {

            }

        }
    }

    public static String giveCurrentTime() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("hh:mm a");
        date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        String localTime = date.format(currentLocalTime);

        return localTime;
    }

    public static String createRandomNumber() {
        Random rand = new Random();
        int randomNum = rand.nextInt((Constants.RandomNumberMaximumLimit - Constants.RandomNumberMinimumLimit) + 1) + Constants.RandomNumberMinimumLimit;
        return String.valueOf(randomNum);
    }


}
