package com.treebo.prowlapp.Utils;

import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.Models.auditmodel.RoomV3Object;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint;
import com.treebo.prowlapp.Models.dashboardmodel.SectionedTaskListModel;
import com.treebo.prowlapp.Models.dashboardmodel.TaskModel;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.response.dashboard.ContactInfoResponse;
import com.treebo.prowlapp.response.staffincentives.DepartmentObject;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhisheknair on 10/11/16.
 */

public class DummyDataUtils {

    public static ArrayList<RoomV3Object> getDummyRooms() {
        ArrayList<RoomV3Object> roomList = new ArrayList<>();
        RoomV3Object model1 = new RoomV3Object();
        model1.setId(1);
        model1.setRoomNumber("104");
        model1.setRoomType("Mahagony");
        model1.setDescription("Not audited since 20 days");
        model1.setAvailable(true);
        model1.setReady(false);

        RoomV3Object model2 = new RoomV3Object();
        model2.setId(2);
        model2.setRoomNumber("201");
        model2.setRoomType("Oak");
        model2.setAvailable(true);
        model2.setDescription("Not audited since 10 days");
        model2.setReady(false);

        RoomV3Object model3 = new RoomV3Object();
        model3.setId(3);
        model3.setRoomNumber("101");
        model3.setRoomType("Maple");
        model3.setDescription("2 open issues");
        model3.setReady(true);

        RoomV3Object model4 = new RoomV3Object();
        model4.setId(4);
        model4.setRoomNumber("100");
        model4.setRoomType("Maple");
        model4.setDescription("4 open issues");
        model4.setReady(true);

        RoomV3Object model5 = new RoomV3Object();
        model5.setId(5);
        model5.setRoomNumber("105");
        model5.setRoomType("Maple");
        model5.setDescription("1 open issue");
        model5.setReady(true);

        roomList.add(model1);
        roomList.add(model2);
        roomList.add(model3);
        roomList.add(model4);
        roomList.add(model5);
        return roomList;
    }


    public static ArrayList<Category> getDummyCategories() {
        ArrayList<Category> checklist = new ArrayList<>();

        Category object1 = new Category();
        object1.mCategoryId = 0;
        object1.mDescription = "Bed and Linen";
        object1.setCheckPoints(new ArrayList<>(getDummyCheckpoints()));

        Category object2 = new Category();
        object2.mCategoryId = 1;
        object2.mDescription = "Beverage Tray";
        object2.setCheckPoints(new ArrayList<>(getDummyCheckpoints()));
        object2.setIssueRaised(true);

        Category object3 = new Category();
        object3.mCategoryId = 2;
        object3.mDescription = "TV and Applicances";
        object3.setCheckPoints(new ArrayList<>(getDummyCheckpoints()));

        Category object4 = new Category();
        object4.mCategoryId = 3;
        object4.mDescription = "Basin Area";
        object4.setCheckPoints(new ArrayList<>(getDummyCheckpoints()));

        Category object5 = new Category();
        object5.mCategoryId = 4;
        object5.mDescription = "Shower Area";
        object5.setCheckPoints(new ArrayList<>(getDummyCheckpoints()));
        object5.setIssueRaised(true);
        object5.setAuditDone(true);

        checklist.add(object1);
        checklist.add(object2);
        checklist.add(object3);
        checklist.add(object4);
        checklist.add(object5);
        return checklist;
    }

    public static ArrayList<SubCheckpoint> getDummySubCheckpoints() {
        ArrayList<SubCheckpoint> subCheckpointArrayList = new ArrayList<>();
        SubCheckpoint subCheckpoint = new SubCheckpoint();
        subCheckpoint.mSubCheckpointId = 1;
        subCheckpoint.mDescription = "Bedsheet";
        SubCheckpoint subCheckpoint2 = new SubCheckpoint();
        subCheckpoint2.mSubCheckpointId = 1;
        subCheckpoint2.mDescription = "Pillow";
        SubCheckpoint subCheckpoint3 = new SubCheckpoint();
        subCheckpoint3.mSubCheckpointId = 1;
        subCheckpoint3.mDescription = "Bed";

        subCheckpointArrayList.add(subCheckpoint);
        subCheckpointArrayList.add(subCheckpoint2);
        subCheckpointArrayList.add(subCheckpoint3);
        return subCheckpointArrayList;
    }

    public static List<Checkpoint> getDummyCheckpoints() {
        List<Checkpoint> subCheckpointArrayList = new ArrayList<>();

        Checkpoint subCheckpoint = new Checkpoint();
        subCheckpoint.mCheckpointId = 1;
        subCheckpoint.mDescription = "Bedsheet";
        subCheckpoint.setIncluded(true);
        Checkpoint subCheckpoint2 = new Checkpoint();
        subCheckpoint2.mCheckpointId = 1;
        subCheckpoint2.mDescription = "Pillow";
        subCheckpoint2.setIncluded(true);
        Checkpoint subCheckpoint3 = new Checkpoint();
        subCheckpoint3.mCheckpointId = 1;
        subCheckpoint3.mDescription = "Bed";
        subCheckpoint3.setIncluded(true);

        subCheckpointArrayList.add(subCheckpoint);
        subCheckpointArrayList.add(subCheckpoint2);
        subCheckpointArrayList.add(subCheckpoint3);
        return subCheckpointArrayList;
    }

    public static ArrayList<IssueListModelV3> getDummyIssues() {
        ArrayList<IssueListModelV3> list = new ArrayList<>();
        IssueListModelV3 issue1 = new IssueListModelV3();
        issue1.setCategoryText("Bla bla");
        issue1.setIssueID(1);
        issue1.setCheckpointText("blu blu");
        issue1.setSubCheckpointText("subcheckpoint");
        issue1.setRoomID("101");
        issue1.setAuditType("room");
        issue1.setSeverity(3);

        IssueListModelV3 issue2 = new IssueListModelV3();
        issue2.setCategoryText("Bla bla");
        issue2.setIssueID(1);
        issue2.setCheckpointText("bluuu bluuu");
        issue2.setSubCheckpointText("subcheckpoint2");
        issue2.setRoomID("102");
        issue2.setAuditType("common");
        issue2.setSeverity(2);

        IssueListModelV3 issue3 = new IssueListModelV3();
        issue3.setCategoryText("Blaaa blaa");
        issue3.setIssueID(1);
        issue3.setCheckpointText("bluuuuu bluuuuuu");
        issue3.setSubCheckpointText("subcheckpoint3");
        issue3.setRoomID("105");
        issue3.setAuditType("room");
        issue2.setSeverity(1);

        list.add(issue1);
        list.add(issue2);
        list.add(issue3);
        return list;
    }

    public static ArrayList<SectionedTaskListModel> getSectionedTaskList() {
        ArrayList<SectionedTaskListModel> masterList = new ArrayList<>();
        SectionedTaskListModel model = new SectionedTaskListModel();
        model.setSectionName("Hot tasks");
        model.setTaskList(getTasks());

        SectionedTaskListModel model1 = new SectionedTaskListModel();
        model1.setSectionName("Pending");
        model1.setTaskList(getTasks());

        SectionedTaskListModel model2 = new SectionedTaskListModel();
        model2.setSectionName("Today");
        model2.setTaskList(getTasks());

        masterList.add(model);
        masterList.add(model1);
        masterList.add(model2);
        return masterList;
    }

    public static ArrayList<TaskModel> getTasks() {
        ArrayList<TaskModel> taskList = new ArrayList<>();
        TaskModel model = new TaskModel();
        model.setHeading("Fill Survey");
        model.setInfo("Random gibberish");
//        model.setTaskCardType(Constants.MANUAL_TASK);
        model.setToolTipText("Tooltip text");

        TaskModel model1 = new TaskModel();
        model1.setHeading("Manual task 1");
        model1.setInfo("Random gibberish");
//        model1.setTaskCardType(Constants.MANUAL_TASK);
        model1.setToolTipText("Tooltip text");

        TaskModel model2 = new TaskModel();
        model2.setHeading("Manual task 2");
        model2.setInfo("Random gibberish");
//        model2.setTaskCardType(Constants.MANUAL_TASK);
        model2.setToolTipText("Tooltip text");

        taskList.add(model);
        taskList.add(model1);
        taskList.add(model2);
        return taskList;
    }


    public static TaskModel getDummyManualTask() {
        TaskModel taskModel = new TaskModel();
        taskModel.setTaskID(111);
        taskModel.setTaskCardType("Manual");
        taskModel.setHeading("Manual Task Heading");
        taskModel.setCompletionDate("2016-11-08T01:44:27.195");
        taskModel.setInfo("fehrbghbehrwb");
        taskModel.setContentLink("www.google.com");
        return taskModel;
    }

    public static TaskModel getAuditCompletedTask() {
        TaskModel taskModel = new TaskModel();
        taskModel.setTaskCardType("Audit");
        taskModel.setTaskStatus("completed");
        taskModel.setHeading("Daily Audit");
        taskModel.setInfo("Completed Successfully");
        return taskModel;
    }

    public static ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> getMonthlyIncentiveList() {
        ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> list = new ArrayList<>();

        MonthlyIncentiveListResponse.MonthlyIncentiveListObject object1 =
                new MonthlyIncentiveListResponse.MonthlyIncentiveListObject();
        object1.setMonth(2);
        object1.setYear(2017);
        object1.setText("DJjqwdasdcjewf cjqewejc");

        MonthlyIncentiveListResponse.MonthlyIncentiveListObject object2 =
                new MonthlyIncentiveListResponse.MonthlyIncentiveListObject();
        object2.setMonth(1);
        object2.setYear(2017);
        object2.setText("DJjqwdasdcjewf cjqewejc");

        MonthlyIncentiveListResponse.MonthlyIncentiveListObject object3 =
                new MonthlyIncentiveListResponse.MonthlyIncentiveListObject();
        object3.setMonth(12);
        object3.setYear(2016);
        object3.setText("DJjqwdasdcjewf cjqewejc");

        list.add(object1);
        list.add(object2);
        list.add(object3);

        return list;
    }

    public static ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> getPastMonths() {
        ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> list = new ArrayList<>();

        MonthlyIncentiveListResponse.MonthlyIncentiveListObject object1 =
                new MonthlyIncentiveListResponse.MonthlyIncentiveListObject();
        object1.setMonth(9);
        object1.setYear(2016);
        object1.setText("DJjqwdasdcjewf cjqewejc");

        MonthlyIncentiveListResponse.MonthlyIncentiveListObject object2 =
                new MonthlyIncentiveListResponse.MonthlyIncentiveListObject();
        object2.setMonth(10);
        object2.setYear(2016);
        object2.setText("DJjqwdasdcjewf cjqewejc");

        MonthlyIncentiveListResponse.MonthlyIncentiveListObject object3 =
                new MonthlyIncentiveListResponse.MonthlyIncentiveListObject();
        object3.setMonth(11);
        object3.setYear(2016);
        object3.setText("DJjqwdasdcjewf cjqewejc");

        list.add(object1);
        list.add(object2);
        list.add(object3);

        return list;
    }

    public static ArrayList<DepartmentObject> getDepartments() {
        ArrayList<DepartmentObject> list = new ArrayList<>();

        DepartmentObject object1 = new DepartmentObject();
        object1.setDepartmentID(1);
        object1.setDepartmentName("House Keeping");
        object1.setTotalStaff(4);

        DepartmentObject object2 = new DepartmentObject();
        object2.setDepartmentID(2);
        object2.setDepartmentName("Front Office");
        object2.setTotalStaff(6);

        DepartmentObject object3 = new DepartmentObject();
        object3.setDepartmentID(1);
        object3.setDepartmentName("Food and Beverages");
        object3.setTotalStaff(2);

        DepartmentObject object4 = new DepartmentObject();
        object4.setDepartmentID(1);
        object4.setDepartmentName("Hotel Manager");
        object4.setTotalStaff(3);

        list.add(object1);
        list.add(object2);
        list.add(object3);
        list.add(object4);
        return list;
    }

    public static ArrayList<ContactInfoResponse.ContactInfoObject> getContactList() {
        ArrayList<ContactInfoResponse.ContactInfoObject> list = new ArrayList<>();

        ContactInfoResponse.ContactInfoObject contact1 = new ContactInfoResponse.ContactInfoObject();
        contact1.setDependency("For any audit or app related queries");
        contact1.setName("Slack channel: #prowl_v3_feedback");
        contact1.setSlackChannel(true);
        contact1.setChannelID("C3UR6QDJM");

        ContactInfoResponse.ContactInfoObject contact2 = new ContactInfoResponse.ContactInfoObject();
        contact2.setDependency("For any mapping related queries");
        contact2.setName("Ritesh Bhargava");
        contact2.setSlackChannel(false);
        contact2.setEmailID("ritesh.bhargava@gmail.com");
        contact2.setPhoneNumber("8888084232");

        ContactInfoResponse.ContactInfoObject contact3 = new ContactInfoResponse.ContactInfoObject();
        contact3.setDependency("For any other queries");
        contact3.setName("Ankita Gandhi");
        contact3.setSlackChannel(false);
        contact3.setEmailID("ankita.gandhi@gmail.com");
        contact3.setPhoneNumber("8888084232");

        list.add(contact1);
        list.add(contact2);
        list.add(contact3);

        return list;
    }
}
