package com.treebo.prowlapp.Utils;

import android.text.TextUtils;

import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.Photo;
import com.treebo.prowlapp.Models.Query;
import com.treebo.prowlapp.Models.Subsection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumandas on 18/06/2016.
 */
public class PhotoUtils {

    public static String getPhotoUrl(Photo photo){
        if(!photo.getmDeviceFilePath().isEmpty()){
            return photo.getmDeviceFilePath();
        }else{
            return photo.getmCloudFilePath();
        }
    }

    public static boolean isPhotoUploadsPending(List<Photo> photos){
        for(Photo photo:photos){
            if(!photo.isUploadedToCloud()){
                return true;
            }
        }
        return false;
    }

    public static List<Photo> getPendingPhotoUploads(List<Photo> photos){
        List<Photo> photoList=new ArrayList<>();
        for(Photo photo:photos){
            if(!photo.isUploadedToCloud()){
               photoList.add(photo);
            }
        }
        return photoList;
    }

    public static List<String> getCloudUrlsList(List<Photo> photos){
        List<String> cloudUrls=new ArrayList<>();
        for(Photo photo:photos){
            if(photo.isUploadedToCloud()){
                cloudUrls.add(photo.getmCloudFilePath());
            }
        }
        return cloudUrls;
    }

    public static String getCSVForUrlList(List<String> mUrls) {
        String urls = "";
        urls=TextUtils.join(",",mUrls);
        return urls;
    }

    @Deprecated
    public static void addPhotoUrlsToForm(List<Photo> photoList,Form form,String formKey){
        ArrayList<String> mPhotoUrlList=new ArrayList<>();
        for(Subsection subsection:form.getSubsections()){
            for(Query query:subsection.getQueries()){
                int queryId=query.getQueryId();
                String key=formKey+"_"+queryId;
                mPhotoUrlList.clear();
                for(Photo photo:photoList){
                    if(photo.getmKey().equals(key)){
                        mPhotoUrlList.add(photo.getmCloudFilePath());
                    }
                }
                query.setImage_url(getCSVForUrlList(mPhotoUrlList));
            }
        }
    }


}
