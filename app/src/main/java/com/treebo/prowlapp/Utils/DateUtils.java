package com.treebo.prowlapp.Utils;

import android.util.Pair;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by devesh on 23/06/16.
 */
public class DateUtils {

    private static final String descendingFormat = "yyyy/MM/dd HH:mm:ss:mmm";

    private static final String timeofDayFormat = "HH:mm:ss";

    public static boolean isDateValid(Date setDate) {
        if (setDate == null) {
            return false;
        }
        int diff = setDate.compareTo(new Date());
        return diff < 0;

    }

    public static String getTaskDueDate(String resolutionDate) {
        int daysLeft = Days.daysBetween(getTodaysDateTime(), DateTime.parse(resolutionDate)).getDays();
        switch (daysLeft) {
            case 0:
                return "Today";

            case 1:
                return "Tomorrow";

            default:
                return getDisplayDateFromTimeStamp(resolutionDate);
        }
    }

    public static String getIssueDueDate(String resolutionDate) {
        int daysLeft = Days.daysBetween(getTodaysDateTime(), DateTime.parse(resolutionDate)).getDays();
        if (daysLeft == 0) {
            return "Today";
        } else if (daysLeft < 0) {
            return daysLeft + " days";
        } else {
            return getDisplayDateFromTimeStamp(resolutionDate);
        }
    }

    public static String getResolutionOverflowToday(String resolutionDate) {
        int daysLeft = Days.daysBetween(getTodaysDateTime(), DateTime.parse(resolutionDate)).getDays();
        return daysLeft + " Days";
    }

    public static String getOverDueResolutionDate(String resolutionDate) {
        int daysLeft = Days.daysBetween(getTodaysDateTime(), DateTime.parse(resolutionDate)).getDays();
        if (daysLeft < 0)
            return "Overdue";
        else if (daysLeft == 0)
            return "Resolve Today";
        else
            return "Future Dues";
    }

    public static String getDisplayDateFromTimeStamp(String timeStamp) {
        DateTime time = DateTime.parse(timeStamp);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("E dd, MMM");
        return time.toString(fmt);
    }

    public static String getDisplayDateSectionHeaderFromTimeStamp(String timeStamp) {
        DateTime time = DateTime.parse(timeStamp);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MMM dd, E");
        return time.toString(fmt);
    }

    public static String getDateAndMonthInFullFromTimeStamp(String timeStamp) {
        DateTime time = DateTime.parse(timeStamp);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("EE, dd MMMM");
        return time.toString(fmt);
    }

    public static String getDateAndMonthInFullWithTimeFromTimeStamp(String timeStamp) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime time = DateTime.parse(timeStamp, formatter);
        int daysLeft = Days.daysBetween(getTodaysDateTime(), time).getDays();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("EE, dd MMMM HH:mm");
        String formatedString = time.toString(fmt);
        if (daysLeft == 0) {
            return "Today " + time.toString(fmt).substring(formatedString.length() - 5, formatedString.length());
        }
        return formatedString;
    }

    public static String getCreatedAtDateComparedToToday(String createdAt) {
        DateTime createdAtDateTime = DateTime.parse(createdAt);
        int daysLeft = Days.daysBetween(createdAtDateTime, getTodaysDateTime()).getDays();
        if (daysLeft == 0) {
            return "Today";
        } else if (daysLeft <= 7) {
            return "Last week";
        } else {
            return "All others";
        }
    }

    public static String getUpdatedAtDateComparedToToday(String updatedAt) {
        DateTime updatedAtTime = DateTime.parse(updatedAt);
        int daysLeft = Days.daysBetween(updatedAtTime, getTodaysDateTime()).getDays();
        if (daysLeft == 0) {
            return "Today";
        } else if (daysLeft == 1) {
            return "Yesterday";
        } else {
            return "All others";
        }
    }

    public static String getTimeRemainingFromEndTime(String endTime) {
        Pair<Integer, Integer> hoursMinutes = getTimeRemaining(endTime);
        String formattedTime;
        if (hoursMinutes.first < 9) {
            formattedTime = "0" + hoursMinutes.first;
        } else {
            formattedTime = Integer.toString(hoursMinutes.first);
        }
        if (hoursMinutes.second < 9) {
            formattedTime = formattedTime + ":" + "0" + hoursMinutes.second;
        } else {
            formattedTime = formattedTime + ":" + hoursMinutes.second;
        }
        return formattedTime;
    }

    private static Pair<Integer, Integer> getTimeRemaining(String endTime) {
        String[] time = endTime.split(":");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        c.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        c.set(Calendar.SECOND, Integer.valueOf(time[2]));
        c.set(Calendar.MILLISECOND, 0);
        long milliseconds = (c.getTimeInMillis() - System.currentTimeMillis());
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
        return new Pair<>(hours, minutes);
    }

    public static String getTimeRemainingFromExpiryDateString(String endTime) {
        Pair<Integer, Integer> hoursMinutes = getTimeRemainingFromExpiryDate(endTime);
        String formattedTime;
        if (hoursMinutes.first < 9) {
            formattedTime = "0" + hoursMinutes.first;
        } else {
            formattedTime = Integer.toString(hoursMinutes.first);
        }
        if (hoursMinutes.second < 9) {
            formattedTime = formattedTime + ":" + "0" + hoursMinutes.second;
        } else {
            formattedTime = formattedTime + ":" + hoursMinutes.second;
        }
        return formattedTime;
    }

    public static String getTimeRemainingFromExpiryDateDesc(String endTime) {
        Pair<Integer, Integer> hoursMinutes = getTimeRemainingFromExpiryDate(endTime);
        int hours = hoursMinutes.first;
        if (hours > 0) {
            return "HOURS LEFT";
        } else {
            return "MINUTES LEFT";
        }
    }

    private static Pair<Integer, Integer> getTimeRemainingFromExpiryDate(String endTime) {
        DateTime endDateTime = DateTime.parse(endTime);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, endDateTime.dayOfMonth().get());
        c.set(Calendar.HOUR_OF_DAY, endDateTime.getHourOfDay());
        c.set(Calendar.MINUTE, endDateTime.getMinuteOfHour());
        c.set(Calendar.SECOND, endDateTime.getSecondOfMinute());
        c.set(Calendar.MILLISECOND, endDateTime.getMillisOfSecond());
        long milliseconds = (c.getTimeInMillis() - System.currentTimeMillis());
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
        return new Pair<>(hours, minutes);
    }

    public static String getTimeRemainingFromEndTimeDesc(String endTime) {
        Pair<Integer, Integer> hoursMinutes = getTimeRemaining(endTime);
        int hours = hoursMinutes.first;
        if (hours > 0) {
            return "HOURS LEFT";
        } else {
            return "MINUTES LEFT";
        }
    }

    public static String getTimeTimeRemainingFromEODFormatted() {
        Pair<Integer, Integer> hoursMinutes = getTimeRemainingFromEOD();
        String formattedTime;
        if (hoursMinutes.first < 9) {
            formattedTime = "0" + hoursMinutes.first;
        } else {
            formattedTime = Integer.toString(hoursMinutes.first);
        }
        if (hoursMinutes.second < 9) {
            formattedTime = formattedTime + ":" + "0" + hoursMinutes.second;
        } else {
            formattedTime = formattedTime + ":" + hoursMinutes.second;
        }
        return formattedTime;
    }

    private static Pair<Integer, Integer> getTimeRemainingFromEOD() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long milliseconds = (c.getTimeInMillis() - System.currentTimeMillis());
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
        return new Pair<>(hours, minutes);
    }

    public static String getTimeRemainingFromEODDesc() {
        Pair<Integer, Integer> hoursMinutes = getTimeRemainingFromEOD();
        int hours = hoursMinutes.first;
        if (hours > 0) {
            return "HOURS LEFT";
        } else {
            return "MINUTES LEFT";
        }
    }

    public static String getDateTimeNow() {
        return getDateTimeNow(descendingFormat);
    }

    private static String getDateTimeNow(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        return dateFormat.format(calendar.getTime());
    }

    private static DateTime getTodaysDateTime() {
        return new DateTime().withTimeAtStartOfDay();
    }


    public static boolean isTimeWithin(String startTime, String endTime) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(timeofDayFormat);
        String timeToCheck = dateFormat.format(calendar.getTime());
        return checkTimeWithin(startTime, endTime, timeToCheck);
    }

    public static boolean checkTimeWithin(String startTime, String endTime, String timeToCheck) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(timeofDayFormat);
        Date startDate = dateFormat.parse(startTime);
        Date endDate = dateFormat.parse(endTime);
        Date now = dateFormat.parse(timeToCheck);
        if (now.after(startDate) && now.before(endDate)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isTimeExpired(String endTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dateTime = formatter.parseDateTime(endTime);
        DateTime now = DateTime.now();
        return dateTime.isBefore(now);
    }

    public static String getCurrentMonthString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM");
        Calendar calendar = Calendar.getInstance();
        return dateFormat.format(calendar.getTime());
    }

    public static String getTodaysDisplayDate() {
        DateTime time = DateTime.now();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("E, dd MMM");
        return time.toString(fmt);
    }

    public static String getDisplayDateFromDatePicker(int year, int monthOfYear, int dayOfMonth) {
        DateTime dateTime = new DateTime().withDate(year, monthOfYear, dayOfMonth);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("EE, dd MMMM YYYY");
        return dateTime.toString(fmt);
    }

    public static String getDisplayTimeFromTimePicker(int hourOfDay, int minute) {
        DateTime dateTime = new DateTime().withTime(hourOfDay, minute, 0, 0);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("hh m a");
        return dateTime.toString(fmt);
    }

    public static String getDisplayTimeFromServerTime(String serverTime) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dateTime;
        try {
            dateTime = formatter.parseDateTime(serverTime);
        } catch (IllegalArgumentException e) {
            return "";
        }
        DateTimeFormatter fmt = DateTimeFormat.forPattern("EE, dd MMM YYYY \nhh:m a");
        return dateTime.toString(fmt);
    }
}
