package com.treebo.prowlapp.Utils;

import android.util.Pair;

import com.treebo.prowlapp.Models.Answer;
import com.treebo.prowlapp.Models.AnswerListWithSectionName;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.Query;
import com.treebo.prowlapp.Models.Room;
import com.treebo.prowlapp.Models.RoomTypeList;
import com.treebo.prowlapp.Models.Subsection;
import com.treebo.prowlapp.sharedprefs.AuditPreferenceManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sumandas on 03/05/2016.
 */
public class AuditUtils {

    public static AuditPreferenceManager mAuditManager= AuditPreferenceManager.getInstance();

    public static Form getPausedCommonAreaAudit(String hotelId){
        return mAuditManager.getPersistedCommonAreaAudit(hotelId);
    }

    public static Form getPausedRoomAreaAudit(String hotelId, String roomNo){
        return mAuditManager.getPersistedRoomAreaAudit(hotelId, roomNo);
    }

    public static String getPercentageTaskRemainingInAudit(Form form){
        if(form==null){
            return "";
        }else{
            int noCompletedTasks=0;
            int noTotalTasks=0;
            for(Subsection subsection:form.getSubsections()){
                for(Query query:subsection.getQueries()){
                    if(!query.getAnswer().equalsIgnoreCase("") ){
                        noCompletedTasks++;
                    }
                    noTotalTasks++;
                }
            }
            return Integer.toString(noTotalTasks-noCompletedTasks)+"/"+Integer.toString(noTotalTasks);
        }
    }

    public static float getPercentTaskRemainingInAudit(Form form){
        if(form==null){
            return -1;
        }else{
            int noCompletedTasks=0;
            int noTotalTasks=0;
            for(Subsection subsection:form.getSubsections()){
                for(Query query:subsection.getQueries()){
                    if(!query.getAnswer().equalsIgnoreCase("") ){
                        noCompletedTasks++;
                    }
                    noTotalTasks++;
                }
            }
            return ((noTotalTasks-noCompletedTasks)*100)/noTotalTasks;
        }
    }

    public static AnswerListWithSectionName convertFormtoSubmit(Form form, String sectionName){
        List<Answer> answerList=new ArrayList<>(form.getSubsections().size());
        for(Subsection subsection:form.getSubsections()){
            for(Query query:subsection.getQueries()){
                Answer answer=new Answer(query.getAnswer(),query.getComment(),query.getSevere()
                        ,query.getImage_url(),query.getQueryName(),query.getQueryId());
                answerList.add(answer);
            }
        }
        AnswerListWithSectionName answerListWithSectionName
                =new AnswerListWithSectionName(sectionName,answerList);
        return answerListWithSectionName;
    }

    public static ArrayList<Pair<String,Form>> getPausedRoomAudits(String hotelId, ArrayList<RoomTypeList> roomList){
        ArrayList<Pair<String,Form>> mPausedRooms=new ArrayList<>(roomList.size());
        for(RoomTypeList roomTypeList:roomList){
            for(Room room:roomTypeList.rooms){
                Form savedRoom= AuditUtils.getPausedRoomAreaAudit(hotelId,room.number);
                if(savedRoom!=null){
                    mPausedRooms.add(new Pair(room.number,savedRoom));
                }
            }
        }
        return mPausedRooms;
    }


}
