package com.treebo.prowlapp.Utils;


import android.app.DownloadManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.pushMessaging.receivers.DownloadCompleteReceiver;

import java.io.File;

/**
 * Created by aa on 12/08/16.
 */
public final class AppUpdateHelperV2 {

    //NOTE: using same preference name as AppUpdateHelper so that data remains same.
    //Need to discuss this handling.
    private static final String SHARED_PREF_NAME = "app_update_prefs";
    private static final String PREF_APP_UPDATE_URL = "app_update_url";
    private static final String PREF_SERVER_APP_VERSION = "server_app_version";
    private static final String PREF_DOWNLOAD_REFERENCE = "download_reference";
    public static final String PREF_UPDATE_DOWNLOADED = "app_update_downloaded";

    private static final String TAG = AppUpdateHelperV2.class.getName();
    private static final String DOWNLOAD_DIRECTORY = "/download/";
    private static final String DOWNLOAD_TEXT = "Updating Prowl";

    private SharedPreferences mSharedPreferences;
    private DownloadManager mDownloadManager;

    private static volatile AppUpdateHelperV2 sInstance;

    private AppUpdateHelperV2() {
        mSharedPreferences = MainApplication.getAppContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public static AppUpdateHelperV2 getInstance() {
        AppUpdateHelperV2 localInstance = sInstance;
        if (localInstance == null) {
            synchronized (AppUpdateHelperV2.class) {
                localInstance = sInstance = new AppUpdateHelperV2();
            }
        }
        return localInstance;
    }

    public long getServerAppVersion() {
        return mSharedPreferences.getLong(PREF_SERVER_APP_VERSION, 0);
    }

    public boolean setServerAppVersion(long serverAppVersion) {
        return mSharedPreferences.edit().putLong(PREF_SERVER_APP_VERSION, serverAppVersion).commit();
    }

    public String getAppUpdateUrl() {
        return mSharedPreferences.getString(PREF_APP_UPDATE_URL, null);
    }

    public boolean setAppUpdateUrl(String appUpdateUrl) {
        return mSharedPreferences.edit().putString(PREF_APP_UPDATE_URL, appUpdateUrl).commit();
    }

    public long getDownloadReference() {
        return mSharedPreferences.getLong(PREF_DOWNLOAD_REFERENCE, -1);
    }

    public boolean setDownloadReference(long downloadReference) {
        return mSharedPreferences.edit().putLong(PREF_DOWNLOAD_REFERENCE, downloadReference).commit();
    }

    public boolean clearDownloadReference() {
        return mSharedPreferences.edit().remove(PREF_DOWNLOAD_REFERENCE).commit();
    }

    public boolean getUpdateDownloaded() {
        return mSharedPreferences.getBoolean(PREF_UPDATE_DOWNLOADED, false);
    }

    public boolean setUpdateDownloaded(boolean updateDownloaded) {
        return mSharedPreferences.edit().putBoolean(PREF_UPDATE_DOWNLOADED, updateDownloaded).commit();
    }

    public String getApkFileName() {
        //return "prowl-v" + getServerAppVersion() + ".apk";
        return "prowl.apk";
    }

    public void updateApp(MixpanelAPI mixpanelAPI) {
        //check if update is required
        if (isUpdateRequired()) {

            //mix panel app update tracking
            MixPanelManager.trackAppUpdateEvent(mixpanelAPI,
                    MixPanelManager.APP_UPDATE_SERVICE,
                    MixPanelManager.EVENT_APP_OUTDATED,
                    String.valueOf(getCurrentAppVersion())
            );

            //get download manager instance
            initDownloadManager();

            //if update is downloaded then install it
            if (isUpdateDownloaded()) {

                //mix panel app update tracking
                MixPanelManager.trackAppUpdateEvent(mixpanelAPI,
                        MixPanelManager.APP_UPDATE_SERVICE,
                        MixPanelManager.EVENT_UPDATE_DOWNLOADED,
                        String.valueOf(getCurrentAppVersion())
                );

                //if update is possible (file is neither moved nor deleted) then update otherwise start new download
                if (!tryInstallUpdate()) {

                    //mix panel app update tracking
                    MixPanelManager.trackAppUpdateEvent(mixpanelAPI,
                            MixPanelManager.APP_UPDATE_SERVICE,
                            MixPanelManager.EVENT_APP_UPDATE__FILE_MOVED,
                            String.valueOf(getCurrentAppVersion())
                    );

                    startUpdateDownload();

                    //enable download complete receiver
                    enableDownloadCompleteReceiver();
                } else {

                    //mix panel app update tracking
                    MixPanelManager.trackAppUpdateEvent(mixpanelAPI,
                            MixPanelManager.APP_UPDATE_SERVICE,
                            MixPanelManager.EVENT_APP_UPDATE_DIALOG_SHOWN,
                            String.valueOf(getCurrentAppVersion())
                    );

                    //disable download complete receiver
                    disableDownloadCompleteReceiver();
                }
            } else if (!isDownloadRunning()) {
                //if download is not running then start download;
                startUpdateDownload();

                //enable download complete receiver
                enableDownloadCompleteReceiver();
            } else {

                //mix panel app update tracking
                MixPanelManager.trackAppUpdateEvent(mixpanelAPI,
                        MixPanelManager.APP_UPDATE_SERVICE,
                        MixPanelManager.EVENT_UPDATE_NOT_DOWNLOADED,
                        String.valueOf(getCurrentAppVersion())
                );
            }
        } else {
            Log.d(TAG, "App is already at latest server version");

            //mix panel app update tracking
            MixPanelManager.trackAppUpdateEvent(mixpanelAPI,
                    MixPanelManager.APP_UPDATE_SERVICE,
                    MixPanelManager.EVENT_APP_UPDATED,
                    String.valueOf(getCurrentAppVersion())
            );

            //remove download manager instance for garbage collection
            removeDownloadManagerInstance();

            //disable download complete receiver
            disableDownloadCompleteReceiver();

            //make is update downloaded false for compatibility with AppUpdateHelper class
            setUpdateDownloaded(false);
        }
    }

    //if server app version is greater than current app version then return true
    public boolean isUpdateRequired() {
        return getServerAppVersion() > getCurrentAppVersion();
    }

    //function to get current app version
    public int getCurrentAppVersion() {
        int versionCode = 0;
        try {
            Context localAppContext = MainApplication.getAppContext();
            PackageInfo pInfo = localAppContext.getPackageManager().getPackageInfo(localAppContext.getPackageName(), 0);
            versionCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        return versionCode;
    }

    //initialize download manager instance
    public void initDownloadManager() {
        if (mDownloadManager == null) {
            mDownloadManager = (DownloadManager) MainApplication.getAppContext().getSystemService(Context.DOWNLOAD_SERVICE);
        }
    }

    //remove download manager instance for garbage collection
    public void removeDownloadManagerInstance() {
        mDownloadManager = null;
    }

    //check if update is running
    private boolean isDownloadRunning() {
        long downloadReference = getDownloadReference();
        if (downloadReference == -1) {
            return false;
        } else {
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(downloadReference);

            Cursor cursor = mDownloadManager.query(query);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                int status = cursor.getInt(columnIndex);
                if (DownloadManager.STATUS_RUNNING == status
                        || DownloadManager.STATUS_PENDING == status
                        || DownloadManager.STATUS_PAUSED == status) {
                    Log.v("AppUpdate","Download running");
                    return true;
                } else {
                    Log.v("AppUpdate","Download failed");
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    //check if update is downloaded or not
    private boolean isUpdateDownloaded() {
        long downloadReference = getDownloadReference();
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadReference);

        Cursor cursor = mDownloadManager.query(query);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);

            //if status is successful that means update is download
            if (DownloadManager.STATUS_SUCCESSFUL == cursor.getInt(columnIndex)) {
                Log.v("AppUpdate","Download complete");
                return true;
            } else {
                Log.v("AppUpdate","Download failed");
                return false;
            }
        } else {
            return false;
        }
    }

    //enqueue download request in DownloadManager
    private boolean startUpdateDownload() {

        //make is update downloaded false for compatibility with AppUpdateHelper class
        setUpdateDownloaded(false);

        Uri uri = Uri.parse(getAppUpdateUrl());
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setTitle(DOWNLOAD_TEXT);

        String PATH = Environment.getExternalStorageDirectory() + DOWNLOAD_DIRECTORY;
        File file = new File(PATH);
        boolean success = true;
        if (!file.exists()) {
            success = file.mkdir();
        }
        if (success) {
            file.mkdirs();
            File outputFile = new File(file, getApkFileName());
            if (outputFile.exists()) {
                outputFile.delete();
            }
            request.setDestinationUri(Uri.fromFile(outputFile));

            //enqueue request and store download reference id
            long downloadReference = mDownloadManager.enqueue(request);
            setDownloadReference(downloadReference);
            return true;
        } else {
            Toast toast = Toast.makeText(MainApplication.getAppContext(), "Update failed \nIf this issue persists, try restarting your device !", Toast.LENGTH_LONG);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            if (v != null) v.setGravity(Gravity.CENTER);
            toast.show();
            return false;
        }
    }

    //install app update
    public boolean tryInstallUpdate() {
        String fileName = getApkFileName();
        Intent viewIntent = new Intent(Intent.ACTION_VIEW);
        File file = new File(Environment.getExternalStorageDirectory() + DOWNLOAD_DIRECTORY + fileName);
        if (file.exists() && !file.isDirectory()) {
            viewIntent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            viewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            MainApplication.getAppContext().startActivity(viewIntent);
            return true;
        } else {
            //if file is not present (moved or deleted), then clear download reference and start start new download
            clearDownloadReference();
            return false;
        }
    }

    //enable DownloadCompleteReceiver Component
    public void enableDownloadCompleteReceiver() {
        ComponentName receiver = new ComponentName(MainApplication.getAppContext(), DownloadCompleteReceiver.class);
        PackageManager pm = MainApplication.getAppContext().getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    //disable DownloadCompleteReceiver Component
    public void disableDownloadCompleteReceiver() {
        ComponentName receiver = new ComponentName(MainApplication.getAppContext(), DownloadCompleteReceiver.class);
        PackageManager pm = MainApplication.getAppContext().getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

}