package com.treebo.prowlapp.Utils;


import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by devesh on 03/05/16.
 */
public class HttpCodeUtils {

    public static boolean isHttp401Error(Throwable throwable) {
        return isErrorCode(throwable, 401);
    }

    public static boolean isHttp400Error(Throwable throwable) {
        return isErrorCode(throwable, 400);
    }

    public static boolean isHttp409Error(Throwable throwable) {
        return isErrorCode(throwable, 409);
    }

    public static boolean isHttp404Error(Throwable throwable) {
        return isErrorCode(throwable, 404);
    }
    private static boolean isErrorCode(Throwable throwable, int code) {
        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            return (httpException.code() == code);
        }
        return false;
    }


}