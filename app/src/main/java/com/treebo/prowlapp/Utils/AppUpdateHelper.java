package com.treebo.prowlapp.Utils;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.io.File;

/**
 * Created by Saksham Dhawan on 19-04-2016.
 */
public class AppUpdateHelper {


    public static final String PREF_APP_VERSION_ON_SERVER = "server_app_version";
    public static final String PREF_LAST_UPDATED = "last_updated";
    public static final String PREF_APP_VERSION = "app_version";
    public static final String PREF_UPDATE_DOWNLOADED = "app_update_downloaded";
    public static final String PREF_DOWNLOAD_REFERENCE = "download_reference";
    public static final String APP_UPDATE_PREFERENCES = "app_update_prefs";
    private static final String PREF_APP_UPDATE_URL = "app_update_url";

    public static final long FREQUENCY_ONE_HOUR = 3600 * 1000;
    public static final long FREQUENCY_ONE_DAY = 86400 * 1000;
    public static final long FREQUENCY_ONE_Week = 86400 * 1000 * 7;


    private Context context;
    private String downloadFileName;
    private String pathPostExternalDirectory;
    private String downloadingText;
    private Long frequency;

    SharedPreferences sharedpreferences;
    private long mDownloadReference;
    private DownloadManager mDownloadManager;
    private BroadcastReceiver mDownloadCompleteReceiver;
    private boolean isReceiverInitialized = false;
    private GetLatestVersionInfo getLatestVersionInfo;
    public MixpanelAPI mMixpanelApi;

    public interface GetLatestVersionInfo {
        void makeLatestVersionInfoCall(UpdateCallFinished updateCallFinished);
    }

    public interface UpdateCallFinished {
        void onUpdateCallFinished(UpdateInfo updateInfo);
    }

    public static class UpdateInfo {
        public long latestVersionCode = -1;
        public String updateUrl;
    }


    public AppUpdateHelper(Context context, String downloadFileName, String downloadingText,
                           String pathPostExternalDirectory, GetLatestVersionInfo getLatestVersionInfo,
                           long frequency, MixpanelAPI mixpanelAPI) {
        this.context = context;
        this.sharedpreferences = this.context.getSharedPreferences(APP_UPDATE_PREFERENCES,
                Context.MODE_PRIVATE);
        mDownloadReference = -1;
        this.downloadFileName = downloadFileName;
        this.pathPostExternalDirectory = pathPostExternalDirectory;
        this.getLatestVersionInfo = getLatestVersionInfo;
        this.downloadingText = downloadingText;
        this.frequency = frequency;

        mMixpanelApi = mixpanelAPI;

        if (!isReceiverInitialized) {
            initializeReceivers();
        }

    }


    public void checkAppUpdateAndInstall() {
        int versionCode = setAndGetAppVersion();
        Boolean appOutdated = isAppOutdated(versionCode);

        Log.d("app outdated", String.valueOf(appOutdated));
        Log.d("downloaded", String.valueOf(isUpdateDownloaded()));
        Log.d("app version", String.valueOf(sharedpreferences.getLong(PREF_APP_VERSION, 0)));
        Log.d("server version", String.valueOf(sharedpreferences.getLong(PREF_APP_VERSION_ON_SERVER, 0)));
        Log.d("download reference", String.valueOf(mDownloadReference));

        if (!downloadRunning())
            if (appOutdated) {
                if (isUpdateDownloaded()) {
                    MixPanelManager.trackAppUpdateEvent(mMixpanelApi, MixPanelManager.SCREEN_HOME,
                            MixPanelManager.EVENT_UPDATE_DOWNLOADED, String.valueOf(sharedpreferences.getLong(PREF_APP_VERSION, 0)));
                    installUpdate();
                } else {
                    Log.d("update status", "app outdated but file not downloaded");
                    MixPanelManager.trackAppUpdateEvent(mMixpanelApi, MixPanelManager.SCREEN_HOME,
                            MixPanelManager.EVENT_UPDATE_NOT_DOWNLOADED, String.valueOf(sharedpreferences.getLong(PREF_APP_VERSION, 0)));
                    checkForUpdates();
                }
            } else {
                sharedpreferences.edit().putBoolean(PREF_UPDATE_DOWNLOADED, false).apply();
                if (((System.currentTimeMillis() - sharedpreferences.getLong(PREF_LAST_UPDATED, 0) > frequency))) {
                    Log.d("update check", "not required");
                    checkForUpdates();
                } else
                    Log.d("update check", "required");
            }
        else
            Log.d("download status", "already running");
    }

    private boolean downloadRunning() {
        mDownloadReference = sharedpreferences.getLong(PREF_DOWNLOAD_REFERENCE, -1);
        if (mDownloadReference == -1) {
            return false;
        } else {
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(mDownloadReference);
            Cursor cursor = mDownloadManager.query(query);
            if (cursor.getCount() == 0) {
                Log.d("download status", "deleted / interrupted");
                return false;
            } else
                return true;
        }
    }


    private int setAndGetAppVersion() {
        PackageInfo pInfo = null;
        int versionCode = 0;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionCode = pInfo.versionCode;
            sharedpreferences.edit().putLong(PREF_APP_VERSION, versionCode).commit();
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        return versionCode;
    }

    boolean isAppOutdated(int versionCode) {
        return (sharedpreferences.getLong(PREF_APP_VERSION_ON_SERVER, 0) > versionCode);
    }

    boolean isUpdateDownloaded() {
        return sharedpreferences.getBoolean(PREF_UPDATE_DOWNLOADED, false);
    }

    protected void installUpdate() {
        Intent viewIntent = new Intent(Intent.ACTION_VIEW);

        File file = new File(Environment.getExternalStorageDirectory() + pathPostExternalDirectory + downloadFileName);
        if (file.exists()) {
            viewIntent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            viewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(viewIntent);
        } else {
            sharedpreferences.edit().putBoolean(PREF_UPDATE_DOWNLOADED, false).apply();
            checkForUpdates();
        }

    }

    private void checkForUpdates() {


        getLatestVersionInfo.makeLatestVersionInfoCall(new UpdateCallFinished() {
            @Override
            public void onUpdateCallFinished(UpdateInfo updateInfo) {
                if (updateInfo == null) {
                    Log.d("UPDATE", " SITE UNREACHABLE");
                    return;
                }
                sharedpreferences.edit().putLong(PREF_APP_VERSION_ON_SERVER, updateInfo.latestVersionCode).commit();
                sharedpreferences.edit().putLong(PREF_LAST_UPDATED, System.currentTimeMillis()).commit();
                PackageInfo pInfo = null;
                try {
                    pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                if (pInfo.versionCode < updateInfo.latestVersionCode) {
                    MixPanelManager.trackAppUpdateEvent(mMixpanelApi, MixPanelManager.SCREEN_HOME,
                            MixPanelManager.EVENT_APP_OUTDATED, String.valueOf(sharedpreferences.getLong(PREF_APP_VERSION, 0)));
                    Log.d("current version", String.valueOf(pInfo.versionCode));
                    Log.d("server version", String.valueOf(updateInfo.latestVersionCode));
                    Log.d("UPDATE", "AVAILABLE");

                    //save update url in shared preferences
                    sharedpreferences.edit().putString(PREF_APP_UPDATE_URL, updateInfo.updateUrl).commit();

                    tryAppUpdate(updateInfo.updateUrl);
                } else {
                    MixPanelManager.trackAppUpdateEvent(mMixpanelApi, MixPanelManager.SCREEN_HOME,
                            MixPanelManager.EVENT_APP_UPDATED, String.valueOf(sharedpreferences.getLong(PREF_APP_VERSION, 0)));
                    Log.d("UPDATE", "NO ALREADY RUNNING THE LATEST VERSION");
                }


            }
        });
    }


    private void tryAppUpdate(String mUpdateUrl) {

        //get download reference from shared preferences
        mDownloadReference = sharedpreferences.getLong(PREF_DOWNLOAD_REFERENCE, -1);

        Uri uri = Uri.parse(mUpdateUrl);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        //request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        request.setTitle(downloadingText);

        String PATH = Environment.getExternalStorageDirectory() + pathPostExternalDirectory;
        File file = new File(PATH);
        boolean success = true;
        if (!file.exists()) {
            success = file.mkdir();
        }
        if (success) {
            file.mkdirs();
            File outputFile = new File(file, downloadFileName);
            if (outputFile.exists()) {
                outputFile.delete();
            }
            request.setDestinationUri(Uri.fromFile(outputFile));
            if (mDownloadReference == -1) {
                mDownloadReference = mDownloadManager.enqueue(request);

                //add download reference in shared preferences
                sharedpreferences.edit().putLong(PREF_DOWNLOAD_REFERENCE, mDownloadReference).commit();
            } else {
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(mDownloadReference);
                Cursor cursor = mDownloadManager.query(query);
                if (cursor.getCount() == 0) {
                    Log.d("download status", "deleted / interrupted");
                    mDownloadReference = mDownloadManager.enqueue(request);

                    //add download reference in shared preferences
                    sharedpreferences.edit().putLong(PREF_DOWNLOAD_REFERENCE, mDownloadReference).commit();

                }
            }
        } else {
            Toast toast = Toast.makeText(context, "Update failed \nIf this issue persists, try restarting your device !", Toast.LENGTH_LONG);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            if (v != null) v.setGravity(Gravity.CENTER);
            toast.show();


        }

    }


    private void initializeReceivers() {
        mDownloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        IntentFilter downloadIntentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);

        mDownloadCompleteReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

                if (mDownloadReference == reference) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(reference);
                    Cursor cursor = mDownloadManager.query(query);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
/*                    JSONObject props = new JSONObject();
                    props.put(MixPanelManager.SCREEN, "HotelsOverViewActivity");
                    props.put(MixPanelManager.TIMESTAMP, Utilities.giveCurrentTime());
                    props.put(MixPanelManager.EVENT_UPDATE, "download status received");*/
                    if (DownloadManager.STATUS_SUCCESSFUL == cursor.getInt(columnIndex)) {
                        Log.d("Download status", "complete");
                        //props.put(MixPanelManager.EVENT_UPDATE, "download successful");
                        //mMixpanelApi.track(MixPanelManager.EVENT_UPDATE, props);
                        sharedpreferences.edit().putBoolean(PREF_UPDATE_DOWNLOADED, true).apply();
                        mDownloadReference = -1;
                        installUpdate();
                    } else {
                        //props.put(MixPanelManager.EVENT_UPDATE, "download not succcessful");
                        //mMixpanelApi.track(MixPanelManager.EVENT_UPDATE, props);
                        Log.d("Download status", "failed");
                        mDownloadReference = -1;
                    }


                }
            }
        };

        context.registerReceiver(mDownloadCompleteReceiver, downloadIntentFilter);

        isReceiverInitialized = true;
    }

    public void unregisterReceivers() {
        context.unregisterReceiver(mDownloadCompleteReceiver);
    }

}


