package com.treebo.prowlapp.Utils;

import android.content.Context;

import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

/**
 * Created by abhisheknair on 14/12/16.
 */

public class SegmentAnalyticsManager {

    private static Context mContext = MainApplication.getAppContext();

    public static LoginSharedPrefManager mLoginManager;

    /**
     * Event Names
     */
    // Audit Events
    public static final String AUDIT_START = "audit_start";
    public static final String AUDIT_SUBMIT = "audit_submit";
    public static final String AUDIT_PAUSED = "audit_paused";
    public static final String AUDIT_RESUMED = "audit_resumed";
    public static final String CHANGES_BUTTON_SUBMIT = "changes_button_submit";
    public static final String SELECT_AREA_CLICK = "select_area_click";
    public static final String SELECT_AREA_SELECTED = "select_area_selected";
    public static final String SELECT_ANOTHER_ROOM = "select_another_room";

    // Issue Events
    public static final String ISSUE_LOG_VIEW = "issue_log_view";
    public static final String ISSUE_DETAILS_VIEW = "issue_details_view";
    public static final String ISSUE_RESOLVE = "issue_resolve";
    public static final String ISSUE_UPDATE = "issue_update";
    public static final String SORT_CLICK = "sort_usage";
    public static final String CREATE_NEW_ISSUE = "create_new_issue";

    // Portfolio Events
    public static final String PORTFOLIO_VIEW = "portfolio_screenload_view";
    public static final String PORTFOLIO_SCORE_CLICK = "score_click_portfolio";
    public static final String PROPERTY_SELECT = "property_select";
    public static final String MOCK_LOCATION_ON = "mock_on";
    public static final String SETTINGS_CLICK = "settings_click";
    public static final String FEEDBACK_CLICK = "feedback_click";
    public static final String FEEDBACK_SUBMIT = "feedback_submit";
    public static final String SIGNOUT_CLICK = "signout_click";
    public static final String EXPLORE_CLICK = "explore_click";

    // Dashboard Events
    public static final String DASHBOARD_VIEW = "property_screenload";
    public static final String DASHBOARD_SCORE_CLICK = "score_click_property";
    public static final String TASK_CARD_CLICK = "task_card_click";
    public static final String AUDIT_ACTION_CLICK = "audit_action_click";
    public static final String TASK_DONE_CLICK = "task_done_click";
    public static final String TOOLTIP_CLICK = "tooltip_click";

    // Crash Events
    public static final String EXCEPTION_CAUGHT_EVENTS = "exception_caught";



    /**
     * Property Names
     */
    // Default Properties
    private static final String EMAIL = "email";
    private static final String LOCATION = "current_user_location";

    // Unique Properties
    public static final String IS_IN_GEOFENCE = "is_in_geofence";
    public static final String HOTEL_NAME = "hotel_name";
    public static final String AUDIT_AREA = "audit_area";
    public static final String IS_ROOM_AVAILABLE = "is_room_available";
    public static final String IS_ROOM_RECOMMENDED = "is_recommended";
    public static final String ROOM_NUMBER = "room_number";
    public static final String COMPLETION_RATIO = "completion_ratio";
    public static final String CATEGORY_SELECTED = "category_selected";
    public static final String SOURCE_SCREEN = "source_screen";
    public static final String AUDIT_CARD_ACTION = "audit_card_action";
    public static final String TASK_CARD_ID = "task_card_id";
    public static final String TASK_TYPE = "task_type";
    public static final String SORT_CRITERIA = "sort_criteria";
    public static final String AUDIT_TYPE = "audit_type";
    public static final String ACTIVITY_NAME = "activity";
    public static final String EXCEPTION_REASON = "reason";

    // Property Values
    public static final String ACTION_TRASH = "trash";
    public static final String ACTION_RESUME = "resume";
    public static final String CARD_TYPE_AUDIT = "audit";
    public static final String CARD_TYPE_ISSUE = "issue";
    public static final String CARD_TYPE_MANUAL = "manual";
    public static final String SOURCE_FROM_GEOFENCE = "geofence";
    public static final String COMMON_AREA = "common_area";
    public static final String ROOM = "room";
    public static final String IS_PRE_MARKED_ISSUE = "pre_marked_issue";

    /*
    *   Screen Names
    * */
    public static final String PORTFOLIO_SCREEN = "portfolio_screen";
    public static final String DASHBOARD_SCREEN = "dashboard_screen";
    public static final String ISSUE_LIST_SCREEN = "issue_list_screen";
    public static final String ISSUE_DETAILS_SCREEN = "issue_details_screen";
    public static final String RESOLVABLE_ISSUES_SCREEN = "resolvable_issues_screen";

    public static void trackEventOnSegment(String eventName, Properties properties) {
        properties.putAll(getDefaultProperties());
        Analytics.with(mContext)
                .track(eventName, properties);
    }

    public static void trackScreenOnSegment(String screenName, Properties properties) {
        properties.putAll(getDefaultProperties());
        Analytics.with(mContext)
                .screen(null, screenName, properties);
    }

    public static void setmLoginManager(LoginSharedPrefManager loginManager) {
        mLoginManager = loginManager;
    }

    private static Properties getDefaultProperties() {
        Properties defaultProperties = new Properties();
        if (mLoginManager != null) {
            defaultProperties.putValue(EMAIL, mLoginManager.getEmailId());
            defaultProperties.putValue(LOCATION, mLoginManager.getCurrentLatAndLong());
        }
        return defaultProperties;
    }

}
