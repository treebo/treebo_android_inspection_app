package com.treebo.prowlapp.Utils;


import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.application.MainApplication;

/**
 * Created by kuliza123 on 19/6/15.
 */
public class AmazonS3ClientCredentials {

    public  static String SUBCATEGORY_PATH(int user_id,int Category_id, int subcategoryId){
        StringBuilder subCatgoeryPath = new StringBuilder();

        subCatgoeryPath.append(BuildConfig.AWS_BUCKET_NAME);
        subCatgoeryPath.append("/user/");
        subCatgoeryPath.append(user_id);
        subCatgoeryPath.append("/category/");
        subCatgoeryPath.append(Category_id);
        subCatgoeryPath.append("/subcategory/");

        return  subCatgoeryPath.toString();
    }


    public  static AmazonS3Client getAmazonImageUploadInstance(){

        AmazonS3Client s3Client = new AmazonS3Client(getCredentialsProvider());
        return s3Client;
    }

    public static CognitoCachingCredentialsProvider getCredentialsProvider(){

        // Initialize the AWS Credential
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                MainApplication.getAppContext(),
                "ap-southeast-1:ea5b278f-d7e7-4a91-8362-0bf54c549325",Regions.AP_SOUTHEAST_1
        );

        return credentialsProvider;
    }

}
