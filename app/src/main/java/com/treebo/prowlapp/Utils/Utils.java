package com.treebo.prowlapp.Utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.common.GoogleApiAvailability;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.Models.IssuesModel;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.Category_Table;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint_Table;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint_Table;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.Models.issuemodel.SortByModel;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.response.staffincentives.MonthlyIncentiveListResponse;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.views.TreeboTextView;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

import rx.subscriptions.CompositeSubscription;

import static android.os.Build.VERSION_CODES.KITKAT;
import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;


/**
 * Created by devesh on 16/03/16.
 */
public class Utils {

    public static String AREA_NAME = "area_name";
    public static String ROOM_NAME = "room_name";

    public static String ROOM_LIST = "room_list";

    public static String FORM_OBJECT = "form_object";
    public static String QUERY_OBJECT = "query_object";
    public static String ROOM_OBJECT = "room_object";
    public static String ISSUE_RESOLUTION_DATE = "resolution_date";
    public static String ISSUES_OBJECT = "issues_object";
    public static String ISSUES_RESPONSE_OBJECT = "issues_response_object";
    public static String UPDATED_ISSUE_OBJECT = "updated_issue_object";
    public static String ISSUES_LIST = "issues_list";
    public static String ISSUE_ID = "issues_id";
    public static String ISSUE_TEXT = "issues_text";
    public static String ISSUE_RESOLVE = "issues_resolve";

    public static String PHOTO_PATH = "photo_path";
    public static String PHOTO_OBJECT = "photo_object";

    public static final String ISSUE_SORT_ORDER = "issue_sort_order";

    public static String IS_FROM_AUDIT_ANY_HOTEL = "is_from_audit_any_hotel";

    public static String AUDIT_BAD = "audit_bad";
    public static String AUDIT_BAD_TITLE = "audit_bad_title";

    public static String CREATED_TASK = "created_task";
    public static String TASK_LOG_ID = "task_log_id";
    public static String ROOM_TYPE_ID = "room_type_id";
    public static String PRE_EXISTING_ISSUES = "pre_existing_issues";
    public static String IS_FROM_PRE_EXISTING_ISSUE = "is_pre_existing_issue";

    public static String HOTEL_ID = "hotel_id";
    public static String HOTEL_NAME = "hotel_name";
    public static String HOTEL_OBJECT = "hotel_object";
    public static String HOTEL_LIST = "hotel_list";
    public static String PORTFOLIO_METRICS = "portfolio_metrics";
    public static String IS_IN_HOTEL = "is_in_hotel";
    public static String GEOGRAPHICAL_DATA = "geographical_data";
    public static String IS_REGION = "is_region";
    public static String IS_CLUSTER = "is_cluster";
    public static String IS_CITY = "is_city";
    public static String IS_PROPERTY = "is_property";
    public static String IS_SEARCH_ENABLED = "is_search_enabled";
    public static String LIST_TYPE = "list_type";
    public static String SELECTED_LIST = "selected_list";
    public static String ID_LIST = "id_list";
    public static String IS_DASHBOARD_EMPTY_SCREEN_ENABLED = "is_gem_enabled";
    public static String IS_EXTRA_ROOM_AUDIT = "is_extra_room_audit";
    public static String IS_FROM_EXPLORE = "is_from_explore";
    public static String CLICKED_CHECKPOINT = "checkpoint_clicked";
    public static String CLICKED_SUBCHECKPOINT = "subcheckpoint_clicked";

    public static String PRICE_IN_HOTEL = "price_in_hotel";
    public static String PRICE_IN_TREEBO = "price_in_treebo";

    public static String ISSUE_CLICKED = "issue_clicked";
    public static String IS_INC_LIST = "is_inc_list";

    public static String START_TIME = "start_time";
    public static String DATE_STRING = "date_string";
    public static String TIME_STRING = "time_string";
    public static String SERVER_TIME = "server_time";

    public static String IS_FROM_INTERMEDIATE_SCREEN = "is_from_intermediate_screen";
    public static String IS_FROM_CREATE_ISSUE = "is_from_create_issue";

    public static String DASHBOARD_BUNDLE = "dashboard_bundle";

    public static String LAST_UPDATE_LIST = "last_update_list";
    public static String LEAVE_HISTORY_LIST = "leave_history_list";
    public static String IS_FROM_INCENTIVES = "is_from_incentives";
    public static String INCENTIVES_DEPARTMENT = "incentives_department";

    public static String ISSUES_STATUS = "issues_status";
    public static String ISSUES_TEXT = "issues_text";
    public static String FRAUD_REASON_TEXT = "fraud_reason";

    public static String PHOTO_KEY = "photo_key";
    public static String PHOTO_ID = "photo_id";
    public static String PHOTO_URL_LIST = "photo_url_save";

    public static String BUNDLE = "bundle";
    public static String URL = "url";

    public static String CHECKPOINT_POSITION = "position";

    public static String PROWL_DIR = "prowl_dir";

    /*
    Staff incentives strings
     */
    public static String PAST_MONTH_LIST = "past_months";
    public static String MONTHLY_RESPONSE_OBJECT = "monthly_object";
    public static String DEPARTMENT_ID = "dept_id";
    public static String DEPARTMENT_OBJECT = "dept_object";
    public static String DEPARTMENT_TITLE = "dept_title";
    public static String REQUIRED_MONTH = "req_month";
    public static String REQUIRED_YEAR = "req_year";
    public static String DEPARTMENT_INCENTIVE_STATUS = "dept_status";
    public static String STAFF_OBJECT = "staff_object";
    public static String TASKS_DONE_COUNT = "tasks_done_count";

    public static final String USER_NAME = "user_name";
    public static final String USER_ID = "user_id";

    public static final int OPEN_ISSUE = 1;

    public static final int ISSUE_UPDATED = 1001;
    public static final int ISSUE_RESOLVED = 1002;

    public static final int START_AUDIT = 1007;
    public static final int AUDIT_CHANGED = 1008;

    public static final int ADD_STAFF_FROM_INCENTIVES = 3199;
    public static final int ADDED_STAFF_FROM_INCENTIVES = 3200;

    public static final int SEVERITY_HIGH_LOW = 1;
    public static final int SEVERITY_LOW_HIGH = 2;
    public static final int RESOLUTION_DATE = 3;
    public static final int ISSUE_THEME = 4;
    public static final int DATE_OLD_NEW = 5;
    public static final int DATE_NEW_OLD = 6;
    public static final int SORT_BY_ROOM = 7;

    public static final String ISSUE_UPDATE = "Pending";
    public static final String ISSUE_CLOSE = "Resolved";

    public static final int RESULT_RESOLVE_ISSUE = 1311;
    public static final int RESULT_UPDATE_ISSUE = 1310;

    public static final int RESULT_INCENTIVE_GIVEN = 181;
    public static final int RESULT_EDIT_INCENTIVE = 182;

    public static final int STAFF_INCENTIVES_STARTING_YEAR = 2017;
    public static final int STAFF_INCENTIVES_STARTING_MONTH = 4;

    public static boolean equals(Object o1, Object o2) {
        if (o1 != null && o2 != null) {
            return o1.equals(o2);
        } else {
            return o1 == null && o2 == null;
        }
    }

    public static <T> ArrayList<T> createDeepCopy(ArrayList<T> list) {
        ArrayList<T> clonedList = new ArrayList<>(list.size());
        for (T t : list) {
            clonedList.add(t);
        }
        return clonedList;
    }


    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    public static void convertImageUrlToRoundedView(Context context, String url, ImageView targetImageView) {
        if (UrlValidator.getInstance().isValid(url)) {
            Glide.with(context).load(url).asBitmap().placeholder(R.drawable.audit_categories_bullet).centerCrop().into(new BitmapImageViewTarget(targetImageView) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    targetImageView.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
    }

    public static void getPaidRoundedImageDrawable(Context context, String url, ImageView targetImageView, boolean isPaid) {
        if (UrlValidator.getInstance().isValid(url)) {
            Glide.with(context).load(url).asBitmap().placeholder(isPaid ? R.drawable.grey_background_tick : R.drawable.grey_background_cross).centerCrop().into(new BitmapImageViewTarget(targetImageView) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    circularBitmapDrawable.setGravity(Gravity.LEFT | Gravity.TOP);
                    BitmapDrawable paidDrawable;
                    if (isPaid) {
                        paidDrawable = (BitmapDrawable) context.getResources().getDrawable(R.drawable.tick_incentives);
                    } else {
                        paidDrawable = (BitmapDrawable) context.getResources().getDrawable(R.drawable.ic_incentives_cross);
                    }
                    paidDrawable.setGravity(Gravity.BOTTOM | Gravity.RIGHT);
                    Drawable drawableArray[] = new Drawable[]{circularBitmapDrawable, paidDrawable};
                    LayerDrawable layerDraw = new LayerDrawable(drawableArray);
                    targetImageView.setImageDrawable(layerDraw);
                }
            });
        }
    }

    public static void getPaidRoundedImageDrawable(Context context, ImageView targetImageView, boolean isPaid) {
        LayerDrawable paidDrawable;
        if (isPaid) {
            paidDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable.grey_background_tick);
        } else {
            paidDrawable = (LayerDrawable) context.getResources().getDrawable(R.drawable.grey_background_cross);
        }
        targetImageView.setImageDrawable(paidDrawable);
    }


    public static int getPercentageColor(Context context, int value) {
        if (value >= 80) {
            return ContextCompat.getColor(context, R.color.emerald);
        } else if (value >= 60 && value < 80) {
            return ContextCompat.getColor(context, R.color.qa_yellow);
        } else {
            return ContextCompat.getColor(context, R.color.coral);
        }

    }

    public static void hideSoftKeyboard(View view) {
        if (view.getContext() != null && view.getWindowToken() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static boolean checkIfFloatHasDecimal(Double d) {
        return (d % 1 != 0);
    }


    public static int getStarResourceForValue(Double d) {
        if (d >= 3.9) {
            return R.drawable.green_star_small;
        } else if (d >= 3.5 && d < 3.9) {
            return R.drawable.star_yellow;
        } else if (d >= 0.0 && d < 3.5) {
            return R.drawable.red_star_small;
        }

        // default case
        return R.drawable.red_star_small;

    }

    public static int getStarRatingValue(Double d) {
        //Based on 7 day average period
        Log.d("double is", "" + d);

        if (d >= 3.9) {
            Log.d("greater then 3.9", "");
            return R.color.emerald;
        } else if (d >= 3.5 && d < 3.9) {
            Log.d("middle", "");
            return R.color.qa_yellow;
        } else if (d >= 0.0 && d < 3.5) {
            Log.d("last", "");
            return R.color.coral;
        }
        Log.d("default", "");
        // default case
        return R.color.emerald;

    }

    public static int getLastAuditTextColor(int days) {
        if (days > 3)
            return R.color.coral;
        else
            return R.color.emerald;
    }


    public static int getFrontOfficeScoreValue(int d) {
        if (d >= 88) {
            return R.color.emerald;
        } else if (d >= 82 && d < 88) {
            return R.color.qa_yellow;
        } else if (d >= 0.0 && d < 82) {
            return R.color.coral;
        }

        // default case
        return R.color.coral;

    }


    public static int getBackgroundDrawableForFOTAudit(Double d) {
        if (d >= 90d) {
            return R.drawable.rounded_corners_green;
        } else if (d >= 80d && d < 90d) {
            return R.drawable.rounded_corners_yellow;
        } else if (d >= 0.0d && d < 80d) {
            return R.drawable.rounded_corners_red;
        }
        return R.drawable.rounded_corners_red;
    }

    public static void persistAuthToken(LoginResponse loginResponse) {
        LoginSharedPrefManager loginSharedPrefManager = LoginSharedPrefManager.getInstance();
        if (loginResponse != null) {
            if (loginResponse.getEmailId() != null)
                loginSharedPrefManager.setEmailId(loginResponse.getEmailId());
            if (loginResponse.getUserId() > 0)
                loginSharedPrefManager.setUserId(loginResponse.getUserId());
            if (!TextUtils.isEmpty(loginResponse.getUserName()))
                loginSharedPrefManager.setUsername(loginResponse.getUserName());
            if (loginResponse.getAccessToken() != null)
                loginSharedPrefManager.setAccessToken(loginResponse.getAccessToken());
            if (loginResponse.getRefreshToken() != null)
                loginSharedPrefManager.setRefreshToken(loginResponse.getRefreshToken());
            if (loginResponse.getTokenType() != null)
                loginSharedPrefManager.setTokenType(loginResponse.getTokenType());
            if (loginResponse.getProfilePicUrl() != null)
                loginSharedPrefManager.setUserProfilePic(loginResponse.getProfilePicUrl());
            if (loginResponse.getUserRoles() != null)
                loginSharedPrefManager.setUserPermissions(loginResponse.getUserRoles());
            loginSharedPrefManager.setAccessTokenValidity(true);
        }
    }

    public static int getColorSchemeForScore(Context context, String score, String value) {
        if (value == null || value.isEmpty()) {
            return R.color.coral;
        }
        if (score.equals(context.getString(R.string.qam_audits))) {

            if (Integer.parseInt(value) > 75) {
                return R.color.color_emerald;
            } else {
                return R.color.coral;
            }
        } else if (score.equals(context.getString(R.string.blind_rooms))) {
            if (Integer.parseInt(value) < 5) {
                return R.color.color_emerald;
            } else {
                return R.color.coral;
            }
        } else if (score.equals(context.getString(R.string.completion_ratio))) {
            if (Integer.parseInt(value) > 75) {
                return R.color.color_emerald;
            } else {
                return R.color.coral;
            }
        } else {
            return R.color.color_emerald;
        }
    }

    public static int getRoomColor(String color) {
        if (color.equals("red")) {
            return R.color.coral;
        } else if (color.equals("yellow")) {
            return R.color.qa_yellow;
        } else {
            return R.color.black_100;
        }
    }

    public static int getIssueColor(String severity) {
        if (severity.equalsIgnoreCase("1")) {
            return R.color.coral;
        } else if (severity.equalsIgnoreCase("2")) {
            return R.color.qa_yellow;
        } else {
            return R.color.black_100;
        }
    }

    public static String getCurrentMonth() {
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH);
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        return months[month];
    }

    public static String getCurrentTime() {
        DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static int getCurrentDate() {
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DATE);
        return day;
    }


    public static int getBackgroundDrawableForGuestFeedback(Double d) {
        if (d >= 55d) {
            return R.drawable.rounded_corners_green;
        } else if (d >= 45d && d < 55d) {
            return R.drawable.rounded_corners_yellow;
        } else if (d >= 0d && d < 45d) {
            return R.drawable.rounded_corners_red;
        }
        return R.drawable.rounded_corners_red;

    }

    public static String getAreaCategoryString(Context context, IssuesModel issue) {
        String areaCategory;
        if (issue.category.equalsIgnoreCase("room") || issue.category.equalsIgnoreCase("bathroom") || issue.category.equalsIgnoreCase("guest room")) {
            areaCategory = context.getResources().getString(R.string.Room) + " " + issue.room_number;
        } else {
            areaCategory = issue.category;
        }
        return areaCategory;
    }


    public static ArrayList<String> getAreaCategoryRooms(Context context, IssuesModel issue) {
        ArrayList<String> areaCategory = new ArrayList<>();
        if (issue.category.equalsIgnoreCase("room") || issue.category.equalsIgnoreCase("bathroom") || issue.category.equalsIgnoreCase("guest room")) {
            String[] roomList = issue.room_number.split(",");
            for (int i = 0; i < roomList.length; i++) {
                areaCategory.add(context.getResources().getString(R.string.Room) + " " + roomList[i]);
            }
        } else {
            areaCategory.add(issue.category);
        }
        return areaCategory;
    }

    public static int getFillRateColor(double d) {
        if (d >= 40.0) {
            return R.color.color_emerald;
        } else if (d >= 25.0 && d < 40.0) {
            return R.color.qa_yellow;
        } else if (d >= 0.0 && d < 25.0) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static int getQAMOverallColor(int d) {
        if (d >= 88) {
            return R.color.color_emerald;
        } else if (d >= 82 && d < 88) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 82) {
            return R.color.coral;
        }
        return R.color.coral;
    }


    public static int getFOtColor(int d) {
        if (d >= 90) {
            return R.color.color_emerald;
        } else if (d >= 80 && d < 90) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 80) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static int getNPSColor(int d) {
        if (d >= 55) {
            return R.color.color_emerald;
        } else if (d >= 45 && d < 55) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 45) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager con = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (con.getActiveNetworkInfo() == null)
            return false;
        return con.getActiveNetworkInfo().isConnected();

    }


    public static int getBackgroundColorForGuestReviews(Double d) {
        if (d >= 3.9) {
            return R.color.color_emerald;
        } else if (d >= 3.5 && d < 3.9) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 3.5) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static int getBackgroundColorForFOTAudit(int d) {
        if (d >= 90) {
            return R.color.color_emerald;
        } else if (d >= 80 && d < 90) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 80) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static int getBackgroundColorForQAMAudit(Double d) {
        if (d >= 88d) {
            return R.color.color_emerald;
        } else if (d >= 82d && d < 88d) {
            return R.color.qa_yellow;
        } else if (d >= 0d && d < 82d) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static int getBackgroundColorForGuestFeedback(int d) {
        if (d >= 55.0) {
            return R.color.color_emerald;
        } else if (d >= 45.0 && d < 55.0) {
            return R.color.qa_yellow;
        } else if (d >= 0d && d < 45.0) {
            return R.color.coral;
        }
        return R.color.coral;

    }

    public static int getBackgroundColorForOthers(int d) {
        if (d >= 90) {
            return R.color.color_emerald;
        } else if (d >= 80 && d < 90) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 80) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static int getNewReviewsColor(Double d) {
        if (d >= 5) {
            return R.color.color_emerald;
        } else if (d >= 2 && d < 5) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 2) {
            return R.color.coral;
        }
        return R.color.coral;

    }

    public static int getColorForHouseKeeping(double d) {
        if (d >= 92) {
            return R.color.color_emerald;
        } else if (d >= 84 && d < 92) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 84) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static int getColorForFnB(double d) {
        if (d >= 90) {
            return R.color.color_emerald;
        } else if (d >= 80 && d < 90) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 80) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static int getColorForFrontReception(double d) {
        if (d >= 88) {
            return R.color.color_emerald;
        } else if (d >= 82 && d < 88) {
            return R.color.qa_yellow;
        } else if (d >= 0 && d < 82) {
            return R.color.coral;
        }
        return R.color.coral;
    }

    public static int getBackgroundColorForTQLRank(Double value1) {
        return R.color.warm_grey_2;
    }

    public static int getIndividualIncentivesColor(String startIncentive, String recievedIncentive) {
        if (startIncentive.equalsIgnoreCase("na") || recievedIncentive.equalsIgnoreCase("na")) {
            return R.color.warm_grey_2;
        } else {
            if (Float.parseFloat(startIncentive) <= Float.parseFloat(recievedIncentive)) {
                return R.color.emerald;
            } else {
                return R.color.coral;
            }
        }
    }

    public static String compress(String string) throws IOException {

        ByteArrayOutputStream os = new ByteArrayOutputStream(string.length());
        GZIPOutputStream gos = new GZIPOutputStream(os);
        gos.write(string.getBytes());
        gos.close();
        byte[] compressed = os.toByteArray();
        os.close();
        return Base64.encodeToString(compressed, Base64.DEFAULT);
    }

    public static int isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        return googleApiAvailability.isGooglePlayServicesAvailable(activity);
    }

    //status code 1, 2, 3 or 9 are user recoverable
    public static boolean isPlayServicesErrorUserRecoverable(int status) {
        return GoogleApiAvailability.getInstance().isUserResolvableError(status);
    }

    public static void showPlayServicesErrorDialog(Activity activity, int status) {
        GoogleApiAvailability.getInstance().getErrorDialog(activity, status, 2404).show();
    }

    public static ArrayList<SortByModel> getAllSortCriterion() {
        ArrayList<SortByModel> sortList = new ArrayList<>();
        SortByModel sort1 = new SortByModel();
        sort1.sortCriteriaId = SEVERITY_HIGH_LOW;
        sort1.sortTitle = "Severity: High - Low";
        sort1.isSelected = true;

        SortByModel sort2 = new SortByModel();
        sort2.sortCriteriaId = SEVERITY_LOW_HIGH;
        sort2.sortTitle = "Severity: Low - High";

        SortByModel sort3 = new SortByModel();
        sort3.sortCriteriaId = RESOLUTION_DATE;
        sort3.sortTitle = "Resolution date";

        SortByModel sort4 = new SortByModel();
        sort4.sortCriteriaId = ISSUE_THEME;
        sort4.sortTitle = "Issue theme";

        SortByModel sort5 = new SortByModel();
        sort5.sortCriteriaId = DATE_NEW_OLD;
        sort5.sortTitle = "Date Added : Newest - Oldest";

        SortByModel sort6 = new SortByModel();
        sort6.sortCriteriaId = DATE_OLD_NEW;
        sort6.sortTitle = "Date Added : Oldest - Newest";

        SortByModel sort7 = new SortByModel();
        sort7.sortCriteriaId = SORT_BY_ROOM;
        sort7.sortTitle = "Room Number";

        sortList.add(sort1);
        sortList.add(sort2);
        sortList.add(sort3);
        sortList.add(sort4);
        sortList.add(sort5);
        sortList.add(sort6);
        sortList.add(sort7);

        return sortList;
    }

    public static Set<String> getUniqueValues(ArrayList<IssueListModelV3> issueList) {
        Set<String> uniqueValues = new HashSet<String>();
        for (IssueListModelV3 issue : issueList) {
            uniqueValues.add(String.valueOf(issue.getSectionName()));
        }
        return uniqueValues;
    }

    public static String getSeverityText(int severity) {
        switch (severity) {
            case Constants.HIGH_SEVERITY:
                return "Severity: High";

            case Constants.MEDIUM_SEVERITY:
                return "Severity: Medium";

            case Constants.LOW_SEVERITY:
                return "Severity: Low";
        }

        return "";
    }

    public static Set<Map.Entry<String, Integer>> getSeverityPriorityHashMap() {
        Map<String, Integer> priorityMap = new HashMap<String, Integer>();
        for (int i = 1; i < 4; i++) {
            priorityMap.put(getSeverityText(i), i);
        }
        return priorityMap.entrySet();
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }


    public static void unSubscribe(CompositeSubscription subscription) {
        if (subscription == null || subscription.isUnsubscribed()) {
            return;
        }
        subscription.unsubscribe();
    }

    public static int getMetricDisplayColor(String colorType) {
        switch (colorType) {
            case "green":
                return R.color.green_color_primary;

            case "red":
                return R.color.warning_red;

            case "black":
                return R.color.greyish_brown;

            case "yellow":
                return R.color.warning_orange;

            default:
                return R.color.white;
        }
    }

    public static CharSequence getMetricStringWithPercent(Context context, int score,
                                                          boolean showPercent,
                                                          int textSize) {
        int textSize1 = context.getResources().getDimensionPixelSize(textSize);
        int textSize2 = context.getResources().getDimensionPixelSize(R.dimen.textsize_xsmall);
        String text1 = String.valueOf(score);
        String text2 = context.getString(R.string.percent_symbol);

        SpannableString span1 = new SpannableString(text1);
        span1.setSpan(new AbsoluteSizeSpan(textSize1), 0, text1.length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString span2 = new SpannableString(text2);
        span2.setSpan(new AbsoluteSizeSpan(textSize2), 0, text2.length(), SPAN_INCLUSIVE_INCLUSIVE);

        return TextUtils.concat(span1, "", showPercent ? span2 : " ");
    }

    public static void animateTextView(Context context, int initialValue, int finalValue,
                                       final TreeboTextView textview, boolean showPercent,
                                       int textSize) {
        DecelerateInterpolator decelerateInterpolator = new DecelerateInterpolator(0.8f);
        int start = Math.min(initialValue, finalValue);
        int end = Math.max(initialValue, finalValue);
        int difference = Math.abs(finalValue - initialValue);
        Handler handler = new Handler();
        for (int count = start; count <= end; count++) {
            int time = Math.round(decelerateInterpolator.getInterpolation((((float) count) / difference)) * 10) * count;
            final int finalCount = ((initialValue > finalValue) ? initialValue - count : count);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textview.setText(getMetricStringWithPercent(context, finalCount, showPercent, textSize));
                }
            }, time);
        }
    }

    public static CharSequence getTaskHistoryHeadingString(Context context, String text1, String userName, String hotelName) {
        userName = "by " + userName;
        String activityText = text1 + " at " + hotelName;
        SpannableString spannableString1 = new SpannableString(activityText);
        spannableString1.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context,
                R.color.text_color_primary)), 0, activityText.length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString spannableString2 = new SpannableString(userName);
        spannableString2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context,
                R.color.text_color_secondary)), 0, userName.length(), SPAN_INCLUSIVE_INCLUSIVE);

        return TextUtils.concat(spannableString1, " ", spannableString2);
    }

    public static String getTaskHistoryIssueHeadingString(String roomNumber, int subcheckpointID) {
        int checkpointID = 0, categoryID = 0;
        SubCheckpoint sb = SQLite.select().from(SubCheckpoint.class)
                .where(SubCheckpoint_Table.mSubCheckpointId.eq(subcheckpointID)).querySingle();
        if (sb != null)
            checkpointID = sb.mCheckpointId;
        Checkpoint ck = SQLite.select().from(Checkpoint.class)
                .where(Checkpoint_Table.mCheckpointId.eq(checkpointID)).querySingle();
        if (ck != null)
            categoryID = ck.mCategoryId;
        Category category = SQLite.select().from(Category.class)
                .where(Category_Table.mCategoryId.eq(categoryID)).querySingle();
        if (category != null && ck != null && sb != null)
            return roomNumber + ", " + category.mDescription + " " + ck.mDescription + "\n" + sb.mDescription;
        else
            return "";
    }

    public static int getTaskTypeFromText(String taskType) {
        switch (taskType) {
            case "Audit":
                return Constants.AUDIT_TASK;

            case "Issue":
                return Constants.ISSUE_TASK;

            case "Manual":
                return Constants.MANUAL_TASK;

            case "Periodic Audit":
                return Constants.PERIODIC_AUDIT_TASK;

            case "Staff Incentive":
                return Constants.STAFF_INCENTIVE_TASK;

            case "Bottom Dummy":
                return Constants.BOTTOM_DUMMY_TASK;

            default:
                return 0;
        }
    }

    public static String getRoomNotReadyReason(int reason) {
        switch (reason) {
            case Constants.ROOM_NOT_READY:
                return "Not Ready";

            case Constants.ROOM_OCCUPIED:
                return "Occupied";

            case Constants.OTHERS:
                return "Other";

            default:
                return "";
        }
    }

    public static Spannable applyCustomLinkSpanToUrlSpan(Context context, String text, String url) {
        Spannable spannable = new SpannableString(Html.fromHtml(text + " " + url));
        Linkify.addLinks(spannable, Linkify.WEB_URLS);
        URLSpan[] spans = spannable.getSpans(0, spannable.length(), URLSpan.class);
        for (URLSpan urlSpan : spans) {
            LinkSpan linkSpan = new LinkSpan(urlSpan.getURL(), context);
            int spanStart = spannable.getSpanStart(urlSpan);
            int spanEnd = spannable.getSpanEnd(urlSpan);
            spannable.setSpan(linkSpan, spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.removeSpan(urlSpan);
        }
        return spannable;
    }

    public static Drawable getTintedDrawable(@NonNull Context context, @NonNull Drawable inputDrawable, @ColorInt int color) {
        Drawable wrapDrawable = DrawableCompat.wrap(inputDrawable);
        DrawableCompat.setTint(wrapDrawable, color);
        DrawableCompat.setTintMode(wrapDrawable, PorterDuff.Mode.SRC_IN);
        return wrapDrawable;
    }

    public static ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> getPastMonths(int lastMonth, int year) {
        ArrayList<MonthlyIncentiveListResponse.MonthlyIncentiveListObject> list = new ArrayList<>();

        int count = 0;
        int MAX_COUNT = 5;
        if (year == STAFF_INCENTIVES_STARTING_YEAR) {
            for (int i = lastMonth; count < MAX_COUNT; i--) {
                if (i >= STAFF_INCENTIVES_STARTING_MONTH) {
                    MonthlyIncentiveListResponse.MonthlyIncentiveListObject object =
                            new MonthlyIncentiveListResponse.MonthlyIncentiveListObject();
                    object.setMonth(i);
                    object.setYear(year);
                    object.setStatus("history");
                    list.add(object);
                    count++;
                } else {
                    break;
                }
            }
        } else {
            for (int i = lastMonth; count < MAX_COUNT; i--) {
                MonthlyIncentiveListResponse.MonthlyIncentiveListObject object =
                        new MonthlyIncentiveListResponse.MonthlyIncentiveListObject();
                if (i <= 0) {
                    object.setMonth(12 + i);
                    object.setYear(year - 1);
                } else {
                    object.setMonth(i);
                    object.setYear(year);
                }
                object.setStatus("history");
                list.add(object);
                count++;
            }

        }

        return list;
    }

    public static int getDeltaColor(Context context, int delta) {
        if (delta < 0) {
            return ContextCompat.getColor(context, R.color.warning_red);
        } else if (delta > 0) {
            return ContextCompat.getColor(context, R.color.green_color_primary);
        } else {
            return ContextCompat.getColor(context, R.color.greyish_brown);
        }
    }

    public static void loadRoundedImageUsingGlide(Context context, String url,
                                                  ImageView imageView, int placeHolder) {
        if (Build.VERSION.SDK_INT <= KITKAT) {
            Glide.with(context).load(url)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new BitmapImageViewTarget(imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                        }
                    });

        } else {
            Glide.with(context).load(url)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(ContextCompat.getDrawable(context, placeHolder))
                    .error(ContextCompat.getDrawable(context, placeHolder))
                    .into(new BitmapImageViewTarget(imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        }
    }

}
