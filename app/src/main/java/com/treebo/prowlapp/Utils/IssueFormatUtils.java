package com.treebo.prowlapp.Utils;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import android.text.TextUtils;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.Models.auditmodel.AuditCategory;
import com.treebo.prowlapp.Models.issuemodel.IssueDetailsV3;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by sumandas on 18/11/2016.
 */

public class IssueFormatUtils {

    public static String getActionTitle(IssueDetailsV3.IssueLog issueLog) {
        String actionTitle = "";
        if (issueLog.mReview == null) {//if reviews is null will be a comment
            if (!TextUtils.isEmpty(issueLog.mIssueComments)) {//comment log
                actionTitle = "added " + (issueLog.isUserFirstAction ? " a " : " another ") + " comment ";
            }
            if (issueLog.mImageUrls != null && !issueLog.mImageUrls.isEmpty()) {//photos added
                String photoString = issueLog.mImageUrls.size() + (issueLog.mImageUrls.size() == 1 ? " photo" : " photos");
                photoString = (TextUtils.isEmpty(issueLog.mIssueComments) ? " added " : " & ") + photoString;
                actionTitle = actionTitle + photoString;
            }
            if (!TextUtils.isEmpty(issueLog.mIssueResolutionDate)) {
                String resolutionString = (actionTitle.isEmpty() ? "" : " & ") + " updated resolution date to";
                actionTitle = actionTitle + resolutionString;
            }
            actionTitle = issueLog.mUserName + " " + actionTitle;
        } else {
            actionTitle = issueLog.mIssueSource + " identified the same issue";
        }
        return actionTitle;
    }

    public static String getCreatedDate(IssueDetailsV3 issueDetailsV3) {
        return issueDetailsV3.mIssueCreatedDate;
    }

    public static String getIssueUpdateDate(IssueDetailsV3.IssueLog issueLog) {
        return DateUtils.getDisplayDateFromTimeStamp(issueLog.mIssueUpdateDate);
    }


    public static String getReviewTitleOrResolutionDate(IssueDetailsV3.IssueLog issueLog) {
        if (issueLog.mReview == null) {
            if (!TextUtils.isEmpty(issueLog.mIssueResolutionDate)) {
                return DateUtils.getDisplayDateFromTimeStamp(issueLog.mIssueResolutionDate);
            }
            return "";
        } else {
            return issueLog.mReview.mTitle;
        }
    }

    public static String getCommentOrReviewText(IssueDetailsV3.IssueLog issueLog) {
        if (issueLog.mReview == null) {
            if (issueLog.mIssueComments == null) {
                return "";
            } else {
                return issueLog.mIssueComments;
            }
        } else {
            return issueLog.mReview.mText;
        }
    }

    public static String getAllSources(IssueDetailsV3 issueDetails) {
        Set<String> sourceSet = new HashSet<String>();
        for (IssueDetailsV3.IssueLog issueLog : issueDetails.mIssueLog) {
            sourceSet.add(issueLog.mIssueSource);
        }
        String[] sources = new String[sourceSet.size()];
        sourceSet.toArray(sources);
        return TextUtils.join(",", sources);
    }

    public static int getSeverityColor(Context context, int severity) {
        switch (severity) {
            case Constants.HIGH_SEVERITY:
                return ContextCompat.getColor(context, R.color.severe_high);
            case Constants.MEDIUM_SEVERITY:
                return ContextCompat.getColor(context, R.color.severe_medium);
            case Constants.LOW_SEVERITY:
            default:
                return ContextCompat.getColor(context, R.color.severe_low);
        }
    }


    public static String getAreaNameCheck(IssueListModelV3 issueListModelV3) {
        Context context = MainApplication.getAppContext();
        switch (AuditCategory.getAuditCategoryConstantByID(issueListModelV3.getAuditCategoryID())) {
            case Constants.ROOM_AUDIT_STRING:
                return context.getString(R.string.room_number_placeholder, issueListModelV3.getRoomNumber());

            case Constants.COMMON_AREA_AUDIT_STRING:
                return context.getString(R.string.common_area);

            case Constants.BREAKFAST_AUDIT_STRING:
                return context.getString(R.string.breakfast_issue);

            case Constants.FRAUD_AUDIT_STRING:
                return context.getString(R.string.fraud_issue);

            case Constants.KITCHEN_PANTRY_AUDIT_STRING:
                return context.getString(R.string.kitchen_pantry_issue);

            case Constants.FnB_AUDIT_STRING:
                return context.getString(R.string.fnb_issue);

            case Constants.RESTAURANT_AUDIT_STRING:
                return context.getString(R.string.restaurant_issue);

            case Constants.RESTAURANT_WASHROOM_AUDIT_STRING:
                return context.getString(R.string.restaurant_washroom_issue);

            case Constants.BACK_OF_THE_HOUSE_AUDIT_STRING:
                return context.getString(R.string.back_of_the_house_issue);

            case Constants.GYM_AUDIT_STRING:
                return context.getString(R.string.gym_issue);

            case Constants.SPA_AUDIT_STRING:
                return context.getString(R.string.spa_issue);

            case Constants.POOL_AUDIT_STRING:
                return context.getString(R.string.pool_issue);

            case Constants.POLICY_AND_CONDUCT_AUDIT_STRING:
                return context.getString(R.string.policy_conduct_issue);

            case Constants.ACCO_FOOD_AUDIT_STRING:
                return context.getString(R.string.acco_food_issue);

            case Constants.STORAGE_SPACE_AUDIT_STRING:
                return context.getString(R.string.storage_space_issue);

            case Constants.SAFETY_AUDIT_STRING:
                return context.getString(R.string.safety_issue);

            case Constants.GARBAGE_MANAGEMENT_AUDIT_STRING:
                return context.getString(R.string.garbage_management_issue);

            case Constants.WATER_STORAGE_AUDIT_STRING:
                return context.getString(R.string.water_storage_issue);

            case Constants.DISABLED_FRIENDLY_AUDIT_STRING:
                return context.getString(R.string.disabled_friendly_issue);

            case Constants.HK_TOOLS_AUDIT_STRING:
                return context.getString(R.string.house_keeping_tools_issue);

            case Constants.FRONT_OFFICE_AUDIT_STRING:
                return context.getString(R.string.front_office_tools_issue);

            case Constants.LUXURY_AMENITIES_AUDIT_STRING:
                return context.getString(R.string.luxury_amenities_issue);

            case Constants.CLOAK_ROOM_AUDIT_STRING:
                return context.getString(R.string.cloak_room_issue);

            case Constants.DRIVERS_QUARTERS_AUDIT_STRING:
                return context.getString(R.string.drivers_quarters_issue);

            case Constants.PARKING_AREA_STRING:
                return context.getString(R.string.parking_areas_issue);

            case Constants.IN_HOUSE_LAUNDRY_AUDIT_STRING:
                return context.getString(R.string.in_house_laundry_issue);

            case Constants.SERVICE_AUDIT_STRING:
                return context.getString(R.string.service_issue);

            default:
                return context.getString(R.string.common_area);

        }
    }

    public static Drawable getTintedIconForAuditType(Context context, int auditCategoryID, int tintColor) {
        switch (AuditCategory.getAuditCategoryConstantByID(auditCategoryID)) {
            case Constants.ROOM_AUDIT_STRING:
                if (tintColor == R.color.green_color_primary)
                    return ContextCompat.getDrawable(context, R.drawable.ic_recommended_rooms);
                else
                    return ContextCompat.getDrawable(context, R.drawable.ic_room);

            case Constants.BREAKFAST_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_breakfast_audit, tintColor);

            case Constants.FRAUD_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_fraud_room, tintColor);

            case Constants.KITCHEN_PANTRY_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_kitchen, tintColor);

            case Constants.FnB_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_food_beverage_audit, tintColor);

            case Constants.RESTAURANT_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_food_beverage_audit, tintColor);

            case Constants.RESTAURANT_WASHROOM_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_washroom, tintColor);

            case Constants.BACK_OF_THE_HOUSE_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_back_of_the_house, tintColor);

            case Constants.GYM_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_gym, tintColor);

            case Constants.SPA_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_spa, tintColor);

            case Constants.POOL_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_pool, tintColor);

            case Constants.POLICY_AND_CONDUCT_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_hr_audit, tintColor);

            case Constants.ACCO_FOOD_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_accomodation, tintColor);

            case Constants.STORAGE_SPACE_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_storage, tintColor);

            case Constants.SAFETY_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_fire_safety_audit, tintColor);

            case Constants.GARBAGE_MANAGEMENT_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_garbage, tintColor);

            case Constants.WATER_STORAGE_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_water_storage, tintColor);

            case Constants.DISABLED_FRIENDLY_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_disabled_friendly, tintColor);

            case Constants.HK_TOOLS_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_house_keeping, tintColor);

            case Constants.FRONT_OFFICE_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_front_office_tools, tintColor);

            case Constants.LUXURY_AMENITIES_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_luxury_amenities, tintColor);

            case Constants.CLOAK_ROOM_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_other_amenities, tintColor);

            case Constants.DRIVERS_QUARTERS_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_driver_quarters, tintColor);

            case Constants.PARKING_AREA_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_parking, tintColor);

            case Constants.IN_HOUSE_LAUNDRY_AUDIT_STRING:
                return getTintedDrawableOfColorResId(context, R.drawable.ic_inhouse_laundry, tintColor);

            case Constants.COMMON_AREA_AUDIT_STRING:
            default:
                if (tintColor == R.color.green_color_primary)
                    return ContextCompat.getDrawable(context, R.drawable.ic_common_area);
                else
                    return ContextCompat.getDrawable(context, R.drawable.ic_common_area_gray);

        }
    }

    public static boolean isAuditRoomOrCommon(int auditCategoryID) {
        switch (AuditCategory.getAuditCategoryConstantByID(auditCategoryID)) {
            case Constants.ROOM_AUDIT_STRING:
            case Constants.COMMON_AREA_AUDIT_STRING:
                return true;

            default:
                return false;
        }
    }

    private static Drawable getTintedDrawableOfColorResId(@NonNull Context context, int drawableResID, @ColorRes int colorResId) {
        return getTintedDrawable(context, ContextCompat.getDrawable(context, drawableResID), ContextCompat.getColor(context, colorResId));
    }

    public static Drawable getTintedDrawable(@NonNull Context context, @NonNull Drawable inputDrawable, @ColorInt int color) {
        Drawable wrapDrawable = DrawableCompat.wrap(inputDrawable);
        DrawableCompat.setTint(wrapDrawable, color);
        DrawableCompat.setTintMode(wrapDrawable, PorterDuff.Mode.SRC_IN);
        return wrapDrawable;
    }

}
