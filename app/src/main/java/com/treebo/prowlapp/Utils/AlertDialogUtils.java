package com.treebo.prowlapp.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;

/**
 * Created by devesh on 12/04/16.
 */
public class AlertDialogUtils {
    public static void showInternetDisconnectedDialog(final Activity activity, DialogInterface.OnClickListener retryButtonListener , DialogInterface.OnCancelListener listerner)  {
        if(activity.isFinishing()){
            return;
        }

        if (retryButtonListener == null) {
            retryButtonListener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
        }
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(activity);
        }


                builder
                .setTitle(Constants.NETWORK_ERROR)
                .setMessage(Constants.CANNOT_CONNECT_TO_SERVER)
                .setPositiveButton(Constants.RETRY, retryButtonListener)
                .setCancelable(false)
                .setNegativeButton(Constants.CANCEL, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setOnCancelListener(listerner)
                .show();
    }

    public static void showConfirmDialog(final Activity activity, String title, String message, DialogInterface.OnClickListener positiveButtonListener)  {
        if (positiveButtonListener == null) {
            positiveButtonListener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            };
        }
        if(activity.isFinishing()){
            return;
        }

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(activity);
        }

                builder
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, positiveButtonListener)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


}

