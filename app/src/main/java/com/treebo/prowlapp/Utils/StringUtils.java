package com.treebo.prowlapp.Utils;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by devesh on 27/04/16.
 */
public class StringUtils {


    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static Pattern IFSC_REGEX = Pattern.compile("^[A-Za-z]{4}[0]\\S{6}$", Pattern.CASE_INSENSITIVE);

    public static String removeLastChar(String s) {
        if (!TextUtils.isEmpty(s)) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    public static String convertToCSV(ArrayList<String> strings) {

        String csvString = "";
        for (String str : strings) {
            if (str.length() >= 1) {
                csvString += str + ",";
            }
        }

        if (csvString.length() >= 1) {
            csvString = csvString.substring(0, csvString.length() - 1);
        }
        return csvString;
    }

    public static String trimAndUpperCase(@Nullable String s, int n) {
        if (TextUtils.isEmpty(s)) {
            return "";
        }
        if (s.length() > n) {
            s = s.substring(0, n);
        }
        return s.toUpperCase();
    }

    public static boolean equals(String s1, String s2) {
        if (s1 == null || s2 == null) {
            return false;
        }

        return s1.equalsIgnoreCase(s2);
    }

    public static boolean equals(Object s1, Object s2) {
        if (s1 == null || s2 == null) {
            return false;
        }
        return s1.toString().equalsIgnoreCase(s2.toString());
    }

    public static boolean equalsJson(Object s1, Object s2) {
        if (s1 == null || s2 == null) {
            return false;
        }
        Gson gson = new Gson();
        String json1 = gson.toJson(s1);
        String json2 = gson.toJson(s2);
        return json1.equalsIgnoreCase(json2);
    }

    public static boolean validatePhoneNumber(String phoneNumber) {
        if (phoneNumber == null) {
            return false;

        }
        return phoneNumber.matches("(\\d){10}");
    }

    public static boolean validatePin(String pin) {
        if (pin == null) {
            return false;

        }
        return pin.matches("(\\d){4}");
    }

    public static boolean validatePinCode(String pinCode) {
        if (pinCode == null) {
            return false;
        }
        return pinCode.matches("(\\d){6}");
    }

    public static boolean validateEmail(String emailId) {

        if (emailId != null) {
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailId);
            return matcher.find();
        }

        return false;
    }

    public static boolean validateIfscCode(String ifscCode) {
        return ifscCode != null && IFSC_REGEX.matcher(ifscCode).matches();
    }

    public static boolean validateBankAccountNumber(@NonNull String validatorPattern,
                                                    String accountNumber) {
        try {
            Pattern accountNumberPattern = Pattern.compile(validatorPattern);
            return !TextUtils.isEmpty(accountNumber) && accountNumberPattern.matcher(accountNumber).matches();
        } catch (PatternSyntaxException exception) {
            return false;
        }
    }

    public static boolean doesArrayListHaveMoreItems(ArrayList<String> list) {
        if (list == null || list.size() == 0) {
            return false;
        }
        return !(list.size() == 1 && list.get(0).trim().length() == 0);

    }

    public static SpannableStringBuilder boldAndColorSpan(@ColorInt int color, CharSequence text, String subSequence) {
        if (TextUtils.isEmpty(text) || TextUtils.isEmpty(subSequence)) {
            return null;
        }
        int subSequenceLength = subSequence.length();
        int subSequenceStart = text.toString().indexOf(subSequence);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        if (subSequenceStart != -1) {
            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
            spannableStringBuilder.setSpan(boldSpan, subSequenceStart, subSequenceStart + subSequenceLength, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            if (color != -1) {
                ForegroundColorSpan colorSpan = new ForegroundColorSpan(color);
                spannableStringBuilder.setSpan(colorSpan, subSequenceStart, subSequenceStart + subSequenceLength, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
        return spannableStringBuilder;
    }

    public static SpannableStringBuilder italicAndColorSpan(@ColorInt int color, CharSequence text, String subSequence) {
        if (TextUtils.isEmpty(text) || TextUtils.isEmpty(subSequence)) {
            return null;
        }
        int subSequenceLength = subSequence.length();
        int subSequenceStart = text.toString().indexOf(subSequence);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        if (subSequenceStart != -1) {
            StyleSpan styleSpan = new StyleSpan(Typeface.ITALIC);
            spannableStringBuilder.setSpan(styleSpan, subSequenceStart, subSequenceStart + subSequenceLength, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            if (color != -1) {
                ForegroundColorSpan colorSpan = new ForegroundColorSpan(color);
                spannableStringBuilder.setSpan(colorSpan, subSequenceStart, subSequenceStart + subSequenceLength, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
        return spannableStringBuilder;
    }


    /**
     * From source of WordUtils
     */

    public static String capitalizeFully(String str, final char... delimiters) {
        final int delimLen = delimiters == null ? -1 : delimiters.length;
        if (TextUtils.isEmpty(str) || delimLen == 0) {
            return str;
        }
        str = str.toLowerCase();
        return capitalize(str, delimiters);
    }

    public static String capitalizeFully(final String str) {
        return capitalizeFully(str, null);
    }


    public static String capitalize(final String str, final char... delimiters) {
        final int delimLen = delimiters == null ? -1 : delimiters.length;
        if (TextUtils.isEmpty(str) || delimLen == 0) {
            return str;
        }
        final char[] buffer = str.toCharArray();
        boolean capitalizeNext = true;
        for (int i = 0; i < buffer.length; i++) {
            final char ch = buffer[i];
            if (isDelimiter(ch, delimiters)) {
                capitalizeNext = true;
            } else if (capitalizeNext) {
                buffer[i] = Character.toTitleCase(ch);
                capitalizeNext = false;
            }
        }
        return new String(buffer);
    }


    /**
     * Is the character a delimiter.
     *
     * @param ch         the character to check
     * @param delimiters the delimiters
     * @return true if it is a delimiter
     */
    private static boolean isDelimiter(final char ch, final char[] delimiters) {
        if (delimiters == null) {
            return Character.isWhitespace(ch);
        }
        for (final char delimiter : delimiters) {
            if (ch == delimiter) {
                return true;
            }
        }
        return false;
    }

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

    public static String addRupeeSymbol(String amount) {
        if (!amount.equalsIgnoreCase("na")) {
            return "₹" + amount;
        } else {
            return amount;
        }
    }

}
