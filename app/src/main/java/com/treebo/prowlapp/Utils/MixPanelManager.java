package com.treebo.prowlapp.Utils;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.treebo.prowlapp.pushMessaging.services.AppUpdateService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sumandas on 04/06/2016.
 */
@Deprecated
public class MixPanelManager {

    public static final String SCREEN="screen";
    public static final String TIMESTAMP="timestamp";
    public static final String NETWORK="network";
    public static final String HOTEL_ID="HotelID";
    public static final String HOTEL_NAME="HotelName";
    public static final String ROOM_NO="RoomNo";

    public static final String APP_VERSION="App Version";


    public static final String NETWORK_AVAILABLE="available";
    public static final String NETWORK_NOT_AVAILABLE="unavailable";

    public static final String STATUS="status";

    public static final String EVENT_LOGIN_ATTEMPT="LoginAttempt";
    public static final String EVENT_LOGIN_SUCCESS="LoginSuccess";
    public static final String EVENT_LOGIN_FAILURE="LoginFailure";
    public static final String EVENT_LOGOUT_ATTEMPT="LogoutAttempt";
    public static final String EVENT_LOGOUT_SUCCESS="LogoutSuccess";

    public static final String EVENT_PORTFOLIO_VIEW_TAP="PortfolioViewTap";
    public static final String EVENT_PORTFOLIO_VIEW_LOAD="PortfolioViewLoad";

    public static final String EVENT_SCORE_TAP="ScoresTap";
    public static final String EVENT_SCORE_LOAD="ScoresLoad";

    public static final String EVENT_STAFF_TAP="StaffTap";

    public static final String EVENT_INCENTIVES_TAP="IncentivesTap";

    public static final String EVENT_AUDITS_TAP="AuditsTap";
    public static final String EVENT_AUDITS_LOAD="AuditsLoad";
    public static final String EVENT_AUDITS_ERROR="AuditError";

    public static final String EVENT_AUDITS_COMMON_TAP="CommonTap";
    public static final String EVENT_AUDITS_COMMON_LOAD="CommonLoad";
    public static final String EVENT_AUDITS_COMMON_CLEAR_TAP="CommonClearTap";
    public static final String EVENT_AUDITS_COMMON_SUBMIT_TAP="CommonSubmitTap";
    public static final String EVENT_AUDITS_COMMON_SUBMIT_OFFLINE_TAP="CommonSubmitOfflineTap";
    public static final String EVENT_AUDITS_COMMON_SUBMIT_DONE="CommonSubmitDone";
    public static final String EVENT_AUDITS_COMMON_SUBMIT_FAIL="CommonSubmitFail";

    public static final String EVENT_AUDITS_ROOM_TAP="RoomTap";
    public static final String EVENT_AUDITS_ROOM_LOAD="RoomLoad";
    public static final String EVENT_AUDITS_ROOM_NUMBER_TAP="RoomNumberTap";
    public static final String EVENT_AUDITS_ROOM_CLEAR_TAP="RoomClearTap";
    public static final String EVENT_AUDITS_ROOM_SUBMIT_TAP="RoomSubmitTap";
    public static final String EVENT_AUDITS_ROOM_SUBMIT_OFFLINE_TAP="RoomSubmitOfflineTap";
    public static final String EVENT_AUDITS_ROOM_SUBMIT_DONE="RoomSubmitDone";
    public static final String EVENT_AUDITS_ROOM_SUBMIT_FAIL="RoomSubmitFail";

    public static final String EVENT_ISSUES_TAP="IssuesTap";
    public static final String EVENT_ISSUES_LOAD="IssuesLoad";
    public static final String EVENT_ISSUES_SELECT_TAP="IssuesSelectTap";

    public static final String EVENT_UPDATE_INFO_TAP="UpdateInfoTap";
    public static final String EVENT_UPDATE_INFO_DONE="UpdateInfoDone";

    public static final String EVENT_MARK_RESOLVED_TAP="MarkResolvedTap";
    public static final String EVENT_MARK_RESOLVED_DONE="MarkResolvedDone";

    public static final String EVENT_APP_UPDATED="AppUpdated";
    public static final String EVENT_UPDATE_DOWNLOADED="AppUpdateDownloaded";
    public static final String EVENT_UPDATE_NOT_DOWNLOADED="AppUpdateNotDownloaded";
    public static final String EVENT_APP_OUTDATED="AppOutdated";
    public static final String EVENT_APP_UPDATE_DIALOG_SHOWN="AppUpdateDialogShown";
    public static final String EVENT_APP_UPDATE__FILE_MOVED="AppUpdateFileMoved";
    public static final String EVENT_REQUEST_STORAGE_PERMISSION="RequestStorageOermission";

    public static final String SCREEN_LOGIN="Login Screen";
    public static final String SCREEN_HOME="Home Screen";
    public static final String SCREEN_PORTFOLIO_SCORES="Portfolio Scores";

    public static final String SCREEN_ALL_HOTELS_BOTTOM_SHEET="All Hotels,Bottom Sheet";
    public static final String SCREEN_SCORES="Scores Screen";
    public static final String SCREEN_AUDITS="Audit Screen";
    public static final String SCREEN_COMMON_AUDITS="Common Audit Screen";
    public static final String SCREEN_ROOM_AUDITS="Room Audit Screen";
    public static final String SCREEN_ROOM_LIST="Room List Screen";

    public static final String SCREEN_ISSUES_LIST="Issues Screen";

    public static final String SCREEN_ISSUES_DETAILS="Issue Details Screen";

    public static final String SCREEN_ISSUES_UPDATE="Update Issue Screen";
    public static final String SCREEN_ISSUES_RESOLVED="Resolve Issue Screen";

    public static final String APP_UPDATE_SERVICE = AppUpdateService.class.getSimpleName();

    public static void trackEvent(MixpanelAPI mixpanelAPI,String screenName, String event){
        JSONObject props = new JSONObject();
        try {
            props.put(MixPanelManager.SCREEN, screenName);
            props.put(MixPanelManager.TIMESTAMP, Utilities.giveCurrentTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track(event, props);
    }

    public static void trackStatusEvent(MixpanelAPI mixpanelAPI,String screenName, String event,String status){
        JSONObject props = new JSONObject();
        try {
            props.put(MixPanelManager.SCREEN, screenName);
            props.put(MixPanelManager.TIMESTAMP, Utilities.giveCurrentTime());
            props.put(MixPanelManager.STATUS, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track(event, props);
    }


    public static void trackAuditEvent(MixpanelAPI mixpanelAPI,String screenName,
                                       String event,String hotelId ,String hotelName){
        JSONObject props = new JSONObject();
        try {
            props.put(MixPanelManager.SCREEN, screenName);
            props.put(MixPanelManager.HOTEL_ID,hotelId);
            props.put(MixPanelManager.HOTEL_NAME,hotelName);
            props.put(MixPanelManager.TIMESTAMP, Utilities.giveCurrentTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track(event, props);
    }

    public static void trackAuditStatusEvent(MixpanelAPI mixpanelAPI,String screenName,
                                       String event,String hotelId ,String hotelName,String status){
        JSONObject props = new JSONObject();
        try {
            props.put(MixPanelManager.SCREEN, screenName);
            props.put(MixPanelManager.HOTEL_ID,hotelId);
            props.put(MixPanelManager.HOTEL_NAME,hotelName);
            props.put(MixPanelManager.STATUS,status);
            props.put(MixPanelManager.TIMESTAMP, Utilities.giveCurrentTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track(event, props);
    }

    public static void trackAuditEvent(MixpanelAPI mixpanelAPI,String screenName,
                                       String event, String hotelId,String hotelName ,String roomNo){
        JSONObject props = new JSONObject();
        try {
            props.put(MixPanelManager.SCREEN, screenName);
            props.put(MixPanelManager.HOTEL_ID,hotelId);
            props.put(MixPanelManager.HOTEL_NAME,hotelName);
            props.put(MixPanelManager.ROOM_NO,roomNo);
            props.put(MixPanelManager.TIMESTAMP, Utilities.giveCurrentTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track(event, props);
    }

    public static void trackAuditStatusEvent(MixpanelAPI mixpanelAPI,String screenName,
                                             String event,String hotelId ,String hotelName,String roomNo,
                                             String status){
        JSONObject props = new JSONObject();
        try {
            props.put(MixPanelManager.SCREEN, screenName);
            props.put(MixPanelManager.HOTEL_ID,hotelId);
            props.put(MixPanelManager.HOTEL_NAME,hotelName);
            props.put(MixPanelManager.ROOM_NO,roomNo);
            props.put(MixPanelManager.STATUS,status);
            props.put(MixPanelManager.TIMESTAMP, Utilities.giveCurrentTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track(event, props);
    }

    public static void trackAppUpdateEvent(MixpanelAPI mixpanelAPI,String screenName, String event,String appVersion){
        JSONObject props = new JSONObject();
        try {
            props.put(MixPanelManager.SCREEN, screenName);
            props.put(MixPanelManager.TIMESTAMP, Utilities.giveCurrentTime());
            props.put(MixPanelManager.APP_VERSION, appVersion);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanelAPI.track(event, props);
    }


    public static void registerUserNameEmail(MixpanelAPI mixpanelAPI,int userName,String userEmail){
        JSONObject props = new JSONObject();
        try {
            props.put("user id", userName);
            props.put("user email", userEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mixpanelAPI.registerSuperProperties(props);
    }



}
