package com.treebo.prowlapp.Utils;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.treebo.prowlapp.Models.auditmodel.Category;
import com.treebo.prowlapp.Models.auditmodel.Category_Table;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint_Table;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint;
import com.treebo.prowlapp.Models.auditmodel.SubCheckpoint_Table;

/**
 * Created by abhisheknair on 15/11/16.
 */

public class DatabaseUtils {

    public static String getSubCheckpointText(int subCheckpointID) {
        SubCheckpoint sb = SQLite.select().from(SubCheckpoint.class)
                .where(SubCheckpoint_Table.mSubCheckpointId.eq(subCheckpointID)).querySingle();
        if (sb != null)
            return sb.mDescription;
        else
            return "";
    }

    public static String getCheckpointText(int checkpointID) {
        Checkpoint checkpoint = SQLite.select().from(Checkpoint.class)
                .where(Checkpoint_Table.mCheckpointId.eq(checkpointID)).querySingle();
        if (checkpoint != null)
            return checkpoint.mDescription;
        else
            return "";
    }

    public static String getCategoryText(int categoryID) {
        Category category = SQLite.select().from(Category.class)
                .where(Category_Table.mCategoryId.eq(categoryID)).querySingle();
        if (category != null)
            return category.mDescription;
        else
            return "";
    }
}
