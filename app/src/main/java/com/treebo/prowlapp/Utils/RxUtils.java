package com.treebo.prowlapp.Utils;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sumandas on 22/11/2016.
 */

public class RxUtils {

    public static <T> Observable<T> build(Observable<T> observable) {
        return observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public static <T> Observable<T> buildTest(Observable<T> observable) {
        return observable
                .observeOn(Schedulers.immediate())
                .subscribeOn(Schedulers.immediate());
    }
}
