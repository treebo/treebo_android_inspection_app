package com.treebo.prowlapp.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.style.URLSpan;
import android.view.View;

import com.treebo.prowlapp.flowdashboard.activity.WebViewActivity;

/**
 * Created by abhisheknair on 22/12/16.
 */

@SuppressLint("ParcelCreator")
public class LinkSpan extends URLSpan {
    private Context mContext;

    public LinkSpan(String url, Context mContext) {
        super(url);
        this.mContext = mContext;
    }

    @Override
    public void onClick(View view) {
        String url = getURL();
        if (url != null) {
            mContext.startActivity(new Intent(mContext, WebViewActivity.class)
                    .putExtra(Utils.URL, url));

        }
    }
}
