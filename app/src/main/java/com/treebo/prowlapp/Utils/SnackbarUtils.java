package com.treebo.prowlapp.Utils;

import android.view.View;

import com.google.android.material.snackbar.Snackbar;

/**
 * Created by devesh on 27/04/16.
 */

public class SnackbarUtils {

    public static void show(View view, CharSequence text) {
        try {
            Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void show(View view, int textRes) {
        try {
            show(view, view.getResources().getText(textRes));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}