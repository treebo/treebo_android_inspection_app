package com.treebo.prowlapp.Utils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.mvpviews.BaseView;

import java.net.URL;
import java.util.Date;

/**
 * Created by kuliza123 on 19/6/15.
 */
public class AmazonS3UploadTask extends AsyncTask<String, Integer, Object> {

    private Context mContext;
    private String mFilePath;
    private CallBackData mCallBackData;
    private String randomNumber;
    private BaseView mBaseView;

    public AmazonS3UploadTask(Context context, String filePath,CallBackData callbackData) {
        mContext = context;
        mFilePath = filePath;
        mCallBackData=callbackData;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Utilities.displayProgressDialog(mContext, "Uploading", false);
    }

    @Override
    protected Object doInBackground(String... urls) {
        AmazonS3Client s3Client = AmazonS3ClientCredentials.getAmazonImageUploadInstance();
        randomNumber = Utilities.createRandomNumber();
        Object obj = null;
        try {
            PutObjectRequest por = new PutObjectRequest(BuildConfig.AWS_BUCKET_NAME, randomNumber + ".jpeg", new java.io.File(mFilePath));
            obj = s3Client.putObject(por);
        } catch (AmazonClientException e) {
            e.printStackTrace();
        }
        return obj;

    }


    protected void onPostExecute(Object result) {
        if (!((Activity) mContext).isFinishing())
            Utilities.cancelProgressDialog();
        if (result != null) {
            System.out.println(result);
            AmazonS3Client s3Client = AmazonS3ClientCredentials.getAmazonImageUploadInstance();
            ResponseHeaderOverrides override = new ResponseHeaderOverrides();
            override.setContentType("image/jpeg");
            GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(BuildConfig.AWS_BUCKET_NAME, randomNumber + ".jpeg");
            urlRequest.setExpiration(new Date(System.currentTimeMillis() + 3600000 * 24 * 30 * 12 * 100));  // Added an 100 years worth of milliseconds to the current time.

            urlRequest.setResponseHeaders(override);
            URL url = s3Client.generatePresignedUrl(urlRequest);
            mCallBackData.onSuccess(url.toString());
        } else {
            mCallBackData.onError("Something went wrong");
        }
    }

    public interface CallBackData {

        // store result of success
        void onSuccess(String CloudUrl);

        // store result for failure
        void onError(Object result);
    }
}