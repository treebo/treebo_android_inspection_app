package com.treebo.prowlapp.Utils;

/**
 * Created by kuliza123 on 4/8/15.
 */

public class Constants {

    public static final String VERSION_V3 = "v3";

    public static final int NUM_RETRIES = 3;
    public static final int MAX_NUMBER_OF_CURRENT_HOTELS = 3;
    public static final int RESOLVABLE_TAT = 0;

    public static final String STATUS_SUCCESS = "success";
    public static final String TOAST_INTERNAL_SERVER_ERROR = "Internet Connection Error";

    public static final String CANCEL = "CANCEL";
    public static final String PENDING = "Pending";


    public static final int RandomNumberMaximumLimit = 100000;
    public static final int RandomNumberMinimumLimit = 10000;

    public static final int REFRESH_TOKEN_FAILED = 115;
    public static final String PORTFOLIO = "Portfolio";

    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String NO_EMPLOYEE_ERROR_MESSAGE = "No employees found for this department";

    public static final String ERROR_MESSAGE_FIRST_NAME_EMPTY = "first name cannot be empty";
    public static final String ERROR_MESSAGE_LAST_NAME_EMPTY = "last name cannot be mpty";
    public static final String ERROR_MESSAGE_SALARY_MISSING = "salary cannot be empty";
    public static final String ERROR_MESSAGE_PHONE_NUMBER_EMPTY = "phone number cannot be empty";
    public static final String ERROR_MESSAGE_PHONE_NUMBER_INVALID = "phone number invalid";
    public static final String ERROR_MESSAGE_PINCODE_INVALID = "Please Enter 6 digit Pincode";
    public static final String ERROR_MESSAGE_CANNOT_EDIT_JOINING_DATE = "joining date cannot be changed";
    public static final String SELECT_DATE = "Tap to select a date";
    public static final String PRESENT = "Present";


    public static final String ERROR_MESSAGE_ADDRESS1_EMPTY = "Address line 1 cannot be empty";
    public static final String ERROR_MESSAGE_CITY_DISTRICT_EMPTY = "City/District cannot be empty";
    public static final String ERROR_MESSAGE_PINCODE_EMPTY = "Pincode cannot be empty";
    public static final String ERROR_MESSAGE_STATE_EMPTY = "State cannot be empty";
    public static final String ERROR_MESSAGE_TENURE_END_DATE_GREATER = "tenure start date cannot be greater than tenure end date";
    public static final String ERROR_MESSAGE_LEAVE_END_DATE_GREATER = "leave start date cannot be greater than leave start date";

    public static final String ERROR_MESSAGE_ALREADY_ON_LEAVE = "Cannot apply for fresh leave as staff is already on leave";

    public static final String ERROR_MESSAGE_LEAVE_START_NOT_FILLED = "leave start date not filled";
    public static final String ERROR_MESSAGE_LEAVE_END_NOT_FILLED = "leave end date not filled";
    public static final String NUMBER_OF_NEW_REVIEWS = "Number Of New Reviews";
    public static final String NEW_REVIEW_SCORE = "New Review Score";
    public static final String DISPLAYED_RATINGS = "Displayed Rating";


    public static final String ERROR_MESSAGE_LEAVE_END_DATE_GREATER_TENURE = "Leave period should be between joining date and leaving date";
    public static final String NETWORK_ERROR = "Network Error";
    public static final String CANNOT_CONNECT_TO_SERVER = "Cannot connect to Server";
    public static final String RETRY = "RETRY";
    public static final String INTERNET_NOT_AVAILABLE = "Internet is not available";
    public static final String NO_INCENTIVES = "No incentives found for this hotel";
    public static final String NO_ISSUES = "No issues exist for this hotel";
    public static final int TIMEOUT_SECONDS = 120;
    public static final String ERROR_MESSAGE_SALARY_ZERO = "salary cannot be zero";
    public static final String NO_RESULTS_FOUND = "No results found";
    public static final String CLEAR = "clear";
    public static final String DISCARD_CHANGES = "Are you sure you want to discard these changes?";
    public static final String ERROR_MESSAGE_DATE_OF_BIRTH_EMPTY = "Date of Birth cannot be empty.";


    public static String UNKNOWN = "Something went wrong";
    public static final String ERROR_UNKNOWN = "Something went wrong";

    public static final String OFFLINE_SUBMIT_FORM_NO_INTERNET = "No Internet. Your Audit will be submitted as soon as we reconnect";
    public static final String OFFLINE_SUBMIT_PHOTOS_UPLOADS_PENDING = " Your Audit will be submitted as soon all pending photo uploads have been completed";

    public static final String INVALID_DATE_OF_BIRTH = "Invalid Date Of Birth";

    public static final String CURRENT_BAD_QUERY = "current_bad_query";
    public static final String CURRENT_BAD_AUDIT_ID = "current_bad_audit_id";
    public static final String CURRENT_BAD_AUDIT_POSITION = "current_bad_audit_position";

    public static final String MSG_CONNECTION_TO_GOOGLE_CLIENT_FAILED = "Connection to google client failed";

    public static final String AUDIT_SUBMISSION = "audit_submission";
    public static final String AUDIT_AREA = "audit_area";
    public static final String ISSUE_LIST = "issue_list";
    public static final String FRAUD_ISSUE_LIST = "fraud_issue_list";
    public static final String ROOM_ID = "room_id";
    public static final String AUDIT_TYPE = "audit_type";
    public static final String IS_RESTART_AUDIT = "is_audit_restarted";

    public static final String AUDIT_TYPE_ROOM = "rooms";
    public static final String AUDIT_TYPE_COMMON = "common areas";

    public static final String COMMON_AREA = "Common Area";
    public static final String ROOM_AREA = "Room";

    public static final int HIGH_SEVERITY = 1;
    public static final int MEDIUM_SEVERITY = 2;
    public static final int LOW_SEVERITY = 3;

    public static final String KEY_COMMON_CODE = "CA";//used for storing audit key

    public static final int REQUEST_RESOLVE_ISSUE = 131;
    public static final int REQUEST_UPDATE_ISSUE = 130;

    public static final int REQUEST_ISSUE_DETAILS = 1112;

    public static final int AUDIT_TASK = 1;
    public static final int ISSUE_TASK = 2;
    public static final int MANUAL_TASK = 3;
    public static final int PERIODIC_AUDIT_TASK = 4;
    public static final int STAFF_INCENTIVE_TASK = 5;
    public static final int AUDIT_TASK_COMPLETED_STATE = 6;
    public static final int BOTTOM_DUMMY_TASK = 7;

    public static final int CREATED_TASK = 8;

    public static final int COMMON_AREA_AUDIT_TYPE_ID = 1;
    public static final int ROOM_AUDIT_TYPE_ID = 2;

    public static final int BREAKFAST_AUDIT_TASK_TYPE = 2;
    public static final int FnB_AUDIT_TASK_TYPE = 3;
    public static final int FRAUD_AUDIT_TASK_TYPE = 4;
    public static final int BACK_OF_THE_HOUSE_AUDIT_TASK_TYPE = 5;

    public static final int HR_AUDIT_TASK_TYPE = 6;
    public static final int STORAGE_SPACE_AUDIT_TASK_TYPE = 7;
    public static final int SAFETY_AUDIT_TASK_TYPE = 8;
    public static final int GARBAGE_MANAGEMENT_AUDIT_TASK_TYPE = 9;
    public static final int WATER_STORAGE_AUDIT_TASK_TYPE = 10;
    public static final int DISABLED_FRIENDLY_AUDIT_TASK_TYPE = 11;
    public static final int HK_TOOLS_AUDIT_TASK_TYPE = 12;
    public static final int FRONT_OFFICE_AUDIT_TASK_TYPE = 13;
    public static final int LUXURY_AMENITIES_AUDIT_TASK_TYPE = 14;
    public static final int PARKING_AREA_TASK_TYPE = 15;
    public static final int IN_HOUSE_LAUNDRY_AUDIT_TASK_TYPE = 16;
    public static final int OTHER_AREAS_AUDIT_TASK_TYPE = 17;

    public static final String COMMON_AREA_AUDIT_STRING = "CONST_CA";
    public static final String ROOM_AUDIT_STRING = "CONST_ROOMS";
    public static final String BREAKFAST_AUDIT_STRING = "CONST_BREAKFAST";
    public static final String FnB_AUDIT_STRING = "CONST_F_&_B";
    public static final String BACK_OF_THE_HOUSE_AUDIT_STRING = "CONST_B_O_T_H";
    public static final String FRAUD_AUDIT_STRING = "CONST_FRAUD";
    public static final String KITCHEN_PANTRY_AUDIT_STRING = "CONST_KITCHEN";
    public static final String RESTAURANT_WASHROOM_AUDIT_STRING = "CONST_REST_WASH";
    public static final String RESTAURANT_AUDIT_STRING = "CONST_RESTAURANT";
    public static final String GYM_AUDIT_STRING = "CONST_GYM";
    public static final String SPA_AUDIT_STRING = "CONST_SPA";
    public static final String POOL_AUDIT_STRING = "CONST_POOL";
    public static final String STORAGE_SPACE_AUDIT_STRING = "CONST_STORAGE";
    public static final String SAFETY_AUDIT_STRING = "CONST_SAFETY";
    public static final String GARBAGE_MANAGEMENT_AUDIT_STRING = "CONST_GARBAGE_MGMT";
    public static final String WATER_STORAGE_AUDIT_STRING = "CONST_WATER_STORAGE";
    public static final String DISABLED_FRIENDLY_AUDIT_STRING = "CONST_DISABLED_FRIENDLY";
    public static final String HK_TOOLS_AUDIT_STRING = "CONST_HK_TOOLS";
    public static final String FRONT_OFFICE_AUDIT_STRING = "CONST_FO";
    public static final String LUXURY_AMENITIES_AUDIT_STRING = "CONST_LUXURY";
    public static final String PARKING_AREA_STRING = "CONST_PARKING";
    public static final String IN_HOUSE_LAUNDRY_AUDIT_STRING = "CONST_LAUNDRY";
    public static final String ACCO_FOOD_AUDIT_STRING = "CONST_ACCO_FOOD";
    public static final String POLICY_AND_CONDUCT_AUDIT_STRING = "CONST_POL";
    public static final String DRIVERS_QUARTERS_AUDIT_STRING = "CONST_DRIVER_QUATERS";
    public static final String CLOAK_ROOM_AUDIT_STRING = "CONST_CLOAK";
    public static final String SERVICE_AUDIT_STRING = "CONST_SERVICE";

    public static final int AUDIT_TASK_START = 11222;

    public static final int ROOM_NOT_READY = 1;
    public static final int ROOM_OCCUPIED = 2;
    public static final int OTHERS = 3;

    public static final String SOURCE_QAM = "QAM";

    public static final String EMPTY = "";

    public static final String TASK_MODEL = "task_model";

    public static final String ROOM_COUNT_AUDIT = "Room Count";
    public static final String HOUSE_COUNT_AUDIT_TEXT = "House Count";
    public static final int ROOM_COUNT_ID = 1001;
    public static final int PRICE_MISMATCH_ID = 1100;
    public static final String PRICE_MISMATCH_AUDIT = "Price Mismatch";
    public static final String FRAUD_CHECKPOINT_AUDIT = "Checkpoint";

    public static final String AUDIT_CATEGORY_ID = "audit_category_id";

    public static final int VISUAL_REPRESENTATION_TYPE_3 = 3;
    public static final int VISUAL_REPRESENTATION_TYPE_1 = 1;
    public static final int VISUAL_REPRESENTATION_TYPE_2 = 2;

    public static final int TASKS_BOTTOM_MARGIN = 20;

}
