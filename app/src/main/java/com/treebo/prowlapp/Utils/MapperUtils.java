package com.treebo.prowlapp.Utils;

import android.util.Log;

import com.treebo.prowlapp.data.DepartmentInformation;
import com.treebo.prowlapp.data.IndividualIncentivesItem;
import com.treebo.prowlapp.flowscores.adapter.HotelScoresViewPagerAdapter;
import com.treebo.prowlapp.Models.DepartmentWiseStaffModel;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.GroupedScoresModel;
import com.treebo.prowlapp.Models.ScoresData;
import com.treebo.prowlapp.Models.ScoresModel;
import com.treebo.prowlapp.Models.StaffModel;
import com.treebo.prowlapp.Models.SubHeadingScoresModel;
import com.treebo.prowlapp.response.HotelIncentivesResponse;
import com.treebo.prowlapp.response.ScoresResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by devesh on 29/04/16.
 */
public class MapperUtils {


    public static ArrayList<IndividualIncentivesItem> mapIndividualIncentivesItem(HotelIncentivesResponse hotelIncentivesResponse) {
        if (hotelIncentivesResponse.data == null || hotelIncentivesResponse.data.departmentIndividualsList == null ) {
            return new ArrayList<>();
        }
        HashSet<String> departmentsAdded=new HashSet<>();
        ArrayList<IndividualIncentivesItem> individualIncentivesItems = new ArrayList<>();
        for (HotelIncentivesResponse.DepartmentIndividuals departmentIndividuals : hotelIncentivesResponse.data.departmentIndividualsList) {
            IndividualIncentivesItem individualIncentivesItem = new IndividualIncentivesItem();
            individualIncentivesItem.heading = departmentIndividuals.individualDepartment;
            departmentsAdded.add(individualIncentivesItem.heading);
            individualIncentivesItem.individualEntryList = departmentIndividuals.individualEntries;
            individualIncentivesItems.add(individualIncentivesItem);
        }
        ArrayList<DepartmentInformation> departmentInformationList =  MapperUtils.mapDepartmentInformation(hotelIncentivesResponse);
        if(departmentInformationList!=null && !departmentInformationList.isEmpty()){
            for(DepartmentInformation departmentInformation:departmentInformationList){
                if(!departmentsAdded.contains(departmentInformation.departmant_name)){
                    IndividualIncentivesItem individualIncentivesItem = new IndividualIncentivesItem();
                    individualIncentivesItem.heading = departmentInformation.departmant_name;
                    individualIncentivesItem.individualEntryList = new ArrayList<>();
                    individualIncentivesItems.add(individualIncentivesItem);
                }
            }
            return individualIncentivesItems;
        }
        return individualIncentivesItems;
    }


    public static ArrayList<GroupedScoresModel> mapGroupedScoresModel(ScoresResponse response) {
        ArrayList<GroupedScoresModel> groupedScoresModelList = new ArrayList<>();
        for (ScoresResponse.ScoresSectionedData datum : response.data) {
            GroupedScoresModel model = new GroupedScoresModel();
            model.heading = datum.heading;
            model.scores = mapScoresToScoresModel(datum.scores);
            groupedScoresModelList.add(model);
        }
        return groupedScoresModelList;
    }

    private static ScoresModel mapScoresToScoresModel(ScoresResponse.Scores scores) {
        ScoresModel scoresModel = new ScoresModel();
        scoresModel.overall = scores.overall;
        scoresModel.tqlMonth = scores.tqlMonth;
        if(scores.subHeading != null) {
            scoresModel.subHeading = mapSubheadingToSubheadingModel(scores.subHeading);
        }
        return scoresModel;
    }

    private static List<SubHeadingScoresModel> mapSubheadingToSubheadingModel(List<ScoresResponse.SubHeadingScores> subHeading) {
        List<SubHeadingScoresModel> subHeadingScoresModels = new ArrayList<>();
        for (ScoresResponse.SubHeadingScores subHeadingScore : subHeading) {
            SubHeadingScoresModel subHeadingScoresModel = new SubHeadingScoresModel();
            subHeadingScoresModel.current = subHeadingScore.current;
            subHeadingScoresModel.name = subHeadingScore.name;
            subHeadingScoresModel.last_seven_days = subHeadingScore.last_seven_days;
            subHeadingScoresModel.last_thirty_days = subHeadingScore.last_thirty_days;
            subHeadingScoresModels.add(subHeadingScoresModel);
        }

        return subHeadingScoresModels;
    }

    public static HashMap<String, List<ScoresData>> mapHeadingToDaywiseScore(List<SubHeadingScoresModel> subHeadings) {
        HashMap<String, List<ScoresData>> hashMapScoresData = new HashMap<>();
        List<ScoresData> subHeadingScoresCurrent = new ArrayList<>();
        List<ScoresData> subHeadingScores7Days = new ArrayList<>();
        List<ScoresData> subHeadingScores30Days = new ArrayList<>();
        for (SubHeadingScoresModel score : subHeadings) {
            subHeadingScoresCurrent.add(new ScoresData(score.name, score.current));
            subHeadingScores7Days.add(new ScoresData(score.name, score.last_seven_days));
            subHeadingScores30Days.add(new ScoresData(score.name, score.last_thirty_days));
        }
        hashMapScoresData.put(HotelScoresViewPagerAdapter.CURRENT, subHeadingScoresCurrent);
        hashMapScoresData.put(HotelScoresViewPagerAdapter.SEVEN_DAYS, subHeadingScores7Days);
        hashMapScoresData.put(HotelScoresViewPagerAdapter.THIRTY_DAYS, subHeadingScores30Days);
        return hashMapScoresData;
    }


    public static HashMap<String, List<StaffModel>> mapStaffByDepartments(List<StaffModel> staffModelList) {
        HashMap<String, List<StaffModel>> map = new HashMap<>();
        ArrayList<StaffModel> list = new ArrayList<>();
        for (StaffModel staff : staffModelList) {
            if (map.get(staff.departmentName) == null) {
                list.addAll(Arrays.asList(staff));
                map.put(staff.departmentName, list);
            } else {
                if (map.containsKey(staff.departmentName) && map.get(staff.departmentName) != null && map.get(staff.departmentName).size() > 0) {
                    Log.v("MapperUtils", "getDepartmentWiseStaff :: " + map);
                    List<StaffModel> arraylist = map.get(staff.departmentName);
                    arraylist.add(staff);
                    map.put(staff.departmentName, arraylist);
                }
            }
        }

        return cleanseMap(map);
    }

    private static HashMap<String, List<StaffModel>> cleanseMap(HashMap<String, List<StaffModel>> map) {
        HashMap<String, List<StaffModel>> cleanMap = new HashMap<>();
        for (String key : map.keySet()) {

            List<StaffModel> newStaffList = new ArrayList<>();
            List<StaffModel> staffList = map.get(key);
            for (StaffModel staff : staffList) {
                if (staff.departmentName.equalsIgnoreCase(key)) {
                    newStaffList.add(staff);
                }
            }
            cleanMap.put(key, newStaffList);
        }
        return cleanMap;
    }

    public static HashMap<Integer, Integer> populateFirstVisiblePositionsMap(List<DepartmentWiseStaffModel> departmentWiseStaffModels){
        HashMap<Integer, Integer> departmentPositionMap = new HashMap<>();
        int count=0;
        for(int dpos = 0 ; dpos < departmentWiseStaffModels.size() ; dpos++){
            departmentPositionMap.put(dpos , count);
            for(int staffPos= 0 ; staffPos < departmentWiseStaffModels.get(dpos).staff.size(); staffPos++){
                count++;
            }
            count++;
        }
        return departmentPositionMap;

    }

    public static HashMap<Integer, Integer> populateDepartmentStaffMap( List<DepartmentWiseStaffModel> departmentWiseStaffModels){
        HashMap<Integer, Integer> countsMap = new HashMap<>();
        int count=0;
        for(int dpos = 0 ; dpos < departmentWiseStaffModels.size() ; dpos++){
            for(int staffPos= 0 ; staffPos < departmentWiseStaffModels.get(dpos).staff.size(); staffPos++){
                countsMap.put(count , dpos);
                count++;
            }
            countsMap.put(count , dpos);
            count++;
        }
        return countsMap;

    }

    public static List<DepartmentWiseStaffModel> getDepartmentWiseStaff(List<StaffModel> staffModelList) {
        List<DepartmentWiseStaffModel> departmentWiseStaffModels = new ArrayList<>();
        HashMap<String, List<StaffModel>> hashMap = mapStaffByDepartments(staffModelList);
        for (String dept : hashMap.keySet()) {
            DepartmentWiseStaffModel departmentWiseStaffModel = new DepartmentWiseStaffModel();
            departmentWiseStaffModel.title = dept;
            departmentWiseStaffModel.staff = hashMap.get(dept);
            departmentWiseStaffModels.add(departmentWiseStaffModel);
        }
        return departmentWiseStaffModels;
    }


    public static ArrayList<DepartmentInformation> mapDepartmentInformation(HotelIncentivesResponse hotelIncentivesResponse) {
        if (hotelIncentivesResponse.data == null || hotelIncentivesResponse.data.departments == null || hotelIncentivesResponse.data.departments.size() == 0) {
            return new ArrayList<>();
        }
        ArrayList<DepartmentInformation> departmentInformationList = new ArrayList<>();
        for (HotelIncentivesResponse.Department department : hotelIncentivesResponse.data.departments) {
            DepartmentInformation departmentInformation = new DepartmentInformation();
            departmentInformation.departmant_name = department.department;
            departmentInformation.score = department.information.score;
            departmentInformation.todaysChanges = department.information.todaysChanges;
            departmentInformation.till_date = department.information.tillDate;
            departmentInformationList.add(departmentInformation);
        }
        return departmentInformationList;
    }




    public static HashMap<String, SubHeadingScoresModel> mapSubHeadeingScoresListToHashMap(List<SubHeadingScoresModel> subHeadingScores) {
        HashMap<String, SubHeadingScoresModel> subHeadingScoresModelHashMap = new HashMap<>();
        for (SubHeadingScoresModel model : subHeadingScores) {
            subHeadingScoresModelHashMap.put(model.name, model);
        }
        return subHeadingScoresModelHashMap;
    }

    public static HashMap<Integer, Integer> getSectionPositionHashMap(Form mAreaList) {
        HashMap<Integer, Integer> countsMap = new HashMap<>();
        int count=0;
        for(int dpos = 0 ; dpos < mAreaList.getSubsections().size() ; dpos++){
            for(int staffPos= 0 ; staffPos < mAreaList.getSubsections().get(dpos).getQueries().size(); staffPos++){
                countsMap.put(count , dpos);
                count++;
            }
            countsMap.put(count , dpos);
            count++;
        }
        return countsMap;
    }

    public static HashMap<Integer, Integer> populateFirstVisibleSectionPositionsMap(Form mAreaList){
        HashMap<Integer, Integer> firstPositionMap = new HashMap<>();
        int count=0;
        for(int dpos = 0 ; dpos < mAreaList.getSubsections().size() ; dpos++){
            firstPositionMap.put(dpos , count);
            for(int staffPos= 0 ; staffPos < mAreaList.getSubsections().get(dpos).getQueries().size(); staffPos++){
                count++;
            }
            count++;
        }
        return firstPositionMap;

    }
}
