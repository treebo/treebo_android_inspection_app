package com.treebo.prowlapp.flowlocation.presenter;

import android.content.Context;

import com.treebo.prowlapp.flowlocation.EnableLocationContract;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.Utils.LocationUtils;

/**
 * Created by abhisheknair on 18/10/16.
 */

public class EnableLocationPresenter implements EnableLocationContract.IEnableLocationPresenter {

    private Context mContext;

    private EnableLocationContract.IEnabledLocationView mView;

    public EnableLocationPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void onEnableLocationClick() {
        if (!LocationUtils.isLocationEnabled(mContext)) {
            mView.showEnableLocationPermissionScreen();
        } else {
            mView.showLocationSplashScreen();
        }
    }

    @Override
    public void onResume() {
        mView.setDashboardText();
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mView =(EnableLocationContract.IEnabledLocationView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return null;
    }
}
