package com.treebo.prowlapp.flowlocation.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;
import com.segment.analytics.Traits;
import com.treebo.prowlapp.BuildConfig;
import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.AppUpdateEvent;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.flowOnboarding.OnBoardingContract;
import com.treebo.prowlapp.flowOnboarding.observers.PortfolioHotelListObserver;
import com.treebo.prowlapp.flowOnboarding.usecase.PortfolioHotelsListUseCase;
import com.treebo.prowlapp.flowdashboard.activity.DashboardActivity;
import com.treebo.prowlapp.flowlocation.DaggerSplashComponent;
import com.treebo.prowlapp.flowlocation.LocationSplashContract;
import com.treebo.prowlapp.flowlocation.SplashComponent;
import com.treebo.prowlapp.flowlocation.SplashModule;
import com.treebo.prowlapp.flowlocation.presenter.LocationSplashActivityPresenter;
import com.treebo.prowlapp.flowportfolionew.activity.PortfolioActivityNew;
import com.treebo.prowlapp.job.OfflineJobUtils;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.navigation.Navigator;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.RxUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.Utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.OnClick;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by abhisheknair on 18/10/16.
 */

public class LocationSplashActivity extends AppCompatActivity
        implements LocationSplashContract.ISplashActivityView, OnBoardingContract.IPortfolioHotelsListView {

    @Inject
    public LocationSplashActivityPresenter mSplashPresenter;

    @Inject
    public LoginSharedPrefManager mLoginManager;

    @Inject
    public RxBus mRxBus;

    @Inject
    public Navigator mNavigator;

    @Inject
    public PortfolioHotelsListUseCase mPortfolioHotelListUseCase;


    private RelativeLayout mBackgroundLayout;

    private FusedLocationProviderClient mLocationProviderClient;

    private View noInternetView;

    CompositeSubscription mSubccription = new CompositeSubscription();

    int portfolioHotelsRetryCount = 0;

    private static final int REQUEST_LOCATION_PERMISSION = 10219;

    private View pleaseWaitTV;
    private View pleaseWaitImage;
    private View pleaseWaitMessage;
    private View hotelLoadErrorTV;
    private View mRetryBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_splash);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        ((TextView) findViewById(R.id.treeboTextView2)).setText(BuildConfig.VERSION_NAME);

        pleaseWaitTV = findViewById(R.id.please_wait_tv);
        pleaseWaitImage = findViewById(R.id.location_iv);
        pleaseWaitMessage = findViewById(R.id.please_wait_message);
        hotelLoadErrorTV = findViewById(R.id.hotels_load_error_tv);
        mRetryBtn = findViewById(R.id.retry_button);

        mRetryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                portfolioHotelsRetryCount += 1;
                checkIfPortfolioHotelsAvailable();
            }
        });
        mBackgroundLayout = (RelativeLayout) findViewById(R.id.location_splash_ll);
        initSplashComponent();
        buildLocationProviderClient();

        if (mLoginManager.getServerAppVersion() > BuildConfig.VERSION_CODE) {
            mNavigator.navigateToBlockerScreen(this);
        }

        noInternetView = findViewById(R.id.no_internet_layout);
        View tryAgainBtn = noInternetView.findViewById(R.id.try_again_btn);
        tryAgainBtn.setOnClickListener(view -> {
            noInternetView.setVisibility(View.GONE);
            //detectLocation();
            checkIfPortfolioHotelsAvailable();
        });
        checkIfPortfolioHotelsAvailable();
        mSubccription.add(RxUtils.build(mRxBus.toObservable()).subscribe(event -> {
            if (event instanceof HotelChangedEvent) {
                ArrayList<HotelLocation> hotelLocation = ((HotelChangedEvent) event).mHotelList;
                mSplashPresenter.onLocationDetected(hotelLocation);
                Analytics.with(this).identify(new Traits().putEmail(mLoginManager.getEmailId()));
            } else if (event instanceof AppUpdateEvent) {
                mNavigator.navigateToBlockerScreen(this);
            }
        }));

    }

    public void initSplashComponent() {
        SplashComponent onBoardingComponent = DaggerSplashComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .splashModule(new SplashModule(this))
                .build();
        onBoardingComponent.injectLocationSplashActivity(this);
    }

    private void detectLocation() {
        if (Utils.isInternetConnected(this)) {
            mSplashPresenter.detectLocation(mLocationProviderClient);
        } else
            noInternetView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLocationNotDetected() {
        AlertDialog.Builder builder;
        if (isFinishing()) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setMessage(R.string.gps_signal_weak)
                .setPositiveButton(R.string.portfolio, (dialogInterface, i) -> {
                    showLandingScreen(new ArrayList<>());
                })
                .setNegativeButton(R.string.retry_text, (dialogInterface, i) -> {
                    mSplashPresenter.retryDetectLocation(mLocationProviderClient);
                })
                .setCancelable(false)
                .show();

    }

    @Override
    public void showLandingScreen(ArrayList<HotelLocation> hotelLocation) {
        if (Utils.isInternetConnected(this)) {
            switch (hotelLocation.size()) {
                case 1:
                    Intent intent1 = new Intent(this, DashboardActivity.class);
                    intent1.putExtra(Utils.HOTEL_ID, hotelLocation.get(0).mHotelId);
                    intent1.putExtra(Utils.HOTEL_NAME, hotelLocation.get(0).mHotelName);
                    intent1.putExtra(Utils.IS_IN_HOTEL, true);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent1);
                    break;
                case 0:
                    OfflineJobUtils.logLocationNotFoundError();
                default:
                    Intent intent2 = new Intent(this, PortfolioActivityNew.class);
                    intent2.putExtra(Utils.HOTEL_LIST, hotelLocation);
                    SegmentAnalyticsManager.trackScreenOnSegment(SegmentAnalyticsManager.PORTFOLIO_VIEW,
                            new Properties().putValue(SegmentAnalyticsManager.SOURCE_SCREEN, SegmentAnalyticsManager.SOURCE_FROM_GEOFENCE));
                    intent2.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent2);
                    break;

            }
            finish();
        } else {
            noInternetView.setVisibility(View.VISIBLE);
        }
    }

    public void checkIfPortfolioHotelsAvailable() {
        showLoading();
        if (mLoginManager.isPortfolioListDownloaded()) {
            detectLocation();
        } else {
            mPortfolioHotelListUseCase.setUserId(mLoginManager.getUserId());
            mPortfolioHotelListUseCase.execute(provideHotelLocationResponseObserver());
        }
    }

    private PortfolioHotelListObserver provideHotelLocationResponseObserver() {
        return new PortfolioHotelListObserver(mSplashPresenter, mPortfolioHotelListUseCase, this);
    }


    //Once Hotel list load is success..start location detection
    @Override
    public void onHotelListLoadSuccess() {
        mLoginManager.setPrefIsPortfolioListDownloaded(true);
        Log.d(LocationSplashActivity.class.getName(), "Portfolio hotel list load success");
        detectLocation();
    }

    @Override
    public void onHotelListLoadError(String error) {
        if (portfolioHotelsRetryCount >= 3) {
            showLoading();
            detectLocation();
        } else {
            showRetry();
        }
    }

    @Override
    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_LOCATION_PERMISSION);
    }

    public void buildLocationProviderClient() {
        mLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @Override
    @TargetApi(23)
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        mSplashPresenter.detectLocation(mLocationProviderClient);
                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = shouldShowRequestPermissionRationale(permissions[0]);
                        String btnText;
                        if (!showRationale) {
                            btnText = getString(R.string.go_to_settings);
                        } else {
                            btnText = getString(R.string.btn_allow);
                        }
                        showGiveLocationPermissionsDialog(btnText);
                    }
                    break;
                }
        }
    }

    @Inject
    public void setUp() {
        mSplashPresenter.setMvpView(this);
        mSplashPresenter.setUserManager(mLoginManager);
        mSplashPresenter.onCreate();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.unSubscribe(mSubccription);
    }


    @Override
    public void setImageAccordingToTime() {
        Calendar calendar = Calendar.getInstance();
        int timeOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        Drawable backgroundDrawable = null;
        if (timeOfDay >= 4 && timeOfDay < 12)
            backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.loaction_splash_morning);
        else if (timeOfDay >= 12 && timeOfDay < 18)
            backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.loaction_splash_afternoon);
        else if ((timeOfDay >= 18 && timeOfDay < 24) || (timeOfDay >= 0 && timeOfDay < 4))
            backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.loaction_splash_night);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
            mBackgroundLayout.setBackgroundDrawable(backgroundDrawable);
        else
            mBackgroundLayout.setBackground(backgroundDrawable);
    }

    @Override
    public void setUpLocationAnimation() {
        ImageView mLocationIcon = (ImageView) findViewById(R.id.location_iv);
        Animation mAnimation = new TranslateAnimation(0, 0, 0, 50);
        mAnimation.setDuration(2000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        mLocationIcon.setAnimation(mAnimation);
    }

    public static Intent getCallingIntent(Activity activity) {
        return new Intent(activity, LocationSplashActivity.class);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {
        mRetryBtn.setVisibility(View.GONE);
        hotelLoadErrorTV.setVisibility(View.GONE);
        pleaseWaitMessage.setVisibility(View.VISIBLE);
        pleaseWaitTV.setVisibility(View.VISIBLE);
        pleaseWaitImage.setVisibility(View.VISIBLE);
        setUpLocationAnimation();
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {
        mRetryBtn.setVisibility(View.VISIBLE);
        hotelLoadErrorTV.setVisibility(View.VISIBLE);
        pleaseWaitMessage.setVisibility(View.GONE);
        pleaseWaitTV.setVisibility(View.GONE);
        pleaseWaitImage.setAnimation(null);
        pleaseWaitImage.setVisibility(View.GONE);
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    public void goToAppSettingsPage() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void showLocationNotAvailable() {
        AlertDialog.Builder builder;
        if (isFinishing()) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setMessage(R.string.privacy_settings)
                .setPositiveButton(R.string.go_back, (dialogInterface, i) -> {
                    finish();
                })
                .setCancelable(false)
                .show();

    }

    public void showGiveLocationPermissionsDialog(String btnText) {
        if (isFinishing()) {
            return;
        }
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setMessage(R.string.permissions_denied_dialog_msg)
                .setPositiveButton(btnText, (dialogInterface, i) -> {
                    if (btnText.equals(getString(R.string.btn_allow)))
                        requestLocationPermission();
                    else if (btnText.equals(getString(R.string.go_to_settings)))
                        goToAppSettingsPage();
                })
                .setCancelable(false)
                .show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.activityResumed();
        if (mLoginManager.getServerAppVersion() > BuildConfig.VERSION_CODE) {
            mNavigator.navigateToBlockerScreen(this);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

    @OnClick(R.id.retry_button)
    void onRetryButtonClicked() {
        checkIfPortfolioHotelsAvailable();
    }
}
