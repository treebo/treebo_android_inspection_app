package com.treebo.prowlapp.flowlocation;

import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.ApplicationComponent;
import com.treebo.prowlapp.application.PerActivity;
import com.treebo.prowlapp.flowlocation.activity.EnableLocationActivity;
import com.treebo.prowlapp.flowlocation.activity.LocationSplashActivity;

import dagger.Component;

/**
 * Created by sumandas on 22/10/2016.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules
        = {ActivityModule.class,SplashModule.class})
public interface SplashComponent {
    void injectEnableLocationActvity(EnableLocationActivity enableLocationActivity);
    void injectLocationSplashActivity(LocationSplashActivity locationSplashActivity);
}
