package com.treebo.prowlapp.flowlocation.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.view.Window;
import android.view.WindowManager;

import com.treebo.prowlapp.R;
import com.treebo.prowlapp.application.ActivityModule;
import com.treebo.prowlapp.application.BaseActivity;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowlocation.DaggerSplashComponent;
import com.treebo.prowlapp.flowlocation.EnableLocationContract;
import com.treebo.prowlapp.flowlocation.SplashComponent;
import com.treebo.prowlapp.flowlocation.SplashModule;
import com.treebo.prowlapp.flowlocation.presenter.EnableLocationPresenter;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.usecase.UseCase;
import com.treebo.prowlapp.Utils.LocationUtils;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;
import com.treebo.prowlapp.Utils.SnackbarUtils;
import com.treebo.prowlapp.views.TreeboButton;
import com.treebo.prowlapp.views.TreeboTextView;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by abhisheknair on 18/10/16.
 */

public class EnableLocationActivity extends AppCompatActivity implements EnableLocationContract.IEnabledLocationView {


    @Inject
    public EnableLocationPresenter mPresenter;

    @Inject
    public LoginSharedPrefManager mLoginManager;


    private TreeboButton mEnableLocationButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enable_location);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        initSplashComponent();
        ((TreeboTextView) findViewById(R.id.hi_user_tv)).setText(getString(R.string.hi_user,
                mLoginManager.getUsername()));
        mEnableLocationButton = (TreeboButton) findViewById(R.id.enable_location_button);
        mEnableLocationButton.setOnClickListener(view -> mPresenter.onEnableLocationClick());
    }

    public void initSplashComponent() {
        SplashComponent onBoardingComponent = DaggerSplashComponent.builder()
                .applicationComponent(MainApplication.get().getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .splashModule(new SplashModule(this))
                .build();
        onBoardingComponent.injectEnableLocationActvity(this);
    }

    @Override
    public void setUpViewAsPerPermissions() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public boolean showError(String errorMessage) {
        SnackbarUtils.show(findViewById(android.R.id.content), errorMessage);
        return false;
    }

    @Override
    public void closeAndNavigateBack(String message) {

    }

    @Override
    public BaseActivity getBaseActivity() {
        return null;
    }

    @Override
    public void showEmptyView() {

    }

    @Override
    public void hideEmptyView() {

    }

    @Override
    public void showServerErrorAlertDialog(UseCase useCase) {

    }

    @Inject
    public void setUp() {
        mPresenter.setMvpView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();
        MainApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MainApplication.activityPaused();
    }

    public static Intent getCallingIntent(Activity activity) {
        return new Intent(activity, EnableLocationActivity.class);
    }

    @Override
    public void setDashboardText() {
        if (LocationUtils.isLocationEnabled(this)) {
            showLocationSplashScreen();
        } else {
            mEnableLocationButton.setText(getString(R.string.enable_location));
        }
    }

    @Override
    public void showLocationSplashScreen() {
        Intent intent = LocationSplashActivity.getCallingIntent(this);
        startActivity(intent);
        finish();
    }

    @Override
    public void showEnableLocationPermissionScreen() {
        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }
}
