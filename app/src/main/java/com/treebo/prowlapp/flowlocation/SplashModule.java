package com.treebo.prowlapp.flowlocation;

import android.content.Context;

import com.treebo.prowlapp.flowOnboarding.usecase.PortfolioHotelsListUseCase;
import com.treebo.prowlapp.flowlocation.presenter.EnableLocationPresenter;
import com.treebo.prowlapp.flowlocation.presenter.LocationSplashActivityPresenter;
import com.treebo.prowlapp.net.RestClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sumandas on 22/10/2016.
 */
@Module
public class SplashModule {

    private Context mContext;

    public SplashModule(Context context){
        mContext=context;
    }

    @Provides
    RestClient providesRestClient(){
        return new RestClient();
    }

    @Provides
    EnableLocationPresenter providesEnableSplashPresenter(){
        return new EnableLocationPresenter(mContext);
    }

    @Provides
    LocationSplashActivityPresenter providesLocationSplashPresenter(){
        return new LocationSplashActivityPresenter(mContext);
    }

    @Provides
    PortfolioHotelsListUseCase providesPortfolioHotelsListUseCase(RestClient restClient){
        return new PortfolioHotelsListUseCase(restClient);
    }

}
