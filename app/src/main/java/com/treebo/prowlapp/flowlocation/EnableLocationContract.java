package com.treebo.prowlapp.flowlocation;

import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.presenter.BasePresenter;

/**
 * Created by abhisheknair on 18/10/16.
 */

public interface EnableLocationContract {

    interface IEnableLocationPresenter extends BasePresenter {
        void onEnableLocationClick();
        void onResume();

    }

    interface IEnabledLocationView  extends BaseView {
        void showLocationSplashScreen();
        void showEnableLocationPermissionScreen();
        void setDashboardText();
    }
}
