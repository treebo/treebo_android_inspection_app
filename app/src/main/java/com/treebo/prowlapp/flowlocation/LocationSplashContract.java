package com.treebo.prowlapp.flowlocation;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.presenter.BasePresenter;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.util.ArrayList;

/**
 * Created by abhisheknair on 18/10/16.
 */

public interface LocationSplashContract {

    interface ISplashActivityView extends BaseView {
        void setImageAccordingToTime();

        void setUpLocationAnimation();

        void showLandingScreen(ArrayList<HotelLocation> hotelList);

        void requestLocationPermission();

        void showLocationNotDetected();

        void showLocationNotAvailable();
    }

    interface ISplashActivityPresenter extends BasePresenter {
        void onCreate();


        void onLocationDetected(ArrayList<HotelLocation> hotelList);

        void setUserManager(LoginSharedPrefManager loginSharedPrefManager);//used to persist user location

        void detectLocation(FusedLocationProviderClient client);

        void retryDetectLocation(FusedLocationProviderClient client);
    }
}
