package com.treebo.prowlapp.flowlocation.presenter;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.segment.analytics.Properties;
import com.treebo.prowlapp.flowlocation.LocationSplashContract;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.mvpviews.BaseView;
import com.treebo.prowlapp.service.LocationService;
import com.treebo.prowlapp.service.UpdateHotelLocationService;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;
import com.treebo.prowlapp.Utils.LocationUtils;
import com.treebo.prowlapp.Utils.PermissionHelper;
import com.treebo.prowlapp.Utils.SegmentAnalyticsManager;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Action1;

/**
 * Created by abhisheknair on 18/10/16.
 */

public class LocationSplashActivityPresenter implements LocationSplashContract.ISplashActivityPresenter {

    private LocationSplashContract.ISplashActivityView mISplashActivityView;
    private Context mContext;
    private LoginSharedPrefManager mLoginManager;
    private FusedLocationProviderClient mLocationProviderClient;

    LocationRequest mLocationRequest;
    PendingIntent mRequestLocationUpdatesPendingIntent;

    int noTries;
    boolean isAccurateLocationDetected = false;
    float maxAccuracySeen = Float.MAX_VALUE;


    public LocationSplashActivityPresenter(Context context) {
        mContext = context;
    }

    @Override
    public void onCreate() {
        if (mISplashActivityView != null) {
            mISplashActivityView.setImageAccordingToTime();
            mISplashActivityView.setUpLocationAnimation();
        }
    }

    @Override
    public void setUserManager(LoginSharedPrefManager loginManager) {
        mLoginManager = loginManager;
    }

    @Override
    public void detectLocation(FusedLocationProviderClient client) {
        mLocationProviderClient = client;
        if (PermissionHelper.hasNoPermissionToAccessLocation(mContext)) {
            mISplashActivityView.requestLocationPermission();
        } else {
            Observable.timer(LocationUtils.DURATION_OF_RETRY, TimeUnit.SECONDS).subscribe(aLong -> {
                if (!isAccurateLocationDetected) {
                    Intent hotelIntent = new Intent(mContext, UpdateHotelLocationService.class);
                    hotelIntent.putExtra(LoginSharedPrefManager.PREF_HOTEL_NOT_FOUND_ACCURACY, maxAccuracySeen);
                    mContext.startService(hotelIntent);
                }
            }, new Action1<Throwable>() {
                @Override
                public void call(Throwable throwable) {
                    Log.d(LocationSplashActivityPresenter.class.getName(), "Observable for location returned  exception " + throwable.getMessage());
                }
            });
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                mLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location == null) {
                            Log.i("First Location Check", "Location not found. Starting with update");
                            startLocationUpdates();
                        } else {
                            Log.i("First Location Check", "Location found" + location.getLatitude() + " - " + location.getLongitude() + " with accuracy " + location.getAccuracy());
                            checkLocationAccuracy(location);
                        }
                    }
                });

                startLocationUpdates();
            }
        }
    }

    private void checkLocationAccuracy(Location location) {
        if (location.getAccuracy() <= LocationUtils.sAccuracy && location.getAccuracy() > 0.0f) {
            Log.d("Location", "accuracy is" + location.getAccuracy());
            isAccurateLocationDetected = true;
            setLocationCoordinates(location.getLatitude(), location.getLongitude());
            if (LocationUtils.areThereMockPermissionApps(mContext)) {
                Log.w("MOCKED!!", "location might be mocked");
                SegmentAnalyticsManager.trackEventOnSegment(SegmentAnalyticsManager.MOCK_LOCATION_ON, new Properties());
            }
            Log.i("LocationFound", "Location coordinates with accuracy found");
            //Remove unnecessary location updates
            mLocationProviderClient.removeLocationUpdates(mLocationCallback);
            //LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationCallBack);
            Intent hotelIntent = new Intent(mContext, UpdateHotelLocationService.class);
            hotelIntent.putExtra(LoginSharedPrefManager.PREF_HOTEL_NOT_FOUND_ACCURACY, location.getAccuracy());
            mContext.startService(hotelIntent);
        } else {
            noTries++;
            Log.d("Location", "accuracy less than threshold retry attempt " + noTries);
            if (location.getAccuracy() == 0.0f) {
                Log.d("Location", "accuracy less is zero continuing retry " + noTries);
            } else if (location.getAccuracy() < maxAccuracySeen) {
                maxAccuracySeen = (location.getAccuracy() < mLoginManager.getLocationAccuracy())
                        ? location.getAccuracy() : mLoginManager.getLocationAccuracy();
                Log.d("Location", "max Accuracy seen " + location.getAccuracy());
                setLocationCoordinates(location.getLatitude(), location.getLongitude());
            }
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if (locationResult != null && locationResult.getLastLocation() != null) {
                Log.i("Location Update", "Location found "
                        + locationResult.getLastLocation().getLatitude() + " - "
                        + locationResult.getLastLocation().getLongitude() + " with accuracy " + locationResult.getLastLocation().getAccuracy());
                checkLocationAccuracy(locationResult.getLastLocation());
            }
        }

        @Override
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            Log.d("onLocationAvailability ", "" + locationAvailability.isLocationAvailable());
            if (!locationAvailability.isLocationAvailable()) {
                mISplashActivityView.showLocationNotDetected();
            }

        }
    };

    @Override
    public void retryDetectLocation(FusedLocationProviderClient client) {
        detectLocation(client);
    }

    @Override
    public void onLocationDetected(ArrayList<HotelLocation> hotelList) {
        if (mISplashActivityView != null) {
            mISplashActivityView.showLandingScreen(hotelList);
        }
        startBackgroundLocationUpdates();
    }

    @Override
    public void setMvpView(BaseView baseView) {
        mISplashActivityView = (LocationSplashContract.ISplashActivityView) baseView;
    }

    @Override
    public void updateViewWithPermissions() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public BaseView getView() {
        return null;
    }


    public void startLocationUpdates() {
        if (mLocationProviderClient != null) {
            //Remove earlier location updates
            mLocationProviderClient.removeLocationUpdates(mLocationCallback);
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationRequest locationRequest = LocationRequest.create()
                        .setExpirationDuration(1000 * LocationUtils.DURATION_OF_RETRY)
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(1000);
                mLocationProviderClient.requestLocationUpdates(locationRequest, mLocationCallback, null);
                Log.i("Update Location Request", "Started");
            }
        }

    }

    public void setLocationCoordinates(double latitude, double longitude) {
        mLoginManager.setLatitude(latitude);
        mLoginManager.setLongitude(longitude);
        SegmentAnalyticsManager.setmLoginManager(mLoginManager);
    }

    public void startBackgroundLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(1000 * 60);
            mLocationRequest.setFastestInterval(1000 * 20);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(50);

            Intent mRequestLocationUpdatesIntent = new Intent(mContext, LocationService.class);
            mRequestLocationUpdatesPendingIntent = PendingIntent.getService(mContext.getApplicationContext(), 0,
                    mRequestLocationUpdatesIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            mLocationProviderClient.requestLocationUpdates(mLocationRequest, mRequestLocationUpdatesPendingIntent);
        }
    }

    public Observable getDelayObserver() {
        return Observable.timer(2, TimeUnit.SECONDS);
    }
}
