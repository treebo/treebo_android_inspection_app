package com.treebo.prowlapp.service;

/**
 * Created by sumandas on 22/11/2016.
 */

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.LocationResult;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.util.ArrayList;
import java.util.HashSet;

import javax.inject.Inject;

public class LocationService extends IntentService {

    private final String TAG = "LocationService";


    @Inject
    LoginSharedPrefManager mLoginManager;

    @Inject
    RxBus mRxBus;

    public LocationService() {
        super("LocationService");
        MainApplication.get().getApplicationComponent().injectLocationService(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (LocationResult.hasResult(intent)) {
            LocationResult locationResult = LocationResult.extractResult(intent);
            Location location = locationResult.getLastLocation();
            if (location != null && location.getAccuracy() < mLoginManager.getLocationAccuracy()) {
                Log.d(TAG, location.getLatitude() + " " + location.getLongitude());
                mLoginManager.setLatitude(location.getLatitude());
                mLoginManager.setLongitude(location.getLongitude());
                ArrayList<HotelLocation> hotelList = HotelLocation.getHotelForLocation(mLoginManager.getLatitude(), mLoginManager.getLongitude());
                if (hotelList.size() > 0) {
                    if (hotelList.size() == 1) {
                        HotelLocation detectedHotel = hotelList.get(0);
                        Log.d("LocationService", detectedHotel.mHotelName + " " + detectedHotel.mHotelId);
                        if (detectedHotel.mHotelId != mLoginManager.getCurrentHotelLocationId()) {//do not update unless hotel actually changes
                            mLoginManager.setCurrentHotelLocationId(detectedHotel.mHotelId);
                            mLoginManager.setCurrentHotelLocationName(detectedHotel.mHotelName);
                            mRxBus.postEvent(new HotelChangedEvent(hotelList));
                        }
                    } else {
                        Log.d("LocationService", "Multiple Hotels Detected: " + hotelList.toString());
                        ArrayList<Integer> currentHotelsIDList = mLoginManager.getMultipleCurrentHotelLocationIDs();
                        boolean isInList = false;
                        for (HotelLocation hotel: hotelList) {
                            for (Integer id: currentHotelsIDList) {
                                if (hotel.mHotelId == id) {
                                    isInList = true;
                                    break;
                                }
                            }
                        }
                        if (!isInList) {
                            HashSet<String> idSet = new HashSet<>();
                            for (HotelLocation hotel: hotelList) {
                                idSet.add(String.valueOf(hotel.mHotelId));
                            }
                            mLoginManager.setMultipleCurrentHotelLocationIDs(idSet);
                            mRxBus.postEvent(new HotelChangedEvent(hotelList));
                        }
                    }

                } else {
                    Log.d("LocationService", "outside a treebo hotel");
                    if (mLoginManager.getCurrentHotelLocationId() != -1) {
                        mLoginManager.setCurrentHotelLocationId(-1);
                        mRxBus.postEvent(new HotelChangedEvent(hotelList));
                    }

                }

            }
        }
    }
}