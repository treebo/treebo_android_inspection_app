package com.treebo.prowlapp.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowlocation.activity.EnableLocationActivity;
import com.treebo.prowlapp.Utils.LocationUtils;

/**
 * Created by sumandas on 08/12/2016.
 */

public class LocationOnOffReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            if (!LocationUtils.isLocationEnabled(context) && MainApplication.isActivityVisible()) {
                Log.d("LocationOnOffReceiver", "location disabled");
                Intent clearIntent = new Intent(context.getApplicationContext(), EnableLocationActivity.class);
                clearIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(clearIntent);
            }
        }
    }
}
