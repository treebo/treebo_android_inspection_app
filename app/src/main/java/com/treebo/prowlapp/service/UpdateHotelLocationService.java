package com.treebo.prowlapp.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.events.HotelChangedEvent;
import com.treebo.prowlapp.Models.HotelLocation;
import com.treebo.prowlapp.rxbus.RxBus;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import java.util.ArrayList;
import java.util.HashSet;

import javax.inject.Inject;

/**
 * Created by sumandas on 19/11/2016.
 */

public class UpdateHotelLocationService extends IntentService {

    @Inject
    LoginSharedPrefManager mLoginManager;

    @Inject
    RxBus mRxBus;

    public UpdateHotelLocationService() {
        super(UpdateHotelLocationService.class.getName());
        MainApplication.get().getApplicationComponent().injectUpdateService(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        float accuracy = intent.getFloatExtra(LoginSharedPrefManager.PREF_HOTEL_NOT_FOUND_ACCURACY, 0f);
        ArrayList<HotelLocation> hotelList = HotelLocation.getHotelForLocation(mLoginManager.getLatitude(), mLoginManager.getLongitude());
        if (hotelList.size() > 0) {
            if (hotelList.size() == 1) {
                HotelLocation detectedHotel = hotelList.get(0);
                Log.d("UpdateHotel", detectedHotel.mHotelName + " " + detectedHotel.mHotelId);
                mLoginManager.setCurrentHotelLocationId(detectedHotel.mHotelId);
                mLoginManager.setCurrentHotelLocationName(detectedHotel.mHotelName);
                mRxBus.postEvent(new HotelChangedEvent(hotelList));
            } else {
                Log.d("UpdateHotel", "Multiple Hotels Detected: " + hotelList.toString());
                HashSet<String> idSet = new HashSet<>();
                for (HotelLocation location: hotelList) {
                    idSet.add(String.valueOf(location.mHotelId));
                }
                mLoginManager.setMultipleCurrentHotelLocationIDs(idSet);
                mRxBus.postEvent(new HotelChangedEvent(hotelList));
            }
        } else {
            Log.d("UpdateHotel", "outside hotel");
            mLoginManager.setCurrentHotelLocationId(-1);
            mRxBus.postEvent(new HotelChangedEvent(hotelList));
            mLoginManager.setHotelLocationNotFoundAccuracy(accuracy);
        }

    }
}
