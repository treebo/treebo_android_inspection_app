package com.treebo.prowlapp.sharedprefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.job.OfflineJobUtils;
import com.treebo.prowlapp.Models.taskcreationmodel.GeographicalDataResponse;
import com.treebo.prowlapp.response.BankListResponse;
import com.treebo.prowlapp.response.common.RolesAndUserPermissions;
import com.treebo.prowlapp.response.issue.EscalationUserListResponse;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.LocationUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by devesh on 26/04/16.
 */
public class LoginSharedPrefManager {
    private static final String SHARED_PREF_NAME = "prowl_login_prefs";
    public static final String PREF_ACCESS_TOKEN = "access_token";
    public static final String PREF_REFRESH_TOKEN = "refresh_token";
    public static final String PREF_EMAIL_ID = "email_id";
    public static final String PREF_USER_PROFILE_PIC = "profile_pic";
    public static final String PREF_TOKEN_TYPE = "token_type";
    public static final String PREF_USER_ID = "user_id";
    public static final String PREF_PHONE_NO = "phone_no";
    public static final String PREF_DEVICE_ID = "device_id";
    public static final String PREF_IS_DEVICE_REGISTERED = "is_device_registered";

    public static final String FORM_ID = "form_id";

    public static final String PREF_USER_LATITUDE = "latitude";
    public static final String PREF_USER_LONGITUDE = "longitude";
    public static final String PREF_ACCURACY_THRESHOLD = "accuracy";
    public static final String PREF_PERCENT_MIN_MAX = "min_max_percent";
    public static final String PREF_APP_VERSION = "server_app_version";
    public static final String PREF_IS_ISSUE_CREATION_ENABLED = "is_issue_creation_enabled";
    public static final String PREF_HOTEL_NOT_FOUND_ACCURACY = "hotel_not_found_accuracy";
    public static final String PREF_ESCALATION_USER_LIST = "escalation_user_list";
    public static final String PREF_MASTER_BANK_LIST = "master_bank_list";
    public static final String PREF_USER_PERMISSIONS = "user_permissions";
    public static final String PREF_GEOGRAPHICAL_DATA = "geographical_data";

    public static final String HOUSE_COUNT_TOOLTIP_TEXT = "house_count_tooltip_text";
    public static final String PRICE_MISMATCH_TOOLTIP_TEXT = "price_mismatch_tooltip_text";

    public static final String MASTER_CHECKLIST_VERSION = "master_checklist_version";

    public static final String PREF_USER_NAME = "username";
    public static final String PREF_ACCESS_TOKEN_VALIDITY = "access_token_validity";

    public static final String PREF_IS_MASTER_LIST_FETCHED = "is_master_list_fetched";

    public static final String PREF_LAST_MASTER_LIST_FETCH_TIME = "last_masterlist_fetch_time";

    public static final String PREF_IS_HOTEL_LOCATIONS_FETCHED = "is_hotel_locations_fetched";

    public static final String PREF_CURRENT_HOTEL_ID = "current_hotel_id";
    public static final String PREF_MULTIPLE_CURRENT_HOTEL_ID = "multiple_current_hotel_id";

    public static final String PREF_CURRENT_HOTEL_NAME = "current_hotel_name";

    public static final String PREF_IS_CLEAR_COMPLETE_AUDIT_STARTED = "clear_complete_audit_started";

    public static final String PREF_IS_AUDIT_LIST_FETCHED = "is_audit_list_fetched";

    public static final String PREF_IS_PORTFOLIO_LIST_DOWNLOADED = "is_portfolio_list_downloaded";

    private static volatile LoginSharedPrefManager sInstance;

    private volatile SharedPreferences sSharedPreferences;

    public static LoginSharedPrefManager getInstance() {
        LoginSharedPrefManager localInstance = sInstance;
        if (localInstance == null) {
            synchronized (LoginSharedPrefManager.class) {
                if (localInstance == null) {
                    localInstance = sInstance = new LoginSharedPrefManager();
                }
            }
        }
        return localInstance;
    }


    private LoginSharedPrefManager() {
        sSharedPreferences = MainApplication.getAppContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public String getAccessToken() {
        return sSharedPreferences.getString(PREF_ACCESS_TOKEN, null);
    }

    public String getRefreshToken() {
        return sSharedPreferences.getString(PREF_REFRESH_TOKEN, null);
    }


    public void setAccessTokenValidity(Boolean value) {
        sSharedPreferences.edit().putBoolean(PREF_ACCESS_TOKEN_VALIDITY, value).apply();
    }

    public String getEmailId() {
        return sSharedPreferences.getString(PREF_EMAIL_ID, null);
    }

    public String getTokenType() {
        return sSharedPreferences.getString(PREF_TOKEN_TYPE, null);
    }

    public void setUserId(int user_id) {
        sSharedPreferences.edit().putInt(PREF_USER_ID, user_id).apply();
    }

    public int getUserId() {
        return sSharedPreferences.getInt(PREF_USER_ID, 0);
    }

    public String getPrefUserProfilePic() {
        return sSharedPreferences.getString(PREF_USER_PROFILE_PIC, "");
    }

    public void setAccessToken(String accessToken) {
        sSharedPreferences.edit().putString(PREF_ACCESS_TOKEN, accessToken).apply();
    }

    public void setRefreshToken(String refreshToken) {
        sSharedPreferences.edit().putString(PREF_REFRESH_TOKEN, refreshToken).apply();
    }

    public void setTokenType(String tokenType) {
        sSharedPreferences.edit().putString(PREF_TOKEN_TYPE, tokenType).apply();
    }

    public void setEmailId(String emailId) {
        sSharedPreferences.edit().putString(PREF_EMAIL_ID, emailId).apply();
    }

    public void setUserProfilePic(String picUrl) {
        sSharedPreferences.edit().putString(PREF_USER_PROFILE_PIC, picUrl).apply();
    }

    public void setLocationAccuracy(float accuracy) {
        Log.d("Stored accuracy", String.valueOf(accuracy));
        sSharedPreferences.edit().putFloat(PREF_ACCURACY_THRESHOLD, accuracy).apply();
    }

    public void setHouseCountToolTipText(String text) {
        sSharedPreferences.edit().putString(HOUSE_COUNT_TOOLTIP_TEXT, text).apply();
    }

    public void setPriceMismatchToolTipText(String text) {
        sSharedPreferences.edit().putString(PRICE_MISMATCH_TOOLTIP_TEXT, text).apply();
    }

    public void setMasterCheckListVersion(int masterCheckListVersion) {
        sSharedPreferences.edit().putInt(MASTER_CHECKLIST_VERSION, masterCheckListVersion).apply();
    }

    public void setPrefPercentMinMax(float percent) {
        sSharedPreferences.edit().putFloat(PREF_PERCENT_MIN_MAX, percent).apply();
    }

    public void setServerAppVersion(int appVersion) {
        sSharedPreferences.edit().putInt(PREF_APP_VERSION, appVersion).apply();
    }

    public void setIsIssueCreationEnabled(boolean isEnabled) {
        sSharedPreferences.edit().putBoolean(PREF_IS_ISSUE_CREATION_ENABLED, isEnabled).apply();
    }

    public void setEscalationUserList(ArrayList<EscalationUserListResponse.EscalationUser> userList) {
        String json = new Gson().toJson(userList);
        sSharedPreferences.edit().putString(PREF_ESCALATION_USER_LIST, json).apply();
    }

    public void setBankList(ArrayList<BankListResponse.Bank> bankList) {
        String json = new Gson().toJson(bankList);
        sSharedPreferences.edit().putString(PREF_MASTER_BANK_LIST, json).apply();
    }

    public void setUserPermissions(RolesAndUserPermissions response) {
        String json = new Gson().toJson(response);
        sSharedPreferences.edit().putString(PREF_USER_PERMISSIONS, json).apply();
    }

    public void setGeographicalData(GeographicalDataResponse data) {
        String json = new Gson().toJson(data);
        sSharedPreferences.edit().putString(PREF_GEOGRAPHICAL_DATA, json).apply();
    }

    public GeographicalDataResponse getGeographicalData() {
        String json = sSharedPreferences.getString(PREF_GEOGRAPHICAL_DATA, null);
        if (json == null) {
            return new GeographicalDataResponse();
        }
        Type type = new TypeToken<GeographicalDataResponse>() {
        }.getType();
        GeographicalDataResponse geographicalData = new Gson().fromJson(json, type);
        return geographicalData != null ? geographicalData
                : new GeographicalDataResponse();
    }

    public RolesAndUserPermissions.PermissionList getUserPermissionList(int hotelID) {
        String json = sSharedPreferences.getString(PREF_USER_PERMISSIONS, null);
        if (json == null) {
            OfflineJobUtils.fetchUserPermissions();
            return new RolesAndUserPermissions.PermissionList();
        }
        Type type = new TypeToken<RolesAndUserPermissions>() {
        }.getType();
        RolesAndUserPermissions permissions = new Gson().fromJson(json, type);
        return permissions != null ? permissions.getUserPermissionsList(hotelID)
                : new RolesAndUserPermissions.PermissionList();
    }

    public String getUserRolesString(int hotelID, boolean isTruncated) {
        String json = sSharedPreferences.getString(PREF_USER_PERMISSIONS, null);
        if (json == null) {
            OfflineJobUtils.fetchUserPermissions();
            return "";
        }
        Type type = new TypeToken<RolesAndUserPermissions>() {
        }.getType();
        RolesAndUserPermissions permissions = new Gson().fromJson(json, type);
        return permissions != null ? permissions.getAssignedRoles(hotelID, isTruncated) : "";
    }

    public ArrayList<BankListResponse.Bank> getBankList() {
        String json = sSharedPreferences.getString(PREF_MASTER_BANK_LIST, null);
        if (json == null) {
            return new ArrayList<>();
        }
        Type type = new TypeToken<ArrayList<BankListResponse.Bank>>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public ArrayList<EscalationUserListResponse.EscalationUser> getEscalationUserList() {
        String json = sSharedPreferences.getString(PREF_ESCALATION_USER_LIST, null);
        if (json == null) {
            return new ArrayList<>();
        }
        Type type = new TypeToken<ArrayList<EscalationUserListResponse.EscalationUser>>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public int getServerAppVersion() {
        return sSharedPreferences.getInt(PREF_APP_VERSION, 0);
    }

    public float getPercentMinMax() {
        return sSharedPreferences.getFloat(PREF_PERCENT_MIN_MAX, 5.0f);
    }

    public int getMasterCheckListVersion() {
        return sSharedPreferences.getInt(MASTER_CHECKLIST_VERSION, 0);
    }

    public boolean isIssueCreationEnabled() {
        return sSharedPreferences.getBoolean(PREF_IS_ISSUE_CREATION_ENABLED, false);
    }

    public float getLocationAccuracy() {
        return sSharedPreferences.getFloat(PREF_ACCURACY_THRESHOLD, LocationUtils.sMaxAccuracyTolerance);
    }

    public float getHotelLocationNotFoundAccuracy() {
        return sSharedPreferences.getFloat(PREF_HOTEL_NOT_FOUND_ACCURACY, 0f);
    }

    public void setHotelLocationNotFoundAccuracy(float accuracy) {
        sSharedPreferences.edit().putFloat(PREF_HOTEL_NOT_FOUND_ACCURACY, accuracy).apply();
    }

    public String getHouseCountToolTipText() {
        return sSharedPreferences.getString(HOUSE_COUNT_TOOLTIP_TEXT, "");
    }

    public String getPriceMismatchToolTipText() {
        return sSharedPreferences.getString(PRICE_MISMATCH_TOOLTIP_TEXT, "");
    }


    public void clearUserData() {
        setUserId(0);
        setUsername(Constants.EMPTY);
        setAccessToken(Constants.EMPTY);
        setRefreshToken(Constants.EMPTY);
    }

    public void clearPreferences() {
        sSharedPreferences.edit().clear().apply();
    }

    public void setPhoneNo(String phoneNo) {
        sSharedPreferences.edit().putString(PREF_PHONE_NO, phoneNo).apply();
    }

    public String getPhoneNo() {
        return sSharedPreferences.getString(PREF_PHONE_NO, null);
    }

    public void setUsername(String username) {
        sSharedPreferences.edit().putString(PREF_USER_NAME, username).apply();
    }

    public void setFormId(int formId) {
        sSharedPreferences.edit().putInt(FORM_ID, formId).apply();
    }

    public int getFormId() {
        return sSharedPreferences.getInt(FORM_ID, -1);
    }

    public void setLatitude(double latitude) {
        sSharedPreferences.edit().putLong(PREF_USER_LATITUDE, Double.doubleToLongBits(latitude)).apply();
    }

    public double getLatitude() {
        return Double.longBitsToDouble(sSharedPreferences.getLong(PREF_USER_LATITUDE, 0));
    }

    public void setLongitude(double longitude) {
        sSharedPreferences.edit().putLong(PREF_USER_LONGITUDE, Double.doubleToLongBits(longitude)).apply();
    }

    public double getLongitude() {
        return Double.longBitsToDouble(sSharedPreferences.getLong(PREF_USER_LONGITUDE, 0));
    }


    public String getUsername() {
        return sSharedPreferences.getString(PREF_USER_NAME, "User");
    }

    public String getDeviceId() {
        return sSharedPreferences.getString(PREF_DEVICE_ID, null);
    }

    public boolean setDeviceId(String deviceID) {
        return sSharedPreferences.edit().putString(PREF_DEVICE_ID, deviceID).commit();
    }

    public boolean isDeviceRegistered() {
        return sSharedPreferences.getBoolean(PREF_IS_DEVICE_REGISTERED, false);
    }

    public boolean setIsDeviceRegistered(boolean isDeviceRegistered) {
        return sSharedPreferences.edit().putBoolean(PREF_IS_DEVICE_REGISTERED, isDeviceRegistered).commit();
    }

    public boolean isUserLoggedIn() {
        String username = getEmailId();
        String token = getAccessToken();
        int userId = getUserId();
        return (!TextUtils.isEmpty(token) && !TextUtils.isEmpty(username) && userId > 0);
    }

    public boolean isMasterListFetched() {
        return sSharedPreferences.getBoolean(PREF_IS_MASTER_LIST_FETCHED, false);
    }

    public void setMasterListFetched(boolean isFetched) {
        sSharedPreferences.edit().putBoolean(PREF_IS_MASTER_LIST_FETCHED, isFetched).apply();
    }

    public boolean isAuditListFetched() {
        return sSharedPreferences.getBoolean(PREF_IS_AUDIT_LIST_FETCHED, false);
    }

    public void setAuditListFetched(boolean isFetched) {
        sSharedPreferences.edit().putBoolean(PREF_IS_AUDIT_LIST_FETCHED, isFetched).apply();
    }


    public boolean isPortfolioListDownloaded() {
        return sSharedPreferences.getBoolean(PREF_IS_PORTFOLIO_LIST_DOWNLOADED, false);
    }

    public void setPrefIsPortfolioListDownloaded(boolean isFetched) {
        sSharedPreferences.edit().putBoolean(PREF_IS_PORTFOLIO_LIST_DOWNLOADED, isFetched).apply();
    }

    public boolean isHotelLocationsFetched() {
        return sSharedPreferences.getBoolean(PREF_IS_HOTEL_LOCATIONS_FETCHED, false);
    }

    public void setHotelLocationsFetched(boolean isFetched) {
        sSharedPreferences.edit().putBoolean(PREF_IS_HOTEL_LOCATIONS_FETCHED, isFetched).apply();
    }

    public int getCurrentHotelLocationId() {
        Log.d("Get Hotel Location", "" + sSharedPreferences.getInt(PREF_CURRENT_HOTEL_ID, -1));
        return sSharedPreferences.getInt(PREF_CURRENT_HOTEL_ID, -1);
    }

    public void setCurrentHotelLocationId(int hotelID) {
        sSharedPreferences.edit().putInt(PREF_CURRENT_HOTEL_ID, hotelID).apply();
    }

    public void setMultipleCurrentHotelLocationIDs(Set<String> idList) {
        sSharedPreferences.edit().putStringSet(PREF_MULTIPLE_CURRENT_HOTEL_ID, idList).apply();
    }

    public ArrayList<Integer> getMultipleCurrentHotelLocationIDs() {
        ArrayList<Integer> list = new ArrayList<>();
        Set<String> stringSet = sSharedPreferences.getStringSet(PREF_MULTIPLE_CURRENT_HOTEL_ID, new HashSet<>());
        for (String id : stringSet) {
            list.add(Integer.valueOf(id));
        }
        return list;
    }

    public String getCurrentHotelLocationName() {
        Log.d("Get Hotel Location", "" + sSharedPreferences.getString(PREF_CURRENT_HOTEL_NAME, ""));
        return sSharedPreferences.getString(PREF_CURRENT_HOTEL_NAME, "");
    }

    public void setCurrentHotelLocationName(String hotelName) {
        sSharedPreferences.edit().putString(PREF_CURRENT_HOTEL_NAME, hotelName).apply();
    }

    public boolean isClearCompleteAuditJobStarted() {
        return sSharedPreferences.getBoolean(PREF_IS_CLEAR_COMPLETE_AUDIT_STARTED, false);
    }

    public boolean setCompleteAuditJobStarted(boolean isStarted) {
        return sSharedPreferences.edit().putBoolean(PREF_IS_CLEAR_COMPLETE_AUDIT_STARTED, isStarted).commit();
    }

    public String getLastMasterListFetchTime() {
        return sSharedPreferences.getString(PREF_LAST_MASTER_LIST_FETCH_TIME, "");
    }

    public void setLastMasterListFetchTime(String timestamp) {
        sSharedPreferences.edit().putString(PREF_LAST_MASTER_LIST_FETCH_TIME, timestamp).apply();
    }

    public String getCurrentLatAndLong() {
        return String.valueOf(getLatitude()) + ", " + String.valueOf(getLongitude());
    }
}
