package com.treebo.prowlapp.sharedprefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.Models.Form;
import com.treebo.prowlapp.Models.RootCauseData;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by sumandas on 02/05/2016.
 */
@Deprecated
public class AuditPreferenceManager {
    private static final String SHARED_PREF_NAME = "audited_area_pref";

    private static volatile AuditPreferenceManager sInstance;
    private volatile SharedPreferences sSharedPreferences;

    public static AuditPreferenceManager getInstance() {
        AuditPreferenceManager localInstance = sInstance;
        if (localInstance == null) {
            synchronized (AuditPreferenceManager.class) {
                if(localInstance==null){
                    localInstance = sInstance = new AuditPreferenceManager();
                }
            }
        }
        return localInstance;
    }

    private AuditPreferenceManager() {
        sSharedPreferences = MainApplication.getAppContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public void clearPreferences() {
        sSharedPreferences.edit().clear().commit();
    }

    //TODO remove this
    public void removeAndPersistCommonAreaAudit(String hotelId,Form formToPersist){
        sSharedPreferences.edit().remove(hotelId).commit();
        sSharedPreferences.edit().putString(hotelId, new Gson().toJson(formToPersist)).commit();
    }

    public Form getPersistedCommonAreaAudit(String hotelId){
        return  getPersistedAudit(hotelId);

    }

    //TODO remove this
    public void removeAndPersistRoomAreaAudit(String hotelId,String roomNo,Form formToPersist) {
        String key = hotelId+"_"+roomNo;
        sSharedPreferences.edit().remove(key).commit();
        sSharedPreferences.edit().putString(key, new Gson().toJson(formToPersist)).commit();
    }

    public void removeAndPersistAudit(String key,Form formToPersist){
        sSharedPreferences.edit().remove(key).commit();
        sSharedPreferences.edit().putString(key, new Gson().toJson(formToPersist)).commit();
    }

    public Form getPersistedRoomAreaAudit(String hotelId, String roomNo) {
        String key = hotelId+"_"+roomNo;
        return  getPersistedAudit(key);
    }

    public void removeAudit(String key){
        sSharedPreferences.edit().remove(key).commit();
    }

    public void persistRootCauseList(RootCauseData rootCause){
        sSharedPreferences.edit().putString("root_causes", new Gson().toJson(rootCause)).commit();
    }

    public void persistLastAuditKey(String key){
        String auditkeyList=sSharedPreferences.getString("audit_list","");
        if(auditkeyList.isEmpty()){
            sSharedPreferences.edit().putString("audit_list",key).commit();
        }else{
            ArrayList<String> newList=new ArrayList<>();
            String [] listSet=auditkeyList.split(",");
            for(int i=0;i<listSet.length;i++){
                if(!listSet[i].equals(key)){
                    newList.add(listSet[i]);
                }
            }
            newList.add(0, key);
            String addedKey=TextUtils.join(",", newList);
            sSharedPreferences.edit().putString("audit_list",addedKey).commit();

        }


    }

    public void removeLastAuditKey(String key){
        String auditkeyList=sSharedPreferences.getString("audit_list","");
        if(auditkeyList.isEmpty()){
            sSharedPreferences.edit().putString("audit_list",key).commit();
        }else{
            ArrayList<String> newList=new ArrayList<>();
            String [] listSet=auditkeyList.split(",");
            for(int i=0;i<listSet.length;i++){
                if(!listSet[i].equals(key)){
                    newList.add(listSet[i]);
                }
            }
            String addedKey=TextUtils.join(",",newList);
            sSharedPreferences.edit().putString("audit_list",addedKey).commit();
        }

    }

    public Form getLastPausedAuditForHotel(String hotelId){
        String auditkeyList=sSharedPreferences.getString("audit_list","");
        if(auditkeyList.isEmpty()){
            return null;
        }else{
            String [] listSet=auditkeyList.split(",");
            for(int i=0;i<listSet.length;i++){
                if(listSet[i].startsWith(hotelId)){
                    return getPersistedAudit(listSet[i]);
                }
            }
            return null;
        }
    }

    public boolean isAuditRowUpdated(){
        return sSharedPreferences.getBoolean("audit_row_updated",false);
    }

    public void setAuditRowUpdated(boolean flag){
         sSharedPreferences.edit().putBoolean("audit_row_updated",flag).commit();
    }


    public RootCauseData getRootCauseList(){
        String json = sSharedPreferences.getString("root_causes", null);
        if(json == null){
            return null;
        }
        Type type = new TypeToken<RootCauseData>() {}.getType();
        RootCauseData rootCauseData = new Gson().fromJson(json, type);
        return rootCauseData;
    }

    public Form getPersistedAudit(String key){
        String json = sSharedPreferences.getString(key, null);
        if(json == null){
            return null;
        }
        Type type = new TypeToken<Form>() {}.getType();
        Form form = new Gson().fromJson(json, type);
        return form;
    }

}
