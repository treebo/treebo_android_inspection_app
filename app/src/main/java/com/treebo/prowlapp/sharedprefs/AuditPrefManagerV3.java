package com.treebo.prowlapp.sharedprefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.flowauditnew.AuditUtilsV3;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.Models.auditmodel.AuditSaveObject;
import com.treebo.prowlapp.Models.auditmodel.Checkpoint;
import com.treebo.prowlapp.Models.auditmodel.FraudAuditSaveObject;
import com.treebo.prowlapp.Models.auditmodel.FraudAuditSubmitObject;
import com.treebo.prowlapp.Models.issuemodel.IssueListModelV3;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.DatabaseUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sumandas on 11/11/2016.
 */

public class AuditPrefManagerV3 {

    private static final String SHARED_PREF_NAME = "audited_area_pref_V3";

    private static final String PREF_CURRENT_AUDIT_KEY = "pref_current_audit_key";
    private static final String PREF_CURRENT_CHECKPOINT_KEY = "pref_current_checkpoint_key";

    private static final String PREF_CURRENT_AUDIT_ROOM_ID = "pref_current_room_id";


    private static volatile AuditPrefManagerV3 sInstance;
    private volatile SharedPreferences sSharedPreferences;

    public static AuditPrefManagerV3 getInstance() {
        AuditPrefManagerV3 localInstance = sInstance;
        if (localInstance == null) {
            synchronized (AuditPrefManagerV3.class) {
                localInstance = sInstance = new AuditPrefManagerV3();
            }
        }
        return localInstance;
    }

    private AuditPrefManagerV3() {
        sSharedPreferences = MainApplication.getAppContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public void clearPreferences() {
        sSharedPreferences.edit().clear().commit();
    }

    public void saveAuditCheckPoint(String key, AnswerV3 issueRaised) {
        AuditSaveObject auditSaveObject = getSavedAudit(key);
        if (auditSaveObject != null) {
            boolean isInList = false;
            for (int i = 0; i < auditSaveObject.mAnswerList.size(); i++) {
                AnswerV3 issue = auditSaveObject.mAnswerList.get(i);
                if (issue.getSubcheckpoint_id() != null && issue.getSubcheckpoint_id().equals(issueRaised.getSubcheckpoint_id())) {
                    auditSaveObject.mAnswerList.set(i, issueRaised);
                    isInList = true;
                }
            }
            if (!isInList)
                auditSaveObject.mAnswerList.add(issueRaised);
        } else {
            auditSaveObject = new AuditSaveObject();
            auditSaveObject.mAnswerList.add(issueRaised);
        }
        sSharedPreferences.edit().putString(key, new Gson().toJson(auditSaveObject)).commit();

    }

    public void updateAuditCheckPoint(String key, AnswerV3 issueRaised) {
        AuditSaveObject auditSaveObject = getSavedAudit(key);
        for (int i = 0; i < auditSaveObject.mAnswerList.size(); i++) {
            AnswerV3 issue = auditSaveObject.mAnswerList.get(i);
            if (issue.getSubcheckpoint_id() != null && issue.getSubcheckpoint_id().equals(issueRaised.getSubcheckpoint_id()))
                auditSaveObject.mAnswerList.set(i, issueRaised);
        }
        sSharedPreferences.edit().putString(key, new Gson().toJson(auditSaveObject)).commit();

    }

    public void cacheCheckpoints(String key, HashMap<Integer, ArrayList<Integer>> categoryMap) {
        sSharedPreferences.edit().putString(key, new Gson().toJson(categoryMap)).commit();
    }

    public void removeCachedCheckPoints(String key) {
        sSharedPreferences.edit().remove(key).commit();
    }

    public HashMap<Integer, ArrayList<Integer>> getCachedCheckPoints(String key) {
        String json = sSharedPreferences.getString(key, null);
        if (json == null) {
            return null;
        }
        Type type = new TypeToken<HashMap<Integer, ArrayList<Integer>>>() {
        }.getType();
        HashMap<Integer, ArrayList<Integer>> checkpointMap = new Gson().fromJson(json, type);
        return checkpointMap;
    }


    public void removeAuditCheckPoint(String key, AnswerV3 issueRaised) {
        AuditSaveObject auditSaveObject = getSavedAudit(key);
        if (auditSaveObject != null) {
            for (AnswerV3 answerV3 : auditSaveObject.mAnswerList) {
                if (issueRaised.equals(answerV3)) {
                    auditSaveObject.mAnswerList.remove(answerV3);
                    break;
                }
            }
            sSharedPreferences.edit().putString(key, new Gson().toJson(auditSaveObject)).commit();
        }
    }

    public void removeCheckPointFromSavedList(String key, Checkpoint checkpoint) {
        AuditSaveObject auditSaveObject = getSavedAudit(key);
        if (auditSaveObject != null) {
            for (AnswerV3 answerV3 : auditSaveObject.mAnswerList) {
                if ((checkpoint.getmDescription().equals(answerV3.getCheckpointText()))) {
                    auditSaveObject.mAnswerList.remove(answerV3);
                    break;
                }
            }
            sSharedPreferences.edit().putString(key, new Gson().toJson(auditSaveObject)).commit();
        }
    }


    public void removeAuditDoneCheckPoint(String key, AnswerV3 categoryDone) {
        AuditSaveObject auditSaveObject = getSavedAudit(key);
        if (auditSaveObject != null) {
            for (int i = 0; i < auditSaveObject.mAnswerList.size(); i++) {
                AnswerV3 answerV3 = auditSaveObject.mAnswerList.get(i);
                if (categoryDone.auditDoneEquals(answerV3)) {
                    auditSaveObject.mAnswerList.remove(i);
                    break;
                }
            }
            sSharedPreferences.edit().putString(key, new Gson().toJson(auditSaveObject)).commit();
        }
    }

    public ArrayList<AnswerV3> getAllIssuesRaisedInARoom(int auditType, int hotelId, String roomNo) {
        AuditSaveObject saveObject = getSavedAudit(AuditUtilsV3.createAuditKey(hotelId, auditType, roomNo));
        ArrayList<AnswerV3> issueList = new ArrayList<>();
        if (saveObject == null) {
            return issueList;
        } else {
            for (AnswerV3 answerV3 : saveObject.mAnswerList) {
                if (!answerV3.isAuditDone) {
                    answerV3.setCategoryText(DatabaseUtils.getCategoryText(answerV3.getCategory_id()));
                    answerV3.setCheckpointText(DatabaseUtils.getCheckpointText(answerV3.getCheckpoint_id()));
                    answerV3.setSubcheckpointText(DatabaseUtils.getSubCheckpointText(answerV3.getSubcheckpoint_id()));
                    issueList.add(answerV3);
                }
            }
        }
        return issueList;
    }

    public void savePreMarkedIssues(String key, ArrayList<IssueListModelV3> preMarkedIssues) {
        AuditSaveObject auditSaveObject = getSavedAudit(key);
        if (auditSaveObject == null) {
            auditSaveObject = new AuditSaveObject();
        }
        auditSaveObject.setPreMarkedIssues(preMarkedIssues);
        sSharedPreferences.edit().putString(key, new Gson().toJson(auditSaveObject)).commit();
    }

    public AuditSaveObject getSavedAudit(String key) {
        String json = sSharedPreferences.getString(key, null);
        if (json == null) {
            return null;
        }
        Type type = new TypeToken<AuditSaveObject>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public void removeAudit(String key) {
        sSharedPreferences.edit().remove(key).commit();
    }


    public void setCurrentAuditKey(String key) {
        sSharedPreferences.edit().putString(PREF_CURRENT_AUDIT_KEY, key).commit();
    }

    public void clearCurrentAuditKey() {
        sSharedPreferences.edit().putString(PREF_CURRENT_AUDIT_KEY, "").commit();
    }

    public String getCurrentAuditKey() {
        return sSharedPreferences.getString(PREF_CURRENT_AUDIT_KEY, "");
    }

    public int getCurrentRoomId() {
        return sSharedPreferences.getInt(PREF_CURRENT_AUDIT_ROOM_ID, -1);
    }

    public void setCurrentRoomId(int roomId) {
        sSharedPreferences.edit().putInt(PREF_CURRENT_AUDIT_ROOM_ID, roomId).commit();
    }

    public FraudAuditSaveObject getSavedFraudAudit(String key) {
        String json = sSharedPreferences.getString(key, null);
        if (json == null) {
            return null;
        }
        Type type = new TypeToken<FraudAuditSaveObject>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public void saveFraudAuditObject(String key, FraudAuditSaveObject auditSaveObject) {
        sSharedPreferences.edit().putString(key, new Gson().toJson(auditSaveObject)).commit();
    }

    public void saveFraudAuditCheckpoint(String key, FraudAuditSubmitObject fraudObject) {
        FraudAuditSaveObject auditSaveObject = getSavedFraudAudit(key);
        if (auditSaveObject != null) {
            boolean isInList = false;
            for (int i = 0; i < auditSaveObject.getList().size(); i++) {
                FraudAuditSubmitObject issue = auditSaveObject.getList().get(i);
                if (((issue.getFraud_type().equals(Constants.HOUSE_COUNT_AUDIT_TEXT) && isFraudTypeEqual(issue.getFraud_type(), fraudObject.getFraud_type()) && issue.getRoomNumber().equals(fraudObject.getRoomNumber()))
                        || (issue.getFraud_type().equals(Constants.PRICE_MISMATCH_AUDIT) && isFraudTypeEqual(issue.getFraud_type(), fraudObject.getFraud_type()) && issue.getRoomNumber().equals(fraudObject.getRoomNumber()))
                        || (issue.getLevel2ID() != null && issue.getLevel2ID().equals(fraudObject.getLevel2ID())))) {
                    auditSaveObject.getList().set(i, fraudObject);
                    isInList = true;
                }
            }
            if (!isInList)
                auditSaveObject.getList().add(fraudObject);
        } else {
            auditSaveObject = new FraudAuditSaveObject();
            auditSaveObject.setList(new ArrayList<>());
            auditSaveObject.getList().add(fraudObject);
        }
        sSharedPreferences.edit().putString(key, new Gson().toJson(auditSaveObject)).commit();
    }

    private boolean isFraudTypeEqual(String fraudType1, String fraudType2) {
        return fraudType1.equals(fraudType2);
    }

    public ArrayList<FraudAuditSubmitObject> getAllFraudIssuesRaisedInARoom(int hotelId, int auditType) {
        FraudAuditSaveObject saveObject = getSavedFraudAudit(AuditUtilsV3.createAuditKey(hotelId,
                auditType, null));
        ArrayList<FraudAuditSubmitObject> issueList = new ArrayList<>();
        if (saveObject == null) {
            return issueList;
        } else {
            issueList.addAll(saveObject.getList());
        }
        return issueList;
    }
}
