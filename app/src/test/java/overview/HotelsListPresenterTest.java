package overview;

import com.treebo.prowlapp.flowhotelview.observer.HotelsListSubscriber;
import com.treebo.prowlapp.flowhotelview.presenter.HotelsListPresenter;
import com.treebo.prowlapp.Models.HotelDataResponse;
import com.treebo.prowlapp.mvpviews.HotelsListMVPView;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.HotelsListResponse;
import com.treebo.prowlapp.Utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by sumandas on 17/08/2016.
 */
public class HotelsListPresenterTest extends HotelsListPresenter {

    @Mock
    HotelsListMVPView hotelsListMVPView;

    @Mock
    RestClient mRestClient;

    HotelsListPresenter mPresenter;


    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mPresenter=new HotelsListPresenter();
        mPresenter.setMvpView(hotelsListMVPView);
    }

    @Test
    public void testGetHotels_success() {
        HotelsListResponse hotelsListResponse=getHotelResponse();
        hotelsListResponse.status = Constants.STATUS_SUCCESS;
        hotelsListResponse.msg = "";
        Mockito.when(mRestClient.getAllHotels(Mockito.anyInt())).thenReturn(Observable.just(hotelsListResponse));
        GetAllHotelsUseCaseTest getAllHotelsUseCaseTest = new GetAllHotelsUseCaseTest(mRestClient);
        getAllHotelsUseCaseTest.setmUserId(1);
        HotelsListSubscriber hotelsListSubscriber = new HotelsListSubscriber(mPresenter, getAllHotelsUseCaseTest, hotelsListMVPView);
        mPresenter.getAllHotels(getAllHotelsUseCaseTest, hotelsListSubscriber);
        Mockito.verify(hotelsListMVPView).onHotelsListSuccess(Mockito.any(HotelsListResponse.class));

    }

    @Test
    public void testGetHotels_failure() {
        HotelsListResponse hotelsListResponse=getHotelResponse();
        hotelsListResponse.status = "";
        hotelsListResponse.msg = "";
        Mockito.when(mRestClient.getAllHotels(Mockito.anyInt())).thenReturn(Observable.just(hotelsListResponse));
        GetAllHotelsUseCaseTest getAllHotelsUseCaseTest = new GetAllHotelsUseCaseTest(mRestClient);
        getAllHotelsUseCaseTest.setmUserId(1);
        HotelsListSubscriber hotelsListSubscriber = new HotelsListSubscriber(mPresenter, getAllHotelsUseCaseTest, hotelsListMVPView);
        mPresenter.getAllHotels(getAllHotelsUseCaseTest, hotelsListSubscriber);
        Mockito.verify(hotelsListMVPView).onHotelsListFailed(Mockito.anyString());

    }

    public HotelsListResponse getHotelResponse(){
        HotelsListResponse hotelsListResponse = new HotelsListResponse();
        hotelsListResponse.data=new HotelsListResponse.HotelsListResponseData();
        hotelsListResponse.data.hotels=new ArrayList<>();
        hotelsListResponse.data.hotels.add(new HotelDataResponse());
        return hotelsListResponse;
    }



    }
