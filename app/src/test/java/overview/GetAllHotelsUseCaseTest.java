package overview;

import com.treebo.prowlapp.flowhotelview.usecase.GetAllHotelsUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.HotelsListResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by sumandas on 17/08/2016.
 */
public class GetAllHotelsUseCaseTest extends GetAllHotelsUseCase {

    public GetAllHotelsUseCaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<HotelsListResponse> buildObservable() {
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }
}
