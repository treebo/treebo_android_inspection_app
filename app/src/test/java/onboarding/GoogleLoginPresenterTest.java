package onboarding;

import android.content.Context;
import android.text.TextUtils;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.treebo.prowlapp.flowOnboarding.OnBoardingContract;
import com.treebo.prowlapp.flowOnboarding.observers.GoogleLoginObserver;
import com.treebo.prowlapp.flowOnboarding.presenter.GoogleLoginPresenter;
import com.treebo.prowlapp.flowOnboarding.usecase.GoogleLoginUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.Utils.Utils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.google.android.gms.common.ConnectionResult.SUCCESS;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by sumandas on 22/11/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Utils.class, TextUtils.class})
public class GoogleLoginPresenterTest  {

    @Mock
    OnBoardingContract.IGoogleLoginView mGoogleView;

    @Mock
    RestClient mRestClient;

    @Mock
    Context mContext;

    GoogleLoginPresenter mGoogleLoginPresenter;

    GoogleLoginPresenter mGoogleLoginSpyPresenter;

    @Mock
    GoogleSignInAccount mGoogleSignInAccount;

    @Mock
    GoogleSignInResult mGoogleSignInResult;

    @Mock
    GoogleLoginObserver mGoogleLoginObserver;

    @Mock
    GoogleLoginUseCase mGoogleSignInUseCase;



    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Utils.class);

        mGoogleLoginPresenter=new GoogleLoginPresenter(mContext);
        mGoogleLoginPresenter.setMvpView(mGoogleView);

        mGoogleLoginSpyPresenter=Mockito.spy(mGoogleLoginPresenter);

        PowerMockito.mockStatic(TextUtils.class);

        PowerMockito.when(TextUtils.isEmpty(Mockito.any(CharSequence.class))).thenAnswer(new Answer<Object>() {
            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {
                CharSequence str = (CharSequence) invocation.getArguments()[0];
                return !(str != null && str.length() > 0);
            }
        });

    }

    @Test
    public void onCreate_test_google_play_services_available(){
        when(mGoogleView.checkForPlayServices()).thenReturn(SUCCESS);
        mGoogleLoginPresenter.onCreate();
        Mockito.verify(mGoogleView,never()).showPlayServicesErrorDialog(SUCCESS);

    }

    @Test
    public void onCreate_test_google_play_services_not_available(){
        int retVal=ConnectionResult.SIGN_IN_FAILED;
        PowerMockito.when(Utils.isPlayServicesErrorUserRecoverable(Mockito.anyInt())).thenReturn(true);
        when(mGoogleView.checkForPlayServices()).thenReturn(retVal);
        mGoogleLoginPresenter.onCreate();
        Mockito.verify(mGoogleView).showPlayServicesErrorDialog(retVal);

        retVal=ConnectionResult.NETWORK_ERROR;
        when(mGoogleView.checkForPlayServices()).thenReturn(retVal);
        mGoogleLoginPresenter.onCreate();
        Mockito.verify(mGoogleView).showPlayServicesErrorDialog(retVal);

        retVal=ConnectionResult.API_UNAVAILABLE;
        when(mGoogleView.checkForPlayServices()).thenReturn(retVal);
        mGoogleLoginPresenter.onCreate();
        Mockito.verify(mGoogleView).showPlayServicesErrorDialog(retVal);

        retVal=ConnectionResult.INTERNAL_ERROR;
        when(mGoogleView.checkForPlayServices()).thenReturn(retVal);
        mGoogleLoginPresenter.onCreate();
        Mockito.verify(mGoogleView).showPlayServicesErrorDialog(retVal);

        retVal=ConnectionResult.INVALID_ACCOUNT;
        when(mGoogleView.checkForPlayServices()).thenReturn(retVal);
        mGoogleLoginPresenter.onCreate();
        Mockito.verify(mGoogleView).showPlayServicesErrorDialog(retVal);


    }

    @Test
    public void google_signIn_button_clicked_play_not_enabled(){
        mGoogleLoginPresenter.mPlayServiceAvailabilityStatus=ConnectionResult.SIGN_IN_FAILED;
        PowerMockito.when(Utils.isPlayServicesErrorUserRecoverable(Mockito.anyInt())).thenReturn(true);
        mGoogleLoginPresenter.googleSignInButtonClicked();
        Mockito.verify(mGoogleView).showPlayServicesErrorDialog(mGoogleLoginPresenter.mPlayServiceAvailabilityStatus);

    }

    @Test
    public void google_signIn_button_net_connected(){
        mGoogleLoginPresenter.mPlayServiceAvailabilityStatus=ConnectionResult.SUCCESS;
        PowerMockito.when(Utils.isPlayServicesErrorUserRecoverable(Mockito.anyInt())).thenReturn(true);
        PowerMockito.when(Utils.isInternetConnected(mContext)).thenReturn(true);
        mGoogleLoginPresenter.googleSignInButtonClicked();
        Mockito.verify(mGoogleView).startGoogleSignIn();

    }

    @Test
    public void google_signIn_button_net_not_connected(){
        mGoogleLoginPresenter.mPlayServiceAvailabilityStatus=ConnectionResult.SUCCESS;
        PowerMockito.when(Utils.isPlayServicesErrorUserRecoverable(Mockito.anyInt())).thenReturn(true);
        PowerMockito.when(Utils.isInternetConnected(mContext)).thenReturn(false);
        mGoogleLoginPresenter.googleSignInButtonClicked();
        Mockito.verify(mGoogleView).showNoInternetScreen();

    }

    @Test
    public void perform_google_sign_in_result_success_account_not_null(){
        when(mGoogleSignInResult.isSuccess()).thenReturn(true);
        when(mGoogleSignInResult.getSignInAccount()).thenReturn(mGoogleSignInAccount);
        String idToken="aabcdeg";
        when(mGoogleSignInAccount.getIdToken()).thenReturn(idToken);
        mGoogleLoginSpyPresenter.performGoogleLogin(mGoogleSignInResult,mGoogleSignInUseCase,mGoogleLoginObserver);
        verify(mGoogleLoginSpyPresenter).sendGoogleIdToken(idToken,mGoogleSignInUseCase,mGoogleLoginObserver);
    }

    @Test
    public void perform_google_sign_in_result_success_account_null(){
        when(mGoogleSignInResult.isSuccess()).thenReturn(true);
        when(mGoogleSignInResult.getSignInAccount()).thenReturn(null);
        mGoogleLoginPresenter.performGoogleLogin(mGoogleSignInResult,mGoogleSignInUseCase,mGoogleLoginObserver);
        verify(mGoogleView).onLoginFailure(any());

    }

    @Test
    public void perform_google_sign_in_result_failure_account_null(){
        when(mGoogleSignInResult.isSuccess()).thenReturn(false);
        mGoogleLoginPresenter.performGoogleLogin(mGoogleSignInResult,mGoogleSignInUseCase,mGoogleLoginObserver);
        verify(mGoogleView).onLoginFailure(any());

    }

    @Test
    public void sendToken_null(){
       mGoogleLoginPresenter.sendGoogleIdToken(null,mGoogleSignInUseCase,mGoogleLoginObserver);
        verify(mGoogleView).onLoginFailure(any());
    }

    @Test
    public void sendToken_empty(){
        mGoogleLoginPresenter.sendGoogleIdToken("",mGoogleSignInUseCase,mGoogleLoginObserver);
        verify(mGoogleView).onLoginFailure(any());
    }

    @Test
    public void sendToken_valid(){
        String idToken="aabcdeg";
        mGoogleLoginPresenter.sendGoogleIdToken(idToken,mGoogleSignInUseCase,mGoogleLoginObserver);
        verify(mGoogleSignInUseCase).execute(mGoogleLoginObserver);
    }

}
