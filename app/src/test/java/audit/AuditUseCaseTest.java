package audit;

import com.treebo.prowlapp.flowaudit.usecase.AuditUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by sumandas on 19/08/2016.
 */
public class AuditUseCaseTest extends AuditUseCase {

    public AuditUseCaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<SubmitAuditResponse> buildObservable(){
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }
}
