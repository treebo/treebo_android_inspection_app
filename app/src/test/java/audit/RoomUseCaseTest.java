package audit;

import com.treebo.prowlapp.flowaudit.usecase.HotelRoomsUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.HotelRoomResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by sumandas on 19/08/2016.
 */
public class RoomUseCaseTest extends HotelRoomsUseCase {

    public RoomUseCaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<HotelRoomResponse> buildObservable(){
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }
}
