package audit;

import com.treebo.prowlapp.flowaudit.observers.AuditSubmitObserver;
import com.treebo.prowlapp.flowaudit.presenter.AuditSubmitPresenter;
import com.treebo.prowlapp.flowaudit.presenter.AuditSubmitPresenterImpl;
import com.treebo.prowlapp.Models.AnswerListWithSectionName;
import com.treebo.prowlapp.mvpviews.AuditAreaView;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.Utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import rx.Observable;

/**
 * Created by sumandas on 19/08/2016.
 */
public class AuditSubmitPresenterTest {


    @Mock
    RestClient mRestClient;

    @Mock
    AuditAreaView mAuditAreaView;

    AuditSubmitPresenter mAuditSubmitPresenter;


    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mAuditSubmitPresenter=new AuditSubmitPresenterImpl();
        mAuditSubmitPresenter.setMvpView(mAuditAreaView);
    }

    @Test
    public void onAuditSubmit_success(){
        SubmitAuditResponse submitAuditResponse=new SubmitAuditResponse();
        submitAuditResponse.data=new SubmitAuditResponse.AuditStatus();
        submitAuditResponse.status= Constants.STATUS_SUCCESS;
        Mockito.when(mRestClient.submitAudit(Mockito.any(AnswerListWithSectionName.class),Mockito.anyString(),
                Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(Observable.just(submitAuditResponse));

        AuditUseCaseTest auditUseCaseTest=new AuditUseCaseTest(mRestClient);
        AuditSubmitObserver auditSubmitObserver=new AuditSubmitObserver(mAuditSubmitPresenter,auditUseCaseTest,mAuditAreaView);

        mAuditSubmitPresenter.onAuditSubmit(Mockito.mock(AnswerListWithSectionName.class),
                1,1,1,"QA",auditUseCaseTest,auditSubmitObserver);

        Mockito.verify(mAuditAreaView).onAuditSubmitSuccess(Mockito.any(SubmitAuditResponse.class));

    }

    @Test
    public void onAuditSubmit_failure(){

        SubmitAuditResponse submitAuditResponse = new SubmitAuditResponse();
        submitAuditResponse.data = new SubmitAuditResponse.AuditStatus();
        submitAuditResponse.status = "";
        Mockito.when(mRestClient.submitAudit(Mockito.any(AnswerListWithSectionName.class), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(Observable.just(submitAuditResponse));

        AuditUseCaseTest auditUseCaseTest = new AuditUseCaseTest(mRestClient);
        AuditSubmitObserver auditSubmitObserver = new AuditSubmitObserver(mAuditSubmitPresenter, auditUseCaseTest, mAuditAreaView);

        mAuditSubmitPresenter.onAuditSubmit(Mockito.mock(AnswerListWithSectionName.class),
                1, 1, 1, "QA", auditUseCaseTest, auditSubmitObserver);

        Mockito.verify(mAuditAreaView).onAuditSubmitFailed(Mockito.any(SubmitAuditResponse.class));

    }
}
