package audit;


import com.treebo.prowlapp.flowaudit.observers.HotelAuditAreaObserver;
import com.treebo.prowlapp.flowaudit.observers.HotelRoomObserver;
import com.treebo.prowlapp.flowaudit.presenter.HotelAuditPresenter;
import com.treebo.prowlapp.flowaudit.presenter.HotelAuditPresenterImpl;
import com.treebo.prowlapp.Models.Audit;
import com.treebo.prowlapp.response.FormDataResponse;
import com.treebo.prowlapp.Models.FormUpdateOutputModel;
import com.treebo.prowlapp.mvpviews.HotelAuditView;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.HotelRoomResponse;
import com.treebo.prowlapp.Utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by sumandas on 19/08/2016.
 */
public class HotelAuditPresenterTest {

    @Mock
    RestClient mRestClient;

    @Mock
    HotelAuditView mHotelAuditView;

    HotelAuditPresenter mHotelsAuditPresenter;

    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mHotelsAuditPresenter=new HotelAuditPresenterImpl();
        mHotelsAuditPresenter.setMvpView(mHotelAuditView);
    }

    @Test
    public void loadRoom_success(){
        HotelRoomResponse hotelRoomResponse=getHotelRoomResponse();
        hotelRoomResponse.status= Constants.STATUS_SUCCESS;
        Mockito.when(mRestClient.getHotelRooms(Mockito.anyInt())).thenReturn(Observable.just(hotelRoomResponse));
        RoomUseCaseTest roomUsecaserTest=new RoomUseCaseTest(mRestClient);
        HotelRoomObserver roomObserver=new HotelRoomObserver(mHotelsAuditPresenter,roomUsecaserTest,mHotelAuditView);
        mHotelsAuditPresenter.loadRooms(1,roomUsecaserTest,roomObserver);
         Mockito.verify(mHotelAuditView).onLoadHotelRooms(Mockito.any(),Mockito.any());

    }


    @Test
    public void loadRoom_failure(){
        HotelRoomResponse hotelRoomResponse=getHotelRoomResponse();
        hotelRoomResponse.status= "";
        hotelRoomResponse.msg="";
        Mockito.when(mRestClient.getHotelRooms(Mockito.anyInt())).thenReturn(Observable.just(hotelRoomResponse));
        RoomUseCaseTest roomUsecaserTest=new RoomUseCaseTest(mRestClient);
        HotelRoomObserver roomObserver=new HotelRoomObserver(mHotelsAuditPresenter,roomUsecaserTest,mHotelAuditView);
        mHotelsAuditPresenter.loadRooms(1,roomUsecaserTest,roomObserver);
        Mockito.verify(mHotelAuditView).onLoadHotelRoomsFailed(Mockito.anyString());
    }


    @Test
    public void loadAreas_success(){
        FormDataResponse formDataResponse=getFormDataResponse();
        formDataResponse.status=Constants.STATUS_SUCCESS;
        Mockito.when(mRestClient.getFormMetaData()).thenReturn(Observable.just(formDataResponse));
        FormMetaDataUseCaseTest formMetaDataUseCaseTest=new FormMetaDataUseCaseTest(mRestClient);
        HotelAuditAreaObserver hotelAuditAreaObserver=new HotelAuditAreaObserver(mHotelsAuditPresenter,formMetaDataUseCaseTest,mHotelAuditView);
        mHotelsAuditPresenter.loadAreas(formMetaDataUseCaseTest,hotelAuditAreaObserver);
        Mockito.verify(mHotelAuditView).onLoadHotelAuditAreas(Mockito.any(FormDataResponse.class));

    }

    @Test
    public void loadAreas_failure(){
        FormDataResponse formDataResponse=getFormDataResponse();
        formDataResponse.status="";
        formDataResponse.msg="";
        Mockito.when(mRestClient.getFormMetaData()).thenReturn(Observable.just(formDataResponse));
        FormMetaDataUseCaseTest formMetaDataUseCaseTest=new FormMetaDataUseCaseTest(mRestClient);
        HotelAuditAreaObserver hotelAuditAreaObserver=new HotelAuditAreaObserver(mHotelsAuditPresenter,formMetaDataUseCaseTest,mHotelAuditView);
        mHotelsAuditPresenter.loadAreas(formMetaDataUseCaseTest,hotelAuditAreaObserver);
        Mockito.verify(mHotelAuditView).onLoadHotelAuditAreaFailed(Mockito.anyString());

    }

    public FormDataResponse getFormDataResponse(){
        FormDataResponse formDataResponse=new FormDataResponse();
        formDataResponse.data=new FormUpdateOutputModel();
        return formDataResponse;
    }

    public HotelRoomResponse getHotelRoomResponse(){
        HotelRoomResponse hotelRoomResponse=new HotelRoomResponse();
        hotelRoomResponse.data=new Audit();
        hotelRoomResponse.data.blind_rooms_count="100";
        hotelRoomResponse.data.completion_ratio="10";
        hotelRoomResponse.data.types=new ArrayList<>();
        return hotelRoomResponse;

    }

}
