package portfolio;

import com.treebo.prowlapp.flowhotelportfolio.PortfolioPresenter;
import com.treebo.prowlapp.flowhotelportfolio.PortfolioResponseObserver;
import com.treebo.prowlapp.mvpviews.PortfolioView;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.PortfolioResponse;
import com.treebo.prowlapp.Utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by sumandas on 16/08/2016.
 */
public class PortfolioPresenterTest {

    @Mock
    PortfolioView portfolioView;

    PortfolioPresenter portfolioPresenter;

    @Mock
    RestClient mRestClient;

    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        portfolioPresenter=new PortfolioPresenter();
        portfolioPresenter.setMvpView(portfolioView);
    }

    @Test
    public void testGetPortfolio_success(){
        PortfolioResponse portfolioResponse=getMockPortFolioResponse();
        portfolioResponse.status= Constants.STATUS_SUCCESS;

        Mockito.when(mRestClient.getAllPortfolio(Mockito.anyInt()))
                .thenReturn(Observable.just(portfolioResponse));

        PortfolioUsecaseTest portfolioUsecaseTest =new PortfolioUsecaseTest(mRestClient);
        PortfolioResponseObserver portfolioResponseObserver=new PortfolioResponseObserver(
                portfolioPresenter, portfolioUsecaseTest,portfolioView);

        portfolioPresenter.getAllPortfolio(portfolioUsecaseTest,portfolioResponseObserver);

        Mockito.verify(portfolioView).onPortfolioSuccess(Mockito.any(PortfolioResponse.class));
    }

    @Test
    public void testGetPortfolio_failure(){
        PortfolioResponse portfolioResponse=getMockPortFolioResponse();
        portfolioResponse.status= "";
        portfolioResponse.msg="";
        Mockito.when(mRestClient.getAllPortfolio(Mockito.anyInt()))
                .thenReturn(Observable.just(portfolioResponse));

        PortfolioUsecaseTest portfolioUsecaseTest =new PortfolioUsecaseTest(mRestClient);
        PortfolioResponseObserver portfolioResponseObserver=new PortfolioResponseObserver(
                portfolioPresenter, portfolioUsecaseTest,portfolioView);

        portfolioPresenter.getAllPortfolio(portfolioUsecaseTest,portfolioResponseObserver);

        Mockito.verify(portfolioView).onPortfolioFailed(Mockito.anyString());
    }

    public PortfolioResponse getMockPortFolioResponse(){
        PortfolioResponse portfolioResponse=new PortfolioResponse();
        portfolioResponse.data=new ArrayList<>();
        PortfolioResponse.PortfolioResponseData data=new PortfolioResponse.PortfolioResponseData();
        data.department="Food & Beverages";
        data.score=25;
        portfolioResponse.data.add(data);
        data=new PortfolioResponse.PortfolioResponseData();
        data.department="Owner / Manager";
        data.score=85;
        portfolioResponse.data.add(data);
        return portfolioResponse;
    }

}
