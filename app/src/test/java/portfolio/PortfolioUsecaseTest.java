package portfolio;

import com.treebo.prowlapp.flowhotelportfolio.PortfolioUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.PortfolioResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by sumandas on 16/08/2016.
 */
public class PortfolioUsecaseTest extends PortfolioUseCase {

    public PortfolioUsecaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<PortfolioResponse> buildObservable() {
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }
}
