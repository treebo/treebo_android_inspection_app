package issuenew;

import com.treebo.prowlapp.flowissuenew.usecase.IssueDetailsUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.issue.IssueDetailResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by abhisheknair on 22/11/16.
 */

public class IssueDetailsUseCaseTest extends IssueDetailsUseCase {

    public IssueDetailsUseCaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<IssueDetailResponse> buildObservable(){
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }
}
