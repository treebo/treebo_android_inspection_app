package issuenew;

import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.presenter.IssueListPresenter;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.issue.IssueListResponse;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.DummyDataUtils;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import rx.Observable;

/**
 * Created by abhisheknair on 22/11/16.
 */

public class IssueListPresenterTest {

    @Mock
    RestClient mRestClient;

    @Mock
    IIssueContract.IIssueListView mIssueListView;

    IssueListPresenter mIssueListPresenter;

    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mIssueListPresenter = new IssueListPresenter();
        mIssueListPresenter.setMvpView(mIssueListView);
        IssueListUseCaseTest useCaseTest = new IssueListUseCaseTest(mRestClient);
        mIssueListPresenter.setIssueListUseCase(useCaseTest);
    }

    @Test
    public void loadIssueList_Success() {
        IssueListResponse response = getIssueList();
        response.status = Constants.STATUS_SUCCESS;
        Mockito.when(mRestClient.getIssueList(Mockito.anyInt())).thenReturn(Observable.just(response));
        mIssueListPresenter.loadIssueList(1);
        Mockito.verify(mIssueListView).setResponseData(response);
    }

    @Test
    public void loadIssueList_Failure() {
        IssueListResponse response = getIssueList();
        response.status = "";
        Mockito.when(mRestClient.getIssueList(Mockito.anyInt())).thenReturn(Observable.just(response));
        mIssueListPresenter.loadIssueList(1);
        Mockito.verify(mIssueListView).showError("Loading issue list failed");
    }

    private IssueListResponse getIssueList() {
        IssueListResponse response = new IssueListResponse();
        response.data = DummyDataUtils.getDummyIssues();
        return response;
    }

}
