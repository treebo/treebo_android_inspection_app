package issuenew;

import com.treebo.prowlapp.flowissuenew.IIssueContract;
import com.treebo.prowlapp.flowissuenew.presenter.IssueDetailsPresenter;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.issue.IssueDetailResponse;
import com.treebo.prowlapp.Utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import rx.Observable;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class IssueDetailsPresenterTest {

    @Mock
    RestClient mRestClient;

    @Mock
    IIssueContract.IIssueDetailsView mIssueDetailsView;

    IssueDetailsPresenter mIssueDetailsPresenter;

    @Before
    public void setUpMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mIssueDetailsPresenter = new IssueDetailsPresenter();
        mIssueDetailsPresenter.setMvpView(mIssueDetailsView);
        IssueDetailsUseCaseTest useCaseTest = new IssueDetailsUseCaseTest(mRestClient);
        mIssueDetailsPresenter.setmUseCase(useCaseTest);
    }

    @Test
    public void loadIssueDetails_Success() {
        IssueDetailResponse response = getIssueDetails();
        response.status = Constants.STATUS_SUCCESS;
        Mockito.when(mRestClient.getIssueDetails(Mockito.anyInt(), Mockito.anyInt())).thenReturn(Observable.just(response));
        mIssueDetailsPresenter.loadIssueDetails(1, 1);
        Mockito.verify(mIssueDetailsView).onLoadIssueDetailsSuccess(response);
    }

    @Test
    public void loadIssueDetails_Failure() {
        IssueDetailResponse response = getIssueDetails();
        response.status = "";
        Mockito.when(mRestClient.getIssueDetails(Mockito.anyInt(), Mockito.anyInt())).thenReturn(Observable.just(response));
        mIssueDetailsPresenter.loadIssueDetails(1, 1);
        Mockito.verify(mIssueDetailsView).onLoadIssueDetailsFailure("Loading issue details failed");
    }

    private IssueDetailResponse getIssueDetails() {
        return new IssueDetailResponse();
    }
}
