package issuenew;

import com.treebo.prowlapp.flowissuenew.usecase.UpdateIssueUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class UpdateIssueUseCaseTest extends UpdateIssueUseCase {
    public UpdateIssueUseCaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<BaseResponse> buildObservable() {
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }
}
