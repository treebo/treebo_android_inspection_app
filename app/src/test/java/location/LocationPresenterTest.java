package location;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.treebo.prowlapp.flowlocation.LocationSplashContract;
import com.treebo.prowlapp.flowlocation.presenter.LocationSplashActivityPresenter;
import com.treebo.prowlapp.Utils.PermissionHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by sumandas on 23/11/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({PermissionHelper.class})
public class LocationPresenterTest {

    @Mock
    Context mContext;

    @Mock
    GoogleApiClient mGoogleApiClient;

    @Mock
    LocationSplashContract.ISplashActivityView mView;

    public LocationSplashActivityPresenter mSplashActivityPresenter;

    public LocationSplashActivityPresenter mSplashActivitySpyPresenter;

    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(PermissionHelper.class);
        mSplashActivityPresenter = new LocationSplashActivityPresenter(mContext);
        mSplashActivityPresenter.setMvpView(mView);
        mSplashActivitySpyPresenter = Mockito.spy(mSplashActivityPresenter);
    }


    @Test
    public void test_presenter_OnCreate() {
        mSplashActivityPresenter.onCreate();
        Mockito.verify(mView).setImageAccordingToTime();
        Mockito.verify(mView).setUpLocationAnimation();
    }

    @Test
    public void detect_location_permissions_disabled() {
        PowerMockito.when(PermissionHelper.hasNoPermissionToAccessLocation(mContext)).thenReturn(false);
        mSplashActivitySpyPresenter.detectLocation(mGoogleApiClient);
        Mockito.verify(mSplashActivitySpyPresenter).startLocationUpdates();
    }

    @Test
    public void detect_location_permissions_enabled() {
        PowerMockito.when(PermissionHelper.hasNoPermissionToAccessLocation(mContext)).thenReturn(true);
        mSplashActivityPresenter.detectLocation(mGoogleApiClient);
        Mockito.verify(mView).requestLocationPermission();
    }
/*
    @Test
    public void on_location_detected(){
        Mockito.when(mSplashActivitySpyPresenter.getDelayObserver()).thenReturn(getDelayObserver());
        mSplashActivitySpyPresenter.onLocationDetected();
        Mockito.verify(mView).showLandingScreen();
    }

    public Observable getDelayObserver(){
        return RxUtils.buildTest(Observable.timer(2,
                TimeUnit.SECONDS));
    }*/

}
