package location;

import android.content.Context;

import com.treebo.prowlapp.flowlocation.EnableLocationContract;
import com.treebo.prowlapp.flowlocation.presenter.EnableLocationPresenter;
import com.treebo.prowlapp.Utils.LocationUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by sumandas on 23/11/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({LocationUtils.class})
public class EnableLocationPresenterTest {

    @Mock
    Context mContext;

    @Mock
    EnableLocationContract.IEnabledLocationView mView;

    public EnableLocationPresenter mEnableLocationPresenter;


    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(LocationUtils.class);
        mEnableLocationPresenter = new EnableLocationPresenter(mContext);
        mEnableLocationPresenter.setMvpView(mView);
    }

    @Test
    public void location_enabled() {
        PowerMockito.when(LocationUtils.isLocationEnabled(mContext)).thenReturn(true);
        mEnableLocationPresenter.onEnableLocationClick();
        Mockito.verify(mView).showLocationSplashScreen();
    }

    @Test
    public void location_disabled() {
        PowerMockito.when(LocationUtils.isLocationEnabled(mContext)).thenReturn(false);
        mEnableLocationPresenter.onEnableLocationClick();
        Mockito.verify(mView).showEnableLocationPermissionScreen();
    }

}
