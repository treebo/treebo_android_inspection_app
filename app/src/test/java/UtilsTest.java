import com.google.android.gms.common.ConnectionResult;
import com.treebo.prowlapp.Utils.Utils;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


/**
 * Created by sumandas on 19/08/2016.
 */
@RunWith(JUnit4.class)
public class UtilsTest extends TestCase {

    @Test
    public void isPlayServicesErrorUserRecoverable_STATUS_CODE_SUCCESS() {
        int statusCode = ConnectionResult.SUCCESS;
        assertEquals(false, Utils.isPlayServicesErrorUserRecoverable(statusCode));
    }

    @Test
    public void isPlayServicesErrorUserRecoverable_STATUS_CODE_SERVICE_MISSING() {
        int statusCode = ConnectionResult.SERVICE_MISSING;
        assertEquals(true, Utils.isPlayServicesErrorUserRecoverable(statusCode));
    }

    @Test
    public void isPlayServicesErrorUserRecoverable_STATUS_CODE_SERVICE_UPDATING() {
        int statusCode = ConnectionResult.SERVICE_UPDATING;
        assertEquals(false, Utils.isPlayServicesErrorUserRecoverable(statusCode));
    }

    @Test
    public void isPlayServicesErrorUserRecoverable_STATUS_CODE_SERVICE_VERSION_UPDATE_REQUIRED() {
        int statusCode = ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED;
        assertEquals(true, Utils.isPlayServicesErrorUserRecoverable(statusCode));
    }

    @Test
    public void isPlayServicesErrorUserRecoverable_STATUS_CODE_SERVICE_DISABLED() {
        int statusCode = ConnectionResult.SERVICE_DISABLED;
        assertEquals(true, Utils.isPlayServicesErrorUserRecoverable(statusCode));
    }

    @Test
    public void isPlayServicesErrorUserRecoverable_STATUS_CODE_SERVICE_INVALID() {
        int statusCode = ConnectionResult.SERVICE_INVALID;
        assertEquals(true, Utils.isPlayServicesErrorUserRecoverable(statusCode));
    }

}

