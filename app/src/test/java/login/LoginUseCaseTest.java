package login;

import com.treebo.prowlapp.flowlogin.usecase.LoginUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.login.LoginResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by sumandas on 16/08/2016.
 */
public class LoginUseCaseTest extends LoginUseCase {

    public LoginUseCaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<LoginResponse> buildObservable(){
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }

}
