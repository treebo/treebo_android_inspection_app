package login;

import android.content.Context;
import android.text.TextUtils;

import com.treebo.prowlapp.application.MainApplication;
import com.treebo.prowlapp.sharedprefs.LoginSharedPrefManager;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by sumandas on 23/08/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({MainApplication.class,TextUtils.class})
public class LoginManagerTest extends TestCase{


    LoginSharedPrefManager loginSharedPrefManager;
    LoginSharedPrefManager spyLoginManager;

    @Mock
    MainApplication mainApplication;

    @Mock
    Context mContext;

    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(MainApplication.class);
        PowerMockito.mockStatic(TextUtils.class);

        PowerMockito.when(TextUtils.isEmpty(Mockito.any(CharSequence.class))).thenAnswer(new Answer<Object>() {
            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {
                CharSequence str = (CharSequence) invocation.getArguments()[0];
                return !(str != null && str.length() > 0);
            }
        });
        PowerMockito.when(MainApplication.getAppContext()).thenReturn(mContext);
        loginSharedPrefManager=LoginSharedPrefManager.getInstance();
        spyLoginManager= Mockito.spy(loginSharedPrefManager);

    }

    @Test
    public void isUseLoggedIn_allFieldsFilled(){
        Mockito.doReturn("aa@gmail.com").when(spyLoginManager).getEmailId();
        Mockito.doReturn(1).when(spyLoginManager).getUserId();
        Mockito.doReturn("aaaa").when(spyLoginManager).getAccessToken();
        boolean isLoggedIn=spyLoginManager.isUserLoggedIn();
        assertEquals(true,isLoggedIn);
    }

    @Test
    public void isUseLoggedIn_userId_Zero(){
        Mockito.doReturn("aa@gmail.com").when(spyLoginManager).getEmailId();
        Mockito.doReturn(0).when(spyLoginManager).getUserId();
        Mockito.doReturn("aaaaa").when(spyLoginManager).getAccessToken();
        boolean isLoggedIn=spyLoginManager.isUserLoggedIn();
        assertEquals(false,isLoggedIn);
    }


    @Test
    public void isUseLoggedIn_emailNull(){
        Mockito.doReturn(null).when(spyLoginManager).getEmailId();
        Mockito.doReturn(1).when(spyLoginManager).getUserId();
        Mockito.doReturn("aaaa").when(spyLoginManager).getAccessToken();
        boolean isLoggedIn=spyLoginManager.isUserLoggedIn();
        assertEquals(false,isLoggedIn);
    }

    @Test
    public void isUseLoggedIn_emailEmpty(){
        Mockito.doReturn("").when(spyLoginManager).getEmailId();
        Mockito.doReturn(1).when(spyLoginManager).getUserId();
        Mockito.doReturn("aaaa").when(spyLoginManager).getAccessToken();
        boolean isLoggedIn=spyLoginManager.isUserLoggedIn();
        assertEquals(false,isLoggedIn);
    }

    @Test
    public void isUseLoggedIn_tokenNull(){
        Mockito.doReturn("aa@gmail.com").when(spyLoginManager).getEmailId();
        Mockito.doReturn(1).when(spyLoginManager).getUserId();
        Mockito.doReturn(null).when(spyLoginManager).getAccessToken();
        boolean isLoggedIn=spyLoginManager.isUserLoggedIn();
        assertEquals(false,isLoggedIn);
    }


    @Test
    public void isUseLoggedIn_tokenEmpty(){
        Mockito.doReturn("aa@gmail.com").when(spyLoginManager).getEmailId();
        Mockito.doReturn(1).when(spyLoginManager).getUserId();
        Mockito.doReturn("").when(spyLoginManager).getAccessToken();
        boolean isLoggedIn=spyLoginManager.isUserLoggedIn();
        assertEquals(false,isLoggedIn);
    }




}
