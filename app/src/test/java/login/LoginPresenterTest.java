package login;

import com.treebo.prowlapp.flowlogin.LoginPresenter;
import com.treebo.prowlapp.flowlogin.observers.ChangeOTPObserver;
import com.treebo.prowlapp.flowlogin.observers.LoginResponseObserver;
import com.treebo.prowlapp.mvpviews.LoginView;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.ChangeOTPResponse;
import com.treebo.prowlapp.response.login.LoginResponse;
import com.treebo.prowlapp.Utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Observable;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by sumandas on 15/08/2016.
 */
public class LoginPresenterTest {

    @Mock
    LoginView mLoginView;

    LoginPresenter mLoginPresenter;

    @Mock
    RestClient mRestClient;


    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mLoginPresenter=new LoginPresenter(mRestClient);
        mLoginPresenter.setMvpView(mLoginView);
    }

    @Test
    public void testLogin_CorrectUserNamePassword(){
        LoginResponse loginResponse=getLoginResponse();
        loginResponse.status= Constants.STATUS_SUCCESS;
        when(mRestClient.attemptLogin(anyString(),anyString()))
               .thenReturn(Observable.just(loginResponse));
        LoginUseCaseTest loginUseCase=new LoginUseCaseTest(mRestClient);
        LoginResponseObserver loginResponseObserver=new LoginResponseObserver(mLoginPresenter,loginUseCase,mLoginView);
        loginUseCase.setLoginFields("hello@treebohotels.com","password");
        mLoginPresenter.attemptLogin(loginUseCase,loginResponseObserver);
        verify(mLoginView).onLoginSuccess(any(LoginResponse.class));

    }

    @Test
    public void testLogin_IncorrectUserNamePassword(){
        LoginResponse loginResponse=getLoginResponse();
        loginResponse.status= "";
        loginResponse.msg= "error";

        when(mRestClient.attemptLogin(anyString(),anyString()))
                .thenReturn(Observable.just(loginResponse));
        LoginUseCaseTest loginUseCase=new LoginUseCaseTest(mRestClient);
        LoginResponseObserver loginResponseObserver=new LoginResponseObserver(mLoginPresenter,loginUseCase,mLoginView);
        loginUseCase.setLoginFields("hello@treebohotels.com","password");
        mLoginPresenter.attemptLogin(loginUseCase,loginResponseObserver);
        verify(mLoginView).onLoginFailure(loginResponse.msg);

    }

    @Test
    public void testLogin_OtpChangeSuccess(){
        ChangeOTPResponse otpResponse=getChangeOTPResponse();
        otpResponse.status=Constants.STATUS_SUCCESS;
        when(mRestClient.changeOtp(anyString()))
                .thenReturn(Observable.just(otpResponse));
        ChangeOTPUseCaseTest changeOTPUseCaseTest=new ChangeOTPUseCaseTest(mRestClient);
        ChangeOTPObserver changeOTPObserver=new ChangeOTPObserver(mLoginPresenter,changeOTPUseCaseTest,mLoginView);
        changeOTPUseCaseTest.setPhone("9721134537");
        changeOTPObserver.setPhone("9721134537");
        mLoginPresenter.changeOtp(changeOTPUseCaseTest,changeOTPObserver);
        verify(mLoginView).onOTPChangeSuccess(anyString());
    }

    @Test
    public void testLogin_OtpChangeFailure(){
        ChangeOTPResponse otpResponse=getChangeOTPResponse();
        otpResponse.status="";
        otpResponse.msg="";
        when(mRestClient.changeOtp(anyString()))
        .thenReturn(Observable.just(otpResponse));
        ChangeOTPUseCaseTest changeOTPUseCaseTest=new ChangeOTPUseCaseTest(mRestClient);
        ChangeOTPObserver changeOTPObserver=new ChangeOTPObserver(mLoginPresenter,changeOTPUseCaseTest,mLoginView);
        changeOTPUseCaseTest.setPhone("9721134537");
        changeOTPObserver.setPhone("9721134537");
        mLoginPresenter.changeOtp(changeOTPUseCaseTest,changeOTPObserver);
        verify(mLoginView).onOTPChangeFailure(anyString(),anyString());
    }

    
    public LoginResponse getLoginResponse(){
        LoginResponse loginResponse=new LoginResponse();
        loginResponse.setData(new LoginResponse.LoginData());
        LoginResponse.LoginData loginData=loginResponse.getData();
        loginData.setUserId(1);
        loginData.setUserEmail("hello@treebohotels.com");
        return loginResponse;
    }

    public ChangeOTPResponse getChangeOTPResponse(){
        ChangeOTPResponse changeOTPResponse=new ChangeOTPResponse();
        changeOTPResponse.data=new ChangeOTPResponse.ChangeOTPData();
        changeOTPResponse.setMessage("");
        return changeOTPResponse;

    }
}
