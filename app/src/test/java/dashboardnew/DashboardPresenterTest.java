package dashboardnew;

import com.treebo.prowlapp.flowdashboard.DashboardContract;
import com.treebo.prowlapp.flowdashboard.presenter.DashboardPresenter;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.response.dashboard.HotelMetricsResponse;
import com.treebo.prowlapp.response.dashboard.HotelTasksResponse;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * Created by abhisheknair on 15/12/16.
 */

public class DashboardPresenterTest {

    @Mock
    RestClient mRestClient;

    @Mock
    DashboardContract.IDashboardView mDashboardView;

    DashboardPresenter mDashboardPresenter;

    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mDashboardPresenter = new DashboardPresenter();
        mDashboardPresenter.setMvpView(mDashboardView);
        HotelTasksUseCaseTest tasksUseCaseTest = new HotelTasksUseCaseTest(mRestClient);
        HotelMetricUseCaseTest hotelMetricsUseCaseTest = new HotelMetricUseCaseTest(mRestClient);
        TaskDoneUseCaseTest taskDoneUseCaseTest = new TaskDoneUseCaseTest(mRestClient);
        mDashboardPresenter.setHotelTaskUseCase(tasksUseCaseTest);
        mDashboardPresenter.setMetricUseCase(hotelMetricsUseCaseTest);
        mDashboardPresenter.setTaskDoneUseCase(taskDoneUseCaseTest);
    }

    private HotelTasksResponse getHotelTasksResponse() {
        return new HotelTasksResponse();
    }

    private HotelMetricsResponse getHotelMetricsResponse() {
        return new HotelMetricsResponse();
    }

    private BaseResponse getTaskDoneResponse() {
        return new BaseResponse();
    }
}
