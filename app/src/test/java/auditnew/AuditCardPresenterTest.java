package auditnew;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.presenter.AuditCardPresenter;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;
import com.treebo.prowlapp.Utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import rx.Observable;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class AuditCardPresenterTest {

    @Mock
    RestClient mRestClient;

    @Mock
    AuditContract.IAuditCardView mAuditCardView;

    AuditCardPresenter mAuditCardPresenter;

    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mAuditCardPresenter = new AuditCardPresenter();
        mAuditCardPresenter.setMvpView(mAuditCardView);
        RoomSingleUseCaseTest roomListUseCaseTest = new RoomSingleUseCaseTest(mRestClient);
        mAuditCardPresenter.setmRoomSingleUse(roomListUseCaseTest);
    }

    @Test
    public void loadSingleRoomDetails_Success() {
        RoomSingleResponse response = getSingleRoomData();
        response.status = Constants.STATUS_SUCCESS;
        response.setRoomSingleData(new RoomSingleResponse.RoomSingleData());
        Mockito.when(mRestClient.getAuditTypeRoom(Mockito.anyInt(), Mockito.anyInt())).thenReturn(Observable.just(response));
        mAuditCardPresenter.loadRoomAudit(1, 1);
//        Mockito.verify(mAuditCardView).onLoadRoomAudit(response);
    }

    @Test
    public void loadSingleRoomDetails_Failure() {
        RoomSingleResponse response = getSingleRoomData();
        response.status = "";
        Mockito.when(mRestClient.getAuditTypeRoom(Mockito.anyInt(), Mockito.anyInt())).thenReturn(Observable.just(response));
        mAuditCardPresenter.loadRoomAudit(1, 1);
        Mockito.verify(mAuditCardView).onLoadRoomAuditFailure("Loading room audit failed");
    }

    private RoomSingleResponse getSingleRoomData() {
        return new RoomSingleResponse();
    }

}
