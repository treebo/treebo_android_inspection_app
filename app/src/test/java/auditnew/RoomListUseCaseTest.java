package auditnew;

import com.treebo.prowlapp.flowauditnew.usecase.RoomListUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.RoomListResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by abhisheknair on 22/11/16.
 */

public class RoomListUseCaseTest extends RoomListUseCase {

    public RoomListUseCaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<RoomListResponse> buildObservable(){
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }
}
