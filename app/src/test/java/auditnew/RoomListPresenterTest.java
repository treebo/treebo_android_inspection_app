package auditnew;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.presenter.RoomListPresenter;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.Utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import rx.Observable;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class RoomListPresenterTest {

    @Mock
    RestClient mRestClient;

    @Mock
    AuditContract.IRoomListView mRoomListView;

    RoomListPresenter mRoomListPresenter;

    @Before
    public void setUpMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mRoomListPresenter = new RoomListPresenter();
        mRoomListPresenter.setMvpView(mRoomListView);
        RoomListUseCaseTest roomListUseCaseTest = new RoomListUseCaseTest(mRestClient);
        mRoomListPresenter.setRoomListUseCase(roomListUseCaseTest);
    }

    @Test
    public void loadSingleRoomDetails_Success() {
        RoomListResponse response = getSingleRoomData();
        response.status = Constants.STATUS_SUCCESS;
        Mockito.when(mRestClient.getRoomList(Mockito.anyInt())).thenReturn(Observable.just(response));
        mRoomListPresenter.loadRoomAudit(1);
        Mockito.verify(mRoomListView).onLoadRoomList(response);
    }

    @Test
    public void loadSingleRoomDetails_Failure() {
        RoomListResponse response = getSingleRoomData();
        response.status = "";
        Mockito.when(mRestClient.getRoomList(Mockito.anyInt())).thenReturn(Observable.just(response));
        mRoomListPresenter.loadRoomAudit(1);
        Mockito.verify(mRoomListView).onLoadRoomListFailure(Mockito.anyString());
    }

    private RoomListResponse getSingleRoomData() {
        return new RoomListResponse();
    }

}
