package auditnew;

import com.treebo.prowlapp.flowauditnew.usecase.RoomSingleUseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.RoomSingleResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class RoomSingleUseCaseTest extends RoomSingleUseCase {
    public RoomSingleUseCaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<RoomSingleResponse> buildObservable() {
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }
}
