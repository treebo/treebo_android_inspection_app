package auditnew;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.presenter.ConfirmActionPresenter;
import com.treebo.prowlapp.Models.auditmodel.AnswerV3;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;
import com.treebo.prowlapp.Utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class ConfirmActionPresenterTest {

    @Mock
    RestClient mRestClient;

    @Mock
    AuditContract.IConfirmAuditActionView mConfirmView;

    ConfirmActionPresenter mConfirmActionPresenter;

    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mConfirmActionPresenter = new ConfirmActionPresenter();
        mConfirmActionPresenter.setMvpView(mConfirmView);
        SubmitAuditUseCaseTest roomListUseCaseTest = new SubmitAuditUseCaseTest(mRestClient);
        mConfirmActionPresenter.setmSubmitUseCase(roomListUseCaseTest);
    }

    @Test
    public void submitAudit_Success() {
        SubmitAuditResponse response = getSingleRoomData();
        response.data.status = Constants.STATUS_SUCCESS;
        Mockito.when(mRestClient
                .submitAuditV3(new ArrayList<>(Mockito.anyCollectionOf(AnswerV3.class)), Mockito.anyInt(), Mockito.anyInt(),
                        Mockito.anyInt(), Mockito.anyInt(), Mockito.anyString(),Mockito.anyInt()))
                .thenReturn(Observable.just(response));
        mConfirmActionPresenter.submitAudit(new ArrayList<>(), 1, 1, 1, 1, "", 1);
        Mockito.verify(mConfirmView).onAuditSubmitSuccess(response);
    }

    @Test
    public void submitAudit_Failure() {
        SubmitAuditResponse response = getSingleRoomData();
        response.data.status = "";
        Mockito.when(mRestClient
                .submitAuditV3(new ArrayList<>(Mockito.anyCollectionOf(AnswerV3.class)), Mockito.anyInt(), Mockito.anyInt(),
                        Mockito.anyInt(), Mockito.anyInt(), Mockito.anyString(),Mockito.anyInt()))
                .thenReturn(Observable.just(response));
        mConfirmActionPresenter.submitAudit(new ArrayList<>(), 1, 1, 1, 1, "", 1);
        Mockito.verify(mConfirmView).onAuditSubmitFailure(Mockito.anyString());
    }

    private SubmitAuditResponse getSingleRoomData() {
        SubmitAuditResponse response = new SubmitAuditResponse();
        response.msg = "Error msg";
        response.data = new SubmitAuditResponse.AuditStatus();
        return response;
    }
}
