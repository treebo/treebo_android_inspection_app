package auditnew;

import com.treebo.prowlapp.flowauditnew.AuditContract;
import com.treebo.prowlapp.flowauditnew.presenter.DailyAuditPresenter;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.BaseResponse;
import com.treebo.prowlapp.response.audit.CommonAreaTypeResponse;
import com.treebo.prowlapp.response.audit.RoomListResponse;
import com.treebo.prowlapp.Utils.Constants;
import com.treebo.prowlapp.Utils.DummyDataUtils;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by abhisheknair on 22/11/16.
 */

public class DailyAuditPresenterTest {

    @Mock
    RestClient mRestClient;

    @Mock
    AuditContract.IDailyAuditView mDailyAuditView;

    DailyAuditPresenter mDailyAuditPresenter;

    @Before
    public void setupMocksAndView() {
        MockitoAnnotations.initMocks(this);
        mDailyAuditPresenter = new DailyAuditPresenter();
        mDailyAuditPresenter.setMvpView(mDailyAuditView);
        RoomListUseCaseTest roomListUseCaseTest = new RoomListUseCaseTest(mRestClient);
        CommonAreaUseCaseTest commonAreaUseCaseTest = new CommonAreaUseCaseTest(mRestClient);
        RoomNotReadyUseCaseTest roomNotReadyUseCaseTest = new RoomNotReadyUseCaseTest(mRestClient);
        mDailyAuditPresenter.setmRoomListUseCase(roomListUseCaseTest);
        mDailyAuditPresenter.setmCommonUseCase(commonAreaUseCaseTest);
        mDailyAuditPresenter.setRoomNotReadyUseCase(roomNotReadyUseCaseTest);
    }

    @Test
    public void loadRoomListSuccess() {
        RoomListResponse roomListResponse = getRoomList();
        roomListResponse.status = Constants.STATUS_SUCCESS;
        Mockito.when(mRestClient.getRoomList(Mockito.anyInt())).thenReturn(Observable.just(roomListResponse));
        mDailyAuditPresenter.loadRoomList(1);
        Mockito.verify(mDailyAuditView).onLoadRoomList(roomListResponse);
    }

    @Test
    public void loadRoomListFailure() {
        RoomListResponse roomListResponse = getRoomList();
        roomListResponse.status = "";
        Mockito.when(mRestClient.getRoomList(Mockito.anyInt())).thenReturn(Observable.just(roomListResponse));
        mDailyAuditPresenter.loadRoomList(1);
        Mockito.verify(mDailyAuditView).onLoadRoomListFailure("Loading room checklist failed");
    }

    @Test
    public void loadCommonAuditSuccess() {
        CommonAreaTypeResponse commonAreaResponse = getCommonArea();
        commonAreaResponse.status = Constants.STATUS_SUCCESS;
        Mockito.when(mRestClient.getAuditTypeCommon(Mockito.anyInt())).thenReturn(Observable.just(commonAreaResponse));
        mDailyAuditPresenter.loadCommonAudit(1);
//        Mockito.verify(mDailyAuditView).onLoadCommonAudit(commonAreaResponse);
        Mockito.verify(mDailyAuditView).hideLoading();
    }

    @Test
    public void loadCommonAuditFailure() {
        CommonAreaTypeResponse commonAreaResponse = getCommonArea();
        commonAreaResponse.status = "";
        Mockito.when(mRestClient.getAuditTypeCommon(Mockito.anyInt())).thenReturn(Observable.just(commonAreaResponse));
        mDailyAuditPresenter.loadCommonAudit(1);
        Mockito.verify(mDailyAuditView).onLoadCommonFailure("Loading common area checklist failed");
    }

    @Test
    public void roomNotReady_failure() {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.status = "";
        Mockito.when(mRestClient.postRoomNotReady(Mockito.anyInt(), Mockito.anyString())).thenReturn(Observable.just(baseResponse));
        mDailyAuditPresenter.markRoomNotReady(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mDailyAuditView).onRoomNotReadyFailure(Mockito.anyInt());
    }

    private RoomListResponse getRoomList() {
        RoomListResponse response = new RoomListResponse();
        response.setRoomDataObject(new RoomListResponse.RoomDataObject());
        response.getRoomDataObject().setRoomList(new ArrayList<>());
        response.getRoomDataObject().getRoomList().addAll(DummyDataUtils.getDummyRooms());
        return response;
    }

    private CommonAreaTypeResponse getCommonArea() {
        CommonAreaTypeResponse response = new CommonAreaTypeResponse();
        response.setData(new CommonAreaTypeResponse.CommonAreaObject());
        return response;
    }
}
