package auditnew;

import com.treebo.prowlapp.flowauditnew.usecase.SubmitAuditV3UseCase;
import com.treebo.prowlapp.net.RestClient;
import com.treebo.prowlapp.response.audit.SubmitAuditResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by abhisheknair on 23/11/16.
 */

public class SubmitAuditUseCaseTest extends SubmitAuditV3UseCase {
    public SubmitAuditUseCaseTest(RestClient restClient) {
        super(restClient);
    }

    @Override
    protected Observable<SubmitAuditResponse> buildObservable() {
        return getObservable().subscribeOn(Schedulers.immediate())
                .debounce(2000, TimeUnit.MILLISECONDS)
                .timeout(120, TimeUnit.SECONDS)
                .observeOn(Schedulers.immediate());
    }
}
