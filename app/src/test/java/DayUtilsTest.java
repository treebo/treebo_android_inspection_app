import com.treebo.prowlapp.Utils.DateUtils;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.text.ParseException;

/**
 * Created by sumandas on 18/01/2017.
 */
@RunWith(JUnit4.class)
public class DayUtilsTest extends TestCase {

    @Test
    public void isTimeWithinTest() throws ParseException {
        String startTime = "00:00:30";
        String endTime = "00:10:30";
        String timeTocheck = "00:05:00";
        assertTrue(DateUtils.checkTimeWithin(startTime, endTime, timeTocheck));
        timeTocheck = "10:05:00";
        assertFalse(DateUtils.checkTimeWithin(startTime, endTime, timeTocheck));
        startTime = "13:12:13";
        endTime = "18:12:16";
        assertFalse(DateUtils.checkTimeWithin(startTime, endTime, timeTocheck));
        startTime = "5:5:5";
        assertTrue(DateUtils.checkTimeWithin(startTime, endTime, timeTocheck));
    }


}
